﻿using AutoMapper;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public sealed class ContentToFilepathResolver : IValueResolver<Content, ContentModel, string>
    {
        private readonly IFileIOService fileIOService;

        public ContentToFilepathResolver(IFileIOService fileIOService)
        {
            this.fileIOService = fileIOService;
        }

        public string Resolve(Content source, ContentModel destination, string destMember, ResolutionContext context)
        {
            if (source != null)
            {
                return fileIOService.GetLink(source);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
