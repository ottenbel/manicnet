﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public sealed class LookupToLocalizedResolver : IValueResolver<CacheConfiguration, CacheConfigurationModifyModel, string>
    {
        private readonly IStringLocalizer<GeneralLocalization> generalLocalizer;
        private readonly IStringLocalizer<ConfigurationLocalization> configurationLocalizer;

        public LookupToLocalizedResolver(IStringLocalizer<ConfigurationLocalization> configurationLocalizer, IStringLocalizer<GeneralLocalization> generalLocalizer)
        {
            this.generalLocalizer = generalLocalizer;
            this.configurationLocalizer = configurationLocalizer;
        }

        public string Resolve(CacheConfiguration source, CacheConfigurationModifyModel destination, string destMember, ResolutionContext context)
        {
            string result = generalLocalizer[GeneralLocalization.UNKNOWN];

            if (source.LookupTypeID == Caching.Configurations.LookupTypes.ALL) { result = configurationLocalizer[ConfigurationLocalization.CacheConfiguration.ALL]; }
            else if (source.LookupTypeID == Caching.Configurations.LookupTypes.FIND) { result = configurationLocalizer[ConfigurationLocalization.CacheConfiguration.FIND]; }

            return result;
        }
    }
}
