﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public sealed class SectionToLocalizedResolver : IValueResolver<CacheConfiguration, CacheConfigurationModifyModel, string>
    {
        private readonly IStringLocalizer<GeneralLocalization> generalLocalizer;

        public SectionToLocalizedResolver(IStringLocalizer<GeneralLocalization> generalLocalizer)
        {
            this.generalLocalizer = generalLocalizer;
        }

        public string Resolve(CacheConfiguration source, CacheConfigurationModifyModel destination, string destMember, ResolutionContext context)
        {
            string result = generalLocalizer[GeneralLocalization.UNKNOWN];

            if (source.SectionID == Caching.Configurations.Sections.TAG_TYPE) { result = generalLocalizer[GeneralLocalization.TAG_TYPE]; }
            else if (source.SectionID == Caching.Configurations.Sections.TAG) { result = generalLocalizer[GeneralLocalization.TAG]; }
            else if (source.SectionID == Caching.Configurations.Sections.TAG_ALIAS) { result = generalLocalizer[GeneralLocalization.TAG_ALIAS]; }
            else if (source.SectionID == Caching.Configurations.Sections.WORK) { result = generalLocalizer[GeneralLocalization.WORK]; }
            else if (source.SectionID == Caching.Configurations.Sections.VOLUME) { result = generalLocalizer[GeneralLocalization.VOLUME]; }
            else if (source.SectionID == Caching.Configurations.Sections.CHAPTER) { result = generalLocalizer[GeneralLocalization.CHAPTER]; }
            else if (source.SectionID == Caching.Configurations.Sections.COLLECTION) { result = generalLocalizer[GeneralLocalization.COLLECTION]; }
            else if (source.SectionID == Caching.Configurations.Sections.REGISTRATION_INVITE) { result = generalLocalizer[GeneralLocalization.REGISTRATION_INVITE]; }
            else if (source.SectionID == Caching.Configurations.Sections.USER_BLOCKED_TAG) { result = generalLocalizer[GeneralLocalization.USER_BLOCKED_TAG]; }
            else if (source.SectionID == Caching.Configurations.Sections.CACHE_CONFIGURATION) { result = generalLocalizer[GeneralLocalization.CACHE_CONFIGURATION]; }
            else if (source.SectionID == Caching.Configurations.Sections.SOCIAL_LINK) { result = generalLocalizer[GeneralLocalization.SOCIAL_LINKS]; }

            return result;
        }
    }
}
