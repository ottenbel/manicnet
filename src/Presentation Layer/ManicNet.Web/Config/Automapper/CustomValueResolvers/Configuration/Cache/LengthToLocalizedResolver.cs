﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public sealed class LengthToLocalizedResolver : IValueResolver<CacheConfiguration, CacheConfigurationModifyModel, List<SelectListItem>>
    {
        private readonly IStringLocalizer<ConfigurationLocalization> configurationLocalizer;

        public LengthToLocalizedResolver(IStringLocalizer<ConfigurationLocalization> configurationLocalizer)
        {
            this.configurationLocalizer = configurationLocalizer;
        }

        public List<SelectListItem> Resolve(CacheConfiguration source, CacheConfigurationModifyModel destination, List<SelectListItem> destMember, ResolutionContext context)
        {
            List<SelectListItem> result = new List<SelectListItem>()
            {
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.DAY], Caching.Configurations.LengthTypes.DAY.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.HOUR], Caching.Configurations.LengthTypes.HOUR.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.MINUTE], Caching.Configurations.LengthTypes.MINUTE.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.SECOND], Caching.Configurations.LengthTypes.SECOND.ToString()),
            };

            return result;
        }
    }
}
