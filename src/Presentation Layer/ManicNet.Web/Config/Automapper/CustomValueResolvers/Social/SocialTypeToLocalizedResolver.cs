﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public class SocialTypeToLocalizedResolver : IValueResolver<SocialLink, SocialLinkViewModel, string>
    {
        private readonly IStringLocalizer<SocialLocalization> socialLocalizer;

        public SocialTypeToLocalizedResolver(IStringLocalizer<SocialLocalization> socialLocalizer)
        {
            this.socialLocalizer = socialLocalizer;
        }

        public string Resolve(SocialLink source, SocialLinkViewModel destination, string destMember, ResolutionContext context)
        {
            string result = socialLocalizer[SocialLocalization.LinkType.UNKNOWN];

            if (source.TypeID == Constants.Social.LinkTypes.EMAIL)
            {
                result = socialLocalizer[SocialLocalization.LinkType.EMAIL];
            }
            else if (source.TypeID == Constants.Social.LinkTypes.LINK)
            {
                result = socialLocalizer[SocialLocalization.LinkType.LINK];
            }

            return result;
        }
    }
}
