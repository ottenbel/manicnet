﻿using AutoMapper;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.CustomValueResolvers
{
    public sealed class ThumbnailToFilepathResolver : IValueResolver<Content, ContentModel, string>
    {
        private readonly IFileIOService fileIOService;

        public ThumbnailToFilepathResolver(IFileIOService fileIOService)
        {
            this.fileIOService = fileIOService;
        }

        public string Resolve(Content source, ContentModel destination, string destMember, ResolutionContext context)
        {
            if (source != null && source.Thumbnail != null)
            {
                return fileIOService.GetLink(source.Thumbnail);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
