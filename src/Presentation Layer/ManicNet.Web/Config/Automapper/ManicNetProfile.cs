﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;
using ManicNet.Web.Config.Automapper.Converters;
using ManicNet.Web.Config.Automapper.Converters.Role;
using ManicNet.Web.Config.Automapper.CustomValueResolvers;
using System.Collections.Generic;

namespace ManicNet.Web.Config.Automapper
{
    //MAYBE: Clean up automapper profile to better fit guidelines https://jimmybogard.com/automapper-usage-guidelines/
    public sealed class ManicNetProfile : Profile
    {
        public ManicNetProfile()
        {
            CreateMap<TagType, TagTypeViewModel>().ReverseMap();
            CreateMap<TagType, TagTypeModifyModel>().ReverseMap();
            CreateMap<List<TagType>, TagTypeTagInputModel>().ConvertUsing(new TagTypeTagInputModelConverter());
            CreateMap<List<TagType>, TagTypeTagPriorityInputModel>().ConvertUsing(new TagTypeTagPriorityInputModelConverter());

            CreateMap<Tag, TagModifyModel>().ConvertUsing(new TagModifyModelConverter());
            CreateMap<TagModifyModel, Tag>()
                .ForMember(t => t.Aliases, opt => opt.Ignore());
            CreateMap<Tag, TagViewModel>().ConvertUsing(new TagViewModelConverter());
            CreateMap<Tag, TagMigrationModel>().ConvertUsing(new TagMigrationModelConverter());
            CreateMap<Tag, TagDownloadModel>().ConvertUsing(new TagDownloadModelConverter());

            CreateMap<RegistrationInvite, RegistrationInviteViewModel>();

            CreateMap<WorkModifyModel, Work>()
                .ForMember(w => w.Tags, opt => opt.Ignore())
                .ForMember(w => w.Cover, opt => opt.Ignore());

            CreateMap<VolumeModifyModel, Volume>()
                .ForMember(w => w.Tags, opt => opt.Ignore())
                .ForMember(w => w.Cover, opt => opt.Ignore());
            CreateMap<Volume, VolumeMigrationModel>().ConvertUsing(new VolumeMigrationModelConverter());

            CreateMap<ChapterModifyModel, Chapter>()
                .ForMember(w => w.Tags, opt => opt.Ignore())
                .ForMember(w => w.Pages, opt => opt.Ignore());
            CreateMap<Chapter, ChapterMigrationModel>().ConvertUsing(new ChapterMigrationModelConverter());

            CreateMap<CollectionModifyModel, Collection>()
                .ForMember(c => c.Works, opt => opt.Ignore());

            CreateMap<Content, ContentModel>()
                .ForMember(dest => dest.Thumbnail,
                src => src.MapFrom<ThumbnailToFilepathResolver>())
                .ForMember(dest => dest.Content,
                src => src.MapFrom<ContentToFilepathResolver>());

            CreateMap<Work, WorkSummaryViewModel>().ConvertUsing(new WorkSummaryViewModelConverter());
            CreateMap<Work, WorkViewModel>().ConvertUsing(new WorkViewModelConverter());
            CreateMap<Work, WorkModifyModel>().ConvertUsing(new WorkModifyModelConverter());

            CreateMap<Volume, VolumeSummaryViewModel>().ConvertUsing(new VolumeSummaryViewModelConverter());
            CreateMap<Volume, VolumeViewModel>().ConvertUsing(new VolumeViewModelConverter());
            CreateMap<Volume, VolumeModifyModel>().ConvertUsing(new VolumeModifyModelConverter());

            CreateMap<Chapter, ChapterSummaryViewModel>().ConvertUsing(new ChapterSummaryViewModelConverter());
            CreateMap<Chapter, ChapterDeletedSummaryViewModel>().ConvertUsing(new ChapterDeletedSummaryViewModelConverter());
            CreateMap<Chapter, ChapterViewModel>().ConvertUsing(new ChapterViewModelConverter());
            CreateMap<Chapter, ChapterModifyModel>().ConvertUsing(new ChapterModifyModelConverter());

            CreateMap<Page, PageViewModel>().ConvertUsing(new PageViewModelConverter());
            CreateMap<Page, PageModifyViewModel>().ConvertUsing(new PageModifyViewModelConverter());

            CreateMap<Collection, CollectionSummaryViewModel>().ConvertUsing(new CollectionSummaryViewModelConverter());
            CreateMap<Collection, CollectionViewModel>().ConvertUsing(new CollectionViewModelConverter());
            CreateMap<Collection_Work, CollectionWorkViewModel>().ConvertUsing(new CollectionWorkViewModelConverter());
            CreateMap<Collection_Work, WorkCollectionViewModel>().ConvertUsing(new WorkCollectionViewModelConverter());
            CreateMap<Collection_Work, CollectionWorkModifyModel>().ConvertUsing(new CollectionWorkModifyModelConverter());
            CreateMap<Collection, CollectionModifyModel>().ConvertUsing(new CollectionModifyModelConverter());

            CreateMap<ManicNetRole, RoleViewModel>().ConvertUsing(new RoleViewModelConverter());

            CreateMap<CacheConfiguration, CacheConfigurationModifyModel>()
                .ForMember(dest => dest.LengthTypes,
                src => src.MapFrom<LengthToLocalizedResolver>())
                .ForMember(dest => dest.LookupTypeText,
                src => src.MapFrom<LookupToLocalizedResolver>())
                .ForMember(dest => dest.SectionText,
                src => src.MapFrom<SectionToLocalizedResolver>());

            CreateMap<SocialLink, SocialLinkViewModel>()
                .ForMember(dest => dest.TypeName, src => src.MapFrom<SocialTypeToLocalizedResolver>());
            CreateMap<SocialLinkModifyModel, SocialLink>().ReverseMap();
        }
    }
}
