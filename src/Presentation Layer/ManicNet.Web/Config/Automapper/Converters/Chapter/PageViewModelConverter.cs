﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class PageViewModelConverter : ITypeConverter<Page, PageViewModel>
    {
        public PageViewModel Convert(Page source, PageViewModel destination, ResolutionContext context)
        {
            PageViewModel result = new PageViewModel
            {
                PageNumber = source.Number,
                Content = context.Mapper.Map<ContentModel>(source.Content)
            };

            return result;
        }
    }
}
