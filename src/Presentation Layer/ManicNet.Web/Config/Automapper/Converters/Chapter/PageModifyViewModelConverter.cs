﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class PageModifyViewModelConverter : ITypeConverter<Page, PageModifyViewModel>
    {
        public PageModifyViewModel Convert(Page source, PageModifyViewModel destination, ResolutionContext context)
        {
            PageModifyViewModel result = new PageModifyViewModel
            {
                PageID = source.PageID,
                PageNumber = source.Number,
                Content = context.Mapper.Map<ContentModel>(source.Content)
            };

            return result;
        }
    }
}
