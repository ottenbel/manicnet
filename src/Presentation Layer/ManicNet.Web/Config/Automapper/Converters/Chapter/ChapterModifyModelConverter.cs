﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class ChapterModifyModelConverter : ITypeConverter<Chapter, ChapterModifyModel>
    {
        public ChapterModifyModel Convert(Chapter source, ChapterModifyModel destination, ResolutionContext context)
        {
            List<Tag> tags = new List<Tag>();
            List<TagType> tagTypes = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tags = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypes = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }            

            ChapterModifyModel result = new ChapterModifyModel(source.ChapterID, source.VolumeID, source.Number, source.Title, source.ShortDescription, source.LongDescription, source.URL);

            context.Items[ModelMapping.PRIMARY_TAGS] = tags.Where(i => source.Tags.Any(t => t.IsPrimary && t.TagID == i.TagID)).OrderBy(t => t.TagType.Name).ThenBy(t => t.Name).ToList();
            context.Items[ModelMapping.SECONDARY_TAGS] = tags.Where(i => source.Tags.Any(t => !t.IsPrimary && t.TagID == i.TagID)).OrderBy(t => t.TagType.Name).ThenBy(t => t.Name).ToList();
            result.Tags = context.Mapper.Map<TagTypeTagPriorityInputModel>(tagTypes);
            result.Tags.AllowVirtual = false;

            result.Pages = context.Mapper.Map<List<PageModifyViewModel>>(source.Pages);

            return result;
        }
    }
}
