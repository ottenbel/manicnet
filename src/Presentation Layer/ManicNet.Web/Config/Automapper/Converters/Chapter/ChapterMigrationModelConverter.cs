﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class ChapterMigrationModelConverter : ITypeConverter<Chapter, ChapterMigrationModel>
    {
        public ChapterMigrationModel Convert(Chapter source, ChapterMigrationModel destination, ResolutionContext context)
        {
            ChapterMigrationModel result = new ChapterMigrationModel(source.ChapterID, source.Number);

            Work work = new Work();

            //As of automapper 9.0 context.Items is null if items aren't passed in from the mapping
            if (context.Items.ContainsKey(ModelMapping.WORK))
            {
                work = (Work)context.Items[ModelMapping.WORK];
            }

            result.Name = string.Format("{0} - Chapter {1}{2}", work.Title, source.Number, string.IsNullOrWhiteSpace(source.Title) ? string.Empty : string.Format(" - {0}", source.Title));
            result.CollectionVolumes = work.Volumes.Where(v => v.VolumeID != source.VolumeID && !v.IsDeleted).Select(v => new SelectListItem { Text = string.IsNullOrWhiteSpace(v.Title) ? string.Format("Volume {0}", v.Number) : string.Format("Volume {0} - {1}", v.Number, v.Title), Value = v.VolumeID.ToString() }).ToList();

            return result;
        }
    }
}
