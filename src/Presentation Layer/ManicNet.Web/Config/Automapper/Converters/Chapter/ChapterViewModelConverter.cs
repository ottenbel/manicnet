﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class ChapterViewModelConverter : ITypeConverter<Chapter, ChapterViewModel>
    {
        public ChapterViewModel Convert(Chapter source, ChapterViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();
            Guid? previousChapterID = null;
            Guid? nextChapterID = null;
            int lastPageOfPreviousChapter = 0;
            int currentPageNumber = 0;

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            if (context.Items.ContainsKey(ModelMapping.PREVIOUS_CHAPTER_ID))
            {
                previousChapterID = (Guid?)context.Items[ModelMapping.PREVIOUS_CHAPTER_ID];
            }

            if (context.Items.ContainsKey(ModelMapping.NEXT_CHAPTER_ID))
            {
                nextChapterID = (Guid?)context.Items[ModelMapping.NEXT_CHAPTER_ID];
            }

            if (context.Items.ContainsKey(ModelMapping.LAST_PAGE_OF_PREVIOUS_CHAPTER))
            {
                int? setValue = (int?)context.Items[ModelMapping.LAST_PAGE_OF_PREVIOUS_CHAPTER];
                lastPageOfPreviousChapter = setValue ?? 0;
            }

            if (context.Items.ContainsKey(ModelMapping.CURRENT_PAGE))
            {
                int? setValue = (int?)context.Items[ModelMapping.CURRENT_PAGE];
                currentPageNumber = setValue ?? 0;
            }

            ChapterViewModel result = new ChapterViewModel(source.ChapterID, source.Title, source.Number, source.ShortDescription, source.LongDescription, source.URL, source.IsDeleted, source.Volume.WorkID, source.Volume.Work.Title, source.VolumeID, source.Volume.Number, source.Volume.Title, source.Volume.IsDeleted || source.Volume.Work.IsDeleted, previousChapterID, nextChapterID, lastPageOfPreviousChapter, currentPageNumber);

            if (currentPageNumber >= 0 && currentPageNumber < source.Pages.Count)
            {
                result.CurrentPage = context.Mapper.Map<ContentModel>(source.Pages[currentPageNumber].Content);
            }
            else
            {
                result.CurrentPage = context.Mapper.Map<ContentModel>(source.Pages[0].Content);
            }

            List<Tag> tags = tagList.Where(i => source.Tags.Any(t => t.TagID == i.TagID)).ToList();

            List<TagTypeTagList> tagsSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(tags, tagTypesList, source.Tags);

            foreach (var sortedTag in tagsSortedTagList)
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                };

                foreach (Tag tag in sortedTag.tags.OrderByDescending(t => t.IsPrimary).ThenByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(tag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Tags.Add(entry);
            }

            result.Pages = context.Mapper.Map<List<PageViewModel>>(source.Pages.OrderBy(p => p.Number).ToList());

            return result;
        }
    }
}
