﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters.Role
{
    public sealed class RoleViewModelConverter : ITypeConverter<ManicNetRole, RoleViewModel>
    {
        public RoleViewModel Convert(ManicNetRole source, RoleViewModel destination, ResolutionContext context)
        {
            RoleViewModel result = new RoleViewModel(source.Id, source.Name, source.Description, source.IsDefaultUser);

            return result;
        }
    }
}
