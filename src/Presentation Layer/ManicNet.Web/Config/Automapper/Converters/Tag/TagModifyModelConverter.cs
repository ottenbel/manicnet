﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagModifyModelConverter : ITypeConverter<Tag, TagModifyModel>
    {
        public TagModifyModel Convert(Tag source, TagModifyModel destination, ResolutionContext context)
        {
            TagModifyModel result = new TagModifyModel(source.TagID, source.TagTypeID, source.Name, source.ShortDescription, source.LongDescription, source.URL, source.IsVirtual, source.Icon, source.FontFamily);

            List<SelectListItem> tagTypeSelectList = new List<SelectListItem>();

            List<TagType> tagTypes = new List<TagType>();
            List<Tag> tags = new List<Tag>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tags = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypes = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            result.TagTypes = tagTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.TagTypeID.ToString() }).ToList();
            List<Tag> implies = tags.Where(i => source.Implies.Any(t => t.ImpliesTagID == i.TagID)).OrderBy(t => t.TagType.Name).ThenBy(t => t.Name).ToList();

            context.Items[ModelMapping.TAGS] = implies;

            result.ImpliedTags = context.Mapper.Map<TagTypeTagInputModel>(tagTypes);
            result.ImpliedTags.AllowVirtual = true;
            result.ImpliedTags.Header = "Implies Tags";
            result.Aliases = string.Join(", ", source.Aliases.Select(t => t.Name).OrderBy(t => t).ToList());

            return result;
        }
    }
}
