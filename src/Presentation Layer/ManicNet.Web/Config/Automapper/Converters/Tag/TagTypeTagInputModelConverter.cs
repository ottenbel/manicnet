﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagTypeTagInputModelConverter : ITypeConverter<List<TagType>, TagTypeTagInputModel>
    {
        public TagTypeTagInputModel Convert(List<TagType> source, TagTypeTagInputModel destination, ResolutionContext context)
        {
            TagTypeTagInputModel result = new TagTypeTagInputModel();

            List<Tag> tags = new List<Tag>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tags = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            foreach (TagType tagType in source)
            {
                DynamicTagInput tag = new DynamicTagInput
                {
                    TagType = new SimplifiedTagType(tagType.TagTypeID, tagType.Name, tagType.Priority),
                    Tags = string.Join(", ", tags.Where(t => t.TagTypeID == tagType.TagTypeID && t.IsDeleted == false)
                    .Select(t => t.TagType.Name + ":" + t.Name).OrderBy(x => x).ToList())
                };

                result.Tags.Add(tag);
            }

            return result;
        }
    }
}
