﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagMigrationModelConverter : ITypeConverter<Tag, TagMigrationModel>
    {
        public TagMigrationModel Convert(Tag source, TagMigrationModel destination, ResolutionContext context)
        {
            TagMigrationModel result = new TagMigrationModel(source.TagID, source.Name);

            return result;
        }
    }
}
