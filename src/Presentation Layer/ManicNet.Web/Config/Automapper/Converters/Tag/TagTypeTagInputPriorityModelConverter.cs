﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagTypeTagPriorityInputModelConverter : ITypeConverter<List<TagType>, TagTypeTagPriorityInputModel>
    {
        public TagTypeTagPriorityInputModel Convert(List<TagType> source, TagTypeTagPriorityInputModel destination, ResolutionContext context)
        {
            TagTypeTagPriorityInputModel result = new TagTypeTagPriorityInputModel();

            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();

            if (context.Items.ContainsKey(ModelMapping.PRIMARY_TAGS))
            {
                primaryTags = (List<Tag>)context.Items[ModelMapping.PRIMARY_TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.SECONDARY_TAGS))
            {
                secondaryTags = (List<Tag>)context.Items[ModelMapping.SECONDARY_TAGS];
            }

            foreach (TagType tagType in source)
            {
                DynamicTagInput primaryTag = new DynamicTagInput();
                DynamicTagInput secondaryTag = new DynamicTagInput();
                primaryTag.TagType = new SimplifiedTagType(tagType.TagTypeID, tagType.Name, tagType.Priority);
                secondaryTag.TagType = new SimplifiedTagType(tagType.TagTypeID, tagType.Name, tagType.Priority);

                primaryTag.Tags = string.Join(", ", primaryTags.Where(t => t.TagTypeID == tagType.TagTypeID && t.IsDeleted == false)
                    .Select(t => t.TagType.Name + ":" + t.Name).OrderBy(x => x).ToList());

                secondaryTag.Tags = string.Join(", ", secondaryTags.Where(t => t.TagTypeID == tagType.TagTypeID && t.IsDeleted == false)
                    .Select(t => t.TagType.Name + ":" + t.Name).OrderBy(x => x).ToList());

                result.PrimaryTags.Add(primaryTag);
                result.SecondaryTags.Add(secondaryTag);
            }

            return result;
        }
    }
}
