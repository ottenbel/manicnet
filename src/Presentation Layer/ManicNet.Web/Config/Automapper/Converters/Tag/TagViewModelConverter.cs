﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagViewModelConverter : ITypeConverter<Tag, TagViewModel>
    {
        public TagViewModel Convert(Tag source, TagViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            TagViewModel result = new TagViewModel(source)
            {
                TagType = context.Mapper.Map<TagTypeViewModel>(source.TagType)
            };

            List<Tag> impliedBy = tagList.Where(i => source.ImpliedBy.Any(t => t.ImpliedByTagID == i.TagID)).ToList();
            List<Tag> implies = tagList.Where(i => source.Implies.Any(t => t.ImpliesTagID == i.TagID)).ToList();

            List<TagTypeTagList> impliedBySortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(impliedBy, tagTypesList);
            List<TagTypeTagList> impliesSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(implies, tagTypesList);

            foreach (var impliedBySortedTag in impliedBySortedTagList)
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(impliedBySortedTag.tagType)
                };
                foreach (Tag impliedByTag in impliedBySortedTag.tags.OrderByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(impliedByTag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(impliedBySortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.ImpliedBy.Add(entry);
            }

            foreach (var impliesSortedTag in impliesSortedTagList)
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(impliesSortedTag.tagType)
                };
                foreach (Tag impliesTag in impliesSortedTag.tags.OrderByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(impliesTag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(impliesSortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Implies.Add(entry);
            }

            result.Aliases = source.Aliases.Select(t => t.Name).OrderBy(t => t).ToList();

            return result;
        }
    }
}
