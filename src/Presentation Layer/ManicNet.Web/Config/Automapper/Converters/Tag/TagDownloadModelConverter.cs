﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class TagDownloadModelConverter : ITypeConverter<Tag, TagDownloadModel>
    {
        public TagDownloadModel Convert(Tag source, TagDownloadModel destination, ResolutionContext context)
        {
            TagDownloadModel result = new TagDownloadModel(source.TagID, source.Name, source.TagTypeID, source.TagType.Name, source.ShortDescription, source.LongDescription, source.URL);

            return result;
        }
    }
}
