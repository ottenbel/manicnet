﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class CollectionModifyModelConverter : ITypeConverter<Collection, CollectionModifyModel>
    {
        public CollectionModifyModel Convert(Collection source, CollectionModifyModel destination, ResolutionContext context)
        {
            CollectionModifyModel result = new CollectionModifyModel(source.CollectionID, source.Title, source.ShortDescription, source.LongDescription, source.URL);

            result.Works = context.Mapper.Map<List<CollectionWorkModifyModel>>(source.Works.Where(cw => cw.Work.IsDeleted == false));

            return result;
        }
    }
}
