﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class CollectionWorkModifyModelConverter : ITypeConverter<Collection_Work, CollectionWorkModifyModel>
    {
        public CollectionWorkModifyModel Convert(Collection_Work source, CollectionWorkModifyModel destination, ResolutionContext context)
        {
            CollectionWorkModifyModel result = new CollectionWorkModifyModel(source.CollectionID, source.WorkID, source.Number, source.Work.Title);

            return result;
        }
    }
}
