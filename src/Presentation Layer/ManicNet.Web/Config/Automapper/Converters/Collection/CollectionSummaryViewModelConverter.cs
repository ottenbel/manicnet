﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class CollectionSummaryViewModelConverter : ITypeConverter<Collection, CollectionSummaryViewModel>
    {
        public CollectionSummaryViewModel Convert(Collection source, CollectionSummaryViewModel destination, ResolutionContext context)
        {
            CollectionSummaryViewModel result = new CollectionSummaryViewModel(source.CollectionID, source.Title, source.ShortDescription, source.Works.Count());

            return result;
        }
    }
}
