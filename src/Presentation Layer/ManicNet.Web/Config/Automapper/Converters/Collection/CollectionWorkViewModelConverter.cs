﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class CollectionWorkViewModelConverter : ITypeConverter<Collection_Work, CollectionWorkViewModel>
    {
        public CollectionWorkViewModel Convert(Collection_Work source, CollectionWorkViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            CollectionWorkViewModel result = new CollectionWorkViewModel(source.Number, source.WorkID, source.Work.Title, source.Work.ShortDescription);

            if (source.Work.Cover != null)
            {
                result.Cover = context.Mapper.Map<ContentModel>(source.Work.Cover);
            }

            List<Tag> tags = tagList.Where(i => source.Work.Tags.Any(t => t.TagID == i.TagID)).ToList();

            List<TagTypeTagList> tagsSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(tags, tagTypesList, source.Work.Tags);

            foreach (var sortedTag in tagsSortedTagList.Where(tstl => tstl.tagType.DisplayedInWorkSummary).OrderBy(tstl => tstl.tagType.Priority))
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                };
                foreach (Tag tag in sortedTag.tags.OrderByDescending(t => t.IsPrimary).ThenByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(tag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Tags.Add(entry);
            }

            return result;
        }
    }
}
