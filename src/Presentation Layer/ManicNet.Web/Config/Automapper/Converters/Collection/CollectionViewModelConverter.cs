﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class CollectionViewModelConverter : ITypeConverter<Collection, CollectionViewModel>
    {
        public CollectionViewModel Convert(Collection source, CollectionViewModel destination, ResolutionContext context)
        {
            CollectionViewModel result = new CollectionViewModel(source.CollectionID, source.Title, source.ShortDescription, source.LongDescription, source.URL, source.IsDeleted)
            {
                Works = context.Mapper.Map<List<CollectionWorkViewModel>>(source.Works.Where(w => w.Work.IsDeleted == false))
            };

            return result;
        }
    }
}
