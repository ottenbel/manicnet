﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class WorkCollectionViewModelConverter : ITypeConverter<Collection_Work, WorkCollectionViewModel>
    {
        public WorkCollectionViewModel Convert(Collection_Work source, WorkCollectionViewModel destination, ResolutionContext context)
        {
            WorkCollectionViewModel result = new WorkCollectionViewModel(source.CollectionID, source.Collection.Title, source.Collection.ShortDescription);

            return result;
        }
    }
}
