﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class VolumeViewModelConverter : ITypeConverter<Volume, VolumeViewModel>
    {
        public VolumeViewModel Convert(Volume source, VolumeViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            VolumeViewModel result = new VolumeViewModel(source.VolumeID, source.WorkID, source.Number, source.Title, source.Work.Title, source.ShortDescription, source.LongDescription, source.URL, source.IsDeleted, source.Work.IsDeleted);

            if (source.Cover != null)
            {
                result.Cover = context.Mapper.Map<ContentModel>(source.Cover);
            }
            else
            {
                result.Cover = new ContentModel();
            }

            List<Tag> tags = tagList.Where(i => source.Tags.Any(t => t.TagID == i.TagID)).ToList();

            List<TagTypeTagList> tagsSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(tags, tagTypesList, source.Tags);

            foreach (var sortedTag in tagsSortedTagList)
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                };
                foreach (Tag tag in sortedTag.tags.OrderByDescending(t => t.IsPrimary).ThenByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(tag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Tags.Add(entry);
            }

            result.Chapters = context.Mapper.Map<List<ChapterSummaryViewModel>>(source.Chapters.Where(c => c.IsDeleted == false).OrderBy(c => c.Number));
            if (result.Chapters.Any())
            {
                result.Chapters.ForEach(c => c.IsExpanded = true);
                result.FirstChapterID = result.Chapters.FirstOrDefault()?.ChapterID;
            }
            result.HasDeletedChapters = source.Chapters.Where(v => v.IsDeleted).Any();

            return result;
        }
    }
}
