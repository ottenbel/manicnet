﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class VolumeMigrationModelConverter : ITypeConverter<Volume, VolumeMigrationModel>
    {
        public VolumeMigrationModel Convert(Volume source, VolumeMigrationModel destination, ResolutionContext context)
        {
            VolumeMigrationModel result = new VolumeMigrationModel(source.VolumeID, source.Number);

            Work work = new Work();

            if (context.Items.ContainsKey(ModelMapping.WORK))
            {
                work = (Work)context.Items[ModelMapping.WORK];
            }

            result.Name = string.Format("{0} - Volume {1}{2}", work.Title, source.Number, string.IsNullOrWhiteSpace(source.Title) ? string.Empty : string.Format(" - {0}", source.Title));

            return result;
        }
    }
}
