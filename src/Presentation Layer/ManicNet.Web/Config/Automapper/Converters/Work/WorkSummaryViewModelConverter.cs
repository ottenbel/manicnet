﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class WorkSummaryViewModelConverter : ITypeConverter<Work, WorkSummaryViewModel>
    {
        public WorkSummaryViewModel Convert(Work source, WorkSummaryViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            WorkSummaryViewModel result = new WorkSummaryViewModel(source.WorkID, source.Title, source.ShortDescription, source.SortableDate);

            if (source.Cover != null)
            {
                result.Cover = context.Mapper.Map<ContentModel>(source.Cover);
            }
            else
            {
                result.Cover = new ContentModel();
            }

            List<Tag> tags = tagList.Where(i => source.Tags.Any(t => t.TagID == i.TagID)).ToList();

            List<TagTypeTagList> tagsSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(tags, tagTypesList, source.Tags);

            foreach (var sortedTag in tagsSortedTagList.Where(tstl => tstl.tagType.DisplayedInWorkSummary).OrderBy(tstl => tstl.tagType.Priority))
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                };

                foreach (Tag tag in sortedTag.tags.OrderByDescending(t => t.IsPrimary).ThenByDescending(t => t.Count).ThenBy(t => t.Name).Take(sortedTag.tagType.NumberDisplayedInWorkSummary ?? 5))
                {
                    TagViewModel tagViewModel = new TagViewModel(tag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Tags.Add(entry);
            }

            return result;
        }
    }
}
