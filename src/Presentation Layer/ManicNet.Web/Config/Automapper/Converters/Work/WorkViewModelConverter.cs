﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class WorkViewModelConverter : ITypeConverter<Work, WorkViewModel>
    {
        public WorkViewModel Convert(Work source, WorkViewModel destination, ResolutionContext context)
        {
            List<Tag> tagList = new List<Tag>();
            List<TagType> tagTypesList = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tagList = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypesList = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            WorkViewModel result = new WorkViewModel(source.WorkID, source.Title, source.ShortDescription, source.LongDescription, source.URL, source.SortableDate, source.IsDeleted);

            if (source.Cover != null)
            {
                result.Cover = context.Mapper.Map<ContentModel>(source.Cover);
            }
            else
            {
                result.Cover = new ContentModel();
            }

            List<Tag> tags = tagList.Where(i => source.Tags.Any(t => t.TagID == i.TagID)).ToList();

            List<TagTypeTagList> tagsSortedTagList = TagOutputHelper.ConvertTagListIntoSortedTagList(tags, tagTypesList, source.Tags);

            foreach (var sortedTag in tagsSortedTagList)
            {
                TagTypeTagListViewModel entry = new TagTypeTagListViewModel
                {
                    TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                };
                foreach (Tag tag in sortedTag.tags.OrderByDescending(t => t.IsPrimary).ThenByDescending(t => t.Count).ThenBy(t => t.Name))
                {
                    TagViewModel tagViewModel = new TagViewModel(tag)
                    {
                        TagType = context.Mapper.Map<TagTypeViewModel>(sortedTag.tagType)
                    };

                    entry.Tags.Add(tagViewModel);
                }
                result.Tags.Add(entry);
            }

            result.Volumes = context.Mapper.Map<List<VolumeSummaryViewModel>>(source.Volumes.Where(v => v.IsDeleted == false).OrderBy(v => v.Number));
            if (result.Volumes.Any())
            {
                result.Volumes.Last().IsExpanded = true;
                result.FirstChapterID = source.Volumes.Where(v => v.Chapters.Any(c => c.IsDeleted == false) && v.IsDeleted == false)
                    .OrderBy(v => v.Number).FirstOrDefault()?.Chapters.Where(c => c.IsDeleted == false).OrderBy(c => c.Number)
                    .FirstOrDefault()?.ChapterID;
            }
            result.HasDeletedVolumes = source.Volumes.Where(v => v.IsDeleted).Any();

            result.Collections = context.Mapper.Map<List<WorkCollectionViewModel>>(source.Collections.Where(c => c.Collection.IsDeleted == false).OrderBy(c => c.Collection.Title));

            return result;
        }
    }
}
