﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public sealed class WorkModifyModelConverter : ITypeConverter<Work, WorkModifyModel>
    {
        public WorkModifyModel Convert(Work source, WorkModifyModel destination, ResolutionContext context)
        {
            List<Tag> tags = new List<Tag>();
            List<TagType> tagTypes = new List<TagType>();

            if (context.Items.ContainsKey(ModelMapping.TAGS))
            {
                tags = (List<Tag>)context.Items[ModelMapping.TAGS];
            }

            if (context.Items.ContainsKey(ModelMapping.TAG_TYPES))
            {
                tagTypes = (List<TagType>)context.Items[ModelMapping.TAG_TYPES];
            }

            WorkModifyModel result = new WorkModifyModel(source.WorkID, source.Title, source.ShortDescription, source.LongDescription, source.URL);

            if (source.Cover != null)
            {
                ContentModel content = context.Mapper.Map<ContentModel>(source.Cover);
                result.CoverImage = content.Thumbnail;
            }

            context.Items[ModelMapping.PRIMARY_TAGS] = tags.Where(i => source.Tags.Any(t => t.IsPrimary && t.TagID == i.TagID)).OrderBy(t => t.TagType.Name).ThenBy(t => t.Name).ToList();
            context.Items[ModelMapping.SECONDARY_TAGS] = tags.Where(i => source.Tags.Any(t => !t.IsPrimary && t.TagID == i.TagID)).OrderBy(t => t.TagType.Name).ThenBy(t => t.Name).ToList();
            result.Tags = context.Mapper.Map<TagTypeTagPriorityInputModel>(tagTypes);
            result.Tags.AllowVirtual = false;

            return result;
        }
    }
}
