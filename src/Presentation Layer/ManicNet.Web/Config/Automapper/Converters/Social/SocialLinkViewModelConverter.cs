﻿using AutoMapper;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Config.Automapper.Converters
{
    public class SocialLinkViewModelConverter : ITypeConverter<SocialLink, SocialLinkViewModel>
    {
        public SocialLinkViewModel Convert(SocialLink source, SocialLinkViewModel destination, ResolutionContext context)
        {
            SocialLinkViewModel result = new SocialLinkViewModel()
            {
                Name = source.Name,
                SocialLinkID = source.SocialLinkID,
                Priority = source.Priority,
                Icon = source.Icon,
                FontFamily = source.FontFamily,
                URL = source.URL,
                Text = source.Text,
                TypeID = source.TypeID,
                IsDeleted = source.IsDeleted
            };

            return result;
        }
    }
}
