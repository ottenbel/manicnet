﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).on('click', '.deleteButton', function (event) {
	ConfirmDelete(event);
});

$(document).on('submit', 'form', function (event) {
	DisableButton(event);
})

$(document).on('click', '.regenerateRSSButton', function (event) {
	ConfirmRSSRegeneration(event);
});

function ConfirmDelete(event) {
	if (confirm("Are you sure you want to delete?") === true) {
		//Allow the event to submit as normal
	}
	else {
		event.preventDefault();
	}
}

function ConfirmRSSRegeneration(event) {
	if (confirm("Are you sure you want to regenerate your site RSS key? Any existing RSS feeds using the existing key will be invalidated and will need to be replaced with feeds containing the new key.") === true) {
		//Allow the event to submit as normal
	}
	else {
		event.preventDefault();
	}
}

function DisableButton(event) {
	event.originalEvent.submitter.disabled = true;
	event.originalEvent.submitter.textContent = "Processing . . .";
}