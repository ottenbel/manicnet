﻿function next_page() {

    $(".previous_page_link").attr("href", window.location);

    if (currentPage + 1 < chapterPagesCount) {
        currentPage = currentPage + 1;
        set_viewer(currentPage);

        $(".previous_page_link_container").show();
    }
    else if (nextChapterID !== "") {
        let nextChapterURL = "/chapter/viewer/" + nextChapterID + "/";
        window.location.href = nextChapterURL;
    }
    else {
        let workURL = "/work/show/" + workID + "/";
        window.location.href = workURL;
    }

    if (currentPage + 1 < chapterPagesCount) {
        let nextPage = currentPage + 1;
        let nextPageURL = "/chapter/viewer/" + currentChapterID + "/" + nextPage;
        $(".next_page_link").attr("href", nextPageURL);
    }
}

function previous_page() {
    $(".next_page_link").attr("href", window.location);

    if (currentPage - 1 >= 0) {
        currentPage = currentPage - 1;
        set_viewer(currentPage);
    }
    else if (previousChapterID !== "") {
        let previousChapterURL = "/chapter/viewer/" + previousChapterID + "/" + previousChapterLastPage;
        window.location.href = previousChapterURL;
    }
    else {
        let workURL = "/work/show/" + workID + "/";
        window.location.href = workURL;
    }

    if (currentPage - 1 > 0) {
        let previousPage = currentPage - 1;
        let previousPageURL = "/chapter/viewer/" + currentChapterID + "/" + previousPage;
        $(".previous_page_link").attr("href", previousPageURL);
    }
    else if (previousChapterID === "") {
        $(".previous_page_link_container").hide();
    }
}

function set_viewer(currentPage) {
    //Update image in viewer
    $("#viewer_current_page").attr("src", chapterPages[currentPage]);
    $('html, body').animate({ scrollTop: $('#page_viewer_container').position().top }, 'fast');

    //Update the page url
    let updatedURL = "/chapter/viewer/" + currentChapterID + "/" + currentPage;
    history.replaceState(null, '', updatedURL);
}

document.onkeydown = function (key_press) {
    //Catch user pressing the left arrow key
    if (+key_press.keyCode === 37) {
        previous_page();
    }

    //Catch user pressing right arrow key
    if (+key_press.keyCode === 39) {
        next_page();
    }
}

window.onload = function () {
    $('.previous_page_link').click(function () {
        previous_page();
        return false;
    });

    $('.next_page_link').click(function () {
        next_page();
        return false;
    });
}
