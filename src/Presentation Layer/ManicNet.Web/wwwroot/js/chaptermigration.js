﻿function ToggleMigrateChapterToVolumeOutsideOfWork(checkbox) {
    $checkbox = $(checkbox);
    if ($checkbox.is(":checked")) {
        $('#ProvidedVolume').val(null);
        $("#FreeformVolume").prop('disabled', false);
        $('#ProvidedVolume').prop('disabled', true);
        $("#MigrateInsideVolume").hide();
        $("#MigrateOutsideOfVolume").show();
    }
    else {
        $("#FreeformVolume").val(null);
        $("#FreeformVolume").prop('disabled', true);
        $('#ProvidedVolume').prop('disabled', false);
        $("#MigrateInsideVolume").show();
        $("#MigrateOutsideOfVolume").hide();
    }
}