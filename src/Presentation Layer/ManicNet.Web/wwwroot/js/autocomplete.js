﻿$(function () {

    function split(val) {
        if (val === undefined) {
            val = '';
        }

        return val.split(/,\s*/);
    }

    function extractLast(term) {
        return split(term).pop();
    }

    function TagTypeAheadTrigger(event) {
        if (event.keyCode === $.ui.keyCode.TAB &&
            $(this).autocomplete("instance").menu.active) {
            event.preventDefault();
        }
    }

    function DynamicTypeAheadAutoCompleteEditSource(request, dynamicTagType, dynamicIsVirtual, response) {
        $.ajax({
            url: "/api/typeahead",
            type: "GET",
            data: {
                searchString: extractLast(request.term),
                tagType: dynamicTagType,
                isVirtual: dynamicIsVirtual
            },
            success: function (data) {
                console.log(data);
                response(data);
            },
            error: function () {
                console.log("Error retrieving tag list from API.");
            }
        });
    }

    function TagTypeAheadAutoCompleteSearchSource(request, response) {
        $.ajax({
            url: "/api/typeahead",
            type: "GET",
            data: {
                searchString: extractLast(request.term),
                isSearch: true
            },
            success: function (data) {
                console.log(data);
                response(data);
            },
            error: function () {
                console.log("Error retrieving tag list from API.");
            }
        });
    }

    function SingleTagTypeAheadAutoComplete(request, response) {
        $.ajax({
            url: "/api/typeahead",
            type: "GET",
            data: {
                searchString: request.term,
                isVirtual: false
            },
            success: function (data) {
                console.log(data);
                response(data);
            },
            error: function () {
                console.log("Error retrieving tag list from API.");
            }
        });
    }

    function TagTypeAheadAutoCompleteSearch() {
        let term = extractLast(this.value);
        if (term.length < MinimumInputLength) {
            return false;
        }
    }

    $('.searchTagTypeAhead').on("keypress", function (event) { TagTypeAheadTrigger(event); }).autocomplete({
        source: function (request, response) { TagTypeAheadAutoCompleteSearchSource(request, response); },
        search: TagTypeAheadAutoCompleteSearch(),
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });

    $('.singleTagTypeAhead').on("keypress", function (event) { TagTypeAheadTrigger(event); }).autocomplete({
        source: function (request, response) { SingleTagTypeAheadAutoComplete(request, response); },
        search: TagTypeAheadAutoCompleteSearch(),
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            this.value = terms;
            return false;
        }
    });

    //Binding of dynamic tag inputs
    $(function () {
        let binderClassPattern = "tagTypeAhead-".toLowerCase();
        let allowVirtualClassPattern = "allowVirtual";

        $('.dynamicTagInput').each(function () {
            let classes = $(this)[0].className.split(/\s+/);
            let dynamicTypeaheadBinderClass = classes.filter(item => item.toLowerCase().indexOf(binderClassPattern) >= 0).join();
            let tagTypeID = dynamicTypeaheadBinderClass.substring(binderClassPattern.length);
            let allowVirtual = null;
            if (!classes.includes(allowVirtualClassPattern)) {
                allowVirtual = false;
            }

            //We don't care about the typeahead distinguishing based on associated type as we're only exposing the types availiable and binding against that

            $(this).on("keypress", function (event) { TagTypeAheadTrigger(event); }).autocomplete({
                source: function (request, response) { DynamicTypeAheadAutoCompleteEditSource(request, tagTypeID, allowVirtual, response); },
                search: TagTypeAheadAutoCompleteSearch(),
                focus: function () {
                    // prevent value inserted on focus
                    return false;
                },
                select: function (event, ui) {
                    var terms = split(this.value);
                    // remove the current input
                    terms.pop();
                    // add the selected item
                    terms.push(ui.item.value);
                    // add placeholder to get the comma-and-space at the end
                    terms.push("");
                    this.value = terms.join(", ");
                    return false;
                }
            });
        });
    });
});