﻿$(function () {
    function DisplayOrHideSection(checkbox, group) {
        if (checkbox.is(":checked")) {
            group.show();
        }
        else {
            group.hide();
        }
    }

    $("#AvailableOnWork").on("change", function () { DisplayOrHideSection($("#AvailableOnWork"), $("#AvailableOnWorkGroup")); });
    $("#AvailableOnVolume").on("change", function () { DisplayOrHideSection($("#AvailableOnVolume"), $("#AvailableOnVolumeGroup")); });
    $("#AvailableOnChapter").on("change", function () { DisplayOrHideSection($("#AvailableOnChapter"), $("#AvailableOnChapterGroup")); });

    $("#DisplayedInWorkSummary").on("change", function () { DisplayOrHideSection($("#DisplayedInWorkSummary"), $("#NumberAvailableOnWorkGroup")); });
    $("#DisplayedInVolumeSummary").on("change", function () { DisplayOrHideSection($("#DisplayedInVolumeSummary"), $("#NumberAvailableOnVolumeGroup")); });
    $("#DisplayedInChapterSummary").on("change", function () { DisplayOrHideSection($("#DisplayedInChapterSummary"), $("#NumberAvailableOnChapterGroup")); });
});