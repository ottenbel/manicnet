﻿using FluentValidation;
using FluentValidation.AspNetCore;
using ManicNet.Context;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.ApplicationBuilder;
using ManicNet.Infrastructure.ServiceCollection;
using ManicNet.Localization;
using ManicNet.Services;
using ManicNet.Validators;
using ManicNet.Web.Helpers.Infrastructure;
using ManicNet.Web.Helpers.ModelBinders.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace ManicNet.Web
{
    public sealed class Startup
    {
        private readonly IConfiguration configuration;
        private readonly ILogger<Startup> logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            this.configuration = configuration;
            this.logger = logger;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<ManicNetSettings>(configuration.GetSection("ManicNetSettings"));
            ManicNetSettings settings = configuration.GetSection("ManicNetSettings").Get<ManicNetSettings>();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.ConfigureCookies();
            services.ConfigureDatabse(configuration, settings, logger);

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddIdentity<ManicNetUser, ManicNetRole>(options =>
            {
                options.User.AllowedUserNameCharacters = string.Empty;
                //options.User.RequireUniqueEmail = true; //Don't require unique email as it exposes email addresses
            })
            .AddEntityFrameworkStores<ManicNetContext>()
            .AddDefaultTokenProviders();

            services.AddRouting(options =>
            {
                options.AppendTrailingSlash = true;
                options.LowercaseUrls = true;
            });

            services.AddHttpContextAccessor();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.ConfigureUIServices(settings);
            services.ConfigureCustomServices(settings, logger);
            services.AddCustomFrontEndValidation();

            services.AddHostedService<SeedingService>();

            services.AddControllers(options =>
            {
                options.ModelBinderProviders.Insert(0, new CleanStringModelBinderProvider());
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            ManicNetSettings settings = configuration.GetSection("ManicNetSettings").Get<ManicNetSettings>();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseMigrationsEndPoint();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var supportedCultures = new[] { "en-CA", "en-US", "en" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);

            app.UseRequestLocalization(localizationOptions);

            app.UseHttpsRedirection();
            app.ConfigureFileUpload(settings, logger);
            app.UseCookiePolicy();
            app.UseRouting();
            app.UseCors();
            app.UseAuthentication();
            app.UseAuthorization();
            app.AddCustomMiddleware();
            app.ConfigureEndpoints();
        }
    }

    public static class StartupHelpers
    {
        public static IServiceCollection ConfigureUIServices(this IServiceCollection services, ManicNetSettings settings)
        {
            services.AddLocalization();

            services.AddControllersWithViews(config =>
            {
                if (settings.RequireAccount)
                {
                    var policy = new AuthorizationPolicyBuilder()
                                 .RequireAuthenticatedUser()
                                 .Build();
                    config.Filters.Add(new AuthorizeFilter(policy));
                }

                config.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
            })
            .AddViewLocalization()
            .AddDataAnnotationsLocalization(options =>
            {
                options.DataAnnotationLocalizerProvider = (type, factory) =>
                {
                    return factory.Create(typeof(ModelLocalization));
                };
            });

            services.AddFluentValidationAutoValidation();
            services.AddValidatorsFromAssemblyContaining(typeof(TagTypeModifyModelValidator));

            services.AddRazorPages(options =>
            {
                options.Conventions.AuthorizeAreaFolder("Identity", "/Account/Manage");
                options.Conventions.AuthorizeAreaPage("Identity", "/Account/Logout");
            });

            services.ConfigureApplicationCookie(options => { options.LoginPath = "/Identity/Account/Login"; });

            //TODO: Add Fluent validation on razor pages as well?

            return services;
        }

        public static IApplicationBuilder ConfigureEndpoints(this IApplicationBuilder app)
        {
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("areaRoute", "{area:exists}/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute("default", "{controller=Work}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            return app;
        }
    }
}
