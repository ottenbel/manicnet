﻿using ManicNet.Contracts.Services;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Web;
using static ManicNet.Domain.Enums.FileValidation;

namespace ManicNet.Web.Helpers.Validation
{
    public interface IImageValidationService
    {
        Task<ValidationModel> ValidateImageUploadAsync(ValidationModel validation, IFormFile cover, string associatedField);
    }

    public sealed class ImageValidationService : IImageValidationService
    {
        private readonly IImageProcessingService imageProcessingService;

        public ImageValidationService(IImageProcessingService imageProcessingService)
        {
            this.imageProcessingService = imageProcessingService;
        }

        public async Task<ValidationModel> ValidateImageUploadAsync(ValidationModel validation, IFormFile image, string associatedField)
        {
            if (image != null)
            {
                FileValidationResult validationResult = await imageProcessingService.ValidateImageAsync(image);

                if (validationResult != FileValidationResult.IsValid)
                {
                    validation.IsValid = false;

                    string outputMessage = string.Empty;

                    if (validation.ModelState.ContainsKey(associatedField))
                    {
                        validation.ModelState.TryGetValue(associatedField, out outputMessage);
                        validation.ModelState.Remove(associatedField);
                    }

                    outputMessage += string.Format("<li class='text-danger'>{0} file uploaded ({1})", associatedField, HttpUtility.HtmlEncode(image.FileName));

                    outputMessage += validationResult switch
                    {
                        FileValidationResult.FileTooLarge => " was too large.",
                        FileValidationResult.FileTooSmall => " was too small.",
                        FileValidationResult.UnsupportedImageType => " is not of an accepted image type.",
                        FileValidationResult.InvalidFileType => " is not an image.",
                        FileValidationResult.UnknownValidationErrror => " failed validation with an unspecified error.",
                        _ => " returned an unexpected validation result.",
                    };
                    outputMessage += "</li>";
                    validation.ModelState.Add(associatedField, outputMessage);
                }

            }

            return validation;
        }
    }
}
