﻿using ManicNet.Web.Helpers.Validation;
using Microsoft.Extensions.DependencyInjection;

namespace ManicNet.Web.Helpers.Infrastructure
{
    public static class ValidationExtensions
    {
        public static IServiceCollection AddCustomFrontEndValidation(this IServiceCollection services)
        {
            services.AddFrontEndTagValidationServices();

            return services;
        }

        private static IServiceCollection AddFrontEndTagValidationServices(this IServiceCollection services)
        {
            services.AddScoped<IImageValidationService, ImageValidationService>();

            return services;
        }
    }
}
