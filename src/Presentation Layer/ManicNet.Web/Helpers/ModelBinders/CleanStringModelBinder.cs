﻿using Ganss.Xss;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;

namespace ManicNet.Web.Helpers.ModelBinders
{
    public sealed class CleanStringModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
            {
                throw new ArgumentNullException(nameof(bindingContext));
            }

            var modelName = bindingContext.ModelName;

            // Try to fetch the value of the argument by name
            var valueProviderResult = bindingContext.ValueProvider.GetValue(modelName);

            if (valueProviderResult == ValueProviderResult.None)
            {
                return Task.CompletedTask;
            }

            if ((string)valueProviderResult is string str && !string.IsNullOrEmpty(str))
            {
                HtmlSanitizer sanitizer = new HtmlSanitizer();

                bindingContext.Result = ModelBindingResult.Success(sanitizer.Sanitize(str.Trim()));
            }

            return Task.CompletedTask;
        }
    }
}
