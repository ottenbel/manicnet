﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;

namespace ManicNet.Web.Helpers.ModelBinders.Providers
{
    public sealed class CleanStringModelBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException(nameof(context));
            }

            if (!context.Metadata.IsComplexType && context.Metadata.ModelType == typeof(string)
                && context.Metadata.DataTypeName != "Password")
            {
                return new CleanStringModelBinder();
            }

            return null;
        }
    }
}
