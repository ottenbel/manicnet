﻿using Microsoft.AspNetCore.Hosting;

[assembly: HostingStartup(typeof(ManicNet.Web.Areas.Identity.IdentityHostingStartup))]
namespace ManicNet.Web.Areas.Identity
{
    public sealed class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
            });
        }
    }
}