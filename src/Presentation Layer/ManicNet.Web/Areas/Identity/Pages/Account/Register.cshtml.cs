﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class RegisterModel : PageModel
    {
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly ManicNetUserManager userManager;
        private readonly IManicNetEmailService emailService;
        private readonly IRegistrationInviteService registrationInviteService;
        private readonly IOptions<ManicNetSettings> manicNetSettings;
        private readonly RoleManager<ManicNetRole> roleManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.RegisterModel";

        public RegisterModel(
            ManicNetUserManager userManager,
            SignInManager<ManicNetUser> signInManager,
            IManicNetEmailService emailService,
            IRegistrationInviteService registrationInviteService,
            IOptions<ManicNetSettings> manicNetSettings,
            RoleManager<ManicNetRole> roleManager,
            IStringLocalizer<IdentityLocalization> localizer,
            IManicNetLoggerService logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailService = emailService;
            this.registrationInviteService = registrationInviteService;
            this.manicNetSettings = manicNetSettings;
            this.roleManager = roleManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public sealed class InputModel
        {
            [Required]
            [Display(Name = ModelLocalization.USER_NAME)]
            public string UserName { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_BETWEEN, MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.PASSWORD)]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.CONFIRM_PASSWORD)]
            [Compare("Password", ErrorMessage = ModelLocalization.THE_PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH)]
            public string ConfirmPassword { get; set; }

            [Display(Name = ModelLocalization.INVITE_CODE)]
            public Guid? InviteCode { get; set; }
        }

        public void OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            const string functionIdentifier = "OnPostAsync()";

            returnUrl ??= Url.Content("~/");
            bool isValid = ModelState.IsValid;

            if (manicNetSettings.Value.Registration.RequiresInvite)
            {
                if (!Input.InviteCode.HasValue)
                {
                    isValid = false;
                    ModelState.AddModelError("Input.InviteCode", localizer[IdentityLocalization.THE_INVITE_CODE_FIELD_IS_REQUIRED]);
                }
                else if (!(await registrationInviteService.ValidateRegistrationInvite(Input.Email, Input.InviteCode.Value)))
                {
                    isValid = false;
                    ModelState.AddModelError("Input.InviteCode", localizer[IdentityLocalization.THE_INVITE_CODE_IS_INVALID]);
                }
            }

            if (!isValid)
            {
                return Page();
            }

            bool firstUser = !userManager.Users.Any();
            var user = new ManicNetUser { UserName = Input.UserName, Email = Input.Email, RSSKey = Guid.NewGuid() };
            var result = await userManager.CreateAsync(user, Input.Password);

            if (result.Succeeded)
            {
                logger.LogInformation(path, functionIdentifier, string.Format("User {0} created a new account with password.", Input.Email));

                RegistrationInvite registrationInvite = await registrationInviteService.FindRegistrationInvite(Input.Email);
                if (registrationInvite != null)
                {
                    await registrationInviteService.RemoveRegistrationInvite(registrationInvite);
                }

                await PopulateUserClaimsAsync(user, firstUser);

                var code = await userManager.GenerateEmailConfirmationTokenAsync(user);
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmail",
                    pageHandler: null,
                    values: new { userId = user.Id, code },
                    protocol: Request.Scheme);

                await emailService.SendAccountRegistration(Input.Email, Input.UserName, callbackUrl);

                await signInManager.SignInAsync(user, isPersistent: false);
                return LocalRedirect(returnUrl);
            }
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }

        private async Task PopulateUserClaimsAsync(ManicNetUser user, bool firstUser)
        {
            ManicNetRole role;
            if (firstUser)
            {
                role = await roleManager.FindByIdAsync(Roles.DefaultOwnerID.ToString());
            }
            else
            {
                role = roleManager.Roles.Where(r => r.IsDefaultUser).FirstOrDefault();
            }

            await userManager.AddToRoleAsync(user, role.Name);
        }
    }
}
