﻿using ManicNet.Contracts.Services;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class ForgotPasswordModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IManicNetEmailService emailService;

        public ForgotPasswordModel(ManicNetUserManager userManager, IManicNetEmailService emailService)
        {
            this.userManager = userManager;
            this.emailService = emailService;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public sealed class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var user = await userManager.FindByEmailAsync(Input.Email);
                if (user == null || !(await userManager.IsEmailConfirmedAsync(user)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return RedirectToPage("./ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please 
                // visit https://go.microsoft.com/fwlink/?LinkID=532713
                var code = await userManager.GeneratePasswordResetTokenAsync(user);
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { code },
                    protocol: Request.Scheme);

                await emailService.SendResetPasssword(user.Email, user.UserName, HtmlEncoder.Default.Encode(callbackUrl));

                return RedirectToPage("./ForgotPasswordConfirmation");
            }

            return Page();
        }
    }
}
