﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using ManicNet.Web.Helpers.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public partial class IndexModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IManicNetEmailService emailService;
        private readonly IImageProcessingService imageProcessingService;
        private readonly IAvatarService avatarService;
        private readonly IFileIOService fileIOService;
        private readonly IAvatarFileHandlerService avatarFileHandlerService;
        private readonly IImageValidationService imageValidationService;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        public IndexModel(
            ManicNetUserManager userManager,
            SignInManager<ManicNetUser> signInManager,
            IManicNetEmailService emailService,
            IImageProcessingService imageProcessingService,
            IAvatarService avatarService,
            IFileIOService fileIOService,
            IAvatarFileHandlerService avatarFileHandlerService,
            IImageValidationService imageValidationService,
            IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.emailService = emailService;
            this.imageProcessingService = imageProcessingService;
            this.avatarService = avatarService;
            this.fileIOService = fileIOService;
            this.avatarFileHandlerService = avatarFileHandlerService;
            this.imageValidationService = imageValidationService;
            this.localizer = localizer;
        }

        public string Username { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public string UserAvatar { get; set; }

        public sealed class InputModel
        {
            [Required]
            [Display(Name = ModelLocalization.USER_NAME)]
            public string Username { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }

            [Phone]
            [Display(Name = ModelLocalization.PHONE_NUMBER)]
            public string PhoneNumber { get; set; }

            [Display(Name = ModelLocalization.AVATAR)]
            public IFormFile Avatar { get; set; }

            [Display(Name = ModelLocalization.DELETE_AVATAR)]
            public bool DeleteAvatar { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var userName = await userManager.GetUserNameAsync(user);
            var email = await userManager.GetEmailAsync(user);
            var phoneNumber = await userManager.GetPhoneNumberAsync(user);
            var avatar = await userManager.GetAvatarAsync(User);

            Username = userName;

            Input = new InputModel
            {
                Username = userName,
                Email = email,
                PhoneNumber = phoneNumber
            };

            if (avatar != null)
            {
                UserAvatar = fileIOService.GetLink(avatar);
            }

            IsEmailConfirmed = await userManager.IsEmailConfirmedAsync(user);

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }


            ValidationModel validation = await imageValidationService.ValidateImageUploadAsync(new ValidationModel(), Input.Avatar, "Input.Avatar");

            if (!validation.IsValid)
            {
                foreach (KeyValuePair<string, string> errorMessage in validation.ModelState)
                {
                    ModelState.AddModelError(errorMessage.Key, errorMessage.Value);
                }

                return Page();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            Guid? previousAvatarID = user.AvatarID;

            if (Input.Avatar != null)
            {
                byte[] hash = await imageProcessingService.GetHash(Input.Avatar);

                Avatar avatar = await avatarService.FindAvatar(hash);

                if (avatar != null)
                {
                    bool uploadingTheSameAvatar = user.AvatarID == avatar.AvatarID;
                    user.AvatarID = avatar.AvatarID;

                    if (!uploadingTheSameAvatar)
                    {
                        await avatarFileHandlerService.CleanupAvatar(previousAvatarID);
                    }
                }
                else
                {
                    try
                    {
                        user.AvatarID = await avatarFileHandlerService.UploadAvatar(Input.Avatar, hash);
                        await avatarFileHandlerService.CleanupAvatar(previousAvatarID);
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError("Input.Avatar", localizer[IdentityLocalization.FILE_UPLOAD_FAILED]);
                        return Page();
                    }
                }
            }
            else if (Input.DeleteAvatar)
            {
                await avatarFileHandlerService.CleanupAvatar(previousAvatarID);
            }

            if (Input.Username != Username)
            {
                var setUserNameResult = await userManager.SetUserNameAsync(user, Input.Username);
                if (!setUserNameResult.Succeeded)
                {
                    var userId = await userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting user name for user with ID '{userId}'.");
                }
            }

            var email = await userManager.GetEmailAsync(user);
            if (Input.Email != email)
            {
                var setEmailResult = await userManager.SetEmailAsync(user, Input.Email);
                if (!setEmailResult.Succeeded)
                {
                    var userId = await userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting email for user with ID '{userId}'.");
                }
            }

            var phoneNumber = await userManager.GetPhoneNumberAsync(user);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await userManager.SetPhoneNumberAsync(user, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    var userId = await userManager.GetUserIdAsync(user);
                    throw new InvalidOperationException($"Unexpected error occurred setting phone number for user with ID '{userId}'.");
                }
            }

            await signInManager.RefreshSignInAsync(user);
            StatusMessage = localizer[IdentityLocalization.YOUR_PROFILE_HAS_BEEN_UPDATED];
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }


            var userId = await userManager.GetUserIdAsync(user);
            var code = await userManager.GenerateEmailConfirmationTokenAsync(user);

            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { userId, code },
                protocol: Request.Scheme);

            await emailService.SendEmailConfirmation(Input.Email, user.UserName, callbackUrl);

            StatusMessage = localizer[IdentityLocalization.VERIFICATION_EMAIL_SENT];
            return RedirectToPage();
        }
    }
}
