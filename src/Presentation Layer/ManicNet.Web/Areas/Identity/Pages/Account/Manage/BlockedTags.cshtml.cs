using AutoMapper;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class BlockedTagsModel : PageModel
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IUserBlockedTagsUpdateService userBlockedTagsUpdateService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;

        private const string path = "ManicNet.Web.Identity.Pages.Account.BlockedTagsModel";

        public BlockedTagsModel(ITagRetrievalService tagRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IUserBlockedTagsUpdateService userBlockedTagsUpdateService, IOptions<ManicNetSettings> settings, IStringLocalizer<IdentityLocalization> localizer, IMapper mapper, IManicNetLoggerService logger)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.userBlockedTagsUpdateService = userBlockedTagsUpdateService;
            this.localizer = localizer;
            this.settings = settings;
            this.mapper = mapper;
            this.logger = logger;

            BlockedTags = new List<TagViewModel>().ToPagedList(1, settings.Value.PageLength);
        }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public AddTagModel AddTag { get; set; }
        [BindProperty]
        public RemoveTagModel RemoveTag { get; set; }

        public IPagedList<TagViewModel> BlockedTags { get; set; }

        public sealed class AddTagModel
        {
            [Required]
            [Display(Name = ModelLocalization.TAG_TO_BLOCK)]
            public string TagName { get; set; }
        }

        public sealed class RemoveTagModel
        {
            [Required]
            [Display(Name = ModelLocalization.TAG_IDENTIFIER)]
            public Guid TagID { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(int pageNumber = 1)
        {
            const string functionIdentifier = "OnGetAsync(pageNumber)";

            Guid? userID = User.GetUserID();
            if (!userID.HasValue)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userID].ToString());
            }

            try
            {
                PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
                List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);

                List<Tag> tags = await tagRetrievalService.GetActiveTags();
                tags = tags.Where(t => userBlockedTags.Select(t => t.TagID).Contains(t.TagID)).ToList();

                List<TagViewModel> tagsView = mapper.Map<List<TagViewModel>>(tags, opt => { });
                BlockedTags = await tagsView.OrderBy(x => x.Name)
                    .ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ pageNumber:{0} }}", pageNumber));
                StatusMessage = localizer[IdentityLocalization.ERROR_UNABLE_TO_LOAD_BLOCKED_TAGS];
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAddBlockedTagAsync()
        {
            const string functionIdentifier = "OnPostAddBlockedTagAsync()";

            if (!ModelState.IsValid)
            {
                return RedirectToPage();
            }

            Guid? userID = User.GetUserID();
            if (!userID.HasValue)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userID].ToString());
            }

            Tag tagToBlock;
            try
            {
                tagToBlock = await tagRetrievalService.FindTagByName(AddTag.TagName, User);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                StatusMessage = localizer[IdentityLocalization.ERROR_UNABLE_TO_BLOCK_TAG_AS_IT_DOES_NOT_EXIST, AddTag.TagName];
                return RedirectToPage();
            }

            if (await userBlockedTagsRetrievalService.HasUserBlockedTag(tagToBlock.TagID, userID.Value))
            {
                StatusMessage = localizer[IdentityLocalization.TAG_HAS_BEEN_BLOCKED, tagToBlock.Name];
            }
            else
            {
                try
                {
                    await userBlockedTagsUpdateService.CreateUserBlockedTag(tagToBlock.TagID, userID.Value);
                    StatusMessage = localizer[IdentityLocalization.TAG_HAS_BEEN_BLOCKED, tagToBlock.Name];
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagName:{0} }}", AddTag.TagName));
                    StatusMessage = localizer[IdentityLocalization.ERROR_UNABLE_TO_BLOCK_TAG, AddTag.TagName];
                }
            }

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostRemoveBlockedTagAsync()
        {
            const string functionIdentifier = "OnPostRemoveBlockedTagAsync()";

            Guid? userID = User.GetUserID();
            if (!userID.HasValue)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userID].ToString());
            }

            try
            {
                User_Blocked_Tag userBlockedTag = await userBlockedTagsRetrievalService.FindUserBlockedTag(RemoveTag.TagID, User);

                if (userBlockedTag != null)
                {
                    await userBlockedTagsUpdateService.RemoveUserBlockedTag(userBlockedTag);
                }

                StatusMessage = localizer[IdentityLocalization.TAG_IS_REMOVED];
                return RedirectToPage();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagID:{0} }}", RemoveTag.TagID));
                StatusMessage = localizer[IdentityLocalization.ERROR_UNABLE_TO_REMOVE_TAG];
            }

            return RedirectToPage();
        }
    }
}
