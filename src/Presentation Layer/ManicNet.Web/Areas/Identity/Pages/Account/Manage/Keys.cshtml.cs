using ManicNet.Constants;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class KeysModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        [TempData]
        public string StatusMessage { get; set; }

        public KeysModel(ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.localizer = localizer;
        }
        public Guid RSSKey { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
            if (TempData[Identifiers.KEYS] != null)
            {
                StatusMessage = TempData[Identifiers.KEYS].ToString();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var rssKey = await userManager.GetRSSKeyAsync(User);

            RSSKey = rssKey;

            return Page();
        }
    }
}
