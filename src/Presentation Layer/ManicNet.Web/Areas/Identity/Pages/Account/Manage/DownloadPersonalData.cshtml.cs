﻿using AutoMapper;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class DownloadPersonalDataModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IManicNetLoggerService logger;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IMapper mapper;
        private readonly IFileIOService fileIOService;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.DownloadPersonalDataModel";

        public DownloadPersonalDataModel(
            ManicNetUserManager userManager,
            IManicNetLoggerService logger,
            ITagRetrievalService tagRetrievalService,
            IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService,
            IStringLocalizer<IdentityLocalization> localizer,
            IMapper mapper,
            IFileIOService fileIOService
            )
        {
            this.userManager = userManager;
            this.logger = logger;
            this.tagRetrievalService = tagRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.localizer = localizer;
            this.mapper = mapper;
            this.fileIOService = fileIOService;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            const string functionIdentifier = "OnPostAsync()";

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            logger.LogInformation(path, functionIdentifier, string.Format("User with ID '{0}' asked for their personal data.", userManager.GetUserId(User)));

            // Only include personal data for download
            var personalData = new Dictionary<string, object>();
            var personalDataProps = typeof(ManicNetUser).GetProperties().Where(
                            prop => Attribute.IsDefined(prop, typeof(PersonalDataAttribute)));
            foreach (var p in personalDataProps)
            {
                personalData.Add(p.Name, p.GetValue(user)?.ToString() ?? "null");
            }

            await GetUserAvatar(user, personalData);
            await GetUserBlockedTags(user, personalData);

            Response.Headers.Add("Content-Disposition", "attachment; filename=PersonalData.json");
            return new FileContentResult(Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(personalData, Formatting.Indented)), "text/json");
        }

        private async Task GetUserAvatar(ManicNetUser user, Dictionary<string, object> personalData)
        {
            if (user.Avatar != null)
            {
                byte[] avatar = await fileIOService.GetFile(user.Avatar);
                string encodedAvatar = Convert.ToBase64String(avatar);
                personalData.Add("Avatar", encodedAvatar);
            }
            else
            {
                personalData.Add("Avatar", "null");
            }
        }

        private async Task GetUserBlockedTags(ManicNetUser user, Dictionary<string, object> personalData)
        {
            List<Tag> tags = await tagRetrievalService.GetActiveTags();
            List<User_Blocked_Tag> userBlockedTagIDs = await userBlockedTagsRetrievalService.GetUserBlockedTags(user.Id);
            List<TagDownloadModel> userBlockedTags = mapper.Map<List<TagDownloadModel>>(tags.Where(t => userBlockedTagIDs.Select(t => t.TagID).Contains(t.TagID)), opt => { });
            personalData.Add("User Blocked Tags", userBlockedTags);
        }
    }
}
