﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public static class ManageNavPages
    {
        public static string Index => "Index";

        public static string ChangePassword => "ChangePassword";

        public static string ExternalLogins => "ExternalLogins";

        public static string PersonalData => "PersonalData";

        public static string TwoFactorAuthentication => "TwoFactorAuthentication";

        public static string ShowRecoveryCodes => "ShowRecoveryCodes";

        public static string Keys => "Keys";

        public static string BlockedTags => "BlockedTags";

        public static string IndexNavClass(ViewContext viewContext) => PageNavClass(viewContext, Index);

        public static string ChangePasswordNavClass(ViewContext viewContext) => PageNavClass(viewContext, ChangePassword);

        public static string ExternalLoginsNavClass(ViewContext viewContext) => PageNavClass(viewContext, ExternalLogins);

        public static string PersonalDataNavClass(ViewContext viewContext) => PageNavClass(viewContext, PersonalData);

        public static string TwoFactorAuthenticationNavClass(ViewContext viewContext) => PageNavClass(viewContext, TwoFactorAuthentication);
        public static string ShowRecoveryCodesNavClass(ViewContext viewContext) => PageNavClass(viewContext, ShowRecoveryCodes);

        public static string KeysNavClass(ViewContext viewContext) => PageNavClass(viewContext, Keys);

        public static string BlockedTagsNavClass(ViewContext viewContext) => PageNavClass(viewContext, BlockedTags);

        private static string PageNavClass(ViewContext viewContext, string page)
        {
            var activePage = viewContext.ViewData["ActivePage"] as string
                ?? System.IO.Path.GetFileNameWithoutExtension(viewContext.ActionDescriptor.DisplayName);
            return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
        }
    }
}