﻿using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class PersonalDataModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        public PersonalDataModel(ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.localizer = localizer;
        }

        public async Task<IActionResult> OnGet()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID_FOR_PERSONAL_DATA, userManager.GetUserId(User)].ToString());
            }

            return Page();
        }
    }
}