﻿using ManicNet.Contracts.Services;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class Disable2faModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.Disable2faModel";

        public Disable2faModel(
            ManicNetUserManager userManager,
            IStringLocalizer<IdentityLocalization> localizer,
            IManicNetLoggerService logger)
        {
            this.userManager = userManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [TempData]
        public string StatusMessage { get; set; }

        public async Task<IActionResult> OnGet()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            if (!await userManager.GetTwoFactorEnabledAsync(user))
            {
                throw new InvalidOperationException($"Cannot disable 2FA for user with ID '{userManager.GetUserId(User)}' as it's not currently enabled.");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            const string functionIdentifier = "OnPostAsync()";

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var disable2faResult = await userManager.SetTwoFactorEnabledAsync(user, false);
            if (!disable2faResult.Succeeded)
            {
                throw new InvalidOperationException($"Unexpected error occurred disabling 2FA for user with ID '{userManager.GetUserId(User)}'.");
            }

            logger.LogInformation(path, functionIdentifier, string.Format("User with ID '{0}' has disabled 2fa.", userManager.GetUserId(User)));
            StatusMessage = localizer[IdentityLocalization.TWO_FACTOR_AUTHENTICATION_HAS_BEEN_DISABLED];
            return RedirectToPage("./TwoFactorAuthentication");
        }
    }
}