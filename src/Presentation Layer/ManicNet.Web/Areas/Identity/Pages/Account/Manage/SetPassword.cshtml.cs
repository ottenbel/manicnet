﻿using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class SetPasswordModel : PageModel
    {
        private readonly UserManager<ManicNetUser> userManager;
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        public SetPasswordModel(
            UserManager<ManicNetUser> userManager,
            SignInManager<ManicNetUser> signInManager,
            IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.localizer = localizer;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public sealed class InputModel
        {
            [Required]
            [StringLength(100, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_BETWEEN, MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.NEW_PASSWORD)]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.CONFIRM_NEW_PASSWORD)]
            [Compare("NewPassword", ErrorMessage = ModelLocalization.THE_NEW_PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH)]
            public string ConfirmPassword { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var hasPassword = await userManager.HasPasswordAsync(user);

            if (hasPassword)
            {
                return RedirectToPage("./ChangePassword");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var addPasswordResult = await userManager.AddPasswordAsync(user, Input.NewPassword);
            if (!addPasswordResult.Succeeded)
            {
                foreach (var error in addPasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return Page();
            }

            await signInManager.RefreshSignInAsync(user);
            StatusMessage = localizer[IdentityLocalization.YOUR_PASSWORD_HAS_BEEN_SET];

            return RedirectToPage();
        }
    }
}
