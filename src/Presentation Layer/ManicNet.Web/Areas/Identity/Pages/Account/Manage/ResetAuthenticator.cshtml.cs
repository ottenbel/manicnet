﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class ResetAuthenticatorModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.ResetAuthenticatorModel";

        public ResetAuthenticatorModel(
            ManicNetUserManager userManager,
            SignInManager<ManicNetUser> signInManager,
            IStringLocalizer<IdentityLocalization> localizer,
            IManicNetLoggerService logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [TempData]
        public string StatusMessage { get; set; }

        public async Task<IActionResult> OnGet()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID_FOR_RESETTING_AUTHENTICATOR, userManager.GetUserId(User)].ToString());
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            const string functionIdentifier = "OnPostAsync()";

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            await userManager.SetTwoFactorEnabledAsync(user, false);
            await userManager.ResetAuthenticatorKeyAsync(user);
            logger.LogInformation(path, functionIdentifier, string.Format("User with ID '{0}' has reset their authentication app key.", user.Id));

            await signInManager.RefreshSignInAsync(user);
            StatusMessage = localizer[IdentityLocalization.YOUR_AUTHENTICATOR_APP_KEY_HAS_BEEN_RESET];

            return RedirectToPage("./EnableAuthenticator");
        }
    }
}