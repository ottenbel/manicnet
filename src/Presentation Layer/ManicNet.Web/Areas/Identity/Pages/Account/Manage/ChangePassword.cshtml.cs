﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class ChangePasswordModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.ChangePasswordModel";
        public ChangePasswordModel(
            ManicNetUserManager userManager,
            SignInManager<ManicNetUser> signInManager,
            IStringLocalizer<IdentityLocalization> localizer,
            IManicNetLoggerService logger)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        public sealed class InputModel
        {
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.CURRENT_PASSWORD)]
            public string OldPassword { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_BETWEEN, MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.NEW_PASSWORD)]
            public string NewPassword { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.CONFIRM_NEW_PASSWORD)]
            [Compare("NewPassword", ErrorMessage = ModelLocalization.THE_NEW_PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH)]
            public string ConfirmPassword { get; set; }
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var hasPassword = await userManager.HasPasswordAsync(user);
            if (!hasPassword)
            {
                return RedirectToPage("./SetPassword");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            const string functionIdentifier = "OnPostAsync()";

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            var changePasswordResult = await userManager.ChangePasswordAsync(user, Input.OldPassword, Input.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                foreach (var error in changePasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return Page();
            }

            await signInManager.RefreshSignInAsync(user);
            logger.LogInformation(path, functionIdentifier, "User changed their password successfully.");
            StatusMessage = localizer[IdentityLocalization.YOUR_PASSWORD_HAS_BEEN_CHANGED];

            return RedirectToPage();
        }
    }
}
