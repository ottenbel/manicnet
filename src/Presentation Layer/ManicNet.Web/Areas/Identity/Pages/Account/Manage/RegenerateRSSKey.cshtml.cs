using ManicNet.Constants;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account.Manage
{
    public sealed class RegenerateRSSKeyModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        public RegenerateRSSKeyModel(ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.localizer = localizer;
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound(localizer[IdentityLocalization.UNABLE_TO_LOAD_USER_WITH_ID, userManager.GetUserId(User)].ToString());
            }

            user.RSSKey = Guid.NewGuid();

            await userManager.UpdateAsync(user);

            TempData[Identifiers.KEYS] = localizer[IdentityLocalization.SUCCESSFULLY_REGENERATED_RSS_KEY].ToString();
            return LocalRedirect("~/Identity/Account/Manage/Keys");
        }
    }
}
