﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class LoginModel : PageModel
    {
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.LoginModel";

        public LoginModel(SignInManager<ManicNetUser> signInManager, ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer, IManicNetLoggerService logger)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public sealed class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.PASSWORD)]
            public string Password { get; set; }

            [Display(Name = ModelLocalization.REMEMBER_ME_QUESTION)]
            public bool RememberMe { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl ??= Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ExternalLogins = (await signInManager.GetExternalAuthenticationSchemesAsync()).ToList();

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            const string functionIdentifier = "OnPostAsync(returnUrl)";

            returnUrl ??= Url.Content("~/");

            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                ManicNetUser user = await userManager.FindByEmailAsync(Input.Email);

                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, localizer[IdentityLocalization.INVALID_LOGIN_ATTEMPT]);
                    return Page();
                }

                var result = await signInManager.PasswordSignInAsync(user.UserName, Input.Password, Input.RememberMe, lockoutOnFailure: true);
                if (result.Succeeded)
                {
                    logger.LogInformation(path, functionIdentifier, "User logged in.");
                    return LocalRedirect(returnUrl);
                }
                if (result.RequiresTwoFactor)
                {
                    return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, Input.RememberMe });
                }
                if (result.IsLockedOut)
                {
                    logger.LogWarning(path, functionIdentifier, "User account locked out.");
                    return RedirectToPage("./Lockout");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, localizer[IdentityLocalization.INVALID_LOGIN_ATTEMPT]);
                    return Page();
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
