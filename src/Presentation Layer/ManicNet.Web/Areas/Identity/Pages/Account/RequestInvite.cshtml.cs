using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    [CanRequestInviteFilter]
    public sealed class RequestInviteModel : PageModel
    {
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetEmailService emailService;
        private readonly IRegistrationInviteService registrationInviteService;
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        private readonly string path = "ManicNet.Web.Identity.RequestInviteModel";

        public RequestInviteModel(IManicNetLoggerService logger, IManicNetEmailService emailService, IRegistrationInviteService registrationInviteService, ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer)
        {
            this.logger = logger;
            this.emailService = emailService;
            this.registrationInviteService = registrationInviteService;
            this.userManager = userManager;
            this.logger = logger;
            this.localizer = localizer;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public string ReturnUrl { get; set; }

        public sealed class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }

            [StringLength(250, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_AT_MOST)]
            [Display(Name = ModelLocalization.NOTE)]
            public string Note { get; set; }
        }

        public IActionResult OnGet(string returnUrl = null)
        {
            ReturnUrl = returnUrl;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            const string functionIdentifier = "RequestInviteModel()";

            try
            {
                RegistrationInvite registrationInvite = await registrationInviteService.FindRegistrationInvite(Input.Email);

                ManicNetUser existingUser = userManager.Users.Where(u => u.Email == Input.Email).FirstOrDefault();

                if (existingUser != null)
                {
                    var user = await userManager.FindByEmailAsync(Input.Email);
                    if (user != null && await userManager.IsEmailConfirmedAsync(user))
                    {
                        TempData[Alerts.TempData.SUCCESS] = localizer[IdentityLocalization.YOUR_REGISTRATION_REQUEST_HAS_BEEN_SUCCESSFULLY_LOGGED].ToString();
                        var code = await userManager.GeneratePasswordResetTokenAsync(user);
                        var callbackUrl = Url.Page(
                            "/Account/ResetPassword",
                            pageHandler: null,
                            values: new { code },
                            protocol: Request.Scheme);

                        await emailService.SendResetPasssword(Input.Email, user.UserName, HtmlEncoder.Default.Encode(callbackUrl));
                    }
                }
                else if (registrationInvite == null)
                {
                    RegistrationInvite inviteRequest = new RegistrationInvite(Input.Email, Input.Note);
                    await registrationInviteService.CreateRegistrationInvite(inviteRequest);

                    TempData[Alerts.TempData.SUCCESS] = localizer[IdentityLocalization.YOUR_REGISTRATION_REQUEST_HAS_BEEN_SUCCESSFULLY_LOGGED].ToString();
                }
                else
                {
                    if (registrationInvite.Approved)
                    {
                        TempData[Alerts.TempData.SUCCESS] = localizer[IdentityLocalization.YOUR_REGISTRATION_REQUEST_HAS_BEEN_SUCCESSFULLY_LOGGED].ToString();

                        await emailService.SendRegistrationInvite(Input.Email, registrationInvite.RegistrationInviteID, registrationInvite.Expires);
                    }
                    else
                    {
                        TempData[Alerts.TempData.SUCCESS] = localizer[IdentityLocalization.YOUR_REGISTRATION_REQUEST_HAS_BEEN_SUCCESSFULLY_LOGGED].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create an invite request.  {{email:{0}, note:{1}}}.", Input.Email, Input.Note));
                TempData[Alerts.TempData.SUCCESS] = localizer[IdentityLocalization.YOUR_REGISTRATION_INVITE_REQUEST_FAILED].ToString();
                return Page();
            }

            return RedirectToAction(@Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = @Routing.Areas.GENERAL });
        }
    }
}
