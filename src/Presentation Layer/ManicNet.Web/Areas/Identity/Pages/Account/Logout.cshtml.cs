﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class LogoutModel : PageModel
    {
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.LogoutModel";

        public LogoutModel(SignInManager<ManicNetUser> signInManager, IManicNetLoggerService logger)
        {
            this.signInManager = signInManager;
            this.logger = logger;
        }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPost(string returnUrl = null)
        {
            const string functionIdentifier = "OnPost(returnUrl)";

            await signInManager.SignOutAsync();
            logger.LogInformation(path, functionIdentifier, "User logged out.");
            if (returnUrl != null)
            {
                return LocalRedirect(returnUrl);
            }
            else
            {
                return Page();
            }
        }
    }
}