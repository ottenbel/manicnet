﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class LoginWithRecoveryCodeModel : PageModel
    {
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.LoginWithRecoveryCodeModel";

        public LoginWithRecoveryCodeModel(SignInManager<ManicNetUser> signInManager, IStringLocalizer<IdentityLocalization> localizer, IManicNetLoggerService logger)
        {
            this.signInManager = signInManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public sealed class InputModel
        {
            [BindProperty]
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = ModelLocalization.RECOVERY_CODE)]
            public string RecoveryCode { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new InvalidOperationException($"Unable to load two-factor authentication user.");
            }

            ReturnUrl = returnUrl;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            const string functionIdentifier = "OnPostAsync(returnUrl)";

            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new InvalidOperationException($"Unable to load two-factor authentication user.");
            }

            var recoveryCode = Input.RecoveryCode.Replace(" ", string.Empty);

            var result = await signInManager.TwoFactorRecoveryCodeSignInAsync(recoveryCode);

            if (result.Succeeded)
            {
                logger.LogInformation(path, functionIdentifier, string.Format("User with ID '{0}' logged in with a recovery code.", user.Id));
                return LocalRedirect(returnUrl ?? Url.Content("~/"));
            }
            if (result.IsLockedOut)
            {
                logger.LogWarning(path, functionIdentifier, string.Format("User with ID '{0}' account locked out.", user.Id));
                return RedirectToPage("./Lockout");
            }
            else
            {
                logger.LogWarning(path, functionIdentifier, string.Format("Invalid recovery code entered for user with ID '{0}' ", user.Id));
                ModelState.AddModelError(string.Empty, localizer[IdentityLocalization.INVALID_RECOVERY_CODE_ENTERED]);
                return Page();
            }
        }
    }
}
