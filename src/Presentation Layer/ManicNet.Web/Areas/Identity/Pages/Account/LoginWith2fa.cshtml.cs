﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class LoginWith2faModel : PageModel
    {
        private readonly SignInManager<ManicNetUser> signInManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Identity.Pages.Account.LoginWith2faModel";

        public LoginWith2faModel(SignInManager<ManicNetUser> signInManager, IStringLocalizer<IdentityLocalization> localizer, IManicNetLoggerService logger)
        {
            this.signInManager = signInManager;
            this.localizer = localizer;
            this.logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        [Display(Name = ModelLocalization.REMEMBER_ME)]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }

        public sealed class InputModel
        {
            [Required]
            [StringLength(7, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_BETWEEN, MinimumLength = 6)]
            [DataType(DataType.Text)]
            [Display(Name = ModelLocalization.AUTHENTICATOR_CODE)]
            public string TwoFactorCode { get; set; }

            [Display(Name = ModelLocalization.REMEMBER_THIS_MACHINE)]
            public bool RememberMachine { get; set; }
        }

        public async Task<IActionResult> OnGetAsync(bool rememberMe, string returnUrl = null)
        {
            // Ensure the user has gone through the username & password screen first
            var user = await signInManager.GetTwoFactorAuthenticationUserAsync();

            if (user == null)
            {
                throw new InvalidOperationException($"Unable to load two-factor authentication user.");
            }

            ReturnUrl = returnUrl;
            RememberMe = rememberMe;

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(bool rememberMe, string returnUrl = null)
        {
            const string functionIdentifier = "OnPostAsync(rememberMe, returnUrl)";

            if (!ModelState.IsValid)
            {
                return Page();
            }

            returnUrl ??= Url.Content("~/");

            var user = await signInManager.GetTwoFactorAuthenticationUserAsync();
            if (user == null)
            {
                throw new InvalidOperationException($"Unable to load two-factor authentication user.");
            }

            var authenticatorCode = Input.TwoFactorCode.Replace(" ", string.Empty).Replace("-", string.Empty);

            var result = await signInManager.TwoFactorAuthenticatorSignInAsync(authenticatorCode, rememberMe, Input.RememberMachine);

            if (result.Succeeded)
            {
                logger.LogInformation(path, functionIdentifier, string.Format("User with ID '{0}' logged in with 2fa.", user.Id));
                return LocalRedirect(returnUrl);
            }
            else if (result.IsLockedOut)
            {
                logger.LogWarning(path, functionIdentifier, string.Format("User with ID '{0}' account locked out.", user.Id));
                return RedirectToPage("./Lockout");
            }
            else
            {
                logger.LogWarning(path, functionIdentifier, string.Format("Invalid authenticator code entered for user with ID '{0}'.", user.Id));
                ModelState.AddModelError(string.Empty, localizer[IdentityLocalization.INVALID_AUTHENTICATOR_CODE]);
                return Page();
            }
        }
    }
}
