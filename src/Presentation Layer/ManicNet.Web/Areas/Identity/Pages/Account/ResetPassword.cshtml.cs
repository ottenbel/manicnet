﻿using ManicNet.Localization;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Localization;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public sealed class ResetPasswordModel : PageModel
    {
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<IdentityLocalization> localizer;

        public ResetPasswordModel(ManicNetUserManager userManager, IStringLocalizer<IdentityLocalization> localizer)
        {
            this.userManager = userManager;
            this.localizer = localizer;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public sealed class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = ModelLocalization.EMAIL)]
            public string Email { get; set; }

            [Required]
            [StringLength(100, ErrorMessage = ModelLocalization.THE_STRING_MUST_BE_BETWEEN, MinimumLength = 6)]
            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.PASSWORD)]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = ModelLocalization.CONFIRM_PASSWORD)]
            [Compare("Password", ErrorMessage = ModelLocalization.THE_PASSWORD_AND_CONFIRMATION_PASSWORD_DO_NOT_MATCH)]
            public string ConfirmPassword { get; set; }

            public string Code { get; set; }
        }

        public IActionResult OnGet(string code = null)
        {
            if (code == null)
            {
                return BadRequest(localizer[IdentityLocalization.A_CODE_MUST_BE_SUPPLIED_FOR_PASSWORD_RESET]);
            }
            else
            {
                Input = new InputModel
                {
                    Code = code
                };
                return Page();
            }
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await userManager.FindByEmailAsync(Input.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToPage("./ResetPasswordConfirmation");
            }

            var result = await userManager.ResetPasswordAsync(user, Input.Code, Input.Password);
            if (result.Succeeded)
            {
                return RedirectToPage("./ResetPasswordConfirmation");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            return Page();
        }
    }
}
