﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_COLLECTION)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class CollectionController : Controller
    {
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly ICollectionUpdateService collectionUpdateService;
        private readonly ICollectionRetrievalService collectionRetrievalService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IStringLocalizer<CollectionLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.CollectionController";

        private List<string> warnings;

        public CollectionController(IManicNetLoggerService logger, IMapper mapper, ICollectionUpdateService collectionUpdateService, ICollectionRetrievalService collectionRetrievalService, IOptions<ManicNetSettings> settings, IManicNetAuthorizationService authorizationService, IStringLocalizer<CollectionLocalization> localizer)
        {
            this.logger = logger;
            this.mapper = mapper;
            this.collectionUpdateService = collectionUpdateService;
            this.collectionRetrievalService = collectionRetrievalService;
            this.settings = settings;
            this.authorizationService = authorizationService;
            this.localizer = localizer;

            this.warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [Route("create")]
        [HttpGet]
        public IActionResult Create()
        {
            CollectionModifyModel model = new CollectionModifyModel();

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(CollectionModifyModel model)
        {
            string functionIdentifier = "Create(model)";

            Collection collection = new Collection();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                collection = mapper.Map<Collection>(model, opt => { });

                await collectionUpdateService.CreateCollection(collection);

                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.COLLECTION_SUCCESSFULLY_CREATED, collection.Title].ToString();

                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id = collection.CollectionID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create collection object. {{ model:{0} }}.", model));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_CREATING_COLLECTION, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionExistenceIncludeCacheFilter(Order = 1)]
        [IsCollectionEditableFilter(Order = 2)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            string functionIdentifier = "Edit(id)";

            CollectionModifyModel model = new CollectionModifyModel();

            try
            {
                Collection collection = await GetCollectionHelperAsync(id, false);

                model = mapper.Map<CollectionModifyModel>(collection, opt => { });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show collection {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_LOAD_COLLECTION_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionExistenceBypassCacheFilter(Order = 1)]
        [IsCollectionEditableFilter(Order = 2)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(CollectionModifyModel model)
        {
            string functionIdentifier = "Edit(model)";

            Collection collection = new Collection();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                collection = await GetCollectionHelperAsync(model.CollectionID, true);

                CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection);

                mapper.Map(model, collection, opt => { });

                collection.Works.RemoveAll(w => model.Works.Where(m => m.Delete).Select(m => m.WorkID).ToList().Contains(w.WorkID));

                foreach (var work in collection.Works)
                {
                    var modelWork = model.Works.Where(w => w.WorkID == work.WorkID).FirstOrDefault();

                    if (modelWork != null && modelWork.Number != work.Number)
                    {
                        work.Number = modelWork.Number;
                    }
                }

                await collectionUpdateService.UpdateCollection(collection, collectionCacheCleanup);

                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.COLLECTION_SUCCESSFULLY_EDITED, collection.Title].ToString();
                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id = collection.CollectionID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit collection object {{ model:{0} }}.", model));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_EDITING_COLLECTION, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            string functionIdentifier = "SoftDelete(id)";

            bool bypassCache = true;

            try
            {
                Collection collection = await GetCollectionHelperAsync(id, bypassCache);

                if (!collection.IsDeleted)
                {
                    CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection);

                    collection.IsDeleted = true;

                    await collectionUpdateService.UpdateCollection(collection, collectionCacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted collection object {{ id:{0} }}", collection.Title));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.COLLECTION_SUCCESSFULLY_SOFT_DELETED, collection.Title].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_COLLECTION)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id = collection.CollectionID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Collection.Routes.INDEX, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete collection {{ id: {0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_SOFT_DELETE_COLLECTION, id].ToString();
                return RedirectToAction(Routing.Controllers.Collection.Routes.EDIT, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionExistenceBypassCacheFilter]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            string functionIdentifier = "Restore(id)";

            bool bypassCache = true;

            try
            {
                Collection collection = await GetCollectionHelperAsync(id, bypassCache);

                if (collection.IsDeleted)
                {
                    CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection);

                    collection.IsDeleted = false;

                    await collectionUpdateService.UpdateCollection(collection, collectionCacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored collection object {{ id:{0} }}", collection.Title));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.COLLECTION_SUCCESSFULLY_RESTORED, collection.Title].ToString();

                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id = collection.CollectionID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore collection {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_RESTORE_COLLECTION, id].ToString();
                return RedirectToAction(Routing.Controllers.Collection.Routes.EDIT, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_COLLECTION)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(int? pageNumber)
        {
            string functionIdentifier = "Deleted(pageNumber)";

            CollectionIndexViewModel model = new CollectionIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.Collection.Routes.DELETED;

            try
            {
                List<Collection> collections = await collectionRetrievalService.GetDeletedCollections();

                List<CollectionSummaryViewModel> collectionsView = mapper.Map<List<CollectionSummaryViewModel>>(collections, opt => { });

                model.Collections = await collectionsView.OrderByDescending(c => c.Title).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted collections. {{pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_COLLECTIONS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Owner.DELETE_COLLECTION)]
        [CheckCollectionExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            string functionIdentifier = "Delete(id)";

            bool bypassCache = true;

            try
            {
                Collection collection = await GetCollectionHelperAsync(id, bypassCache);

                await collectionUpdateService.RemoveCollection(collection);

                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.COLLECTION_SUCCESSFULLY_DELETED, collection.Title].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_COLLECTION)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Collection.Routes.DELETED, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Collection.Routes.INDEX, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (CollectionNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Collection is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[CollectionLocalization.COLLECTION_IS_NOT_DELETABLE, id].ToString());
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete collection {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_DELETE_COLLECTION, id].ToString();
                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
        }


        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionWorkEditableBasedOnParentFilter]
        [Route("addwork/{id}")]
        [HttpGet]
        public async Task<IActionResult> AddWork(Guid id)
        {
            string functionIdentifier = "AddWork(id)";

            CollectionWorkAddModel model = new CollectionWorkAddModel(id);

            try
            {
                Collection collection = await GetCollectionHelperAsync(model.CollectionID, true);

                int workNumber = 1;

                if (collection.Works.Any())
                {
                    workNumber = collection.Works.OrderByDescending(cw => cw.Number).FirstOrDefault().Number + 1;
                }

                model.Number = workNumber;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to retrieve information for  collection object {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_LOAD_COLLECTION_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_COLLECTION)]
        [CheckCollectionWorkEditableBasedOnParentFilter]
        [Route("addwork")]
        [HttpPost]
        public async Task<IActionResult> AddWork(CollectionWorkAddModel model)
        {
            string functionIdentifier = "AddWork(model)";

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Collection collection = (Collection)HttpContext.Items[FilterIdentifiers.COLLECTION_FROM_FILTER];
                if (collection == null)
                {
                    collection = await collectionRetrievalService.FindCollection(model.CollectionID, User);
                }

                if (collection.Works.Any(w => w.WorkID == model.WorkID))
                {
                    ModelState.AddModelError("WorkID", localizer[CollectionLocalization.Validation.WORK_ALREADY_EXISTS_ON_COLLECTION].ToString());
                    return View(model);
                }

                await collectionUpdateService.UpdateCollectionWithNewWork(collection, model.WorkID.Value, model.Number);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully added {0} work to collection ({1}).", model.WorkID, model.CollectionID));
                TempData[Alerts.TempData.SUCCESS] = localizer[CollectionLocalization.WORK_WAS_SUCCESSFULLY_ADDED_TO_COLLECTION].ToString();


                return RedirectToAction(Routing.Controllers.Collection.Routes.SHOW, Routing.Controllers.Collection.CONTROLLER, new { area = Routing.Areas.GENERAL, id = model.CollectionID });
            }

            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to add work to collection. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_ADDING_WORK_TO_COLLECTION, model.WorkID, model.CollectionID].ToString();
            }

            return View(model);
        }

        public async Task<Collection> GetCollectionHelperAsync(Guid id, bool bypassCache)
        {
            Collection collection = (Collection)HttpContext.Items[FilterIdentifiers.COLLECTION_FROM_FILTER];
            if (collection == null)
            {
                collection = await collectionRetrievalService.FindCollection(id, User, bypassCache);
            }

            return collection;
        }
    }
}
