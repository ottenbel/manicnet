﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_USER_PERMISSION)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class UserController : Controller
    {
        private readonly IOptions<ManicNetSettings> manicNetSettings;
        private readonly ManicNetUserManager userManager;
        private readonly IManicNetPermissionService manicNetPermissionService;
        private readonly IStringLocalizer<UserLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Administration.UserController";

        private List<string> warnings;

        public UserController(ManicNetUserManager userManager, IOptions<ManicNetSettings> manicNetSettings, IManicNetPermissionService manicNetPermissionService, IStringLocalizer<UserLocalization> localizer, IManicNetLoggerService logger)
        {
            this.manicNetSettings = manicNetSettings;
            this.userManager = userManager;
            this.manicNetPermissionService = manicNetPermissionService;
            this.localizer = localizer;
            this.logger = logger;

            warnings = new List<string>();
        }

        [Authorize(Policies.Generic.MANAGE_USER_PERMISSION)]
        [Route("")]
        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber, string search)
        {
            const string functionIdentifier = "Index(pageNumber, search)";

            UserIndexViewModel model = new UserIndexViewModel(search);
            PaginationModel paginationModel = new PaginationModel(pageNumber, manicNetSettings.Value.PageLength);

            try
            {
                var query = userManager.Users.Select
                (
                    x => new IndexUser()
                    {
                        ID = x.Id.ToString(),
                        Name = x.UserName,
                    }
                );

                if (!(string.IsNullOrWhiteSpace(search)))
                {
                    search = search.Trim();
                    query = query.Where(x => x.Name.Contains(search));
                }

                model.Users = await query.OrderBy(x => x.Name).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate administration user index. {{ pageNumber:{0}, search:{1} }}", pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[UserLocalization.UNABLE_TO_POPULATE_ADMINISTRATION_USER_INDEX].ToString();
                return View();
            }

            return View(model);
        }

        [Authorize(Policies.Generic.MANAGE_USER_PERMISSION)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            try
            {
                ManicNetUser userToBeEdited = await userManager.FindByIdAsync(id.ToString());

                if (userToBeEdited == null)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Unable to find user {{ id:{0} }}.", id));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_FIND_USER_WITH_USER_ID, id]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.User.Routes.INDEX, Routing.Controllers.User.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }

                List<PermissionSection> permissionSections = await manicNetPermissionService.BuildSection(userToBeEdited, User);
                RoleSection roleSection = await manicNetPermissionService.BuildRoleSection(userToBeEdited, User);

                UserEditViewModel model = new UserEditViewModel()
                {
                    ID = id,
                    Username = userToBeEdited.UserName,
                    Permissions = permissionSections,
                    Roles = roleSection
                };

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate administration user edit page for user. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[UserLocalization.UNABLE_TO_POPULATE_ADMINISTRATION_USER_EDIT_PAGE].ToString();
                return RedirectToAction(Routing.Controllers.User.Routes.INDEX, Routing.Controllers.User.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
        }

        [Authorize(Policies.Generic.MANAGE_USER_PERMISSION)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(UserEditViewModel model)
        {
            const string functionIdentifier = "Edit(model)";

            ManicNetUser userToBeEdited = new ManicNetUser();
            ManicNetUser editingUser = new ManicNetUser();

            try
            {
                if (!ModelState.IsValid) //Return and display the validation errors
                {
                    return View(model);
                }

                userToBeEdited = await userManager.FindByIdAsync(model.ID.ToString());
                editingUser = await userManager.GetUserAsync(User);

                if (userToBeEdited == null)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Unable to retrieve information about the user to be edited {{ id:{0} }}.", model.ID));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_RETRIEVE_INFORMATION_ABOUT_THE_USER_TO_BE_EDITED]);
                    TempData[Alerts.TempData.WARNING] = warnings;

                    return RedirectToAction(Routing.Controllers.User.Routes.INDEX, Routing.Controllers.User.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }

                if (editingUser == null)
                {
                    logger.LogWarning(path, functionIdentifier, "Unable to retrieve information about the editing user.");
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_RETRIEVE_INFORMATION_ABOUT_THE_EDITING_USER]);
                    TempData[Alerts.TempData.WARNING] = warnings;

                    return RedirectToAction(Routing.Controllers.User.Routes.INDEX, Routing.Controllers.User.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }

                UpdateClaimsHolder updateClaimsHolder = await manicNetPermissionService.CreatePermissionAdditionAndRemovalLists(model.Permissions, userToBeEdited, User);

                IdentityResult addedClaims = await userManager.AddClaimsAsync(userToBeEdited, updateClaimsHolder.ClaimsToAdd);
                IdentityResult removedClaims = await userManager.RemoveClaimsAsync(userToBeEdited, updateClaimsHolder.ClaimsToRemove);

                if (!addedClaims.Succeeded)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("User {0} ({1}) unable to add permissions to user {2} ({3})", editingUser.UserName, editingUser.Id, userToBeEdited.UserName, userToBeEdited.Id));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_ADD_PERMISSIONS_TO_USER, userToBeEdited.UserName]);
                }

                if (!removedClaims.Succeeded)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("User {0} ({1}) unable to remove permissions from user {2} ({3})", editingUser.UserName, editingUser.Id, userToBeEdited.UserName, userToBeEdited.Id));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_REMOVE_PERMISSIONS_FROM_USER, userToBeEdited.UserName]);
                }

                UpdateRolesHolder updateRolesHolder = await manicNetPermissionService.CreateRoleAdditionAndRemovalLists(model.Roles, userToBeEdited, User);

                IdentityResult addedRoles = await userManager.AddToRolesAsync(userToBeEdited, updateRolesHolder.RolesToAdd.Select(r => r.RoleName));
                IdentityResult removedRoles = await userManager.RemoveFromRolesAsync(userToBeEdited, updateRolesHolder.RolesToRemove.Select(r => r.RoleName));

                if (!addedRoles.Succeeded)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("User {0} ({1}) unable to add roles to user {2} ({3})", editingUser.UserName, editingUser.Id, userToBeEdited.UserName, userToBeEdited.Id));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_ADD_ROLES_TO_USER, userToBeEdited.UserName]);
                }

                if (!removedRoles.Succeeded)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("User {0} ({1}) unable to remove roles from user {2} ({3})", editingUser.UserName, editingUser.Id, userToBeEdited.UserName, userToBeEdited.Id));
                    warnings.Add(localizer[UserLocalization.UNABLE_TO_REMOVE_ROLES_FROM_USER, userToBeEdited.UserName]);
                }

                if (addedClaims.Succeeded && removedClaims.Succeeded && addedRoles.Succeeded && removedRoles.Succeeded)
                {
                    // TODO: Investigate way to update claims stored in user cookie without requiring the user to sign out and back in
                    // Maybe add a sign out table and then have this function generate an entry in it
                    // Have a middleware check the table on every request and force sign out any user who is in it

                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) updated permissions from user {2} ({3}) in backend claims", editingUser.UserName, editingUser.Id, userToBeEdited.UserName, userToBeEdited.Id));
                    TempData[Alerts.TempData.SUCCESS] = localizer[UserLocalization.USER_PERMISSIONS_SUCCESSFULLY_UPDATED, userToBeEdited.UserName].ToString();
                }
                else
                {
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to update permissions for user {0}", model.ID));
                TempData[Alerts.TempData.ERRROR] = localizer[UserLocalization.UNABLE_TO_UPDATE_USER_PERMISSIONS, model.ID].ToString();
            }

            return RedirectToAction(Routing.Controllers.User.Routes.EDIT, Routing.Controllers.User.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = model.ID });
        }
    }
}