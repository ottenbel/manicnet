﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [RequiresInviteFilter]
    [Authorize(Policies.Generic.MANAGE_REGISTRATION_INVITE)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class RegistrationInviteController : Controller
    {
        private readonly IRegistrationInviteService registrationInviteService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IMapper mapper;
        private readonly ManicNetUserManager userManager;
        private readonly IManicNetEmailService emailService;
        private readonly IStringLocalizer<RegistrationInviteLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Administration.RegistrationInviteController";

        private List<string> warnings;

        public RegistrationInviteController(IRegistrationInviteService registrationInviteService, IOptions<ManicNetSettings> settings, IMapper mapper, ManicNetUserManager userManager, IManicNetEmailService emailService, IStringLocalizer<RegistrationInviteLocalization> localizer, IManicNetLoggerService logger)
        {
            this.registrationInviteService = registrationInviteService;
            this.settings = settings;
            this.mapper = mapper;
            this.userManager = userManager;
            this.emailService = emailService;
            this.localizer = localizer;
            this.logger = logger;

            warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_REGISTRATION_INVITE)]
        [Route("create")]
        [HttpGet]
        public IActionResult Create()
        {
            RequestInviteCreationModel model = new RequestInviteCreationModel();

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_REGISTRATION_INVITE)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(RequestInviteCreationModel model)
        {
            const string functionIdentifier = "Create(model)";

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                ManicNetUser existingUser = userManager.Users.Where(u => u.Email == model.Email).FirstOrDefault();

                if (existingUser != null)
                {
                    warnings.Add(localizer[RegistrationInviteLocalization.A_USER_ALREADY_EXISTS_WITH_THE_EMAIL_ADDRESS, model.Email]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.CREATE, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }

                RegistrationInvite registrationInvite = await registrationInviteService.FindRegistrationInvite(model.Email);

                if (registrationInvite == null)
                {
                    registrationInvite = new RegistrationInvite(model.Email, model.Note, true)
                    {
                        Expires = DateTime.UtcNow.AddDays(settings.Value.Registration.InviteLength)
                    };

                    await registrationInviteService.CreateRegistrationInvite(registrationInvite);
                }
                else
                {
                    warnings.Add(localizer[RegistrationInviteLocalization.A_REGISTRATION_INVITE_ALREADY_EXISTS, model.Email]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.CREATE, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }

                await emailService.SendRegistrationInvite(registrationInvite.Email, registrationInvite.RegistrationInviteID, registrationInvite.Expires);

                logger.LogInformation(path, functionIdentifier, string.Format("A registration invite was created for email address {0}", model.Email));
                TempData[Alerts.TempData.SUCCESS] = localizer[RegistrationInviteLocalization.A_REGISTRATION_INVITE_WAS_SUCCESSFULLY_CREATED, model.Email].ToString();

                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.CREATE, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create registration invite. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_CREATE_REGISTRATION_INVITE].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Generic.MANAGE_REGISTRATION_INVITE)]
        [Route("pending")]
        [HttpGet]
        public async Task<IActionResult> Pending(int? pageNumber)
        {
            const string functionIdentifier = "Pending(pageNumber)";

            RegistrationInviteListViewModel model = new RegistrationInviteListViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<RegistrationInvite> invites = await registrationInviteService.GetPendingInvites();
                List<RegistrationInviteViewModel> RegistrationInvitesView = mapper.Map<List<RegistrationInviteViewModel>>(invites, opt => { });

                model.invites = await RegistrationInvitesView.OrderBy(i => i.Email).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of pending registration invites. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_RETRIEVE_LIST_OF_PENDING_REGISTRATION_INVITES].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Generic.MANAGE_REGISTRATION_INVITE)]
        [Route("approved")]
        [HttpGet]
        public async Task<IActionResult> Approved(int? pageNumber)
        {
            const string functionIdentifier = "Approved(pageNumber)";

            RegistrationInviteListViewModel model = new RegistrationInviteListViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<RegistrationInvite> invites = await registrationInviteService.GetActiveInvites();
                List<RegistrationInviteViewModel> RegistrationInvitesView = mapper.Map<List<RegistrationInviteViewModel>>(invites, opt => { });

                model.invites = await RegistrationInvitesView.OrderByDescending(i => i.Expires).ThenBy(i => i.Email).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of approved registration invites. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_RETRIEVE_LIST_OF_APPROVED_REGISTRATION_INVITES].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Generic.MANAGE_REGISTRATION_INVITE)]
        [Route("expired")]
        [HttpGet]
        public async Task<IActionResult> Expired(int? pageNumber)
        {
            const string functionIdentifier = "Expired(pageNumber)";

            RegistrationInviteListViewModel model = new RegistrationInviteListViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<RegistrationInvite> invites = await registrationInviteService.GetExpiredInvites();
                List<RegistrationInviteViewModel> RegistrationInvitesView = mapper.Map<List<RegistrationInviteViewModel>>(invites, opt => { });

                model.invites = await RegistrationInvitesView.OrderBy(i => i.Expires).ThenBy(i => i.Email).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of expired registration invites. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_RETRIEVE_LIST_OF_EXPIRED_REGISTRATION_INVITES].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_REGISTRATION_INVITE)]
        [Route("approve")]
        [HttpPost]
        public async Task<IActionResult> Approve(Guid RegistrationInviteID, string Callback)
        {
            const string functionIdentifier = "Approve(RegistrationInviteID, Callback)";

            try
            {
                RegistrationInvite registration = await registrationInviteService.FindRegistrationInvite(RegistrationInviteID);

                registration.Approved = true;
                registration.Expires = DateTime.UtcNow.AddDays(settings.Value.Registration.InviteLength);

                await registrationInviteService.UpdateRegistrationInvite(registration);

                await emailService.SendRegistrationInvite(registration.Email, registration.RegistrationInviteID, registration.Expires);

                TempData[Alerts.TempData.SUCCESS] = localizer[RegistrationInviteLocalization.REGISTRATION_INVITE_REQUEST_WAS_SUCCESSFULLY_APPROVED].ToString();
            }
            catch (RegistrationInviteNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find invite request {0} for approval", RegistrationInviteID));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_FIND_INVITE_REQUEST, RegistrationInviteID].ToString();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to approve registration invite request. {{ registrationInviteID:{0}, Callback:{1} }}", RegistrationInviteID, Callback));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_APPROVE_REGISTRATION_INVITE_REQUEST, RegistrationInviteID].ToString();
            }

            if (Callback == "Pending")
            {
                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.PENDING, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            else
            {
                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.EXPIRED, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
        }

        [Authorize(Policies.Owner.DELETE_REGISTRATION_INVITE)]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid RegistrationInviteID, string Callback)
        {
            const string functionIdentifier = "Delete(RegistrationInviteID, Callback)";

            try
            {
                RegistrationInvite registrationInvite = await registrationInviteService.FindRegistrationInvite(RegistrationInviteID);

                await registrationInviteService.RemoveRegistrationInvite(registrationInvite);

                TempData[Alerts.TempData.SUCCESS] = localizer[RegistrationInviteLocalization.REGISTRATION_INVITE_REQUEST_WAS_SUCCESSFULLY_DELETED].ToString();
            }
            catch (RegistrationInviteNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find invite request {0} for deletion", RegistrationInviteID));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_FIND_INVITE_REQUEST, RegistrationInviteID].ToString();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete registration invite request. {{ RegistrationInviteID:{0}, Callback:{1} }}", RegistrationInviteID, Callback));
                TempData[Alerts.TempData.ERRROR] = localizer[RegistrationInviteLocalization.UNABLE_TO_DELETE_REGISTRATION_INVITE_REQUEST, RegistrationInviteID].ToString();
            }

            if (Callback == "Pending")
            {
                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.PENDING, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            else if (Callback == "Approved")
            {
                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.APPROVED, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            else
            {
                return RedirectToAction(Routing.Controllers.RegistrationInvite.Routes.EXPIRED, Routing.Controllers.RegistrationInvite.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
        }
    }
}