﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_WORK)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class VolumeController : CommonWorkController
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly IVolumeUpdateService volumeUpdateService;
        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<VolumeLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.VolumeController";

        private List<string> warnings;

        public VolumeController(ITagRetrievalService tagRetrievalService, ITagTypeRetrievalService tagTypeRetrievalService,
            IManicNetLoggerService logger, IMapper mapper, IVolumeUpdateService volumeUpdateService,
            IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService,
            IManicNetAuthorizationService authorizationService, IOptions<ManicNetSettings> settings,
            IChapterRetrievalService chapterRetrievalService, IStringLocalizer<VolumeLocalization> localizer)
            : base(workRetrievalService, volumeRetrievalService, chapterRetrievalService)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.logger = logger;
            this.mapper = mapper;
            this.volumeUpdateService = volumeUpdateService;
            this.volumeRetrievalService = volumeRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.authorizationService = authorizationService;
            this.settings = settings;
            this.volumeRetrievalService = volumeRetrievalService;
            this.localizer = localizer;

            warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeCreatableBasedOnParentFilter]
        [Route("create/{id}")]
        [HttpGet]
        public async Task<IActionResult> Create(Guid id)
        {
            const string functionIdentifier = "Create(id)";

            VolumeModifyModel model = new VolumeModifyModel(id);

            try
            {
                Work work = await GetWorkHelperAsync(id, false);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.VOLUME);
                model.Tags = mapper.Map<TagTypeTagPriorityInputModel>(tagTypes, opt => { });
                model.Tags.AllowVirtual = false;

                if (work.Volumes.Any())
                {
                    model.Number = work.Volumes.OrderByDescending(v => v.Number).Select(v => v.Number).FirstOrDefault() + 1;
                }
                else
                {
                    model.Number = 1;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create volume for work. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_CREATING_VOLUME_FOR_WORK, id].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeCreatableBasedOnParentFilter]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(VolumeModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            Volume volume = new Volume();
            List<string> volumeProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Work work = await GetWorkHelperAsync(model.WorkID, false);

                volume = mapper.Map<Volume>(model, opt => { });

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.VOLUME, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.VOLUME, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primary.Included.Select(p => p.TagID).Concat(secondary.Included.Select(s => s.TagID)));

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    volumeProblemList.Add(localizer[VolumeLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_VOLUME, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                string message = await volumeUpdateService.CreateVolume(volume, primary.Included, secondary.Included, model.Cover, cacheCleanup);

                if (!string.IsNullOrWhiteSpace(message))
                {
                    volumeProblemList.Add(message);
                }

                if (volumeProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Created volume ({0}), but problems occurred.", volume.Title));
                    warnings.Add(localizer[VolumeLocalization.CREATED_VOLUME_WITH_PROBLEMS]);
                    warnings = warnings.Concat(volumeProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully created volume ({0}).", volume.Title));
                    TempData[Alerts.TempData.SUCCESS] = string.IsNullOrWhiteSpace(volume.Title) ? localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_CREATED, volume.Number].ToString() : localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_CREATED_WITH_TITLE, volume.Number, volume.Title].ToString();
                }

                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = volume.VolumeID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create volume. {{ model:{0} }}.", model));
                TempData[Alerts.TempData.ERRROR] = string.IsNullOrWhiteSpace(model.Title) ? localizer[VolumeLocalization.ERROR_CREATING_VOLUME, model.Number].ToString() : localizer[VolumeLocalization.ERROR_CREATING_VOLUME_WITH_TITLE, model.Number, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceIncludeCacheFilter(Order = 1)]
        [CheckVolumeEditableBasedOnParentFilter(Order = 2)]
        [IsVolumeEditableFilter(Order = 3)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            VolumeModifyModel model = new VolumeModifyModel();

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, false);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.VOLUME);
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<VolumeModifyModel>(volume, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(model);

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show volume. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_LOAD_VOLUME_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceBypassCacheFilter(Order = 1)]
        [CheckVolumeEditableBasedOnParentFilter(Order = 2)]
        [IsVolumeEditableFilter(Order = 3)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(VolumeModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";

            Volume volume = new Volume();
            bool bypassCache = true;
            List<string> volumeProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                volume = await GetVolumeHelperAsync(model.VolumeID, bypassCache);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                mapper.Map(model, volume);

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.VOLUME, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.VOLUME, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primary.Included.Select(p => p.TagID).Concat(secondary.Included.Select(s => s.TagID)));

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    volumeProblemList.Add(localizer[VolumeLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_VOLUME, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                string message = await volumeUpdateService.UpdateVolume(volume, primary.Included, secondary.Included, model.Cover, model.DeleteCover, cacheCleanup);

                if (!string.IsNullOrWhiteSpace(message))
                {
                    volumeProblemList.Add(message);
                }

                if (volumeProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Edited volume number ({0}), but problems occurred.", volume.Number));
                    warnings.Add(localizer[VolumeLocalization.EDITED_VOLUME_WITH_PROBLEMS]);
                    warnings = warnings.Concat(volumeProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully edited volume number ({0}).", volume.Number));
                    TempData[Alerts.TempData.SUCCESS] = localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_EDITED, volume.Number].ToString();
                }

                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = volume.VolumeID });

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit volume object. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_EDITING_VOLUME, model.Number].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(id)";
            const bool bypassCache = true;

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, bypassCache);

                if (!volume.IsDeleted)
                {
                    Work work = await GetWorkHelperAsync(volume.WorkID, false);
                    WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                    volume.IsDeleted = true;

                    await volumeUpdateService.UpdateVolume(volume, cacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted volume {{ id:{0} }}", volume.VolumeID));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_SOFT_DELETED, volume.Number].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = volume.VolumeID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = volume.WorkID });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete volume. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_SOFT_DELETE_VOLUME, id].ToString();
                return RedirectToAction(Routing.Controllers.Volume.Routes.EDIT, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Owner.DELETE_WORK)]
        [CheckVolumeExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";
            const bool bypassCache = true;

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, !bypassCache);
                Volume volumeToDelete = await volumeRetrievalService.FindVolumeForDeletion(id, User, !bypassCache);

                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                await volumeUpdateService.RemoveVolume(volumeToDelete, cacheCleanup);

                TempData[Alerts.TempData.SUCCESS] = localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_DELETED, volume.Number].ToString();

                if (!volume.Work.IsDeleted || (volume.Work.IsDeleted && (await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = volume.WorkID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (VolumeNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Volume is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[VolumeLocalization.VOLUME_IS_NOT_IN_DELETABLE_STATE, id]);
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.Volume.Routes.EDIT, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete volume. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_DELETE_VOLUME, id].ToString();
                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceBypassCacheFilter(Order = 1)]
        [CheckVolumeRestorableBasedOnParentFilter(Order = 2)]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(id)";
            bool bypassCache = true;

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, bypassCache);

                if (volume.IsDeleted)
                {
                    Work work = await GetWorkHelperAsync(volume.WorkID, false);
                    WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                    volume.IsDeleted = false;

                    await volumeUpdateService.UpdateVolume(volume, cacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored volume object. {{ id: {0} }}", volume.VolumeID));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_RESTORED, volume.Number].ToString();

                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore volume. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_RESTORE_VOLUME, id].ToString();
            }

            return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_WORK)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(Guid? workID, int? pageNumber, string search)
        {
            const string functionIdentifier = "Deleted(workID, pageNumber, search)";

            VolumeIndexViewModel model = new VolumeIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.Volume.Routes.DELETED;
            model.WorkID = workID;

            try
            {
                if (!workID.HasValue || workID == Guid.Empty)
                {
                    workID = null;
                }
                else
                {
                    try
                    {
                        Work work = await workRetrievalService.FindWorkAsync(workID.Value, User);
                        model.WorkTitle = work.Title;
                    }
                    catch (Exception) //Default to listing all deleted volumes if the work ID is bad
                    {
                        workID = null;
                    }
                }

                List<Volume> volumes = await workRetrievalService.GetDeletedVolumes(workID, User.GetUserID());
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                List<VolumeSummaryViewModel> worksView = mapper.Map<List<VolumeSummaryViewModel>>(volumes, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                if (workID.HasValue)
                {
                    model.Volumes = await worksView.OrderBy(v => v.Number).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                }
                else
                {
                    model.Volumes = await worksView.OrderByDescending(v => v.UpdatedDate).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                }

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted volumes. {{ workID:{0}, pageNumber:{1}, search:{2} }}", workID, pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_VOLUMES].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceIncludeCacheFilter(Order = 1)]
        [CheckVolumeEditableBasedOnParentFilter(Order = 2)]
        [IsVolumeEditableFilter(Order = 3)]
        [Route("migrate/{id}")]
        [HttpGet]
        public async Task<IActionResult> Migrate(Guid id)
        {
            const string functionIdentifier = "Migrate(id)";

            VolumeMigrationModel model = new VolumeMigrationModel();

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                model = mapper.Map<VolumeMigrationModel>(volume, opt =>
                {
                    opt.Items[ModelMapping.WORK] = work;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error loading volume information for volume. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_LOAD_VOLUME_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckVolumeExistenceBypassCacheFilter(Order = 1)]
        [IsVolumeEditableFilter(Order = 2)]
        [Route("migrate")]
        [HttpPost]
        public async Task<IActionResult> Migrate(VolumeMigrationModel migrationModel)
        {
            const string functionIdentifier = "Migrate(migrationModel)";

            try
            {
                if (!ModelState.IsValid)
                {
                    //Ensure that the dropdown is populated on error
                    return View(migrationModel);
                }

                Volume volume = await GetVolumeHelperAsync(migrationModel.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                Work destinationWork = await workRetrievalService.FindWorkAsync(migrationModel.WorkID.Value, User);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);
                cacheCleanup.Add(new WorkCacheCleanup(destinationWork));

                if (destinationWork.IsDeleted)
                {
                    warnings.Add(localizer[VolumeLocalization.UNABLE_TO_MIGRATE_VOLUME_DESTINATION_WORK_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO, migrationModel.VolumeID, destinationWork.Title]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.VolumeID });
                }

                volume.WorkID = destinationWork.WorkID;
                volume.Number = migrationModel.Number;

                await volumeUpdateService.MigrateVolume(volume, cacheCleanup);

                TempData[Alerts.TempData.SUCCESS] = localizer[VolumeLocalization.VOLUME_SUCCESSFULLY_MIGRATED, migrationModel.Number, destinationWork.Title].ToString();
                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.VolumeID });
            }
            catch (Exception ex) when (ex is WorkNotFoundException || ex is WorkNotViewableException)
            {
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_MIGRATE_VOLUME_DESTINATION_WORK_DOES_NOT_EXIST, migrationModel.VolumeID, migrationModel.WorkID].ToString();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to migrate volume. {{ migrationModel:{0} }}", migrationModel.VolumeID));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_MIGRATE_VOLUME, migrationModel.VolumeID].ToString();
            }

            return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.VolumeID });
        }
    }
}