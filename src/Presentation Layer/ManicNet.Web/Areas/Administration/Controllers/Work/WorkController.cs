﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_WORK)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class WorkController : CommonWorkController
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly IWorkUpdateService workUpdateService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IStringLocalizer<WorkLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.WorkController";

        private List<string> warnings;

        public WorkController(ITagRetrievalService tagRetrievalService, IManicNetLoggerService logger, IMapper mapper,
            IWorkUpdateService workUpdateService, IWorkRetrievalService workRetrievalService,
            ITagTypeRetrievalService tagTypeRetrievalService, IOptions<ManicNetSettings> settings,
            IManicNetAuthorizationService authorizationService, IVolumeRetrievalService volumeRetrievalService,
            IChapterRetrievalService chapterRetrievalService, IStringLocalizer<WorkLocalization> localizer)
            : base(workRetrievalService, volumeRetrievalService, chapterRetrievalService)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.logger = logger;
            this.mapper = mapper;
            this.workUpdateService = workUpdateService;
            this.workRetrievalService = workRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.settings = settings;
            this.authorizationService = authorizationService;
            this.localizer = localizer;
            this.warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [Route("create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            WorkModifyModel model = new WorkModifyModel();
            var tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.WORK);

            model.Tags = mapper.Map<TagTypeTagPriorityInputModel>(tagTypes, opt => { });
            model.Tags.AllowVirtual = false;

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(WorkModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            Work work = new Work();
            List<string> workProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                work = mapper.Map<Work>(model, opt => { });

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.WORK, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.WORK, false);

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    workProblemList.Add(localizer[WorkLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_WORK, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                string message = await workUpdateService.CreateWork(work, primary.Included, secondary.Included, model.Cover);

                if (!string.IsNullOrWhiteSpace(message))
                {
                    workProblemList.Add(message);
                }

                if (workProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Created work object ({0}), but problems occurred.", work.Title));
                    warnings.Add(localizer[WorkLocalization.CREATED_WORK_WITH_PROBLEMS, work.Title]);
                    warnings = warnings.Concat(workProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully created work object ({0}).", work.Title));
                    TempData[Alerts.TempData.SUCCESS] = localizer[WorkLocalization.WORK_SUCCESSFULLY_CREATED, work.Title].ToString();
                }

                return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = work.WorkID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create work. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.ERROR_CREATING_WORK, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckWorkExistenceIncludeCacheFilter(Order = 1)]
        [IsWorkEditableFilter(Order = 2)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";
            bool bypassCache = false;

            WorkModifyModel model = new WorkModifyModel();

            try
            {
                Work work = await GetWorkHelperAsync(model.WorkID, bypassCache);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.WORK);
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<WorkModifyModel>(work, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(model);

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show work. {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_WORK].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckWorkExistenceBypassCacheFilter(Order = 1)]
        [IsWorkEditableFilter(Order = 2)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(WorkModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";
            const bool bypassCache = true;

            Work work = new Work();
            List<string> workProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                work = await GetWorkHelperAsync(model.WorkID, bypassCache);

                mapper.Map(model, work);

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.WORK, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.WORK, false);

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    workProblemList.Add(localizer[WorkLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_WORK, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                string message = await workUpdateService.UpdateWork(work, primary.Included, secondary.Included, model.Cover, model.DeleteCover);

                if (!string.IsNullOrWhiteSpace(message))
                {
                    workProblemList.Add(message);
                }

                if (workProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Edited work object ({0}), but problems occurred.", work.Title));
                    warnings.Add(localizer[WorkLocalization.EDITED_WORK_WITH_PROBLEMS, work.Title]);
                    warnings = warnings.Concat(workProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully edited work object ({0}).", work.Title));
                    TempData[Alerts.TempData.SUCCESS] = localizer[WorkLocalization.WORK_SUCCESSFULLY_EDITED, work.Title].ToString();
                }

                return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = work.WorkID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit work. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.ERROR_EDITING_WORK, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckWorkExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(model)";
            const bool bypassCache = true;

            try
            {
                Work work = await GetWorkHelperAsync(id, bypassCache);

                if (!work.IsDeleted)
                {
                    work.IsDeleted = true;

                    await workUpdateService.UpdateWork(work);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted work {{ id:{0} }}", id));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[WorkLocalization.WORK_SUCCESSFULLY_SOFT_DELETED, work.Title].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = work.WorkID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete work. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_SOFT_DELETE_WORK, id].ToString();
                return RedirectToAction(Routing.Controllers.Work.Routes.EDIT, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_WORK)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(int? pageNumber, string search)
        {
            const string functionIdentifier = "Deleted(pageNumber, search)";

            WorkIndexViewModel model = new WorkIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.Work.Routes.DELETED;

            try
            {
                List<Work> works = await workRetrievalService.GetDeletedWorks(User.GetUserID());
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                List<WorkSummaryViewModel> worksView = mapper.Map<List<WorkSummaryViewModel>>(works, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                model.Works = await worksView.OrderByDescending(w => w.SortabledDate).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted works. {{ pageNumber:{0}, search:{1}", pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_WORKS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckWorkExistenceBypassCacheFilter]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(model)";
            const bool bypassCache = true;

            try
            {
                Work work = await GetWorkHelperAsync(id, bypassCache);

                if (work.IsDeleted)
                {
                    work.IsDeleted = false;

                    await workUpdateService.UpdateWork(work);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored work object ({0}).", work.Title));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[WorkLocalization.WORK_WAS_SUCCESSFULLY_RESTORED, work.Title].ToString();

                return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = work.WorkID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore work. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_RESTORE_WORK, id].ToString();
                return RedirectToAction(Routing.Controllers.Work.Routes.EDIT, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Owner.DELETE_WORK)]
        [CheckWorkExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";
            const bool bypassCache = false;

            try
            {
                Work work = await GetWorkHelperAsync(id, bypassCache);
                Work workToDelete = await workRetrievalService.FindWorkForDeletion(id, User, bypassCache);

                await workUpdateService.RemoveWork(workToDelete);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully deleted work object ({0}).", work.Title));

                TempData[Alerts.TempData.SUCCESS] = localizer[WorkLocalization.WORK_WAS_SUCCESSFULLY_DELETED, work.Title].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.DELETED, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (WorkNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Work is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[WorkLocalization.WORK_IS_NOT_IN_DELETABLE_STATE, id]);
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete work. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_DELETE_WORK, id].ToString();
                return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
        }
    }
}