﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_WORK)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class ChapterController : CommonWorkController
    {
        private readonly IChapterUpdateService chapterUpdateService;
        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<ChapterLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.ChapterController";

        private List<string> warnings;

        public ChapterController(IChapterRetrievalService chapterRetrievalService, IChapterUpdateService chapterUpdateService,
            IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger,
            IMapper mapper, ITagRetrievalService tagRetrievalService, ITagTypeRetrievalService tagTypeRetrievalService,
            IManicNetAuthorizationService authorizationService, IOptions<ManicNetSettings> settings, IStringLocalizer<ChapterLocalization> localizer)
            : base(workRetrievalService, volumeRetrievalService, chapterRetrievalService)
        {
            this.chapterUpdateService = chapterUpdateService;
            this.volumeRetrievalService = volumeRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.logger = logger;
            this.mapper = mapper;
            this.tagRetrievalService = tagRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.authorizationService = authorizationService;
            this.settings = settings;
            this.localizer = localizer;

            warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterCreatableBasedOnParentFilter(Order = 1)]
        [Route("create/{id}")]
        [HttpGet]
        public async Task<IActionResult> Create(Guid id)
        {
            const string functionIdentifier = "Create(id)";

            ChapterModifyModel model = new ChapterModifyModel(id);

            try
            {
                Volume volume = await GetVolumeHelperAsync(id, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.CHAPTER);
                model.Tags = mapper.Map<TagTypeTagPriorityInputModel>(tagTypes, opt => { });
                model.Tags.AllowVirtual = false;

                if (work.Volumes.SelectMany(c => c.Chapters).Any())
                {
                    model.Number = work.Volumes.SelectMany(c => c.Chapters).OrderByDescending(c => c.Number).Select(c => c.Number).FirstOrDefault() + 1;
                }
                else
                {
                    model.Number = 1;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create chapter object for id. {{ id:{0} }}", model.VolumeID));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_CREATING_CHAPTER, id].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterCreatableBasedOnParentFilter(Order = 1)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(ChapterModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            Chapter chapter = new Chapter();
            List<string> chapterProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                Volume volume = await GetVolumeHelperAsync(model.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                chapter = mapper.Map<Chapter>(model, opt => { });

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.CHAPTER, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.CHAPTER, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primary.Included.Select(p => p.TagID).Concat(secondary.Included.Select(s => s.TagID)));

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    chapterProblemList.Add(localizer[ChapterLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_CHAPTER, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                List<string> messages = await chapterUpdateService.CreateChapter(chapter, primary.Included, secondary.Included, model.Files, work.WorkID, cacheCleanup);

                if (messages.Any(m => !string.IsNullOrWhiteSpace(m)))
                {
                    chapterProblemList = chapterProblemList.Concat(messages).ToList();
                }

                if (chapterProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Created chapter object ({0}), but problems occurred.", chapter.Title));
                    warnings.Add(localizer[ChapterLocalization.CREATED_CHAPTER_WITH_PROBLEMS]);
                    warnings = warnings.Concat(chapterProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully created chapter object ({0}).", chapter.Title));
                    TempData[Alerts.TempData.SUCCESS] = string.IsNullOrWhiteSpace(chapter.Title) ? localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_CREATED, chapter.Number].ToString() : localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_CREATED_WITH_TITLE, chapter.Number, chapter.Title].ToString();
                }

                return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.ChapterID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create chapter. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_CREATING_CHAPTER, model.VolumeID].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceIncludeCacheFilter(Order = 1)]
        [CheckChapterEditableBasedOnParentFilter(Order = 2)]
        [IsChapterEditableFilter(Order = 3)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            ChapterModifyModel model = new ChapterModifyModel();

            try
            {
                Chapter chapter = await GetChapterHelperAsync(id, false);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypesForWorkType(WorkTypes.CHAPTER);
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<ChapterModifyModel>(chapter, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(model);

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show chapter {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_CHAPTER].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceBypassCacheFilter(Order = 1)]
        [CheckChapterEditableBasedOnParentFilter(Order = 2)]
        [IsChapterEditableFilter(Order = 3)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(ChapterModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";

            Chapter chapter = new Chapter();
            bool bypassCache = true;
            List<string> chapterProblemList = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                chapter = await GetChapterHelperAsync(model.ChapterID, bypassCache);
                Volume volume = await GetVolumeHelperAsync(model.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                mapper.Map(model, chapter);

                string primaryTags = string.Join(", ", model.Tags.PrimaryTags.Select(t => t.Tags));
                string secondaryTags = string.Join(", ", model.Tags.SecondaryTags.Select(t => t.Tags));
                TagSearchLists primaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(primaryTags);
                TagSearchLists secondaryTagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(secondaryTags);
                TagSearchLists primary = tagRetrievalService.FilterTagsByWorkType(primaryTagSearchLists.Included, WorkTypes.CHAPTER, false);
                TagSearchLists secondary = tagRetrievalService.FilterTagsByWorkType(secondaryTagSearchLists.Included, WorkTypes.CHAPTER, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primary.Included.Select(p => p.TagID).Concat(secondary.Included.Select(s => s.TagID)));

                var workExcluded = primary.Excluded.Concat(secondary.Excluded).Distinct();

                foreach (Tag excluded in workExcluded)
                {
                    chapterProblemList.Add(localizer[ChapterLocalization.TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_CHAPTER, HttpUtility.HtmlEncode(excluded.Name)]);
                }

                List<string> messages = await chapterUpdateService.UpdateChapter(chapter, primary.Included, secondary.Included, work.WorkID, model, cacheCleanup);

                if (messages.Any(m => !string.IsNullOrWhiteSpace(m)))
                {
                    chapterProblemList = chapterProblemList.Concat(messages).ToList();
                }

                if (chapterProblemList.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Updated chapter object ({0}), but problems occurred.", chapter.Title));
                    warnings.Add(localizer[ChapterLocalization.UPDATED_CHAPTER_WITH_PROBLEMS]);
                    warnings = warnings.Concat(chapterProblemList).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully updated chapter object ({0}).", chapter.Title));
                    TempData[Alerts.TempData.SUCCESS] = string.IsNullOrWhiteSpace(chapter.Title) ? localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_UPDATED, chapter.Number].ToString() : localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_UPDATED_WITH_TITLE, chapter.Number, chapter.Title].ToString();
                }

                return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.ChapterID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit chapter. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = string.IsNullOrWhiteSpace(chapter.Title) ? localizer[ChapterLocalization.ERROR_EDITING_CHAPTER, model.Number].ToString() : localizer[ChapterLocalization.ERROR_EDITING_CHAPTER, model.Number, model.Title].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(id)";
            bool bypassCache = true;

            try
            {
                Chapter chapter = await GetChapterHelperAsync(id, bypassCache);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);

                if (!chapter.IsDeleted)
                {
                    Work work = await GetWorkHelperAsync(volume.WorkID, false);
                    WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                    chapter.IsDeleted = true;

                    await chapterUpdateService.UpdateChapter(chapter, cacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted chapter. {{ id:{0} }}.", chapter.ChapterID));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_SOFT_DELETED, chapter.Number].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.ChapterID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.VolumeID });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete chapter. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_SOFT_DELETE_CHAPTER, id].ToString();
                return RedirectToAction(Routing.Controllers.Chapter.Routes.EDIT, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Owner.DELETE_WORK)]
        [CheckChapterExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";
            bool bypassCache = true;

            try
            {
                Chapter chapter = await GetChapterHelperAsync(id, bypassCache);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                await chapterUpdateService.RemoveChapter(chapter, cacheCleanup);

                TempData[Alerts.TempData.SUCCESS] = localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_DELETED, chapter.Number].ToString();

                if (!chapter.Volume.IsDeleted || (chapter.Volume.IsDeleted && (await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.VolumeID });
                }
                else if (!chapter.Volume.Work.IsDeleted || (chapter.Volume.Work.IsDeleted && (await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = chapter.Volume.WorkID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (ChapterNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Chapter is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[ChapterLocalization.CHAPTER_IS_NOT_IN_DELETABLE_STATE, id]);
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.Chapter.Routes.EDIT, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete chapter. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_DELETE_CHAPTER, id].ToString();
                return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceBypassCacheFilter(Order = 1)]
        [CheckChapterRestorableBasedOnParentFilter(Order = 2)]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(id)";

            bool bypassCache = true;

            try
            {
                Chapter chapter = await GetChapterHelperAsync(id, bypassCache);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);

                if (chapter.IsDeleted)
                {
                    Work work = await GetWorkHelperAsync(volume.WorkID, false);
                    WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                    chapter.IsDeleted = false;

                    await chapterUpdateService.UpdateChapter(chapter, cacheCleanup);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored chapter. {{ id:{0} }}", chapter.ChapterID));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_RESTORED, chapter.Number].ToString();

                return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore chapter. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_RESTORE_CHAPTER, id].ToString();
            }

            return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id });
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_WORK)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(Guid? volumeID, int? pageNumber, string search)
        {
            const string functionIdentifier = "Deleted(volumeID, pageNumber, search)";

            ChapterDeletedIndexViewModel model = new ChapterDeletedIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.Chapter.Routes.DELETED;
            model.VolumeID = volumeID;

            try
            {
                if (!volumeID.HasValue || volumeID == Guid.Empty)
                {
                    volumeID = null;
                }
                else
                {
                    try
                    {
                        Volume volume = await volumeRetrievalService.FindVolume(volumeID.Value, User);
                        model.VolumeTitle = volume.Title;
                        model.VolumeNumber = volume.Number;
                        model.WorkID = volume.WorkID;
                        model.WorkTitle = volume.Work.Title;
                    }
                    catch (Exception) //Default to listing all deleted chapters if the volume ID is bad
                    {
                        volumeID = null;
                    }
                }

                List<Chapter> chapters = await workRetrievalService.GetDeletedChapters(volumeID, User.GetUserID());
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                List<ChapterDeletedSummaryViewModel> chaptersView = mapper.Map<List<ChapterDeletedSummaryViewModel>>(chapters, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                if (volumeID.HasValue)
                {
                    model.Chapters = await chaptersView.OrderBy(c => c.Number).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                }
                else
                {
                    model.Chapters = await chaptersView.OrderByDescending(v => v.UpdatedDate).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                }

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted chapters. {{ volumeID:{0}, pageNumber:{1}, search:{2}", volumeID, pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_CHAPTERS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceIncludeCacheFilter(Order = 1)]
        [CheckChapterEditableBasedOnParentFilter(Order = 2)]
        [IsChapterEditableFilter(Order = 3)]
        [Route("migrate/{id}")]
        [HttpGet]
        public async Task<IActionResult> Migrate(Guid id)
        {
            const string functionIdentifier = "Migrate(id)";

            ChapterMigrationModel model = new ChapterMigrationModel();

            try
            {
                Chapter chapter = await GetChapterHelperAsync(id, false);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                model = mapper.Map<ChapterMigrationModel>(chapter, opt =>
                {
                    opt.Items[ModelMapping.WORK] = work;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error loading chapter information for chapter. {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_CHAPTER].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_WORK)]
        [CheckChapterExistenceBypassCacheFilter(Order = 1)]
        [IsChapterEditableFilter(Order = 2)]
        [Route("migrate")]
        [HttpPost]
        public async Task<IActionResult> Migrate(ChapterMigrationModel migrationModel)
        {
            const string functionIdentifier = "Migrate(migrationModel)";

            try
            {
                Chapter chapter = await GetChapterHelperAsync(migrationModel.ChapterID, false);
                Volume volume = await GetVolumeHelperAsync(chapter.VolumeID, false);
                Work work = await GetWorkHelperAsync(volume.WorkID, false);

                if (!ModelState.IsValid)
                {
                    //Ensure that the dropdown is populated on error
                    migrationModel.CollectionVolumes = work.Volumes.Where(v => v.VolumeID != volume.VolumeID && !v.IsDeleted).Select(v => new SelectListItem { Text = string.IsNullOrWhiteSpace(v.Title) ? string.Format("Volume {0}", v.Number) : string.Format("Volume {0} - {1}", v.Number, v.Title), Value = v.VolumeID.ToString() }).ToList();
                    return View(migrationModel);
                }

                Volume destinationVolume = await volumeRetrievalService.FindVolume(migrationModel.VolumeID.Value, User);
                Volume sourceVolume = await volumeRetrievalService.FindVolume(chapter.VolumeID, User);

                Work destinationWork = await GetWorkHelperAsync(volume.WorkID, false);

                if (destinationVolume.IsDeleted)
                {
                    warnings.Add(localizer[ChapterLocalization.UNABLE_TO_MIGRATE_CHAPTER_DESTINATION_VOLUME_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO, destinationVolume.VolumeID]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.ChapterID });
                }

                Guid sourceVolumeId = chapter.VolumeID;
                chapter.VolumeID = destinationVolume.VolumeID;
                chapter.Number = migrationModel.Number;

                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);
                cacheCleanup.Add(new WorkCacheCleanup(destinationWork));

                await chapterUpdateService.MigrateChapter(chapter, cacheCleanup);

                TempData[Alerts.TempData.SUCCESS] = localizer[ChapterLocalization.CHAPTER_SUCCESSFULLY_MIGRATED, migrationModel.Name].ToString();
                return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.ChapterID });
            }
            catch (Exception ex) when (ex is VolumeNotFoundException || ex is VolumeNotViewableException || ex is VolumeUnableToViewParentWorkException)
            {
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_MIGRATE_CHAPTER_DESTINATION_VOLUME_DOES_NOT_EXIST, migrationModel.VolumeID].ToString();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to migrate chapter. {{ migrationModel:{0} }}", migrationModel));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_MIGRATE_CHAPTER, migrationModel.ChapterID].ToString();
            }

            return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.ChapterID });
        }
    }
}