﻿using ManicNet.Constants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.ELEVATED_SITE_PERMISSION)]
    [Area("Administration")]
    public sealed class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_USER_PERMISSION)]
        public IActionResult Users()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_TAG_GENERIC)]
        public IActionResult Tags()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_WORK)]
        public IActionResult Works()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_COLLECTION)]
        public IActionResult Collections()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_CONFIGURATION_GENERIC)]
        public IActionResult Configurations()
        {
            return View();
        }

        [Authorize(Policies.Generic.MANAGE_SOCIAL_LINK)]
        public IActionResult SocialLinks()
        {
            return View();
        }
    }
}