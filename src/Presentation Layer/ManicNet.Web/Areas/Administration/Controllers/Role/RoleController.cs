﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers.Role
{
    [Authorize(Policies.Generic.MANAGE_ROLE_PERMISSION)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class RoleController : Controller
    {

        private readonly RoleManager<ManicNetRole> roleManager;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetPermissionService manicNetPermissionService;
        private readonly IMapper mapper;
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<RoleLocalization> localizer;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Web.Administration.RoleController";

        private List<string> warnings;

        public RoleController(RoleManager<ManicNetRole> roleManager, IOptions<ManicNetSettings> settings, IManicNetPermissionService manicNetPermissionService, IMapper mapper, ManicNetUserManager userManager, IStringLocalizer<RoleLocalization> localizer, IManicNetLoggerService logger)
        {
            this.roleManager = roleManager;
            this.settings = settings;
            this.manicNetPermissionService = manicNetPermissionService;
            this.mapper = mapper;
            this.userManager = userManager;
            this.localizer = localizer;
            this.logger = logger;

            this.warnings = new List<string>();
        }

        [Authorize(Policies.Generic.MANAGE_ROLE_PERMISSION)]
        [Route("")]
        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber)
        {
            const string functionIdentifier = "Index(pageNumber)";

            RoleIndexViewModel model = new RoleIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                var query = roleManager.Roles.Select
                (
                    x => new IndexRole()
                    {
                        ID = x.Id.ToString(),
                        Name = x.Name,
                        IsDefaultUser = x.IsDefaultUser
                    }
                );


                model.Roles = await query.OrderBy(x => x.Name).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate administration role index. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_RETRIEVE_ROLES].ToString();
                return View();
            }

            return View(model);
        }

        [Authorize(Policies.Generic.MANAGE_ROLE_PERMISSION)]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            RoleViewModel model = new RoleViewModel();

            try
            {
                var role = await roleManager.FindByIdAsync(id.ToString());
                model = mapper.Map<RoleViewModel>(role, opt => { });

                model.Permissions = await manicNetPermissionService.BuildSection(role, User);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate role screen."));
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_RETRIEVE_ROLE_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Owner.EDIT_ROLE)]
        [Route("create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            const string functionIdentifier = "Create()";

            try
            {
                RoleEditModel model = new RoleEditModel();

                ManicNetRole role = new ManicNetRole();
                List<PermissionSection> permissionSections = await manicNetPermissionService.BuildSection(role, User);

                model.Permissions = permissionSections;

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, "Unable to populate create role screen.");
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_POPULATE_CREATE_ROLE_SCREEN].ToString();
                return RedirectToAction(Routing.Controllers.Role.Routes.INDEX, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });

            }
        }

        [Authorize(Policies.Owner.EDIT_ROLE)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(RoleEditModel model)
        {
            const string functionIdentifier = "Create(model)";

            try
            {
                if (!ModelState.IsValid) //Return and display the validation errors
                {
                    return View(model);
                }

                ManicNetRole role = new ManicNetRole() { Name = model.Name, Description = model.Description };

                await roleManager.CreateAsync(role);

                UpdateClaimsHolder updateClaimsHolder = await manicNetPermissionService.CreatePermissionAdditionAndRemovalLists(model.Permissions, role, User);

                bool issueWithUpdate = false;
                foreach (Claim claim in updateClaimsHolder.ClaimsToAdd)
                {
                    IdentityResult addedClaim = await roleManager.AddClaimAsync(role, claim);
                    if (!addedClaim.Succeeded)
                    {
                        logger.LogWarning(path, functionIdentifier, string.Format("Unable to add permission {0} to role ({1})", claim.Value, role.Name));
                        warnings.Add(localizer[RoleLocalization.UNABLE_TO_ADD_PERMISSION_TO_ROLE, claim.Value, role.Name]);
                        issueWithUpdate = true;
                    }
                }

                if (!issueWithUpdate)
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Created role {0}", role.Name));
                    TempData[Alerts.TempData.SUCCESS] = localizer[RoleLocalization.CREATED_ROLE, role.Name].ToString();
                }
                else
                {
                    TempData[Alerts.TempData.WARNING] = warnings;
                }

                return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = role.Id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to successfully create role. {{ model:{0} }}", model.Name));
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_CREATE_ROLE, model.Name];
            }

            return View(model);
        }

        [Authorize(Policies.Owner.EDIT_ROLE)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            try
            {
                RoleEditModel model = new RoleEditModel();

                ManicNetRole role = await roleManager.FindByIdAsync(id.ToString());
                List<PermissionSection> permissionSections = await manicNetPermissionService.BuildSection(role, User);

                model.ID = role.Id;
                model.Name = role.Name;
                model.Description = role.Description;
                model.IsDefaultUser = role.IsDefaultUser;
                model.Permissions = permissionSections;

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate edit role screen. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_POPULATE_EDIT_ROLE_SCREEN].ToString();
                return RedirectToAction(Routing.Controllers.Role.Routes.INDEX, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
        }

        [Authorize(Policies.Owner.EDIT_ROLE)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(RoleEditModel model)
        {
            const string functionIdentifier = "Edit(model)";

            try
            {
                if (!ModelState.IsValid) //Return and display the validation errors
                {
                    return View(model);
                }

                ManicNetRole role = await roleManager.FindByIdAsync(model.ID.ToString());
                role.Description = model.Description;

                await roleManager.UpdateAsync(role);

                UpdateClaimsHolder updateClaimsHolder = await manicNetPermissionService.CreatePermissionAdditionAndRemovalLists(model.Permissions, role, User);

                bool issueWithAdding = false;
                bool issueWithRemoving = false;

                foreach (Claim claim in updateClaimsHolder.ClaimsToAdd)
                {
                    IdentityResult addedClaim = await roleManager.AddClaimAsync(role, claim);
                    if (!addedClaim.Succeeded)
                    {
                        logger.LogWarning(path, functionIdentifier, string.Format("Unable to add permission {0} to role ({1})", claim.Value, role.Name));
                        warnings.Add(localizer[RoleLocalization.UNABLE_TO_ADD_PERMISSION_TO_ROLE, claim.Value, role.Name]);
                        issueWithAdding = true;
                    }
                }

                foreach (Claim claim in updateClaimsHolder.ClaimsToRemove)
                {
                    IdentityResult removedClaim = await roleManager.RemoveClaimAsync(role, claim);
                    if (!removedClaim.Succeeded)
                    {
                        logger.LogWarning(path, functionIdentifier, string.Format("Unable to remove permission {0} from role ({1})", claim.Value, role.Name));
                        warnings.Add(localizer[RoleLocalization.UNABLE_TO_REMOVE_PERMISSION_FROM_ROLE, claim.Value, role.Name]);
                        issueWithRemoving = true;
                    }
                }

                if (!(issueWithAdding || issueWithRemoving))
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Updated Role {0}", role.Name));
                    TempData[Alerts.TempData.SUCCESS] = localizer[RoleLocalization.UPDATED_ROLE, role.Name].ToString();
                }
                else
                {
                    TempData[Alerts.TempData.WARNING] = warnings;
                }

                return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = model.ID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to update permissions for role. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[RoleLocalization.UNABLE_TO_UPDATE_PERMISSIONS_FOR_ROLE, model.Name].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Owner.DELETE_ROLE)]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";

            try
            {
                ManicNetRole role = await roleManager.FindByIdAsync(id.ToString());
                ManicNetUser user = await userManager.GetUserAsync(User);

                if (role.IsDefaultUser)
                {
                    warnings.Add(localizer[RoleLocalization.ROLE_IS_DEFAULT_USER_ROLE_AND_CANNOT_BE_DELETED, role.Name]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
                }

                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    warnings.Add(localizer[RoleLocalization.ROLE_IS_ASSIGNED_TO_YOUR_ACCOUNT_AND_CANNOT_BE_DELETED, role.Name]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
                }

                await roleManager.DeleteAsync(role);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully deleted role {0}", role.Name));
                TempData[Alerts.TempData.SUCCESS] = localizer[RoleLocalization.SUCCESSFULLY_DELETED_ROLE, role.Name].ToString();

                return RedirectToAction(Routing.Controllers.Role.Routes.INDEX, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete role. {{ id:{0} }}", id));
                TempData[Alerts.TempData.WARNING] = localizer[RoleLocalization.UNABLE_TO_DELETE_ROLE].ToString();
                return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
            }
        }

        [Authorize(Policies.Owner.REASSIGN_DEFAULT_USER_ROLE)]
        [Route("reassign")]
        [HttpPost]
        public async Task<IActionResult> Reassign(Guid id)
        {
            const string functionIdentifier = "Reassign(id)";

            try
            {
                ManicNetRole role = await roleManager.FindByIdAsync(id.ToString());

                if (role.IsDefaultUser)
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully assigned role {0} as default role", role.Name));
                    TempData[Alerts.TempData.SUCCESS] = localizer[RoleLocalization.SUCCESSFULLY_ASSIGNED_ROLE_AS_DEFAULT_ROLE, role.Name].ToString();
                    return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
                }

                ManicNetRole previousRole = roleManager.Roles.Where(r => r.IsDefaultUser == true).FirstOrDefault();
                previousRole = await roleManager.FindByIdAsync(previousRole.Id.ToString());

                role.IsDefaultUser = true;
                previousRole.IsDefaultUser = false;

                await roleManager.UpdateAsync(role);
                await roleManager.UpdateAsync(previousRole);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully assigned role {0} as default role", role.Name));
                TempData[Alerts.TempData.SUCCESS] = localizer[RoleLocalization.SUCCESSFULLY_ASSIGNED_ROLE_AS_DEFAULT_ROLE, role.Name].ToString();
                return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete role. {{ id:{0} }}", id));
                TempData[Alerts.TempData.WARNING] = localizer[RoleLocalization.UNABLE_TO_REASSIGN_ROLE].ToString();
                return RedirectToAction(Routing.Controllers.Role.Routes.SHOW, Routing.Controllers.Role.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = id });
            }
        }
    }
}
