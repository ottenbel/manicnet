﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Administration.Controllers.Configuration
{
    [Authorize(Policies.Generic.MANAGE_CACHING_CONFIGURATION_GENERIC)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class CacheConfigurationController : Controller
    {
        private readonly ICacheConfigurationRetrievalService cacheConfigurationRetrievalService;
        private readonly ICacheConfigurationUpdateService cacheConfigurationUpdateService;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly IStringLocalizer<ConfigurationLocalization> localizer;
        private readonly IStringLocalizer<ConfigurationLocalization> configurationLocalizer;

        private readonly string path = "ManicNet.Web.Administration.CacheConfigurationController";

        public CacheConfigurationController(ICacheConfigurationRetrievalService cacheConfigurationRetrievalService,
            ICacheConfigurationUpdateService cacheConfigurationUpdateService,
            IMapper mapper,
            IManicNetLoggerService logger,
            IStringLocalizer<ConfigurationLocalization> localizer,
            IStringLocalizer<ConfigurationLocalization> configurationLocalizer)
        {
            this.cacheConfigurationRetrievalService = cacheConfigurationRetrievalService;
            this.cacheConfigurationUpdateService = cacheConfigurationUpdateService;
            this.mapper = mapper;
            this.logger = logger;
            this.localizer = localizer;
            this.configurationLocalizer = configurationLocalizer;
        }

        [Authorize(Policies.Owner.MANAGE_CACHE_CONFIGURATIONS)]
        [Route("manage")]
        [HttpGet]
        public async Task<IActionResult> Manage()
        {
            const string functionIdentifier = "Manage()";
            CacheConfigurationManageModel model = new CacheConfigurationManageModel();

            try
            {
                List<CacheConfiguration> cacheConfigurations = await cacheConfigurationRetrievalService.GetCacheConfigurationsAsync();

                model.cacheConfigurations = mapper.Map<List<CacheConfigurationModifyModel>>(cacheConfigurations, opt => { }).OrderBy(c => c.SectionText).ThenBy(c => c.LookupTypeText).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of cache configurations."));
                TempData[Alerts.TempData.ERRROR] = localizer[ConfigurationLocalization.CacheConfiguration.UNABLE_TO_RETRIEVE_LIST_OF_CACHE_CONFIGURATIONS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Owner.MANAGE_CACHE_CONFIGURATIONS)]
        [Route("manage")]
        [HttpPost]
        public async Task<IActionResult> Manage(CacheConfigurationManageModel model)
        {
            const string functionIdentifier = "Manage(model)";

            try
            {
                if (!ModelState.IsValid)
                {
                    foreach (var cacheConfiguration in model.cacheConfigurations)
                    {
                        cacheConfiguration.LengthTypes = GetLengthTypes();
                    }

                    return View(model);
                }

                List<CacheConfiguration> cacheConfigurations = await cacheConfigurationRetrievalService.GetCacheConfigurationsAsync();
                List<CacheConfiguration> cacheConfigurationsToUpdate = new List<CacheConfiguration>();

                foreach (var cacheConfiguration in cacheConfigurations)
                {
                    var cacheConfigurationToUpdate = model.cacheConfigurations.Where(c => c.CacheConfigurationID == cacheConfiguration.CacheConfigurationID).FirstOrDefault();
                    if (cacheConfigurationToUpdate != null && (cacheConfigurationToUpdate.LengthTypeID != cacheConfiguration.LengthTypeID || cacheConfigurationToUpdate.Length != cacheConfiguration.Length))
                    {
                        cacheConfiguration.LengthTypeID = cacheConfigurationToUpdate.LengthTypeID;
                        cacheConfiguration.Length = cacheConfigurationToUpdate.Length;
                        cacheConfigurationsToUpdate.Add(cacheConfiguration);
                    }
                }

                if (cacheConfigurationsToUpdate.Any())
                {
                    await cacheConfigurationUpdateService.BulkUpdateCacheConfiguration(cacheConfigurationsToUpdate);
                }

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully edited cache configurations."));
                TempData[Alerts.TempData.SUCCESS] = localizer[ConfigurationLocalization.CacheConfiguration.CACHE_CONFIGURATION_SUCCESSFULLY_EDITED].ToString();
                return RedirectToAction(Routing.Controllers.CacheConfiguration.Routes.MANAGE, Routing.Controllers.CacheConfiguration.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            catch (Exception ex)
            {
                foreach (var cacheConfiguration in model.cacheConfigurations)
                {
                    cacheConfiguration.LengthTypes = GetLengthTypes();
                }

                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to update cache configuration object. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[ConfigurationLocalization.CacheConfiguration.ERROR_EDITING_CACHE_CONFIGURATIONS].ToString();
            }

            return View(model);
        }

        private List<SelectListItem> GetLengthTypes()
        {
            List<SelectListItem> result = new List<SelectListItem>()
            {
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.DAY], Caching.Configurations.LengthTypes.DAY.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.HOUR], Caching.Configurations.LengthTypes.HOUR.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.MINUTE], Caching.Configurations.LengthTypes.MINUTE.ToString()),
                new SelectListItem(configurationLocalizer[ConfigurationLocalization.CacheConfiguration.SECOND], Caching.Configurations.LengthTypes.SECOND.ToString()),
            };

            return result;
        }
    }
}
