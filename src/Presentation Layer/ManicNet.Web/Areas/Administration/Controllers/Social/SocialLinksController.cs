﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_SOCIAL_LINK)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public class SocialLinksController : Controller
    {
        private readonly ISocialLinkService socialLinkService;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IStringLocalizer<SocialLocalization> localizer;

        private readonly string path = "ManicNet.Web.SocialLinksController";

        private List<string> warnings;

        public SocialLinksController(ISocialLinkService socialLinkService, IMapper mapper, IManicNetLoggerService logger, 
            IManicNetAuthorizationService authorizationService,IStringLocalizer<SocialLocalization> localizer)
        {
            this.socialLinkService = socialLinkService;
            this.mapper = mapper;
            this.logger = logger;
            this.localizer = localizer;
            this.authorizationService = authorizationService;
            this.warnings = new List<string>();
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            const string functionIdentifier = "Index()";
            
            SocialLinkIndexViewModel model = new SocialLinkIndexViewModel();

            try
            {
                //TODO: We should be caching this
                List<SocialLink> socialLinks = await socialLinkService.GetActiveSocialLinks();

                List<SocialLinkViewModel> socialLinksView = mapper.Map<List<SocialLinkViewModel>>(socialLinks);
                model.Links = socialLinksView.OrderBy(sl => sl.Priority).ToList();
                //TODO: We should be caching this
                model.HasDeleted = await socialLinkService.DoDeletedSocialLinksExist();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of social links."));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_RETRIEVE_LIST_OF_SOCIAL_LINKS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_SOCIAL_LINK)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted()
        {
            const string functionIdentifier = "Deleted()";

            SocialLinkIndexViewModel model = new SocialLinkIndexViewModel();

            try
            {
                List<SocialLink> socialLinks = await socialLinkService.GetDeletedSocialLinks(User);

                List<SocialLinkViewModel> socialLinksView = mapper.Map<List<SocialLinkViewModel>>(socialLinks);
                model.Links = socialLinksView.OrderBy(sl => sl.Priority).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted social links."));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_SOCIAL_LINKS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [Route("create")]
        [HttpGet]
        public IActionResult Create()
        {
            SocialLinkModifyModel model = new SocialLinkModifyModel
            {
                LinkTypes = GetLinkTypes()
            };

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(SocialLinkModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            SocialLink socialLink = new SocialLink();

            try
            {
                if (!ModelState.IsValid)
                {
                    model.LinkTypes = GetLinkTypes();

                    return View(model);
                }

                socialLink = mapper.Map<SocialLink>(model);

                await socialLinkService.CreateSocialLink(socialLink);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully created social link object ({0}).", socialLink.Text));
                TempData[Alerts.TempData.SUCCESS] = localizer[SocialLocalization.SOCIAL_LINK_SUCCESSFULLY_CREATED, socialLink.Text].ToString();

                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = socialLink.SocialLinkID });

            }
            catch (Exception ex)
            {
                model.LinkTypes = GetLinkTypes();

                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create social link object. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.ERROR_CREATING_SOCIAL_LINK, model.Text].ToString();
            }

            return View(model);
        }

        [CheckSocialLinkExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            SocialLinkViewModel model = new SocialLinkViewModel();

            try
            {
                SocialLink socialLink = (SocialLink)HttpContext.Items[FilterIdentifiers.SOCIAL_LINK_FROM_FILTER];
                if (socialLink == null)
                {
                    socialLink = await socialLinkService.FindSocialLinkAsync(id, User);
                }

                model = mapper.Map<SocialLinkViewModel>(socialLink);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show social link {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_LOAD_SOCIAL_LINK_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [CheckSocialLinkExistenceIncludeCacheFilter(Order = 1)]
        [IsSocialLinkEditableFilter(Order = 2)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            SocialLinkModifyModel model = new SocialLinkModifyModel();

            try
            {
                SocialLink socialLink = await GetSocialLinkHelperAsync(id, false);
                model = mapper.Map<SocialLinkModifyModel>(socialLink);

                model.LinkTypes = GetLinkTypes();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show social link. {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_LOAD_SOCIAL_LINK_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [CheckSocialLinkExistenceBypassCacheFilter(Order = 1)]
        [IsSocialLinkEditableFilter(Order = 2)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(SocialLinkModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";

            SocialLink socialLink = new SocialLink();
            bool bypassCache = true;
            List<string> tagProblems = new List<string>();

            try
            {
                if (!ModelState.IsValid)
                {
                    model.LinkTypes = GetLinkTypes();

                    return View(model);
                }

                socialLink = await GetSocialLinkHelperAsync(model.SocialLinkID, bypassCache);

                mapper.Map(model, socialLink);

                await socialLinkService.UpdateSocialLink(socialLink);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully updated social link object ({0}).", socialLink.Text));
                TempData[Alerts.TempData.SUCCESS] = localizer[SocialLocalization.SOCIAL_LINK_SUCCESSFULLY_UPDATED, socialLink.Text].ToString();

                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = socialLink.SocialLinkID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit social link object ({0}).", model));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.ERROR_EDITING_SOCIAL_LINK, model.Text].ToString();
            }

            return View(model);
        }



        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [CheckSocialLinkExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(id)";

            bool bypassCache = true;

            try
            {
                SocialLink socialLink = await GetSocialLinkHelperAsync(id, bypassCache);

                socialLink.IsDeleted = true;

                await socialLinkService.UpdateSocialLink(socialLink);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted social link object ({0}).", socialLink.Text));
                TempData[Alerts.TempData.SUCCESS] = localizer[SocialLocalization.SOCIAL_LINK_SUCCESSFULLY_SOFT_DELETED, socialLink.Text].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_SOCIAL_LINK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = socialLink.SocialLinkID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.TagType.Routes.INDEX, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete social link. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_SOFT_DELETE_SOCIAL_LINK, id].ToString();
                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.EDIT, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_SOCIAL_LINK)]
        [CheckSocialLinkExistenceBypassCacheFilter]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(id)";

            bool bypassCache = true;

            try
            {
                SocialLink socialLink = await GetSocialLinkHelperAsync(id, bypassCache);

                socialLink.IsDeleted = false;

                await socialLinkService.UpdateSocialLink(socialLink);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored social link object ({0}).", socialLink.Text));
                TempData[Alerts.TempData.SUCCESS] = localizer[SocialLocalization.SOCIAL_LINK_SUCCESSFULLY_RESTORED, socialLink.Text].ToString();

                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id = socialLink.SocialLinkID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore social link. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_RESTORE_SOCIAL_LINK, id].ToString();
                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.EDIT, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Owner.DELETE_SOCIAL_LINK)]
        [CheckSocialLinkExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            string functionIdentifier = "Delete(id)";

            bool bypassCache = true;

            try
            {
                SocialLink socialLink = await GetSocialLinkHelperAsync(id, bypassCache);

                await socialLinkService.RemoveSocialLink(socialLink);

                TempData[Alerts.TempData.SUCCESS] = localizer[SocialLocalization.SOCIAL_LINK_SUCCESSFULLY_DELETED, socialLink.Text].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_SOCIAL_LINK)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.SocialLinks.Routes.DELETED, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.SocialLinks.Routes.INDEX, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
            }
            catch (SocialLinkNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Social link is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[SocialLocalization.SOCIAL_LINK_IS_NOT_DELETABLE, id].ToString());
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete social link {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[SocialLocalization.UNABLE_TO_DELETE_SOCIAL_LINK, id].ToString();
                return RedirectToAction(Routing.Controllers.SocialLinks.Routes.SHOW, Routing.Controllers.SocialLinks.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        public async Task<SocialLink> GetSocialLinkHelperAsync(Guid id, bool bypassCache)
        {
            SocialLink socialLink = (SocialLink)HttpContext.Items[FilterIdentifiers.SOCIAL_LINK_FROM_FILTER];
            if (socialLink == null)
            {
                socialLink = await socialLinkService.FindSocialLinkAsync(id, User, bypassCache);
            }

            return socialLink;
        }

        private List<SelectListItem> GetLinkTypes()
        {
            return new List<SelectListItem>()
            {
                new SelectListItem(localizer[SocialLocalization.LinkType.EMAIL], Social.LinkTypes.EMAIL.ToString()),
                new SelectListItem(localizer[SocialLocalization.LinkType.LINK], Social.LinkTypes.LINK.ToString())
            };
        }
    }
}
