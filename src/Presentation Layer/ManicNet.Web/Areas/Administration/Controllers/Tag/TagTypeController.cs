﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_TAG_TYPE)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class TagTypeController : Controller
    {
        private readonly ITagTypeUpdateService tagTypeUpdateService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IStringLocalizer<TagTypeLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.TagTypeController";

        private List<string> warnings;

        public TagTypeController(ITagTypeUpdateService tagTypeUpdateService, ITagTypeRetrievalService tagTypeRetrievalService,
            ITagRetrievalService tagRetrievalService, IManicNetLoggerService logger, IMapper mapper, IOptions<ManicNetSettings> settings,
            IManicNetAuthorizationService authorizationService, IStringLocalizer<TagTypeLocalization> localizer)
        {
            this.tagTypeUpdateService = tagTypeUpdateService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagRetrievalService = tagRetrievalService;
            this.logger = logger;
            this.mapper = mapper;
            this.settings = settings;
            this.authorizationService = authorizationService;
            this.localizer = localizer;
            warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_TAG_TYPE)]
        [Route("create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            TagTypeStyling styling = settings.Value.StylingSettings.TagTypeStyling;

            List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();

            int priority = 1;
            int defaultNumberDisplayed = 5;
            if (tagTypes.Any())
            {
                priority = tagTypes.OrderByDescending(tt => tt.Priority).FirstOrDefault().Priority + 1;
            }

            TagTypeModifyModel model = new TagTypeModifyModel(styling.PrimaryColouring.Body, styling.PrimaryColouring.Border, styling.PrimaryColouring.Text, styling.SecondaryColouring.Body, styling.SecondaryColouring.Border, styling.SecondaryColouring.Text, styling.DefaultIcon, styling.DefaultFontFamily, priority, defaultNumberDisplayed);
            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG_TYPE)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(TagTypeModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            TagType tagType = new TagType();

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                tagType = mapper.Map<TagType>(model, opt => { });

                await tagTypeUpdateService.CreateTagType(tagType);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully created tag type object ({0}).", tagType.Name));

                TempData[Alerts.TempData.SUCCESS] = localizer[TagTypeLocalization.TAG_TYPE_SUCCESSFULLY_CREATED, tagType.Name].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.SHOW, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tagType.TagTypeID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create tag type. {{ tagType:{0} }}.", model));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.ERROR_CREATING_TAG_TYPE, model.Name].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG_TYPE)]
        [CheckTagTypeExistenceIncludeCacheFilter(Order = 1)]
        [IsTagTypeEditableFilter(Order = 2)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            TagTypeModifyModel model = new TagTypeModifyModel();

            try
            {
                TagType tagType = await GetTagTypeHelperAsync(id, false);

                model = mapper.Map<TagTypeModifyModel>(tagType, opt => { });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show tag type. {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_LOAD_TAG_TYPE_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG_TYPE)]
        [CheckTagTypeExistenceBypassCacheFilter(Order = 1)]
        [IsTagTypeEditableFilter(Order = 2)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(TagTypeModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";

            bool bypassCache = true;

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                TagType tagType = await GetTagTypeHelperAsync(model.TagTypeID, bypassCache);
                List<Tag> tags = await tagRetrievalService.GetTagsByTagType(tagType.TagTypeID);

                mapper.Map(model, tagType);

                await tagTypeUpdateService.UpdateTagType(tagType, tags);

                TempData["success"] = localizer[TagTypeLocalization.TAG_TYPE_SUCCESSFULLY_UPDATED, tagType.Name].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.SHOW, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tagType.TagTypeID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to update tag type. {{ id:{0} }}.", model.TagTypeID));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_UPDATE_TAG_TYPE_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [CheckTagTypeExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(id)";

            bool bypassCache = true;

            try
            {
                TagType tagType = await GetTagTypeHelperAsync(id, bypassCache);
                List<Tag> tags = await tagRetrievalService.GetTagsByTagType(tagType.TagTypeID);

                tagType.IsDeleted = true;

                await tagTypeUpdateService.UpdateTagType(tagType, tags);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted tag type object ({0}).", tagType.Name));
                TempData[Alerts.TempData.SUCCESS] = localizer[TagTypeLocalization.TAG_TYPE_SUCCESSFULLY_SOFT_DELETED, tagType.Name].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.TagType.Routes.SHOW, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tagType.TagTypeID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.TagType.Routes.INDEX, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete tag type. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_SOFT_DELETE_TAG_TYPE, id].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.EDIT, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_TAG_TYPE)]
        [CheckTagTypeExistenceBypassCacheFilter]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(id)";

            bool bypassCache = true;

            try
            {
                TagType tagType = await GetTagTypeHelperAsync(id, bypassCache);
                List<Tag> tags = await tagRetrievalService.GetTagsByTagType(tagType.TagTypeID);

                tagType.IsDeleted = false;

                await tagTypeUpdateService.UpdateTagType(tagType, tags);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored tag type object ({0}).", tagType.Name));
                TempData[Alerts.TempData.SUCCESS] = localizer[TagTypeLocalization.TAG_TYPE_SUCCESSFULLY_RESTORED, tagType.Name].ToString();

                return RedirectToAction(Routing.Controllers.TagType.Routes.SHOW, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tagType.TagTypeID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore tag type. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_RESTORE_TAG_TYPE, id].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.EDIT, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Owner.DELETE_TAG_TYPE)]
        [CheckTagTypeExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";

            bool bypassCache = true;

            try
            {
                TagType tagType = await GetTagTypeHelperAsync(id, bypassCache);
                List<Tag> tags = await tagRetrievalService.GetTagsByTagType(tagType.TagTypeID);

                await tagTypeUpdateService.RemoveTagType(tagType, tags);

                TempData[Alerts.TempData.SUCCESS] = localizer[TagTypeLocalization.TAG_TYPE_SUCCESSFULLY_DELETED, tagType.Name].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.DELETED, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            catch (TagTypeNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(string.Format(localizer[TagTypeLocalization.TAG_TYPE_IS_NOT_IN_A_DELETABLE_STATE, id].ToString()));
                TempData[Alerts.TempData.WARNING] = warnings;
                return RedirectToAction(Routing.Controllers.TagType.Routes.EDIT, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete tag type. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_DELETE_TAG_TYPE, id].ToString();
                return RedirectToAction(Routing.Controllers.TagType.Routes.EDIT, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_TAG_TYPE)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(int? pageNumber)
        {
            const string functionIdentifier = "Deleted(pageNumber)";

            TagTypeIndexViewModel model = new TagTypeIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.TagType.Routes.DELETED;

            try
            {
                List<TagType> tagTypes = await tagTypeRetrievalService.GetDeletedTagTypes();
                List<TagTypeViewModel> tagTypesView = mapper.Map<List<TagTypeViewModel>>(tagTypes, opt => { });
                model.TagTypes = await tagTypesView.OrderBy(x => x.Priority)
                    .ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted tag types. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_TAG_TYPES].ToString();
            }

            return View(model);
        }

        private async Task<TagType> GetTagTypeHelperAsync(Guid id, bool bypassCache)
        {
            TagType tagType = (TagType)HttpContext.Items[FilterIdentifiers.TAG_TYPE_FROM_FILTER];
            if (tagType == null)
            {
                tagType = await tagTypeRetrievalService.FindTagTypeAsync(id, User, bypassCache);
            }

            return tagType;
        }
    }
}