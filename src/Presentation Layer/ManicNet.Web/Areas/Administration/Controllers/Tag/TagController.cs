﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Extensions;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using X.PagedList;

namespace ManicNet.Web.Areas.Administration.Controllers
{
    [Authorize(Policies.Generic.MANAGE_TAG)]
    [Area("Administration")]
    [Route("administration/[controller]")]
    public sealed class TagController : Controller
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagUpdateService tagUpdateService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagAliasService tagAliasService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IStringLocalizer<TagLocalization> localizer;

        private readonly string path = "ManicNet.Web.Administration.TagController";

        private List<string> warnings;

        public TagController(ITagRetrievalService tagRetrievalService, ITagUpdateService tagUpdateService, ITagTypeRetrievalService tagTypeRetrievalService,
            ITagAliasService tagAliasService, IManicNetLoggerService logger, IMapper mapper, IOptions<ManicNetSettings> settings,
            IManicNetAuthorizationService authorizationService, IStringLocalizer<TagLocalization> localizer)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.tagUpdateService = tagUpdateService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagAliasService = tagAliasService;
            this.logger = logger;
            this.mapper = mapper;
            this.settings = settings;
            this.authorizationService = authorizationService;
            this.localizer = localizer;

            warnings = new List<string>();
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [Route("create")]
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            const string functionIdentifier = "Create()";

            TagModifyModel model = new TagModifyModel();

            try
            {
                var tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<SelectListItem> tagTypeList = await tagTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.TagTypeID.ToString() }).ToListAsync();

                if (!tagTypeList.Any())
                {
                    warnings.Add(localizer[TagLocalization.ACTIVE_TAG_TYPES_MUST_EXIST]);
                    TempData[Alerts.TempData.WARNING] = warnings;

                    if ((await authorizationService.Authorize(User, Policies.Administrator.EDIT_TAG_TYPE)).Succeeded)
                    {
                        return RedirectToAction(Routing.Controllers.TagType.Routes.CREATE, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                    }
                    else
                    {
                        return RedirectToAction(Routing.Controllers.TagType.Routes.INDEX, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL });
                    }
                }

                model.ImpliedTags = mapper.Map<TagTypeTagInputModel>(tagTypes, opt => { });
                model.ImpliedTags.AllowVirtual = true;
                model.ImpliedTags.Header = localizer[TagLocalization.IMPLIES_TAGS]; ;

                model.TagTypes = tagTypeList;
                model.Aliases = string.Empty;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error loading tag type information."));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RETRIEVE_TAG_TYPE_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [Route("create")]
        [HttpPost]
        public async Task<IActionResult> Create(TagModifyModel model)
        {
            const string functionIdentifier = "Create(model)";

            Tag tag = new Tag();

            try
            {
                if (!ModelState.IsValid)
                {
                    model.TagTypes = await tagTypeRetrievalService.GetTagTypesSelectList();

                    return View(model);
                }

                tag = mapper.Map<Tag>(model, opt => { });

                string impliedTags = string.Join(", ", model.ImpliedTags.Tags.Select(t => t.Tags));
                TagSearchLists tagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(impliedTags);
                TagSearchLists implications = tagUpdateService.ValidateTagImplicationList(tag, tagSearchLists.Included, new List<Tag>());

                List<string> aliasComponents = SearchStringHelper.FromCommaSeparatedStringToDistinctPopulatedList(model.Aliases);
                List<TagAlias> aliases = await tagAliasService.GetAliasList(aliasComponents);

                tag.Aliases.Clear();
                tag.Aliases.AddRange(aliases);

                await tagUpdateService.CreateTag(tag, implications.Included);

                logger.LogInformation(path, functionIdentifier, string.Format("Successfully created tag object ({0}).", tag.Name));
                TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAG_SUCCESSFULLY_CREATED].ToString();

                return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create tag object. {{ model:{0} }}", model));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.ERROR_CREATING_TAG, model.Name].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [Route("bulkcreate")]
        [HttpGet]
        public async Task<IActionResult> BulkCreate()
        {
            TagBulkCreationModel model = new TagBulkCreationModel();

            List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
            List<SelectListItem> tagTypeList = tagTypes.Select(x => new SelectListItem { Text = x.Name, Value = x.TagTypeID.ToString() }).ToList();

            if (!tagTypeList.Any())
            {
                warnings.Add(localizer[TagLocalization.ACTIVE_TAG_TYPES_MUST_EXIST]);
                TempData[Alerts.TempData.WARNING] = warnings;

                if ((await authorizationService.Authorize(User, Policies.Administrator.EDIT_TAG_TYPE)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.TagType.Routes.CREATE, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.TagType.Routes.INDEX, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }

            model.TagTypes = tagTypeList;

            model.ImpliedTags = mapper.Map<TagTypeTagInputModel>(tagTypes, opt => { });
            model.ImpliedTags.AllowVirtual = true;
            model.ImpliedTags.Header = localizer[TagLocalization.IMPLIES_TAGS];

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [Route("bulkcreate")]
        [HttpPost]
        public async Task<IActionResult> BulkCreate(TagBulkCreationModel model)
        {
            const string functionIdentifier = "BulkCreate(model)";

            string createdTagList = string.Empty;
            bool createdTag = false;

            try
            {
                if (!ModelState.IsValid)
                {
                    model.TagTypes = await tagTypeRetrievalService.GetTagTypesSelectList();

                    return View(model);
                }

                string impliedTags = string.Join(", ", model.ImpliedTags.Tags.Select(t => t.Tags));
                TagSearchLists tagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(impliedTags);

                List<TagModifyModel> tagModels = new List<TagModifyModel>();
                List<string> tagNames = model.Names.FromCommaSeparatedStringToDistinctPopulatedList();

                foreach (string tagName in tagNames)
                {
                    TagModifyModel tagModel = new TagModifyModel(Guid.Empty, model.TagTypeID, tagName, string.Empty, string.Empty, string.Empty, model.IsVirtual, model.Icon, model.FontFamily);
                    Tag tag = mapper.Map<Tag>(tagModel, opt => { });

                    TagSearchLists implications = tagUpdateService.ValidateTagImplicationList(tag, tagSearchLists.Included, new List<Tag>());

                    await tagUpdateService.CreateTag(tag, implications.Included);

                    logger.LogInformation(path, functionIdentifier, string.Format("Tag successfully created as part of bulk create: ({0}).", tag.Name));

                    createdTagList = string.Format("{0}{1}{2}", createdTagList, (createdTag ? ", " : string.Empty), tag.Name);
                    createdTag = true;
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAGS_WERE_SUCCESSFULLY_CREATED, createdTagList].ToString();
                return RedirectToAction(Routing.Controllers.Tag.Routes.INDEX, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to create tags. {{ model:{0} }}.", model));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.ERROR_CREATING_TAGS, model.Names].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.VIEW_DELETED_TAG)]
        [Route("deleted")]
        [HttpGet]
        public async Task<IActionResult> Deleted(int? pageNumber, string search)
        {
            const string functionIdentifier = "Deleted(pageNumber, search)";

            TagIndexViewModel model = new TagIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.Tag.Routes.DELETED;

            try
            {
                List<Tag> tags = await tagRetrievalService.GetDeletedTags(User);

                if (!(string.IsNullOrWhiteSpace(search)))
                {
                    search = search.Trim();
                    tags = tags.Where(t => t.Name.Contains(search)).ToList();
                }

                List<TagViewModel> tagTypesView = mapper.Map<List<TagViewModel>>(tags, opt => { });
                model.Tags = await tagTypesView.OrderBy(x => x.Name)
                    .ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of deleted tags. {{ pageNumber:{0}, search:{1} }}", pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RETRIEVE_LIST_OF_DELETED_TAGS].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [CheckTagExistenceIncludeCacheFilter(Order = 1)]
        [IsTagEditableFilter(Order = 2)]
        [Route("edit/{id}")]
        [HttpGet]
        public async Task<IActionResult> Edit(Guid id)
        {
            const string functionIdentifier = "Edit(id)";

            TagModifyModel model = new TagModifyModel();

            try
            {
                List<Tag> tags = await tagRetrievalService.GetActiveTags();
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                Tag tag = await GetTagHelperAsync(id, false);

                if (!tagTypes.Any())
                {
                    warnings.Add(localizer[TagLocalization.ACTIVE_TAG_TYPES_MUST_EXIST]);
                    TempData[Alerts.TempData.WARNING] = warnings;

                    if ((await authorizationService.Authorize(User, Policies.Administrator.EDIT_TAG_TYPE)).Succeeded)
                    {
                        return RedirectToAction(Routing.Controllers.TagType.Routes.CREATE, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
                    }
                    else
                    {
                        return RedirectToAction(Routing.Controllers.TagType.Routes.INDEX, Routing.Controllers.TagType.CONTROLLER, new { area = Routing.Areas.GENERAL });
                    }
                }

                model = mapper.Map<TagModifyModel>(tag, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error loading tag information."));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RETRIEVE_TAG_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [CheckTagExistenceBypassCacheFilter(Order = 1)]
        [IsTagEditableFilter(Order = 2)]
        [Route("edit")]
        [HttpPost]
        public async Task<IActionResult> Edit(TagModifyModel model)
        {
            const string functionIdentifier = "Edit(model)";

            Tag tag = new Tag();
            bool bypassCache = true;
            List<string> tagProblems = new List<string>();

            try
            {
                model.TagTypes = await tagTypeRetrievalService.GetTagTypesSelectList();

                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                tag = await GetTagHelperAsync(model.TagID, bypassCache);

                mapper.Map(model, tag);

                string impliedTags = string.Join(", ", model.ImpliedTags.Tags.Select(t => t.Tags));
                TagSearchLists tagSearchLists = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(impliedTags);
                List<Tag> ancestry = await tagRetrievalService.GetListOfTagsImplyingTag(tag);
                TagSearchLists implications = tagUpdateService.ValidateTagImplicationList(tag, tagSearchLists.Included, ancestry);

                TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag, implications.Included.Select(t => t.TagID));

                foreach (Tag excluded in implications.Excluded)
                {
                    tagProblems.Add(localizer[TagLocalization.TAG_CAUSES_IMPLICATION_LOOPS, HttpUtility.HtmlEncode(excluded.Name), tag.Name]);
                }

                List<string> aliasComponents = SearchStringHelper.FromCommaSeparatedStringToDistinctPopulatedList(model.Aliases);
                List<TagAlias> aliases = await tagAliasService.GetAliasList(aliasComponents);

                tag.Aliases.Clear();
                tag.Aliases.AddRange(aliases);

                await tagUpdateService.UpdateTag(tag, implications.Included, tagCacheCleanup);

                if (tagProblems.Any())
                {
                    warnings.Add(localizer[TagLocalization.SUCCESSFULLY_UPDATED_TAG_UNABLE_TO_IMPLY_ALL]);
                    warnings = warnings.Concat(tagProblems).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully edited tag object ({0}).", tag.Name));
                    TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAG_SUCCESSFULLY_EDITED, tag.Name].ToString();
                }

                return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to edit tag object ({0}).", model));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.ERROR_EDITING_TAG, model.Name].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [CheckTagExistenceBypassCacheFilter]
        [Route("softdelete")]
        [HttpPost]
        public async Task<ActionResult> SoftDelete(Guid id)
        {
            const string functionIdentifier = "SoftDelete(id)";

            bool bypassCache = true;

            try
            {
                Tag tag = await GetTagHelperAsync(id, bypassCache);

                if (!tag.IsDeleted)
                {
                    tag.IsDeleted = true;

                    await tagUpdateService.UpdateTag(tag);

                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully soft deleted tag object ({0}).", tag.Name));
                }

                TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAG_SUCCESSFULLY_SOFT_DELETED, tag.Name].ToString();

                if ((await authorizationService.Authorize(User, Policies.Administrator.VIEW_DELETED_TAG)).Succeeded)
                {
                    return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
                }
                else
                {
                    return RedirectToAction(Routing.Controllers.Tag.Routes.INDEX, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL });
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to soft delete tag. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_SOFT_DELETE_TAG, id].ToString();
                return RedirectToAction(Routing.Controllers.Tag.Routes.EDIT, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
            }
        }

        [Authorize(Policies.Administrator.EDIT_TAG)]
        [CheckTagExistenceBypassCacheFilter]
        [Route("restore")]
        [HttpPost]
        public async Task<ActionResult> Restore(Guid id)
        {
            const string functionIdentifier = "Restore(id)";

            bool bypassCache = true;
            Tag tag = null;
            List<Tag> tags = new List<Tag>();
            TagType tagType = null;
            List<string> tagProblems = new List<string>();

            try
            {
                tag = await GetTagHelperAsync(id, bypassCache);

                tagType = await tagTypeRetrievalService.FindTagTypeAsync(tag.TagTypeID, User, bypassCache);
                tags = await tagRetrievalService.GetActiveTags();

                if (tagType.IsDeleted)
                {
                    warnings.Add(localizer[TagLocalization.UNABLE_TO_RESTORE_TAG_WITH_DELETED_TAG_TYPE, tag.Name, tagType.Name]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
                }

                tag.IsDeleted = false;

                List<Tag> impliedTags = tags.Where(t => tag.Implies.Select(i => i.ImpliesTagID).Contains(t.TagID)).ToList();
                List<Tag> ancestry = await tagRetrievalService.GetListOfTagsImplyingTag(tag);
                TagSearchLists implications = tagUpdateService.ValidateTagImplicationList(tag, impliedTags, ancestry);

                TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag, implications.Included.Select(t => t.TagID));

                foreach (Tag excluded in implications.Excluded)
                {
                    tagProblems.Add(localizer[TagLocalization.TAG_CAUSES_IMPLICATION_LOOPS, HttpUtility.HtmlEncode(excluded.Name), tag.Name]);
                }

                await tagUpdateService.UpdateTag(tag, implications.Included, tagCacheCleanup);

                if (tagProblems.Any())
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored tag object ({0}), but was unable to successfully include all implied tags.", tag.Name));
                    warnings.Add(localizer[TagLocalization.SUCCESSFULLY_RESTORED_TAG_UNABLE_TO_IMPLY_ALL]);
                    warnings = warnings.Concat(tagProblems).ToList();
                    TempData[Alerts.TempData.WARNING] = warnings;
                }
                else
                {
                    logger.LogInformation(path, functionIdentifier, string.Format("Successfully restored tag {{ id:{0} }}", id));
                    TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAG_SUCCESSFULLY_RESTORED, tag.Name].ToString();
                }

                return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to restore tag. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RESTORE_TAG, id].ToString();
            }

            return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = tag.TagID });
        }

        [Authorize(Policies.Owner.DELETE_TAG)]
        [CheckTagExistenceBypassCacheFilter]
        [Route("delete")]
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            const string functionIdentifier = "Delete(id)";

            bool bypassCache = true;

            try
            {
                Tag tag = await GetTagHelperAsync(id, bypassCache);

                await tagUpdateService.RemoveTag(tag);

                TempData[Alerts.TempData.SUCCESS] = localizer[TagLocalization.TAG_SUCCESSFULLY_DELETED, tag.Name].ToString();
                return RedirectToAction(Routing.Controllers.Tag.Routes.DELETED, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION });
            }
            catch (TagNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag is not in a deletable state. {{ id:{0} }}", id));
                warnings.Add(localizer[TagLocalization.TAG_IS_NOT_IN_DELETABLE_STATE, id]);
                TempData[Alerts.TempData.WARNING] = warnings;
            }
            catch (TagNotDeletableHasChildrenException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag is not in a deletable state as it implies other tags. {{ id:{0} }}", id));
                warnings.Add(localizer[TagLocalization.TAG_IS_NOT_IN_DELETABLE_STATE_IMPLIES_OTHER_TAGS, id]);
                TempData[Alerts.TempData.WARNING] = warnings;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to delete tag. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_DELETE_TAG].ToString();
            }

            return RedirectToAction(Routing.Controllers.Tag.Routes.EDIT, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.ADMINISTRATION, id });
        }

        [Authorize(Policies.Owner.MIGRATE_TAG)]
        [CheckTagExistenceIncludeCacheFilter(Order = 1)]
        [IsTagEditableFilter(Order = 2)]
        [IsTagNotVirtualFilter(Order = 3)]
        [Route("migrate/{id}")]
        [HttpGet]
        public async Task<IActionResult> Migrate(Guid id)
        {
            const string functionIdentifier = "Migrate(id)";

            TagMigrationModel model = new TagMigrationModel();

            try
            {
                Tag tag = await GetTagHelperAsync(id, false);

                model = mapper.Map<TagMigrationModel>(tag, opt => { });
            }
            catch (Exception ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Error loading tag information."));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RETRIEVE_TAG_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.Owner.DELETE_TAG)]
        [CheckTagExistenceBypassCacheFilter(Order = 1)]
        [IsTagEditableFilter(Order = 2)]
        [IsTagNotVirtualFilter(Order = 3)]
        [Route("migrate")]
        [HttpPost]
        public async Task<IActionResult> Migrate(TagMigrationModel migrationModel)
        {
            const string functionIdentifier = "Migrate(migrationModel)";

            try
            {
                if (!ModelState.IsValid)
                {
                    return View(migrationModel);
                }

                Tag sourceTag = await GetTagHelperAsync(migrationModel.TagID, true);
                Tag destinationTag = await tagRetrievalService.FindTagByName(migrationModel.DestinationTagName, User);

                if (sourceTag.TagID == destinationTag.TagID)
                {
                    warnings.Add(localizer[TagLocalization.UNABLE_TO_MIGRATE_TAG_SOURCE_AND_DESTINATION_CANNOT_BE_THE_SAME, destinationTag.Name]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.TagID });
                }

                if (destinationTag.IsDeleted || destinationTag.IsVirtual)
                {
                    warnings.Add(localizer[TagLocalization.UNABLE_TO_MIGRATE_TAG_DESTINATION_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO, destinationTag.Name]);
                    TempData[Alerts.TempData.WARNING] = warnings;
                    return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.TagID });
                }

                await tagUpdateService.MigrateTag(sourceTag, destinationTag, migrationModel.ConvertMigratedTagToDestinationAlias, migrationModel.RedirectMigratedAliasesToDestination);

                TempData[Alerts.TempData.SUCCESS] = string.Format("Tag {0} was successfully migrated to {1}.", sourceTag.Name, destinationTag.Name);
                return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = destinationTag.TagID });
            }
            catch (Exception ex) when (ex is TagNotFoundException || ex is TagNotViewableException || ex is TagUnableToViewParentTagTypeException)
            {
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_MIGRATE_TAG_DESTINATION_DOES_NOT_EXIST, migrationModel.DestinationTagName].ToString();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to migrate tag. {{ migrationModel:{0} }}", migrationModel.TagID));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_MIRGRATE_TAG, migrationModel.TagID].ToString();
            }

            return RedirectToAction(Routing.Controllers.Tag.Routes.SHOW, Routing.Controllers.Tag.CONTROLLER, new { area = Routing.Areas.GENERAL, id = migrationModel.TagID });
        }

        public async Task<Tag> GetTagHelperAsync(Guid id, bool bypassCache)
        {
            Tag tag = (Tag)HttpContext.Items[FilterIdentifiers.TAG_FROM_FILTER];
            if (tag == null)
            {
                tag = await tagRetrievalService.FindTag(id, User, bypassCache);
            }

            return tag;
        }
    }
}