﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Areas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public sealed class TypeAheadController : ControllerBase
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagAliasService tagAliasService;

        private readonly int minimumInputLength;
        private readonly int resultsReturnedLength;

        public TypeAheadController(ITagRetrievalService tagRetrievalService, ITagTypeRetrievalService tagTypeRetrievalService,
            ITagAliasService tagAliasService, IOptions<ManicNetSettings> settings)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagAliasService = tagAliasService;

            this.minimumInputLength = settings.Value.TypeAheadSettings.MinimumInputLength;
            this.resultsReturnedLength = settings.Value.TypeAheadSettings.ResultsReturnedLength;
        }

        /// <summary>
        /// Takes an input string and provides type-ahead functionality for matching tags.
        /// </summary>
        /// <param name="searchString">The string fragment for used to search for matching tags.</param>
        /// <param name="isSearch">Determine whether or not the excludes symbol "-" is valid.</param>
        /// <param name="isVirtual">Determine whether or the isVirtual property of the tag should be checked against.</param>
        /// <param name="workType">The work type that the tags must be eligible for.</param>
        /// <returns></returns>
        [Route("")]
        [HttpGet]
        public async Task<List<String>> Get(string searchString, bool? isSearch, bool? isVirtual = null, Guid? tagType = null)
        {
            List<string> result = new List<string>();

            try
            {
                if (string.IsNullOrWhiteSpace(searchString))
                {
                    return result;
                }

                searchString = searchString.Trim();

                if (searchString.Length < minimumInputLength)
                {
                    return result;
                }

                bool searchStringIsExcluding = searchString.StartsWith("-");

                ExplodedTagFragment fragment = TagInputHelper.ParseInputFragment(searchString);

                List<TagType> tagTypes = await HandleTagTypesAsync(fragment, tagType);
                List<Tag> tags = await HandleTagsAsync(fragment, tagTypes, isVirtual);
                List<TagAlias> tagAliases = await HandleTagAliasesAsync(fragment, tagTypes, isVirtual, tags.Count);

                List<string> tagResult = tags.Select(t => ((isSearch.HasValue && isSearch.Value && searchStringIsExcluding) ? "-" : string.Empty) + tagTypes.Where(tt => tt.TagTypeID == t.TagTypeID).FirstOrDefault().Name + ":" + t.Name).ToList();
                List<string> tagAliasResult = tagAliases.Select(t => ((isSearch.HasValue && isSearch.Value && searchStringIsExcluding) ? "-" : string.Empty) + tagTypes.Where(tt => tt.TagTypeID == t.Tag.TagTypeID).FirstOrDefault().Name + ":" + t.Name).ToList();

                result = tagResult.Concat(tagAliasResult).OrderBy(t => t).ToList();
            }
            catch (Exception) { } //Swallow the exception

            return result;
        }

        private async Task<List<Tag>> HandleTagsAsync(ExplodedTagFragment fragment, List<TagType> tagTypes, bool? isVirtual)
        {
            List<Tag> tags = await tagRetrievalService.GetActiveTags();

            tags = tags.Where(t => tagTypes.Select(tt => tt.TagTypeID).ToList().Contains(t.TagTypeID)).ToList();

            if (isVirtual.HasValue)
            {
                tags = tags.Where(t => t.IsVirtual == isVirtual).ToList();
            }

            tags = tags.Where(t => t.Name.Contains(fragment.tagName, StringComparison.CurrentCultureIgnoreCase)).OrderBy(t => t.Name).Take(resultsReturnedLength).ToList();

            return tags;
        }

        private async Task<List<TagType>> HandleTagTypesAsync(ExplodedTagFragment fragment, Guid? tagType)
        {
            List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();

            if (tagType.HasValue)
            {
                tagTypes = tagTypes.Where(tt => tt.TagTypeID == tagType.Value).ToList();
            }

            if (!string.IsNullOrWhiteSpace(fragment.tagType))
            {
                tagTypes = tagTypes.Where(tt => tt.Name.Contains(fragment.tagType, StringComparison.CurrentCultureIgnoreCase)).ToList();
            }

            return tagTypes;
        }

        private async Task<List<TagAlias>> HandleTagAliasesAsync(ExplodedTagFragment fragment, List<TagType> tagTypes, bool? isVirtual, int tagsCount)
        {
            List<TagAlias> tagAliases = await tagAliasService.GetActiveTagAliases();

            tagAliases = tagAliases.Where(t => tagTypes.Select(tt => tt.TagTypeID).ToList().Contains(t.Tag.TagTypeID)).ToList();

            if (isVirtual.HasValue)
            {
                tagAliases = tagAliases.Where(t => t.Tag.IsVirtual == isVirtual).ToList();
            }

            tagAliases = tagAliases.Where(t => t.Name.Contains(fragment.tagName, StringComparison.CurrentCultureIgnoreCase)).OrderBy(t => t.Name).Take(resultsReturnedLength - tagsCount).ToList();

            return tagAliases;
        }
    }
}