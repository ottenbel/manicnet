﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Controllers
{
    [Route("[controller]")]
    public sealed class TagController : Controller
    {
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly IStringLocalizer<TagLocalization> localizer;

        private readonly string path = "ManicNet.Web.TagController";

        public TagController(ITagRetrievalService tagRetrievalService, ITagTypeRetrievalService tagTypeRetrievalService,
            IOptions<ManicNetSettings> settings, IMapper mapper, IManicNetLoggerService logger, IStringLocalizer<TagLocalization> localizer)
        {
            this.tagRetrievalService = tagRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.settings = settings;
            this.mapper = mapper;
            this.logger = logger;
            this.localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber, string search)
        {
            const string functionIdentifier = "Index(pageNumber, search)";

            TagIndexViewModel model = new TagIndexViewModel(Routing.Controllers.Tag.Routes.INDEX, search);
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                if (!(string.IsNullOrWhiteSpace(search)))
                {
                    search = search.Trim();
                    tags = tags.Where(t => t.Name.Contains(search)).ToList();
                }

                List<TagViewModel> tagsView = mapper.Map<List<TagViewModel>>(tags, opt => { });
                model.Tags = await tagsView.OrderBy(x => x.Name)
                    .ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);

                model.HasDeleted = await tagRetrievalService.DoDeletedTagTypesExist(User);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of tags. {{ pageNumber:{0}, search:{1} }}", pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_RETRIEVE_LIST_OF_TAGS].ToString();
            }

            return View(model);
        }

        [CheckTagExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            TagViewModel model = new TagViewModel();

            try
            {
                List<Tag> tags = await tagRetrievalService.GetActiveTags();
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();

                Tag tag = (Tag)HttpContext.Items[FilterIdentifiers.TAG_FROM_FILTER];
                if (tag == null)
                {
                    tag = await tagRetrievalService.FindTag(id, User);
                }

                model = mapper.Map<TagViewModel>(tag, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show tag {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_TAG].ToString();
            }

            return View(model);
        }
    }
}