﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Controllers
{
    [Route("[controller]")]
    public sealed class TagTypeController : Controller
    {
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IMapper mapper;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<TagTypeLocalization> localizer;

        private readonly string path = "ManicNet.Web.TagTypeController";

        public TagTypeController(ITagTypeRetrievalService tagTypeRetrievalService, IManicNetLoggerService logger, IMapper mapper,
            IOptions<ManicNetSettings> settings, IStringLocalizer<TagTypeLocalization> localizer)
        {
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.logger = logger;
            this.mapper = mapper;
            this.settings = settings;
            this.localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber)
        {
            const string functionIdentifier = "Index(pageNumber)";

            TagTypeIndexViewModel model = new TagTypeIndexViewModel();
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);
            model.Path = Routing.Controllers.TagType.Routes.INDEX;

            try
            {
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<TagTypeViewModel> tagTypesView = mapper.Map<List<TagTypeViewModel>>(tagTypes);
                model.TagTypes = await tagTypesView.OrderBy(x => x.Priority)
                    .ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                model.HasDeleted = await tagTypeRetrievalService.DoDeletedTagTypesExistAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of tag types. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_RETRIEVE_LIST_OF_TAG_TYPES].ToString();
            }

            return View(model);
        }

        [CheckTagTypeExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            TagTypeViewModel model = new TagTypeViewModel();

            try
            {
                TagType tagType = (TagType)HttpContext.Items[FilterIdentifiers.TAG_TYPE_FROM_FILTER];
                if (tagType == null)
                {
                    tagType = await tagTypeRetrievalService.FindTagTypeAsync(id, User);
                }

                model = mapper.Map<TagTypeViewModel>(tagType);

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show tag type {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[TagTypeLocalization.UNABLE_TO_LOAD_TAG_TYPE_INFORMATION].ToString();
            }

            return View(model);
        }
    }
}