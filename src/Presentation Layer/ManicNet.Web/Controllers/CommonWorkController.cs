﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace ManicNet.Web.Controllers
{
    public class CommonWorkController : Controller
    {
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IChapterRetrievalService chapterRetrievalService;

        public CommonWorkController(IWorkRetrievalService workRetrievalService, IVolumeRetrievalService volumeRetrievalService, IChapterRetrievalService chapterRetrievalService)
        {
            this.workRetrievalService = workRetrievalService;
            this.volumeRetrievalService = volumeRetrievalService;
            this.chapterRetrievalService = chapterRetrievalService;
        }

        protected async Task<Work> GetWorkHelperAsync(Guid id, bool bypassCache)
        {
            Work work = (Work)HttpContext.Items[FilterIdentifiers.WORK_FROM_FILTER];
            if (work == null)
            {
                work = await workRetrievalService.FindWorkAsync(id, User, bypassCache);
            }

            return work;
        }

        protected async Task<Volume> GetVolumeHelperAsync(Guid id, bool bypassCache)
        {
            Volume volume = (Volume)HttpContext.Items[FilterIdentifiers.VOLUME_FROM_FILTER];
            if (volume == null)
            {
                volume = await volumeRetrievalService.FindVolume(id, User, bypassCache);
            }

            return volume;
        }

        protected async Task<Chapter> GetChapterHelperAsync(Guid id, bool bypassCache)
        {
            Chapter chapter = (Chapter)HttpContext.Items[FilterIdentifiers.CHAPTER_FROM_FILTER];
            if (chapter == null)
            {
                chapter = await chapterRetrievalService.FindChapter(id, User, bypassCache);
            }

            return chapter;
        }
    }
}