﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Web.Controllers
{
    [Route("[controller]")]
    public sealed class ChapterController : Controller
    {
        private readonly IChapterRetrievalService chapterRetrievalService;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IFileIOService fileIOService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<ChapterLocalization> localizer;

        private readonly string path = "ManicNet.Web.ChapterController";

        public ChapterController(IChapterRetrievalService chapterRetrievalService, IMapper mapper, IManicNetLoggerService logger,
            ITagTypeRetrievalService tagTypeRetrievalService, ITagRetrievalService tagRetrievalService,
            IWorkRetrievalService workRetrievalService, IFileIOService fileIOService, IOptions<ManicNetSettings> settings,
            IStringLocalizer<ChapterLocalization> localizer)
        {
            this.chapterRetrievalService = chapterRetrievalService;
            this.mapper = mapper;
            this.logger = logger;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagRetrievalService = tagRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.fileIOService = fileIOService;
            this.settings = settings;
            this.localizer = localizer;
        }

        [CheckChapterExistenceIncludeCacheFilter]
        [Route("overview/{id}")]
        [HttpGet]
        public async Task<IActionResult> Overview(Guid id)
        {
            const string functionIdentifier = "Overview(id)";

            ChapterViewModel model = new ChapterViewModel();

            try
            {
                Chapter chapter = (Chapter)HttpContext.Items[FilterIdentifiers.CHAPTER_FROM_FILTER];
                if (chapter == null)
                {
                    chapter = await chapterRetrievalService.FindChapter(id, User);
                }

                Work work = await workRetrievalService.FindWorkAsync(chapter.Volume.WorkID, User);

                Guid? previousChapterID = work.Volumes.Where(v => !v.IsDeleted).SelectMany(v => v.Chapters).Where(c => !c.IsDeleted && c.Number < chapter.Number).OrderByDescending(c => c.Number).FirstOrDefault()?.ChapterID;
                Guid? nextChapterID = work.Volumes.Where(v => !v.IsDeleted).SelectMany(v => v.Chapters).Where(c => !c.IsDeleted && c.Number > chapter.Number).OrderBy(c => c.Number).FirstOrDefault()?.ChapterID;

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<ChapterViewModel>(chapter, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                    opt.Items[ModelMapping.PREVIOUS_CHAPTER_ID] = previousChapterID;
                    opt.Items[ModelMapping.NEXT_CHAPTER_ID] = nextChapterID;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show chapter {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_CHAPTER].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.User.DOWNLOAD_CHAPTER)]
        [CheckChapterExistenceIncludeCacheFilter]
        [Route("download/{id}")]
        [HttpPost]
        public async Task<IActionResult> Download(Guid id)
        {
            const string functionIdentifier = "Download(id)";

            try
            {
                Chapter chapter = (Chapter)HttpContext.Items[FilterIdentifiers.CHAPTER_FROM_FILTER];
                if (chapter == null)
                {
                    chapter = await chapterRetrievalService.FindChapter(id, User);
                }

                Work work = await workRetrievalService.FindWorkAsync(chapter.Volume.WorkID, User);

                string fileName = string.Format("[{0}] {1} - Chapter {2}{3}.zip", settings.Value.Metadata.SiteName, work.Title, chapter.Number, !string.IsNullOrWhiteSpace(chapter.Title) ? string.Format(" - {0}", chapter.Title) : string.Empty);
                byte[] file = await fileIOService.GetDownloadChapter(work.Title, chapter);

                return File(file, "application/zip", fileName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to download chapter {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_DOWNLOAD_CHAPTER].ToString();
            }

            return RedirectToAction(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = id });
        }

        [CheckChapterExistenceIncludeCacheFilter]
        [Route("viewer/{id}/{page:int?}")]
        [HttpGet]
        public async Task<IActionResult> Viewer(Guid id, int page = 0)
        {
            const string functionIdentifier = "Viewer(id, page)";

            ChapterViewModel model = new ChapterViewModel();

            try
            {
                Chapter chapter = (Chapter)HttpContext.Items[FilterIdentifiers.CHAPTER_FROM_FILTER];
                if (chapter == null)
                {
                    chapter = await chapterRetrievalService.FindChapter(id, User);
                }

                Work work = await workRetrievalService.FindWorkAsync(chapter.Volume.WorkID, User);

                Guid? previousChapterID = work.Volumes.Where(v => !v.IsDeleted).SelectMany(v => v.Chapters).Where(c => !c.IsDeleted && c.Number < chapter.Number).OrderByDescending(c => c.Number).FirstOrDefault()?.ChapterID;
                int? lastPageOfPreviousChapter = null;
                Guid? nextChapterID = work.Volumes.Where(v => !v.IsDeleted).SelectMany(v => v.Chapters).Where(c => !c.IsDeleted && c.Number > chapter.Number).OrderBy(c => c.Number).FirstOrDefault()?.ChapterID;

                if (previousChapterID.HasValue)
                {
                    Chapter nextChapter = await chapterRetrievalService.FindChapter(previousChapterID.Value, User);
                    lastPageOfPreviousChapter = nextChapter.Pages.OrderByDescending(p => p.Number).FirstOrDefault().Number - 1;
                }

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<ChapterViewModel>(chapter, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                    opt.Items[ModelMapping.PREVIOUS_CHAPTER_ID] = previousChapterID;
                    opt.Items[ModelMapping.NEXT_CHAPTER_ID] = nextChapterID;
                    opt.Items[ModelMapping.LAST_PAGE_OF_PREVIOUS_CHAPTER] = lastPageOfPreviousChapter;
                    opt.Items[ModelMapping.CURRENT_PAGE] = page;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show viewer. {{ id:{0}, page:{1} }}", id, page));
                TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_CHAPTER].ToString();
            }

            return View(model);
        }
    }
}