﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Web.Controllers
{
    [Route("[controller]")]
    public sealed class VolumeController : Controller
    {
        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IFileIOService fileIOService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IStringLocalizer<VolumeLocalization> localizer;

        private readonly string path = "ManicNet.Web.VolumeController";

        public VolumeController(IVolumeRetrievalService volumeRetrievalService, IMapper mapper, IManicNetLoggerService logger,
            ITagTypeRetrievalService tagTypeRetrievalService, ITagRetrievalService tagRetrievalService, IFileIOService fileIOService,
            IOptions<ManicNetSettings> settings, IStringLocalizer<VolumeLocalization> localizer)
        {
            this.volumeRetrievalService = volumeRetrievalService;
            this.mapper = mapper;
            this.logger = logger;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagRetrievalService = tagRetrievalService;
            this.fileIOService = fileIOService;
            this.settings = settings;
            this.localizer = localizer;
        }

        [CheckVolumeExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            VolumeViewModel model = new VolumeViewModel();

            try
            {
                Volume volume = (Volume)HttpContext.Items[FilterIdentifiers.VOLUME_FROM_FILTER];
                if (volume == null)
                {
                    volume = await volumeRetrievalService.FindVolume(id, User);
                }

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<VolumeViewModel>(volume, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show volume {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_LOAD_VOLUME_INFORMATION].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.User.DOWNLOAD_VOLUME)]
        [CheckVolumeExistenceIncludeCacheFilter]
        [Route("download/{id}")]
        [HttpPost]
        public async Task<IActionResult> Download(Guid id)
        {
            const string functionIdentifier = "Download(id)";

            try
            {
                Volume volume = await volumeRetrievalService.FindVolumeForDownload(id, User);

                string fileName = string.Format("[{0}] {1} - Volume {2}{3}.zip", settings.Value.Metadata.SiteName, volume.Work.Title, volume.Number, !string.IsNullOrWhiteSpace(volume.Title) ? string.Format(" - {0}", volume.Title) : string.Empty);
                byte[] file = await fileIOService.GetDownloadVolume(volume.Work.Title, volume);

                return File(file, "application/zip", fileName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to download volume {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.UNABLE_TO_DOWNLOAD_VOLUME].ToString();
            }

            return RedirectToAction(Routing.Controllers.Volume.Routes.SHOW, Routing.Controllers.Volume.CONTROLLER, new { area = Routing.Areas.GENERAL, id = id });
        }
    }
}