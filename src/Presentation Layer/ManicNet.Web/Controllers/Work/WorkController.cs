﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using X.PagedList;

namespace ManicNet.Web.Controllers.Works
{
    [Route("[controller]")]
    public sealed class WorkController : CommonWorkController
    {
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IFileIOService fileIOService;
        private readonly ManicNetUserManager userManager;
        private readonly IStringLocalizer<WorkLocalization> localizer;

        private readonly string path = "ManicNet.Web.WorkController";

        public WorkController(IWorkRetrievalService workRetrievalService, IOptions<ManicNetSettings> settings, IMapper mapper,
            IManicNetLoggerService logger, ITagTypeRetrievalService tagTypeRetrievalService, ITagRetrievalService tagRetrievalService,
            IFileIOService fileIOService, ManicNetUserManager userManager, IVolumeRetrievalService volumeRetrievalService,
            IChapterRetrievalService chapterRetrievalService, IStringLocalizer<WorkLocalization> localizer)
            : base(workRetrievalService, volumeRetrievalService, chapterRetrievalService)
        {
            this.workRetrievalService = workRetrievalService;
            this.settings = settings;
            this.mapper = mapper;
            this.logger = logger;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.tagRetrievalService = tagRetrievalService;
            this.fileIOService = fileIOService;
            this.userManager = userManager;
            this.localizer = localizer;
        }

        [Route("/")]
        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber, string search)
        {
            const string functionIdentifier = "Index(pageNumber, search)";

            WorkIndexViewModel model = new WorkIndexViewModel(@Routing.Controllers.Work.Routes.INDEX, search);
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<Work> works = await workRetrievalService.GetActiveWorksBySearch(search, User.GetUserID());
                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();
                bool hasDeletedWorks = await workRetrievalService.DoDeletedWorksExist();

                List<WorkSummaryViewModel> worksView = mapper.Map<List<WorkSummaryViewModel>>(works, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                model.Works = await worksView.OrderByDescending(w => w.SortabledDate).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                model.HasDeleted = hasDeletedWorks;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of works. {{ pageNumber:{0}, search:{1} }}", pageNumber, search));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_RETRIEVE_LIST_OF_WORKS].ToString();
            }

            return View(model);
        }

        [CheckWorkExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            WorkViewModel model = new WorkViewModel();

            try
            {
                Work work = await GetWorkHelperAsync(id, false);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<WorkViewModel>(work, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show work {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_WORK].ToString();
            }

            return View(model);
        }

        [Authorize(Policies.User.DOWNLOAD_WORK)]
        [CheckWorkExistenceIncludeCacheFilter]
        [Route("download/{id}")]
        [HttpPost]
        public async Task<IActionResult> Download(Guid id)
        {
            const string functionIdentifier = "Download(id)";

            try
            {
                Work work = await workRetrievalService.FindWorkForDownload(id, User);

                string fileName = string.Format("[{0}] {1}.zip", settings.Value.Metadata.SiteName, work.Title);
                byte[] file = await fileIOService.GetDownloadWork(work);

                return File(file, "application/zip", fileName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to download work. {{ id:{0} }}", id));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_DOWNLOAD_WORK].ToString();
            }

            return RedirectToAction(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = id });
        }

        [Route("random")]
        [HttpGet]
        public async Task<IActionResult> Random(string search)
        {
            const string functionIdentifier = "Random(search)";

            WorkViewModel model = new WorkViewModel();

            try
            {
                List<Work> works = await workRetrievalService.GetActiveWorksBySearch(search, User.GetUserID());

                if (!works.Any())
                {
                    return View();
                }

                Work randomWork = works.ElementAt(RandomNumberGenerator.GetInt32(works.Count));

                Work work = await workRetrievalService.FindWorkAsync(randomWork.WorkID, User);

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<WorkViewModel>(work, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });

                return View(Routing.Controllers.Work.Routes.SHOW, model);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show random work. {{ search:{0} }}", search));
                TempData[Alerts.TempData.ERRROR] = localizer[WorkLocalization.UNABLE_TO_LOAD_INFORMATION_FOR_RANDOM_WORK].ToString();
            }

            return View(model);
        }

        //Based on code from https://mitchelsellers.com/blog/article/creating-an-rss-feed-in-asp-net-core-3-0
        [AllowAnonymous]
        [ValidateRSSFilter(Order = 1)]
        [Route("rss")]
        [HttpGet]
        public async Task<IActionResult> RSS(string search, bool newOnly, Guid? rssKey)
        {
            const string functionIdentifier = "Show(search, newOnly, rssKey)";

            try
            {
                string title = newOnly ? localizer[WorkLocalization.RECENTLY_ADDED_WORK_TYPES] : localizer[WorkLocalization.RECENTLY_UPDATED_WORK_TYPES];

                string description;

                if (newOnly)
                {
                    description = string.IsNullOrWhiteSpace(search) ? localizer[WorkLocalization.RECENTLY_ADDED_WORK_TYPES_DESCRIPTION, settings.Value.Metadata.SiteName] : localizer[WorkLocalization.RECENTLY_ADDED_WORK_TYPES_DESCRIPTION_WITH_SEARCH, settings.Value.Metadata.SiteName];
                }
                else
                {
                    description = string.IsNullOrWhiteSpace(search) ? localizer[WorkLocalization.RECENTLY_UPDATED_WORK_TYPES_DESCRIPTION, settings.Value.Metadata.SiteName] : localizer[WorkLocalization.RECENTLY_UPDATED_WORK_TYPES_DESCRIPTION_WITH_SEARCH, settings.Value.Metadata.SiteName];
                }

                SyndicationFeed feed = new SyndicationFeed(title, description, new Uri(Url.Action(Routing.Controllers.Work.Routes.INDEX, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, search = search }, this.Request.Scheme)), "RSSUrl", DateTime.UtcNow)
                {
                    Copyright = new TextSyndicationContent(localizer[WorkLocalization.WORK_COPYRIGHT, settings.Value.Metadata.SiteName, settings.Value.Metadata.SiteCopyright])
                };

                List<SyndicationItem> items = new List<SyndicationItem>();

                ManicNetUser user = userManager.GetUserFromRSSKey(rssKey);
                List<Work> works = await workRetrievalService.GetActiveWorksBySearch(search, user.Id);
                List<Work> postings = new List<Work>();

                if (newOnly)
                {
                    postings = works.OrderByDescending(w => w.CreatedDate).Take(settings.Value.PageLength).ToList();
                }
                else
                {
                    newOnly = false;
                    postings = works.OrderByDescending(w => w.SortableDate).Take(settings.Value.PageLength).ToList();
                }

                foreach (Work post in postings)
                {
                    SyndicationItem item = new SyndicationItem()
                    {
                        Title = new TextSyndicationContent(post.Title, TextSyndicationContentKind.Plaintext),
                        Summary = new TextSyndicationContent(post.ShortDescription, TextSyndicationContentKind.Plaintext),
                        Id = post.WorkID.ToString(),
                        PublishDate = newOnly ? post.CreatedDate : post.SortableDate,
                    };

                    item.Links.Add(new SyndicationLink(new Uri(Url.Action(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = post.WorkID }, HttpContext.Request.Scheme))));

                    items.Add(item);
                }

                feed.Items = items;
                var writerSettings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    NewLineHandling = NewLineHandling.Entitize,
                    NewLineOnAttributes = true,
                    Indent = true
                };
                using (var stream = new MemoryStream())
                {
                    using (var xmlWriter = XmlWriter.Create(stream, writerSettings))
                    {
                        var rssFormatter = new Rss20FeedFormatter(feed, false);
                        rssFormatter.WriteTo(xmlWriter);
                        xmlWriter.Flush();
                    }
                    return File(stream.ToArray(), "application/rss+xml; charset=utf-8");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to generate RSS feed. {{ search:{0}, newOnly:{1}}}.", search, newOnly));
                throw;
            }
        }

        //Based on code from https://mitchelsellers.com/blog/article/creating-an-rss-feed-in-asp-net-core-3-0
        [AllowAnonymous]
        [ValidateRSSFilter(Order = 1)]
        [CheckWorkExistenceIncludeCacheFilter(Order = 2)]
        [Route("rss/{id}")]
        [HttpGet]
        public async Task<IActionResult> RSS(Guid id, Guid? rssKey)
        {
            const string functionIdentifier = "RSS(id, rssKey)";

            try
            {
                Work work = await GetWorkHelperAsync(id, false);

                string title = work.Title;
                string description = localizer[WorkLocalization.WORK_CHAPTER_UPDATE_FEED, settings.Value.Metadata.SiteName, work.Title];

                SyndicationFeed feed = new SyndicationFeed(title, description, new Uri(Url.Action(Routing.Controllers.Work.Routes.SHOW, Routing.Controllers.Work.CONTROLLER, new { area = Routing.Areas.GENERAL, id = id }, this.Request.Scheme)), "RSSUrl", DateTime.UtcNow)
                {
                    Copyright = new TextSyndicationContent(localizer[WorkLocalization.WORK_COPYRIGHT, settings.Value.Metadata.SiteName, settings.Value.Metadata.SiteCopyright])
                };

                List<SyndicationItem> items = new List<SyndicationItem>();

                List<Chapter> postings = work.Volumes.SelectMany(v => v.Chapters).Where(c => c.IsDeleted == false).OrderByDescending(c => c.CreatedDate).Take(settings.Value.PageLength).ToList();

                foreach (Chapter post in postings)
                {
                    string postTitle = string.IsNullOrWhiteSpace(post.Title) ? localizer[WorkLocalization.WORK_FEED_CHAPTER, work.Title, post.Number] : localizer[WorkLocalization.WORK_FEED_CHAPTER_WITH_TITLE, work.Title, post.Number, post.Title];

                    SyndicationItem item = new SyndicationItem()
                    {

                        Title = new TextSyndicationContent(postTitle, TextSyndicationContentKind.Plaintext),
                        Summary = new TextSyndicationContent(post.ShortDescription, TextSyndicationContentKind.Plaintext),
                        Id = post.ChapterID.ToString(),
                        PublishDate = post.CreatedDate,
                    };

                    item.Links.Add(new SyndicationLink(new Uri(Url.Action(Routing.Controllers.Chapter.Routes.OVERVIEW, Routing.Controllers.Chapter.CONTROLLER, new { area = Routing.Areas.GENERAL, id = post.ChapterID }, HttpContext.Request.Scheme))));

                    items.Add(item);
                }

                feed.Items = items;
                var writerSettings = new XmlWriterSettings
                {
                    Encoding = Encoding.UTF8,
                    NewLineHandling = NewLineHandling.Entitize,
                    NewLineOnAttributes = true,
                    Indent = true
                };
                using (var stream = new MemoryStream())
                {
                    using (var xmlWriter = XmlWriter.Create(stream, writerSettings))
                    {
                        var rssFormatter = new Rss20FeedFormatter(feed, false);
                        rssFormatter.WriteTo(xmlWriter);
                        xmlWriter.Flush();
                    }
                    return File(stream.ToArray(), "application/rss+xml; charset=utf-8");
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to generate RSS feed. {{ id:{0} }}.", id));
                throw;
            }
        }
    }
}