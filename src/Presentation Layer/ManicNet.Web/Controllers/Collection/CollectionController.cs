﻿using AutoMapper;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Infrastructure.Filters;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ManicNet.Web.Controllers
{
    [Route("[controller]")]
    public sealed class CollectionController : Controller
    {
        private readonly ICollectionRetrievalService collectionRetrievalService;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IMapper mapper;
        private readonly IManicNetLoggerService logger;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IStringLocalizer<CollectionLocalization> localizer;

        private readonly string path = "ManicNet.Web.CollectionController";

        public CollectionController(ICollectionRetrievalService collectionRetrievalService, IOptions<ManicNetSettings> settings, IMapper mapper, IManicNetLoggerService logger, ITagRetrievalService tagRetrievalService, ITagTypeRetrievalService tagTypeRetrievalService, IStringLocalizer<CollectionLocalization> localizer)
        {
            this.collectionRetrievalService = collectionRetrievalService;
            this.settings = settings;
            this.settings = settings;
            this.mapper = mapper;
            this.logger = logger;
            this.tagRetrievalService = tagRetrievalService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.localizer = localizer;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? pageNumber)
        {
            const string functionIdentifier = "Index(pageNumber)";

            CollectionIndexViewModel model = new CollectionIndexViewModel(Routing.Controllers.Collection.Routes.INDEX);
            PaginationModel paginationModel = new PaginationModel(pageNumber, settings.Value.PageLength);

            try
            {
                List<Collection> collections = await collectionRetrievalService.GetActiveCollections();
                bool hasDeletedCollections = await collectionRetrievalService.DoDeletedCollectionsExist();

                List<CollectionSummaryViewModel> collectionsView = mapper.Map<List<CollectionSummaryViewModel>>(collections, opt => { });

                model.Collections = await collectionsView.OrderByDescending(c => c.Title).ToPagedListAsync(paginationModel.PageNumber, paginationModel.PageLength);
                model.HasDeleted = hasDeletedCollections;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to populate list of collections. {{ pageNumber:{0} }}", pageNumber));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_RETRIEVE_LIST_OF_COLLECTIONS].ToString();
            }

            return View(model);
        }

        [CheckCollectionExistenceIncludeCacheFilter]
        [Route("show/{id}")]
        [HttpGet]
        public async Task<IActionResult> Show(Guid id)
        {
            const string functionIdentifier = "Show(id)";

            CollectionViewModel model = new CollectionViewModel();

            try
            {
                Collection collection = (Collection)HttpContext.Items[FilterIdentifiers.COLLECTION_FROM_FILTER];
                if (collection == null)
                {
                    collection = await collectionRetrievalService.FindCollection(id, User);
                }

                List<TagType> tagTypes = await tagTypeRetrievalService.GetActiveTagTypes();
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                model = mapper.Map<CollectionViewModel>(collection, opt =>
                {
                    opt.Items[ModelMapping.TAGS] = tags;
                    opt.Items[ModelMapping.TAG_TYPES] = tagTypes;
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Unable to load information to show collection {{ id:{0} }}.", id));
                TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.UNABLE_TO_LOAD_COLLECTION_INFORMATION].ToString();
            }

            return View(model);
        }
    }
}
