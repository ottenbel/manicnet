﻿using ManicNet.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Web;

namespace ManicNet.Web.TagHelpers
{
    public sealed class TagViewTagHelper : TagHelper
    {
        public TagViewModel Info { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string CSSString = string.Format("tag {0}-tag {0}-type-{1} tag-{2}", Info.IsPrimary ? "primary" : "secondary", HttpUtility.HtmlEncode(Info.TagType.Name.Replace(' ', '_')), HttpUtility.HtmlEncode(Info.Name.Replace(' ', '_')));

            output.Content.SetHtmlContent(
                $@"<a href='/Tag/show/{Info.TagID}'><span class='{CSSString}' title='{HttpUtility.HtmlEncode(Info.ShortDescription)}'>{HttpUtility.HtmlEncode(Info.Name)} ({Info.Count})</span></a>"
            );
        }
    }
}
