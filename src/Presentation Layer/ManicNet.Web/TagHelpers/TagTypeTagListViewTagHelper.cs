﻿using ManicNet.Models;
using Microsoft.AspNetCore.Razor.TagHelpers;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManicNet.Web.TagHelpers
{
    public sealed class TagTypeTagListViewTagHelper : TagHelper
    {
        public List<TagTypeTagListViewModel> Info { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            string outputString = "<div class='row'>";

            foreach (var listElement in Info.OrderBy(le => le.TagType.Priority).ToList())
            {
                outputString += $@"<div class='col-xs-12 col-sm-2'><strong>{listElement.TagType.Name}:</strong></div><div class='col-xs-12 col-sm-10'>";

                foreach (TagViewModel tag in listElement.Tags)
                {
                    //TODO: See if I can use the tag-view tag helper tag instead of duplicating the functionality here
                    string CSSString = string.Format("tag {0}-tag {0}-type-{1} tag-{2}", tag.IsPrimary ? "primary" : "secondary", HttpUtility.HtmlEncode(listElement.TagType.Name.Replace(' ', '_')), HttpUtility.HtmlEncode(tag.Name.Replace(' ', '_')));
                    outputString += $@"<a href='/Tag/show/{tag.TagID}'><span class='{CSSString}' title='{HttpUtility.HtmlEncode(tag.ShortDescription)}'>{HttpUtility.HtmlEncode(tag.Name)} ({tag.Count})</span></a>";
                }
                outputString += "</div>";
            }

            outputString += "</div>";

            output.Content.SetHtmlContent(outputString);
        }
    }
}
