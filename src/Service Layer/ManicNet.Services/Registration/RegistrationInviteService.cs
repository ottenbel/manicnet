﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class RegistrationInviteService : IRegistrationInviteService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IRegistrationInviteRepository registrationInviteRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.RegistrationInviteService";

        public RegistrationInviteService(IManicNetUnitOfWork unitOfWork, IRegistrationInviteRepository registrationInviteRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.registrationInviteRepository = registrationInviteRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
        }

        public async Task CreateRegistrationInvite(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "CreateRegistrationInvite(registrationInvite)";

            try
            {
                RegistrationInviteCacheCleanup registrationInviteCacheCleanup = new RegistrationInviteCacheCleanup();
                registrationInviteRepository.Insert(registrationInvite);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForRegistrationInvite(registrationInviteCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }

        public async Task<RegistrationInvite> FindRegistrationInvite(string email)
        {
            const string functionIdentifier = "FindRegistrationInvite(email)";

            try
            {
                RegistrationInvite registrationInvite = await registrationInviteRepository.FindAsync(email);
                return registrationInvite;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0} }}", email));
                throw;
            }
        }

        public async Task<RegistrationInvite> FindRegistrationInvite(Guid id)
        {
            const string functionIdentifier = "FindRegistrationInvite(id)";

            try
            {
                RegistrationInvite registrationInvite = await registrationInviteRepository.FindAsync(id);

                if (registrationInvite == null)
                {
                    throw new RegistrationInviteNotFoundException(string.Format("Registration invite with ID {0} not found", id));
                }

                return registrationInvite;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<List<RegistrationInvite>> GetPendingInvites()
        {
            const string functionIdentifier = "GetPendingInvites()";

            List<RegistrationInvite> invites = new List<RegistrationInvite>();

            try
            {
                invites = (await registrationInviteRepository.AllAsync()).Where(i => i.Approved == false).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return invites;
        }

        public async Task<List<RegistrationInvite>> GetActiveInvites()
        {
            const string functionIdentifier = "GetActiveInvites()";

            List<RegistrationInvite> invites = new List<RegistrationInvite>();

            try
            {
                invites = (await registrationInviteRepository.AllAsync()).Where(i => i.Approved == true && i.Expires >= DateTime.UtcNow).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return invites;
        }

        public async Task<List<RegistrationInvite>> GetExpiredInvites()
        {
            const string functionIdentifier = "GetExpiredInvites()";

            List<RegistrationInvite> invites = new List<RegistrationInvite>();

            try
            {
                invites = (await registrationInviteRepository.AllAsync()).Where(i => i.Expires < DateTime.UtcNow).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return invites;
        }

        public async Task UpdateRegistrationInvite(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "UpdateRegistrationInvite(registrationInvite)";

            try
            {
                RegistrationInviteCacheCleanup registrationInviteCacheCleanup = new RegistrationInviteCacheCleanup(registrationInvite);
                registrationInviteRepository.Update(registrationInvite);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForRegistrationInvite(registrationInviteCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }

        public async Task RemoveRegistrationInvite(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "RemoveRegistrationInvite(registrationInvite)";

            try
            {
                RegistrationInviteCacheCleanup registrationInviteCacheCleanup = new RegistrationInviteCacheCleanup(registrationInvite);
                registrationInviteRepository.Delete(registrationInvite);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForRegistrationInvite(registrationInviteCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }

        public async Task<bool> ValidateRegistrationInvite(string email, Guid inviteCode)
        {
            const string functionIdentifier = "ValidateRegistrationInvite(email, inviteCode)";

            bool isValid = false;

            try
            {
                RegistrationInvite registrationInvite = null;

                try
                {
                    registrationInvite = await FindRegistrationInvite(inviteCode);
                }
                catch (RegistrationInviteNotFoundException)
                {
                    return isValid;
                }

                if (registrationInvite.Approved && registrationInvite.Email == email && registrationInvite.Expires >= DateTime.UtcNow)
                {
                    isValid = true;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0}, inviteCode:{1} }}", email, inviteCode));
            }

            return isValid;
        }
    }
}
