﻿using ManicNet.Constants;
using ManicNet.Context;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ManicNet.Services
{
    public sealed class SeedingService : IHostedService
    {
        private readonly IServiceProvider serviceProvider;

        public SeedingService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await RunMigrations();
            await SeedRoles();
            await SeedCacheConfigurations();
            await GenerateDynamicTagCSS();
            await GenerateDynamicSocialLinkCSS();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private async Task RunMigrations()
        {
            using (var scope = serviceProvider.CreateScope())
            {
                var db = scope.ServiceProvider.GetService<ManicNetContext>();
                IOptions<ManicNetSettings> settings = scope.ServiceProvider.GetService<IOptions<ManicNetSettings>>();
                if (settings.Value.SeedingSettings.Migration)
                {
                    await db.Database.MigrateAsync();
                }
            }
        }

        private async Task SeedRoles()
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IOptions<ManicNetSettings> settings = scope.ServiceProvider.GetService<IOptions<ManicNetSettings>>();

                if (settings.Value.SeedingSettings.Roles)
                {
                    RoleManager<ManicNetRole> roleManager = scope.ServiceProvider.GetService<RoleManager<ManicNetRole>>();

                    await SeedOwnerAccount(roleManager);
                    await SeedUserAccount(roleManager);
                }
            }
        }

        private async Task SeedOwnerAccount(RoleManager<ManicNetRole> roleManager)
        {
            List<string> permissions = new List<string>()
                    .Concat(ManicNetPermissions.User.Permissions)
                    .Concat(ManicNetPermissions.Role.Permissions)
                    .Concat(ManicNetPermissions.TagType.Permissions)
                    .Concat(ManicNetPermissions.Tag.Permissions)
                    .Concat(ManicNetPermissions.RegistrationInvite.Permissions)
                    .Concat(ManicNetPermissions.Work.Permissions)
                    .Concat(ManicNetPermissions.Collection.Permissions)
                    .Concat(ManicNetPermissions.SiteConfiguration.Cache.Permissions)
                    .Concat(ManicNetPermissions.SocialLink.Permissions)
                    .ToList();

            ManicNetRole role = await roleManager.FindByIdAsync(Roles.DefaultOwnerID.ToString());

            if (role == null)
            {
                role = new ManicNetRole()
                {
                    Id = Roles.DefaultOwnerID,
                    Name = Roles.DefaultOwnerName,
                    Description = "A top level role assigned to the site owner"
                };

                await roleManager.CreateAsync(role);

                var claims = await roleManager.GetClaimsAsync(role);

                foreach (string permission in permissions)
                {
                    Claim claim = new Claim(ManicNetPermissions.PERMISSION_CLAIM_TYPE, permission);
                    await roleManager.AddClaimAsync(role, claim);
                }
            }
            else
            {
                var claims = await roleManager.GetClaimsAsync(role);

                foreach (string permission in permissions)
                {
                    if (!claims.Where(c => c.Value == permission).Any())
                    {
                        Claim claim = new Claim(ManicNetPermissions.PERMISSION_CLAIM_TYPE, permission);
                        await roleManager.AddClaimAsync(role, claim);
                    }
                }
            }
        }

        private async Task SeedUserAccount(RoleManager<ManicNetRole> roleManager)
        {
            ManicNetRole role = await roleManager.FindByIdAsync(Roles.DefaultUserID.ToString());

            if (role == null)
            {
                role = new ManicNetRole()
                {
                    Id = Roles.DefaultUserID,
                    Name = Roles.DefaultUserName,
                    Description = "The default role that is assigned to new user accounts.",
                    IsDefaultUser = true
                };

                await roleManager.CreateAsync(role);
            }
        }

        private async Task SeedCacheConfigurations()
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IOptions<ManicNetSettings> settings = scope.ServiceProvider.GetService<IOptions<ManicNetSettings>>();

                if (settings.Value.SeedingSettings.CacheConfiguration)
                {
                    var cacheConfigurationRetrievalService = scope.ServiceProvider.GetService<ICacheConfigurationRetrievalService>();
                    var cacheConfigurationUpdateService = scope.ServiceProvider.GetService<ICacheConfigurationUpdateService>();

                    List<CacheConfiguration> cacheConfigurationsToSeed = new List<CacheConfiguration>();
                    List<CacheConfiguration> knownCacheConfigurations = new List<CacheConfiguration>()
                    {
                        //Tag type
                        new CacheConfiguration(Caching.Configurations.Sections.TAG_TYPE, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.TAG_TYPE, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Tag
                        new CacheConfiguration(Caching.Configurations.Sections.TAG, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.TAG, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Tag Alias
                        new CacheConfiguration(Caching.Configurations.Sections.TAG_ALIAS, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.TAG_ALIAS, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Work
                        new CacheConfiguration(Caching.Configurations.Sections.WORK, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.WORK, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Volume
                        new CacheConfiguration(Caching.Configurations.Sections.VOLUME, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.VOLUME, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Chapter
                        new CacheConfiguration(Caching.Configurations.Sections.CHAPTER, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.CHAPTER, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Collection
                        new CacheConfiguration(Caching.Configurations.Sections.COLLECTION, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.COLLECTION, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Registration Invite
                        new CacheConfiguration(Caching.Configurations.Sections.REGISTRATION_INVITE, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.REGISTRATION_INVITE, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //User Blocked Tag
                        new CacheConfiguration(Caching.Configurations.Sections.USER_BLOCKED_TAG, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.USER_BLOCKED_TAG, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Cache Configuration
                        new CacheConfiguration(Caching.Configurations.Sections.CACHE_CONFIGURATION, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.CACHE_CONFIGURATION, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                        //Social link Configuration
                        new CacheConfiguration(Caching.Configurations.Sections.SOCIAL_LINK, Caching.Configurations.LookupTypes.FIND, Caching.Configurations.LengthTypes.HOUR, 1),
                        new CacheConfiguration(Caching.Configurations.Sections.SOCIAL_LINK, Caching.Configurations.LookupTypes.ALL, Caching.Configurations.LengthTypes.DAY, 30),
                    };

                    List<CacheConfiguration> existingCacheConfigurations = await cacheConfigurationRetrievalService.GetCacheConfigurationsAsync();

                    foreach (var cacheConfiguration in knownCacheConfigurations)
                    {
                        if (!existingCacheConfigurations.Any(x => x.SectionID == cacheConfiguration.SectionID && x.LookupTypeID == cacheConfiguration.LookupTypeID))
                        {
                            cacheConfigurationsToSeed.Add(cacheConfiguration);
                        }
                    }

                    if (cacheConfigurationsToSeed.Any())
                    {
                        await cacheConfigurationUpdateService.BulkCreateCacheConfiguration(cacheConfigurationsToSeed);
                    }
                }
            }
        }

        private async Task GenerateDynamicTagCSS()
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IOptions<ManicNetSettings> settings = scope.ServiceProvider.GetService<IOptions<ManicNetSettings>>();

                if (settings.Value.SeedingSettings.TagCSS)
                {
                    TagTypeRetrievalService tagTypeRetrievalService = (TagTypeRetrievalService)scope.ServiceProvider.GetService<ITagTypeRetrievalService>();
                    TagRetrievalService tagRetrievalService = (TagRetrievalService)scope.ServiceProvider.GetService<ITagRetrievalService>();

                    List<TagType> tagtypes = await tagTypeRetrievalService.GetActiveTagTypes();
                    List<Tag> tags = await tagRetrievalService.GetActiveTags();

                    var defaultStyling = settings.Value.StylingSettings.TagTypeStyling;

                    List<string> file = new List<string>() {
                    "/*A dynamically generated file controlling the appearance of site tags */",
                    ".icon::before { display: inline-block; -webkit-font-smoothing: antialiased; font-style: normal; font-variant: normal; text-rendering: auto; }",
                    string.Format(".primary-tag {{ background: {0}; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px; border-color: {1}; border-style: solid; border-top-left-radius: 4px; border-top-right-radius: 4px; color: {2}; height: 24px; margin-left: 5px; padding-left: 12px; padding-right: 10px; text-decoration: none; white-space: nowrap; }}", defaultStyling.PrimaryColouring.Body, defaultStyling.PrimaryColouring.Border, defaultStyling.PrimaryColouring.Text),
                    string.Format(".secondary-tag {{ background: {0}; border-bottom-left-radius: 4px; border-bottom-right-radius: 4px; border-color: {1}; border-style: solid; border-top-left-radius: 4px; border-top-right-radius: 4px; color: {2}; height: 24px; margin-left: 5px; padding-left: 12px; padding-right: 10px; text-decoration: none; white-space: nowrap; }}", defaultStyling.SecondaryColouring.Body, defaultStyling.SecondaryColouring.Border, defaultStyling.SecondaryColouring.Text),
                    string.Format(".tag::before {{ content: \"\\{0}\"; font-family: \"Font Awesome 5 Free\"; font-weight: 900; }}", defaultStyling.DefaultIcon)
                };

                    foreach (TagType tagType in tagtypes)
                    {
                        file.Add(string.Format(".primary-type-{0} {{ background: {1}; border-color: {2}; color: {3}; }}", HttpUtility.HtmlEncode(tagType.Name.Replace(' ', '_')), tagType.PrimaryBodyColour, tagType.PrimaryBorderColour, tagType.PrimaryTextColour));
                        file.Add(string.Format(".secondary-type-{0} {{ background: {1}; border-color: {2}; color: {3}; }}", HttpUtility.HtmlEncode(tagType.Name.Replace(' ', '_')), tagType.SecondaryBodyColour, tagType.SecondaryBorderColour, tagType.SecondaryTextColour));

                        if (!string.IsNullOrWhiteSpace(tagType.Icon))
                        {
                            file.Add(string.Format(".primary-type-{0}::before {{ content: \"\\{1}\"; font-family: \"{2}\"; font-weight: 900; }}", HttpUtility.HtmlEncode(tagType.Name.Replace(' ', '_')), tagType.Icon, FrontEnd.FONT_FAMILY_MAP[tagType.FontFamily.Value]));
                            file.Add(string.Format(".secondary-type-{0}::before {{ content: \"\\{1}\"; font-family: \"{2}\"; font-weight: 900; }}", HttpUtility.HtmlEncode(tagType.Name.Replace(' ', '_')), tagType.Icon, FrontEnd.FONT_FAMILY_MAP[tagType.FontFamily.Value]));
                        }
                    }

                    foreach (Tag tag in tags.Where(t => !string.IsNullOrWhiteSpace(t.Icon)).ToList())
                    {
                        if (!string.IsNullOrWhiteSpace(tag.Icon))
                        {
                            file.Add(string.Format(".tag-{0}::before {{ content: \"\\{1}\"; font-family: \"{2}\"; font-weight: 900; }}", HttpUtility.HtmlEncode(tag.Name.Replace(' ', '_')), tag.Icon, FrontEnd.FONT_FAMILY_MAP[tag.FontFamily.Value]));
                        }
                    }

                    string filePath = Path.Combine(Environment.CurrentDirectory, "wwwroot", "css", "tag.css");

                    using (StreamWriter outputFile = new StreamWriter(filePath))
                    {
                        foreach (string line in file)
                        {
                            outputFile.WriteLine(line);
                        }
                    }
                }
            }
        }

        private async Task GenerateDynamicSocialLinkCSS()
        {
            using (var scope = serviceProvider.CreateScope())
            {
                IOptions<ManicNetSettings> settings = scope.ServiceProvider.GetService<IOptions<ManicNetSettings>>();

                if (settings.Value.SeedingSettings.SocialLinkCSS)
                {
                    List<string> file = new List<string>()
                    {
                        "/*A dynamically generated file controlling the appearance of site social links */",
                        ".icon::before { display: inline-block; -webkit-font-smoothing: antialiased; font-style: normal; font-variant: normal; text-rendering: auto; }",
                        ".socialLink-url::before { content: \"\\f0c1\"; font-family: \"Font Awesome 5 Free\"; font-weight: 900; }", 
                        ".socialLink-email::before { content: \"\\f0e0\"; font-family: \"Font Awesome 5 Free\"; font-weight: 900; }"
                    };

                    SocialLinkService socialLinkService = (SocialLinkService)scope.ServiceProvider.GetService<ISocialLinkService>();

                    List<SocialLink> socialLinks = await socialLinkService.GetActiveSocialLinks();

                    foreach (SocialLink socialLink in socialLinks.Where(t => !string.IsNullOrWhiteSpace(t.Icon)).ToList())
                    {
                        if (!string.IsNullOrWhiteSpace(socialLink.Icon))
                        {
                            string type = "url";
                            if (socialLink.TypeID == Constants.Social.LinkTypes.LINK)
                            {
                                type = "url";
                            }
                            else if (socialLink.TypeID == Constants.Social.LinkTypes.EMAIL)
                            {
                                type = "email";
                            }
                            //Font style is wrong
                            file.Add(string.Format(".socialLink-{0}-{1}::before {{ content: \"\\{2}\"; font-family: \"{3}\"; font-weight: 900; }}", type, HttpUtility.HtmlEncode(socialLink.Name.Replace(' ', '_')), socialLink.Icon, FrontEnd.FONT_FAMILY_MAP[socialLink.FontFamily.Value]));
                        }
                    }

                    string filePath = Path.Combine(Environment.CurrentDirectory, "wwwroot", "css", "socialLink.css");

                    using (StreamWriter outputFile = new StreamWriter(filePath))
                    {
                        foreach (string line in file)
                        {
                            outputFile.WriteLine(line);
                        }
                    }
                }
            }
        }
    }
}
