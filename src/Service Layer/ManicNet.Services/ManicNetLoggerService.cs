﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using Microsoft.Extensions.Logging;
using System;

namespace ManicNet.Services
{
    public sealed class ManicNetLoggerService : IManicNetLoggerService
    {
        private readonly ILogger<ManicNetLoggerService> logger;
        private readonly IUserRepository userRepository;

        public ManicNetLoggerService(ILogger<ManicNetLoggerService> logger, IUserRepository userRepository)
        {
            this.logger = logger;
            this.userRepository = userRepository;
        }

        public void LogCritical(Exception exception, string message)
        {
            logger.LogCritical(exception, string.Format("{0}{1}", userRepository.GetHumanReadableUserIdentifier(), message));
        }

        public void LogError(string path, string functionIdentifier, Exception exception)
        {
            string location = string.Format("{{ Error in function {0}.{1} }}|", path, functionIdentifier);
            logger.LogError(exception, string.Format("{0}{1}|", userRepository.GetHumanReadableUserIdentifier(), location));
        }

        public void LogError(string path, string functionIdentifier, Exception exception, string message)
        {
            string location = string.Format("{{ Error in function {0}.{1}. }}|", path, functionIdentifier);
            logger.LogError(exception, string.Format("{0}{1}{2}|", userRepository.GetHumanReadableUserIdentifier(), location, message));
        }

        public void LogInformation(string path, string functionIdentifier, string message)
        {
            string location = string.Format("{{ Information in function {0}.{1}. }}|", path, functionIdentifier);
            logger.LogInformation(string.Format("{0}{1}{2}|", userRepository.GetHumanReadableUserIdentifier(), location, message));
        }

        public void LogWarning(string path, string functionIdentifier, string message)
        {
            logger.LogWarning(string.Format("{0}{1}", userRepository.GetHumanReadableUserIdentifier(), message));
        }

        public void LogWarning(string path, string functionIdentifier, Exception exception, string message)
        {
            logger.LogWarning(exception, string.Format("{0}{1}", userRepository.GetHumanReadableUserIdentifier(), message));
        }
    }
}
