﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class VolumeRetrievalService : IVolumeRetrievalService
    {
        private readonly IVolumeRepository volumeRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;

        private readonly string path = "ManicNet.Services.VolumeRetrievalService";

        public VolumeRetrievalService(IVolumeRepository volumeRepository, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService)
        {
            this.volumeRepository = volumeRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
        }

        public async Task<Volume> FindVolume(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindVolume(id, user, bypassCache)";

            try
            {
                Volume volume = await volumeRepository.FindAsync(id, bypassCache);

                if (volume == null)
                {
                    throw new VolumeNotFoundException(string.Format("Volume with ID {0} not found", id));
                }

                if ((volume.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeNotViewableException(string.Format("User does not have permission to view volume with ID {0}", id));
                }

                if ((volume.Work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeUnableToViewParentWorkException(string.Format("User does not have permission to view deleted parent work type of volume with ID {0}", id));
                }

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public async Task<Volume> FindVolumeForDownload(Guid id, ClaimsPrincipal user)
        {
            const string functionIdentifier = "FindVolumeForDownload(id, user)";

            try
            {
                Volume volume = await volumeRepository.FindForDownload(id);

                if (volume == null)
                {
                    throw new VolumeNotFoundException(string.Format("Volume with ID {0} not found", id));
                }

                if ((volume.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeNotViewableException(string.Format("User does not have permission to view volume with ID {0}", id));
                }

                if ((volume.Work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeUnableToViewParentWorkException(string.Format("User does not have permission to view deleted parent work type of volume with ID {0}", id));
                }

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1} }}", id, user));
                throw;
            }
        }

        public async Task<Volume> FindVolumeForDeletion(Guid id, ClaimsPrincipal user, bool noTracking = false)
        {
            const string functionIdentifier = "FindVolumeForDeletion(id, user, noTracking)";

            try
            {
                Volume volume = await volumeRepository.FindForDeletion(id, noTracking);

                if (volume == null)
                {
                    throw new VolumeNotFoundException(string.Format("Volume with ID {0} not found", id));
                }

                if ((volume.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeNotViewableException(string.Format("User does not have permission to view volume with ID {0}", id));
                }

                if ((volume.Work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new VolumeUnableToViewParentWorkException(string.Format("User does not have permission to view deleted parent work type of volume with ID {0}", id));
                }

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, noTracking:{2} }}", id, user, noTracking));
                throw;
            }
        }

        public async Task<List<Volume>> GetVolumesForWork(Guid workID)
        {
            const string functionIdentifier = "GetVolumesForWork(workID)";

            List<Volume> volumes = new List<Volume>();

            try
            {
                volumes = (await volumeRepository.AllAsync()).Where(x => x.WorkID == workID).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ workID:{0} }}", workID));
                throw;
            }

            return volumes;
        }
    }
}
