﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class VolumeUpdateService : IVolumeUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IVolumeRepository volumeRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IContentFileHandlerService contentFileHandlerService;
        private readonly IUploadService uploadService;
        private readonly IChapterUpdateService chapterUpdateService;
        private readonly ICacheCleanerService cacheCleanerService;

        private readonly string path = "ManicNet.Services.VolumeUpdateService";

        public VolumeUpdateService(IManicNetUnitOfWork unitOfWork, IVolumeRepository volumeRepository, IManicNetLoggerService logger, IContentFileHandlerService contentFileHandlerService, IUploadService uploadService, IChapterUpdateService chapterUpdateService, ICacheCleanerService cacheCleanerService)
        {
            this.unitOfWork = unitOfWork;
            this.volumeRepository = volumeRepository;
            this.logger = logger;
            this.contentFileHandlerService = contentFileHandlerService;
            this.uploadService = uploadService;
            this.chapterUpdateService = chapterUpdateService;
            this.cacheCleanerService = cacheCleanerService;
        }

        public async Task<string> CreateVolume(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "CreateVolume(work, primaryTags, secondaryTags, coverFile, cacheCleanup)";

            string message;

            try
            {
                message = await UpdateCoverAsync(volume, coverFile, false);

                volumeRepository.Insert(volume, primaryTags, secondaryTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, primaryTags:{1}, secondaryTags:{2}, coverFile:{3}, cacheCleanup:{4} }}", volume, primaryTags, secondaryTags, coverFile, cacheCleanup));
                throw;
            }

            return message;
        }

        public async Task<string> UpdateVolume(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, bool deleteCover, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "UpdateVolume(volume, primaryTags, secondaryTags, coverFile, deleteCover, cacheCleanup)";

            try
            {
                Guid? previousCoverID = volume.CoverID;

                string message = await UpdateCoverAsync(volume, coverFile, deleteCover);

                volumeRepository.Update(volume, primaryTags, secondaryTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
                await contentFileHandlerService.CleanupContent(previousCoverID);

                return message;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, primaryTags:{1}, secondaryTags:{2}, coverFile:{3}, deleteCover:{4}, cacheCleanup:{5} }}", volume, primaryTags, secondaryTags, coverFile, deleteCover, cacheCleanup));
                throw;
            }
        }

        public async Task UpdateVolume(Volume volume, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "UpdateVolume(volume, cacheCleanup)";

            try
            {
                volumeRepository.Update(volume);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, cacheCleanup:{1} }}", volume, cacheCleanup));
                throw;
            }
        }

        public async Task<CleanupWork> RemoveVolume(Volume volume, WorkCacheCleanup cacheCleanup, bool calledAsTopLevel = true)
        {
            const string functionIdentifier = "RemoveVolume(volume, cacheCleanup, calledAsTopLevel)";

            CleanupWork cleanupWork = new CleanupWork(volume);

            try
            {
                if (!volume.IsDeleted && calledAsTopLevel)
                {
                    throw new VolumeNotDeletableException(string.Format("Volume with ID {0} not is not in the state to be deleted", volume.VolumeID));
                }

                foreach (Chapter chapter in volume.Chapters)
                {
                    cleanupWork.Add(await chapterUpdateService.RemoveChapter(chapter, cacheCleanup, false));
                }

                volumeRepository.Delete(volume);
                if (calledAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                    await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
                    await contentFileHandlerService.CleanupContent(cleanupWork.Contents);
                    cleanupWork.Clear();
                }
            }
            catch (VolumeNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Volume not deletable. {{ volume:{0}, cacheCleanup:{1} }}", volume, cacheCleanup));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, cacheCleanup:{1} }}", volume, cacheCleanup));
                throw;
            }

            return cleanupWork;
        }

        public async Task MigrateVolume(Volume volume, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "MigrateVolume(volume, cacheCleanup)";

            try
            {
                volumeRepository.Update(volume);
                await unitOfWork.SaveAsync();
                //await volumeRepository.UpdateCacheFromMigration(volume.VolumeID, originalWorkID, volume.WorkID, volume.Chapters.Select(c => c.ChapterID));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, cacheCleanup:{1}}}", volume, cacheCleanup));
                throw;
            }
        }

        private async Task<string> UpdateCoverAsync(Volume volume, IFormFile coverFile, bool deleteCover)
        {
            const string functionIdentifier = "UpdateCoverAsync(volume, coverFile, deleteCover)";

            string message = string.Empty;

            if (deleteCover)
            {
                volume.CoverID = null;
            }

            try
            {
                if (coverFile != null)
                {
                    Content cover = await uploadService.UploadFileOrRetrieveFromContent(coverFile);
                    volume.CoverID = cover.ContentID;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, coverFile:{1}, deleteCover:{2} }}", volume, coverFile, deleteCover));
                message = string.Format("A problem occurred while processing the cover image.");
            }

            return message;
        }
    }
}
