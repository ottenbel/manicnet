﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class WorkRetrievalService : IWorkRetrievalService
    {
        private readonly IWorkRepository workRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;

        private readonly string path = "ManicNet.Services.WorkRetrievalService";

        public WorkRetrievalService(IWorkRepository workRepository, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService, ITagRetrievalService tagRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService)
        {
            this.workRepository = workRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
            this.tagRetrievalService = tagRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
        }

        public async Task<List<Work>> GetActiveWorksBySearch(string search, Guid? userID)
        {
            const string functionIdentifier = "GetActiveWorksBySearch(search, user)";

            List<Work> works = new List<Work>();

            try
            {
                var query = (await workRepository.AllAsync()).Where(x => x.IsDeleted == false);

                if (!string.IsNullOrEmpty(search))
                {
                    var searchTags = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(search);
                    List<RelatedTagList> relatedTagsToSearch = await tagRetrievalService.GetListOfTagsImplyingTags(searchTags.Included);

                    //Include directly searched for tags and any tags that imply that tag
                    foreach (RelatedTagList related in relatedTagsToSearch)
                    {
                        List<Guid> included = related.Related.Append(related.Target).ToList().Select(t => t.TagID).ToList();

                        query = query.Where(w => w.Tags.Where(t => t.Tag.IsDeleted == false).Select(wt => wt.TagID).Intersect(included).Any()
                        || w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(included).Any()
                        || w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false)))
                            .Select(vt => vt.TagID).Intersect(included).Any());
                    }

                    //Exclude directly searched for tags
                    foreach (Tag tag in searchTags.Excluded)
                    {
                        query = query.Where(w => !w.Tags.Where(t => t.Tag.IsDeleted == false).Select(wt => wt.TagID).Contains(tag.TagID)
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Contains(tag.TagID)
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false)))
                            .Select(vt => vt.TagID).Contains(tag.TagID));
                    }
                }


                if (userID.HasValue)
                {
                    List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                    query = query.Where(w => !w.Tags.Where(t => t.Tag.IsDeleted == false).Select(wt => wt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false)))
                            .Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());
                }

                works = query.ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ search:{0}, userID:{1} }}", search, userID));
                throw;
            }

            return works;
        }

        public async Task<List<Work>> GetDeletedWorks(Guid? userID)
        {
            const string functionIdentifier = "GetDeletedWorks()";

            List<Work> works = new List<Work>();

            try
            {
                var query = (await workRepository.AllAsync()).Where(x => x.IsDeleted == true);

                if (userID.HasValue)
                {
                    List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                    query = query.Where(w => !w.Tags.Select(wt => wt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false))).Select(vt => vt.TagID)
                            .Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());
                }

                works = query.ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return works;
        }

        public async Task<bool> DoDeletedWorksExist()
        {
            const string functionIdentifier = "DoDeletedWorksExist()";

            try
            {
                return (await workRepository.AllAsync()).Where(x => x.IsDeleted == true).Any();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<Work> FindWorkAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindWorkAsync(id, user, bypassCache)";

            try
            {
                Work work = await workRepository.FindAsync(id, bypassCache);

                if (work == null)
                {
                    throw new WorkNotFoundException(string.Format("Work with ID {0} not found", id));
                }

                if ((work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new WorkNotViewableException(string.Format("User does not have permission to view work with ID {0}", id));
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public Work FindWork(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindWork(id, user, bypassCache)";

            try
            {
                Work work = workRepository.Find(id, bypassCache);

                if (work == null)
                {
                    throw new WorkNotFoundException(string.Format("Work with ID {0} not found", id));
                }

                if ((work.IsDeleted) && (!authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK).GetAwaiter().GetResult().Succeeded))
                {
                    throw new WorkNotViewableException(string.Format("User does not have permission to view work with ID {0}", id));
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public async Task<Work> FindWorkForDeletion(Guid id, ClaimsPrincipal user, bool noTracking = false)
        {
            const string functionIdentifier = "FindWorkForDeletion(id, user, noTracking)";

            try
            {
                Work work = await workRepository.FindForDeletionAsync(id, noTracking);

                if (work == null)
                {
                    throw new WorkNotFoundException(string.Format("Work with ID {0} not found", id));
                }

                if ((work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new WorkNotViewableException(string.Format("User does not have permission to view work with ID {0}", id));
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, noTracking:{2}}}", id, user, noTracking));
                throw;
            }
        }


        public async Task<Work> FindWorkForDownload(Guid id, ClaimsPrincipal user)
        {
            const string functionIdentifier = "FindWorkForDownload(id, user)";

            try
            {
                Work work = await workRepository.FindForDownloadAsync(id);

                if (work == null)
                {
                    throw new WorkNotFoundException(string.Format("Work with ID {0} not found", id));
                }

                if ((work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new WorkNotViewableException(string.Format("User does not have permission to view work with ID {0}", id));
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1} }}", id, user));
                throw;
            }
        }

        public async Task<List<Guid>> GetAllTagsAssociatedWithWork(Guid workID)
        {
            List<Guid> results = new List<Guid>();

            try
            {
                Work work = await workRepository.FindAsync(workID);

                results = results.Concat(work.Tags.Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags).Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(v => v.IsDeleted == false).SelectMany(c => c.Tags).Select(t => t.TagID))).ToList();
            }
            catch (Exception)
            {
                //Swallow the error
            }

            return results;
        }

        public async Task<List<Guid>> GetAllTagsAssociatedWithVolume(Guid workID, Guid volumeID)
        {
            List<Guid> results = new List<Guid>();

            try
            {
                Work work = await workRepository.FindAsync(workID);

                results = results.Concat(work.Tags.Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false || v.VolumeID == volumeID).SelectMany(v => v.Tags).Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false || v.VolumeID == volumeID).SelectMany(v => v.Chapters.Where(v => v.IsDeleted == false).SelectMany(c => c.Tags).Select(t => t.TagID))).ToList();
            }
            catch (Exception)
            {
                //Swallow the error
            }

            return results;
        }

        public async Task<List<Guid>> GetAllTagsAssociatedWithChapter(Guid workID, Guid volumeID, Guid chapterID)
        {
            List<Guid> results = new List<Guid>();

            try
            {
                Work work = await workRepository.FindAsync(workID);

                results = results.Concat(work.Tags.Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false || v.VolumeID == volumeID).SelectMany(v => v.Tags).Select(t => t.TagID))
                    .Concat(work.Volumes.Where(v => v.IsDeleted == false || v.VolumeID == volumeID).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false || c.ChapterID == chapterID).SelectMany(c => c.Tags).Select(t => t.TagID))).ToList();
            }
            catch (Exception)
            {
                //Swallow the error
            }

            return results;
        }

        public async Task<List<Volume>> GetDeletedVolumes(Guid? workID, Guid? userID)
        {
            const string functionIdentifier = "GetDeletedVolumes(workID, userID)";

            List<Volume> volumes = new List<Volume>();

            try
            {
                var workQuery = (await workRepository.AllAsync()).Where(w => ((workID.HasValue && w.WorkID == workID.Value) || (!workID.HasValue)));

                List<User_Blocked_Tag> userBlockedTags = new List<User_Blocked_Tag>();

                if (userID.HasValue)
                {
                    userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                    workQuery = workQuery.Where(w => !w.Tags.Select(wt => wt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());
                }

                var volumeQuery = workQuery.SelectMany(w => w.Volumes).Where(v => v.IsDeleted == true
                    && !v.Tags.Where(t => t.Tag.IsDeleted == false).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                    && !v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());

                volumes = volumeQuery.ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ workID:{0}, userID:{1} }}", workID, userID));
                throw;
            }

            return volumes;
        }

        public async Task<List<Chapter>> GetDeletedChapters(Guid? volumeID, Guid? userID)
        {
            const string functionIdentifier = "GetDeletedChapters(volumeID, userID)";

            List<Chapter> chapters = new List<Chapter>();

            try
            {
                var workQuery = (await workRepository.AllAsync()).AsQueryable();

                List<User_Blocked_Tag> userBlockedTags = new List<User_Blocked_Tag>();

                if (userID.HasValue)
                {
                    userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                    workQuery = workQuery.Where(w => !w.Tags.Select(wt => wt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Volumes.Where(v => v.IsDeleted == false || v.VolumeID == volumeID).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());
                }

                var chapterQuery = workQuery.SelectMany(w => w.Volumes.SelectMany(v => v.Chapters)).Where(c => c.IsDeleted == true
                && !c.Tags.Where(t => t.Tag.IsDeleted == false).Select(vt => vt.TagID)
                            .Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any());

                chapters = chapterQuery.ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volumeID:{0}, userID:{1} }}", volumeID, userID));
                throw;
            }

            return chapters;
        }
    }
}
