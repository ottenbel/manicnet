﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class WorkUpdateService : IWorkUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IWorkRepository workRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IContentFileHandlerService contentFileHandlerService;
        private readonly IVolumeUpdateService volumeUpdateService;
        private readonly IUploadService uploadService;
        private readonly ICacheCleanerService cacheCleanerService;

        private readonly string path = "ManicNet.Services.WorkUpdateService";

        public WorkUpdateService(IManicNetUnitOfWork unitOfWork, IWorkRepository workRepository, IManicNetLoggerService logger, IContentFileHandlerService contentFileHandlerService, IVolumeUpdateService volumeUpdateService, IUploadService uploadService, ICacheCleanerService cacheCleanerService)
        {
            this.unitOfWork = unitOfWork;
            this.workRepository = workRepository;
            this.logger = logger;
            this.contentFileHandlerService = contentFileHandlerService;
            this.volumeUpdateService = volumeUpdateService;
            this.uploadService = uploadService;
            this.cacheCleanerService = cacheCleanerService;
        }

        public async Task<string> CreateWork(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile)
        {
            const string functionIdentifier = "CreateWork(work, primaryTags, secondaryTags, coverFile)";

            string message;

            try
            {
                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primaryTags.Select(t => t.TagID).Concat(secondaryTags.Select(t => t.TagID)).Distinct());
                message = await UpdateCoverAsync(work, coverFile, false);

                workRepository.Insert(work, primaryTags, secondaryTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0}, primaryTags:{1}, secondaryTags:{2} coverFile:{3} }}", work, primaryTags, secondaryTags, coverFile));
                throw;
            }

            return message;
        }

        public async Task<string> UpdateWork(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, bool deleteCover)
        {
            const string functionIdentifier = "UpdateWork(work, primaryTags, secondaryTags, cover, deleteCover)";

            string message = string.Empty;

            try
            {
                Guid? previousCoverID = work.CoverID;
                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work, primaryTags.Select(t => t.TagID).Concat(secondaryTags.Select(t => t.TagID)).Distinct());

                message = await UpdateCoverAsync(work, coverFile, deleteCover);

                //TODO: Pull added and removed calculation out of repo and into the service
                workRepository.Update(work, primaryTags, secondaryTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
                await contentFileHandlerService.CleanupContent(previousCoverID);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0}, primaryTags:{1}, secondaryTags:{2}, coverFile:{3}, deleteCover:{4}}}", work, primaryTags, secondaryTags, coverFile, deleteCover));
                throw;
            }

            return message;
        }

        public async Task UpdateWork(Work work)
        {
            const string functionIdentifier = "UpdateWork(work)";

            try
            {
                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);
                workRepository.Update(work);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0} }}", work));
                throw;
            }
        }

        public async Task RemoveWork(Work work)
        {
            const string functionIdentifier = "RemoveWork(work)";

            try
            {
                CleanupWork cleanupWork = new CleanupWork(work);
                WorkCacheCleanup cacheCleanup = new WorkCacheCleanup(work);

                if (!work.IsDeleted)
                {
                    throw new WorkNotDeletableException(string.Format("Work with ID {0} not is not in the state to be deleted", work.WorkID));
                }

                foreach (Volume volume in work.Volumes)
                {
                    cleanupWork.Add(await volumeUpdateService.RemoveVolume(volume, cacheCleanup, false));
                }

                workRepository.Delete(work);
                await unitOfWork.SaveAsync();

                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);

                await contentFileHandlerService.CleanupContent(cleanupWork.Contents);
            }
            catch (WorkNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Work not deletable. {{ work:{0} }}", work));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0} }}", work));
                throw;
            }
        }

        private async Task<string> UpdateCoverAsync(Work work, IFormFile coverFile, bool deleteCover)
        {
            const string functionIdentifier = "UpdateCoverAsync(work, coverFile, deleteCover)";

            string message = string.Empty;

            if (deleteCover)
            {
                work.CoverID = null;
            }

            try
            {
                if (coverFile != null)
                {
                    Content cover = await uploadService.UploadFileOrRetrieveFromContent(coverFile);
                    work.CoverID = cover.ContentID;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error uploading cover. {{ work:{0}, coverFile:{1}, deleteCover:{2} }}", work, coverFile, deleteCover));
                message = string.Format("A problem occurred while processing the cover image.");
            }

            return message;
        }
    }
}
