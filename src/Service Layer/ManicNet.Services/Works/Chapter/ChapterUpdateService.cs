﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ChapterUpdateService : IChapterUpdateService
    {
        private readonly IUploadService uploadService;
        private readonly IChapterRepository chapterRepository;
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IManicNetLoggerService logger;
        private readonly IContentFileHandlerService contentFileHandlerService;
        private readonly IWorkRepository workRepository;
        private readonly ICacheCleanerService cacheCleanerService;

        private readonly string path = "ManicNet.Services.ChapterUpdateService";

        public ChapterUpdateService(IUploadService uploadService, IChapterRepository chapterRepository, IManicNetUnitOfWork unitOfWork, IManicNetLoggerService logger, IContentFileHandlerService contentFileHandlerService, IWorkRepository workRepository, ICacheCleanerService cacheCleanerService)
        {
            this.uploadService = uploadService;
            this.chapterRepository = chapterRepository;
            this.unitOfWork = unitOfWork;
            this.logger = logger;
            this.contentFileHandlerService = contentFileHandlerService;
            this.workRepository = workRepository;
            this.cacheCleanerService = cacheCleanerService;
        }

        public async Task<List<string>> CreateChapter(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile[] pageFiles, Guid workID, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "CreateChapter(work, primaryTags, secondaryTags, pageFiles, workID, cacheCleanup)";

            List<string> messages = new List<string>();
            List<Content> contents = new List<Content>();

            try
            {
                await UploadPages(pageFiles, contents, functionIdentifier, chapter, messages);

                chapterRepository.Insert(chapter, primaryTags, secondaryTags, contents);
                await this.UpdateSortableDate(workID);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, primaryTags:{1}, secondaryTags:{2}, pageFiles:{3}, workID:{4}, cacheCleanup:{5} }}", chapter, primaryTags, secondaryTags, pageFiles, workID, cacheCleanup));
                throw;
            }

            return messages;
        }

        public async Task<List<string>> UpdateChapter(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, Guid workID, ChapterModifyModel model, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "EditChapterAsync(chapter, primaryTags, secondaryTags, workID, model, cacheCleanup)";

            List<string> messages = new List<string>();
            List<Content> contents = new List<Content>();
            List<Guid> deletedContents = new List<Guid>();

            try
            {
                for (int i = 0; i < model.Pages.Count; i++)
                {
                    PageModifyViewModel pageViewModel = model.Pages[i];
                    Page page = chapter.Pages.Where(p => p.PageID == pageViewModel.PageID).FirstOrDefault();
                    if (pageViewModel.Delete)
                    {
                        deletedContents.Add(page.ContentID);
                        chapter.Pages.Remove(page);
                        model.Pages.Remove(pageViewModel);
                        i--;
                    }
                    else if (pageViewModel.PageNumber != page.Number)
                    {
                        page.Number = pageViewModel.PageNumber;
                    }
                }

                if (model.Files != null)
                {
                    await UploadPages(model.Files, contents, functionIdentifier, chapter, messages);
                }

                chapterRepository.Update(chapter, primaryTags, secondaryTags, contents);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
                await contentFileHandlerService.CleanupContent(deletedContents);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, primaryTags:{1}, secondaryTags:{2}, model:{3}, cacheCleanup:{4} }}", chapter, primaryTags, secondaryTags, model, cacheCleanup));
                throw;
            }

            return messages;
        }

        public async Task UpdateChapter(Chapter chapter, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "UpdateChapter(chapter, cacheCleanup)";

            try
            {
                chapterRepository.Update(chapter);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, cacheCleanup:{1} }}", chapter, cacheCleanup));
                throw;
            }
        }

        public async Task<CleanupWork> RemoveChapter(Chapter chapter, WorkCacheCleanup cacheCleanup, bool calledAsTopLevel = true)
        {
            const string functionIdentifier = "RemoveChapter(chapter, cacheCleanup, calledAsTopLevel)";

            CleanupWork cleanupWork = new CleanupWork(chapter);

            try
            {
                if (!chapter.IsDeleted && calledAsTopLevel)
                {
                    throw new ChapterNotDeletableException(string.Format("Chapter with ID {0} not is not in the state to be deleted", chapter.ChapterID));
                }

                chapterRepository.Delete(chapter);
                if (calledAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                    await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
                    await contentFileHandlerService.CleanupContent(chapter.Pages.Select(p => p.ContentID));
                    cleanupWork.Clear();
                }
            }
            catch (ChapterNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Chapter not deletable. {{ chapter:{0}, cacheCleanup:{1}, calledAsTopLevel:{2} }}", chapter, cacheCleanup, calledAsTopLevel));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, cacheCleanup:{1}, calledAsTopLevel:{2} }}", chapter, cacheCleanup, calledAsTopLevel));
                throw;
            }

            return cleanupWork;
        }

        public async Task UpdateSortableDate(Guid workID)
        {
            const string functionIdentifier = "UpdateSortableDate(workID)";

            try
            {
                Work work = await FindWorkWithoutUserCheckAsync(workID, true);
                work.SortableDate = DateTime.UtcNow;
                workRepository.Update(work);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ workID:{0} }}", workID));
                throw;
            }
        }

        public async Task MigrateChapter(Chapter chapter, WorkCacheCleanup cacheCleanup)
        {
            const string functionIdentifier = "MigrateChapter(chapter, cacheCleanup)";

            try
            {
                chapterRepository.Update(chapter);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForWork(cacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, cacheCleanup:{1} }}", chapter, cacheCleanup));
                throw;
            }
        }

        private async Task<Work> FindWorkWithoutUserCheckAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindWorkAsync(id, bypassCache)";

            try
            {
                Work work = await workRepository.FindAsync(id, bypassCache);

                if (work == null)
                {
                    throw new WorkNotFoundException(string.Format("Work with ID {0} not found", id));
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        private async Task UploadPages(IFormFile[] pageFiles, List<Content> contents, string functionIdentifier, Chapter chapter, List<string> messages)
        {
            foreach (IFormFile pageFile in pageFiles)
            {
                try
                {
                    Content page = await uploadService.UploadFileOrRetrieveFromContent(pageFile);
                    contents.Add(page);
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex, string.Format("{{chapter:{0}, pageFile:{1}}}", chapter, pageFile));
                    messages.Add(string.Format("A problem occurred while processing the page {0}.", pageFile.FileName));
                }
            }
        }
    }
}
