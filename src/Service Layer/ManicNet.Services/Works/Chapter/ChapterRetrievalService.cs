﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ChapterRetrievalService : IChapterRetrievalService
    {
        private readonly IChapterRepository chapterRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;

        private readonly string path = "ManicNet.Services.ChapterRetrievalService";

        public ChapterRetrievalService(IChapterRepository chapterRepository, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService)
        {
            this.chapterRepository = chapterRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
        }

        public async Task<Chapter> FindChapter(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindChapter(id, user, bypassCache)";

            try
            {
                Chapter chapter = await chapterRepository.FindAsync(id, bypassCache);

                if (chapter == null)
                {
                    throw new ChapterNotFoundException(string.Format("Chapter with ID {0} not found", id));
                }

                if ((chapter.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new ChapterNotViewableException(string.Format("User does not have permission to view chapter with ID {0}", id));
                }

                if ((chapter.Volume.IsDeleted || chapter.Volume.Work.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new ChapterUnableToViewParentWorkException(string.Format("User does not have permission to view deleted parent of chapter with ID {0}", id));
                }

                return chapter;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }
    }
}
