﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ManicNetPermissionService : IManicNetPermissionService
    {
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly IManicNetLoggerService logger;
        private readonly ManicNetUserManager userManager;
        private readonly RoleManager<ManicNetRole> roleManager;

        private readonly string path = "ManicNet.Services.ManicNetPermissionService";

        public ManicNetPermissionService(IManicNetAuthorizationService authorizationService, IManicNetLoggerService logger, ManicNetUserManager userManager, RoleManager<ManicNetRole> roleManager)
        {
            this.authorizationService = authorizationService;
            this.logger = logger;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public async Task<List<PermissionSection>> BuildSection(ManicNetUser user, ClaimsPrincipal adminUser)
        {
            List<PermissionSection> permissionSection = new List<PermissionSection>();

            List<Claim> claims = new List<Claim>(await userManager.GetClaimsAsync(user));
            claims = claims.Where(x => x.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE).ToList();

            Task<AuthorizationResult> hasOwnerPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_PERMISSIONS);
            Task<AuthorizationResult> hasAdministratorPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_PERMISSIONS);
            Task<AuthorizationResult> hasUserPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_PERMISSIONS);

            await Task.WhenAll(hasOwnerPermissionTask, hasAdministratorPermissionTask, hasUserPermissionTask);

            PermissionHolder permissionHolder = new PermissionHolder((await hasOwnerPermissionTask).Succeeded, (await hasAdministratorPermissionTask).Succeeded, (await hasUserPermissionTask).Succeeded);

            foreach (var section in ManicNetPermissions.sections)
            {
                permissionSection.Add(new PermissionSection(section, claims, permissionHolder.HasOwnerPermission, permissionHolder.HasAdministratorPermission, permissionHolder.HasUserPermission));
            }

            return permissionSection;
        }

        public async Task<List<PermissionSection>> BuildSection(ManicNetRole role, ClaimsPrincipal adminUser)
        {
            List<PermissionSection> permissionSection = new List<PermissionSection>();

            List<Claim> claims = new List<Claim>(await roleManager.GetClaimsAsync(role));
            claims = claims.Where(x => x.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE).ToList();

            Task<AuthorizationResult> hasOwnerPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_PERMISSIONS);
            Task<AuthorizationResult> hasAdministratorPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_PERMISSIONS);
            Task<AuthorizationResult> hasUserPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_PERMISSIONS);

            await Task.WhenAll(hasOwnerPermissionTask, hasAdministratorPermissionTask, hasUserPermissionTask);

            PermissionHolder permissionHolder = new PermissionHolder((await hasOwnerPermissionTask).Succeeded, (await hasAdministratorPermissionTask).Succeeded, (await hasUserPermissionTask).Succeeded);

            foreach (var section in ManicNetPermissions.sections)
            {
                permissionSection.Add(new PermissionSection(section, claims, permissionHolder.HasOwnerPermission, permissionHolder.HasAdministratorPermission, permissionHolder.HasUserPermission));
            }

            return permissionSection;
        }

        public async Task<RoleSection> BuildRoleSection(ManicNetUser user, ClaimsPrincipal adminUser)
        {
            RoleSection roleSection = new RoleSection();

            Task<AuthorizationResult> hasOwnerPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_ROLES);
            Task<AuthorizationResult> hasAdministratorPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_ROLES);
            Task<AuthorizationResult> hasUserPermissionTask = authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_ROLES);

            await Task.WhenAll(hasOwnerPermissionTask, hasAdministratorPermissionTask, hasUserPermissionTask);

            roleSection.HasOwnerPermission = (await hasOwnerPermissionTask).Succeeded;
            roleSection.HasAdministratorPermission = (await hasAdministratorPermissionTask).Succeeded;
            roleSection.HasNormalPermission = (await hasUserPermissionTask).Succeeded;

            List<ManicNetRole> roles = roleManager.Roles.ToList();
            List<string> userRoles = (await userManager.GetRolesAsync(user)).ToList();

            foreach (ManicNetRole role in roles)
            {
                List<Claim> roleClaims = (await roleManager.GetClaimsAsync(role)).Where(c => c.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE).ToList();

                bool hasRole = userRoles.Contains(role.Name);

                if (roleClaims.Any(c => ManicNetPermissions.OwnerPermissions.Contains(c.Value))) //Role has owner claim
                {
                    roleSection.OwnerRoles.Add(new SectionRole(role.Name, hasRole));
                }
                else if (roleClaims.Any(c => ManicNetPermissions.AdministratorPermissions.Contains(c.Value))) //Role has admin claim
                {
                    roleSection.AdministratorRoles.Add(new SectionRole(role.Name, hasRole));
                }
                else //Role is only normal claims
                {
                    roleSection.NormalRoles.Add(new SectionRole(role.Name, hasRole));
                }
            }

            return roleSection;
        }

        public async Task<UpdateClaimsHolder> CreatePermissionAdditionAndRemovalLists(IEnumerable<PermissionSection> permissionSections, ManicNetUser user, ClaimsPrincipal adminUser)
        {
            ManicNetUser manicAdminUser = await userManager.GetUserAsync(adminUser);

            List<Claim> claims = new List<Claim>(await userManager.GetClaimsAsync(user));

            UpdateClaimsHolder updateClaimsHolder = new UpdateClaimsHolder(claims);

            AuthorizationResult hasOwnerPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_PERMISSIONS);
            AuthorizationResult hasAdministratorPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_PERMISSIONS);
            AuthorizationResult hasUserPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_PERMISSIONS);

            PermissionHolder permissionHolder = new PermissionHolder(hasOwnerPermission.Succeeded, hasAdministratorPermission.Succeeded, hasUserPermission.Succeeded);

            foreach (PermissionSection section in permissionSections)
            {
                if (permissionHolder.HasOwnerPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.OwnerPermissions, updateClaimsHolder, user, manicAdminUser);
                }

                if (permissionHolder.HasAdministratorPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.AdministratorPermissions, updateClaimsHolder, user, manicAdminUser);
                }

                if (permissionHolder.HasUserPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.NormalPermissions, updateClaimsHolder, user, manicAdminUser);
                }
            }

            return updateClaimsHolder;
        }

        public async Task<UpdateRolesHolder> CreateRoleAdditionAndRemovalLists(RoleSection roleSection, ManicNetUser user, ClaimsPrincipal adminUser)
        {
            ManicNetUser manicAdminUser = await userManager.GetUserAsync(adminUser);

            List<string> roleNames = new List<string>(await userManager.GetRolesAsync(user));
            List<ManicNetRole> roles = roleManager.Roles.Where(r => roleNames.Contains(r.Name)).ToList();

            UpdateRolesHolder updateRolesHolder = new UpdateRolesHolder(roles);

            AuthorizationResult hasOwnerPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_ROLES);
            AuthorizationResult hasAdministratorPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_ROLES);
            AuthorizationResult hasUserPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_ROLES);

            PermissionHolder permissionHolder = new PermissionHolder(hasOwnerPermission.Succeeded, hasAdministratorPermission.Succeeded, hasUserPermission.Succeeded);

            if (permissionHolder.HasOwnerPermission)
            {
                updateRolesHolder = ProcessRoleSection(roleSection.OwnerRoles, updateRolesHolder, user, manicAdminUser);
            }

            if (permissionHolder.HasAdministratorPermission)
            {
                updateRolesHolder = ProcessRoleSection(roleSection.AdministratorRoles, updateRolesHolder, user, manicAdminUser);
            }

            if (permissionHolder.HasUserPermission)
            {
                updateRolesHolder = ProcessRoleSection(roleSection.NormalRoles, updateRolesHolder, user, manicAdminUser);
            }

            return updateRolesHolder;
        }

        public async Task<UpdateClaimsHolder> CreatePermissionAdditionAndRemovalLists(IEnumerable<PermissionSection> permissionSections, ManicNetRole role, ClaimsPrincipal adminUser)
        {
            ManicNetUser manicAdminUser = await userManager.GetUserAsync(adminUser);

            List<Claim> claims = new List<Claim>(await roleManager.GetClaimsAsync(role));

            UpdateClaimsHolder updateClaimsHolder = new UpdateClaimsHolder(claims);

            AuthorizationResult hasOwnerPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_OWNER_PERMISSIONS);
            AuthorizationResult hasAdministratorPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_ADMINISTRATOR_PERMISSIONS);
            AuthorizationResult hasUserPermission = await authorizationService.Authorize(adminUser, Policies.Owner.EDIT_USER_PERMISSIONS);

            PermissionHolder permissionHolder = new PermissionHolder(hasOwnerPermission.Succeeded, hasAdministratorPermission.Succeeded, hasUserPermission.Succeeded);

            foreach (PermissionSection section in permissionSections)
            {
                if (permissionHolder.HasOwnerPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.OwnerPermissions, updateClaimsHolder, role, manicAdminUser);
                }

                if (permissionHolder.HasAdministratorPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.AdministratorPermissions, updateClaimsHolder, role, manicAdminUser);
                }

                if (permissionHolder.HasUserPermission)
                {
                    updateClaimsHolder = ProcessPermissionSection(section.NormalPermissions, updateClaimsHolder, role, manicAdminUser);
                }
            }

            return updateClaimsHolder;
        }

        private UpdateClaimsHolder ProcessPermissionSection(List<ManicNetPermission> permissions, UpdateClaimsHolder updateClaimsHolder, ManicNetUser user, ManicNetUser adminUser)
        {
            const string functionIdentifier = "ProcessPermissionSection(permissions, updateClaimsHolder, user, adminUser)";

            foreach (ManicNetPermission manicNetPermission in permissions)
            {
                Claim permission = new Claim(ManicNetPermissions.PERMISSION_CLAIM_TYPE, manicNetPermission.PermissionName);

                if (manicNetPermission.HasPermission && (!updateClaimsHolder.UserClaims.Any(c => c.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE && c.Value == manicNetPermission.PermissionName)))
                {
                    updateClaimsHolder.ClaimsToAdd.Add(permission);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued permission {2} to add to user {3} ({4})", adminUser.UserName, adminUser.Id, manicNetPermission.PermissionName, user.UserName, user.Id));
                }
                else if ((!manicNetPermission.HasPermission) && (updateClaimsHolder.UserClaims.Any(c => c.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE && c.Value == manicNetPermission.PermissionName)))
                {
                    updateClaimsHolder.ClaimsToRemove.Add(permission);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued permission {2} to remove from user {3} ({4})", adminUser.UserName, adminUser.Id, manicNetPermission.PermissionName, user.UserName, user.Id));
                }
            }

            return updateClaimsHolder;
        }

        private UpdateRolesHolder ProcessRoleSection(List<SectionRole> roles, UpdateRolesHolder updateRolesHolder, ManicNetUser user, ManicNetUser adminUser)
        {
            const string functionIdentifier = "UpdateRolesHolder(roles, updateRolesHolder, user, adminUser)";

            foreach (SectionRole role in roles)
            {
                if (role.HasRole && (!updateRolesHolder.UserRoles.Any(r => r.Name == role.RoleName)))
                {
                    updateRolesHolder.RolesToAdd.Add(role);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued role {2} to add to user {3} ({4})", adminUser.UserName, adminUser.Id, role.RoleName, user.UserName, user.Id));
                }
                else if ((!role.HasRole) && (updateRolesHolder.UserRoles.Any(r => r.Name == role.RoleName)))
                {
                    updateRolesHolder.RolesToRemove.Add(role);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued role {2} to remove from user {3} ({4})", adminUser.UserName, adminUser.Id, role.RoleName, user.UserName, user.Id));
                }
            }

            return updateRolesHolder;
        }

        private UpdateClaimsHolder ProcessPermissionSection(List<ManicNetPermission> permissions, UpdateClaimsHolder updateClaimsHolder, ManicNetRole role, ManicNetUser adminUser)
        {
            const string functionIdentifier = "ProcessPermissionSection(permissions, updateClaimsHolder, role, adminUser)";

            foreach (ManicNetPermission manicNetPermission in permissions)
            {
                Claim permission = new Claim(ManicNetPermissions.PERMISSION_CLAIM_TYPE, manicNetPermission.PermissionName);

                if (manicNetPermission.HasPermission && (!updateClaimsHolder.UserClaims.Any(c => c.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE && c.Value == manicNetPermission.PermissionName)))
                {
                    updateClaimsHolder.ClaimsToAdd.Add(permission);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued permission {2} to add to user {3} ({4})", adminUser.UserName, adminUser.Id, manicNetPermission.PermissionName, role.Name, role.Id));
                }
                else if ((!manicNetPermission.HasPermission) && (updateClaimsHolder.UserClaims.Any(c => c.Type == ManicNetPermissions.PERMISSION_CLAIM_TYPE && c.Value == manicNetPermission.PermissionName)))
                {
                    updateClaimsHolder.ClaimsToRemove.Add(permission);
                    logger.LogInformation(path, functionIdentifier, string.Format("User {0} ({1}) queued permission {2} to remove from user {3} ({4})", adminUser.UserName, adminUser.Id, manicNetPermission.PermissionName, role.Name, role.Id));
                }
            }

            return updateClaimsHolder;
        }
    }
}
