﻿using ManicNet.Contracts.Services;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ManicNetAuthorizationService : IManicNetAuthorizationService
    {
        private readonly IAuthorizationService authorizationService;

        public ManicNetAuthorizationService(IAuthorizationService authorizationService)
        {
            this.authorizationService = authorizationService;
        }

        public async Task<AuthorizationResult> Authorize(ClaimsPrincipal user, string policyName)
        {
            return await authorizationService.AuthorizeAsync(user, policyName);
        }
    }
}
