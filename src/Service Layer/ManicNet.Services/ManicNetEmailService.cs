﻿using FluentEmail.Core;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Email.Models;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ManicNetEmailService : IManicNetEmailService
    {
        private readonly IFluentEmail emailSender;
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.ManicNetEmailService";
        private readonly string templatePath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Views", "Templates");

        public ManicNetEmailService(IFluentEmail emailSender, IOptions<ManicNetSettings> settings, IManicNetLoggerService logger)
        {
            this.emailSender = emailSender;
            this.settings = settings;
            this.logger = logger;
        }

        public async Task SendResetPasssword(string email, string userName, string passwordResetURL)
        {
            const string functionIdentifier = "SendResetPasssword(email, userName, passwordResetURL)";
            const string HTMLTemplateName = "ResetPasswordEmail.cshtml";
            const string plaintextTemplateName = "ResetPasswordEmailPlaintext.cshtml";

            try
            {
                string subject = string.Format("Reset Password");

                ResetPasswordEmailModel model = new ResetPasswordEmailModel
                {
                    Username = userName,
                    PasswordResetUrl = passwordResetURL,
                    SiteName = settings.Value.Metadata.SiteName
                };

                logger.LogInformation(path, functionIdentifier, string.Format("Sending Password Reset Email to {0}", email));

                if (settings.Value.EmailSettings.EmailType == Constants.Email.Types.NONE)
                {
                    logger.LogWarning(path, functionIdentifier, "Swallowing Password Reset Email. Email handler is set to NONE");
                    return;
                }

                var response = await emailSender
                    .To(email)
                    .Subject(subject)
                    .UsingTemplateFromFile($"{Path.Combine(templatePath, HTMLTemplateName)}", model)
                    .PlaintextAlternativeUsingTemplateFromFile($"{Path.Combine(templatePath, plaintextTemplateName)}", model)
                    .SendAsync();

                if (!response.Successful)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Failed sending Password Reset Email to {0}. \n{1}", email, string.Join(Environment.NewLine, response.ErrorMessages)));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0}, username:{1} }}", email, userName));
            }
        }

        public async Task SendAccountRegistration(string email, string userName, string accountConfirmationURL)
        {
            const string functionIdentifier = "SendAccountRegistration(email, userName, accountConfirmationURL)";
            const string HTMLTemplateName = "AccountRegistrationEmail.cshtml";
            const string plaintextTemplateName = "AccountRegistrationEmailPlaintext.cshtml";

            try
            {
                string subject = string.Format("Account Registration");

                AccountRegistrationEmailModel model = new AccountRegistrationEmailModel
                {
                    Username = userName,
                    AccountConfirmationUrl = accountConfirmationURL,
                    SiteName = settings.Value.Metadata.SiteName,
                };

                logger.LogInformation(path, functionIdentifier, string.Format("Sending Account Registration Email to {0}", email));

                if (settings.Value.EmailSettings.EmailType == Constants.Email.Types.NONE)
                {
                    logger.LogWarning(path, functionIdentifier, "Swallowing Account Registration Email. Email handler is set to NONE");
                    return;
                }

                var response = await emailSender
                    .To(email)
                    .Subject(subject)
                    .UsingTemplateFromFile($"{Path.Combine(templatePath, HTMLTemplateName)}", model)
                    .PlaintextAlternativeUsingTemplateFromFile($"{Path.Combine(templatePath, plaintextTemplateName)}", model)
                    .SendAsync();

                if (!response.Successful)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Failed sending Account Registration Email to {0}. \n{1}", email, string.Join(Environment.NewLine, response.ErrorMessages)));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0}, username:{1} }}", email, userName));
            }
        }

        public async Task SendEmailConfirmation(string email, string userName, string accountConfirmationURL)
        {
            const string functionIdentifier = "SendEmailConfirmation(email, userName, accountConfirmationURL)";
            const string HTMLTemplateName = "EmailConfirmationEmail.cshtml";
            const string plaintextTemplateName = "EmailConfirmationEmailPlaintext.cshtml";

            try
            {
                string subject = string.Format("Email Confirmation");

                EmailConfirmationEmailModel model = new EmailConfirmationEmailModel
                {
                    Username = userName,
                    AccountConfirmationUrl = accountConfirmationURL,
                    SiteName = settings.Value.Metadata.SiteName,
                };

                logger.LogInformation(path, functionIdentifier, string.Format("Sending Email Confirmation Email to {0}", email));

                if (settings.Value.EmailSettings.EmailType == Constants.Email.Types.NONE)
                {
                    logger.LogWarning(path, functionIdentifier, "Swallowing Email Confirmation Email. Email handler is set to NONE");
                    return;
                }

                var response = await emailSender
                    .To(email)
                    .Subject(subject)
                    .UsingTemplateFromFile($"{Path.Combine(templatePath, HTMLTemplateName)}", model)
                    .PlaintextAlternativeUsingTemplateFromFile($"{Path.Combine(templatePath, plaintextTemplateName)}", model)
                    .SendAsync();

                if (!response.Successful)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Failed sending Email Confirmation Email to {0}. \n{1}", email, string.Join(Environment.NewLine, response.ErrorMessages)));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0}, username:{1}}}", email, userName));
            }
        }

        public async Task SendRegistrationInvite(string email, Guid registrationInviteId, DateTime? expires)
        {
            const string functionIdentifier = "SendRegistrationInvite(email, registrationInviteId, expiryDate)";
            const string HTMLTemplateName = "RegistrationInviteEmail.cshtml";
            const string plaintextTemplateName = "RegistrationInviteEmailPlaintext.cshtml";

            try
            {
                string subject = string.Format("Registration Invite");

                RegistrationInviteEmailModel model = new RegistrationInviteEmailModel
                {
                    RegistrationInviteId = registrationInviteId,
                    Expires = expires,
                    SiteName = settings.Value.Metadata.SiteName
                };

                logger.LogInformation(path, functionIdentifier, string.Format("Sending Registration Invite Email to {0}", email));

                if (settings.Value.EmailSettings.EmailType == Constants.Email.Types.NONE)
                {
                    logger.LogWarning(path, functionIdentifier, "Swallowing Registration Invite Email. Email handler is set to NONE");
                    return;
                }

                var response = await emailSender
                    .To(email)
                    .Subject(subject)
                    .UsingTemplateFromFile($"{Path.Combine(templatePath, HTMLTemplateName)}", model)
                    .PlaintextAlternativeUsingTemplateFromFile($"{Path.Combine(templatePath, plaintextTemplateName)}", model)
                    .SendAsync();

                if (!response.Successful)
                {
                    logger.LogWarning(path, functionIdentifier, string.Format("Failed sending Registration Invite Email Email to {0}. \n{1}", email, string.Join(Environment.NewLine, response.ErrorMessages)));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0}, registrationInviteId:{1}, expires:{2} }}", email, registrationInviteId, expires));
            }
        }
    }
}
