﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class CacheCleanerService : ICacheCleanerService
    {
        private readonly IDistributedCache cache;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.CacheCleanerService";

        public CacheCleanerService(IDistributedCache cache, IManicNetLoggerService logger)
        {
            this.cache = cache;
            this.logger = logger;
        }

        public async Task UpdateCacheForWork(WorkCacheCleanup workCacheCleanup)
        {
            const string functionIdentifier = "UpdateCacheForWork(workCacheCleanup)";

            try
            {
                List<Task> tasks = new List<Task>();

                foreach (Guid workID in workCacheCleanup.Works)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(workID)));
                }

                foreach (Guid volumeID in workCacheCleanup.Volumes)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualVolumeCachingKey(volumeID)));
                }

                foreach (Guid chapterID in workCacheCleanup.Chapters)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualChapterCachingKey(chapterID)));
                }

                foreach (Guid tagID in workCacheCleanup.Tags)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualTagCachingKey(tagID)));
                }

                foreach (Guid collectionID in workCacheCleanup.Collections)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualCollectionCachingKey(collectionID)));
                }

                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllWorkCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllVolumeCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllChapterCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllCollectionCachingKey()));

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ workCacheCleanup:{0} }}", workCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCacheForCollection(CollectionCacheCleanup collectionCacheCleanup)
        {
            const string functionIdentifier = "UpdateCacheForCollection(collectionCacheCleanup)";

            try
            {
                List<Task> tasks = new List<Task>();

                foreach (Guid collection in collectionCacheCleanup.Collections)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualCollectionCachingKey(collection)));
                }

                foreach (Guid work in collectionCacheCleanup.Works)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(work)));
                }

                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllCollectionCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllWorkCachingKey()));

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collectionCacheCleanup:{0} }}", collectionCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCacheForRegistrationInvite(RegistrationInviteCacheCleanup registrationInviteCacheCleanup)
        {
            const string functionIdentifier = "UpdateCacheForRegistrationInvite(registrationInviteCacheCleanup)";

            try
            {
                List<Task> tasks = new List<Task>();

                foreach (Guid registrationInvite in registrationInviteCacheCleanup.RegistrationInvites)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualRegistrationInviteCachingKey(registrationInvite)));
                }

                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllRegistrationInviteCachingKey()));

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInviteCacheCleanup:{0} }}", registrationInviteCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCacheForTag(TagCacheCleanup tagCacheCleanup)
        {
            const string functionIdentifier = "UpdateCacheForTag(tagCacheCleanup)";

            try
            {
                List<Task> tasks = new List<Task>();

                foreach (Guid tag in tagCacheCleanup.Tags.Concat(tagCacheCleanup.RelatedTags).Distinct())
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualTagCachingKey(tag)));
                }

                foreach (Guid work in tagCacheCleanup.Works)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(work)));
                }

                foreach (Guid volume in tagCacheCleanup.Volumes)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(volume)));
                }

                foreach (Guid chapter in tagCacheCleanup.Chapters)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(chapter)));
                }

                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllWorkCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllVolumeCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllChapterCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagAliasCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllUserBlockedTagsCachingKey()));

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagCacheCleanup:{0} }}", tagCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCacheForTagType(TagTypeCacheCleanup tagTypeCacheCleanup)
        {
            const string functionIdentifier = "UpdateCacheForTagType(tagTypeCacheCleanup)";

            try
            {
                List<Task> tasks = new List<Task>();

                foreach (Guid tagTypeID in tagTypeCacheCleanup.TagTypes)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualTagTypeCachingKey(tagTypeID)));
                }

                foreach (Guid tagID in tagTypeCacheCleanup.Tags)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualTagCachingKey(tagID)));
                }

                foreach (Guid workID in tagTypeCacheCleanup.Works)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualWorkCachingKey(workID)));
                }

                foreach (Guid volumeID in tagTypeCacheCleanup.Volumes)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualVolumeCachingKey(volumeID)));
                }

                foreach (Guid chapterID in tagTypeCacheCleanup.Chapters)
                {
                    tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetIndividualChapterCachingKey(chapterID)));
                }

                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagAliasCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllTagTypeCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllWorkCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllVolumeCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllChapterCachingKey()));
                tasks.Add(cache.RemoveAsync(CacheIdentifierHelper.GetAllUserBlockedTagsCachingKey()));

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{tagTypeCacheCleanup:{0}}}", tagTypeCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCacheForUserBlockedTags()
        {
            const string functionIdentifier = "UpdateCacheForUserBlockedTags()";

            try
            {
                List<Task> tasks = new List<Task>
                {
                    cache.RemoveAsync(CacheIdentifierHelper.GetAllUserBlockedTagsCachingKey())
                };

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task UpdateCacheForCacheConfiguration()
        {
            const string functionIdentifier = "UpdateCacheForCacheConfiguration()";

            try
            {
                List<Task> tasks = new List<Task>
                {
                    cache.RemoveAsync(CacheIdentifierHelper.GetAllCacheConfigurationCachingKey())
                };

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task UpdateCacheForSocialLink(Guid socialLinkID)
        {
            const string functionIdentifier = "UpdateCacheForSocialLink(socialLinkID)";

            try
            {
                List<Task> tasks = new List<Task>
                {
                    cache.RemoveAsync(CacheIdentifierHelper.GetIndividualSocialLinkCachingKey(socialLinkID)),
                    cache.RemoveAsync(CacheIdentifierHelper.GetAllSocialLinkCachingKey())
                };

                await Task.WhenAll(tasks);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
