﻿using Microsoft.AspNetCore.Authorization;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericTagAdministrationRequirement : IAuthorizationRequirement
    {
        public GenericTagAdministrationRequirement()
        {

        }
    }
}
