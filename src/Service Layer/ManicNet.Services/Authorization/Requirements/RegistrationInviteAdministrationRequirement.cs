﻿using Microsoft.AspNetCore.Authorization;

namespace ManicNet.Services.Authorization
{
    public sealed class RegistrationInviteAdministrationRequirement : IAuthorizationRequirement
    {
        public RegistrationInviteAdministrationRequirement()
        {

        }
    }
}
