﻿using Microsoft.AspNetCore.Authorization;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericPermissionRequirement : IAuthorizationRequirement
    {
        public string Permission { get; set; }

        public GenericPermissionRequirement(string permission)
        {
            this.Permission = permission;
        }
    }
}
