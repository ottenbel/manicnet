﻿using Microsoft.AspNetCore.Authorization;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericConfigurationAdministrationRequirement : IAuthorizationRequirement
    {
        public GenericConfigurationAdministrationRequirement()
        {

        }
    }
}
