﻿using Microsoft.AspNetCore.Authorization;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericCachingConfigurationAdministrationRequirement : IAuthorizationRequirement
    {
        public GenericCachingConfigurationAdministrationRequirement()
        {

        }
    }
}
