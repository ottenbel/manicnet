﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericTagAdministrationHandler : AuthorizationHandler<GenericTagAdministrationRequirement>
    {
        public List<Claim> Permissions { get; set; }

        public GenericTagAdministrationHandler()
        {
            Permissions = new List<Claim>()
            {
                //Tag type permissions
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Owner.DELETE_TAG_TYPE),
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Administrator.EDIT_TAG_TYPE),
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Administrator.VIEW_DELETED_TAG_TYPE),

                //Tag permissions
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Owner.DELETE_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Owner.MIGRATE_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Administrator.EDIT_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Administrator.VIEW_DELETED_TAG),
            };
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       GenericTagAdministrationRequirement requirement)
        {
            foreach (var claim in Permissions)
            {
                if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;
        }
    }
}
