﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Services.Authorization
{
    public sealed class UserAdministrationHandler : AuthorizationHandler<UserAdministrationRequirement>
    {
        public List<Claim> Permissions { get; set; }

        public UserAdministrationHandler()
        {
            Permissions = new List<Claim>()
            {
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_OWNER_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_ADMINISTRATOR_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_USER_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_OWNER_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_ADMINISTRATOR_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_USER_ROLES),
            };
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       UserAdministrationRequirement requirement)
        {
            foreach (var claim in Permissions)
            {
                if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;
        }
    }
}
