﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Services.Authorization
{
    public sealed class AdministrationHandler : AuthorizationHandler<AdministrationRequirement>
    {
        public List<Claim> Permissions { get; set; }

        public AdministrationHandler()
        {
            Permissions = new List<Claim>()
            {
                //User permissions
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_OWNER_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_ADMINISTRATOR_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_USER_PERMISSIONS),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_OWNER_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_ADMINISTRATOR_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, User.Owner.EDIT_USER_ROLES),

                //Role permissions
                new Claim(PERMISSION_CLAIM_TYPE, Role.Owner.EDIT_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, Role.Owner.DELETE_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, Role.Owner.REASSIGN_DEFAULT_USER_ROLE),

                //Tag type permissions
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Owner.DELETE_TAG_TYPE),
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Administrator.EDIT_TAG_TYPE),
                new Claim(PERMISSION_CLAIM_TYPE, TagType.Administrator.VIEW_DELETED_TAG_TYPE),

                //Tag permissions
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Owner.DELETE_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Owner.MIGRATE_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Administrator.EDIT_TAG),
                new Claim(PERMISSION_CLAIM_TYPE, Tag.Administrator.VIEW_DELETED_TAG),

                //Registration invitge permissions
                new Claim(PERMISSION_CLAIM_TYPE, RegistrationInvite.Owner.DELETE_REGISTRATION_INVITE),
                new Claim(PERMISSION_CLAIM_TYPE, RegistrationInvite.Administrator.EDIT_REGISTRATION_INVITE),

                //Work prermissions
                new Claim(PERMISSION_CLAIM_TYPE, Work.Owner.DELETE_WORK),
                new Claim(PERMISSION_CLAIM_TYPE, Work.Administrator.EDIT_WORK),
                new Claim(PERMISSION_CLAIM_TYPE, Work.Administrator.VIEW_DELETED_WORK),

                //Collection permissions
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Owner.DELETE_COLLECTION),
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Administrator.EDIT_COLLECTION),
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Administrator.VIEW_DELETED_COLLECTION),

                //Configuration permissions

                //Cache configurations
                new Claim(PERMISSION_CLAIM_TYPE, SiteConfiguration.Cache.Owner.MANAGE_CACHE_CONFIGURATIONS),

                //Social link permissions
                new Claim(PERMISSION_CLAIM_TYPE, SocialLink.Owner.DELETE_SOCIAL_LINK),
                new Claim(PERMISSION_CLAIM_TYPE, SocialLink.Administrator.EDIT_SOCIAL_LINK),
                new Claim(PERMISSION_CLAIM_TYPE, SocialLink.Administrator.VIEW_DELETED_SOCIAL_LINK),
            };
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       AdministrationRequirement requirement)
        {
            foreach (var claim in Permissions)
            {
                if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;
        }
    }
}
