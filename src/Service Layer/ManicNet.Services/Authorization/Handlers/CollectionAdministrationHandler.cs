﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Services.Authorization
{
    public sealed class CollectionAdministrationHandler : AuthorizationHandler<CollectionAdministrationRequirement>
    {
        public List<Claim> Permissions { get; set; }

        public CollectionAdministrationHandler()
        {
            Permissions = new List<Claim>()
            {
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Owner.DELETE_COLLECTION),
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Administrator.EDIT_COLLECTION),
                new Claim(PERMISSION_CLAIM_TYPE, Collection.Administrator.VIEW_DELETED_COLLECTION),
            };
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       CollectionAdministrationRequirement requirement)
        {
            foreach (var claim in Permissions)
            {
                if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;
        }
    }
}
