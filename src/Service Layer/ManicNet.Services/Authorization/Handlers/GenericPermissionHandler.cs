﻿using ManicNet.Constants;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services.Authorization
{
    public sealed class GenericPermissionHandler : AuthorizationHandler<GenericPermissionRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                   GenericPermissionRequirement requirement)
        {
            Claim claim = new Claim(ManicNetPermissions.PERMISSION_CLAIM_TYPE, requirement.Permission);

            if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}
