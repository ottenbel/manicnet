﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Services.Authorization
{
    public sealed class RoleAdministrationHandler : AuthorizationHandler<RoleAdministrationRequirement>
    {
        public List<Claim> Permissions { get; set; }

        public RoleAdministrationHandler()
        {
            Permissions = new List<Claim>()
            {
                new Claim(PERMISSION_CLAIM_TYPE, Role.Owner.EDIT_ROLES),
                new Claim(PERMISSION_CLAIM_TYPE, Role.Owner.DELETE_ROLES),
            };
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context,
                                                       RoleAdministrationRequirement requirement)
        {
            foreach (var claim in Permissions)
            {
                if (context.User.HasClaim(c => string.Equals(c.Type, claim.Type, StringComparison.OrdinalIgnoreCase)
                    && string.Equals(c.Value, claim.Value, StringComparison.OrdinalIgnoreCase)))
                {
                    context.Succeed(requirement);
                    break;
                }
            }

            return Task.CompletedTask;
        }
    }
}
