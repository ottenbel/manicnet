﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class UserBlockedTagsRetrievalService : IUserBlockedTagsRetrievalService
    {
        private readonly IUserBlockedTagsRepository userBlockedTagsRepository;
        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.UserBlockedTagsRetrievalService";

        public UserBlockedTagsRetrievalService(IUserBlockedTagsRepository userBlockedTagsRepository, ITagRetrievalService tagRetrievalService, IManicNetLoggerService logger)
        {
            this.userBlockedTagsRepository = userBlockedTagsRepository;
            this.tagRetrievalService = tagRetrievalService;
            this.logger = logger;
        }

        public async Task<bool> HasUserBlockedTag(Guid tagID, Guid userID)
        {
            const string functionIdentifier = "HasUserBlockedTag(tagID, userID)";

            bool hasBlockedTag;

            try
            {
                hasBlockedTag = (await userBlockedTagsRepository.Find(tagID, userID)) != null;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagID:{0}, userID:{1} }}", tagID, userID));
                throw;
            }

            return hasBlockedTag;
        }

        public async Task<User_Blocked_Tag> FindUserBlockedTag(Guid tagID, ClaimsPrincipal user)
        {
            const string functionIdentifier = "FindUserBlockedTag(tagID, userID)";

            try
            {
                User_Blocked_Tag userBlockedTag = await userBlockedTagsRepository.Find(tagID, user.GetUserID().Value);
                if (userBlockedTag != null)
                {
                    try
                    {
                        Tag tag = await tagRetrievalService.FindTag(tagID, user);
                    }
                    catch (Exception)
                    {
                        //Swallow the error fo being unable to find the tag
                        return null;
                    }
                }

                return userBlockedTag;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagID:{0}, user:{1} }}", tagID, user));
                throw;
            }
        }

        public async Task<List<User_Blocked_Tag>> GetUserBlockedTags(Guid userID)
        {
            const string functionIdentifier = "GetUserBlockedTags(userID)";

            List<User_Blocked_Tag> userBlockedTags = new List<User_Blocked_Tag>();

            try
            {
                userBlockedTags = await userBlockedTagsRepository.Find(userID);
                List<Tag> tags = await tagRetrievalService.GetActiveTags();

                userBlockedTags = userBlockedTags.Where(ubt => tags.Select(t => t.TagID).Contains(ubt.TagID)).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ userID:{0} }}", userID));
                throw;
            }

            return userBlockedTags;
        }
    }
}
