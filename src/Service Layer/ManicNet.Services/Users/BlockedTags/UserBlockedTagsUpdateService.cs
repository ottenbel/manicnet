﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class UserBlockedTagsUpdateService : IUserBlockedTagsUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IUserBlockedTagsRepository userBlockedTagsRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;


        private readonly string path = "ManicNet.Services.UserBlockedTagsUpdateService";

        public UserBlockedTagsUpdateService(IManicNetUnitOfWork unitOfWork, IUserBlockedTagsRepository userBlockedTagsRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.userBlockedTagsRepository = userBlockedTagsRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
        }

        public async Task CreateUserBlockedTag(Guid tagID, Guid userID)
        {
            const string functionIdentifier = "CreateUserBlockedTag(tagID, userID)";

            try
            {
                User_Blocked_Tag userBlockedTag = new User_Blocked_Tag(tagID, userID);
                userBlockedTagsRepository.Insert(userBlockedTag);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForUserBlockedTags();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagID:{0}, userID:{1} }}", tagID, userID));
                throw;
            }
        }

        public async Task RemoveUserBlockedTag(User_Blocked_Tag userBlockedTag)
        {
            const string functionIdentifier = "RemoveUserBlockedTag(userBlockedTag)";

            try
            {
                userBlockedTagsRepository.Remove(userBlockedTag);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForUserBlockedTags();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ userBlockedTag:{0} }}", userBlockedTag));
                throw;
            }
        }
    }
}
