﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public class SocialLinkService : ISocialLinkService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly ISocialLinkRepository socialLinkRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;
        private readonly ICacheCleanerService cacheCleanerService;

        private readonly string path = "ManicNet.Services.SocialLinkService";

        public SocialLinkService(IManicNetUnitOfWork unitOfWork, ISocialLinkRepository socialLinkRepository, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService, ICacheCleanerService cacheCleanerService)
        {
            this.unitOfWork = unitOfWork;
            this.socialLinkRepository = socialLinkRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
            this.cacheCleanerService = cacheCleanerService;
        }

        public async Task<List<SocialLink>> GetActiveSocialLinks()
        {
            const string functionIdentifier = "GetActiveSocialLinks()";
            
            try
            {
                List<SocialLink> socialLinks = (await socialLinkRepository.AllAsync()).Where(sl => sl.IsDeleted == false).ToList();

                return socialLinks;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<SocialLink>> GetDeletedSocialLinks(ClaimsPrincipal user)
        {
            const string functionIdentifier = "GetDeletedSocialLinks(user)";

            List<SocialLink> socialLinks = new List<SocialLink>();

            try
            {
                if ((!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_SOCIAL_LINK)).Succeeded))
                {
                    return socialLinks;
                }

                socialLinks = (await socialLinkRepository.AllAsync())
                    .Where(x => x.IsDeleted == true).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ user:{0} }}", user));
                throw;
            }

            return socialLinks;
        }

        public async Task<bool> DoDeletedSocialLinksExist()
        {
            const string functionIdentifier = "DoDeletedSocialLinksExist()";

            try
            {
                bool result = (await socialLinkRepository.AllAsync()).Any(sl => sl.IsDeleted == true);

                return result;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task CreateSocialLink(SocialLink socialLink)
        {
            const string functionIdentifier = "CreateSocialLink(socialLink)";

            try
            {
                socialLinkRepository.Insert(socialLink);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForSocialLink(socialLink.SocialLinkID);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }

        public async Task<SocialLink> FindSocialLinkAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindSocialLinkAsync(id, user, bypassCache)";

            try
            {
                SocialLink socialLink = await socialLinkRepository.FindAsync(id, bypassCache);

                if (socialLink == null)
                {
                    throw new SocialLinkNotFoundException(string.Format("Social link with ID {0} not found", id));
                }

                if ((socialLink.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_SOCIAL_LINK)).Succeeded))
                {
                    throw new SocialLinkNotViewableException(string.Format("User does not have permission to view social link ID {0}", id));
                }

                return socialLink;
            }
            catch (SocialLinkNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Social link not found. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (SocialLinkNotViewableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Social link not viewable. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public async Task UpdateSocialLink(SocialLink socialLink)
        {
            const string functionIdentifier = "UpdateSocialLink(socialLink)";

            try
            {
                socialLinkRepository.Update(socialLink);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForSocialLink(socialLink.SocialLinkID);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }

        public async Task RemoveSocialLink(SocialLink socialLink)
        {
            const string functionIdentifier = "RemoveSocialLink(socialLink)";

            try
            {
                if (!socialLink.IsDeleted)
                {
                    throw new SocialLinkNotDeletableException(string.Format("Social link with ID {0} not is not in the state to be deleted", socialLink.SocialLinkID));
                }

                socialLinkRepository.Delete(socialLink);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForSocialLink(socialLink.SocialLinkID);
            }
            catch (SocialLinkNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Collection is not deletable. {{ socialLink:{0} }}", socialLink));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }
    }
}
