﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class CollectionUpdateService : ICollectionUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly ICollectionRepository collectionRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.CollectionUpdateService";

        public CollectionUpdateService(IManicNetUnitOfWork unitOfWork, ICollectionRepository collectionRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.collectionRepository = collectionRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
        }

        public async Task CreateCollection(Collection collection)
        {
            const string functionIdentifier = "CreateCollection(collection)";

            try
            {
                CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection);
                collectionRepository.Insert(collection);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCollection(collectionCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0} }}", collection));
                throw;
            }
        }

        public async Task UpdateCollection(Collection collection, CollectionCacheCleanup collectionCacheCleanup)
        {
            const string functionIdentifier = "UpdateCollection(collection, collectionCacheCleanup)";

            try
            {
                collectionRepository.Update(collection);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCollection(collectionCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0}, collectionCacheCleanup:{1} }}", collection, collectionCacheCleanup));
                throw;
            }
        }

        public async Task UpdateCollectionWithNewWork(Collection collection, Guid workID, int number)
        {
            const string functionIdentifier = "UpdateCollectionWithNewWork(collection, workID, number)";

            try
            {
                CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection, new List<Guid>() { workID });

                collectionRepository.UpdateWithWork(collection, workID, number);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCollection(collectionCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0}, workID:{1}, number:{2} }}", collection, workID, number));
                throw;
            }
        }

        public async Task RemoveCollection(Collection collection)
        {
            const string functionIdentifier = "RemoveCollection(collection)";

            try
            {
                if (!collection.IsDeleted)
                {
                    throw new CollectionNotDeletableException(string.Format("Collection with ID {0} not is not in the state to be deleted", collection.CollectionID));
                }

                CollectionCacheCleanup collectionCacheCleanup = new CollectionCacheCleanup(collection);

                collectionRepository.Delete(collection);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCollection(collectionCacheCleanup);
            }
            catch (WorkNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Collection is not deletable. {{ collection:{0} }}", collection));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0} }}", collection));
                throw;
            }
        }
    }
}
