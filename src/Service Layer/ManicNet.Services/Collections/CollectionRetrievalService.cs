﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class CollectionRetrievalService : ICollectionRetrievalService
    {
        private readonly ICollectionRepository collectionRepository;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;

        private readonly string path = "ManicNet.Services.CollectionRetrievalService";

        public CollectionRetrievalService(ICollectionRepository collectionRepository, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService)
        {
            this.collectionRepository = collectionRepository;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.logger = logger;
            this.authorizationService = authorizationService;
        }

        public async Task<List<Collection>> GetActiveCollections()
        {
            const string functionIdentifier = "GetActiveCollections()";

            List<Collection> collections = new List<Collection>();

            try
            {
                collections = (await collectionRepository.AllAsync()).Where(x => x.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return collections;
        }

        public async Task<List<Collection>> GetDeletedCollections()
        {
            const string functionIdentifier = "GetDeletedCollections()";

            List<Collection> collections = new List<Collection>();

            try
            {
                collections = (await collectionRepository.AllAsync()).Where(x => x.IsDeleted == true).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return collections;
        }


        public async Task<bool> DoDeletedCollectionsExist()
        {
            const string functionIdentifier = "DoDeletedCollectionsExist()";

            try
            {
                return (await collectionRepository.AllAsync()).Where(x => x.IsDeleted == true).Any();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<Collection> FindCollection(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindWorkAsync(id, user, bypassCache)";

            try
            {
                Collection collection = await collectionRepository.FindAsync(id, bypassCache);

                if (collection == null)
                {
                    throw new CollectionNotFoundException(string.Format("Collection with ID {0} not found", id));
                }

                if ((collection.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_WORK)).Succeeded))
                {
                    throw new CollectionNotViewableException(string.Format("User does not have permission to view collection with ID {0}", id));
                }

                Guid? userID = user.GetUserID();

                //Make sure we only filter the works when we are retrieving for the front end [This prevents admin users with blocked tags from removing works from the collection due to them not appearing]
                if (userID.HasValue && !bypassCache)
                {
                    List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                    List<Collection_Work> filteredWorks = collection.Works.Where(w => !w.Work.Tags.Select(wt => wt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Work.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Tags.Where(t => t.Tag.IsDeleted == false)).Select(vt => vt.TagID).Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()
                        && !w.Work.Volumes.Where(v => v.IsDeleted == false).SelectMany(v => v.Chapters.Where(c => c.IsDeleted == false).SelectMany(c => c.Tags.Where(t => t.Tag.IsDeleted == false))).Select(vt => vt.TagID)
                            .Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any()).ToList();

                    collection.Works = filteredWorks;
                }

                return collection;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }
    }
}
