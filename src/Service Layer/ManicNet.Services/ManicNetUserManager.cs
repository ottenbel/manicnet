﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ManicNetUserManager : UserManager<ManicNetUser>
    {
        private readonly IAvatarService avatarService;

        public ManicNetUserManager(IUserStore<ManicNetUser> store, IOptions<IdentityOptions> optionsAccessor, IPasswordHasher<ManicNetUser> passwordHasher, IEnumerable<IUserValidator<ManicNetUser>> userValidators, IEnumerable<IPasswordValidator<ManicNetUser>> passwordValidators, ILookupNormalizer keyNormalizer, IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<ManicNetUser>> logger, IAvatarService avatarService)
            : base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            this.avatarService = avatarService;
        }

        public override async Task<ManicNetUser> GetUserAsync(ClaimsPrincipal principal)
        {
            //Get default basic user
            ManicNetUser user = await base.GetUserAsync(principal);

            //Get user avatar
            if (user?.AvatarID != null)
            {
                user.Avatar = await avatarService.FindAvatar(user.AvatarID.Value);
            }

            return user;
        }

        public async Task<Avatar> GetAvatarAsync(ClaimsPrincipal principal)
        {
            //Get default basic user
            ManicNetUser user = await base.GetUserAsync(principal);
            Avatar avatar = null;

            //Get user avatar
            if (user?.AvatarID != null)
            {
                avatar = await avatarService.FindAvatar(user.AvatarID.Value);
            }

            return avatar;
        }

        public async Task<Guid> GetRSSKeyAsync(ClaimsPrincipal principal)
        {
            ManicNetUser user = await base.GetUserAsync(principal);

            Guid rssKey = user?.RSSKey ?? Guid.Empty;

            return rssKey;
        }

        public bool ValidateRSSKey(Guid? rssKey)
        {
            bool valid = this.Users.Where(u => u.RSSKey == rssKey).Any();

            return valid;
        }

        public ManicNetUser GetUserFromRSSKey(Guid? rssKey)
        {
            ManicNetUser user = null;

            if (rssKey.HasValue)
            {
                user = this.Users.Where(u => u.RSSKey == rssKey).FirstOrDefault();
            }

            return user;
        }
    }
}
