﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class AvatarFileHandlerService : IAvatarFileHandlerService
    {
        private readonly IAvatarService avatarService;
        private readonly IManicNetLoggerService logger;
        private readonly IFileIOService fileIOService;
        private readonly IImageProcessingService imageProcessingService;

        private readonly string path = "ManicNet.Services.AvatarFileHandlerService";

        public AvatarFileHandlerService(IAvatarService avatarService, IManicNetLoggerService logger, IFileIOService fileIOService, IImageProcessingService imageProcessingService)
        {
            this.avatarService = avatarService;
            this.logger = logger;
            this.fileIOService = fileIOService;
            this.imageProcessingService = imageProcessingService;
        }

        public async Task<Guid> UploadAvatar(IFormFile image, byte[] hash)
        {
            const string functionIdentifier = "UploadAvatar(image, hash)";

            try
            {
                byte[] processedAvatar = await imageProcessingService.ProcessAvatar(image);
                string filename = await fileIOService.UploadAvatarAsync(processedAvatar, image.FileName);
                Avatar avatar = new Avatar(filename, hash);
                await avatarService.CreateAvatar(avatar);
                return avatar.AvatarID;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;

            }
        }

        public async Task CleanupAvatar(Guid? avatarID)
        {
            const string functionIdentifier = "CleanupAvatar(avatarID)";

            try
            {
                if (avatarID.HasValue)
                {
                    Avatar previousAvatar = await avatarService.FindAvatar(avatarID.Value);
                    if (previousAvatar.UserAvatars.Count - 1 == 0)
                    {
                        await avatarService.RemoveAvatar(previousAvatar);
                        await fileIOService.DeleteAvatar(previousAvatar.Filename);
                    }
                }
            }
            catch (AvatarNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Avatar not found. {{ avatarID:{0} }}", avatarID));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ avatarID:{0} }}", avatarID));
                throw;
            }
        }
    }
}
