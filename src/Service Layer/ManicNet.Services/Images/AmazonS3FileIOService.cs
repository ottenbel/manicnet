﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class AmazonS3FileIOService : IFileIOService
    {
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetLoggerService logger;

        private readonly AmazonS3Client avatarClient;
        private readonly AmazonS3Client thumbnailClient;
        private readonly AmazonS3Client contentClient;

        private readonly string path = "ManicNet.Services.AmazonS3StorageFileIOService";

        public AmazonS3FileIOService(IOptions<ManicNetSettings> settings, IManicNetLoggerService logger)
        {
            this.settings = settings;
            this.logger = logger;

            BasicAWSCredentials credentials = new BasicAWSCredentials(settings.Value.FileUploadSettings.AmazonS3Upload.AccessKey, settings.Value.FileUploadSettings.AmazonS3Upload.Secret);

            var avatarConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.Region) };
            var thumbnailConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.Region) };
            var contentConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.Value.FileUploadSettings.AmazonS3Upload.Content.Region) };

            avatarClient = new AmazonS3Client(credentials, avatarConfig);
            thumbnailClient = new AmazonS3Client(credentials, thumbnailConfig);
            contentClient = new AmazonS3Client(credentials, contentConfig);
        }

        public async Task<string> UploadAvatarAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadAvatar(file, inputName)";

            try
            {
                return await UploadFileAsync(file, avatarClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.Bucket, settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.AccessLevel);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadThumbnailAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadThumbnailAsync(file, inputName)";

            try
            {
                return await UploadFileAsync(file, thumbnailClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.Bucket, settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.AccessLevel);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadContentAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadContent(file, inputName)";

            try
            {
                return await UploadFileAsync(file, contentClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Content.Bucket, settings.Value.FileUploadSettings.AmazonS3Upload.Content.AccessLevel);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task DeleteAvatar(string inputName)
        {
            const string functionIdentifier = "DeleteAvatar(inputName)";

            try
            {
                await DeleteFile(avatarClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.Bucket);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task DeleteTumbnail(string inputName)
        {
            const string functionIdentifier = "DeleteTumbnail(inputName)";

            try
            {
                await DeleteFile(thumbnailClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.Bucket);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task DeleteContent(string inputName)
        {
            const string functionIdentifier = "DeleteContent(inputName)";

            try
            {
                await DeleteFile(contentClient, inputName, settings.Value.FileUploadSettings.AmazonS3Upload.Content.Bucket);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public string GetLink(Avatar avatar)
        {
            GetPreSignedUrlRequest urlRequest = new GetPreSignedUrlRequest
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.Bucket,
                Key = avatar.Filename,
                Expires = DateTime.UtcNow.AddHours(settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.SecureURLExpiryLength)
            };

            string url = avatarClient.GetPreSignedURL(urlRequest);

            if (!string.Equals(settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.AccessLevel, Uploads.AmazonS3.Bucket.PRIVATE, StringComparison.OrdinalIgnoreCase))
            {
                int keyStart = url.IndexOf("?");
                url = url.Remove(keyStart);
            }

            return url;
        }

        public string GetLink(Thumbnail thumbnail)
        {
            GetPreSignedUrlRequest urlRequest = new GetPreSignedUrlRequest
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.Bucket,
                Key = thumbnail.Filename,
                Expires = DateTime.UtcNow.AddHours(settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.SecureURLExpiryLength)
            };

            string url = thumbnailClient.GetPreSignedURL(urlRequest);

            if (!string.Equals(settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.AccessLevel, Uploads.AmazonS3.Bucket.PRIVATE, StringComparison.OrdinalIgnoreCase))
            {
                int keyStart = url.IndexOf("?");
                url = url.Remove(keyStart);
            }

            return url;
        }

        public string GetLink(Content content)
        {
            GetPreSignedUrlRequest urlRequest = new GetPreSignedUrlRequest
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Content.Bucket,
                Key = content.Filename,
                Expires = DateTime.UtcNow.AddHours(settings.Value.FileUploadSettings.AmazonS3Upload.Content.SecureURLExpiryLength)
            };

            string url = contentClient.GetPreSignedURL(urlRequest); ;

            if (!string.Equals(settings.Value.FileUploadSettings.AmazonS3Upload.Content.AccessLevel, Uploads.AmazonS3.Bucket.PRIVATE, StringComparison.OrdinalIgnoreCase))
            {
                int keyStart = url.IndexOf("?");
                url = url.Remove(keyStart);
            }

            return url;
        }

        public async Task<byte[]> GetFile(Avatar avatar)
        {
            byte[] file;
            GetObjectRequest getObjectRequest = new GetObjectRequest()
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Avatar.Bucket,
                Key = avatar.Filename,
            };
            GetObjectResponse result = await contentClient.GetObjectAsync(getObjectRequest);

            MemoryStream memoryStream = new MemoryStream();
            result.ResponseStream.CopyTo(memoryStream);
            file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetFile(Thumbnail thumbnail)
        {
            byte[] file;
            GetObjectRequest getObjectRequest = new GetObjectRequest()
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Thumbnail.Bucket,
                Key = thumbnail.Filename,
            };
            GetObjectResponse result = await contentClient.GetObjectAsync(getObjectRequest);

            MemoryStream memoryStream = new MemoryStream();
            result.ResponseStream.CopyTo(memoryStream);
            file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetFile(Content content)
        {
            byte[] file;
            GetObjectRequest getObjectRequest = new GetObjectRequest()
            {
                BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Content.Bucket,
                Key = content.Filename,
            };
            GetObjectResponse result = await contentClient.GetObjectAsync(getObjectRequest);

            MemoryStream memoryStream = new MemoryStream();
            result.ResponseStream.CopyTo(memoryStream);
            file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetDownloadChapter(string work, Chapter chapter)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    await CompressChapter(chapter, string.Empty, archive, work);
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadVolume(string work, Volume volume)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    await CompressVolume(volume, string.Empty, archive, work);
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadWork(Work work)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    foreach (Volume volume in work.Volumes.Where(v => v.IsDeleted == false))
                    {
                        DirectoryInfo volumeDirectory = new DirectoryInfo(string.Format("Volume {0}{1}", volume.Number, string.IsNullOrWhiteSpace(volume.Title) ? string.Empty : string.Format(" - {0}", volume.Title)));

                        await CompressVolume(volume, volumeDirectory.Name, archive, work.Title);
                    }
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }


        private async Task CompressVolume(Volume volume, string subfolder, ZipArchive archive, string work)
        {
            foreach (Chapter chapter in volume.Chapters.Where(c => c.IsDeleted == false))
            {
                DirectoryInfo chapterDirectory = new DirectoryInfo(string.Format("Chapter {0}{1}", chapter.Number, string.IsNullOrWhiteSpace(chapter.Title) ? string.Empty : string.Format(" - {0}", chapter.Title)));

                string subpath = Path.Join(subfolder, chapterDirectory.Name);

                await CompressChapter(chapter, subpath, archive, work);
            }
        }

        private async Task CompressChapter(Chapter chapter, string subfolder, ZipArchive archive, string work)
        {
            foreach (Page page in chapter.Pages)
            {
                GetObjectRequest getObjectRequest = new GetObjectRequest()
                {
                    BucketName = settings.Value.FileUploadSettings.AmazonS3Upload.Content.Bucket,
                    Key = page.Content.Filename,
                };
                GetObjectResponse result = await contentClient.GetObjectAsync(getObjectRequest);

                string extension = string.Empty;

                int extensionStart = page.Content.Filename.LastIndexOf('.');
                if (extensionStart != -1)
                {
                    extension = page.Content.Filename.Substring(extensionStart);
                }

                string filename = string.Format("[{0}] {1} - Chapter {2} - Page {3}{4}", settings.Value.Metadata.SiteName, work, chapter.Number, page.Number, extension);
                string filepath = string.IsNullOrWhiteSpace(subfolder) ? filename : Path.Join(subfolder, filename);

                var file = archive.CreateEntry(filepath, CompressionLevel.Optimal);
                using (Stream entryStream = file.Open())
                {
                    await result.ResponseStream.CopyToAsync(entryStream);
                }
            }
        }

        private async Task<string> UploadFileAsync(byte[] file, AmazonS3Client client, string inputName, string bucket, string accessLevel)
        {
            const string functionIdentifier = "UploadFileAsync(file, client, inputName)";

            string extension = string.Empty;

            int extensionStart = inputName.LastIndexOf('.');
            if (extensionStart != -1)
            {
                extension = inputName.Substring(extensionStart);
            }

            try
            {
                string filename;
                bool exists = true;
                do
                {
                    filename = Guid.NewGuid().ToString() + extension;
                    try
                    {
                        GetObjectMetadataRequest request = new GetObjectMetadataRequest
                        {
                            BucketName = bucket,
                            Key = filename
                        };
                        await client.GetObjectMetadataAsync(request);
                    }
                    catch (Exception)
                    {
                        exists = false;
                    }
                }
                while (exists);

                using (var stream = new MemoryStream(file, false))
                {
                    PutObjectRequest putRequest = new PutObjectRequest
                    {
                        InputStream = stream,
                        Key = filename,
                        BucketName = bucket,
                        CannedACL = string.Equals(accessLevel, Uploads.AmazonS3.Bucket.PUBLIC, StringComparison.OrdinalIgnoreCase) ? S3CannedACL.PublicRead : S3CannedACL.Private
                    };

                    await client.PutObjectAsync(putRequest);
                }

                return filename;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0}, bucket:{1} }}", inputName, bucket));
                throw;
            }
        }

        private async Task DeleteFile(AmazonS3Client client, string inputName, string bucket)
        {
            const string functionIdentifier = "DeleteFile(directory, inputName)";

            try
            {
                DeleteObjectRequest deleteRequest = new DeleteObjectRequest
                {
                    BucketName = bucket,
                    Key = inputName
                };

                await client.DeleteObjectAsync(deleteRequest);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0}, bucket:{1} }}", inputName, bucket));
                throw;
            }
        }
    }
}
