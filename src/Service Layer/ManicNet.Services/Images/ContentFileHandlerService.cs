﻿using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ContentFileHandlerService : IContentFileHandlerService
    {
        private readonly IContentService contentService;
        private readonly IThumbnailService thumbnailService;
        private readonly IManicNetLoggerService logger;
        private readonly IFileIOService fileIOService;
        private readonly IImageProcessingService imageProcessingService;
        private readonly IManicNetUnitOfWork unitOfWork;

        private readonly string path = "ManicNet.Services.ContentFileHandlerService";

        public ContentFileHandlerService(IContentService contentService, IThumbnailService thumbnailService, IManicNetLoggerService logger, IFileIOService fileIOService, IImageProcessingService imageProcessingService, IManicNetUnitOfWork unitOfWork)
        {
            this.contentService = contentService;
            this.thumbnailService = thumbnailService;
            this.logger = logger;
            this.fileIOService = fileIOService;
            this.imageProcessingService = imageProcessingService;
            this.unitOfWork = unitOfWork;
        }

        public async Task CleanupContent(Guid? contentID)
        {
            const string functionIdentifier = "CleanupContent(contentID)";

            try
            {
                if (contentID.HasValue)
                {
                    await HandleCleanupAsync(contentID.Value);
                }
            }
            catch (ContentNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Content not found. {{ contentID:{0} }}", contentID));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ contentID:{0} }}", contentID));
                throw;
            }
        }

        public async Task CleanupContent(IEnumerable<Guid> contents)
        {
            const string functionIdentifier = "CleanupContent(contents)";

            try
            {
                foreach (Guid contentID in contents)
                {
                    try
                    {
                        await HandleCleanupAsync(contentID);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(path, functionIdentifier, ex, string.Format("{{ contentID:{0} }}", contentID));
                    }
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ contents:{0} }}", contents));
                throw;
            }
        }

        private async Task HandleCleanupAsync(Guid contentID)
        {
            Content content = await contentService.FindContent(contentID);

            if (!(content.Works.Any() || content.Volumes.Any() || content.Pages.Any()))
            {
                Thumbnail thumbnail = content.Thumbnail;
                await contentService.RemoveContent(content, false);

                await unitOfWork.SaveAsync();

                Task deleteThumbnail = fileIOService.DeleteTumbnail(thumbnail.Filename);
                Task deleteContent = fileIOService.DeleteContent(content.Filename);

                await Task.WhenAll(deleteThumbnail, deleteContent);
            }
        }

        public async Task<Content> UploadContent(IFormFile image, byte[] hash)
        {
            const string functionIdentifier = "UploadContent(image, hash)";

            string contentFilename = string.Empty;
            string thumbnailFilename = string.Empty;

            try
            {
                byte[] imageBytes = await imageProcessingService.GetImageBytes(image);
                byte[] processedContent = imageProcessingService.ProcessContent(imageBytes);
                byte[] processedThumbnail = imageProcessingService.ProcessThumbnail(imageBytes);

                Task<string> contentFilenameTask = fileIOService.UploadContentAsync(processedContent, image.FileName);
                Task<string> thumbnailFilenameTask = fileIOService.UploadThumbnailAsync(processedThumbnail, image.FileName);

                await Task.WhenAll(contentFilenameTask, thumbnailFilenameTask);

                contentFilename = await contentFilenameTask;
                thumbnailFilename = await thumbnailFilenameTask;

                Content content = new Content(contentFilename, hash);
                Thumbnail thumbnail = new Thumbnail(thumbnailFilename, hash, content);

                var createContentTask = contentService.CreateContent(content, false);
                var createThumbnailTask = thumbnailService.CreateThumbnail(thumbnail, false);

                await Task.WhenAll(createContentTask, createThumbnailTask);

                await unitOfWork.SaveAsync();

                return content;
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrWhiteSpace(contentFilename))
                {
                    try
                    {
                        await fileIOService.DeleteContent(contentFilename);
                    }
                    catch (Exception) { /*Swallow the exception*/ }
                }

                if (!string.IsNullOrWhiteSpace(thumbnailFilename))
                {
                    try
                    {
                        await fileIOService.DeleteTumbnail(thumbnailFilename);
                    }
                    catch (Exception) { /*Swallow the exception*/ }
                }

                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }
        }
    }
}