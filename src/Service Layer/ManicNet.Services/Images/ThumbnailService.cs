﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ThumbnailService : IThumbnailService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IThumbnailRepository thumbnailRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.ThumbnailService";

        public ThumbnailService(IManicNetUnitOfWork unitOfWork, IThumbnailRepository thumbnailRepository, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.thumbnailRepository = thumbnailRepository;
            this.logger = logger;
        }

        public async Task<Thumbnail> FindThumbnail(Guid id)
        {
            const string functionIdentifier = "FindThumbnail(id)";

            try
            {
                Thumbnail thumbnail = await thumbnailRepository.FindAsync(id);

                if (thumbnail == null)
                {
                    throw new ThumbnailNotFoundException(string.Format("Thumbnail with ID {0} not found", id));
                }

                return thumbnail;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Thumbnail> FindThumbnail(byte[] hash)
        {
            const string functionIdentifier = "FindThumbnail(hash)";

            try
            {
                return await thumbnailRepository.FindAsync(hash);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }
        }

        public async Task CreateThumbnail(Thumbnail thumbnail, bool callAsTopLevel = true)
        {
            const string functionIdentifier = "CreateThumbnail(thumbnail, callAsTopLevel)";

            try
            {
                thumbnailRepository.Insert(thumbnail);
                if (callAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ thumbnail:{0}, callAsTopLevel:{1} }}", thumbnail, callAsTopLevel));
                throw;
            }
        }

        public async Task RemoveThumbnail(Thumbnail thumbnail, bool callAsTopLevel = true)
        {
            const string functionIdentifier = "RemoveThumbnail(thumbnail, callAsTopLevel)";

            try
            {
                thumbnailRepository.Delete(thumbnail);
                if (callAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                }

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ thumbnail:{0}, callAsTopLevel:{1} }}", thumbnail, callAsTopLevel));
                throw;
            }
        }
    }
}
