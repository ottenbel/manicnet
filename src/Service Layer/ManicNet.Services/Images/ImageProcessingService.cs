﻿using ImageMagick;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using static ManicNet.Domain.Enums.FileValidation;

namespace ManicNet.Services
{
    public sealed class ImageProcessingService : IImageProcessingService
    {
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetLoggerService logger;

        public const string NO_DECODE_DELEGATE = "no decode delegate for this image format";

        private readonly string path = "ManicNet.Services.ImageIOService";

        public ImageProcessingService(IOptions<ManicNetSettings> settings, IManicNetLoggerService logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        public async Task<FileValidationResult> ValidateImageAsync(IFormFile file)
        {
            const string functionIdentifier = "ValidateImageAsync(file)";

            FileValidationResult result = FileValidationResult.IsValid;

            try
            {
                if (file.Length > settings.Value.FileUploadSettings.MaximumFileSize)
                {
                    result = FileValidationResult.FileTooLarge;
                }
                else if (file.Length <= 0)
                {
                    result = FileValidationResult.FileTooSmall;
                }
                else
                {
                    try
                    {
                        using var memStream = new MemoryStream();
                        await file.CopyToAsync(memStream);
                        //Reset the stream so MagickImageInfo can read it properly
                        memStream.Seek(0, SeekOrigin.Begin);
                        MagickImageInfo info = new MagickImageInfo(memStream);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains(NO_DECODE_DELEGATE))
                        {
                            result = FileValidationResult.InvalidFileType;
                        }
                        else
                        {
                            result = FileValidationResult.UnknownValidationErrror;
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public FileValidationResult ValidateImage(IFormFile file)
        {
            const string functionIdentifier = "ValidateImage(file)";

            FileValidationResult result = FileValidationResult.IsValid;

            try
            {
                if (file.Length > settings.Value.FileUploadSettings.MaximumFileSize)
                {
                    result = FileValidationResult.FileTooLarge;
                }
                else if (file.Length <= 0)
                {
                    result = FileValidationResult.FileTooSmall;
                }
                else
                {
                    try
                    {
                        using var memStream = new MemoryStream();
                        file.CopyTo(memStream);
                        //Reset the stream so MagickImageInfo can read it properly
                        memStream.Seek(0, SeekOrigin.Begin);
                        MagickImageInfo info = new MagickImageInfo(memStream);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains(NO_DECODE_DELEGATE))
                        {
                            result = FileValidationResult.InvalidFileType;
                        }
                        else
                        {
                            result = FileValidationResult.UnknownValidationErrror;
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<byte[]> GetHash(IFormFile image)
        {
            const string functionIdentifier = "GetHash(image)";

            SHA256 hasher = SHA256.Create();

            byte[] hash = Array.Empty<byte>();

            try
            {
                byte[] fileBytes = await GetImageBytes(image);
                hash = hasher.ComputeHash(fileBytes);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }
            return hash;
        }

        public async Task<byte[]> ProcessAvatar(IFormFile image)
        {
            const string functionIdentifier = "ProcessAvatar(image)";

            byte[] avatar;
            try
            {
                avatar = await GetImageBytes(image);
                avatar = this.ResizeImage(avatar, settings.Value.FileUploadSettings.MaximumAvatarDimensions.Width, settings.Value.FileUploadSettings.MaximumAvatarDimensions.Height);
                avatar = this.CompressImage(avatar, settings.Value.FileUploadSettings.CompressionMethod);

            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return avatar;
        }

        public byte[] ProcessThumbnail(byte[] image)
        {
            const string functionIdentifier = "ProcessThumbnailAsync(image)";

            byte[] thumbnail;
            try
            {
                thumbnail = this.ResizeImage(image, settings.Value.FileUploadSettings.MaximumThumbnailDimensions.Width, settings.Value.FileUploadSettings.MaximumThumbnailDimensions.Height);
                thumbnail = this.CompressImage(thumbnail, settings.Value.FileUploadSettings.CompressionMethod);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return thumbnail;
        }

        public byte[] ProcessContent(byte[] image)
        {
            const string functionIdentifier = "ProcessContentAsync(image)";

            byte[] content;
            try
            {
                content = this.CompressImage(image, settings.Value.FileUploadSettings.CompressionMethod);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return content;
        }

        private byte[] CompressImage(byte[] inputImage, string compressionMethod)
        {
            const string functionIdentifier = "CompressImage(inputImage, compressionMethod)";

            byte[] image = null;

            try
            {
                using var memStream = new MemoryStream(inputImage);
                if (compressionMethod == Compression.LOSSLESS || compressionMethod == Compression.LOSSLESS)
                {
                    ImageOptimizer optimizer = new ImageOptimizer
                    {
                        IgnoreUnsupportedFormats = true
                    };

                    //Reset the stream so MagickImageInfo can read it properly
                    memStream.Seek(0, SeekOrigin.Begin);

                    if (compressionMethod == Compression.LOSSLESS)
                    {
                        optimizer.LosslessCompress(memStream);
                    }
                    else if (compressionMethod == Compression.COMPRESSION)
                    {
                        optimizer.Compress(memStream);
                    }
                }

                image = memStream.ToArray();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ compressionMethod:{0} }}", compressionMethod));
                throw;
            }

            return image;
        }

        private Byte[] ResizeImage(Byte[] inputImage, int width, int height)
        {
            const string functionIdentifier = "ResizeImage(inputImage, width, height)";

            byte[] image = null;

            try
            {
                using var resized = new MagickImage(inputImage);
                var size = new MagickGeometry(width, height);

                resized.Resize(size);

                image = resized.ToByteArray();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ width:{0}, height:{1} }}", width, height));
                throw;
            }

            return image;
        }

        public async Task<byte[]> GetImageBytes(IFormFile image)
        {
            const string functionIdentifier = "GetImageBytes(image)";

            try
            {
                using var ms = new MemoryStream();
                await image.CopyToAsync(ms);
                return ms.ToArray();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
