﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class ContentService : IContentService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IContentRepository contentRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.ContentService";

        public ContentService(IManicNetUnitOfWork unitOfWork, IContentRepository contentRepository, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.contentRepository = contentRepository;
            this.logger = logger;
        }

        public async Task<Content> FindContent(Guid id)
        {
            const string functionIdentifier = "FindContent(id)";

            try
            {
                Content content = await contentRepository.FindAsync(id);

                if (content == null)
                {
                    throw new ContentNotFoundException(string.Format("Content with ID {0} not found", id));
                }

                return content;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Content> FindContent(byte[] hash)
        {
            const string functionIdentifier = "FindContent(hash)";

            try
            {
                return await contentRepository.FindAsync(hash);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }
        }

        public async Task CreateContent(Content content, bool callAsTopLevel = true)
        {
            const string functionIdentifier = "CreateContent(content, callAsTopLevel)";

            try
            {
                contentRepository.Insert(content);
                if (callAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ content:{0}, callAsTopLevel:{1} }}", content, callAsTopLevel));
                throw;
            }
        }

        public async Task RemoveContent(Content content, bool callAsTopLevel = true)
        {
            const string functionIdentifier = "RemoveContent(content, callAsTopLevel)";

            try
            {
                contentRepository.Delete(content);
                if (callAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ content:{0}, callAsTopLevel:{1} }}", content, callAsTopLevel));
                throw;
            }
        }
    }
}
