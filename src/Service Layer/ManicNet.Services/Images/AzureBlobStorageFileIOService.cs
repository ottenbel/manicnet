﻿using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class AzureBlobStorageFileIOService : IFileIOService
    {
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetLoggerService logger;

        private readonly BlobContainerClient avatarContainerClient;
        private readonly BlobContainerClient thumbnailContainerClient;
        private readonly BlobContainerClient contentContainerClient;

        private readonly string path = "ManicNet.Services.AzureBlobStorageFileIOService";

        public AzureBlobStorageFileIOService(IOptions<ManicNetSettings> settings, IManicNetLoggerService logger)
        {
            this.settings = settings;
            this.logger = logger;

            BlobServiceClient blobServiceClient = new BlobServiceClient(settings.Value.FileUploadSettings.AzureBlobStorageUpload.ConnectionString);
            this.avatarContainerClient = blobServiceClient.GetBlobContainerClient(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Avatar.Container);
            this.thumbnailContainerClient = blobServiceClient.GetBlobContainerClient(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Thumbnail.Container);
            this.contentContainerClient = blobServiceClient.GetBlobContainerClient(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Content.Container);
        }

        public async Task<string> UploadAvatarAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadAvatar(file, inputName)";

            try
            {
                return await UploadFileAsync(file, avatarContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadThumbnailAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadThumbnailAsync(file, inputName)";

            try
            {
                return await UploadFileAsync(file, thumbnailContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadContentAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadContent(file, inputName)";

            try
            {
                return await UploadFileAsync(file, contentContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task DeleteAvatar(string inputName)
        {
            const string functionIdentifier = "DeleteAvatar(inputName)";

            try
            {
                await DeleteFile(avatarContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task DeleteTumbnail(string inputName)
        {
            const string functionIdentifier = "DeleteTumbnail(inputName)";

            try
            {
                await DeleteFile(thumbnailContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("Error in function {0}.{1}. {{inputName:{2}}}", path, functionIdentifier, inputName));
                throw;
            }
        }

        public async Task DeleteContent(string inputName)
        {
            const string functionIdentifier = "DeleteContent(inputName)";

            try
            {
                await DeleteFile(contentContainerClient, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public string GetLink(Avatar avatar)
        {
            if (string.Equals(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Avatar.AccessLevel, Uploads.AzureContainer.PRIVATE, StringComparison.OrdinalIgnoreCase) && avatarContainerClient.GetBlobClient(avatar.Filename).CanGenerateSasUri)
            {
                return avatarContainerClient.GetBlobClient(avatar.Filename).GenerateSasUri(BlobSasPermissions.Read, DateTimeOffset.UtcNow.AddHours(1)).AbsoluteUri.ToString();
            }
            else
            {
                return avatarContainerClient.GetBlobClient(avatar.Filename).Uri.ToString();
            }
        }

        public string GetLink(Thumbnail thumbnail)
        {
            if (string.Equals(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Thumbnail.AccessLevel, Uploads.AzureContainer.PRIVATE, StringComparison.OrdinalIgnoreCase) && avatarContainerClient.GetBlobClient(thumbnail.Filename).CanGenerateSasUri)
            {
                return thumbnailContainerClient.GetBlobClient(thumbnail.Filename).GenerateSasUri(BlobSasPermissions.Read, DateTimeOffset.UtcNow.AddHours(1)).AbsoluteUri.ToString();
            }
            else
            {
                return thumbnailContainerClient.GetBlobClient(thumbnail.Filename).Uri.ToString();
            }
        }

        public string GetLink(Content content)
        {
            if (string.Equals(settings.Value.FileUploadSettings.AzureBlobStorageUpload.Content.AccessLevel, Uploads.AzureContainer.PRIVATE, StringComparison.OrdinalIgnoreCase) && avatarContainerClient.GetBlobClient(content.Filename).CanGenerateSasUri)
            {
                return contentContainerClient.GetBlobClient(content.Filename).GenerateSasUri(BlobSasPermissions.Read, DateTimeOffset.UtcNow.AddHours(1)).AbsoluteUri.ToString();
            }
            else
            {
                return contentContainerClient.GetBlobClient(content.Filename).Uri.ToString();
            }
        }

        public async Task<byte[]> GetFile(Avatar avatar)
        {
            Stream result = await avatarContainerClient.GetBlobClient(avatar.Filename).OpenReadAsync();

            MemoryStream memoryStream = new MemoryStream();
            result.CopyTo(memoryStream);
            byte[] file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetFile(Thumbnail thumbnail)
        {
            Stream result = await thumbnailContainerClient.GetBlobClient(thumbnail.Filename).OpenReadAsync();

            MemoryStream memoryStream = new MemoryStream();
            result.CopyTo(memoryStream);
            byte[] file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetFile(Content content)
        {
            Stream result = await contentContainerClient.GetBlobClient(content.Filename).OpenReadAsync();

            MemoryStream memoryStream = new MemoryStream();
            result.CopyTo(memoryStream);
            byte[] file = memoryStream.ToArray();

            return file;
        }

        public async Task<byte[]> GetDownloadChapter(string work, Chapter chapter)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    await CompressChapter(chapter, string.Empty, archive, work);
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadVolume(string work, Volume volume)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    foreach (Chapter chapter in volume.Chapters.Where(c => c.IsDeleted == false))
                    {
                        await CompressVolume(volume, string.Empty, archive, work);
                    }
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadWork(Work work)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    foreach (Volume volume in work.Volumes.Where(v => v.IsDeleted == false))
                    {
                        DirectoryInfo volumeDirectory = new DirectoryInfo(string.Format("Volume {0}{1}", volume.Number, String.IsNullOrWhiteSpace(volume.Title) ? string.Empty : string.Format(" - {0}", volume.Title)));

                        await CompressVolume(volume, volumeDirectory.Name, archive, work.Title);
                    }
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }


        private async Task CompressVolume(Volume volume, string subfolder, ZipArchive archive, string work)
        {
            foreach (Chapter chapter in volume.Chapters.Where(c => c.IsDeleted == false))
            {
                DirectoryInfo chapterDirectory = new DirectoryInfo(string.Format("Chapter {0}{1}", chapter.Number, String.IsNullOrWhiteSpace(chapter.Title) ? string.Empty : string.Format(" - {0}", chapter.Title)));

                string subpath = Path.Join(subfolder, chapterDirectory.Name);

                await CompressChapter(chapter, subpath, archive, work);
            }
        }

        private async Task CompressChapter(Chapter chapter, string subfolder, ZipArchive archive, string work)
        {
            foreach (Page page in chapter.Pages)
            {
                Stream result = await contentContainerClient.GetBlobClient(page.Content.Filename).OpenReadAsync();

                string extension = string.Empty;

                int extensionStart = page.Content.Filename.LastIndexOf('.');
                if (extensionStart != -1)
                {
                    extension = page.Content.Filename.Substring(extensionStart);
                }

                string filename = string.Format("[{0}] {1} - Chapter {2} - Page {3}{4}", settings.Value.Metadata.SiteName, work, chapter.Number, page.Number, extension);
                string filepath = string.IsNullOrWhiteSpace(subfolder) ? filename : Path.Join(subfolder, filename);

                var file = archive.CreateEntry(filepath, CompressionLevel.Optimal);
                using (Stream entryStream = file.Open())
                {
                    await result.CopyToAsync(entryStream);
                }
            }
        }

        private async Task<string> UploadFileAsync(byte[] file, BlobContainerClient client, string inputName)
        {
            const string functionIdentifier = "UploadFileAsync(file, client, inputName)";

            string extension = string.Empty;

            int extensionStart = inputName.LastIndexOf('.');
            if (extensionStart != -1)
            {
                extension = inputName.Substring(extensionStart);
            }

            try
            {
                BlobClient blobClient = null;

                string filename;
                do
                {
                    filename = Guid.NewGuid().ToString() + extension;
                    blobClient = client.GetBlobClient(filename);
                }
                while (await blobClient.ExistsAsync());

                using (var stream = new MemoryStream(file, false))
                {
                    await blobClient.UploadAsync(stream);
                }

                return filename;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ client:{0}, inputName:{1}}}", client, inputName));
                throw;
            }
        }

        private async Task DeleteFile(BlobContainerClient client, string inputName)
        {
            const string functionIdentifier = "DeleteFile(client, inputName)";

            try
            {
                bool result = await client.DeleteBlobIfExistsAsync(inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ client:{0}, inputName:{1} }}", client, inputName));
                throw;
            }
        }
    }
}
