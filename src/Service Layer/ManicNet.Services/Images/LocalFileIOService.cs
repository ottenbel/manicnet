﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class LocalFileIOService : IFileIOService
    {
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.LocalFileIOService";

        public LocalFileIOService(IOptions<ManicNetSettings> settings, IManicNetLoggerService logger)
        {
            this.settings = settings;
            this.logger = logger;
        }

        public async Task<string> UploadAvatarAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadAvatar(file, inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Avatar.Directory;
                return await UploadFileAsync(file, directory, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadThumbnailAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadThumbnailAsync(file, inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Thumbnail.Directory;
                return await UploadFileAsync(file, directory, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public async Task<string> UploadContentAsync(byte[] file, string inputName)
        {
            const string functionIdentifier = "UploadContent(file, inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;
                return await UploadFileAsync(file, directory, inputName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public Task DeleteAvatar(string inputName)
        {
            const string functionIdentifier = "DeleteAvatar(inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Avatar.Directory;
                DeleteFile(directory, inputName);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public Task DeleteTumbnail(string inputName)
        {
            const string functionIdentifier = "DeleteTumbnail(inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Thumbnail.Directory;
                DeleteFile(directory, inputName);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public Task DeleteContent(string inputName)
        {
            const string functionIdentifier = "DeleteContent(inputName)";

            try
            {
                string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;
                DeleteFile(directory, inputName);
                return Task.CompletedTask;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ inputName:{0} }}", inputName));
                throw;
            }
        }

        public string GetLink(Avatar avatar)
        {
            return settings.Value.FileUploadSettings.LocalUpload.Avatar.PublicPath + "/" + avatar.Filename;
        }

        public string GetLink(Thumbnail thumbnail)
        {
            return settings.Value.FileUploadSettings.LocalUpload.Thumbnail.PublicPath + "/" + thumbnail.Filename;
        }

        public string GetLink(Content content)
        {
            return settings.Value.FileUploadSettings.LocalUpload.Content.PublicPath + "/" + content.Filename;
        }

        public async Task<byte[]> GetFile(Avatar avatar)
        {
            string directory = settings.Value.FileUploadSettings.LocalUpload.Avatar.Directory;
            string filePath = Path.Combine(directory, avatar.Filename);
            byte[] file = await File.ReadAllBytesAsync(filePath);

            return file;
        }

        public async Task<byte[]> GetFile(Thumbnail thumbnail)
        {
            string directory = settings.Value.FileUploadSettings.LocalUpload.Thumbnail.Directory;
            string filePath = Path.Combine(directory, thumbnail.Filename);
            byte[] file = await File.ReadAllBytesAsync(filePath);

            return file;
        }

        public async Task<byte[]> GetFile(Content content)
        {
            string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;
            string filePath = Path.Combine(directory, content.Filename);
            byte[] file = await File.ReadAllBytesAsync(filePath);

            return file;
        }

        public async Task<byte[]> GetDownloadChapter(string work, Chapter chapter)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;

                    await CompressChapter(chapter, directory, string.Empty, archive, work);
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadVolume(string work, Volume volume)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;

                    await CompressVolume(volume, directory, string.Empty, archive, work);
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        public async Task<byte[]> GetDownloadWork(Work work)
        {
            byte[] zipped;
            using (MemoryStream outStream = new MemoryStream())
            {
                using (ZipArchive archive = new ZipArchive(outStream, ZipArchiveMode.Create, true))
                {
                    string directory = settings.Value.FileUploadSettings.LocalUpload.Content.Directory;

                    foreach (Volume volume in work.Volumes.Where(v => v.IsDeleted == false))
                    {
                        DirectoryInfo volumeDirectory = new DirectoryInfo(string.Format("Volume {0}{1}", volume.Number, string.IsNullOrWhiteSpace(volume.Title) ? string.Empty : string.Format(" - {0}", volume.Title)));

                        await CompressVolume(volume, directory, volumeDirectory.Name, archive, work.Title);
                    }
                }

                //TODO: Add info file

                zipped = outStream.ToArray();
            }

            return zipped;
        }

        private async Task CompressVolume(Volume volume, string directory, string subfolder, ZipArchive archive, string work)
        {
            foreach (Chapter chapter in volume.Chapters.Where(c => c.IsDeleted == false))
            {
                DirectoryInfo chapterDirectory = new DirectoryInfo(string.Format("Chapter {0}{1}", chapter.Number, string.IsNullOrWhiteSpace(chapter.Title) ? string.Empty : string.Format(" - {0}", chapter.Title)));

                string subpath = Path.Join(subfolder, chapterDirectory.Name);

                await CompressChapter(chapter, directory, subpath, archive, work);
            }
        }

        private async Task CompressChapter(Chapter chapter, string fileSource, string subfolder, ZipArchive archive, string work)
        {
            foreach (Page page in chapter.Pages)
            {
                string filePath = Path.Combine(fileSource, page.Content.Filename);
                byte[] fileContents = await File.ReadAllBytesAsync(filePath);

                string extension = string.Empty;

                int extensionStart = page.Content.Filename.LastIndexOf('.');
                if (extensionStart != -1)
                {
                    extension = page.Content.Filename.Substring(extensionStart);
                }

                string filename = string.Format("[{0}] {1} - Chapter {2} - Page {3}{4}", settings.Value.Metadata.SiteName, work, chapter.Number, page.Number, extension);
                string filepath = string.IsNullOrWhiteSpace(subfolder) ? filename : Path.Join(subfolder, filename);

                var file = archive.CreateEntry(filepath, CompressionLevel.Optimal);
                using (Stream entryStream = file.Open())
                {
                    using (MemoryStream fileToCompressStream = new MemoryStream(fileContents))
                    {
                        await fileToCompressStream.CopyToAsync(entryStream);
                    }
                }
            }
        }

        private async Task<string> UploadFileAsync(byte[] file, string directory, string inputName)
        {
            const string functionIdentifier = "UploadFileAsync(file, directory, inputName)";

            string extension = string.Empty;

            int extensionStart = inputName.LastIndexOf('.');
            if (extensionStart != -1)
            {
                extension = inputName.Substring(extensionStart);
            }

            try
            {
                string filename;
                string filepath;
                do
                {
                    filename = Guid.NewGuid().ToString() + extension;
                    filepath = Path.Combine(directory, filename);
                }
                while (File.Exists(filepath));

                using (var stream = new FileStream(filepath, FileMode.Create))
                {
                    await stream.WriteAsync(file, 0, file.Length);
                }

                return filename;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ directory:{0}, inputName:{1} }}", directory, inputName));
                throw;
            }
        }

        private void DeleteFile(string directory, string inputName)
        {
            const string functionIdentifier = "DeleteFile(directory, inputName)";

            try
            {
                string fileName = Path.Combine(directory, inputName);
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ directory:{0}, inputName:{1} }}", directory, inputName));
                throw;
            }
        }
    }
}
