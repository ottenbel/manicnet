﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class AvatarService : IAvatarService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly IAvatarRepository avatarRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.AvatarService";

        public AvatarService(IManicNetUnitOfWork unitOfWork, IAvatarRepository avatarRepository, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.avatarRepository = avatarRepository;
            this.logger = logger;
        }

        public async Task<Avatar> FindAvatar(Guid id)
        {
            const string functionIdentifier = "FindAvatar(id)";

            try
            {
                Avatar avatar = await avatarRepository.FindAsync(id);

                if (avatar == null)
                {
                    throw new AvatarNotFoundException(string.Format("Avatar with ID {0} not found", id));
                }

                return avatar;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Avatar> FindAvatar(byte[] hash)
        {
            const string functionIdentifier = "FindAvatar(hash)";

            try
            {
                return await avatarRepository.FindAsync(hash);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }
        }

        public async Task CreateAvatar(Avatar avatar)
        {
            const string functionIdentifier = "CreateAvatar(avatar)";

            try
            {
                avatarRepository.Insert(avatar);
                await unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ avatar:{0} }}", avatar));
                throw;
            }
        }

        public async Task RemoveAvatar(Avatar avatar)
        {
            const string functionIdentifier = "RemoveAvatar(avatar)";

            try
            {
                avatarRepository.Delete(avatar);
                await unitOfWork.SaveAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ avatar:{0} }}", avatar));
                throw;
            }
        }
    }
}
