﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class UploadService : IUploadService
    {
        private readonly IImageProcessingService imageProcessingService;
        private readonly IContentService contentService;
        private readonly IContentFileHandlerService contentFileHandlerService;

        public UploadService(IImageProcessingService imageProcessingService, IContentService contentService, IContentFileHandlerService contentFileHandlerService)
        {
            this.imageProcessingService = imageProcessingService;
            this.contentService = contentService;
            this.contentFileHandlerService = contentFileHandlerService;
        }

        public async Task<Content> UploadFileOrRetrieveFromContent(IFormFile contentFile)
        {
            Content content = null;

            if (contentFile != null)
            {
                byte[] hash = await imageProcessingService.GetHash(contentFile);
                content = await contentService.FindContent(hash);

                if (content == null)
                {
                    content = await contentFileHandlerService.UploadContent(contentFile, hash);
                }
            }

            return content;
        }
    }
}
