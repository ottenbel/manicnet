﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class CacheConfigurationRetrievalService : ICacheConfigurationRetrievalService
    {
        private readonly ICacheConfigurationRepository cacheConfigurationRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.CacheConfigurationRetrievalService";

        public CacheConfigurationRetrievalService(ICacheConfigurationRepository cacheConfigurationRepository, IManicNetLoggerService logger)
        {
            this.cacheConfigurationRepository = cacheConfigurationRepository;
            this.logger = logger;
        }

        public List<CacheConfiguration> GetCacheConfigurations()
        {
            const string functionIdentifier = "GetCacheConfigurations(user)";

            try
            {
                List<CacheConfiguration> cacheConfigurations = cacheConfigurationRepository.All();
                return cacheConfigurations;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<CacheConfiguration>> GetCacheConfigurationsAsync()
        {
            const string functionIdentifier = "GetCacheConfigurationsAsync(user)";

            try
            {
                List<CacheConfiguration> cacheConfigurations = await cacheConfigurationRepository.AllAsync();
                return cacheConfigurations;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public List<CacheConfiguration> GetCacheConfigurationsForSection(Guid sectionID)
        {
            const string functionIdentifier = "GetCacheConfigurations(user)";

            try
            {
                List<CacheConfiguration> cacheConfigurations = GetCacheConfigurations().Where(x => x.SectionID == sectionID).ToList();
                return cacheConfigurations;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
