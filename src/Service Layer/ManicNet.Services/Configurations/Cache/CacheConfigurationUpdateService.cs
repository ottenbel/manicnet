﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class CacheConfigurationUpdateService : ICacheConfigurationUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly ICacheConfigurationRepository cacheConfigurationRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.CacheConfigurationUpdateService";

        public CacheConfigurationUpdateService(IManicNetUnitOfWork unitOfWork, ICacheConfigurationRepository cacheConfigurationRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.cacheConfigurationRepository = cacheConfigurationRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
        }

        public async Task BulkCreateCacheConfiguration(List<CacheConfiguration> cacheConfigurations)
        {
            const string functionIdentifier = "BulkCreateCacheConfiguration(cacheConfigurations)";

            try
            {
                cacheConfigurationRepository.BulkInsert(cacheConfigurations);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCacheConfiguration();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ cacheConfigurations:{0} }}", cacheConfigurations));
                throw;
            }
        }

        public async Task BulkUpdateCacheConfiguration(List<CacheConfiguration> cacheConfigurations)
        {
            const string functionIdentifier = "BulkUpdateCacheConfiguration(cacheConfigurations)";

            try
            {
                cacheConfigurationRepository.BulkUpdate(cacheConfigurations);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForCacheConfiguration();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ cacheConfigurations:{0} }}", cacheConfigurations));
                throw;
            }
        }
    }
}
