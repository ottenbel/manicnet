﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class TagTypeUpdateService : ITagTypeUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly ITagTypeRepository tagTypeRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;
        private readonly ITagUpdateService tagUpdateService;

        private readonly string path = "ManicNet.Services.TagTypeUpdateService";

        public TagTypeUpdateService(IManicNetUnitOfWork unitOfWork, ITagTypeRepository tagTypeRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger, ITagUpdateService tagUpdateService)
        {
            this.unitOfWork = unitOfWork;
            this.tagTypeRepository = tagTypeRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
            this.tagUpdateService = tagUpdateService;
        }

        public async Task CreateTagType(TagType tagType)
        {
            const string functionIdentifier = "CreateTagType(tagType)";

            try
            {
                TagTypeCacheCleanup tagTypeCacheCleanup = new TagTypeCacheCleanup(tagType);
                tagTypeRepository.Insert(tagType);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTagType(tagTypeCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0} }}", tagType));
                throw;
            }
        }

        public async Task RemoveTagType(TagType tagType, IEnumerable<Tag> associatedTags)
        {
            const string functionIdentifier = "RemoveTagType(tagType, associatedTags)";



            try
            {

                TagTypeCacheCleanup tagTypeCacheCleanup = new TagTypeCacheCleanup(tagType, associatedTags);

                if (!tagType.IsDeleted)
                {
                    throw new TagTypeNotDeletableException(string.Format("Tag type with ID {0} not is not in the state to be deleted", tagType.TagTypeID));
                }

                foreach (Tag tag in associatedTags)
                {
                    await tagUpdateService.RemoveTag(tag, false);
                }

                tagTypeRepository.Delete(tagType);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTagType(tagTypeCacheCleanup);
            }
            catch (TagTypeNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not deletable. {{ tagType:{0}, associatedTags:{1} }}", tagType, associatedTags));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0}, associatedTags:{1} }}", tagType, associatedTags));
                throw;
            }
        }

        public async Task UpdateTagType(TagType tagType, IEnumerable<Tag> associatedTags)
        {
            const string functionIdentifier = "UpdateTagType(tagType, associatedTags)";

            try
            {
                TagTypeCacheCleanup tagTypeCacheCleanup = new TagTypeCacheCleanup(tagType, associatedTags);
                tagTypeRepository.Update(tagType);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTagType(tagTypeCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0}, associatedTags:{1} }}", tagType, associatedTags));
                throw;
            }
        }
    }
}
