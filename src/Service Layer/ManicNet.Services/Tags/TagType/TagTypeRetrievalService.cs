﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class TagTypeRetrievalService : ITagTypeRetrievalService
    {
        private readonly ITagTypeRepository tagTypeRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;

        private readonly string path = "ManicNet.Services.TagTypeRetrievalService";

        public TagTypeRetrievalService(ITagTypeRepository tagTypeRepository,
            IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService)
        {
            this.tagTypeRepository = tagTypeRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
        }

        public async Task<TagType> FindTagTypeAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindTagTypeAsync(id, user, bypassCache)";

            try
            {
                TagType tagType = await tagTypeRepository.FindAsync(id, bypassCache);

                if (tagType == null)
                {
                    throw new TagTypeNotFoundException(string.Format("Tag type with ID {0} not found", id));
                }

                if ((tagType.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Succeeded))
                {
                    throw new TagTypeNotViewableException(string.Format("User does not have permission to view tag type with ID {0}", id));
                }

                return tagType;
            }
            catch (TagTypeNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not found. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (TagTypeNotViewableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not viewable. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public TagType FindTagType(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindTagType(id, user, bypassCache)";

            try
            {
                TagType tagType = tagTypeRepository.Find(id, bypassCache);

                if (tagType == null)
                {
                    throw new TagTypeNotFoundException(string.Format("Tag type with ID {0} not found", id));
                }

                if ((tagType.IsDeleted) && (!authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE).GetAwaiter().GetResult().Succeeded))
                {
                    throw new TagTypeNotViewableException(string.Format("User does not have permission to view tag type with ID {0}", id));
                }

                return tagType;
            }
            catch (TagTypeNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not found. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (TagTypeNotViewableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not viewable. {{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public TagType FindTagTypeWithWorkAssociation(Guid id, ClaimsPrincipal user)
        {
            const string functionIdentifier = "FindTagTypeWithWorkAssociationAsync(id, user, bypassCache)";

            try
            {
                TagType tagType = tagTypeRepository.FindWithWorkAssociation(id);

                if (tagType == null)
                {
                    throw new TagTypeNotFoundException(string.Format("Tag type with ID {0} not found", id));
                }

                if ((tagType.IsDeleted) && (!authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE).GetAwaiter().GetResult().Succeeded))
                {
                    throw new TagTypeNotViewableException(string.Format("User does not have permission to view tag type with ID {0}", id));
                }

                return tagType;
            }
            catch (TagTypeNotFoundException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not found. {{ id:{0}, user:{1} }}", id, user));
                throw;
            }
            catch (TagTypeNotViewableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag type not viewable. {{ id:{0}, user:{1} }}", id, user));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1} }}", id, user));
                throw;
            }
        }

        public async Task<List<TagType>> GetActiveTagTypes()
        {
            const string functionIdentifier = "GetActivePubliclyAvailiableTagTypesAsync()";

            List<TagType> tagTypes = new List<TagType>();

            try
            {
                tagTypes = (await tagTypeRepository.AllAsync()).Where(x => x.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return tagTypes;
        }

        public async Task<List<TagType>> GetActiveTagTypesForWorkType(Guid workType)
        {
            const string functionIdentifier = "GetActiveTagTypesForWorkType(workType)";

            List<TagType> tagTypes = new List<TagType>();

            try
            {
                var query = (await tagTypeRepository.AllAsync()).Where(x => x.IsDeleted == false);

                if (workType == WorkTypes.WORK)
                {
                    query = query.Where(tt => tt.AvailableOnWork);
                }
                else if (workType == WorkTypes.VOLUME)
                {
                    query = query.Where(tt => tt.AvailableOnVolume);
                }
                else if (workType == WorkTypes.CHAPTER)
                {
                    query = query.Where(tt => tt.AvailableOnChapter);
                }

                tagTypes = query.ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ workType:{0} }}", workType));
                throw;
            }

            return tagTypes;
        }

        public async Task<List<TagType>> GetDeletedTagTypes()
        {
            const string functionIdentifier = "GetDeletedTagTypes()";

            List<TagType> tagTypes = new List<TagType>();

            try
            {
                tagTypes = (await tagTypeRepository.AllAsync()).Where(x => x.IsDeleted == true).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return tagTypes;
        }

        public async Task<bool> DoDeletedTagTypesExistAsync()
        {
            const string functionIdentifier = "DoDeletedTagTypesExist()";

            List<TagType> tagTypes = new List<TagType>();

            try
            {
                return (await tagTypeRepository.AllAsync()).Where(x => x.IsDeleted == true).Any();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public bool HasUniqueName(string name, Guid? tagTypeID = null)
        {
            const string functionIdentifier = "HasUniqueName(name, tagTypeID)";

            bool result;

            try
            {
                result = (!tagTypeRepository.All().Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && x.TagTypeID != tagTypeID));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ name:{0}, tagTypeID:{1} }}", name, tagTypeID));
                throw;
            }

            return result;
        }

        public bool HasUniqueSearchablePrefix(string prefix, Guid? tagTypeID = null)
        {
            const string functionIdentifier = "HasUniqueSearchPrefixAsync(prefix, tagTypeID)";

            bool result;

            try
            {
                result = (!tagTypeRepository.All().Any(x => x.SearchablePrefix.Equals(prefix, StringComparison.CurrentCultureIgnoreCase) && x.TagTypeID != tagTypeID));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ prefix:{0}, tagTypeID:{1} }}", prefix, tagTypeID));
                throw;
            }

            return result;
        }

        public async Task<List<SelectListItem>> GetTagTypesSelectList()
        {
            var result = (await this.GetActiveTagTypes())
                .Select(x => new SelectListItem { Text = x.Name, Value = x.TagTypeID.ToString() })
                .ToList();

            return result;
        }
    }
}
