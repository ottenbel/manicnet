﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using System;
using System.Linq;

namespace ManicNet.Services
{
    public sealed class CommonTagService : ICommonTagService
    {
        private readonly ITagRepository tagRepository;
        private readonly ITagAliasRepository tagAliasRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.CommonTagService";

        public CommonTagService(ITagRepository tagRepository, ITagAliasRepository tagAliasRepository, IManicNetLoggerService logger)
        {
            this.tagRepository = tagRepository;
            this.tagAliasRepository = tagAliasRepository;
            this.logger = logger;
        }

        public bool HasUniqueName(string name, Guid? tagID = null)
        {
            const string functionIdentifier = "HasUniqueName(name, tagID)";

            bool result;

            try
            {
                bool existsAsTagName = tagRepository.All().Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && x.TagID != tagID);
                bool existsAsAlias = tagAliasRepository.All().Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
                result = (!(existsAsTagName || existsAsAlias));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ name:{0}, tagID:{1} }}", name, tagID));
                throw;
            }

            return result;
        }

        public bool HasUniqueAlias(string name, Guid? tagID = null)
        {
            const string functionIdentifier = "HasUniqueAlias(name, tagID)";

            bool result;

            try
            {
                bool existsAsTagName = tagRepository.All().Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase));
                bool existsAsAlias = tagAliasRepository.All().Any(x => x.Name.Equals(name, StringComparison.CurrentCultureIgnoreCase) && x.TagID != tagID);
                result = (!(existsAsTagName || existsAsAlias));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ name:{0}, tagID:{1} }}", name, tagID));
                throw;
            }

            return result;
        }
    }
}
