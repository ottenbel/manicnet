﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class TagAliasService : ITagAliasService
    {
        private readonly ITagAliasRepository tagAliasRepository;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.TagAliasService";

        public TagAliasService(ITagAliasRepository tagAliasRepository, IManicNetLoggerService logger)
        {
            this.tagAliasRepository = tagAliasRepository;
            this.logger = logger;
        }

        public async Task<List<TagAlias>> GetAliasList(IEnumerable<string> aliases)
        {
            const string functionIdentifier = "GetAliasList(aliases)";

            List<TagAlias> result = new List<TagAlias>();

            try
            {
                List<TagAlias> tagAliases = await tagAliasRepository.FindAsync(aliases);

                result.AddRange(tagAliases);

                foreach (string alias in aliases.Except(tagAliases.Select(a => a.Name)))
                {
                    result.Add(new TagAlias(alias));
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ aliases:{0} }}", aliases));
                throw;
            }

            return result;
        }

        public async Task<List<TagAlias>> GetActiveTagAliases()
        {
            const string functionIdentifier = "GetActiveTagAliases()";

            List<TagAlias> tagAliases = new List<TagAlias>();

            try
            {
                tagAliases = (await tagAliasRepository.AllAsync()).Where(t => t.Tag.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return tagAliases;
        }

    }
}
