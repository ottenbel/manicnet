﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class TagRetrievalService : ITagRetrievalService
    {
        private readonly ITagRepository tagRepository;
        private readonly IManicNetLoggerService logger;
        private readonly IManicNetAuthorizationService authorizationService;

        private readonly string path = "ManicNet.Services.TagRetrievalService";

        public TagRetrievalService(ITagRepository tagRepository, IManicNetLoggerService logger, IManicNetAuthorizationService authorizationService)
        {
            this.tagRepository = tagRepository;
            this.logger = logger;
            this.authorizationService = authorizationService;
        }

        public async Task<Tag> FindTag(Guid id, ClaimsPrincipal user, bool bypassCache = false)
        {
            const string functionIdentifier = "FindTag(id, user, bypassCache)";

            try
            {
                Tag tag = await tagRepository.FindAsync(id, bypassCache);

                if (tag == null)
                {
                    throw new TagNotFoundException(string.Format("Tag type with ID {0} not found", id));
                }

                if ((tag.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG)).Succeeded))
                {
                    throw new TagNotViewableException(string.Format("User does not have permission to view tag with ID {0}", id));
                }

                if ((tag.TagType.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Succeeded))
                {
                    throw new TagUnableToViewParentTagTypeException(string.Format("User does not have permission to view deleted parent tag type of tag with ID {0}", id));
                }

                return tag;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, user:{1}, bypassCache:{2} }}", id, user, bypassCache));
                throw;
            }
        }

        public async Task<Tag> FindTagByName(string name, ClaimsPrincipal user)
        {
            const string functionIdentifier = "FindTagByName(name, user)";

            try
            {
                ExplodedTagFragment fragment = TagInputHelper.ParseInputFragment(name);

                Tag tag = await tagRepository.FindByNameAsync(fragment.tagName);

                if (tag == null)
                {
                    throw new TagNotFoundException(string.Format("Tag type with name {0} not found", fragment.tagName));
                }

                if ((tag.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG)).Succeeded))
                {
                    throw new TagNotViewableException(string.Format("User does not have permission to view tag with name {0}", fragment.tagName));
                }

                if ((tag.TagType.IsDeleted) && (!(await authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Succeeded))
                {
                    throw new TagUnableToViewParentTagTypeException(string.Format("User does not have permission to view deleted parent tag type of tag with name {0}", fragment.tagName));
                }

                return tag;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ searchString:{0} }}", name));
                throw;
            }
        }

        public async Task<TagSearchLists> GetActiveTagSearchListsFromSearchString(string searchString)
        {
            const string functionIdentifier = "GetActiveTagSearchListsFromSearchString(searchString)";

            TagSearchLists tagSearchLists = new TagSearchLists();
            List<string> included = new List<string>();
            List<string> excluded = new List<string>();

            try
            {
                if (string.IsNullOrWhiteSpace(searchString))
                {
                    return tagSearchLists;
                }

                List<string> tagNames = searchString.FromCommaSeparatedStringToDistinctPopulatedList();

                foreach (string tag in tagNames)
                {
                    ExplodedTagFragment fragment = TagInputHelper.ParseInputFragment(tag);
                    if (!string.IsNullOrWhiteSpace(fragment.tagName))
                    {
                        if (fragment.isExcluded)
                        {
                            excluded.Add(fragment.tagName);
                        }
                        else
                        {
                            included.Add(fragment.tagName);
                        }
                    }
                }

                List<Tag> tags = await GetActiveTags();

                tagSearchLists.Included = tags.Where(t => included.Contains(t.Name) || included.Intersect(t.Aliases.Select(a => a.Name).ToList()).Any()).ToList();
                tagSearchLists.Excluded = tags.Where(t => excluded.Contains(t.Name) || excluded.Intersect(t.Aliases.Select(a => a.Name).ToList()).Any()).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ searchString:{0} }}", searchString));
                throw;
            }

            return tagSearchLists;
        }

        public TagSearchLists FilterTagsByWorkType(IEnumerable<Tag> tags, Guid WorkTypeID, bool includeVirtual)
        {
            TagSearchLists results = new TagSearchLists();

            IEnumerable<Tag> includeQuery = null;
            IEnumerable<Tag> excludeQuery = null;

            if (WorkTypeID == WorkTypes.WORK)
            {
                includeQuery = tags.Where(t => DetermineTagInclude(t.TagType.AvailableOnWork, t.IsVirtual, includeVirtual));
                excludeQuery = tags.Where(t => DetermineTagExclude(t.TagType.AvailableOnWork, t.IsVirtual, includeVirtual));
            }
            else if (WorkTypeID == WorkTypes.VOLUME)
            {
                includeQuery = tags.Where(t => DetermineTagInclude(t.TagType.AvailableOnVolume, t.IsVirtual, includeVirtual));
                excludeQuery = tags.Where(t => DetermineTagExclude(t.TagType.AvailableOnVolume, t.IsVirtual, includeVirtual));
            }
            else if (WorkTypeID == WorkTypes.CHAPTER)
            {
                includeQuery = tags.Where(t => DetermineTagInclude(t.TagType.AvailableOnChapter, t.IsVirtual, includeVirtual));
                excludeQuery = tags.Where(t => DetermineTagExclude(t.TagType.AvailableOnChapter, t.IsVirtual, includeVirtual));
            }

            results.Included = includeQuery.ToList();
            results.Excluded = excludeQuery.ToList();

            return results;
        }

        private static bool DetermineTagInclude(bool tagAvailableOnWorkType, bool tagIsVirtual, bool includeVirtual)
        {
            bool result = tagAvailableOnWorkType && (tagIsVirtual == false || tagIsVirtual == includeVirtual);

            return result;
        }

        private static bool DetermineTagExclude(bool tagAvailableOnWorkType, bool tagIsVirtual, bool includeVirtual)
        {
            bool result = !tagAvailableOnWorkType || (tagIsVirtual == true && !includeVirtual);

            return result;
        }

        public async Task<List<Tag>> GetActiveTags()
        {
            const string functionIdentifier = "GetActivePubliclyAvailiableTagsAsync()";

            List<Tag> tags = new List<Tag>();

            try
            {
                tags = (await tagRepository.AllAsync()).Where(x => x.IsDeleted == false && x.TagType.IsDeleted == false).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return tags;
        }

        public async Task<List<Tag>> GetTagsByTagType(Guid tagTypeID)
        {
            const string functionIdentifier = "GetTagsByTagType(tagTypeID)";

            List<Tag> tags = new List<Tag>();

            try
            {
                tags = (await tagRepository.AllAsync()).Where(x => x.TagTypeID == tagTypeID).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagTypeID:{0} }}", tagTypeID));
                throw;
            }

            return tags;
        }


        public async Task<bool> DoDeletedTagTypesExist(ClaimsPrincipal user)
        {
            const string functionIdentifier = "DoDeletedTagTypesExist(user)";

            try
            {
                Task<AuthorizationResult> canViewDeletedTagsTask = authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG);
                Task<AuthorizationResult> canViewDeletedParentTask = authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE);

                await Task.WhenAll(canViewDeletedTagsTask, canViewDeletedParentTask);

                if (!(await canViewDeletedTagsTask).Succeeded)
                {
                    return false;
                }

                bool canViewDeletedParent = (await canViewDeletedParentTask).Succeeded;

                return (await tagRepository.AllAsync())
                    .Where(x => (x.IsDeleted == true && x.TagType.IsDeleted == false)
                    || (x.TagType.IsDeleted == true && canViewDeletedParent)).Any();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<Tag>> GetDeletedTags(ClaimsPrincipal user)
        {
            const string functionIdentifier = "GetDeletedTags(user)";

            List<Tag> tags = new List<Tag>();

            try
            {
                Task<AuthorizationResult> canViewDeletedTagsTask = authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG);
                Task<AuthorizationResult> canViewDeletedParentTask = authorizationService.Authorize(user, Policies.Administrator.VIEW_DELETED_TAG_TYPE);

                if (!(await canViewDeletedTagsTask).Succeeded)
                {
                    return tags;
                }

                bool canViewDeletedParent = (await canViewDeletedParentTask).Succeeded;

                tags = (await tagRepository.AllAsync())
                    .Where(x => (x.IsDeleted == true && x.TagType.IsDeleted == false)
                    || (x.TagType.IsDeleted == true && canViewDeletedParent)).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ user:{0} }}", user));
                throw;
            }

            return tags;
        }

        public async Task<List<RelatedTagList>> GetListOfTagsImplyingTags(IEnumerable<Tag> tags)
        {
            List<RelatedTagList> result = new List<RelatedTagList>();

            List<Tag> allTags = (await tagRepository.AllAsync()).Where(t => t.IsDeleted == false).ToList();

            foreach (Tag tag in tags)
            {
                RelatedTagList currentTag = new RelatedTagList
                {
                    Target = tag
                };

                Stack<Tag> processList = new Stack<Tag>();
                List<Tag> tagParentage = new List<Tag>();

                List<Tag> impliedByTags = allTags.Where(t => tag.ImpliedBy.Select(i => i.ImpliedByTagID).Contains(t.TagID)).ToList();
                foreach (Tag impliedByTag in impliedByTags)
                {
                    processList.Push(impliedByTag);
                }

                while (processList.Any())
                {
                    Tag processingTag = processList.Pop();
                    if (!tagParentage.Contains(processingTag))
                    {
                        tagParentage.Add(processingTag);

                        impliedByTags = allTags.Where(t => processingTag.ImpliedBy.Select(t => t.ImpliedByTagID).Contains(t.TagID)).ToList();

                        foreach (Tag impliedByTag in impliedByTags)
                        {
                            processList.Push(impliedByTag);
                        }
                    }
                }

                currentTag.Related = tagParentage;

                result.Add(currentTag);
            }

            return result;
        }

        public async Task<List<Tag>> GetListOfTagsImplyingTag(Tag tag)
        {
            List<Tag> tags = await tagRepository.AllAsync();

            Stack<Tag> processList = new Stack<Tag>();
            List<Tag> tagParentage = new List<Tag>();

            List<Tag> impliedByTags = tags.Where(t => tag.ImpliedBy.Select(i => i.ImpliedByTagID).Contains(t.TagID)).ToList();
            foreach (Tag impliedByTag in impliedByTags)
            {
                processList.Push(impliedByTag);
            }

            while (processList.Any())
            {
                Tag processingTag = processList.Pop();
                if (!tagParentage.Contains(processingTag))
                {
                    tagParentage.Add(processingTag);

                    impliedByTags = tags.Where(t => processingTag.ImpliedBy.Select(t => t.ImpliedByTagID).Contains(t.TagID)).ToList();

                    foreach (Tag impliedByTag in impliedByTags)
                    {
                        processList.Push(impliedByTag);
                    }
                }
            }

            return tagParentage;
        }
    }
}
