﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Services
{
    public sealed class TagUpdateService : ITagUpdateService
    {
        private readonly IManicNetUnitOfWork unitOfWork;
        private readonly ITagRepository tagRepository;
        private readonly ICacheCleanerService cacheCleanerService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Services.TagUpdateService";

        public TagUpdateService(IManicNetUnitOfWork unitOfWork, ITagRepository tagRepository, ICacheCleanerService cacheCleanerService, IManicNetLoggerService logger)
        {
            this.unitOfWork = unitOfWork;
            this.tagRepository = tagRepository;
            this.cacheCleanerService = cacheCleanerService;
            this.logger = logger;
        }

        public async Task CreateTag(Tag tag, IEnumerable<Tag> includedTags)
        {
            const string functionIdentifier = "CreateTag(tag, includedTags)";

            List<Guid> includedTagsIDList = includedTags.Select(s => s.TagID).ToList();

            try
            {
                TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag, includedTags.Select(t => t.TagID));
                tagRepository.Insert(tag, includedTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTag(tagCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, includedTags:{1} }}", tag, includedTags));
                throw;
            }
        }

        public async Task UpdateTag(Tag tag, IEnumerable<Tag> includedTags, TagCacheCleanup tagCacheCleanup)
        {
            const string functionIdentifier = "UpdateTag(tag, includedTags, tagCacheCleanup)";

            try
            {
                //TODO: Pull added and removed calculation out of repo and into the service
                List<Guid> modifiedTags = tagRepository.Update(tag, includedTags);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTag(tagCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, includedTags:{1}, tagCacheCleanup:{2} }}", tag, includedTags, tagCacheCleanup));
                throw;
            }
        }

        public async Task UpdateTag(Tag tag, bool calledAsTopLevel = true)
        {
            const string functionIdentifier = "UpdateTag(tag, calledAsTopLevel)";

            try
            {
                TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag);
                tagRepository.Update(tag);
                if (calledAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                    await cacheCleanerService.UpdateCacheForTag(tagCacheCleanup);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, calledAsTopLevel:{1} }}", tag, calledAsTopLevel));
                throw;
            }
        }

        public TagSearchLists ValidateTagImplicationList(Tag tag, IEnumerable<Tag> impliedTags, IEnumerable<Tag> ancestry)
        {
            TagSearchLists result = new TagSearchLists();

            if (!tag.ImpliedBy.Any() && !impliedTags.Contains(tag))
            {
                result.Included = impliedTags.ToList();
                return result;
            }
            else if (!tag.ImpliedBy.Any() && impliedTags.Contains(tag))
            {
                result.Excluded = new List<Tag>() { tag };
                result.Included = impliedTags.Where(t => t.TagID != tag.TagID).ToList();

                return result;
            }

            result.Excluded = ancestry.Intersect(impliedTags).ToList();
            result.Included = impliedTags.Where(t => !ancestry.Any(a => a.TagID == t.TagID)).ToList();

            return result;
        }

        public async Task RemoveTag(Tag tag, bool calledAsTopLevel = true)
        {
            const string functionIdentifier = "RemoveTag(tag, calledAsTopLevel)";

            TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag);

            try
            {
                List<Guid> relatedTags = new List<Guid>();

                bool tagImpliesOthers = tag.Implies.Any();
                bool tagImpliedByOthers = tag.ImpliedBy.Any();
                bool cleanedRelationships = tagImpliesOthers || tagImpliedByOthers;

                if (!tag.IsDeleted && calledAsTopLevel)
                {
                    throw new TagNotDeletableException(string.Format("Tag with ID {0} not is not in the state to be deleted", tag.TagID));
                }

                if (tagImpliesOthers)
                {
                    relatedTags = relatedTags.Concat(tag.Implies.Select(i => i.ImpliesTagID)).ToList();
                    tag.Implies.Clear();
                }

                if (tagImpliedByOthers)
                {
                    relatedTags = relatedTags.Concat(tag.ImpliedBy.Select(i => i.ImpliedByTagID)).ToList();
                    tag.ImpliedBy.Clear();
                }

                if (cleanedRelationships)
                {
                    tagRepository.Update(tag);
                }

                tagRepository.Delete(tag);
                if (calledAsTopLevel)
                {
                    await unitOfWork.SaveAsync();
                    await cacheCleanerService.UpdateCacheForTag(tagCacheCleanup);
                }
            }
            catch (TagNotDeletableException ex)
            {
                logger.LogWarning(path, functionIdentifier, ex, string.Format("Tag is not deletable. {{ tag:{0}, calledAsTopLevel:{1} }}", tag, calledAsTopLevel));
                throw;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, calledAsTopLevel:{1}}}", tag, calledAsTopLevel));
                throw;
            }
        }

        public async Task MigrateTag(Tag source, Tag destination, bool ConvertMigratedTagToDestinationAlias, bool RedirectMigratedAliasesToDestination)
        {
            const string functionIdentifier = "MigrateTag(source, destination, ConvertMigratedTagToDestinationAlias, RedirectMigratedAliasesToDestination)";

            try
            {
                TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(source);
                tagCacheCleanup.Add(new TagCacheCleanup(destination));

                List<Guid> works = source.Works.Select(w => w.WorkID).ToList();
                List<Guid> volumes = source.Volumes.Select(v => v.VolumeID).ToList();
                List<Guid> chapters = source.Chapters.Select(c => c.ChapterID).ToList();

                foreach (Work_Tag work in source.Works.Where(s => !destination.Works.Any(d => d.WorkID == s.WorkID)))
                {
                    destination.Works.Add(new Work_Tag() { WorkID = work.WorkID, TagID = destination.TagID, IsPrimary = work.IsPrimary });
                }

                foreach (Volume_Tag volume in source.Volumes.Where(s => !destination.Volumes.Any(d => d.VolumeID == s.VolumeID)))
                {
                    destination.Volumes.Add(new Volume_Tag() { VolumeID = volume.VolumeID, TagID = destination.TagID, IsPrimary = volume.IsPrimary });
                }

                foreach (Chapter_Tag chapter in source.Chapters.Where(s => !destination.Chapters.Any(d => d.ChapterID == s.ChapterID)))
                {
                    destination.Chapters.Add(new Chapter_Tag() { ChapterID = chapter.ChapterID, TagID = destination.TagID, IsPrimary = chapter.IsPrimary });
                }

                source.Works.Clear();
                source.Volumes.Clear();
                source.Chapters.Clear();

                if (RedirectMigratedAliasesToDestination)
                {
                    foreach (TagAlias alias in source.Aliases)
                    {
                        destination.Aliases.Add(new TagAlias() { TagID = destination.TagID, Name = alias.Name });
                    }

                    source.Aliases.Clear();
                }

                if (ConvertMigratedTagToDestinationAlias)
                {
                    destination.Aliases.Add(new TagAlias() { TagID = destination.TagID, Name = source.Name });
                }

                await this.RemoveTag(source, false);
                await this.UpdateTag(destination, false);
                await unitOfWork.SaveAsync();
                await cacheCleanerService.UpdateCacheForTag(tagCacheCleanup);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ source:{0}, destination:{1}, ConvertMigratedTagToDestinationAlias:{2}, RedirectMigratedAliasesToDestination:{3}}}", source, destination, ConvertMigratedTagToDestinationAlias, RedirectMigratedAliasesToDestination));
                throw;
            }
        }
    }
}
