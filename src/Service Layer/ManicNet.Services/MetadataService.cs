﻿using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Options;

namespace ManicNet.Services
{
    public sealed class MetadataService : IMetadataService
    {
        private readonly IOptions<ManicNetSettings> settings;
        private readonly IHttpContextAccessor httpContextAccessor;

        public MetadataService(IOptions<ManicNetSettings> settings, IHttpContextAccessor httpContextAccessor)
        {
            this.settings = settings;
            this.httpContextAccessor = httpContextAccessor;
        }

        public MetadataViewModel GetMetadata<T>(T model)
        {
            MetadataViewModel result = model as MetadataViewModel;

            if (result == null)
            {
                result = new MetadataViewModel();
            }
            else
            {
                result = result.GetMetadata();
            }

            if (string.IsNullOrWhiteSpace(result.Author))
            {
                result.Author = settings.Value.Metadata.Author;
            }

            if (string.IsNullOrWhiteSpace(result.Description))
            {
                result.Description = settings.Value.Metadata.Description;
            }

            if (result.Image == null || string.IsNullOrWhiteSpace(result.Image.Content))
            {
                result.Image = new ContentModel() { Content = settings.Value.Metadata.Image };
            }

            if (string.IsNullOrWhiteSpace(result.Keywords))
            {
                result.Keywords = settings.Value.Metadata.Keywords;
            }

            if (string.IsNullOrWhiteSpace(result.CanonicalURL))
            {
                result.CanonicalURL = httpContextAccessor.HttpContext.Request.GetDisplayUrl();
            }

            return result;
        }

        public MetadataViewModel GetMetadata()
        {
            MetadataViewModel result = new MetadataViewModel();

            if (string.IsNullOrWhiteSpace(result.Author))
            {
                result.Author = settings.Value.Metadata.Author;
            }

            if (string.IsNullOrWhiteSpace(result.Description))
            {
                result.Description = settings.Value.Metadata.Description;
            }

            if (result.Image == null || string.IsNullOrWhiteSpace(result.Image.Content))
            {
                result.Image = new ContentModel() { Content = settings.Value.Metadata.Image };
            }

            if (string.IsNullOrWhiteSpace(result.Keywords))
            {
                result.Keywords = settings.Value.Metadata.Keywords;
            }

            if (string.IsNullOrWhiteSpace(result.CanonicalURL))
            {
                result.CanonicalURL = httpContextAccessor.HttpContext.Request.GetDisplayUrl();
            }

            return result;
        }

    }
}
