﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class TagTypeRepository : ITagTypeRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.TagTypeRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public TagTypeRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.TAG_TYPE);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG_TYPE, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG_TYPE, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<TagType>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            try
            {
                List<TagType> tagTypes = (await cache.GetDeserializedAsync<List<TagType>>(CacheIdentifierHelper.GetAllTagTypeCachingKey()));
                if (tagTypes == null)
                {
                    tagTypes = await context.TagTypes.ToListAsync();

                    if (tagTypes != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllTagTypeCachingKey(), tagTypes, allCache);
                    }
                }

                return tagTypes;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public List<TagType> All()
        {
            const string functionIdentifier = "All()";

            try
            {
                List<TagType> tagTypes = cache.GetDeserialized<List<TagType>>(CacheIdentifierHelper.GetAllTagTypeCachingKey());
                if (tagTypes == null)
                {
                    tagTypes = context.TagTypes.ToList();

                    if (tagTypes != null)
                    {
                        cache.SetSerialized(CacheIdentifierHelper.GetAllTagTypeCachingKey(), tagTypes, allCache);
                    }
                }

                return tagTypes;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(TagType tagType)
        {
            const string functionIdentifier = "Delete(tagType)";

            try
            {
                context.Entry(tagType).State = EntityState.Deleted;
                context.TagTypes.Remove(tagType);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0} }}", tagType));
                throw;
            }
        }

        public async Task<TagType> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualTagTypeCachingKey(id);

            try
            {
                TagType tagType;
                if (bypassCache)
                {
                    tagType = await context.TagTypes.FindAsync(id);
                }
                else
                {
                    tagType = await cache.GetDeserializedAsync<TagType>(individualKey);
                    if (tagType == null)
                    {
                        tagType = await context.TagTypes.FindAsync(id);

                        if (tagType != null)
                        {
                            await cache.SetSerializedAsync(individualKey, tagType, findCache);
                        }
                    }
                }

                return tagType;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public TagType Find(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualTagTypeCachingKey(id);

            try
            {
                TagType tagType;
                if (bypassCache)
                {
                    tagType = context.TagTypes.Find(id);
                }
                else
                {
                    tagType = cache.GetDeserialized<TagType>(individualKey);
                    if (tagType == null)
                    {
                        tagType = context.TagTypes.Find(id);

                        if (tagType != null)
                        {
                            cache.SetSerialized(individualKey, tagType, findCache);
                        }
                    }
                }

                return tagType;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1}}}", id, bypassCache));
                throw;
            }
        }

        public async Task<TagType> FindWithWorkAssociationAsync(Guid id)
        {
            const string functionIdentifier = "FindWithWorkAssociationAsync(id)";

            try
            {
                TagType tagType = await context.TagTypes
                    .Include(tt => tt.Tags).ThenInclude(t => t.Works)
                    .Include(tt => tt.Tags).ThenInclude(t => t.Volumes)
                    .Include(tt => tt.Tags).ThenInclude(t => t.Chapters)
                    .AsNoTracking()
                    .FirstOrDefaultAsync(tt => tt.TagTypeID == id);

                return tagType;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public TagType FindWithWorkAssociation(Guid id)
        {
            const string functionIdentifier = "FindWithWorkAssociation(id)";

            try
            {
                TagType tagType = context.TagTypes
                    .Include(tt => tt.Tags).ThenInclude(t => t.Works)
                    .Include(tt => tt.Tags).ThenInclude(t => t.Volumes)
                    .Include(tt => tt.Tags).ThenInclude(t => t.Chapters)
                    .AsNoTracking()
                    .FirstOrDefault(tt => tt.TagTypeID == id);

                return tagType;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public void Insert(TagType tagType)
        {
            const string functionIdentifier = "Insert(tagType)";

            try
            {
                tagType.Create(userRepository.GetUserID());
                context.TagTypes.Add(tagType);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0} }}", tagType));
                throw;
            }
        }

        public void Update(TagType tagType)
        {
            const string functionIdentifier = "Update(tagType)";

            try
            {
                tagType.Update(userRepository.GetUserID());
                context.Entry(tagType).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagType:{0} }}", tagType));
                throw;
            }
        }
    }
}
