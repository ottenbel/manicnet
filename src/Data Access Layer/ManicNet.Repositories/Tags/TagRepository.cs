﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class TagRepository : ITagRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.TagRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public TagRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.TAG);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<Tag>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<Tag> tags = new List<Tag>();

            try
            {
                tags = (await cache.GetDeserializedAsync<List<Tag>>(CacheIdentifierHelper.GetAllTagCachingKey()));
                if (tags == null)
                {
                    tags = await context.Tags
                        .Include(t => t.TagType)
                        .Include(t => t.ImpliedBy)
                        .Include(t => t.Implies)
                        .Include(t => t.Aliases)
                        .Include(t => t.Works)
                        .Include(t => t.Volumes)
                        .Include(t => t.Chapters)
                        .AsNoTracking()
                    .ToListAsync();

                    if (tags != null)
                    {
                        //TODO: Tweak set count to only get count when we are caching the results
                        tags.ForEach(t => t.SetCount());

                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllTagCachingKey(), tags, allCache);
                    }
                }

                return tags;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public List<Tag> All()
        {
            const string functionIdentifier = "All()";

            List<Tag> tags = new List<Tag>();

            try
            {
                tags = cache.GetDeserialized<List<Tag>>(CacheIdentifierHelper.GetAllTagCachingKey());
                if (tags == null)
                {
                    tags = context.Tags
                        .Include(t => t.TagType)
                        .Include(t => t.ImpliedBy)
                        .Include(t => t.Implies)
                        .Include(t => t.Aliases)
                        .Include(t => t.Works)
                        .Include(t => t.Volumes)
                        .Include(t => t.Chapters)
                        .AsNoTracking()
                    .ToList();

                    if (tags != null)
                    {
                        tags.ForEach(t => t.SetCount());

                        cache.SetSerialized(CacheIdentifierHelper.GetAllTagCachingKey(), tags, allCache);
                    }
                }

                return tags;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(Tag tag)
        {
            const string functionIdentifier = "Delete(tag)";

            try
            {
                context.Entry(tag).State = EntityState.Deleted;
                context.Tags.Remove(tag);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0} }}", tag));
                throw;
            }
        }

        public async Task<Tag> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualTagCachingKey(id);

            try
            {
                Tag tag;
                if (bypassCache)
                {
                    tag = await GetDeepTagAsync(id, false);
                }
                else
                {
                    tag = await cache.GetDeserializedAsync<Tag>(individualKey);
                    if (tag == null)
                    {
                        tag = await GetDeepTagAsync(id, true);

                        if (tag != null)
                        {
                            await cache.SetSerializedAsync(individualKey, tag, findCache);
                        }
                    }
                }

                return tag;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public async Task<Tag> FindByNameAsync(string name)
        {
            const string functionIdentifier = "FindByNameAsync(name)";

            try
            {
                var query = context.Tags
                .Include(t => t.TagType)
                .Include(t => t.Implies)
                .Include(t => t.ImpliedBy)
                .Include(t => t.Aliases)
                .Include(t => t.Works)
                .Include(t => t.Volumes)
                .Include(t => t.Chapters).AsQueryable();

                Tag tag = await query.FirstOrDefaultAsync(t => t.Name == name);

                return tag;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ name:{0} }}", name));
                throw;
            }
        }

        public void Insert(Tag tag, IEnumerable<Tag> impliedTags)
        {
            const string functionIdentifier = "Insert(tag, impliedTags)";

            try
            {
                tag.Create(userRepository.GetUserID());

                foreach (Tag impliedTag in impliedTags)
                {
                    tag.Implies.Add(new Tag_Tag { ImpliedByTag = tag, ImpliesTagID = impliedTag.TagID });
                }

                context.Tags.Add(tag);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, impliedTags:{1} }}", tag, impliedTags));
                throw;
            }
        }

        public void Insert(Tag tag)
        {
            throw new NotImplementedException();
        }

        public void Update(Tag tag)
        {
            const string functionIdentifier = "Update(tag)";

            try
            {
                tag.Update(userRepository.GetUserID());
                context.Entry(tag).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0} }}", tag));
                throw;
            }
        }

        public List<Guid> Update(Tag tag, IEnumerable<Tag> impliedTags)
        {
            const string functionIdentifier = "Update(tag, impliedTags)";
            List<Guid> result = new List<Guid>();

            try
            {
                tag.Update(userRepository.GetUserID());

                List<Guid> noChangeInTags = tag.Implies.Select(t => t.ImpliesTagID).Intersect(impliedTags.Select(t => t.TagID)).ToList();
                List<Guid> addedTags = impliedTags.Where(t => !noChangeInTags.Contains(t.TagID)).Select(t => t.TagID).ToList();
                List<Guid> removedTags = tag.Implies.Select(t => t.ImpliesTagID).Where(t => !noChangeInTags.Contains(t)).ToList();
                List<Guid> modifiedTags = addedTags.Concat(removedTags).ToList();
                result = modifiedTags;

                tag.Implies.RemoveAll(t => removedTags.Contains(t.ImpliesTagID));
                foreach (Guid addedTag in addedTags)
                {
                    tag.Implies.Add(new Tag_Tag { ImpliedByTag = tag, ImpliesTagID = addedTag });
                }

                context.Entry(tag).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tag:{0}, impliedTags:{1} }}", tag, impliedTags));
                throw;
            }

            return result;
        }

        private async Task<Tag> GetDeepTagAsync(Guid id, bool asNoTracking)
        {
            var query = context.Tags
                .Include(t => t.TagType)
                .Include(t => t.Implies)
                .Include(t => t.ImpliedBy)
                .Include(t => t.Aliases)
                .Include(t => t.Works)
                .Include(t => t.Volumes)
                .Include(t => t.Chapters)
                .AsQueryable();

            if (asNoTracking)
            {
                query = query.AsNoTracking();
            }

            Tag tag = await query.FirstOrDefaultAsync(t => t.TagID == id);

            if (asNoTracking && tag != null)
            {
                tag.SetCount();
            }

            return tag;
        }
    }
}
