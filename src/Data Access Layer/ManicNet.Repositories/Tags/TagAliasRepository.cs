﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class TagAliasRepository : ITagAliasRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;

        private readonly string path = "ManicNet.Repositories.TagAliasRepository";

        private readonly CacheConfiguration allCache;

        public TagAliasRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.TAG_ALIAS);

            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG_ALIAS, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<TagAlias>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<TagAlias> tagAliases = new List<TagAlias>();

            try
            {
                tagAliases = (await cache.GetDeserializedAsync<List<TagAlias>>(CacheIdentifierHelper.GetAllTagAliasCachingKey()));
                if (tagAliases == null)
                {
                    tagAliases = await context.TagAliases
                        .Include(t => t.Tag).ToListAsync();

                    if (tagAliases != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllTagAliasCachingKey(), tagAliases, allCache);
                    }
                }

                return tagAliases;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public List<TagAlias> All()
        {
            const string functionIdentifier = "All()";

            List<TagAlias> tagAliases = new List<TagAlias>();

            try
            {
                tagAliases = cache.GetDeserialized<List<TagAlias>>(CacheIdentifierHelper.GetAllTagAliasCachingKey());
                if (tagAliases == null)
                {
                    tagAliases = context.TagAliases
                        .Include(t => t.Tag).ToList();

                    if (tagAliases != null)
                    {
                        cache.SetSerialized(CacheIdentifierHelper.GetAllTagAliasCachingKey(), tagAliases, allCache);
                    }
                }

                return tagAliases;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<TagAlias>> FindAsync(IEnumerable<string> names)
        {
            const string functionIdentifier = "FindAsync(name)";

            List<TagAlias> tagAlias = new List<TagAlias>();

            try
            {
                tagAlias = await context.TagAliases.Where(t => names.Contains(t.Name)).ToListAsync();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ name:{0} }}", names));
                throw;
            }

            return tagAlias;
        }
    }
}
