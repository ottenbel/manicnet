﻿using ManicNet.Contracts.Repositories;
using System;
using System.Security.Claims;

namespace ManicNet.Repositories
{
    public sealed class UserRepository : IUserRepository
    {
        private string humanReadableUserIdentifier;
        private Guid? userID;

        public string GetHumanReadableUserIdentifier()
        {
            return humanReadableUserIdentifier;
        }

        public Guid? GetUserID()
        {
            return userID;
        }

        public void Configure(ClaimsPrincipal User)
        {
            SetHumanReadableUserIdentifier(User);
            SetUserID(User);
        }

        private void SetHumanReadableUserIdentifier(ClaimsPrincipal User)
        {
            string identifier = string.Format("{{username:Unknown, userID:{0}}}| ", Guid.Empty);

            try
            {
                string userID = null;
                string username = null;

                if (User?.FindFirst(ClaimTypes.NameIdentifier)?.Value != null)
                {
                    userID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                }

                if (User?.FindFirst(ClaimTypes.Name)?.Value != null)
                {
                    username = User.FindFirst(ClaimTypes.Name)?.Value;
                }

                username = (!string.IsNullOrWhiteSpace(username)) ? username : "Unknown";
                userID = (!string.IsNullOrWhiteSpace(userID)) ? userID : Guid.Empty.ToString();

                identifier = string.Format("{{ username:{0}, userID:{1} }}| ", username, userID);
            }
            catch (Exception)
            {
                //Swallow the error if there are problems
            }

            humanReadableUserIdentifier = identifier;
        }

        private void SetUserID(ClaimsPrincipal User)
        {
            Guid? id = null;

            string identifier = "";

            if (User?.FindFirst(ClaimTypes.NameIdentifier)?.Value != null)
            {
                identifier = User?.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            }

            if (!string.IsNullOrWhiteSpace(identifier))
            {
                id = new Guid(identifier);
            }

            userID = id;
        }
    }
}
