﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class UserBlockedTagsRepository : IUserBlockedTagsRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;

        private readonly string path = "ManicNet.Repositories.UserBlockedTagsRepository";

        private readonly CacheConfiguration allCache;

        public UserBlockedTagsRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.USER_BLOCKED_TAG);

            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.USER_BLOCKED_TAG, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<User_Blocked_Tag>> All()
        {
            const string functionIdentifier = "All()";

            try
            {
                List<User_Blocked_Tag> userBlockedTags = (await cache.GetDeserializedAsync<List<User_Blocked_Tag>>(CacheIdentifierHelper.GetAllUserBlockedTagsCachingKey()));

                if (userBlockedTags == null)
                {
                    userBlockedTags = await context.User_Blocked_Tags.ToListAsync();

                    if (userBlockedTags != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllUserBlockedTagsCachingKey(), userBlockedTags, allCache);
                    }
                }

                return userBlockedTags;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<User_Blocked_Tag>> Find(Guid userID)
        {
            const string functionIdentifier = "Find(userID)";

            List<User_Blocked_Tag> userBlockedTag = new List<User_Blocked_Tag>();

            try
            {
                userBlockedTag = (await this.All()).Where(ubt => ubt.UserID == userID).ToList();
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ userID:{0} }}", userID));
                throw;
            }

            return userBlockedTag;
        }

        public async Task<User_Blocked_Tag> Find(Guid tagID, Guid userID)
        {
            const string functionIdentifier = "Find(tagID, userID)";

            User_Blocked_Tag userBlockedTag;

            try
            {
                userBlockedTag = (await this.All()).FirstOrDefault(ubt => ubt.TagID == tagID && ubt.UserID == userID);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ tagID:{0}, userID:{1} }}", tagID, userID));
                throw;
            }

            return userBlockedTag;
        }

        public void Insert(User_Blocked_Tag userBlockedTag)
        {
            const string functionIdentifier = "Insert(userBlockedTag)";

            try
            {
                context.User_Blocked_Tags.Add(userBlockedTag);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ userBlockedTag:{0} }}", userBlockedTag));
                throw;
            }
        }

        public void Remove(User_Blocked_Tag userBlockedTag)
        {
            const string functionIdentifier = "Remove(userBlockedTag)";

            try
            {
                context.Entry(userBlockedTag).State = EntityState.Deleted;
                context.User_Blocked_Tags.Remove(userBlockedTag);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ userBlockedTag:{0} }}", userBlockedTag));
                throw;
            }
        }
    }
}
