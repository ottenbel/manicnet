﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class ChapterRepository : IChapterRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.ChapterRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public ChapterRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.CHAPTER);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.CHAPTER, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.CHAPTER, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<Chapter>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<Chapter> chapters = new List<Chapter>();

            try
            {
                chapters = (await cache.GetDeserializedAsync<List<Chapter>>(CacheIdentifierHelper.GetAllChapterCachingKey()));
                if (chapters == null)
                {
                    chapters = await context.Chapters
                        .Include(c => c.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                        .Include(c => c.Volume).ThenInclude(v => v.Work)
                    .ToListAsync();

                    if (chapters != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllChapterCachingKey(), chapters, allCache);
                    }
                }

                return chapters;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(Chapter chapter)
        {
            const string functionIdentifier = "Delete(chapter)";

            try
            {
                context.Entry(chapter).State = EntityState.Deleted;
                context.Chapters.Remove(chapter);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0} }}", chapter));
                throw;
            }
        }

        public async Task<Chapter> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualChapterCachingKey(id);

            try
            {
                Chapter chapter;
                if (bypassCache)
                {
                    chapter = await GetDeepChapterAsync(id, false);
                }
                else
                {
                    chapter = await cache.GetDeserializedAsync<Chapter>(individualKey);
                    if (chapter == null)
                    {
                        chapter = await GetDeepChapterAsync(id, true);

                        if (chapter != null)
                        {
                            await cache.SetSerializedAsync(individualKey, chapter, findCache);
                        }
                    }
                }

                return chapter;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public void Insert(Chapter chapter)
        {
            throw new NotImplementedException();
        }

        public void Insert(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IEnumerable<Content> contents)
        {
            const string functionIdentifier = "Insert(work, primaryTags, secondaryTags, contents)";

            try
            {
                chapter.Create(userRepository.GetUserID());

                foreach (Tag tag in primaryTags)
                {
                    chapter.Tags.Add(new Chapter_Tag { Chapter = chapter, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    chapter.Tags.Add(new Chapter_Tag { Chapter = chapter, TagID = tag.TagID, IsPrimary = false });
                }

                int pageNumber = 1;
                foreach (Content content in contents)
                {
                    Page page = new Page() { Chapter = chapter, Content = content, Number = pageNumber };
                    chapter.Pages.Add(page);
                    pageNumber++;
                }

                context.Chapters.Add(chapter);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, primaryTags:{1}, secondaryTags:{2}, contents:{3} }}", chapter, primaryTags, secondaryTags, contents));
                throw;
            }
        }

        public void Update(Chapter chapter)
        {
            const string functionIdentifier = "Update(chapter)";

            try
            {
                chapter.Update(userRepository.GetUserID());
                context.Entry(chapter).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0} }}", chapter));
                throw;
            }
        }

        public void Update(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IEnumerable<Content> contents)
        {
            const string functionIdentifier = "Update(work, primaryTags, secondaryTags, contents)";

            try
            {
                chapter.Update(userRepository.GetUserID());

                chapter.Tags.Clear();

                foreach (Tag tag in primaryTags)
                {
                    chapter.Tags.Add(new Chapter_Tag { Chapter = chapter, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    chapter.Tags.Add(new Chapter_Tag { Chapter = chapter, TagID = tag.TagID, IsPrimary = false });
                }

                Page lastPage = chapter.Pages.OrderByDescending(p => p.Number).FirstOrDefault();
                int pageNumber = lastPage != null ? lastPage.Number + 1 : 1;
                foreach (Content content in contents)
                {
                    Page page = new Page() { Chapter = chapter, Content = content, Number = pageNumber };
                    chapter.Pages.Add(page);
                    pageNumber++;
                }

                //TODO: Mark removed pages?

                context.Entry(chapter).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ chapter:{0}, primaryTags:{1}, secondaryTags:{2}, contents:{3} }}", chapter, primaryTags, secondaryTags, contents));
                throw;
            }
        }

        private async Task<Chapter> GetDeepChapterAsync(Guid id, bool noTracking)
        {

            var query = context.Chapters
                .Include(c => c.Tags)
                .Include(c => c.Volume).ThenInclude(v => v.Tags)
                .Include(c => c.Volume).ThenInclude(v => v.Work).ThenInclude(w => w.Tags)
                .Include(c => c.Pages).ThenInclude(p => p.Content).ThenInclude(c => c.Thumbnail)
                .AsQueryable();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            Chapter chapter = await query.FirstOrDefaultAsync(c => c.ChapterID == id);

            return chapter;
        }
    }
}
