﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class WorkRepository : IWorkRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.WorkRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public WorkRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.WORK);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.WORK, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.WORK, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<Work>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<Work> works = new List<Work>();

            try
            {
                works = (await cache.GetDeserializedAsync<List<Work>>(CacheIdentifierHelper.GetAllWorkCachingKey()));
                if (works == null)
                {
                    works = await context.Works
                        .Include(w => w.Cover).ThenInclude(c => c.Thumbnail)
                        .Include(w => w.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                        .Include(w => w.Volumes).ThenInclude(v => v.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                        .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(c => c.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                    .ToListAsync();

                    if (works != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllWorkCachingKey(), works, allCache);
                    }
                }

                return works;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(Work work)
        {
            const string functionIdentifier = "Delete(work)";

            try
            {
                context.Entry(work).State = EntityState.Deleted;
                context.Works.Remove(work);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0} }}", work));
                throw;
            }
        }

        public async Task<Work> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualWorkCachingKey(id);

            try
            {
                Work work;
                if (bypassCache)
                {
                    work = await GetDeepWorkAsync(id, false);
                }
                else
                {
                    work = await cache.GetDeserializedAsync<Work>(individualKey);
                    if (work == null)
                    {
                        work = await GetDeepWorkAsync(id, true);

                        if (work != null)
                        {
                            await cache.SetSerializedAsync(individualKey, work, findCache);
                        }
                    }
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public Work Find(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "Find(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualWorkCachingKey(id);

            try
            {
                Work work;
                if (bypassCache)
                {
                    work = GetDeepWork(id, false);
                }
                else
                {
                    work = cache.GetDeserialized<Work>(individualKey);
                    if (work == null)
                    {
                        work = GetDeepWork(id, true);

                        if (work != null)
                        {
                            cache.SetSerialized(individualKey, work, findCache);
                        }
                    }
                }

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public async Task<Work> FindForDeletionAsync(Guid id, bool noTracking = false)
        {
            const string functionIdentifier = "FindForDeletion(id, noTracking)";

            try
            {
                var query = context.Works
                .Include(w => w.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Volumes).ThenInclude(v => v.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(v => v.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(c => c.Pages)
                    .ThenInclude(p => p.Content).ThenInclude(c => c.Thumbnail)
                .AsQueryable();

                if (noTracking)
                {
                    query = query.AsNoTracking();
                }

                Work work = await query.FirstOrDefaultAsync(w => w.WorkID == id);

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, noTracking:{1} }}", id, noTracking));
                throw;
            }
        }

        public async Task<Work> FindForDownloadAsync(Guid id)
        {
            const string functionIdentifier = "FindForDownload(id)";

            try
            {
                var query = context.Works
                .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(c => c.Pages).ThenInclude(p => p.Content)
                .AsQueryable();

                Work work = await query.FirstOrDefaultAsync(w => w.WorkID == id);

                return work;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public void Insert(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags)
        {
            const string functionIdentifier = "Insert(work, primaryTags, secondaryTags)";

            try
            {
                work.Create(userRepository.GetUserID());
                work.SortableDate = DateTime.UtcNow;

                foreach (Tag tag in primaryTags)
                {
                    work.Tags.Add(new Work_Tag { Work = work, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    work.Tags.Add(new Work_Tag { Work = work, TagID = tag.TagID, IsPrimary = false });
                }

                context.Works.Add(work);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0}, primaryTags:{1}, secondaryTags:{2} }}", work, primaryTags, secondaryTags));
                throw;
            }
        }

        public void Insert(Work work)
        {
            throw new NotImplementedException();
        }

        public void Update(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags)
        {

            const string functionIdentifier = "Update(work, primaryTags, secondaryTags)";

            try
            {
                work.Update(userRepository.GetUserID());

                work.Tags.Clear();

                foreach (Tag tag in primaryTags)
                {
                    work.Tags.Add(new Work_Tag { Work = work, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    work.Tags.Add(new Work_Tag { Work = work, TagID = tag.TagID, IsPrimary = false });
                }

                context.Entry(work).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0}, primaryTags:{1}, secondaryTags:{2} }}", work, primaryTags, secondaryTags));
                throw;
            }
        }

        public void Update(Work work)
        {
            const string functionIdentifier = "Update(work)";

            try
            {
                work.Update(userRepository.GetUserID());
                context.Entry(work).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ work:{0} }}", work));
                throw;
            }
        }

        private async Task<Work> GetDeepWorkAsync(Guid id, bool noTracking)
        {
            var query = context.Works
                .Include(w => w.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Volumes).ThenInclude(v => v.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(v => v.Tags)
                .Include(w => w.Collections).ThenInclude(c => c.Collection)
                .AsQueryable();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            Work work = await query.FirstOrDefaultAsync(w => w.WorkID == id);

            return work;
        }

        private Work GetDeepWork(Guid id, bool noTracking)
        {
            var query = context.Works
                .Include(w => w.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Cover).ThenInclude(c => c.Thumbnail)
                .Include(w => w.Volumes).ThenInclude(v => v.Tags)
                .Include(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(v => v.Tags)
                .Include(w => w.Collections).ThenInclude(c => c.Collection)
                .AsQueryable();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            Work work = query.FirstOrDefault(w => w.WorkID == id);

            return work;
        }
    }
}
