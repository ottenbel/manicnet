﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class VolumeRepository : IVolumeRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.VolumeRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public VolumeRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.VOLUME);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.VOLUME, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.VOLUME, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<Volume>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<Volume> volumes = new List<Volume>();

            try
            {
                volumes = (await cache.GetDeserializedAsync<List<Volume>>(CacheIdentifierHelper.GetAllVolumeCachingKey()));
                if (volumes == null)
                {
                    volumes = await context.Volumes
                        .Include(v => v.Cover).ThenInclude(c => c.Thumbnail)
                        .Include(v => v.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                        .Include(v => v.Chapters).ThenInclude(c => c.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                        .Include(v => v.Work)
                    .ToListAsync();

                    if (volumes != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllVolumeCachingKey(), volumes, allCache);
                    }
                }

                return volumes;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(Volume volume)
        {
            const string functionIdentifier = "Delete(volume)";

            try
            {
                context.Entry(volume).State = EntityState.Deleted;
                context.Volumes.Remove(volume);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0} }}", volume));
                throw;
            }
        }

        public async Task<Volume> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualVolumeCachingKey(id);

            try
            {
                Volume volume;
                if (bypassCache)
                {
                    volume = await GetDeepVolumeAsync(id, false);
                }
                else
                {
                    volume = await cache.GetDeserializedAsync<Volume>(individualKey);
                    if (volume == null)
                    {
                        volume = await GetDeepVolumeAsync(id, true);

                        if (volume != null)
                        {
                            await cache.SetSerializedAsync(individualKey, volume, findCache);
                        }
                    }
                }

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public async Task<Volume> FindForDownload(Guid id)
        {
            const string functionIdentifier = "FindForDownload(id)";

            try
            {

                Volume volume = await context.Volumes
                .Include(v => v.Work)
                .Include(v => v.Chapters).ThenInclude(c => c.Pages).ThenInclude(p => p.Content).AsNoTracking().FirstOrDefaultAsync(v => v.VolumeID == id);

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Volume> FindForDeletion(Guid id, bool noTracking = false)
        {
            const string functionIdentifier = "FindForDeletion(id, noTracking)";

            try
            {
                Volume volume;

                var query = context.Volumes
                .Include(v => v.Cover).ThenInclude(c => c.Thumbnail)
                .Include(v => v.Tags)
                .Include(v => v.Work)
                .Include(v => v.Chapters).ThenInclude(c => c.Tags)
                .Include(v => v.Chapters).ThenInclude(c => c.Pages)
                    .ThenInclude(p => p.Content).ThenInclude(c => c.Thumbnail)
                .AsQueryable();

                if (noTracking)
                {
                    query = query.AsNoTracking();
                }

                volume = await query.FirstOrDefaultAsync(v => v.VolumeID == id);

                return volume;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, noTracking:{1} }}", id, noTracking));
                throw;
            }
        }

        public void Insert(Volume volume)
        {
            throw new NotImplementedException();
        }

        public void Insert(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags)
        {
            const string functionIdentifier = "Insert(work, primaryTags, secondaryTags)";

            try
            {
                volume.Create(userRepository.GetUserID());

                foreach (Tag tag in primaryTags)
                {
                    volume.Tags.Add(new Volume_Tag { Volume = volume, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    volume.Tags.Add(new Volume_Tag { Volume = volume, TagID = tag.TagID, IsPrimary = false });
                }

                context.Volumes.Add(volume);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, primaryTags:{1}, secondaryTags:{2} }}", volume, primaryTags, secondaryTags));
                throw;
            }
        }

        public void Update(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags)
        {
            const string functionIdentifier = "Update(volume, primaryTags, secondaryTags)";

            try
            {
                volume.Update(userRepository.GetUserID());

                volume.Tags.Clear();

                foreach (Tag tag in primaryTags)
                {
                    volume.Tags.Add(new Volume_Tag { Volume = volume, TagID = tag.TagID, IsPrimary = true });
                }

                foreach (Tag tag in secondaryTags.Except(primaryTags))
                {
                    volume.Tags.Add(new Volume_Tag { Volume = volume, TagID = tag.TagID, IsPrimary = false });
                }

                context.Entry(volume).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0}, primaryTags:{1}, secondaryTags:{2} }}", volume, primaryTags, secondaryTags));
                throw;
            }
        }

        public void Update(Volume volume)
        {
            const string functionIdentifier = "Update(volume)";

            try
            {
                volume.Update(userRepository.GetUserID());
                context.Entry(volume).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ volume:{0} }}", volume));
                throw;
            }
        }

        private async Task<Volume> GetDeepVolumeAsync(Guid id, bool noTracking)
        {

            var query = context.Volumes
                .Include(v => v.Cover).ThenInclude(c => c.Thumbnail)
                .Include(v => v.Tags)
                .Include(v => v.Work).ThenInclude(w => w.Tags)
                .Include(v => v.Chapters).ThenInclude(c => c.Tags)
                .AsQueryable();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            Volume volume = await query.FirstOrDefaultAsync(v => v.VolumeID == id);

            return volume;
        }
    }
}
