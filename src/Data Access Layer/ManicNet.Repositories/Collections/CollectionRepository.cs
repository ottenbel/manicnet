﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class CollectionRepository : ICollectionRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.CollectionRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public CollectionRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.COLLECTION);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.COLLECTION, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.COLLECTION, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<Collection>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            List<Collection> collections = new List<Collection>();

            try
            {
                collections = (await cache.GetDeserializedAsync<List<Collection>>(CacheIdentifierHelper.GetAllCollectionCachingKey()));
                if (collections == null)
                {
                    collections = await context.Collections
                        .Include(c => c.Works)
                        .ToListAsync();

                    if (collections != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllCollectionCachingKey(), collections, allCache);
                    }
                }

                return collections;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(Collection collection)
        {
            const string functionIdentifier = "Delete(collection)";

            try
            {
                context.Entry(collection).State = EntityState.Deleted;
                context.Collections.Remove(collection);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0} }}", collection));
                throw;
            }
        }

        public async Task<Collection> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualCollectionCachingKey(id);

            try
            {
                Collection collection;
                if (bypassCache)
                {
                    collection = await GetDeepCollectionAsync(id, false);
                }
                else
                {
                    collection = await cache.GetDeserializedAsync<Collection>(individualKey);
                    if (collection == null)
                    {
                        collection = await GetDeepCollectionAsync(id, true);

                        if (collection != null)
                        {
                            await cache.SetSerializedAsync(individualKey, collection, findCache);
                        }
                    }
                }

                return collection;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public void Insert(Collection collection)
        {
            const string functionIdentifier = "Insert(collection)";

            try
            {
                collection.Create(userRepository.GetUserID());

                context.Collections.Add(collection);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0} }}", collection));
                throw;
            }
        }

        public void Update(Collection collection)
        {
            const string functionIdentifier = "Update(collection)";

            try
            {
                collection.Update(userRepository.GetUserID());
                context.Entry(collection).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0} }}", collection));
                throw;
            }
        }

        public void UpdateWithWork(Collection collection, Guid workID, int number)
        {
            const string functionIdentifier = "UpdateWithWork(collection, workID, number)";

            try
            {
                collection.Update(userRepository.GetUserID());
                collection.Works.Add(new Collection_Work() { Collection = collection, WorkID = workID, Number = number });
                context.Entry(collection).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ collection:{0}, workID:{1}, number:{2} }}", collection, workID, number));
                throw;
            }
        }

        private async Task<Collection> GetDeepCollectionAsync(Guid id, bool noTracking)
        {
            var query = context.Collections
                .Include(c => c.Works).ThenInclude(cw => cw.Work).ThenInclude(w => w.Cover).ThenInclude(c => c.Thumbnail)
                .Include(c => c.Works).ThenInclude(cw => cw.Work).ThenInclude(w => w.Tags).ThenInclude(t => t.Tag).ThenInclude(tt => tt.TagType)
                .Include(c => c.Works).ThenInclude(cw => cw.Work).ThenInclude(w => w.Volumes).ThenInclude(v => v.Tags).ThenInclude(t => t.Tag)
                .Include(c => c.Works).ThenInclude(cw => cw.Work).ThenInclude(w => w.Volumes).ThenInclude(v => v.Chapters).ThenInclude(c => c.Tags).ThenInclude(t => t.Tag)
                .AsQueryable();

            if (noTracking)
            {
                query = query.AsNoTracking();
            }

            Collection collection = await query.FirstOrDefaultAsync(c => c.CollectionID == id);

            return collection;
        }
    }
}
