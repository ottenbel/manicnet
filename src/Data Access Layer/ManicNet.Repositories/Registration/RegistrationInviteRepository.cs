﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class RegistrationInviteRepository : IRegistrationInviteRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.RegistrationInviteRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public RegistrationInviteRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.REGISTRATION_INVITE);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.REGISTRATION_INVITE, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.REGISTRATION_INVITE, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<RegistrationInvite>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            try
            {
                List<RegistrationInvite> registrationInvites = (await cache.GetDeserializedAsync<List<RegistrationInvite>>(CacheIdentifierHelper.GetAllRegistrationInviteCachingKey()));

                if (registrationInvites == null)
                {
                    registrationInvites = await context.RegistrationInvites.ToListAsync();

                    if (registrationInvites != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllRegistrationInviteCachingKey(), registrationInvites, allCache);
                    }
                }

                return registrationInvites;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "Delete(registrationInvite)";

            try
            {
                context.Entry(registrationInvite).State = EntityState.Deleted;
                context.RegistrationInvites.Remove(registrationInvite);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }

        public async Task<RegistrationInvite> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualRegistrationInviteCachingKey(id);

            try
            {
                RegistrationInvite registrationInvite = await cache.GetDeserializedAsync<RegistrationInvite>(individualKey);
                if (registrationInvite == null)
                {
                    registrationInvite = await context.RegistrationInvites.FindAsync(id);

                    if (registrationInvite != null)
                    {
                        await cache.SetSerializedAsync(individualKey, registrationInvite, findCache);
                    }
                }

                return registrationInvite;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public async Task<RegistrationInvite> FindAsync(string email)
        {
            const string functionIdentifier = "FindAsync(email)";

            try
            {
                RegistrationInvite registrationInvite = await context.RegistrationInvites.SingleOrDefaultAsync(r => r.Email == email);
                return registrationInvite;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ email:{0} }}", email));
                throw;
            }
        }

        public void Insert(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "Insert(registrationInvite)";

            try
            {
                registrationInvite.Create(userRepository.GetUserID());
                context.RegistrationInvites.Add(registrationInvite);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }

        public void Update(RegistrationInvite registrationInvite)
        {
            const string functionIdentifier = "Update(registrationInvite)";

            try
            {
                registrationInvite.Update(userRepository.GetUserID());
                context.Entry(registrationInvite).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ registrationInvite:{0} }}", registrationInvite));
                throw;
            }
        }
    }
}
