﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class CacheConfigurationRepository : ICacheConfigurationRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IDistributedCache cache;
        private readonly IUserRepository userRepository;

        private readonly string path = "ManicNet.Repositories.CacheConfigurationRepository";

        public CacheConfigurationRepository(ManicNetContext context, IManicNetLoggerService logger, IDistributedCache cache, IUserRepository userRepository/*, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService*/)
        {
            this.context = context;
            this.logger = logger;
            this.cache = cache;
            this.userRepository = userRepository;
        }

        public List<CacheConfiguration> All()
        {
            const string functionIdentifier = "All()";

            try
            {
                List<CacheConfiguration> cacheConfigurations = (cache.GetDeserialized<List<CacheConfiguration>>(CacheIdentifierHelper.GetAllCacheConfigurationCachingKey()));
                if (cacheConfigurations == null)
                {
                    cacheConfigurations = context.CacheConfigurations.ToList();

                    if (cacheConfigurations != null)
                    {
                        CacheConfiguration cacheToCacheWith = cacheConfigurations.Where(c => c.SectionID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.CACHE_CONFIGURATION, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
                        cache.SetSerialized(CacheIdentifierHelper.GetAllCacheConfigurationCachingKey(), cacheConfigurations, cacheToCacheWith);
                    }
                }

                return cacheConfigurations;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public async Task<List<CacheConfiguration>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            try
            {
                List<CacheConfiguration> cacheConfigurations = (await cache.GetDeserializedAsync<List<CacheConfiguration>>(CacheIdentifierHelper.GetAllCacheConfigurationCachingKey()));
                if (cacheConfigurations == null)
                {
                    cacheConfigurations = await context.CacheConfigurations.ToListAsync();

                    if (cacheConfigurations != null)
                    {
                        CacheConfiguration cacheToCacheWith = cacheConfigurations.Where(c => c.SectionID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.CACHE_CONFIGURATION, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllCacheConfigurationCachingKey(), cacheConfigurations, cacheToCacheWith);
                    }
                }

                return cacheConfigurations;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void BulkInsert(List<CacheConfiguration> cacheConfigurations)
        {
            const string functionIdentifier = "BulkInsert(cacheConfigurations";

            try
            {
                foreach (CacheConfiguration cacheConfiguration in cacheConfigurations)
                {
                    cacheConfiguration.Create(userRepository.GetUserID());

                    context.CacheConfigurations.Add(cacheConfiguration);
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ cacheConfigurations:{0} }}", cacheConfigurations));
                throw;
            }
        }

        public void BulkUpdate(List<CacheConfiguration> cacheConfigurations)
        {
            const string functionIdentifier = "BulkUpdate(cacheConfigurations)";

            try
            {
                foreach (CacheConfiguration cacheConfiguration in cacheConfigurations)
                {
                    cacheConfiguration.Update(userRepository.GetUserID());
                    context.Entry(cacheConfiguration).State = EntityState.Modified;
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ cacheConfigurations:{0} }}", cacheConfigurations));
                throw;
            }
        }
    }
}
