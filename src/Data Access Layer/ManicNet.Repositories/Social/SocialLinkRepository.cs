﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Helpers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public class SocialLinkRepository : ISocialLinkRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;
        private readonly IUserRepository userRepository;
        private readonly IDistributedCache cache;

        private readonly string path = "ManicNet.Repositories.RegistrationInviteRepository";

        private readonly CacheConfiguration allCache;
        private readonly CacheConfiguration findCache;

        public SocialLinkRepository(ManicNetContext context, IManicNetLoggerService logger, IUserRepository userRepository, IDistributedCache cache, ICacheConfigurationRetrievalService cacheConfigurationRetrievalService)
        {
            this.context = context;
            this.logger = logger;
            this.userRepository = userRepository;
            this.cache = cache;

            var caches = cacheConfigurationRetrievalService.GetCacheConfigurationsForSection(Constants.Caching.Configurations.Sections.SOCIAL_LINK);

            this.findCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.FIND).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG_TYPE, Constants.Caching.Configurations.LookupTypes.FIND, Constants.Caching.Configurations.LengthTypes.HOUR, 1);
            this.allCache = caches.Where(c => c.LookupTypeID == Constants.Caching.Configurations.LookupTypes.ALL).FirstOrDefault() ?? new CacheConfiguration(Constants.Caching.Configurations.Sections.TAG_TYPE, Constants.Caching.Configurations.LookupTypes.ALL, Constants.Caching.Configurations.LengthTypes.DAY, 30);
        }

        public async Task<List<SocialLink>> AllAsync()
        {
            const string functionIdentifier = "AllAsync()";

            try
            {
                List<SocialLink> socialLinks = cache.GetDeserialized<List<SocialLink>>(CacheIdentifierHelper.GetAllSocialLinkCachingKey());
                
                if (socialLinks == null)
                {
                    socialLinks = await context.SocialLinks.AsNoTracking().ToListAsync();

                    if (socialLinks != null)
                    {
                        await cache.SetSerializedAsync(CacheIdentifierHelper.GetAllSocialLinkCachingKey(), socialLinks, allCache);
                    }
                }
                    

                return socialLinks;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }

        public void Delete(SocialLink socialLink)
        {
            const string functionIdentifier = "Delete(socialLink)";

            try
            {
                context.Entry(socialLink).State = EntityState.Deleted;
                context.SocialLinks.Remove(socialLink);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }

        public async Task<SocialLink> FindAsync(Guid id, bool bypassCache = false)
        {
            const string functionIdentifier = "FindAsync(id, bypassCache)";

            string individualKey = CacheIdentifierHelper.GetIndividualSocialLinkCachingKey(id);

            try
            {
                SocialLink socialLink;
                
                if (bypassCache)
                {
                    socialLink = await context.SocialLinks.FindAsync(id);
                }
                else
                {
                    socialLink = cache.GetDeserialized<SocialLink>(individualKey);

                    if (socialLink == null)
                    {
                        socialLink = await context.SocialLinks.FindAsync(id);

                        if (socialLink != null)
                        {
                            await cache.SetSerializedAsync(individualKey, socialLink, findCache);
                        }    
                    }
                }
                
                return socialLink;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0}, bypassCache:{1} }}", id, bypassCache));
                throw;
            }
        }

        public void Insert(SocialLink socialLink)
        {
            const string functionIdentifier = "Insert(registrationInvite)";

            try
            {
                socialLink.Create(userRepository.GetUserID());
                context.SocialLinks.Add(socialLink);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }

        public void Update(SocialLink socialLink)
        {
            const string functionIdentifier = "Update(socialLink)";

            try
            {
                socialLink.Update(userRepository.GetUserID());
                context.Entry(socialLink).State = EntityState.Modified;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ socialLink:{0} }}", socialLink));
                throw;
            }
        }
    }
}
