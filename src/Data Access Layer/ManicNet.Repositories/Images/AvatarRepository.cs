﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class AvatarRepository : IAvatarRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Repositories.AvatarRepository";

        public AvatarRepository(ManicNetContext context, IManicNetLoggerService logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public async Task<Avatar> FindAsync(Guid id)
        {
            const string functionIdentifier = "FindAsync(id)";

            try
            {
                Avatar avatar = await context.Avatars
                    .Include(a => a.UserAvatars)
                    .FirstOrDefaultAsync(a => a.AvatarID == id);

                return avatar;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Avatar> FindAsync(byte[] hash)
        {
            const string functionIdentifier = "FindAsync(hash)";

            Avatar avatar = new Avatar();

            try
            {
                avatar = await context.Avatars.SingleOrDefaultAsync(t => t.FileHash.SequenceEqual(hash));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }

            return avatar;
        }

        public void Insert(Avatar avatar)
        {
            const string functionIdentifier = "Insert(avatar)";

            try
            {
                context.Avatars.Add(avatar);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ avatar:{0} }}", avatar));
                throw;
            }
        }

        public void Delete(Avatar avatar)
        {
            const string functionIdentifier = "Delete(avatar)";

            try
            {
                context.Entry(avatar).State = EntityState.Deleted;
                context.Avatars.Remove(avatar);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ avatar:{0} }}", avatar));
                throw;
            }
        }
    }
}
