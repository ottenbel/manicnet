﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class ContentRepository : IContentRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Repositories.ContentRepository";

        public ContentRepository(ManicNetContext context, IManicNetLoggerService logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public async Task<Content> FindAsync(Guid id)
        {
            const string functionIdentifier = "FindAsync(id)";

            try
            {
                Content content = await context.Contents
                    .Include(c => c.Thumbnail)
                    .Include(c => c.Works)
                    .Include(c => c.Volumes)
                    .Include(c => c.Pages)
                    .FirstOrDefaultAsync(a => a.ContentID == id);

                return content;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Content> FindAsync(byte[] hash)
        {
            const string functionIdentifier = "FindAsync(hash)";

            Content content = new Content();

            try
            {
                content = await context.Contents.SingleOrDefaultAsync(t => t.FileHash.SequenceEqual(hash));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{hash:{0}}}", hash));
                throw;
            }

            return content;
        }

        public void Insert(Content content)
        {
            const string functionIdentifier = "Insert(content)";

            try
            {
                context.Contents.Add(content);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ content:{0} }}", content));
                throw;
            }
        }

        public void Delete(Content content)
        {
            const string functionIdentifier = "Delete(content)";

            try
            {
                context.Entry(content).State = EntityState.Deleted;
                context.Contents.Remove(content);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ content:{0} }}", content));
                throw;
            }
        }
    }
}
