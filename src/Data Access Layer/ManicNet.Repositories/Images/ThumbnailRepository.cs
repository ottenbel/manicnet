﻿using ManicNet.Context;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Repositories
{
    public sealed class ThumbnailRepository : IThumbnailRepository
    {
        private readonly ManicNetContext context;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Repositories.ThumbnailRepository";

        public ThumbnailRepository(ManicNetContext context, IManicNetLoggerService logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public async Task<Thumbnail> FindAsync(Guid id)
        {
            const string functionIdentifier = "FindAsync(id)";

            try
            {
                Thumbnail thumbnail = await context.Thumbnails.FindAsync(id);

                return thumbnail;
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ id:{0} }}", id));
                throw;
            }
        }

        public async Task<Thumbnail> FindAsync(byte[] hash)
        {
            const string functionIdentifier = "FindAsync(hash)";

            Thumbnail thumbnail = new Thumbnail();

            try
            {
                thumbnail = await context.Thumbnails.SingleOrDefaultAsync(t => t.FileHash.SequenceEqual(hash));
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ hash:{0} }}", hash));
                throw;
            }

            return thumbnail;
        }

        public void Insert(Thumbnail thumbnail)
        {
            const string functionIdentifier = "Insert(thumbnail)";

            try
            {
                context.Thumbnails.Add(thumbnail);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ thumbnail:{0} }}", thumbnail));
                throw;
            }
        }

        public void Delete(Thumbnail thumbnail)
        {
            const string functionIdentifier = "Delete(thumbnail)";

            try
            {
                context.Entry(thumbnail).State = EntityState.Deleted;
                context.Thumbnails.Remove(thumbnail);
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex, string.Format("{{ thumbnail:{0} }}", thumbnail));
                throw;
            }
        }
    }
}
