﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Constants
{
    public static class Social
    {
        public static class LinkTypes
        {
            public static Guid EMAIL = new Guid("D21FA4A9-1362-4E21-883A-DA34ADD72492");
            public static Guid LINK = new Guid("DEE1DEB6-659E-4B81-9026-CF32EB35C7E6");
        }
    }
}
