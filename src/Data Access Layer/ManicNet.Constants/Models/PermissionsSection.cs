﻿using System.Collections.Generic;

namespace ManicNet.Constants.Models
{
    public class PermissionsSection
    {
        public string SectionName { get; set; }
        public List<string> OwnerPermissions { get; set; }
        public List<string> AdministratorPermissions { get; set; }
        public List<string> NormalPermissions { get; set; }

        public PermissionsSection()
        {
            OwnerPermissions = new List<string>();
            AdministratorPermissions = new List<string>();
            NormalPermissions = new List<string>();
        }

        public PermissionsSection(string sectionName, List<string> ownerPermissions, List<string> administratorPermissions, List<string> normalPermissions)
        {
            SectionName = sectionName;
            OwnerPermissions = ownerPermissions;
            AdministratorPermissions = administratorPermissions;
            NormalPermissions = normalPermissions;
        }
    }
}
