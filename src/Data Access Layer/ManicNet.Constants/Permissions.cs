﻿using ManicNet.Constants.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Constants
{
    public static class ManicNetPermissions
    {
        public const string PERMISSION_CLAIM_TYPE = "Permission";

        public static class User
        {
            public static class Owner
            {
                /// <summary>Manage administrator level permissions on an user account.</summary>
                public const string EDIT_ADMINISTRATOR_PERMISSIONS = "Edit Administrator Permissions";

                /// <summary>Manage owner level permissions on an user account.</summary>
                public const string EDIT_OWNER_PERMISSIONS = "Edit Owner Permissions";

                /// <summary>Manage standard permissions on a user account.</summary>
                public const string EDIT_USER_PERMISSIONS = "Edit User Permissions";

                /// <summary>Manage administrator level roles on an user account.</summary>
                public const string EDIT_ADMINISTRATOR_ROLES = "Edit Administrator Roles";

                /// <summary>Manage owner level roles on an user account.</summary>
                public const string EDIT_OWNER_ROLES = "Edit Owner Roles";

                /// <summary>Manage standard roless on a user account.</summary>
                public const string EDIT_USER_ROLES = "Edit User Roles";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_OWNER_PERMISSIONS,
                    EDIT_ADMINISTRATOR_PERMISSIONS,
                    EDIT_USER_PERMISSIONS,
                    EDIT_OWNER_ROLES,
                    EDIT_ADMINISTRATOR_ROLES,
                    EDIT_USER_ROLES,
                };
            }

            public static class Administrator
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("User", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class Role
        {
            public static class Owner
            {
                /// <summary>Manage administrator level permissions on an user account.</summary>
                public const string EDIT_ROLES = "Edit Role";

                /// <summary>Manage owner level permissions on an user account.</summary>
                public const string DELETE_ROLES = "Delete Role";

                public const string REASSIGN_DEFAULT_USER_ROLE = "Reassign Default User Role";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_ROLES,
                    DELETE_ROLES,
                    REASSIGN_DEFAULT_USER_ROLE,
                };
            }

            public static class Administrator
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Role", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class TagType
        {
            public static class Owner
            {
                public const string DELETE_TAG_TYPE = "Delete Tag Type";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_TAG_TYPE,
                };
            }

            public static class Administrator
            {
                public const string EDIT_TAG_TYPE = "Edit Tag Type";
                public const string VIEW_DELETED_TAG_TYPE = "View Deleted Tag Type";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_TAG_TYPE,
                    VIEW_DELETED_TAG_TYPE,
                };
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Tag Type", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class Tag
        {
            public static class Owner
            {
                public const string DELETE_TAG = "Delete Tag";
                public const string MIGRATE_TAG = "Migrate Tag";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_TAG,
                    MIGRATE_TAG,
                };
            }

            public static class Administrator
            {
                public const string EDIT_TAG = "Edit Tag";
                public const string VIEW_DELETED_TAG = "View Deleted Tag";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_TAG,
                    VIEW_DELETED_TAG,
                };
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Tag", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class RegistrationInvite
        {
            public static class Owner
            {
                public const string DELETE_REGISTRATION_INVITE = "Delete Registration Invite";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_REGISTRATION_INVITE,
                };
            }

            public static class Administrator
            {
                public const string EDIT_REGISTRATION_INVITE = "Edit Registration Invite";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_REGISTRATION_INVITE,
                };
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Registration Invite", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class Work
        {
            public static class Owner
            {
                public const string DELETE_WORK = "Delete Work";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_WORK,
                };
            }

            public static class Administrator
            {
                public const string EDIT_WORK = "Edit Work";
                public const string VIEW_DELETED_WORK = "View Deleted Work";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_WORK,
                    VIEW_DELETED_WORK,
                };
            }

            public static class Normal
            {
                public const string DOWNLOAD_WORK = "Download Work";
                public const string DOWNLOAD_VOLUME = "Download Volume";
                public const string DOWNLOAD_CHAPTER = "Download Chapter";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DOWNLOAD_WORK,
                    DOWNLOAD_VOLUME,
                    DOWNLOAD_CHAPTER
                };
            }

            public static readonly PermissionsSection section = new PermissionsSection("Work", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class Collection
        {
            public static class Owner
            {
                public const string DELETE_COLLECTION = "Delete Collection";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_COLLECTION,
                };
            }

            public static class Administrator
            {
                public const string EDIT_COLLECTION = "Edit Collection";
                public const string VIEW_DELETED_COLLECTION = "View Deleted Collection";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_COLLECTION,
                    VIEW_DELETED_COLLECTION,
                };
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Collection", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static class SiteConfiguration
        {
            public static class Cache
            {
                public static class Owner
                {
                    public const string MANAGE_CACHE_CONFIGURATIONS = "Manage Cache Configurations";

                    public static readonly List<string> Permissions = new List<string>()
                    {
                        MANAGE_CACHE_CONFIGURATIONS,
                    };
                }

                public static class Administrator
                {
                    public static readonly List<string> Permissions = new List<string>();
                }

                public static class Normal
                {
                    public static readonly List<string> Permissions = new List<string>();
                }

                public static readonly PermissionsSection section = new PermissionsSection("Cache Configuration", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
                public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
            }
        }

        public static class SocialLink
        {
            public static class Owner
            {
                public const string DELETE_SOCIAL_LINK = "Delete Social Link";

                public static readonly List<string> Permissions = new List<string>()
                {
                    DELETE_SOCIAL_LINK,
                };
            }

            public static class Administrator
            {
                public const string EDIT_SOCIAL_LINK = "Edit Social Link";
                public const string VIEW_DELETED_SOCIAL_LINK = "View Deleted Social Link";

                public static readonly List<string> Permissions = new List<string>()
                {
                    EDIT_SOCIAL_LINK,
                    VIEW_DELETED_SOCIAL_LINK,
                };
            }

            public static class Normal
            {
                public static readonly List<string> Permissions = new List<string>();
            }

            public static readonly PermissionsSection section = new PermissionsSection("Social Link", Owner.Permissions, Administrator.Permissions, Normal.Permissions);
            public static readonly List<string> Permissions = Owner.Permissions.Concat(Administrator.Permissions).Concat(Normal.Permissions).ToList();
        }

        public static readonly List<PermissionsSection> sections = new List<PermissionsSection>()
        {
            User.section,
            Role.section,
            TagType.section,
            Tag.section,
            RegistrationInvite.section,
            Work.section,
            Collection.section,
            SiteConfiguration.Cache.section,
            SocialLink.section,
        };

        public static readonly List<string> OwnerPermissions = User.Owner.Permissions.Concat(Role.Owner.Permissions)
            .Concat(TagType.Owner.Permissions)
            .Concat(Tag.Owner.Permissions)
            .Concat(RegistrationInvite.Owner.Permissions)
            .Concat(Work.Owner.Permissions)
            .Concat(Collection.Owner.Permissions)
            .Concat(SiteConfiguration.Cache.Owner.Permissions)
            .Concat(SocialLink.Owner.Permissions)
            .ToList();

        public static readonly List<string> AdministratorPermissions = User.Administrator.Permissions.Concat(Role.Administrator.Permissions)
            .Concat(TagType.Administrator.Permissions)
            .Concat(Tag.Administrator.Permissions)
            .Concat(RegistrationInvite.Administrator.Permissions)
            .Concat(Work.Administrator.Permissions)
            .Concat(Collection.Administrator.Permissions)
            .Concat(SiteConfiguration.Cache.Administrator.Permissions)
            .Concat(SocialLink.Administrator.Permissions)
            .ToList();
    }
}
