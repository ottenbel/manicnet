﻿namespace ManicNet.Constants
{
    public static class Database
    {
        public static string MSSQL = "MSSQL";
        public static string MYSQL = "MYSQL";
        public static string MARIADB = "MARIADB";
        public static string POSTGRESQL = "POSTGRESQL";
    }
}
