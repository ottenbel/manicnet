﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Constants
{
    public static class FrontEnd
    {
        public static Guid FREE = new Guid("BCD2B399-E51C-4570-B24C-EEA515621409"); //
        public static Guid BRANDS = new Guid("7877547F-0962-44D8-B57F-435DBF1FA770"); //

        public static string FREE_NAME = "Font Awesome 6 Free";
        public static string BRANDS_NAME = "Font Awesome 6 Brands";

        public static readonly List<Guid> FONT_FAMILIES = new List<Guid>() { FREE, BRANDS };

        //Dictonary mapping Guids to strings
        public static readonly Dictionary<Guid, string> FONT_FAMILY_MAP = new Dictionary<Guid, string>() 
        {
            { FREE, FREE_NAME } ,
            { BRANDS, BRANDS_NAME }
        };
    }
}
