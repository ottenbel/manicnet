﻿namespace ManicNet.Constants
{
    public static class Email
    {
        public static class Types
        {
            public static string NONE = "None";
            public static string SMTP = "SMTP";
            public static string SENDGRID = "SendGrid";
            public static string MAILGUN = "Mailgun";
        }

        public static class Mailgun
        {
            public static class Region
            {
                public const string USA = "USA";
                public const string EU = "EU";
            }
        }
    }
}
