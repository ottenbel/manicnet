﻿using System;

namespace ManicNet.Constants
{
    public static class Roles
    {
        public static readonly Guid DefaultOwnerID = new Guid("8D51D313-5C25-4ADE-82C4-1DF383CD2CF7");
        public static readonly Guid DefaultUserID = new Guid("32418B2D-C01D-4166-81A8-C354E59B054F");

        public static readonly string DefaultOwnerName = "Owner";
        public static readonly string DefaultUserName = "User";
    }
}
