﻿using System;

namespace ManicNet.Constants
{
    public static class WorkTypes
    {
        public static readonly Guid WORK = new Guid("D3F994EA-55C7-48A5-BE0C-15EC14624692");
        public static readonly Guid VOLUME = new Guid("09B65045-226A-4B15-B3F6-4682C00876F1");
        public static readonly Guid CHAPTER = new Guid("B402AE8A-8F81-4733-802A-E8B124911DE6");
    }
}
