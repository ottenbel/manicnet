﻿namespace ManicNet.Constants
{
    public static class ModelMapping
    {
        public static readonly string TAGS = "Tags";
        public static readonly string TAG_TYPES = "TagTypes";
        public static readonly string WORK = "Work";
        public static readonly string PREVIOUS_CHAPTER_ID = "PreviousChapterID";
        public static readonly string NEXT_CHAPTER_ID = "NextChapterID";
        public static readonly string LAST_PAGE_OF_PREVIOUS_CHAPTER = "LastPageOfPreviousChapter";
        public static readonly string CURRENT_PAGE = "CurrentPage";
        public static readonly string PRIMARY_TAGS = "PrimaryTags";
        public static readonly string SECONDARY_TAGS = "SecondaryTags";
    }
}
