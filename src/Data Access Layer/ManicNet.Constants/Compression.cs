﻿namespace ManicNet.Constants
{
    public static class Compression
    {
        public static readonly string LOSSLESS = "Lossless";
        public static readonly string COMPRESSION = "Compression";
        public static readonly string NONE = "None";
    }
}
