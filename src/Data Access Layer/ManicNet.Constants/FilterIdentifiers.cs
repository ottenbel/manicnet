﻿using System;

namespace ManicNet.Constants
{
    public static class FilterIdentifiers
    {
        public static readonly Guid ADDING = new Guid("9487A65B-A962-4C72-A6B5-92226CA609E7");
        public static readonly Guid EDITING = new Guid("A49317E5-A5BA-49D2-B01D-0F0A3427F985");
        public static readonly Guid RESTORING = new Guid("C6F96053-7DFC-46FF-9A15-CD23CE2FB2BE");

        public const string TAG_FROM_FILTER = "TagFromFilter";
        public const string TAG_TYPE_FROM_FILTER = "TagTypeFromFilter";
        public const string WORK_FROM_FILTER = "WorkFromFilter";
        public const string VOLUME_FROM_FILTER = "VolumeFromFilter";
        public const string CHAPTER_FROM_FILTER = "ChapterFromFilter";
        public const string COLLECTION_FROM_FILTER = "CollectionFromFilter";
        public const string SOCIAL_LINK_FROM_FILTER = "SocialLinkFromFilter";

        public const string WORK_ID_FROM_FILTER = "WorkIDFromFilter";
        public const string VOLUME_ID_FROM_FILTER = "VolumeIDFromFilter";
        public const string CHAPTER_ID_FROM_FILTER = "ChapterIDFromFilter";
        public const string COLLECTION_ID_FROM_FILTER = "CollectionIDFromFilter";
    }
}
