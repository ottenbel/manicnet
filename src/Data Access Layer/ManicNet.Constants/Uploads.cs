﻿namespace ManicNet.Constants
{
    public static class Uploads
    {
        public static class Type
        {
            public const string LOCAL = "Local";
            public const string AZURE_BLOB_STORAGE = "AzureBlobStorage";
            public const string AMAZON_S3 = "AmazonS3";
            public const string DIGITAL_OCEAN_SPACES = "DigitalOceanSpaces";
        }

        public static class AzureContainer
        {
            public const string PRIVATE = "Private";
            public const string BLOB = "Blob";
            public const string CONTAINER = "Container";
        }

        public static class AmazonS3
        {
            public static class Bucket
            {
                public const string PRIVATE = "Private";
                public const string PUBLIC = "Public";
            }

            public static class Region
            {
                public const string AF_SOUTH_1 = "AFSouth1";
                public const string AP_EAST_1 = "APEast1";
                public const string AP_NORTH_EAST_1 = "APNortheast1";
                public const string AP_NORTH_EAST_2 = "APNortheast2";
                public const string AP_NORTH_EAST_3 = "APNortheast3";
                public const string AP_SOUTH_1 = "APSouth1";
                public const string AP_SOUTH_EAST_1 = "APSoutheast1";
                public const string AP_SOUTH_EAST_2 = "APSoutheast2";
                public const string CA_CENTRAL_1 = "CACentral1";
                public const string CN_NORTH_1 = "CNNorth1";
                public const string CN_NORTH_WEST_1 = "CNNorthWest1";
                public const string EU_CENTRAL_1 = "EUCentral1";
                public const string EU_NORTH_1 = "EUNorth1";
                public const string EU_SOUTH_1 = "EUSouth1";
                public const string EU_WEST_1 = "EUWest1";
                public const string EU_WEST_2 = "EUWest2";
                public const string EU_WEST_3 = "EUWest3";
                public const string ME_SOUTH_1 = "MESouth1";
                public const string SA_EAST_1 = "SAEast1";
                public const string US_EAST_1 = "USEast1";
                public const string US_EAST_2 = "USEast2";
                public const string US_GOV_CLOUD_EAST_1 = "USGovCloudEast1";
                public const string US_GOV_CLOUD_WEST_1 = "USGovCloudWest1";
                public const string US_WEST_1 = "USWest1";
                public const string US_WEST_2 = "USWest2";

            }
        }
    }
}
