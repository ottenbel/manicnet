﻿namespace ManicNet.Constants
{
    public static class Regex
    {
        public const string TAG_NAME = @"^[A-Za-z0-9][A-Za-z0-9 ()\-\[\]]*[A-Za-z0-9()\-\[\]]$";
        public const string TAG_TYPE_NAME = @"^[A-Za-z0-9][A-Za-z0-9 ]*[A-Za-z0-9]$";
        public const string TAG_TYPE_PREFIX = @"^[A-Za-z0-9]*$";
        public const string COLOUR = @"^#[a-fA-F0-9]{6}$";
        public const string SOCIAL_LINK_NAME = @"^[A-Za-z0-9]*$";
    }
}
