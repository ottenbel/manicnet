﻿namespace ManicNet.Constants
{
    public class Routing
    {
        public class Areas
        {
            public const string ADMINISTRATION = "Administration";
            public const string API = "API";
            public const string IDENTITY = "Identity";
            public const string GENERAL = "";
        }

        public class Controllers
        {
            public class Tag
            {
                public const string CONTROLLER = "Tag";

                public class Routes
                {
                    public const string BULK_CREATE = "BulkCreate";
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string INDEX = "Index";
                    public const string MIGRATE = "Migrate";
                    public const string RESTORE = "Restore";
                    public const string SHOW = "Show";
                    public const string SOFT_DELETE = "SoftDelete";
                }
            }

            public class TagType
            {
                public const string CONTROLLER = "TagType";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string INDEX = "Index";
                    public const string RESTORE = "Restore";
                    public const string SHOW = "Show";
                    public const string SOFT_DELETE = "SoftDelete";
                }
            }

            public class Work
            {
                public const string CONTROLLER = "Work";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string DOWNLOAD = "Download";
                    public const string INDEX = "Index";
                    public const string RANDOM = "Random";
                    public const string RESTORE = "Restore";
                    public const string RSS = "Rss";
                    public const string SHOW = "Show";
                    public const string SOFT_DELETE = "SoftDelete";
                }
            }

            public class Collection
            {
                public const string CONTROLLER = "Collection";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string INDEX = "Index";
                    public const string RESTORE = "Restore";
                    public const string SHOW = "Show";
                    public const string SOFT_DELETE = "SoftDelete";

                    public const string ADD_WORK = "AddWork";
                }
            }

            public class Volume
            {
                public const string CONTROLLER = "Volume";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string DOWNLOAD = "Download";
                    public const string INDEX = "Index";
                    public const string MIGRATE = "Migrate";
                    public const string RESTORE = "Restore";
                    public const string SHOW = "Show";
                    public const string SOFT_DELETE = "SoftDelete";
                }
            }

            public class Chapter
            {
                public const string CONTROLLER = "Chapter";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string DELETED = "Deleted";
                    public const string DOWNLOAD = "Download";
                    public const string INDEX = "Index";
                    public const string MIGRATE = "Migrate";
                    public const string OVERVIEW = "Overview";
                    public const string RESTORE = "Restore";
                    public const string SOFT_DELETE = "SoftDelete";
                    public const string VIEWER = "Viewer";
                }
            }

            public class Role
            {
                public const string CONTROLLER = "Role";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string EDIT = "Edit";
                    public const string DELETE = "Delete";
                    public const string INDEX = "Index";
                    public const string REASSIGN = "Reassign";
                    public const string SHOW = "Show";
                }
            }

            public class RegistrationInvite
            {
                public const string CONTROLLER = "RegistrationInvite";

                public class Routes
                {
                    public const string APPROVE = "Approve";
                    public const string APPROVED = "Approved";
                    public const string CREATE = "Create";
                    public const string DELETE = "Delete";
                    public const string EXPIRED = "Expired";
                    public const string PENDING = "Pending";
                }
            }

            public class User
            {
                public const string CONTROLLER = "User";

                public class Routes
                {
                    public const string EDIT = "Edit";
                    public const string INDEX = "Index";
                }
            }

            public class Home
            {
                public const string CONTROLLER = "Home";

                public class Routes
                {
                    public const string COLLECTIONS = "Collections";
                    public const string INDEX = "Index";
                    public const string PRIVACY = "Privacy";
                    public const string SOCIAL_LINKS = "SocialLinks";
                    public const string TAGS = "Tags";
                    public const string USERS = "Users";
                    public const string WORKS = "Works";
                    public const string CONFIGURATIONS = "Configurations";
                }
            }

            public class CacheConfiguration
            {
                public const string CONTROLLER = "CacheConfiguration";

                public class Routes
                {
                    public const string MANAGE = "Manage";
                }
            }

            public class SocialLinks
            {
                public const string CONTROLLER = "SocialLinks";

                public class Routes
                {
                    public const string CREATE = "Create";
                    public const string DELETED = "Deleted";
                    public const string INDEX = "Index";
                    public const string SHOW = "Show";
                    public const string DELETE = "Delete";
                    public const string RESTORE = "Restore";
                    public const string EDIT = "Edit";
                    public const string SOFT_DELETE = "SoftDelete";
                }
            }
        }
    }
}
