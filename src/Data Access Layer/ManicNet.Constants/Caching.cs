﻿using System;

namespace ManicNet.Constants
{
    public static class Caching
    {
        public static class Types
        {
            public static readonly string MEMORY_CACHE = "MemoryCache";
            public static readonly string SQL_SERVER_CACHE = "SQLServerCache";
            public static readonly string REDIS_CACHE = "RedisCache";
        }

        public static class Configurations
        {
            public static class Sections
            {
                public static readonly Guid TAG_TYPE = new Guid("583560D5-A408-4310-8ED8-B0889F108ABE");
                public static readonly Guid TAG = new Guid("065DE6F6-543A-4FF1-93DD-947D44107F62");
                public static readonly Guid TAG_ALIAS = new Guid("2B87350F-2331-4018-A601-51AEFD5E9FE3");
                public static readonly Guid WORK = new Guid("3F01882F-2BE6-4D66-9A30-DB222627F5FC");
                public static readonly Guid VOLUME = new Guid("D04FCB68-4CE6-407D-A4C2-C2D9C8912607");
                public static readonly Guid CHAPTER = new Guid("BD7713D4-8830-4032-B813-B4489D6D83FD");
                public static readonly Guid COLLECTION = new Guid("2F4EA084-75F5-44FE-B0F1-A674045FB12E");
                public static readonly Guid REGISTRATION_INVITE = new Guid("F00D825A-FD75-4929-BE70-7C39FC2BF4A4");
                public static readonly Guid USER_BLOCKED_TAG = new Guid("BFECFA98-0080-42FE-9D04-7179C8295FE8");
                public static readonly Guid CACHE_CONFIGURATION = new Guid("B3E63D3B-C952-427F-A7D7-B6245DA114D3");
                public static readonly Guid SOCIAL_LINK = new Guid("1D612149-C5B6-4FD1-8302-22C663AF6887");
            }

            public static class LookupTypes
            {
                public static readonly Guid ALL = new Guid("767B4D73-F91C-413F-BFD1-4082D681B891");
                public static readonly Guid FIND = new Guid("E207768D-193A-4892-B212-F49518C0A0A4");
            }

            public static class LengthTypes
            {
                public static readonly Guid SECOND = new Guid("AE28BEEA-E0CC-481E-8552-680F96ED6753");
                public static readonly Guid MINUTE = new Guid("DAED07AD-8782-4320-97AE-8095E0C9ABFA");
                public static readonly Guid HOUR = new Guid("09A84BEF-6002-4667-8306-16B7A15D07FD");
                public static readonly Guid DAY = new Guid("88AB7892-6107-4D39-8481-A1FCD715AEF6");
            }
        }

        public static class Keys
        {
            public static class TagType
            {
                public static readonly string ALL = "TagTypeAll";
                public static readonly string INDIVIDUAL = "TagType";
            }

            public static class Tag
            {
                public static readonly string ALL = "TagAll";
                public static readonly string INDIVIDUAL = "Tag";
            }

            public static class TagAlias
            {
                public static readonly string ALL = "TagAliasAll";
                public static readonly string INDIVIDUAL = "TagAlias";
            }

            public static class RegistrationInvite
            {
                public static readonly string ALL = "RegistrationInviteAll";
                public static readonly string INDIVIDUAL = "RegistrationInvite";
            }

            public static class Work
            {
                public static readonly string ALL = "WorkAll";
                public static readonly string INDIVIDUAL = "Work";
            }

            public static class Volume
            {
                public static readonly string ALL = "VolumeAll";
                public static readonly string INDIVIDUAL = "Volume";
            }

            public static class Chapter
            {
                public static readonly string ALL = "ChapterAll";
                public static readonly string INDIVIDUAL = "Chapter";
            }

            public static class Collection
            {
                public static readonly string ALL = "CollectionAll";
                public static readonly string INDIVIDUAL = "Collection";
            }

            public static class SocialLink
            {
                public static readonly string ALL = "SocialLinkAll";
                public static readonly string INDIVIDUAL = "SocialLink";
            }

            public static class UserBlockedTags
            {
                public const string ALL = "UserBlockedTags";
            }

            public static class CacheConfiguration
            {
                public const string ALL = "CacheConfiguration";
            }
        }
    }
}
