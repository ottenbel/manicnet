﻿namespace ManicNet.Constants
{
    public static class Policies
    {
        public static class Generic
        {
            public const string ELEVATED_SITE_PERMISSION = "Elevated Site Permissions";
            public const string MANAGE_USER_PERMISSION = "Manage User Permission";
            public const string MANAGE_ROLE_PERMISSION = "Manage Role Permission";
            public const string MANAGE_TAG_GENERIC = "Manage Generic Tag Permission";
            public const string MANAGE_TAG_TYPE = "Manage Tag Type Permission";
            public const string MANAGE_TAG = "Manage Tag Permission";
            public const string MANAGE_REGISTRATION_INVITE = "Manage Registration Invite";
            public const string MANAGE_WORK = "Manage Work Permission";
            public const string MANAGE_COLLECTION = "Manage Collection Permission";
            public const string MANAGE_CONFIGURATION_GENERIC = "Manage Generic Configuration";
            public const string MANAGE_CACHING_CONFIGURATION_GENERIC = "Manage Caching Configuration Generic";
            public const string MANAGE_SOCIAL_LINK = "Manage Social Link Permission";
        }

        public static class Owner
        {
            public const string EDIT_OWNER_PERMISSIONS = "Edit Owner Permissions";
            public const string EDIT_ADMINISTRATOR_PERMISSIONS = "Edit Administrator Permissions";
            public const string EDIT_USER_PERMISSIONS = "Edit User Permissions";
            public const string EDIT_OWNER_ROLES = "Edit Owner Role";
            public const string EDIT_ADMINISTRATOR_ROLES = "Edit Administrator Role";
            public const string EDIT_USER_ROLES = "Edit User Role";
            public const string EDIT_ROLE = "Edit Role";
            public const string DELETE_ROLE = "Delete Role";
            public const string REASSIGN_DEFAULT_USER_ROLE = "Reassign Default User Role";
            public const string DELETE_TAG_TYPE = "Delete Tag Type";
            public const string DELETE_TAG = "Delete Tag";
            public const string MIGRATE_TAG = "Migrate Tag";
            public const string DELETE_REGISTRATION_INVITE = "Delete Registration Invite";
            public const string DELETE_WORK = "Delete Work";
            public const string DELETE_COLLECTION = "Delete Collection";
            public const string MANAGE_CACHE_CONFIGURATIONS = "Manage Cache Configurations";
            public const string DELETE_SOCIAL_LINK = "Delete Social Link";
        }

        public static class Administrator
        {
            public const string EDIT_TAG_TYPE = "Edit Tag Type";
            public const string VIEW_DELETED_TAG_TYPE = "View Deleted Tag Type";
            public const string EDIT_TAG = "Edit Tag";
            public const string VIEW_DELETED_TAG = "View Deleted Tag";
            public const string EDIT_REGISTRATION_INVITE = "Edit Registration Invite";
            public const string EDIT_WORK = "Edit Work";
            public const string VIEW_DELETED_WORK = "View Deleted Work";
            public const string EDIT_COLLECTION = "Edit Collection";
            public const string VIEW_DELETED_COLLECTION = "View Deleted Collection";
            public const string EDIT_SOCIAL_LINK = "Edit Social Link";
            public const string VIEW_DELETED_SOCIAL_LINK = "View Deleted Social Link";
        }

        public static class User
        {
            public const string DOWNLOAD_WORK = "Download Work";
            public const string DOWNLOAD_VOLUME = "Download Volume";
            public const string DOWNLOAD_CHAPTER = "Download Chapter";
        }
    }
}
