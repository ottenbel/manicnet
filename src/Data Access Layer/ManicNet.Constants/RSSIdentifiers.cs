﻿namespace ManicNet.Constants
{
    public static class RSSIdentifiers
    {
        public static readonly string TAG_RSS_IDENTIFIER = "TagRSSIdentifier";
        public static readonly string COLLECTION_RSS_IDENTIFIER = "CollectionRSSIdentifier";
        public static readonly string COLLECTION_RSS_NAME = "CollectionRSSName";
    }
}
