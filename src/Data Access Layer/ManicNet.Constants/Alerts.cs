﻿namespace ManicNet.Constants
{
    public static class Alerts
    {
        public static class TempData
        {
            public static readonly string SUCCESS = "success";
            public static readonly string ERRROR = "error";
            public static readonly string WARNING = "warning";
        }
    }
}
