﻿using ManicNet.Context.Extensions;
using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace ManicNet.Context
{
    public class ManicNetContext : IdentityDbContext<ManicNetUser, ManicNetRole, Guid>
    {
        public ManicNetContext()
        {

        }

        public ManicNetContext(DbContextOptions<ManicNetContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ConfigureTagTypeModelProperties()
                .ConfigureTagModelProperties()
                .ConfigureTagAliasModelProperties()
                .ConfigureContentModelProperties()
                .ConfigureThumbnailModelProperties()
                .ConfigureAvatarModelProperties()
                .ConfigureRegistrationInviteModelProperties()
                .ConfigureWorkModelProperties()
                .ConfigureVolumeModelProperties()
                .ConfigureChapterModelProperties()
                .ConfigurePageModelProperties()
                .ConfigureCollectionModelProperties()
                .ConfigureUserModelProperties()
                .ConfigureCacheConfigurationModelProperties()
                .ConfigureSocialLinkModelProperties();

            // https://stackoverflow.com/questions/28531201/entitytype-identityuserlogin-has-no-key-defined-define-the-key-for-this-entit
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TagType> TagTypes { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<TagAlias> TagAliases { get; set; }

        public DbSet<Content> Contents { get; set; }
        public DbSet<Thumbnail> Thumbnails { get; set; }
        public DbSet<Avatar> Avatars { get; set; }

        public DbSet<RegistrationInvite> RegistrationInvites { get; set; }

        public DbSet<Work> Works { get; set; }
        public DbSet<Volume> Volumes { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<User_Blocked_Tag> User_Blocked_Tags { get; set; }
        public DbSet<CacheConfiguration> CacheConfigurations { get; set; }
        public DbSet<SocialLink> SocialLinks { get; set; }
    }
}
