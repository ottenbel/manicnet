﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class ThumbnailCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the avatar model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureThumbnailModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddThumbnailModelProperties()
                .AddThumbnailModelRelationships()
                .AddThumbnailIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddThumbnailModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Thumbnail>().HasKey(t => t.ThumbnailID);
            modelBuilder.Entity<Thumbnail>().Property(t => t.ThumbnailID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Thumbnail>().Property(t => t.Filename).IsRequired();
            modelBuilder.Entity<Thumbnail>().Property(t => t.FileHash).IsRequired();

            return modelBuilder;
        }

        private static ModelBuilder AddThumbnailModelRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Thumbnail>().HasOne(t => t.Content).WithOne(c => c.Thumbnail).HasForeignKey<Thumbnail>(t => t.ContentID);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the avatars.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddThumbnailIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Thumbnail>().HasIndex(a => a.FileHash).IsUnique();

            return modelBuilder;
        }
    }
}
