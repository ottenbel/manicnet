﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class ContentCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the avatar model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureContentModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddContentModelProperties()
                .AddContentIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddContentModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Content>().HasKey(c => c.ContentID);
            modelBuilder.Entity<Content>().Property(c => c.ContentID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Content>().Property(c => c.Filename).IsRequired();
            modelBuilder.Entity<Content>().Property(c => c.FileHash).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the avatars.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddContentIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Content>().HasIndex(a => a.FileHash).IsUnique();

            return modelBuilder;
        }
    }
}
