﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class AvatarCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the avatar model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureAvatarModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddAvatarModelProperties()
                .AddAvatarIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddAvatarModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avatar>().HasKey(a => a.AvatarID);
            modelBuilder.Entity<Avatar>().Property(a => a.AvatarID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Avatar>().Property(a => a.Filename).IsRequired();
            modelBuilder.Entity<Avatar>().Property(a => a.FileHash).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the avatars.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddAvatarIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Avatar>().HasIndex(a => a.FileHash).IsUnique();

            return modelBuilder;
        }
    }
}
