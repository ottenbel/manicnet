﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class ChapterCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the chapter model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureChapterModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder = modelBuilder.AddChapterModelProperties()
                .AddChapterIndexes()
                .AddChapterRelationships()
                .AddTagToChapterRelationship();

            return modelBuilder;
        }

        private static ModelBuilder AddChapterModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter>().HasKey(c => c.ChapterID);
            modelBuilder.Entity<Chapter>().Property(c => c.ChapterID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Chapter>().Property(c => c.Number).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the works.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder AddChapterIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }

        public static ModelBuilder AddChapterRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter>().HasOne(c => c.Volume).WithMany(v => v.Chapters).HasForeignKey(c => c.VolumeID);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to define the chapter to tag relationship.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder AddTagToChapterRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter_Tag>()
                .HasKey(ct => new { ct.ChapterID, ct.TagID });

            modelBuilder.Entity<Chapter_Tag>().Property(ct => ct.IsPrimary).IsRequired();

            modelBuilder.Entity<Chapter_Tag>()
                .HasOne(ct => ct.Chapter)
                .WithMany(c => c.Tags)
                .HasForeignKey(ct => ct.ChapterID);

            modelBuilder.Entity<Chapter_Tag>()
                .HasOne(ct => ct.Tag)
                .WithMany(t => t.Chapters)
                .HasForeignKey(ct => ct.TagID);

            return modelBuilder;
        }
    }
}
