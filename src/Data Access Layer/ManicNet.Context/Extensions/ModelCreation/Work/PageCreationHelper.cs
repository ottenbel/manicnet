﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class PageCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the page model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigurePageModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder = modelBuilder.AddPageModelProperties()
                .AddPageIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddPageModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Page>().HasKey(p => p.PageID);
            modelBuilder.Entity<Page>().Property(p => p.PageID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Page>().Property(p => p.Number).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the works.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder AddPageIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }

        public static ModelBuilder AddPageRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Page>().HasOne(p => p.Chapter).WithMany(c => c.Pages).HasForeignKey(p => p.ChapterID);
            modelBuilder.Entity<Page>().HasOne(p => p.Content).WithMany(c => c.Pages).HasForeignKey(p => p.ContentID);

            return modelBuilder;
        }
    }
}
