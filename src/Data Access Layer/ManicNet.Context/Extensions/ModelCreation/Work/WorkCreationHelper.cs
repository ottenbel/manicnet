﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class WorkCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the work model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureWorkModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddWorkModelProperties()
                .AddWorkIndexes()
                .AddTagToWorkRelationship();

            return modelBuilder;
        }

        private static ModelBuilder AddWorkModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Work>().HasKey(w => w.WorkID);
            modelBuilder.Entity<Work>().Property(w => w.WorkID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Work>().Property(w => w.Title).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the works.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddWorkIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to define the work to tag relationship.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagToWorkRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Work_Tag>()
                .HasKey(ct => new { ct.WorkID, ct.TagID });

            modelBuilder.Entity<Work_Tag>().Property(wt => wt.IsPrimary).IsRequired();

            modelBuilder.Entity<Work_Tag>()
                .HasOne(ct => ct.Work)
                .WithMany(c => c.Tags)
                .HasForeignKey(ct => ct.WorkID);

            modelBuilder.Entity<Work_Tag>()
                .HasOne(ct => ct.Tag)
                .WithMany(t => t.Works)
                .HasForeignKey(ct => ct.TagID);

            return modelBuilder;
        }
    }
}
