﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class VolumeCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the volume model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureVolumeModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddVolumeModelProperties()
                .AddVolumeIndexes()
                .AddVolumeRelationships()
                .AddTagToVolumeRelationship();

            return modelBuilder;
        }

        private static ModelBuilder AddVolumeModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Volume>().HasKey(v => v.VolumeID);
            modelBuilder.Entity<Volume>().Property(v => v.VolumeID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Volume>().Property(v => v.Number).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the volumes.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddVolumeIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }

        private static ModelBuilder AddVolumeRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Volume>().HasOne(v => v.Work).WithMany(w => w.Volumes).HasForeignKey(v => v.WorkID);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to define the volume to tag relationship.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagToVolumeRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Volume_Tag>()
                .HasKey(ct => new { ct.VolumeID, ct.TagID });

            modelBuilder.Entity<Volume_Tag>().Property(vt => vt.IsPrimary).IsRequired();

            modelBuilder.Entity<Volume_Tag>()
                .HasOne(ct => ct.Volume)
                .WithMany(c => c.Tags)
                .HasForeignKey(ct => ct.VolumeID);

            modelBuilder.Entity<Volume_Tag>()
                .HasOne(ct => ct.Tag)
                .WithMany(t => t.Volumes)
                .HasForeignKey(ct => ct.TagID);

            return modelBuilder;
        }
    }
}
