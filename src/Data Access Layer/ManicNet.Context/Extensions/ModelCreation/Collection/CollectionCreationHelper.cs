﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class CollectionCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the chapter model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureCollectionModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder = modelBuilder.AddCollectionModelProperties()
                .AddCCollectionIndexes()
                .AddWorkToCollectionRelationship();

            return modelBuilder;
        }

        private static ModelBuilder AddCollectionModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Collection>().HasKey(c => c.CollectionID);
            modelBuilder.Entity<Collection>().Property(c => c.CollectionID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Collection>().Property(c => c.Title).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the works.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddCCollectionIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to define the chapter to tag relationship.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddWorkToCollectionRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Collection_Work>()
                .HasKey(cw => new { cw.CollectionID, cw.WorkID });

            modelBuilder.Entity<Collection_Work>().Property(cw => cw.Number).IsRequired();

            modelBuilder.Entity<Collection_Work>()
                .HasOne(cw => cw.Collection)
                .WithMany(c => c.Works)
                .HasForeignKey(cw => cw.CollectionID);

            modelBuilder.Entity<Collection_Work>()
                .HasOne(cw => cw.Work)
                .WithMany(w => w.Collections)
                .HasForeignKey(cw => cw.WorkID);

            return modelBuilder;
        }
    }
}
