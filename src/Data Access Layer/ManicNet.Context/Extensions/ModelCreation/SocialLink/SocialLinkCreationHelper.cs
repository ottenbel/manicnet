﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class SocialLinkCreationHelper
    {
        public static ModelBuilder ConfigureSocialLinkModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddSocialLinkModelProperties()
                .AddSocialLinkIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddSocialLinkModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SocialLink>().HasKey(s => s.SocialLinkID);
            modelBuilder.Entity<SocialLink>().Property(t => t.Name).IsRequired().HasMaxLength(10);
            modelBuilder.Entity<SocialLink>().Property(s => s.Priority).IsRequired();
            modelBuilder.Entity<SocialLink>().Property(s => s.Icon).HasMaxLength(6);
            modelBuilder.Entity<SocialLink>().Property(s => s.URL).IsRequired();
            modelBuilder.Entity<SocialLink>().Property(s => s.Text).IsRequired();
            modelBuilder.Entity<SocialLink>().Property(s => s.TypeID).IsRequired();

            return modelBuilder;
        }


        private static ModelBuilder AddSocialLinkIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SocialLink>().HasIndex(tt => tt.Name).IsUnique();

            return modelBuilder;
        }
    }
}
