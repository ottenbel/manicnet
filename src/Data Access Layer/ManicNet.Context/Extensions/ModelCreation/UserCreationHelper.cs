﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class UserCreationHelper
    {
        public static ModelBuilder ConfigureUserModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddUserAvatarRelationship()
                .AddUserBlockedTagTagRelationship();

            return modelBuilder;
        }

        private static ModelBuilder AddUserAvatarRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ManicNetUser>()
            .HasOne(u => u.Avatar)
            .WithMany(a => a.UserAvatars)
            .HasForeignKey(u => u.AvatarID);

            return modelBuilder;
        }

        private static ModelBuilder AddUserBlockedTagTagRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User_Blocked_Tag>()
            .HasKey(ubt => new { ubt.UserID, ubt.TagID });

            modelBuilder.Entity<User_Blocked_Tag>()
            .HasOne(ubt => ubt.User)
            .WithMany(u => u.UserBlockedTags)
            .HasForeignKey(ubt => ubt.UserID);

            modelBuilder.Entity<User_Blocked_Tag>()
            .HasOne(ubt => ubt.Tag)
            .WithMany(u => u.UserBlockedTags)
            .HasForeignKey(ubt => ubt.TagID);

            return modelBuilder;
        }
    }
}
