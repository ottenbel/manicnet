﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class CacheConfigurationsHelper
    {
        public static ModelBuilder ConfigureCacheConfigurationModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder = modelBuilder.AddCacheConfigurationModelProperties()
                .AddCacheConfigurationIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddCacheConfigurationModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CacheConfiguration>().HasKey(c => c.CacheConfigurationID);
            modelBuilder.Entity<CacheConfiguration>().Property(c => c.CacheConfigurationID).ValueGeneratedOnAdd();
            modelBuilder.Entity<CacheConfiguration>().Property(c => c.LookupTypeID).IsRequired();
            modelBuilder.Entity<CacheConfiguration>().Property(c => c.LengthTypeID).IsRequired();
            modelBuilder.Entity<CacheConfiguration>().Property(c => c.Length).IsRequired();

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the cache configuration.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder AddCacheConfigurationIndexes(this ModelBuilder modelBuilder)
        {
            //Leaving this as an empty passthrough for now in case we decide to index on something in the future
            return modelBuilder;
        }
    }
}
