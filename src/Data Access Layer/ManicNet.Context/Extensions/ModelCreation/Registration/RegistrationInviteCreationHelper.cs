﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class RegistrationInviteCreationHelper
    {
        public static ModelBuilder ConfigureRegistrationInviteModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddRegistrationInviteModelProperties()
                .AddRegistrationInviteIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddRegistrationInviteModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RegistrationInvite>().HasKey(r => r.RegistrationInviteID);
            modelBuilder.Entity<RegistrationInvite>().Property(r => r.RegistrationInviteID).ValueGeneratedOnAdd();
            modelBuilder.Entity<RegistrationInvite>().Property(r => r.Approved).IsRequired();
            modelBuilder.Entity<RegistrationInvite>().Property(r => r.Note).HasMaxLength(250);

            return modelBuilder;
        }


        private static ModelBuilder AddRegistrationInviteIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RegistrationInvite>().HasIndex(r => r.Email).IsUnique();

            return modelBuilder;
        }
    }
}
