﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class TagCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the tag model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureTagModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddTagModelProperties()
                .AddTagIndexes()
                .AddTagModelRelationships()
                .AddTagToTagRelationship();

            return modelBuilder;
        }

        public static ModelBuilder AddTagModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag>().HasKey(t => t.TagID);
            modelBuilder.Entity<Tag>().Property(t => t.TagID).ValueGeneratedOnAdd();
            modelBuilder.Entity<Tag>().Property(t => t.Name).IsRequired();
            modelBuilder.Entity<Tag>().Property(t => t.Icon).HasMaxLength(6);
            modelBuilder.Entity<Tag>().Ignore(t => t.Count);
            modelBuilder.Entity<Tag>().Ignore(t => t.IsPrimary);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the tags.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag>().HasIndex(t => t.Name).IsUnique();

            return modelBuilder;
        }

        private static ModelBuilder AddTagModelRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag>().HasOne(t => t.TagType).WithMany(tt => tt.Tags).HasForeignKey(t => t.TagTypeID);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to define the tag to tag relationship.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagToTagRelationship(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tag_Tag>()
            .HasKey(tt => new { tt.ImpliedByTagID, tt.ImpliesTagID });

            modelBuilder.Entity<Tag_Tag>()
            .HasOne(tt => tt.ImpliedByTag)
            .WithMany(t => t.Implies)
            .HasForeignKey(tt => tt.ImpliedByTagID)
            .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Tag_Tag>()
                .HasOne(tt => tt.ImpliesTag)
                .WithMany(t => t.ImpliedBy)
                .HasForeignKey(tt => tt.ImpliesTagID)
                .OnDelete(DeleteBehavior.Cascade);

            return modelBuilder;
        }
    }
}
