﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class TagTypeCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the tag type model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureTagTypeModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddTagTypeModelProperties()
                .AddTagTypeIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddTagTypeModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TagType>().HasKey(t => t.TagTypeID);
            modelBuilder.Entity<TagType>().Property(t => t.TagTypeID).ValueGeneratedOnAdd();
            modelBuilder.Entity<TagType>().Property(t => t.Name).IsRequired();
            modelBuilder.Entity<TagType>().Property(t => t.SearchablePrefix).IsRequired().HasMaxLength(20);
            modelBuilder.Entity<TagType>().Property(t => t.Priority).IsRequired();
            modelBuilder.Entity<TagType>().Property(t => t.PrimaryBodyColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.PrimaryBorderColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.PrimaryTextColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.SecondaryBodyColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.SecondaryBorderColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.SecondaryTextColour).HasMaxLength(7);
            modelBuilder.Entity<TagType>().Property(t => t.Icon).HasMaxLength(6);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the tag types.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagTypeIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TagType>().HasIndex(tt => tt.Name).IsUnique();
            modelBuilder.Entity<TagType>().HasIndex(tt => tt.SearchablePrefix).IsUnique();

            return modelBuilder;
        }

    }
}
