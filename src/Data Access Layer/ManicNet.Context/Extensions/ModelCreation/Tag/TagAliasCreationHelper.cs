﻿using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ManicNet.Context.Extensions
{
    public static class TagAliasCreationHelper
    {
        /// <summary>
        /// Extension method to handle the creation of all properties that need to be explicitly set 
        /// on the tag alias model (indexes/custom relationships/etc.)
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        public static ModelBuilder ConfigureTagAliasModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.AddTagAliasModelProperties()
                .AddTagAliasModelRelationships()
                .AddTagAliasIndexes();

            return modelBuilder;
        }

        private static ModelBuilder AddTagAliasModelProperties(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TagAlias>().HasKey(t => t.TagAliasID);
            modelBuilder.Entity<TagAlias>().Property(t => t.TagAliasID).ValueGeneratedOnAdd();
            modelBuilder.Entity<TagAlias>().Property(t => t.Name).IsRequired();

            return modelBuilder;
        }

        private static ModelBuilder AddTagAliasModelRelationships(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TagAlias>().HasOne(t => t.Tag).WithMany(c => c.Aliases).HasForeignKey(t => t.TagID);

            return modelBuilder;
        }

        /// <summary>
        /// Private extension method to register all indexes on the tag aliases.
        /// </summary>
        /// <param name="modelBuilder"></param>
        /// <returns></returns>
        private static ModelBuilder AddTagAliasIndexes(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TagAlias>().HasIndex(t => t.Name).IsUnique();

            return modelBuilder;
        }
    }
}
