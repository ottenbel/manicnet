﻿namespace ManicNet.Domain.Enums
{
    public static class FileValidation
    {
        public enum FileValidationResult
        {
            FileTooLarge = 0,
            FileTooSmall = 1,
            UnsupportedImageType = 2,
            InvalidFileType = 3,
            UnknownValidationErrror = 4,
            IsValid = 5,
        }
    }

}
