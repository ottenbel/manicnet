﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class RegistrationInvite : AuditableModel, IEquatable<RegistrationInvite>, IComparable<RegistrationInvite>
    {
        public Guid RegistrationInviteID { get; set; }

        public string Email { get; set; }

        public bool Approved { get; set; }

        public DateTime? Expires { get; set; }

        public string Note { get; set; }

        public RegistrationInvite()
        {
            //Default constructor
        }

        public RegistrationInvite(string Email, string Note)
        {
            this.Email = Email;
            this.Note = Note;
            Approved = false;
        }

        public RegistrationInvite(string Email, string Note, bool Approved)
        {
            this.Email = Email;
            this.Note = Note;
            this.Approved = Approved;
        }

        public RegistrationInvite(Guid RegistrationInviteID, string Email, string Note)
        {
            this.RegistrationInviteID = RegistrationInviteID;
            this.Email = Email;
            this.Note = Note;
            Approved = false;
        }

        public RegistrationInvite(Guid RegistrationInviteID, string Email, string Note, bool Approved, DateTime Expires)
        {
            this.RegistrationInviteID = RegistrationInviteID;
            this.Email = Email;
            this.Note = Note;
            this.Approved = Approved;
            this.Expires = Expires;
        }

        public bool Equals([AllowNull] RegistrationInvite r)
        {
            // If parameter is null, return false.
            if (r is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, r))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != r.GetType())
            {
                return false;
            }

            return this.RegistrationInviteID == r.RegistrationInviteID && this.Email == r.Email && this.Expires == r.Expires && this.Approved == r.Approved;
        }

        public override bool Equals(object obj)
        {
            return obj is RegistrationInvite registrationInviteObject && Equals(registrationInviteObject);
        }

        public int CompareTo([AllowNull] RegistrationInvite other)
        {
            return string.Compare(this.RegistrationInviteID.ToString(), other.RegistrationInviteID.ToString());
        }

        public override int GetHashCode()
        {
            return 0;
        }


    }
}
