﻿using System;

namespace ManicNet.Domain.Models
{
    public sealed class StylingSettings
    {
        public TagTypeStyling TagTypeStyling { get; set; }
    }

    public sealed class TagTypeStyling
    {
        public Colouring PrimaryColouring { get; set; }
        public Colouring SecondaryColouring { get; set; }
        public string DefaultIcon { get; set; }
        public Guid DefaultFontFamily { get; set; }
    }

    public sealed class Colouring
    {
        public string Body { get; set; }
        public string Border { get; set; }
        public string Text { get; set; }
    }
}
