﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetRegistrationSettings
    {
        public bool RequiresInvite { get; set; }
        public bool CanRequestInvite { get; set; }
        public int InviteLength { get; set; }
    }
}
