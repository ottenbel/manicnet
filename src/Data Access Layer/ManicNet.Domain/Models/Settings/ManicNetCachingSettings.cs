﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetCachingSettings
    {
        public string CacheType { get; set; }

        public SQLServerCacheSettings SQLServer { get; set; }
        public RedisCacheSettings RedisCache { get; set; }
    }

    public sealed class SQLServerCacheSettings
    {
        public string ConnectionString { get; set; }
        public string SchemaName { get; set; }
        public string TableName { get; set; }
    }

    public sealed class RedisCacheSettings
    {
        public string Configuration { get; set; }
        public string InstanceName { get; set; }
    }
}
