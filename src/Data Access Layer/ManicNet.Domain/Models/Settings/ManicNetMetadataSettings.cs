﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetMetadata
    {
        public string SiteName { get; set; }
        public string SiteCopyright { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Keywords { get; set; }
    }
}
