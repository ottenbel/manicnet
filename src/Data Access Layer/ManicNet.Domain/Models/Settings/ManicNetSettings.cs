﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetSettings
    {
        public ManicNetMetadata Metadata { get; set; }

        public const int DEFAULT_PAGE_LENGTH = 15;

        private int _PageLength;


        public int PageLength
        {
            get { return (_PageLength > 0) ? _PageLength : DEFAULT_PAGE_LENGTH; }
            set { _PageLength = value; }
        }

        public bool RequireAccount { get; set; }

        public ManicNetRegistrationSettings Registration { get; set; }

        public ManicNetCachingSettings CacheSettings { get; set; }

        public ManicNetTypeAheadSettings TypeAheadSettings { get; set; }

        public ManicNetFileUploadSettings FileUploadSettings { get; set; }

        public ManicNetSeedingSettings SeedingSettings { get; set; }

        public ManicNetEmailSettings EmailSettings { get; set; }
        public ManicNetDatabaseSettings DatabaseSettings { get; set; }

        public StylingSettings StylingSettings { get; set; }
    }
}