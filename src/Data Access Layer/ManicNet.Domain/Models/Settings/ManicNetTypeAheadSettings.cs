﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetTypeAheadSettings
    {
        public const int DEFAULT_MINIMUM_INPUT_LENGTH = 3;
        public const int DEFAULT_RESULTS_RETURNED_LENGTH = 6;

        private int _MinimumInputLength;
        private int _ResultsReturnedLength;

        public int MinimumInputLength
        {
            get { return (_MinimumInputLength > 0) ? _MinimumInputLength : DEFAULT_MINIMUM_INPUT_LENGTH; }
            set { _MinimumInputLength = value; }
        }

        public int ResultsReturnedLength
        {
            get { return (_ResultsReturnedLength > 0) ? _ResultsReturnedLength : DEFAULT_RESULTS_RETURNED_LENGTH; }
            set { _ResultsReturnedLength = value; }
        }
    }
}
