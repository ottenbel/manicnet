﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetSeedingSettings
    {
        public bool Migration { get; set; }
        public bool Roles { get; set; }
        public bool CacheConfiguration { get; set; }
        public bool TagCSS { get; set; }
        public bool SocialLinkCSS { get; set; }
    }
}
