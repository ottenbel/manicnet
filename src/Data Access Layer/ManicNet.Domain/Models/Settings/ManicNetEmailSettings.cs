﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetEmailSettings
    {
        public string SentAs { get; set; }
        public string EmailType { get; set; }

        public SMTPEmailSettings SMTP { get; set; }
        public SendGridEmailSettings SendGrid { get; set; }
        public MailgunEmailSettings Mailgun { get; set; }
    }

    public sealed class SMTPEmailSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public sealed class SendGridEmailSettings
    {
        public string ApiKey { get; set; }
        public bool SandboxMode { get; set; }
    }

    public sealed class MailgunEmailSettings
    {
        public string DomainName { get; set; }
        public string ApiKey { get; set; }
        public string Region { get; set; }
    }
}
