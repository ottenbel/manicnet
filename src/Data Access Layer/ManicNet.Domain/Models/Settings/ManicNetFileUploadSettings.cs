﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetFileUploadSettings
    {
        public int MaximumFileSize { get; set; }
        public string CompressionMethod { get; set; }
        public FileDimensions MaximumAvatarDimensions { get; set; }
        public FileDimensions MaximumThumbnailDimensions { get; set; }
        public string UploadType { get; set; }
        public LocalUpload LocalUpload { get; set; }

        public AzureBlobStorageUpload AzureBlobStorageUpload { get; set; }
        public AmazonS3Upload AmazonS3Upload { get; set; }

        public DigitalOceanSpacesUpload DigitalOceanSpacesUpload { get; set; }
    }

    public sealed class LocalUpload
    {
        public LocalUploadSettings Avatar { get; set; }
        public LocalUploadSettings Thumbnail { get; set; }
        public LocalUploadSettings Content { get; set; }
    }

    public sealed class AzureBlobStorageUpload
    {
        public string ConnectionString { get; set; }
        public AzureBlobStorageUploadSettings Avatar { get; set; }
        public AzureBlobStorageUploadSettings Thumbnail { get; set; }
        public AzureBlobStorageUploadSettings Content { get; set; }

    }

    public sealed class AmazonS3Upload
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public AmazonS3UploadSettings Avatar { get; set; }
        public AmazonS3UploadSettings Thumbnail { get; set; }
        public AmazonS3UploadSettings Content { get; set; }
    }

    public sealed class DigitalOceanSpacesUpload
    {
        public string AccessKey { get; set; }
        public string Secret { get; set; }
        public AmazonS3UploadSettings Avatar { get; set; }
        public AmazonS3UploadSettings Thumbnail { get; set; }
        public AmazonS3UploadSettings Content { get; set; }
    }

    public sealed class FileDimensions
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    public sealed class LocalUploadSettings
    {
        public string Directory { get; set; }
        public string PublicPath { get; set; }
    }

    public sealed class AzureBlobStorageUploadSettings
    {
        public string Container { get; set; }
        public string AccessLevel { get; set; }
    }

    public sealed class AmazonS3UploadSettings
    {
        public string Bucket { get; set; }
        public string Region { get; set; }
        public string AccessLevel { get; set; }
        public int SecureURLExpiryLength { get; set; }
    }

    public sealed class DigitalOceanSpacesUploadSettings
    {
        public string Bucket { get; set; }
        public string Region { get; set; }
        public string AccessLevel { get; set; }
        public int SecureURLExpiryLength { get; set; }
    }
}
