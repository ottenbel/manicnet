﻿namespace ManicNet.Domain.Models
{
    public sealed class ManicNetDatabaseSettings
    {
        public string DatabaseType { get; set; }

        public MYSQL MSSQL { get; set; }
        public MYSQL MYSQL { get; set; }
    }

    public sealed class MSSQL { }

    public sealed class MYSQL
    {
        public MYSQLServerVersion ServerVersion { get; set; }
    }

    public sealed class MariaDB
    {
        public MariaDBServerVersion ServerVersion { get; set; }
    }

    public sealed class PostgreSQL { }

    public sealed class MYSQLServerVersion
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
    }

    public sealed class MariaDBServerVersion
    {
        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
    }
}
