﻿using Microsoft.AspNetCore.Identity;
using System;

namespace ManicNet.Domain.Models
{
    public sealed class ManicNetRole : IdentityRole<Guid>
    {
        public string Description { get; set; }
        public bool IsDefaultUser { get; set; }

        public ManicNetRole()
        {
            IsDefaultUser = false;
        }
    }
}
