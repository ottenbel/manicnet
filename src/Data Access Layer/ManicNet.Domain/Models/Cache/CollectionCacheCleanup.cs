﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Domain.Models.Cache
{
    public sealed class CollectionCacheCleanup
    {
        public List<Guid> Collections;
        public List<Guid> Works;

        public CollectionCacheCleanup()
        {
            Collections = new List<Guid>();
            Works = new List<Guid>();
        }

        public CollectionCacheCleanup(Collection collection)
        {
            Collections = new List<Guid>();
            Works = new List<Guid>();

            if (collection != null)
            {
                if (collection.CollectionID != Guid.Empty)
                {
                    Collections.Add(collection.CollectionID);
                }

                Works = collection.Works.Select(w => w.WorkID).ToList();
            }
        }

        public CollectionCacheCleanup(Collection collection, IEnumerable<Guid> works)
        {
            Collections = new List<Guid>();
            Works = works != null ? works.Distinct().ToList() : new List<Guid>();

            if (collection != null)
            {
                if (collection.CollectionID != Guid.Empty)
                {
                    Collections.Add(collection.CollectionID);
                }

                Works = Works.Concat(collection.Works.Select(w => w.WorkID)).Distinct().ToList();
            }
        }
    }
}
