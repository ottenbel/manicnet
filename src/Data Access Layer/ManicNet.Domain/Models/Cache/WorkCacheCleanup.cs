﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Domain.Models.Cache
{
    //TODO: Add tests
    public sealed class WorkCacheCleanup
    {
        public List<Guid> Works { get; set; }
        public List<Guid> Volumes { get; set; }
        public List<Guid> Chapters { get; set; }
        public List<Guid> Tags { get; set; }
        public List<Guid> Collections { get; set; }

        public WorkCacheCleanup()
        {
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();
            Tags = new List<Guid>();
            Collections = new List<Guid>();
        }

        public WorkCacheCleanup(Work work)
        {
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();
            Tags = new List<Guid>();
            Collections = new List<Guid>();
            if (work != null)
            {
                if (work.WorkID != Guid.Empty)
                {
                    Works.Add(work.WorkID);
                }

                Volumes = work.Volumes.Select(v => v.VolumeID).Distinct().ToList();
                Chapters = work.Volumes.SelectMany(v => v.Chapters).Select(c => c.ChapterID).Distinct().ToList();
                Tags = work.Tags.Select(t => t.TagID).Concat(work.Volumes.SelectMany(v => v.Tags).Select(t => t.TagID)).Concat(work.Volumes.SelectMany(v => v.Chapters).SelectMany(c => c.Tags).Select(t => t.TagID)).Distinct().ToList();
                Collections = work.Collections.Select(c => c.CollectionID).Distinct().ToList();
            }
        }

        public WorkCacheCleanup(Work work, IEnumerable<Guid> tags)
        {
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();
            Tags = tags != null ? tags.ToList() : new List<Guid>();
            Collections = new List<Guid>();

            if (work != null)
            {
                if (work.WorkID != Guid.Empty)
                {
                    Works.Add(work.WorkID);
                }

                Volumes = work.Volumes.Select(v => v.VolumeID).Distinct().ToList();
                Chapters = work.Volumes.SelectMany(v => v.Chapters).Select(c => c.ChapterID).Distinct().ToList();
                Tags = Tags.Concat(work.Tags.Select(t => t.TagID)).Concat(work.Volumes.SelectMany(v => v.Tags).Select(t => t.TagID)).Concat(work.Volumes.SelectMany(v => v.Chapters).SelectMany(c => c.Tags).Select(t => t.TagID)).Distinct().ToList();
                Collections = work.Collections.Select(c => c.CollectionID).Distinct().ToList();
            }
        }

        public void Add(WorkCacheCleanup work)
        {
            Works = Works.Concat(work.Works).Distinct().ToList();
            Volumes = Volumes.Concat(work.Volumes).Distinct().ToList();
            Chapters = Chapters.Concat(work.Chapters).Distinct().ToList();
            Tags = Tags.Concat(work.Tags).Distinct().ToList();
            Collections = work.Collections.Concat(work.Collections).Distinct().ToList();
        }
    }
}
