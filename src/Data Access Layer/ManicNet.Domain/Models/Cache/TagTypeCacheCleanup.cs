﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Domain.Models.Cache
{
    public sealed class TagTypeCacheCleanup
    {
        public List<Guid> TagTypes { get; set; }
        public List<Guid> Tags { get; set; }
        public List<Guid> Works { get; set; }
        public List<Guid> Volumes { get; set; }
        public List<Guid> Chapters { get; set; }

        public TagTypeCacheCleanup()
        {
            TagTypes = new List<Guid>();
            Tags = new List<Guid>();
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();
        }

        public TagTypeCacheCleanup(TagType tagType)
        {
            TagTypes = new List<Guid>();
            Tags = new List<Guid>();
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();

            if (tagType != null && tagType.TagTypeID != Guid.Empty)
            {
                TagTypes.Add(tagType.TagTypeID);
            }
        }

        public TagTypeCacheCleanup(TagType tagType, IEnumerable<Tag> tags)
        {
            TagTypes = new List<Guid>();
            Tags = new List<Guid>();
            Works = tags != null ? tags.SelectMany(t => t.Works).Select(w => w.WorkID).Distinct().ToList() : new List<Guid>();
            Volumes = tags != null ? tags.SelectMany(t => t.Volumes).Select(v => v.VolumeID).Distinct().ToList() : new List<Guid>();
            Chapters = tags != null ? tags.SelectMany(t => t.Chapters).Select(c => c.ChapterID).Distinct().ToList() : new List<Guid>();

            if (tagType != null && tagType.TagTypeID != Guid.Empty)
            {
                TagTypes.Add(tagType.TagTypeID);
            }

            if (tags != null)
            {
                foreach (Tag tag in tags)
                {
                    if (tag.TagID != Guid.Empty)
                    {
                        Tags.Add(tag.TagID);
                    }

                    Tags = Tags.Concat(tag.ImpliedBy.Select(t => t.ImpliedByTagID)).Concat(tag.Implies.Select(t => t.ImpliesTagID)).ToList();
                }

                Tags = Tags.Distinct().ToList();
            }
        }
    }
}
