﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Domain.Models.Cache
{
    public sealed class TagCacheCleanup
    {
        public List<Guid> Tags;
        public List<Guid> RelatedTags;
        public List<Guid> Works;
        public List<Guid> Volumes;
        public List<Guid> Chapters;

        public TagCacheCleanup()
        {
            Tags = new List<Guid>();
            RelatedTags = new List<Guid>();
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();
        }

        public TagCacheCleanup(Tag tag)
        {
            Tags = new List<Guid>();
            RelatedTags = new List<Guid>();
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();

            if (tag != null)
            {
                if (tag.TagID != Guid.Empty)
                {
                    Tags.Add(tag.TagID);
                }

                RelatedTags = tag.ImpliedBy.Select(t => t.ImpliedByTagID).Concat(tag.Implies.Select(t => t.ImpliesTagID)).Distinct().ToList();
                Works = tag.Works.Select(t => t.WorkID).ToList();
                Volumes = tag.Volumes.Select(t => t.VolumeID).ToList();
                Chapters = tag.Chapters.Select(t => t.ChapterID).ToList();
            }
        }

        public TagCacheCleanup(Tag tag, IEnumerable<Guid> relatedTags)
        {
            Tags = new List<Guid>();
            RelatedTags = relatedTags != null ? relatedTags.ToList() : new List<Guid>();
            Works = new List<Guid>();
            Volumes = new List<Guid>();
            Chapters = new List<Guid>();

            if (tag != null)
            {
                if (tag.TagID != Guid.Empty)
                {
                    Tags.Add(tag.TagID);
                }

                RelatedTags = RelatedTags.Concat(tag.ImpliedBy.Select(t => t.ImpliedByTagID)).Concat(tag.Implies.Select(t => t.ImpliesTagID)).Distinct().ToList();
                Works = tag.Works.Select(t => t.WorkID).ToList();
                Volumes = tag.Volumes.Select(t => t.VolumeID).ToList();
                Chapters = tag.Chapters.Select(t => t.ChapterID).ToList();
            }
        }

        public void Add(TagCacheCleanup tag)
        {
            Tags = Tags.Concat(tag.Tags).Distinct().ToList();
            RelatedTags = RelatedTags.Concat(tag.RelatedTags).Distinct().ToList();
            Works = Works.Concat(tag.Works).Distinct().ToList();
            Volumes = Volumes.Concat(tag.Volumes).Distinct().ToList();
            Chapters = Chapters.Concat(tag.Chapters).Distinct().ToList();
        }
    }
}
