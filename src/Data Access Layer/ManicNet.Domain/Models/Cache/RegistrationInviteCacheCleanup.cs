﻿using System;
using System.Collections.Generic;

namespace ManicNet.Domain.Models.Cache
{
    public sealed class RegistrationInviteCacheCleanup
    {
        public List<Guid> RegistrationInvites;

        public RegistrationInviteCacheCleanup()
        {
            RegistrationInvites = new List<Guid>();
        }

        public RegistrationInviteCacheCleanup(RegistrationInvite registrationInvite)
        {
            RegistrationInvites = new List<Guid>();

            if (registrationInvite != null && registrationInvite.RegistrationInviteID != Guid.Empty)
            {
                RegistrationInvites.Add(registrationInvite.RegistrationInviteID);
            }
        }
    }
}
