﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Chapter : DescribableModel, IEquatable<Chapter>, IComparable<Chapter>
    {
        public Guid ChapterID { get; set; }

        public Guid VolumeID { get; set; }

        public int Number { get; set; }

        public string Title { get; set; }

        /*Navigation Properties*/
        public Volume Volume { get; set; }
        public List<Chapter_Tag> Tags { get; set; }
        public List<Page> Pages { get; set; }

        public Chapter()
        {
            this.Tags = new List<Chapter_Tag>();
            this.Pages = new List<Page>();
        }

        public Chapter(Guid ChapterID, Volume Volume, int Number, string Title, List<Page> Pages,
            string ShortDescription, string LongDescription, string URL, bool IsDeleted, DateTime CreatedDate,
            Guid? CreatedByUserID, DateTime UpdatedDate, Guid? UpdatedByUserID)
        {
            this.ChapterID = ChapterID;
            this.VolumeID = Volume.VolumeID;
            this.Volume = Volume;
            this.Number = Number;
            this.Title = Title;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsDeleted = IsDeleted;
            this.CreatedDate = CreatedDate;
            this.CreatedByUserID = CreatedByUserID;
            this.UpdatedDate = UpdatedDate;
            this.UpdatedByUserID = UpdatedByUserID;

            this.Pages = Pages ?? new List<Page>();
            this.Tags = new List<Chapter_Tag>();
        }

        public Chapter(Guid ChapterID, Volume Volume, int Number, string Title, List<Page> Pages,
            string ShortDescription, string LongDescription, string URL, bool IsDeleted, DateTime CreatedDate,
            Guid? CreatedByUserID, DateTime UpdatedDate, Guid? UpdatedByUserID, List<Chapter_Tag> tags)
        {
            this.ChapterID = ChapterID;
            this.Volume = Volume;
            this.VolumeID = Volume.VolumeID;
            this.Number = Number;
            this.Title = Title;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsDeleted = IsDeleted;
            this.CreatedDate = CreatedDate;
            this.CreatedByUserID = CreatedByUserID;
            this.UpdatedDate = UpdatedDate;
            this.UpdatedByUserID = UpdatedByUserID;

            this.Pages = Pages ?? new List<Page>();
            this.Tags = tags ?? new List<Chapter_Tag>();
        }

        public bool Equals(Chapter c)
        {
            // If parameter is null, return false.
            if (c is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, c))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != c.GetType())
            {
                return false;
            }

            return (this.ChapterID == c.ChapterID) && (this.VolumeID == c.VolumeID) && (this.Number == c.Number)
                && (this.Title == c.Title) && (this.ShortDescription == c.ShortDescription)
                && (this.LongDescription == c.LongDescription) && (this.URL == c.URL)
                && (this.IsDeleted == c.IsDeleted) && (this.CreatedDate == c.CreatedDate)
                && (this.CreatedByUserID == c.CreatedByUserID) && (this.UpdatedDate == c.UpdatedDate)
                && (this.UpdatedByUserID == c.UpdatedByUserID);
        }

        public override bool Equals(object obj)
        {
            return obj is Chapter chapterObject && Equals(chapterObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Chapter other)
        {
            return string.Compare(this.ChapterID.ToString(), other.ChapterID.ToString());
        }
    }
}
