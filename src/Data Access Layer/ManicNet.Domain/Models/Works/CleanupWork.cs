﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Domain.Models
{
    public sealed class CleanupWork : IEquatable<CleanupWork>
    {
        public List<Guid> Contents { get; set; }
        public List<Guid> Tags { get; set; }
        public List<Guid> Works { get; set; }
        public List<Guid> Volumes { get; set; }
        public List<Guid> Chapters { get; set; }

        public CleanupWork()
        {
            this.Contents = new List<Guid>();
            this.Tags = new List<Guid>();
            this.Works = new List<Guid>();
            this.Volumes = new List<Guid>();
            this.Chapters = new List<Guid>();
        }

        public CleanupWork(Work work)
        {
            this.Contents = new List<Guid>();
            this.Tags = work.Tags.Select(t => t.TagID).ToList();
            this.Works = new List<Guid>() { work.WorkID };
            this.Volumes = work.Volumes.Select(v => v.VolumeID).ToList();
            this.Chapters = new List<Guid>();

            if (work.CoverID.HasValue)
            {
                Contents.Add(work.CoverID.Value);
            }
        }

        public CleanupWork(Volume volume)
        {
            this.Contents = new List<Guid>();
            this.Tags = volume.Tags.Select(t => t.TagID).ToList();
            this.Works = new List<Guid>() { volume.WorkID };
            this.Volumes = new List<Guid>() { volume.VolumeID };
            this.Chapters = new List<Guid>();

            if (volume.CoverID.HasValue)
            {
                Contents.Add(volume.CoverID.Value);
            }
        }

        public CleanupWork(Chapter chapter)
        {
            this.Contents = new List<Guid>();
            this.Tags = chapter.Tags.Select(t => t.TagID).ToList();
            this.Works = new List<Guid>() { };
            this.Volumes = new List<Guid>() { chapter.VolumeID };
            this.Chapters = new List<Guid>() { chapter.ChapterID };

            if (chapter.Volume != null)
            {
                this.Works.Add(chapter.Volume.WorkID);
            }

            foreach (Page page in chapter.Pages)
            {
                Contents.Add(page.ContentID);
            }
        }

        public void Add(CleanupWork cleanupWork)
        {
            this.Contents = Contents.Concat(cleanupWork.Contents).Distinct().ToList();
            this.Tags = Tags.Concat(cleanupWork.Tags).Distinct().ToList();
            this.Works = Works.Concat(cleanupWork.Works).Distinct().ToList();
            this.Volumes = Volumes.Concat(cleanupWork.Volumes).Distinct().ToList();
            this.Chapters = Chapters.Concat(cleanupWork.Chapters).Distinct().ToList();
        }

        public void Clean()
        {
            this.Contents = Contents.Distinct().ToList();
            this.Tags = Tags.Distinct().ToList();
            this.Works = Works.Distinct().ToList();
            this.Volumes = Volumes.Distinct().ToList();
            this.Chapters = Chapters.Distinct().ToList();
        }

        public void Clear()
        {
            this.Contents.Clear();
            this.Tags.Clear();
            this.Works.Clear();
            this.Volumes.Clear();
            this.Chapters.Clear();
        }

        public bool Equals(CleanupWork t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.Contents.SequenceEqual(t.Contents)) && (this.Tags.SequenceEqual(t.Tags))
                && (this.Works.SequenceEqual(t.Works)) && (this.Volumes.SequenceEqual(t.Volumes))
                && (this.Chapters.SequenceEqual(t.Chapters));
        }

        public override bool Equals(object obj)
        {
            return obj is CleanupWork cleanupWorkObject && Equals(cleanupWorkObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}
