﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Work : WorkModel, IEquatable<Work>, IComparable<Work>
    {
        public Guid WorkID { get; set; }

        public override string Title { get; set; }

        public DateTime SortableDate { get; set; }

        /*Navigation Properties*/
        public List<Work_Tag> Tags { get; set; }
        public List<Volume> Volumes { get; set; }
        public List<Collection_Work> Collections { get; set; }

        public Work()
        {
            Tags = new List<Work_Tag>();
            Volumes = new List<Volume>();
            Collections = new List<Collection_Work>();
        }

        public Work(Guid WorkID, string Title, Guid? CoverID, DateTime SortableDate,
            string ShortDescription, string LongDescription, string URL, bool IsDeleted,
            DateTime CreatedDate, Guid? CreatedByUserID, DateTime UpdatedDate, Guid? UpdatedByUserID)
        {
            this.WorkID = WorkID;
            this.Title = Title;
            this.CoverID = CoverID;
            this.SortableDate = SortableDate;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsDeleted = IsDeleted;
            this.CreatedDate = CreatedDate;
            this.CreatedByUserID = CreatedByUserID;
            this.UpdatedDate = UpdatedDate;
            this.UpdatedByUserID = UpdatedByUserID;

            Tags = new List<Work_Tag>();
            Volumes = new List<Volume>();
            Collections = new List<Collection_Work>();
        }

        public bool Equals(Work t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.WorkID == t.WorkID) && (this.Title == t.Title)
                && (this.CoverID == t.CoverID) && (this.SortableDate == t.SortableDate)
                && (this.ShortDescription == t.ShortDescription) && (this.LongDescription == LongDescription)
                && (this.URL == t.URL) && (this.IsDeleted == IsDeleted) && (this.CreatedDate == CreatedDate)
                && (this.CreatedByUserID == CreatedByUserID) && (this.UpdatedDate == UpdatedDate)
                && (this.UpdatedByUserID == UpdatedByUserID);
        }

        public override bool Equals(object obj)
        {
            return obj is Work workObject && Equals(workObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Work other)
        {
            return string.Compare(this.WorkID.ToString(), other.WorkID.ToString());
        }
    }
}
