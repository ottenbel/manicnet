﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public class WorkModel : DescribableModel
    {
        public virtual string Title { get; set; }
        public Guid? CoverID { get; set; }
        public Content Cover { get; set; }
    }
}
