﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Volume : WorkModel, IEquatable<Volume>, IComparable<Volume>
    {
        public Guid VolumeID { get; set; }

        public Guid WorkID { get; set; }

        public int Number { get; set; }

        /*Navigation Properties*/
        public Work Work { get; set; }
        public List<Volume_Tag> Tags { get; set; }
        public List<Chapter> Chapters { get; set; }

        public Volume()
        {
            Tags = new List<Volume_Tag>();
            Chapters = new List<Chapter>();
        }

        public Volume(Guid VolumeID, Work Work, int Number, string Title, Guid? CoverID,
            string ShortDescription, string LongDescription, string URL,
            bool IsDeleted, DateTime CreatedDate, Guid? CreatedByUserID,
            DateTime UpdatedDate, Guid? UpdatedByUserID)
        {
            this.VolumeID = VolumeID;
            this.Work = Work;
            this.WorkID = Work.WorkID;
            this.Number = Number;
            this.Title = Title;
            this.CoverID = CoverID;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsDeleted = IsDeleted;
            this.CreatedDate = CreatedDate;
            this.CreatedByUserID = CreatedByUserID;
            this.UpdatedDate = UpdatedDate;
            this.UpdatedByUserID = UpdatedByUserID;

            Tags = new List<Volume_Tag>();
            Chapters = new List<Chapter>();
        }

        public Volume(Guid VolumeID, Work Work, int Number, string Title, Guid? CoverID,
            string ShortDescription, string LongDescription, string URL, bool IsDeleted,
            DateTime CreatedDate, Guid? CreatedByUserID, DateTime UpdatedDate,
            Guid? UpdatedByUserID, List<Volume_Tag> tags)
        {
            this.VolumeID = VolumeID;
            this.Work = Work;
            this.WorkID = Work.WorkID;
            this.Number = Number;
            this.Title = Title;
            this.CoverID = CoverID;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsDeleted = IsDeleted;
            this.CreatedDate = CreatedDate;
            this.CreatedByUserID = CreatedByUserID;
            this.UpdatedDate = UpdatedDate;
            this.UpdatedByUserID = UpdatedByUserID;

            this.Tags = tags ?? new List<Volume_Tag>();
            Chapters = new List<Chapter>();
        }

        public bool Equals(Volume t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.VolumeID == t.VolumeID) && (this.WorkID == t.Work.WorkID) && (this.Number == t.Number)
                && (this.Title == t.Title) && (this.CoverID == t.CoverID)
                && (this.ShortDescription == t.ShortDescription) && (this.LongDescription == t.LongDescription)
                && (this.IsDeleted == t.IsDeleted) && (this.CreatedDate == t.CreatedDate)
                && (this.URL == t.URL) && (this.CreatedByUserID == t.CreatedByUserID)
                && (this.UpdatedDate == t.UpdatedDate) && (this.UpdatedByUserID == t.UpdatedByUserID);
        }

        public override bool Equals(object obj)
        {
            return obj is Volume volumeObject && Equals(volumeObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Volume other)
        {
            return string.Compare(this.WorkID.ToString(), other.WorkID.ToString());
        }
    }
}