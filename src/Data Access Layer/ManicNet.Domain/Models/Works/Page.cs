﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Page
    {
        public Guid PageID { get; set; }

        public Guid ChapterID { get; set; }

        public Guid ContentID { get; set; }

        public int Number { get; set; }

        /*Navigation Properties*/
        public Chapter Chapter { get; set; }
        public Content Content { get; set; }
    }
}
