﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public class DescribableModel : AuditableModel
    {
        [MaxLength(160)]
        public string ShortDescription { get; set; }

        public string LongDescription { get; set; }

        public string URL { get; set; }
    }
}
