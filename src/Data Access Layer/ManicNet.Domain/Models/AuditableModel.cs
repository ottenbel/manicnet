﻿using System;

namespace ManicNet.Domain.Models
{
    /// <summary>
    /// A shared base class used to provide common properties to all inherited classes.
    /// </summary>
    [Serializable]
    public class AuditableModel
    {
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public Guid? CreatedByUserID { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Guid? UpdatedByUserID { get; set; }

        /// <summary>
        /// Function called item creation of the item that populates default creation information about the item.
        /// </summary>
        /// <param name="userGuid">The user creating the item in question.</param>
        public void Create(Guid? userGuid)
        {
            CreatedByUserID = userGuid;
            CreatedDate = DateTime.UtcNow;
            UpdatedByUserID = userGuid;
            UpdatedDate = DateTime.UtcNow;
        }

        /// <summary>
        /// Function called during item updates that records information about the update.
        /// </summary>
        /// <param name="userGuid">The user updating the item in question.</param>
        public void Update(Guid? userGuid)
        {
            UpdatedByUserID = userGuid;
            UpdatedDate = DateTime.UtcNow;
        }
    }
}
