﻿using System;

namespace ManicNet.Domain.Models
{
    public sealed class CacheConfiguration : AuditableModel
    {
        public Guid CacheConfigurationID { get; set; }
        public Guid SectionID { get; set; }
        public Guid LookupTypeID { get; set; }
        public Guid LengthTypeID { get; set; }
        public int Length { get; set; }

        public CacheConfiguration()
        {
            //Empty constructor
        }

        public CacheConfiguration(Guid section, Guid lookupTypeID, Guid lengthTypeID, int length)
        {
            this.SectionID = section;
            this.LookupTypeID = lookupTypeID;
            this.LengthTypeID = lengthTypeID;
            this.Length = length;
        }
    }
}
