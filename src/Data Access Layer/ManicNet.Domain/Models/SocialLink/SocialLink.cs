﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Domain.Models
{
    public class SocialLink : AuditableModel
    {
        public Guid SocialLinkID { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }
        public string Icon { get; set; } //Default to link or email 
        public Guid? FontFamily { get; set; }
        public string URL { get; set; }
        public string Text { get; set; }
        public Guid TypeID { get; set; } //Link VS Email
    }
}
