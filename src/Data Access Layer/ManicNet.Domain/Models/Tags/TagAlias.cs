﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class TagAlias : IEquatable<TagAlias>, IComparable<TagAlias>
    {
        public Guid TagAliasID { get; set; }

        public Guid TagID { get; set; }

        public string Name { get; set; }

        /*Navigation Properties*/
        public Tag Tag { get; set; }

        public TagAlias()
        {
            //Default constructor
        }

        public TagAlias(Guid TagAliasID, Guid TagID, string Name)
        {
            this.TagAliasID = TagAliasID;
            this.TagID = TagID;
            this.Name = Name;
        }

        public TagAlias(string Name)
        {
            this.Name = Name;
        }

        public TagAlias(Guid TagAliasID, Guid TagID, string Name, Tag Tag)
        {
            this.TagAliasID = TagAliasID;
            this.TagID = TagID;
            this.Name = Name;
            this.Tag = Tag;
        }

        public bool Equals(TagAlias t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.TagAliasID == t.TagAliasID) && (this.TagID == t.TagID)
                && (this.Name == t.Name);
        }

        public override bool Equals(object obj)
        {
            return obj is TagAlias tagAliasObject && Equals(tagAliasObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] TagAlias other)
        {
            return string.Compare(this.TagAliasID.ToString(), other.TagAliasID.ToString());
        }
    }
}
