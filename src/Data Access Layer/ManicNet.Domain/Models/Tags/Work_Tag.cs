﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Work_Tag
    {
        public Guid WorkID { get; set; }

        public Guid TagID { get; set; }

        public bool IsPrimary { get; set; }

        /*Navigation Properties*/
        public Work Work { get; set; }
        public Tag Tag { get; set; }

        public Work_Tag()
        {
            //Default constructor
        }

        public Work_Tag(Work Work, Tag Tag)
        {
            this.Work = Work;
            this.Tag = Tag;
        }
    }
}
