﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Chapter_Tag
    {
        public Guid ChapterID { get; set; }

        public Guid TagID { get; set; }

        public bool IsPrimary { get; set; }

        /*Navigation Properties*/
        public Chapter Chapter { get; set; }
        public Tag Tag { get; set; }

        public Chapter_Tag()
        {
            //Default constructor
        }

        public Chapter_Tag(Chapter Chapter, Tag Tag)
        {
            this.Chapter = Chapter;
            this.Tag = Tag;
        }
    }
}
