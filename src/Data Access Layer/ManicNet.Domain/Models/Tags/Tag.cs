﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public sealed class Tag : DescribableModel, IEquatable<Tag>, IComparable<Tag>
    {
        public Guid TagID { get; set; }

        public Guid TagTypeID { get; set; }

        public string Name { get; set; }

        public bool IsVirtual { get; set; }

        public string Icon { get; set; }
        public Guid? FontFamily { get; set; }

        public int Count { get; set; }
        public bool IsPrimary { get; set; }

        /*Navigation Properties*/
        public TagType TagType { get; set; }
        public List<TagAlias> Aliases { get; set; }
        public List<Tag_Tag> ImpliedBy { get; set; }
        public List<Tag_Tag> Implies { get; set; }
        public List<Work_Tag> Works { get; set; }
        public List<Volume_Tag> Volumes { get; set; }
        public List<Chapter_Tag> Chapters { get; set; }
        public List<User_Blocked_Tag> UserBlockedTags { get; set; }

        public Tag()
        {
            this.IsPrimary = true;

            ImpliedBy = new List<Tag_Tag>();
            Implies = new List<Tag_Tag>();
            Aliases = new List<TagAlias>();
            Works = new List<Work_Tag>();
            Volumes = new List<Volume_Tag>();
            Chapters = new List<Chapter_Tag>();
            UserBlockedTags = new List<User_Blocked_Tag>();
        }

        public Tag(Guid TagID, Guid TagTypeID, string Name, string ShortDescription, string LongDescription,
            string URL, bool IsVirtual, bool IsDeleted)
        {
            this.TagID = TagID;
            this.TagTypeID = TagTypeID;
            this.Name = Name;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsVirtual = IsVirtual;
            this.IsDeleted = IsDeleted;
            this.IsPrimary = true;

            ImpliedBy = new List<Tag_Tag>();
            Implies = new List<Tag_Tag>();
            Aliases = new List<TagAlias>();
            Works = new List<Work_Tag>();
            Volumes = new List<Volume_Tag>();
            Chapters = new List<Chapter_Tag>();
            UserBlockedTags = new List<User_Blocked_Tag>();
        }

        public Tag(Guid TagID, Guid TagTypeID, string Name, string ShortDescription, string LongDescription, string URL,
            bool IsVirtual, bool IsDeleted, List<Guid> ImpliedByList, List<Guid> ImpliesList, List<TagAlias> Aliases)
        {
            this.TagID = TagID;
            this.TagTypeID = TagTypeID;
            this.Name = Name;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsVirtual = IsVirtual;
            this.IsDeleted = IsDeleted;
            this.IsPrimary = true;

            this.ImpliedBy = new List<Tag_Tag>();
            this.Implies = new List<Tag_Tag>();
            this.Aliases = Aliases ?? new List<TagAlias>();

            if (ImpliedByList != null)
            {
                foreach (Guid impliedBy in ImpliedByList)
                {
                    Tag_Tag relationship = new Tag_Tag() { ImpliedByTagID = impliedBy, ImpliesTagID = TagID };
                    this.ImpliedBy.Add(relationship);
                }
            }

            if (ImpliesList != null)
            {
                foreach (Guid implies in ImpliesList)
                {
                    Tag_Tag relationship = new Tag_Tag() { ImpliedByTagID = TagID, ImpliesTagID = implies };
                    this.Implies.Add(relationship);
                }
            }

            Works = new List<Work_Tag>();
            Volumes = new List<Volume_Tag>();
            Chapters = new List<Chapter_Tag>();
            UserBlockedTags = new List<User_Blocked_Tag>();
        }

        public Tag(Guid TagID, TagType tagType, string Name, bool IsVirtual, bool IsDeleted,
            List<Guid> ImpliedByList, List<Guid> ImpliesList, List<TagAlias> Aliases)
        {
            this.TagID = TagID;
            this.TagType = tagType;
            this.TagTypeID = tagType.TagTypeID;
            this.Name = Name;
            this.IsVirtual = IsVirtual;
            this.IsDeleted = IsDeleted;
            this.IsPrimary = true;

            this.ImpliedBy = new List<Tag_Tag>();
            this.Implies = new List<Tag_Tag>();
            this.Aliases = Aliases ?? new List<TagAlias>();

            if (ImpliedByList != null)
            {
                foreach (Guid impliedBy in ImpliedByList)
                {
                    Tag_Tag relationship = new Tag_Tag() { ImpliedByTagID = impliedBy, ImpliesTagID = TagID };
                    this.ImpliedBy.Add(relationship);
                }
            }

            if (ImpliesList != null)
            {
                foreach (Guid implies in ImpliesList)
                {
                    Tag_Tag relationship = new Tag_Tag() { ImpliedByTagID = TagID, ImpliesTagID = implies };
                    this.Implies.Add(relationship);
                }
            }

            Works = new List<Work_Tag>();
            Volumes = new List<Volume_Tag>();
            Chapters = new List<Chapter_Tag>();
            UserBlockedTags = new List<User_Blocked_Tag>();
        }

        public void SetCount()
        {
            this.Count = this.Works.Count + this.Volumes.Count + this.Chapters.Count;

            /*this.Works.Clear();
            this.Volumes.Clear();
            this.Chapters.Clear();*/
        }

        public bool Equals(Tag t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.TagID == t.TagID) && (this.TagTypeID == t.TagTypeID)
                && (this.LongDescription == t.LongDescription) && (this.ShortDescription == t.ShortDescription)
                && (this.URL == t.URL);
        }

        public override bool Equals(object obj)
        {
            return obj is Tag tagObject && Equals(tagObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Tag other)
        {
            return string.Compare(this.TagID.ToString(), other.TagID.ToString());
        }
    }

    public sealed class TagSearchLists
    {
        public List<Tag> Included { get; set; }
        public List<Tag> Excluded { get; set; }

        public TagSearchLists()
        {
            this.Included = new List<Tag>();
            this.Excluded = new List<Tag>();
        }

        public TagSearchLists(List<Tag> Included, List<Tag> Excluded)
        {
            this.Included = Included ?? new List<Tag>();
            this.Excluded = Excluded ?? new List<Tag>();
        }
    }

    public sealed class RelatedTagList
    {
        public Tag Target { get; set; }
        public List<Tag> Related { get; set; }

        public RelatedTagList()
        {
            this.Related = new List<Tag>();
        }
    }
}
