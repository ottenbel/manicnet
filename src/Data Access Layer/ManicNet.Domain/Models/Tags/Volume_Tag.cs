﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Volume_Tag
    {
        public Guid VolumeID { get; set; }

        public Guid TagID { get; set; }

        public bool IsPrimary { get; set; }

        /*Navigation Properties*/
        public Volume Volume { get; set; }
        public Tag Tag { get; set; }

        public Volume_Tag()
        {
            //Default constructor
        }

        public Volume_Tag(Volume Volume, Tag Tag)
        {
            this.Volume = Volume;
            this.Tag = Tag;
        }
    }
}
