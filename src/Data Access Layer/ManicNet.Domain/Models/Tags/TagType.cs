﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace ManicNet.Domain.Models
{
    /// <summary>
    /// A class that is used to define the types of tags that are availiable to the site and how they can be used.
    /// </summary>
    [Serializable]
    public sealed class TagType : DescribableModel, IEquatable<TagType>, IComparable<TagType>
    {
        public Guid TagTypeID { get; set; }

        public string Name { get; set; }

        public string SearchablePrefix { get; set; }

        public int Priority { get; set; }

        public string PrimaryBodyColour { get; set; }

        public string PrimaryBorderColour { get; set; }

        public string PrimaryTextColour { get; set; }

        public string SecondaryBodyColour { get; set; }

        public string SecondaryBorderColour { get; set; }

        public string SecondaryTextColour { get; set; }

        public string Icon { get; set; }
        public Guid? FontFamily { get; set; }

        public bool AvailableOnWork { get; set; }

        public bool AvailableOnVolume { get; set; }

        public bool AvailableOnChapter { get; set; }

        public bool DisplayedInWorkSummary { get; set; }

        public bool DisplayedInVolumeSummary { get; set; }

        public bool DisplayedInChapterSummary { get; set; }

        public int? NumberDisplayedInWorkSummary { get; set; }

        public int? NumberDisplayedInVolumeSummary { get; set; }

        public int? NumberDisplayedInChapterSummary { get; set; }

        /*Navigation Properties*/
        public List<Tag> Tags { get; set; }

        public TagType()
        {
            this.Tags = new List<Tag>();
        }

        public TagType(Guid TagTypeID, string Name, string SearchablePrefix, int Priority, string shortDescription,
            string longDescription, string URL, bool AvailableOnWork, bool AvailableOnVolume, bool AvailableOnChapter,
            bool DisplayedInWorkSummary, bool DisplayedInVolumeSummary, bool DisplayedInChapterSummary,
            int? NumberDisplayedInWorkSummary, int NumberDisplayedInVolumeSummary, int NumberDisplayedInChapterSummary,
            bool IsDeleted)
        {
            this.TagTypeID = TagTypeID;
            this.Name = Name;
            this.SearchablePrefix = SearchablePrefix;
            this.Priority = Priority;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.AvailableOnWork = AvailableOnWork;
            this.AvailableOnVolume = AvailableOnVolume;
            this.AvailableOnChapter = AvailableOnChapter;
            this.DisplayedInWorkSummary = DisplayedInWorkSummary;
            this.DisplayedInVolumeSummary = DisplayedInVolumeSummary;
            this.DisplayedInChapterSummary = DisplayedInChapterSummary;
            this.NumberDisplayedInWorkSummary = NumberDisplayedInWorkSummary;
            this.NumberDisplayedInVolumeSummary = NumberDisplayedInVolumeSummary;
            this.NumberDisplayedInChapterSummary = NumberDisplayedInChapterSummary;
            this.IsDeleted = IsDeleted;

            this.Tags = new List<Tag>();
        }

        public TagType(Guid TagTypeID, string Name, string SearchablePrefix, int Priority, string shortDescription,
            string longDescription, string URL, bool AvailableOnWork, bool AvailableOnVolume, bool AvailableOnChapter,
            bool DisplayedInWorkSummary, bool DisplayedInVolumeSummary, bool DisplayedInChapterSummary,
            int? NumberDisplayedInWorkSummary, int NumberDisplayedInVolumeSummary, int NumberDisplayedInChapterSummary,
            bool IsDeleted, List<Tag> tags)
        {
            this.TagTypeID = TagTypeID;
            this.Name = Name;
            this.SearchablePrefix = SearchablePrefix;
            this.Priority = Priority;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.AvailableOnWork = AvailableOnWork;
            this.AvailableOnVolume = AvailableOnVolume;
            this.AvailableOnChapter = AvailableOnChapter;
            this.DisplayedInWorkSummary = DisplayedInWorkSummary;
            this.DisplayedInVolumeSummary = DisplayedInVolumeSummary;
            this.DisplayedInChapterSummary = DisplayedInChapterSummary;
            this.NumberDisplayedInWorkSummary = NumberDisplayedInWorkSummary;
            this.NumberDisplayedInVolumeSummary = NumberDisplayedInVolumeSummary;
            this.NumberDisplayedInChapterSummary = NumberDisplayedInChapterSummary;
            this.IsDeleted = IsDeleted;

            this.Tags = tags ?? new List<Tag>();
        }

        public bool Equals(TagType t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.TagTypeID == t.TagTypeID) && (this.Name == t.Name) && (this.SearchablePrefix == t.SearchablePrefix)
                && (this.Priority == t.Priority) && (this.ShortDescription == t.ShortDescription)
                && (this.LongDescription == t.LongDescription) && (this.URL == t.URL) && (this.AvailableOnWork == t.AvailableOnWork)
                && (this.AvailableOnVolume == t.AvailableOnVolume) && (this.AvailableOnChapter == t.AvailableOnChapter)
                && (this.DisplayedInWorkSummary == t.DisplayedInWorkSummary) && (this.DisplayedInVolumeSummary == t.DisplayedInVolumeSummary)
                && (this.DisplayedInChapterSummary == t.DisplayedInChapterSummary) && (this.NumberDisplayedInWorkSummary == t.NumberDisplayedInWorkSummary)
                && (this.NumberDisplayedInVolumeSummary == t.NumberDisplayedInVolumeSummary) && (this.NumberDisplayedInChapterSummary == t.NumberDisplayedInChapterSummary)
                && (this.IsDeleted == t.IsDeleted)
                /*&& (this.Tags.Equals(t.Tags))*/;
        }

        public override bool Equals(object obj)
        {
            return obj is TagType tagTypeObject && Equals(tagTypeObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] TagType other)
        {
            return string.Compare(this.TagTypeID.ToString(), other.TagTypeID.ToString());
        }
    }
}
