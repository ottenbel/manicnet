﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class User_Blocked_Tag
    {
        public Guid UserID { get; set; }
        public Guid TagID { get; set; }

        /*Navigation Properties*/
        public ManicNetUser User { get; set; }
        public Tag Tag { get; set; }

        public User_Blocked_Tag()
        {
            //Default constructor
        }

        public User_Blocked_Tag(Guid tagID, Guid userID)
        {
            this.TagID = tagID;
            this.UserID = userID;
        }
    }
}
