﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Tag_Tag
    {
        public Guid ImpliedByTagID { get; set; }

        public Guid ImpliesTagID { get; set; }

        /*Navigation Properties*/
        public Tag ImpliedByTag { get; set; }
        public Tag ImpliesTag { get; set; }

        public Tag_Tag()
        {
            //Default constructor
        }

        public Tag_Tag(Tag ImpliedByTag, Tag ImpliesTag)
        {
            this.ImpliedByTag = ImpliedByTag;
            this.ImpliesTag = ImpliesTag;
        }
    }
}
