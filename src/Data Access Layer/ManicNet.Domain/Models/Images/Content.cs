﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Content : Image, IEquatable<Content>, IComparable<Content>
    {
        public Guid ContentID { get; set; }

        /*Navigation Properties*/
        public Thumbnail Thumbnail { get; set; }
        public List<Work> Works { get; set; }
        public List<Volume> Volumes { get; set; }
        public List<Page> Pages { get; set; }

        public Content()
        {
            Works = new List<Work>();
            Volumes = new List<Volume>();
            Pages = new List<Page>();
        }

        public Content(string filename, byte[] hash)
        {
            this.Filename = filename;
            this.FileHash = hash;

            Works = new List<Work>();
            Volumes = new List<Volume>();
            Pages = new List<Page>();
        }

        public bool Equals(Content c)
        {
            // If parameter is null, return false.
            if (c is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, c))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != c.GetType())
            {
                return false;
            }

            return (this.Filename == c.Filename) && (this.FileHash.SequenceEqual(c.FileHash))
                && (this.ContentID == c.ContentID);
        }

        public override bool Equals(object obj)
        {
            return obj is Content contentObject && Equals(contentObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Content other)
        {
            return string.Compare(this.ContentID.ToString(), other.ContentID.ToString());
        }
    }
}
