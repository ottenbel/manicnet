﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Avatar : Image, IEquatable<Avatar>, IComparable<Avatar>
    {
        public Guid AvatarID { get; set; }

        /*Navigation Properties*/
        public List<ManicNetUser> UserAvatars { get; set; }

        public Avatar()
        {
            UserAvatars = new List<ManicNetUser>();
        }

        public Avatar(string filename, byte[] hash)
        {
            this.Filename = filename;
            this.FileHash = hash;

            UserAvatars = new List<ManicNetUser>();
        }

        public Avatar(Guid avatarID, string filename, byte[] hash)
        {
            this.AvatarID = avatarID;
            this.Filename = filename;
            this.FileHash = hash;

            UserAvatars = new List<ManicNetUser>();
        }

        public bool Equals(Avatar t)
        {
            // If parameter is null, return false.
            if (t is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, t))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != t.GetType())
            {
                return false;
            }

            return (this.AvatarID == t.AvatarID) && (this.FileHash.SequenceEqual(t.FileHash)) && (this.Filename == t.Filename);
        }

        public override bool Equals(object obj)
        {
            return obj is Avatar avatarObject && Equals(avatarObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] Avatar other)
        {
            return string.Compare(this.AvatarID.ToString(), other.AvatarID.ToString());
        }
    }
}
