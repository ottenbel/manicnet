﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public class Image
    {
        public string Filename { get; set; }

        public byte[] FileHash { get; set; }
    }
}
