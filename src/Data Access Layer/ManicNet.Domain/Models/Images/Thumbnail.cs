﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Thumbnail : Image
    {
        public Guid ThumbnailID { get; set; }

        public Guid ContentID { get; set; }

        /*Navigation Properties*/
        public Content Content { get; set; }

        public Thumbnail()
        {
            //Default constructor
        }

        public Thumbnail(string filename, byte[] hash, Content content)
        {
            this.Filename = filename;
            this.FileHash = hash;
            this.Content = content;
        }
    }
}
