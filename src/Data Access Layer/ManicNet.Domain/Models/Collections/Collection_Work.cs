﻿using System;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Collection_Work
    {
        public Guid CollectionID { get; set; }

        public Guid WorkID { get; set; }

        public int Number { get; set; }

        /*Navigation Properties*/
        public Collection Collection { get; set; }
        public Work Work { get; set; }

        public Collection_Work()
        {
            //Empty constructor
        }

        public Collection_Work(Collection collection, Work work, int number)
        {
            this.Collection = collection;
            this.Work = work;
            this.Number = number;
        }
    }
}
