﻿using System;
using System.Collections.Generic;

namespace ManicNet.Domain.Models
{
    [Serializable]
    public sealed class Collection : DescribableModel
    {
        public Guid CollectionID { get; set; }

        public string Title { get; set; }

        /*Navigation Properties*/
        public List<Collection_Work> Works { get; set; }

        public Collection()
        {
            Works = new List<Collection_Work>();
        }
    }
}
