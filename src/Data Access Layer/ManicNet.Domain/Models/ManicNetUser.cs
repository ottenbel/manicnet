﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace ManicNet.Domain.Models
{
    public sealed class ManicNetUser : IdentityUser<Guid>
    {
        public Guid? AvatarID { get; set; }
        public Guid RSSKey { get; set; }

        /*Navigation Properties*/
        public Avatar Avatar { get; set; }
        public List<User_Blocked_Tag> UserBlockedTags { get; set; }
    }
}
