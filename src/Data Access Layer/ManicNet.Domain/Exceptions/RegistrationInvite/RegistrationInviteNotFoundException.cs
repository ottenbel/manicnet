﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class RegistrationInviteNotFoundException : Exception
    {
        public RegistrationInviteNotFoundException()
        {
        }

        public RegistrationInviteNotFoundException(string message)
            : base(message)
        {
        }

        public RegistrationInviteNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
