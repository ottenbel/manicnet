﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagNotFoundException : Exception
    {
        public TagNotFoundException()
        {
        }

        public TagNotFoundException(string message)
            : base(message)
        {
        }

        public TagNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
