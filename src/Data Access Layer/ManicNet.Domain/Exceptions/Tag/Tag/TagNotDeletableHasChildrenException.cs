﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagNotDeletableHasChildrenException : Exception
    {
        public TagNotDeletableHasChildrenException()
        {
        }

        public TagNotDeletableHasChildrenException(string message)
            : base(message)
        {
        }

        public TagNotDeletableHasChildrenException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
