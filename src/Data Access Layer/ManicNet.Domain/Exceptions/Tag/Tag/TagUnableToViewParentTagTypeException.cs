﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagUnableToViewParentTagTypeException : Exception
    {
        public TagUnableToViewParentTagTypeException()
        {
        }

        public TagUnableToViewParentTagTypeException(string message)
            : base(message)
        {
        }

        public TagUnableToViewParentTagTypeException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
