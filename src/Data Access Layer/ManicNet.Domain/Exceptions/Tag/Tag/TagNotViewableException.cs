﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagNotViewableException : Exception
    {
        public TagNotViewableException()
        {
        }

        public TagNotViewableException(string message)
            : base(message)
        {
        }

        public TagNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
