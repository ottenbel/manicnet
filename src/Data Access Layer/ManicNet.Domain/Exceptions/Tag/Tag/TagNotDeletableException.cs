﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagNotDeletableException : Exception
    {
        public TagNotDeletableException()
        {
        }

        public TagNotDeletableException(string message)
            : base(message)
        {
        }

        public TagNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
