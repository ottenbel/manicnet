﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagTypeNotFoundException : Exception
    {
        public TagTypeNotFoundException()
        {
        }

        public TagTypeNotFoundException(string message)
            : base(message)
        {
        }

        public TagTypeNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
