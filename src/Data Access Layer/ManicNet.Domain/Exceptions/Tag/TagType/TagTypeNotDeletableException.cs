﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagTypeNotDeletableException : Exception
    {
        public TagTypeNotDeletableException()
        {
        }

        public TagTypeNotDeletableException(string message)
            : base(message)
        {
        }

        public TagTypeNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
