﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class TagTypeNotViewableException : Exception
    {
        public TagTypeNotViewableException()
        {
        }

        public TagTypeNotViewableException(string message)
            : base(message)
        {
        }

        public TagTypeNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
