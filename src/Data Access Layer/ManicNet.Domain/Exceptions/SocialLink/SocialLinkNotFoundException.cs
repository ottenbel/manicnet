﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public class SocialLinkNotFoundException : Exception
    {
        public SocialLinkNotFoundException()
        {
        }

        public SocialLinkNotFoundException(string message)
            : base(message)
        {
        }

        public SocialLinkNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
