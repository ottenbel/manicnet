﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public class SocialLinkNotDeletableException : Exception
    {
        public SocialLinkNotDeletableException()
        {
        }

        public SocialLinkNotDeletableException(string message)
            : base(message)
        {
        }

        public SocialLinkNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
