﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public class SocialLinkNotViewableException : Exception
    {
        public SocialLinkNotViewableException()
        {
        }

        public SocialLinkNotViewableException(string message)
            : base(message)
        {
        }

        public SocialLinkNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
