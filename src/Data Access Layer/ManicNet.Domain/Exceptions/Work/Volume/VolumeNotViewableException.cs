﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class VolumeNotViewableException : Exception
    {
        public VolumeNotViewableException()
        {
        }

        public VolumeNotViewableException(string message)
            : base(message)
        {
        }

        public VolumeNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
