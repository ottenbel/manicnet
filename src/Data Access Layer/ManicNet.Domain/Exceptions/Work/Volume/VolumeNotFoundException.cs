﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class VolumeNotFoundException : Exception
    {
        public VolumeNotFoundException()
        {
        }

        public VolumeNotFoundException(string message)
            : base(message)
        {
        }

        public VolumeNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
