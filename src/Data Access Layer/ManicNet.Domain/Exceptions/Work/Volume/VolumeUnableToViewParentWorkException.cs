﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class VolumeUnableToViewParentWorkException : Exception
    {
        public VolumeUnableToViewParentWorkException()
        {
        }

        public VolumeUnableToViewParentWorkException(string message)
            : base(message)
        {
        }

        public VolumeUnableToViewParentWorkException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
