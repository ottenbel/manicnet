﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class VolumeNotDeletableException : Exception
    {
        public VolumeNotDeletableException()
        {
        }

        public VolumeNotDeletableException(string message)
            : base(message)
        {
        }

        public VolumeNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
