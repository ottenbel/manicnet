﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class WorkNotFoundException : Exception
    {
        public WorkNotFoundException()
        {
        }

        public WorkNotFoundException(string message)
            : base(message)
        {
        }

        public WorkNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
