﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class WorkNotDeletableException : Exception
    {
        public WorkNotDeletableException()
        {
        }

        public WorkNotDeletableException(string message)
            : base(message)
        {
        }

        public WorkNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
