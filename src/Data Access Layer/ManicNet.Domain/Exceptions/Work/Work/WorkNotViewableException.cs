﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class WorkNotViewableException : Exception
    {
        public WorkNotViewableException()
        {
        }

        public WorkNotViewableException(string message)
            : base(message)
        {
        }

        public WorkNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
