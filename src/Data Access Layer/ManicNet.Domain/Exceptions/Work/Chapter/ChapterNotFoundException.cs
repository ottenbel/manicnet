﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ChapterNotFoundException : Exception
    {
        public ChapterNotFoundException()
        {
        }

        public ChapterNotFoundException(string message)
            : base(message)
        {
        }

        public ChapterNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
