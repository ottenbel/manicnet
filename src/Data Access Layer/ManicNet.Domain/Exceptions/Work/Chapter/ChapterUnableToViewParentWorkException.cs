﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ChapterUnableToViewParentWorkException : Exception
    {
        public ChapterUnableToViewParentWorkException()
        {
        }

        public ChapterUnableToViewParentWorkException(string message)
            : base(message)
        {
        }

        public ChapterUnableToViewParentWorkException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
