﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ChapterNotDeletableException : Exception
    {
        public ChapterNotDeletableException()
        {
        }

        public ChapterNotDeletableException(string message)
            : base(message)
        {
        }

        public ChapterNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
