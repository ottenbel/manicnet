﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ChapterNotViewableException : Exception
    {
        public ChapterNotViewableException()
        {
        }

        public ChapterNotViewableException(string message)
            : base(message)
        {
        }

        public ChapterNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
