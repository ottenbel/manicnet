﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class CollectionNotFoundException : Exception
    {
        public CollectionNotFoundException()
        {
        }

        public CollectionNotFoundException(string message)
            : base(message)
        {
        }

        public CollectionNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
