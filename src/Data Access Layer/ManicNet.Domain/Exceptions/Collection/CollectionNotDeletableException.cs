﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class CollectionNotDeletableException : Exception
    {
        public CollectionNotDeletableException()
        {
        }

        public CollectionNotDeletableException(string message)
            : base(message)
        {
        }

        public CollectionNotDeletableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
