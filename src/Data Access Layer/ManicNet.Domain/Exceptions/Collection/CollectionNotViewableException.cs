﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class CollectionNotViewableException : Exception
    {
        public CollectionNotViewableException()
        {
        }

        public CollectionNotViewableException(string message)
            : base(message)
        {
        }

        public CollectionNotViewableException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
