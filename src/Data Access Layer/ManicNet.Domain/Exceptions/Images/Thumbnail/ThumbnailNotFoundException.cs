﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ThumbnailNotFoundException : Exception
    {
        public ThumbnailNotFoundException()
        {
        }

        public ThumbnailNotFoundException(string message)
            : base(message)
        {
        }

        public ThumbnailNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
