﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class AvatarNotFoundException : Exception
    {
        public AvatarNotFoundException()
        {
        }

        public AvatarNotFoundException(string message)
            : base(message)
        {
        }

        public AvatarNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
