﻿using System;

namespace ManicNet.Domain.Exceptions
{
    public sealed class ContentNotFoundException : Exception
    {
        public ContentNotFoundException()
        {
        }

        public ContentNotFoundException(string message)
            : base(message)
        {
        }

        public ContentNotFoundException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
