﻿using ManicNet.Context;
using ManicNet.Contracts.UnitOfWork;
using System.Threading.Tasks;

namespace ManicNet.ManicNetUnitOfWork
{
    public sealed class ManicNetUnitOfWork : IManicNetUnitOfWork
    {
        private readonly ManicNetContext context;

        public ManicNetUnitOfWork(ManicNetContext context)
        {
            this.context = context;
        }

        public async Task SaveAsync()
        {
            await context.SaveChangesAsync();
        }

        public ManicNetContext GetContext()
        {
            return context;
        }
    }
}
