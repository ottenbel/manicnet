﻿using ManicNet.Domain.Models;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the registration repository.
    /// </summary>
    public interface IRegistrationInviteRepository : IRepository<RegistrationInvite>
    {
        /// <summary>
        /// Retrieve a specified registration from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="email">The email address to find the registration for.</param>
        /// <returns>The registration invite corresponding to the provided id.</returns>
        Task<RegistrationInvite> FindAsync(string email);
    }
}
