﻿using ManicNet.Domain.Models;
using System;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the collection repository.
    /// </summary>
    public interface ICollectionRepository : IRepository<Collection>
    {
        /// <summary>
        /// Update a collection with a work
        /// </summary>
        /// <param name="collection">The collection ID of the work to be updated.</param>
        /// <param name="workID">The work to be added to the collection.</param>
        /// <param name="number">The number that the work should be aded to the collection as.</param>
        void UpdateWithWork(Collection collection, Guid workID, int number);
    }
}
