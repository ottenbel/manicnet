﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the tag type repository.
    /// </summary>
    public interface ITagTypeRepository : IRepository<TagType>
    {
        /// <summary>
        /// Retrieve a list of all tag types.
        /// </summary>
        /// <returns>A list of all tags types in the database.</returns>
        List<TagType> All();

        /// <summary>
        /// Find a tag type by identifier.
        /// </summary>
        /// <param name="id">The identifier of the tag type to be retrieved.</param>
        /// <param name="bypassCache">If the call should hit the databse directly or attempt to hit the cache first. Only set to true when editing.</param>
        /// <returns>The tag type corresponding to the provided id.</returns>
        TagType Find(Guid id, bool bypassCache = false);

        /// <summary>
        /// Find a tag type (including the relationship through tags to work associations) by identifier.
        /// </summary>
        /// <param name="tagTypeID">The identifier of the tag type to be retrieved.</param>
        /// <returns>The tag type corresponding to the provided id.</returns>
        TagType FindWithWorkAssociation(Guid id);

        /// <summary>
        /// Find a tag type (including the relationship through tags to work associations) by identifier.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tagTypeID">The identifier of the tag type to be retrieved.</param>
        /// <returns>The tag type corresponding to the provided id.</returns>
        Task<TagType> FindWithWorkAssociationAsync(Guid tagTypeID);
    }
}
