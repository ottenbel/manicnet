﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all methods for the simple tag alias repository.
    /// </summary>
    public interface ITagAliasRepository
    {
        /// <summary>
        /// Retrieve a list of all tag aliases.
        /// </summary>
        /// <returns>A list of all tag aliases in the database.</returns>
        List<TagAlias> All();

        /// <summary>
        /// Retrieve a list of all tag aliases.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all tag aliases in the database.</returns>
        Task<List<TagAlias>> AllAsync();

        /// <summary>
        /// Retrieve a list of tag aliases from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="names">The list of names of the aliases to be retrieved.</param>
        /// <returns>A list of all tag aliases in the database corresponded to the provided names.</returns>
        Task<List<TagAlias>> FindAsync(IEnumerable<string> names);
    }
}
