﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the tag repository.
    /// </summary>
    public interface ITagRepository : IRepository<Tag>
    {
        /// <summary>
        /// Retrieve a list of all tags.
        /// </summary>
        /// <returns>A list of all tags in the database.</returns>
        List<Tag> All();

        /// <summary>
        /// Find a tag by the given tag name.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>The tag corresponding to the provided name.</returns>
        Task<Tag> FindByNameAsync(string name);

        /// <summary>
        /// Create a tag in the database.
        /// </summary>
        /// <param name="tag">The tag to be created.</param>
        /// <param name="impliedTags">A list of tags that are implied by the tag to be created.</param>
        void Insert(Tag tag, IEnumerable<Tag> impliedTags);

        /// <summary>
        /// Update an existing tag in the database.
        /// </summary>
        /// <param name="tag">The tag to be updated.</param>
        /// <param name="impliedTags">A list of tags that are implied by the tag to be created</param>
        /// <returns>A list of identifiers corresponding to tags related to the tag being updated.</returns>
        List<Guid> Update(Tag tag, IEnumerable<Tag> impliedTags);
    }
}
