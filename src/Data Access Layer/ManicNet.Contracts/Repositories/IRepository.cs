﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Base repository interface that all other repository interfaces inherit from.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Retrieve all objects of the provided type from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all entities in the database.</returns>
        Task<List<TEntity>> AllAsync();

        /// <summary>
        /// Delete an existing entity from the database.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Find the object of a provided type matching the provided ID.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier for the object.</param>
        /// <param name="bypassCache">Whether or not the cache should be bypassed or not. Only used when finding to edit.</param>
        /// <returns>The entity corresponding to the provided id.</returns>
        Task<TEntity> FindAsync(Guid id, bool bypassCache = false);

        /// <summary>
        /// Create an entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be created in the database.</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Update an existing entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        void Update(TEntity entity);
    }
}
