﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    public interface ICacheConfigurationRepository
    {
        List<CacheConfiguration> All();
        Task<List<CacheConfiguration>> AllAsync();
        void BulkInsert(List<CacheConfiguration> cacheConfigurations);
        void BulkUpdate(List<CacheConfiguration> cacheConfiguration);
    }
}
