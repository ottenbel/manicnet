﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    public interface ISocialLinkRepository : IRepository<SocialLink>
    {
    }
}
