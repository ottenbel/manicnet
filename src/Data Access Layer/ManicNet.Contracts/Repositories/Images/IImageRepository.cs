﻿using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Base repository interface that all other image repository interfaces inherit from.
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IImageRepository<TEntity>
    {
        /// <summary>
        /// Delete an existing entity from the database.
        /// </summary>
        /// <param name="entity">The entity to be deleted.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Retrieve a specified entity from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the entity in question.</param>
        /// <returns>The image corresponding to the provided id.</returns>
        Task<TEntity> FindAsync(Guid id);

        /// <summary>
        /// Retrieve a specified entity from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The hash code corresponding to the entity image.</param>
        /// <returns>The image corresponding to the provided hash.</returns>
        Task<TEntity> FindAsync(byte[] hash);

        /// <summary>
        /// Create a new entity in the database.
        /// </summary>
        /// <param name="entity">The entity to be created.</param>
        void Insert(TEntity entity);
    }
}
