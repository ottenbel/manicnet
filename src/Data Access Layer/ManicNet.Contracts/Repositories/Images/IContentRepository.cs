﻿using ManicNet.Domain.Models;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the content repository.
    /// </summary>
    public interface IContentRepository : IImageRepository<Content>
    {

    }
}
