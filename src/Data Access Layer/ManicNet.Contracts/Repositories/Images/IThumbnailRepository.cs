﻿using ManicNet.Domain.Models;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the thumbnail repository.
    /// </summary>
    public interface IThumbnailRepository : IImageRepository<Thumbnail>
    {

    }
}
