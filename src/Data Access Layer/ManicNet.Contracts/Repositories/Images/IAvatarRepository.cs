﻿using ManicNet.Domain.Models;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the avatar repository.
    /// </summary>
    public interface IAvatarRepository : IImageRepository<Avatar>
    {

    }
}
