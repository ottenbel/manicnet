﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the volume repository.
    /// </summary>
    public interface IVolumeRepository : IRepository<Volume>
    {
        /// <summary>
        /// Retrieve the volume with associated information necessary for deletion.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the volume to be retreived.</param>
        /// <param name="noTracking">Whether or not the query should be set as no tracking.</param>
        /// <returns>The volume corresponding to the provided id.</returns>
        Task<Volume> FindForDeletion(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieve the volume with associated information necessary for downloading.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the volume to be retreived.</param>
        /// <returns>The volume corresponding to the provided id.</returns>
        Task<Volume> FindForDownload(Guid id);

        /// <summary>
        /// Create a volume in the database.
        /// </summary>
        /// <param name="volume">The volume to be created.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the volume.</param>
        /// <param name="secondaryTags">A list of (secondary) tags that are included on the volume.</param>
        void Insert(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags);

        /// <summary>
        /// Update an existing volume in the database.
        /// </summary>
        /// <param name="volume">The volume to be updated.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the volume.</param>
        /// <param name="primaryTags">A list of (secondary) tags that are included on the volume.</param>
        /// <returns>A list of identifiers corresponding to tags associated with the volume.</returns>
        void Update(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags);
    }
}
