﻿using ManicNet.Domain.Models;
using System.Collections.Generic;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the chapter repository.
    /// </summary>
    public interface IChapterRepository : IRepository<Chapter>
    {
        /// <summary>
        /// Create a chapter in the database.
        /// </summary>
        /// <param name="chapter">The chapter to be created.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the chapter.</param>
        /// <param name="secondaryTags">A list of (secondary) tags that are included on the chapter.</param>
        /// <param name="contents">The contents that will be added as pages to the chapter.</param>
        void Insert(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IEnumerable<Content> contents);

        /// <summary>
        /// Update an existing chapter in the database.
        /// </summary>
        /// <param name="chapter">The chapter to be updated.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the chapter.</param>
        /// <param name="secondaryTags">A list of (secondary) tags that are included on the chapter.</param>
        /// <param name="contents">The contents that will be added as pages to the chapter.</param>
        /// <returns>A list of identifiers corresponding to tags associated with the chapter.</returns>
        void Update(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IEnumerable<Content> contents);
    }
}
