﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface to define all base and custom methods for the work repository.
    /// </summary>
    public interface IWorkRepository : IRepository<Work>
    {
        /// <summary>
        /// Find a work by identifier.
        /// </summary>
        /// <param name="id">The identifier of the work to be retrieved.</param>
        /// <param name="bypassCache">If the call should hit the databse directly or attempt to hit the cache first. Only set to true when editing.</param>
        /// <returns>The work corresponding to the provided id.</returns>
        Work Find(Guid id, bool bypassCache = false);

        /// <summary>
        /// Retrieve the work with associated information necessary for deletion.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the work to be retreived.</param>
        /// <param name="noTracking">Whether or not the query should be set as no tracking.</param>
        /// <returns>The work corresponding to the provided id.</returns>
        Task<Work> FindForDeletionAsync(Guid id, bool noTracking = false);

        /// <summary>
        /// Retrieve the work with associated information necessary for downloading.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the work to be retreived.</param>
        /// <returns>The work corresponding to the provided id.</returns>
        Task<Work> FindForDownloadAsync(Guid id);

        /// <summary>
        /// Create a work in the database.
        /// </summary>
        /// <param name="work">The work to be created.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the work.</param>
        /// <param name="primaryTags">A list of (secondary) tags that are included on the work.</param>
        void Insert(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags);

        /// <summary>
        /// Create an existing work in the database.
        /// </summary>
        /// <param name="work">The work to be updated.</param>
        /// <param name="primaryTags">A list of (primary) tags that are included on the work.</param>
        /// <param name="secondaryTags">A list of (secondary) tags that are included on the work.</param>
        void Update(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags);
    }
}
