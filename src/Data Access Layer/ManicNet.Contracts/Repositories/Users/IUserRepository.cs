﻿using System;
using System.Security.Claims;

namespace ManicNet.Contracts.Repositories
{
    /// <summary>
    /// Interface for methods to retrieve generic user information.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Configure the values in the user repository correctly.
        /// </summary>
        /// <param name="User">The current user of the session.</param>
        void Configure(ClaimsPrincipal User);

        /// <summary>
        /// Basic functionality to retrieve username of the signed in user from wherever so the
        /// user object doesn't have to be passed around to use the name for logging purposes
        /// </summary>
        /// <returns>A string identifying the current user.</returns>
        string GetHumanReadableUserIdentifier();

        /// <summary>
        /// Basic functionality to retrive the identifier of the signed in user.
        /// </summary>
        /// <returns>The ID for the current user.</returns>
        Guid? GetUserID();
    }
}
