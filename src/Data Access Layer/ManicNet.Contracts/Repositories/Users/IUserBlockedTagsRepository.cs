﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Repositories
{
    public interface IUserBlockedTagsRepository
    {
        /// <summary>
        /// Return a list of all blocked tags.
        /// </summary>
        /// <returns></returns>
        Task<List<User_Blocked_Tag>> All();

        /// <summary>
        /// Find a list of tags blocked by a given user.
        /// </summary>
        /// <param name="userID">The identifier of the user that the list of blocked tags should be retrieved for.</param>
        /// <returns></returns>
        Task<List<User_Blocked_Tag>> Find(Guid userID);

        /// <summary>
        /// Retrieve the blocked tag for a given user.
        /// </summary>
        /// <param name="tagID">The identifier of the blocked tag.</param>
        /// <param name="userID">The identifier of the user that the blocked tag should be retrieved for.</param>
        /// <returns></returns>
        Task<User_Blocked_Tag> Find(Guid tagID, Guid userID);

        /// <summary>
        /// Create a blocked tag in the database.
        /// </summary>
        /// <param name="userBlockedTag">The blocked tag to be retrieved.</param>
        void Insert(User_Blocked_Tag userBlockedTag);

        /// <summary>
        /// Remove a blocked tag from the datbaase.
        /// </summary>
        /// <param name="userBlockedTag">The blocked tag to be removed.</param>
        void Remove(User_Blocked_Tag userBlockedTag);
    }
}
