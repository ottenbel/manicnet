﻿using ManicNet.Context;
using System.Threading.Tasks;

namespace ManicNet.Contracts.UnitOfWork
{
    public interface IManicNetUnitOfWork
    {
        /// <summary>
        /// Get the context used to connect to the database.
        /// </summary>
        /// <returns>The database context.</returns>
        ManicNetContext GetContext();

        /// <summary>
        /// Save changes to the database.
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();
    }
}
