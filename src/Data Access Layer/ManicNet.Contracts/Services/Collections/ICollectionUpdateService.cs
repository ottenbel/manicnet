﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying collections.
    /// </summary>
    public interface ICollectionUpdateService
    {
        /// <summary>
        /// Create a new collection in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="collection">The collection to be created.</param>
        /// <returns></returns>
        Task CreateCollection(Collection collection);

        /// <summary>
        /// Delete an existing collection from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="collection">The collection to be updated.</param>
        /// <exception cref="CollectionNotDeletableException">Thrown when the collection in question is not in a soft deleted state.</exception>
        /// <returns></returns>
        Task RemoveCollection(Collection collection);

        /// <summary>
        /// Update a collection in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="collection">The collection to be updated.</param>
        /// <param name="collectionCacheCleanup"></param>
        /// <returns></returns>
        Task UpdateCollection(Collection collection, CollectionCacheCleanup collectionCacheCleanup);

        /// <summary>
        /// Update a collection with a work in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="collection">The collection to be updated.</param>
        /// <param name="workID">The work identifier to be associated with the collection.</param>
        /// <param name="number">The number of the work to be associated with the collection.</param>
        /// <returns></returns>
        Task UpdateCollectionWithNewWork(Collection collection, Guid workID, int number);
    }
}
