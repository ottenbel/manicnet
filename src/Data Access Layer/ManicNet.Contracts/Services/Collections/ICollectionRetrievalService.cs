﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding collections.
    /// </summary>
    public interface ICollectionRetrievalService
    {
        /// <summary>
        /// Returns a boolean signifying if there are any deleted collections in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A boolean value denoting whether or not there are any soft deleted collections in the database.</returns>
        Task<bool> DoDeletedCollectionsExist();

        /// <summary>
        /// Retrieve a list of all deleted collections.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all soft deleted collections in the database.</returns>
        Task<List<Collection>> GetDeletedCollections();

        /// <summary>
        /// Retrieve a specified collection from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the collection to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the collection.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a collection to be modified).</param>
        /// <exception cref="CollectionNotFoundException">Thrown when the identifier does not correspond to a collection in the database.</exception>
        /// <exception cref="CollectionNotViewableException">Thrown when the user making the call does not have the correct permissions to view the collection in question.</exception>
        /// <returns>The collection corresponding to the provided id.</returns>
        Task<Collection> FindCollection(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a list of all non-deleted collections.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active collections in the database.</returns>
        Task<List<Collection>> GetActiveCollections();
    }
}
