﻿using ManicNet.Models;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for service used to retrieve page metadata.
    /// </summary>
    public interface IMetadataService
    {
        /// <summary>
        /// Retrieve metadata about a specific model.
        /// </summary>
        /// <param name="model">The model that metadata should be set for.</param>
        /// <returns>A metadata object reflecting the object defining the page.</returns>
        public MetadataViewModel GetMetadata<T>(T model);
        /// <summary>
        /// Retrieve metadata for a specific page.
        /// </summary>
        /// <returns>A generic metadata object.</returns>
        public MetadataViewModel GetMetadata();
    }
}
