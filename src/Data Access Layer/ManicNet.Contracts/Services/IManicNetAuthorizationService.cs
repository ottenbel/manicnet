﻿using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for service wrapper handing authorization.
    /// </summary>
    public interface IManicNetAuthorizationService
    {
        /// <summary>
        /// Authorize user access to content.
        /// </summary>
        /// <param name="user">The user attempting to access the content.</param>
        /// <param name="policyName">The permission used to protect the content.</param>
        /// <returns>An authorization result containg whether or not the user matches the provided policy name.</returns>
        Task<AuthorizationResult> Authorize(ClaimsPrincipal user, string policyName);
    }
}
