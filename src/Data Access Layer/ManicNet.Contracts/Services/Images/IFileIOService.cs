﻿using ManicNet.Domain.Models;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service interacting with immage files.
    /// </summary>
    public interface IFileIOService
    {
        /// <summary>
        /// Deletes an avatar from file storage.
        /// </summary>
        /// <param name="inputName">The file name to be deleted.</param>
        Task DeleteAvatar(string inputName);

        /// <summary>
        /// Deletes a content image from file storage.
        /// </summary>
        /// <param name="inputName">The file name to be deleted.</param>
        Task DeleteContent(string inputName);

        /// <summary>
        /// Deletes a thumbnail from file storage.
        /// </summary>
        /// <param name="inputName">The file name to be deleted.</param>
        Task DeleteTumbnail(string inputName);

        /// <summary>
        /// Retrieves a byte array corresponding to the contents of a chapter.
        /// </summary>
        /// <param name="work">A string containing the name of the top level work.</param>
        /// <param name="chapter">The chapter that the zip file will be created for.</param>
        /// <returns>The data of a zip file containing the image contents of the chapter object.</returns>
        Task<byte[]> GetDownloadChapter(string work, Chapter chapter);

        /// <summary>
        /// Retrieves a byte array corresponding to the contents of a volume.
        /// </summary>
        /// <param name="work">A string containing the name of the top level work.</param>
        /// <param name="volume">The volume that the zip file will be created for.</param>
        /// <returns>The data of a zip file containing the image contents of the volume object.</returns>
        Task<byte[]> GetDownloadVolume(string work, Volume volume);

        /// <summary>
        /// Retrieves a byte array corresponding to the contents of a work.
        /// </summary>
        /// <param name="work">The work that the zip file will be created for.</param>
        /// <returns>The data of a zip file containing the image contents of the work object.</returns>
        Task<byte[]> GetDownloadWork(Work work);

        /// <summary>
        /// Retrieves the URL address for an avatar.
        /// </summary>
        /// <param name="avatar">The avatar to retrieve the URL for.</param>
        /// <returns>The accessible URL corresponding to the provided avatar.</returns>
        string GetLink(Avatar avatar);

        /// <summary>
        /// Retrieves the URL address for a content.
        /// </summary>
        /// <param name="content">The content to retrieve the URL address for.</param>
        /// <returns>The accessible URL corresponding to the provided content.</returns>
        string GetLink(Content content);

        /// <summary>
        /// Retrieves the URL address for a thumbnail
        /// </summary>
        /// <param name="thumbnail">The thumbnail to retrieve the URL for.</param>
        /// <returns>The accessible URL corresponding to the provided thumbnail.</returns>
        string GetLink(Thumbnail thumbnail);

        /// <summary>
        /// Retrieves the byte array of an avatar.
        /// </summary>
        /// <param name="avatar">The avatar to retrieve the byte array for.</param>
        /// <returns>The byte array corresponding to the provided avatar.</returns>
        Task<byte[]> GetFile(Avatar avatar);

        /// <summary>
        /// Retrieves the byte array of content.
        /// </summary>
        /// <param name="content">The content to retrieve the byte array for.</param>
        /// <returns>The byte array corresponding to the provided content.</returns>
        Task<byte[]> GetFile(Content content);

        /// <summary>
        /// Retrieves byte array for a thumbnail
        /// </summary>
        /// <param name="thumbnail">The thumbnail to retrieve the URL for.</param>
        /// <returns>The accessible URL corresponding to the provided thumbnail.</returns>
        Task<byte[]> GetFile(Thumbnail thumbnail);

        /// <summary>
        /// Upload an avatar to the relevant storage location.
        /// </summary>
        /// <param name="file">The file to be uploaded.</param>
        /// <param name="inputName">The name of the file.</param>
        /// <returns>A string containing the safed file name.</returns>
        Task<string> UploadAvatarAsync(byte[] file, string inputName);

        /// <summary>
        /// Upload a full sized content image to the relevant storage location
        /// </summary>
        /// <param name="file">The file to be uploaded.</param>
        /// <param name="inputName">The name of the file.</param>
        /// <returns>A string containing the safed file name.</returns>
        Task<string> UploadContentAsync(byte[] file, string inputName);

        /// <summary>
        /// Upload a thumbnail to the relevant storage location.
        /// </summary>
        /// <param name="file">The file to be uploaded.</param>
        /// <param name="inputName">The name of the file.</param>
        /// <returns>A string containing the safed file name.</returns>
        Task<string> UploadThumbnailAsync(byte[] file, string inputName);
    }
}
