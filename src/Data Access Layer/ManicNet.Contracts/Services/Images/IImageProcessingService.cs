﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using static ManicNet.Domain.Enums.FileValidation;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service used when processing images (image validation/compression/etc.).
    /// </summary>
    public interface IImageProcessingService
    {
        /// <summary>
        /// Calculate the hash of an image to determine if a copy is already stored by the system.
        /// </summary>
        /// <param name="image">The image to calculate the hash for.</param>
        /// <returns>A byte array containing the hash value of the provided image.</returns>
        Task<byte[]> GetHash(IFormFile image);

        /// <summary>
        /// Convert the uploaded image file into byte array.
        /// </summary>
        /// <param name="image">The file to be processed.</param>
        /// <returns>A byte array containing the contents of the uploaded image.</returns>
        Task<byte[]> GetImageBytes(IFormFile image);

        /// <summary>
        /// Process an image for storage to the system as an avatar.
        /// </summary>
        /// <param name="image">The image to be processed.</param>
        /// <returns>A byte array containing the processed contents of the uploaded avatar image.</returns>
        Task<byte[]> ProcessAvatar(IFormFile image);

        /// <summary>
        /// Process an image for storage to the system as regular content.
        /// </summary>
        /// <param name="image">The image to be processed.</param>
        /// <returns>A byte array containing the processed contents of the uploaded image.</returns>
        byte[] ProcessContent(byte[] image);

        /// <summary>
        /// Process an image for storage to the system as an thumbnail.
        /// </summary>
        /// <param name="image">The image to be processed.</param>
        /// <returns>A byte array containing the processed contents of the uploaded image as an avatar.</returns>
        byte[] ProcessThumbnail(byte[] image);

        /// <summary>
        /// Validate that an uploaded file is a valid image.
        /// </summary>
        /// <param name="file">The file to be validated.</param>
        /// <returns>The validation result of the image upload.</returns>
        FileValidationResult ValidateImage(IFormFile file);

        /// <summary>
        /// Validate that an uploaded file is a valid image.
        /// </summary>
        /// <param name="file">The file to be validated.</param>
        /// <returns>The validation result of the image upload.</returns>
        Task<FileValidationResult> ValidateImageAsync(IFormFile file);
    }
}
