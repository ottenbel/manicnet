﻿using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling content objects.
    /// </summary>
    public interface IContentService
    {
        /// <summary>
        /// Create a new content entity in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="content">The content to be inserted in the database.</param>
        /// <returns></returns>
        Task CreateContent(Content content, bool callAsTopLevel = true);

        /// <summary>
        /// Retrieve a specified content entry from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The ID of the avatar to be retrieved</param>
        /// /// <exception cref="ContentNotFoundException">Thrown when the identifier in question does not correspond to a content entity in the database.</exception>
        /// <returns>The content corresponding to the provided id.</returns>
        Task<Content> FindContent(Guid id);

        /// <summary>
        /// Retrieve a specified avatar from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="hash">The hash of the content to be retrieved.</param>
        /// <returns>The content corresponding to the provided hash value.</returns>
        Task<Content> FindContent(byte[] hash);

        /// <summary>
        /// Delete an existing content entry from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="content">The content to be deleted.</param>
        /// <returns></returns>
        Task RemoveContent(Content content, bool callAsTopLevel = true);
    }
}
