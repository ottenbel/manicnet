﻿using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling thumbnail objects.
    /// </summary>
    public interface IThumbnailService
    {
        /// <summary>
        /// Create a new thumbnail in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="thumbnail">The thumbnail to be inserted in the database.</param>
        /// <returns></returns>
        Task CreateThumbnail(Thumbnail thumbnail, bool callAsTopLevel = true);
    }
}
