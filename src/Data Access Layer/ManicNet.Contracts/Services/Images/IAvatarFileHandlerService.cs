﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service controlling how file bahaviour for avatar objects.
    /// </summary>
    public interface IAvatarFileHandlerService
    {
        /// <summary>
        /// Processes the cleanup of an avatar. Only deletes the avatar if it is referenced
        /// by one entry in the database.
        /// </summary>
        /// <param name="avatarID">The identifier of the avatar to be cleaned up.</param>
        /// <returns></returns>
        Task CleanupAvatar(Guid? avatarID);

        /// <summary>
        /// Upload an avatar to the system.
        /// </summary>
        /// <param name="image">The file to be uploaded as an avatar.</param>
        /// <param name="hash">The hash of the file being uploaded.</param>
        /// <returns>The guid corresponding to the avatar that was created using the uploaded image.</returns>
        Task<Guid> UploadAvatar(IFormFile image, byte[] hash);
    }
}
