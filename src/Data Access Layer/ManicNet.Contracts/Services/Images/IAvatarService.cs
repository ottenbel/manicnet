﻿using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling avatar objects.
    /// </summary>
    public interface IAvatarService
    {
        /// <summary>
        /// Create a new avatar in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="avatar">The avatar to be inserted in the database.</param>
        /// <returns></returns>
        Task CreateAvatar(Avatar avatar);

        /// <summary>
        /// Retrieve a specified avatar from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The ID of the avatar to be retrieved</param>
        /// <exception cref="AvatarNotFoundException">Thrown when the identifier in question does not correspond to a avatar in the database.</exception>
        /// <returns>The avatar corresponding to the provided id.</returns>
        Task<Avatar> FindAvatar(Guid id);

        /// <summary>
        /// Retrieve a specified avatar from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="hash">The hash of the avatar to be retrieved.</param>
        /// <returns>The avatar object corresponding to the provided hash value.</returns>
        Task<Avatar> FindAvatar(byte[] hash);

        /// <summary>
        /// Delete an existing avatar from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="avatar">The avatar to be deleted.</param>
        /// <returns></returns>
        Task RemoveAvatar(Avatar avatar);
    }
}
