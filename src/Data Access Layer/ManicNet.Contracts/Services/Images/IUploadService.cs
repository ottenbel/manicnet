﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the uploading content.
    /// </summary>
    public interface IUploadService
    {
        /// <summary>
        /// Returns the content corresponding with an uploaded file either by retrieving an already existing content entry 
        /// matching the file hash or by uploading the file and creating a corresponding content entry.
        /// </summary>
        /// <param name="contentFile">The file to be uploaded.</param>
        /// <returns>The content corresponding to the provided file.</returns>
        Task<Content> UploadFileOrRetrieveFromContent(IFormFile contentFile);
    }
}
