﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service controlling how file bahaviour for content objects.
    /// </summary>
    public interface IContentFileHandlerService
    {
        /// <summary>
        /// Processes the cleanup of an content entry. Only deletes the content entry 
        /// if it is referenced by one entry in the database.
        /// </summary>
        /// <param name="contentID">The identifier of the content entry to be cleaned up.</param>
        /// <returns></returns>
        Task CleanupContent(Guid? contentID);

        /// <summary>
        /// Processes the cleanup of a list of content entries. Only deletes the content entries 
        /// if they are referenced by one entry in the database.
        /// </summary>
        /// <param name="contents">The identifiers of the content entries to be cleaned up.</param>
        /// <returns></returns>
        Task CleanupContent(IEnumerable<Guid> contents);

        /// <summary>
        /// Upload a content entry (and associated thumbnail) to the system.
        /// </summary>
        /// <param name="image">The file to be uploaded as a content entry.</param>
        /// <param name="hash">The hash of the file being uploaded.</param>
        /// <returns>The content object that was created using the uploaded image.</returns>
        Task<Content> UploadContent(IFormFile image, byte[] hash);
    }
}
