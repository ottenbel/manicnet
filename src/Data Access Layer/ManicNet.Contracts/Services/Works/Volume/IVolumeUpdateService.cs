﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying volumes.
    /// </summary>
    public interface IVolumeUpdateService
    {
        /// <summary>
        /// Create a new volume in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="volume">The volume to be created.</param>
        /// <param name="primaryTags">The list of (primary) tags to attached to the newly created volume.</param>
        /// <param name="secondaryTags">The list of (secondary) tags to attached to the newly created volume.</param>
        /// <param name="coverFile"></param>
        /// <param name="cacheCleanup"></param>
        /// <returns>An error string for problems when updating the volume.</returns>
        Task<string> CreateVolume(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Migrate a chapter to associated with a work to a new work.
        /// </summary>
        /// <param name="volume">The volume to be migrated.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns></returns>
        Task MigrateVolume(Volume volume, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Delete an existing volume from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="volume">The volume to be deleted</param>
        /// <param name="calledAsTopLevel">Whether the volume deletion was triggered by a higher level call or directly. 
        /// Controls whether or not the deletion actions are committed by this function or passed back to be saved by the caller.</param>
        /// <exception cref="VolumeNotDeletableException">Thrown when the volume in question is not in a soft deleted state.</exception>
        /// <returns></returns>
        Task<CleanupWork> RemoveVolume(Volume volume, WorkCacheCleanup cacheCleanup, bool calledAsTopLevel = true);

        /// <summary>
        /// Update an existing volume in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="volume">The volume to be updated.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns></returns>
        Task UpdateVolume(Volume volume, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Update an existing volume in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="volume">The volume to be updated.</param>
        /// <param name="primaryTags">The list of (primary) tags to attached to the volume.</param>
        /// <param name="secondaryTags">The list of (secondary) tags to attached to the volume.</param>
        /// <param name="coverFile"></param>
        /// <param name="deleteCover"></param>
        /// <param name="cacheCleanup"></param>
        /// <returns>An error string for problems when updating the volume.</returns>
        Task<string> UpdateVolume(Volume volume, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, bool deleteCover, WorkCacheCleanup cacheCleanup);
    }
}
