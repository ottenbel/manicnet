﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding volumes.
    /// </summary>
    public interface IVolumeRetrievalService
    {
        /// <summary>
        /// Retrieve a specified volume from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the volume to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the volume.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a volume to be modified).</param>
        /// <exception cref="VolumeNotFoundException">Thrown when the identifier does not correspond to a volume in the database.</exception>
        /// <exception cref="VolumeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the volume in question.</exception>
        /// <exception cref="VolumeUnableToViewParentWorkException">Thrown when the parent work is in a soft deleted state and the user making the call doesn't have the correct permissions to view the parent work in question.</exception>
        /// <returns>The volume corresponding to the provided id.</returns>
        Task<Volume> FindVolume(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified volume from the database with additional information for handing deletion.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the volume to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the volume.</param>
        /// <param name="noTracking">Whether or not to to track the retrieval function.</param>
        /// <exception cref="VolumeNotFoundException">Thrown when the identifier does not correspond to a volume in the database.</exception>
        /// <exception cref="VolumeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the volume in question.</exception>
        /// <exception cref="VolumeUnableToViewParentWorkException">Thrown when the parent work is in a soft deleted state and the user making the call doesn't have the correct permissions to view the parent work in question.</exception>
        /// <returns></returns>
        Task<Volume> FindVolumeForDeletion(Guid id, ClaimsPrincipal user, bool noTracking = false);

        /// <summary>
        /// Retrieve a specified volume from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the volume to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the volume.</param>
        /// <exception cref="VolumeNotFoundException">Thrown when the identifier does not correspond to a volume in the database.</exception>
        /// <exception cref="VolumeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the volume in question.</exception>
        /// <exception cref="VolumeUnableToViewParentWorkException">Thrown when the parent work is in a soft deleted state and the user making the call doesn't have the correct permissions to view the parent work in question.</exception>
        /// <returns>The volume corresponding to the provided id.</returns>
        Task<Volume> FindVolumeForDownload(Guid id, ClaimsPrincipal user);

        /// <summary>
        /// Retrieve a list of all volumes associated with a given work.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="workID">The identifier corresponding to the work in question.</param>
        /// <returns>A list of all volumes associated with a given work in the database.</returns>
        Task<List<Volume>> GetVolumesForWork(Guid workID);
    }
}
