﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying works.
    /// </summary>
    public interface IWorkUpdateService
    {
        /// <summary>
        /// Create a new volume in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="work">The work to be created.</param>
        /// <param name="primaryTags">The list of (primary) tags to attached to the newly created work.</param>
        /// <param name="secondaryTags">The list of (secondary) tags to attached to the newly created work.</param>
        /// <returns>A list of error strings for problems when creating the work.</returns>
        Task<string> CreateWork(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile);

        /// <summary>
        /// Delete an existing work from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="work">The work to be deleted</param>
        /// <exception cref="WorkNotDeletableException">Thrown when the work in question is not in a soft deleted state.</exception>
        /// <returns></returns>
        Task RemoveWork(Work work);

        /// <summary>
        /// Update an existing volume in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="work">The work to be updated.</param>
        /// <returns></returns>
        Task UpdateWork(Work work);

        /// <summary>
        /// Update an existing work in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="work">The work to be updated.</param>
        /// <param name="primaryTags">The list of (primary) tags to attached to the work.</param>
        /// <param name="secondaryTags">The list of (secondary) tags to attached to the work.</param>
        /// <returns>A list of error strings for problems when updating the work.</returns>
        Task<string> UpdateWork(Work work, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile coverFile, bool deleteCover);
    }
}
