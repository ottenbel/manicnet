﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding works.
    /// </summary>
    public interface IWorkRetrievalService
    {
        /// <summary>
        /// Returns a boolean signifying if there are any deleted works in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A boolean value denoting whether or not there are any soft deleted works in the database.</returns>
        Task<bool> DoDeletedWorksExist();

        /// <summary>
        /// Retrieve a specified work from the database.
        /// </summary>
        /// <param name="id">The identifier of the work to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the work.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a work to be modified).</param>
        /// <exception cref="WorkNotFoundException">Thrown when the identifier does not correspond to a work in the database.</exception>
        /// <exception cref="WorkNotViewableException">Thrown when the user making the call does not have the correct permissions to view the work in question.</exception>
        /// <returns>The work corresponding to the provided id.</returns>
        Work FindWork(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified work from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the work to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the work.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a work to be modified).</param>
        /// <exception cref="WorkNotFoundException">Thrown when the identifier does not correspond to a work in the database.</exception>
        /// <exception cref="WorkNotViewableException">Thrown when the user making the call does not have the correct permissions to view the work in question.</exception>
        /// <returns>The work corresponding to the provided id.</returns>
        Task<Work> FindWorkAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified work from the database with additional information for handing deletion.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the work to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the work.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a work to be modified).</param>
        /// <exception cref="WorkNotFoundException">Thrown when the identifier does not correspond to a work in the database.</exception>
        /// <exception cref="WorkNotViewableException">Thrown when the user making the call does not have the correct permissions to view the work in question.</exception>
        /// <returns>The work corresponding to the provided id.</returns>
        Task<Work> FindWorkForDeletion(Guid id, ClaimsPrincipal user, bool noTracking = false);

        /// <summary>
        /// Retrieve a specified work from the database with additional information for handing deletion.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the work to be retrieved.</param>
        /// <param name="user">The user attempting to retrieve the work.</param>
        /// <exception cref="WorkNotFoundException">Thrown when the identifier does not correspond to a work in the database.</exception>
        /// <exception cref="WorkNotViewableException">Thrown when the user making the call does not have the correct permissions to view the work in question.</exception>
        /// <returns>The work corresponding to the provided id.</returns>
        Task<Work> FindWorkForDownload(Guid id, ClaimsPrincipal user);

        /// <summary>
        /// Retrieves all active works that match the search string
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="search">A comma delimited search string specifying tags that should(/should not) be included in the work.</param>
        /// <param name="userID">The identifier for the user who is looking for active works (to include the user tag block list)</param>
        /// <returns>A list of all active tags in the database matching the search string.</returns>
        Task<List<Work>> GetActiveWorksBySearch(string search, Guid? userID);

        /// <summary>
        /// Retrieve a list of all deleted works.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="userID">The identifier for the user who is looking for active works (to include the user tag block list)</param>
        /// <returns>A list of all soft deleted works in the database.</returns>
        Task<List<Work>> GetDeletedWorks(Guid? userID);

        /// <summary>
        /// Retrieve a list of all active tags associated with a given work (and all descendents)
        /// </summary>
        /// <param name="workID">The identifier of the work to retrieve all tags for.</param>
        /// <returns></returns>
        Task<List<Guid>> GetAllTagsAssociatedWithWork(Guid workID);

        /// <summary>
        /// Retrieve a list of all active tags associated with a given work (and all descendents)
        /// </summary>
        /// <param name="workID">The identifier of the work to retrieve all tags for.</param>
        /// <param name="volumeID">The identifier of the volume to retrieve all tags for.</param>
        /// <returns></returns>
        Task<List<Guid>> GetAllTagsAssociatedWithVolume(Guid workID, Guid volumeID);

        /// <summary>
        /// Retrieve a list of all active tags associated with a given work (and all descendents)
        /// </summary>
        /// <param name="workID">The identifier of the work to retrieve all tags for.</param>
        /// <param name="volumeID">The identifier of the volume to retrieve all tags for.</param>
        /// <param name="chapterID">The identifier of the chapter to retrieve all tags for.</param>
        /// <returns></returns>
        Task<List<Guid>> GetAllTagsAssociatedWithChapter(Guid workID, Guid volumeID, Guid chapterID);

        /// <summary>
        /// Retrieve a list of all deleted volumes associated with a given work (included in the work repository because we are going through works to include full work tags for blocking purposes).
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="workID">The identifier corresponding to the work in question.</param>
        /// <param name="userID">The identifier for the user who is looking for active works (to include the user tag block list)</param>
        /// <returns>A list of all soft deleted volumes in the database (limited by the associated work identifier).</returns>
        Task<List<Volume>> GetDeletedVolumes(Guid? workID, Guid? userID);

        /// <summary>
        /// Retrieve all deleted chapters associated with a given volume (included in the work repository because we are going through works to include full work tags for blocking purposes).
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="volumeID">The identifier of the volume that chapters should be retrieved for.</param>
        /// <param name="userID">The identifier for the user who is looking for active works (to include the user tag block list)</param>
        /// <returns>A list of all soft deleted tag types in the database (limited by the associated volume identifier).</returns>
        Task<List<Chapter>> GetDeletedChapters(Guid? volumeID, Guid? userID);
    }
}
