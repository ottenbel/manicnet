﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying chapters.
    /// </summary>
    public interface IChapterUpdateService
    {
        /// <summary>
        /// Create a new chapter in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="chapter">The chapter object to be created.</param>
        /// <param name="primaryTags">A list of primary tags to be added on the chapter.</param>
        /// <param name="secondaryTags">A list of secondary tags to be added on the chapter.</param>
        /// <param name="pageFiles">A list of pages to be added to the chapter.</param>
        /// <param name="workID">The associated work that the chapter is created under.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns>A list of error strings for problems when creating the chapter.</returns>
        Task<List<string>> CreateChapter(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, IFormFile[] pageFiles, Guid workID, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Migrate a chapter to associated with a volume to a new volume.
        /// </summary>
        /// <param name="chapter">The chapter to be migrated.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns></returns>
        Task MigrateChapter(Chapter chapter, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Delete an existing chapter from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="chapter">The chapter to be deleted from the database.</param>
        /// <param name="cacheCleanup"></param>
        /// <param name="calledAsTopLevel">Controls whether or not this is called at the top level or not, 
        /// this controls when the changes are saved.</param>
        /// <returns></returns>
        Task<CleanupWork> RemoveChapter(Chapter chapter, WorkCacheCleanup cacheCleanup, bool calledAsTopLevel = true);

        /// <summary>
        /// Update an existing chapter in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="chapter">The chapter object to be updated.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns></returns>
        Task UpdateChapter(Chapter chapter, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Update an existing chapter in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="chapter">The chapter object to be updated.</param>
        /// <param name="primaryTags">A list of (primary) tags to be set on the chapter.</param>
        /// <param name="secondaryTags">A list of (secondary) tags to be set on the chapter.</param>
        /// <param name="workID">The associated work that the chapter is created under.</param>
        /// <param name="model">The model containing information for updating pages.</param>
        /// <param name="cacheCleanup"></param>
        /// <returns>A list of error strings for problems when updating the chapter.</returns>
        Task<List<string>> UpdateChapter(Chapter chapter, IEnumerable<Tag> primaryTags, IEnumerable<Tag> secondaryTags, Guid workID, ChapterModifyModel model, WorkCacheCleanup cacheCleanup);

        /// <summary>
        /// Update the sortable date on the work when a chapter is updated.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="workID">The associated work that the chapter is created under.</param>
        /// <returns></returns>
        Task UpdateSortableDate(Guid workID);
    }
}
