﻿using ManicNet.Domain.Models;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding chapters.
    /// </summary>
    public interface IChapterRetrievalService
    {
        /// <summary>
        /// Retrieve a specified chapter from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the chapter to be found.</param>
        /// <param name="user">The user attempting to find the chapter (used to determine if they can view the content)</param>
        /// <param name="bypassCache">Whether or not the cache should be bypassed when looking up the chapter 
        /// (only relevant when modifying a chapter)</param>
        /// <returns>The chapter corresponding to the provided id.</returns>
        Task<Chapter> FindChapter(Guid id, ClaimsPrincipal user, bool bypassCache = false);
    }
}
