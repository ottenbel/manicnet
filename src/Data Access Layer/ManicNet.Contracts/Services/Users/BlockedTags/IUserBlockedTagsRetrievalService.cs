﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface IUserBlockedTagsRetrievalService
    {
        /// <summary>
        /// Returns a boolean value denoting whether or not the user in question has blocked the provided tag or not.
        /// </summary>
        /// <param name="tagID">The identifier of the blocked tag.</param>
        /// <param name="userID">The identifier of the user that the blocked tag should be retrieved for.</param>
        /// <returns></returns>
        Task<bool> HasUserBlockedTag(Guid tagID, Guid userID);

        /// <summary>
        /// Retrieve the selected blocked tag from the database.
        /// </summary>
        /// <param name="tagID">The identifier of the blocked tag.</param>
        /// <param name="userID">The user that the blocked tag should be retrieved for.</param>
        /// <returns></returns>
        Task<User_Blocked_Tag> FindUserBlockedTag(Guid tagID, ClaimsPrincipal user);

        /// <summary>
        /// Retrieve a list of blocked tags for the specified user.
        /// </summary>
        /// <param name="userID">The identifier of the user that the list of blocked tags should be retrieved for.</param>
        /// <returns></returns>
        Task<List<User_Blocked_Tag>> GetUserBlockedTags(Guid userID);
    }
}
