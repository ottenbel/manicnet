﻿using ManicNet.Domain.Models;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface IUserBlockedTagsUpdateService
    {
        /// <summary>
        /// Creates a blocked tag in the database for the provided parameters.
        /// </summary>
        /// <param name="tagID">The identifier of the tag to be blocked.</param>
        /// <param name="userID">The identifier of the user blocking the tag.</param>
        /// <returns></returns>
        Task CreateUserBlockedTag(Guid tagID, Guid userID);

        /// <summary>
        /// Removes a blocked tag from the database.
        /// </summary>
        /// <param name="userBlockedTag">The blocked tag to delete.</param>
        /// <returns></returns>
        Task RemoveUserBlockedTag(User_Blocked_Tag userBlockedTag);
    }
}
