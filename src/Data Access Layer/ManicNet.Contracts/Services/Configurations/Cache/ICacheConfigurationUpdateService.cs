﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface ICacheConfigurationUpdateService
    {
        Task BulkCreateCacheConfiguration(List<CacheConfiguration> cacheConfigurations);
        Task BulkUpdateCacheConfiguration(List<CacheConfiguration> cacheConfigurations);
    }
}
