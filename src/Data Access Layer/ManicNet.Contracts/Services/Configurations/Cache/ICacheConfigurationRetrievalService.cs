﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface ICacheConfigurationRetrievalService
    {
        List<CacheConfiguration> GetCacheConfigurations();
        Task<List<CacheConfiguration>> GetCacheConfigurationsAsync();
        List<CacheConfiguration> GetCacheConfigurationsForSection(Guid sectionID);
    }
}
