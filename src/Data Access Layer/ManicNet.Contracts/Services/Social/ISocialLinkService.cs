﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface ISocialLinkService
    {
        /// <summary>
        /// Retrieve a list of all active non-deleted social links.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active social links in the database.</returns>
        Task<List<SocialLink>> GetActiveSocialLinks();


        /// <summary>
        /// Retrieves a list of all deleted social links.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all soft deleted social links in the database.</returns>
        Task<List<SocialLink>> GetDeletedSocialLinks(ClaimsPrincipal user);

        /// <summary>
        /// Returns a true/false value indicating if there are social links in a "deleted" state. 
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A boolean value denoting whether or not there are any soft deleted social links in the database.</returns>
        Task<bool> DoDeletedSocialLinksExist();

        /// <summary>
        /// Create a new social link in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="socialLink">The social link object to be created.</param>
        /// <returns></returns>
        Task CreateSocialLink(SocialLink socialLink);

        /// <summary>
        /// Retrieve a specified social link from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the social link in question.</param>
        /// <param name="user">The user attempting to retrieve the social link.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a social link to be modified).</param>
        /// <exception cref="SocialLinkNotFoundException">Thrown when the identifier in question does not correspond to a social link in the database.</exception>
        /// <exception cref="SocialLinkNotViewableException">Thrown when the user making the call does not have the correct permissions to view the social link in question.</exception>
        /// <returns></returns>
        Task<SocialLink> FindSocialLinkAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Update an existing social link in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="socialLink">The social link to be updated.</param>
        /// <returns></returns>
        Task UpdateSocialLink(SocialLink socialLink);

        /// <summary>
        /// Delete an existing social link from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="socialLink">The social link to be updated.</param>
        /// <exception cref="SocialLinkNotDeletableException">Thrown when the social link in question is not in a soft deleted state.</exception>
        /// <returns></returns>
        Task RemoveSocialLink(SocialLink socialLink);
    }
}
