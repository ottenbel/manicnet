﻿using System;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for service wrapping logging functions.
    /// </summary>
    public interface IManicNetLoggerService
    {
        /// <summary>
        /// Log a critical exception.
        /// </summary>
        /// <param name="exception">The exception to be logged.</param>
        /// <param name="message">The message to be logged.</param>
        void LogCritical(Exception exception, string message);

        /// <summary>
        /// Log an error.
        /// </summary>
        /// <param name="path">The location that the error was logged from.</param>
        /// <param name="functionIdentifier">The identifier of the function logging the error.</param>
        /// <param name="exception">The exception to be logged</param>
        /// <param name="message">The message to be logged.</param>
        void LogError(string path, string functionIdentifier, Exception exception);

        /// <summary>
        /// Log an error.
        /// </summary>
        /// <param name="path">The location that the error was logged from.</param>
        /// <param name="functionIdentifier">The identifier of the function logging the error.</param>
        /// <param name="exception">The exception to be logged</param>
        /// <param name="message">The message to be logged.</param>
        void LogError(string path, string functionIdentifier, Exception exception, string message);

        /// <summary>
        /// Log information.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        void LogInformation(string path, string functionIdentifier, string message);

        /// <summary>
        /// Log a warning.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        void LogWarning(string path, string functionIdentifier, string message);

        /// <summary>
        /// Log a warning.
        /// </summary>
        /// <param name="exception">The exception to be logged</param>
        /// <param name="message">The message to be logged.</param>
        void LogWarning(string path, string functionIdentifier, Exception exception, string message);
    }
}
