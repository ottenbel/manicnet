﻿using ManicNet.Domain.Models.Cache;
using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    public interface ICacheCleanerService
    {
        /// <summary>
        /// Update the related cache information for a collection.
        /// </summary>
        /// <param name="workCacheCleanup">The cache cleanup object containing the guids of all cache entries to be updated.</param>
        /// <returns></returns>
        Task UpdateCacheForWork(WorkCacheCleanup workCacheCleanup);

        /// <summary>
        /// Update the related cache information for a collection.
        /// </summary>
        /// <param name="collectionCacheCleanup">The cache cleanup object containing the guids of all cache entries to be updated.</param>
        /// <returns></returns>
        Task UpdateCacheForCollection(CollectionCacheCleanup collectionCacheCleanup);

        /// <summary>
        /// Update the related cache information for registration invites.
        /// </summary>
        /// <param name="registrationInviteCacheCleanup">The cache cleanup object containing the guids of all cache entries to be updated.</param>
        /// <returns></returns>
        Task UpdateCacheForRegistrationInvite(RegistrationInviteCacheCleanup registrationInviteCacheCleanup);

        /// <summary>
        /// Update the related cache information for tags.
        /// </summary>
        /// <param name="tagCacheCleanup">The cache cleanup object containing the guids of all cache entries to be updated.</param>
        /// <returns></returns>
        Task UpdateCacheForTag(TagCacheCleanup tagCacheCleanup);

        /// <summary>
        /// Update the related cache information for tag types.
        /// </summary>
        /// <param name="tagTypeCacheCleanup">The cache cleanup object containing the guids of all cache entries to be updated.</param>
        /// <returns></returns>
        Task UpdateCacheForTagType(TagTypeCacheCleanup tagTypeCacheCleanup);

        /// <summary>
        /// Update the related cache information for user blocked tags.
        /// </summary>
        /// <returns></returns>
        Task UpdateCacheForUserBlockedTags();

        /// <summary>
        /// Update the related cache information for cache configuration.
        /// </summary>
        /// <returns></returns>
        Task UpdateCacheForCacheConfiguration();

        /// Update the related cache information for social link.
        /// </summary>
        /// <returns></returns>
        Task UpdateCacheForSocialLink(Guid socialLinkID);
    }
}
