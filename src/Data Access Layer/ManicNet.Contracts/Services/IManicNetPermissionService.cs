﻿using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for service used to handle updating permissions/roles on site objects.
    /// </summary>
    public interface IManicNetPermissionService
    {
        /// <summary>
        /// Build a role section defining a list of roles for a user.
        /// </summary>
        /// <param name="user">The user that the role section is built for.</param>
        /// <param name="adminUser">The administrator that the role section is being built for.</param>
        /// <returns>A list of role sections.</returns>
        Task<RoleSection> BuildRoleSection(ManicNetUser user, ClaimsPrincipal adminUser);

        /// <summary>
        /// Build a permission section defining a list of permissions for a user.
        /// </summary>
        /// <param name="user">The user that the permission section is built for.</param>
        /// <param name="adminUser">The administrator that the permission section is being built for.</param>
        /// <returns>A list of permission sections.</returns>
        Task<List<PermissionSection>> BuildSection(ManicNetUser user, ClaimsPrincipal adminUser);

        /// <summary>
        /// Build a permission section defining a list of permissions for a role.
        /// </summary>
        /// <param name="role">The role that the permission section is built for.</param>
        /// <param name="adminUser">The administrator that the permission section is being built for.</param>
        /// <returns>A list of permission sections.</returns>
        Task<List<PermissionSection>> BuildSection(ManicNetRole role, ClaimsPrincipal adminUser);

        /// <summary>
        /// Create a list of claims to be updated for a user.
        /// </summary>
        /// <param name="permissionSections">The permission sections that should be updated.</param>
        /// <param name="user">The user that will have their permissions updated.</param>
        /// <param name="adminUser">The administrator updating user permissions.</param>
        /// <returns>A list of claims to be updated.</returns>
        Task<UpdateClaimsHolder> CreatePermissionAdditionAndRemovalLists(IEnumerable<PermissionSection> permissionSections, ManicNetUser user, ClaimsPrincipal adminUser);

        /// <summary>
        /// Create a list of claims to be updated for a role.
        /// </summary>
        /// <param name="permissionSections">The permission sections that should be updated.</param>
        /// <param name="role">The role that will have their permissions updated.</param>
        /// <param name="adminUser">The administrator updating role permissions.</param>
        /// <returns>A list of claims to be updated.</returns>
        Task<UpdateClaimsHolder> CreatePermissionAdditionAndRemovalLists(IEnumerable<PermissionSection> permissionSections, ManicNetRole role, ClaimsPrincipal adminUser);

        /// <summary>
        /// Create a list of roles to be updated for a user.
        /// </summary>
        /// <param name="roleSection">The list of roles to be updated.</param>
        /// <param name="user">The user that will have their roles updated.</param>
        /// <param name="adminUser">The administrator updating user roles.</param>
        /// <returns>A list of roles to be updated.</returns>
        Task<UpdateRolesHolder> CreateRoleAdditionAndRemovalLists(RoleSection roleSection, ManicNetUser user, ClaimsPrincipal adminUser);
    }
}
