﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling registration invites.
    /// </summary>
    public interface IRegistrationInviteService
    {
        /// <summary>
        /// Create a new registration invite in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="registrationInvite">The identifier of the registration invite in question.</param>
        /// <returns></returns>
        Task CreateRegistrationInvite(RegistrationInvite registrationInvite);

        /// <summary>
        /// Retrieve a specific registration invite by id.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the registration invite in question.</param>
        /// <exception cref="RegistrationInviteNotFoundException">Thrown when the identifier in question does not correspond to a registration invite in the database.</exception>
        /// <returns>The registration invite corresponding to the provided hash value.</returns>
        Task<RegistrationInvite> FindRegistrationInvite(Guid id);

        /// <summary>
        /// Retrieve a specific registration invite by email address.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="email">The email address to retrieve a registration invite for.</param>
        /// <returns></returns>
        Task<RegistrationInvite> FindRegistrationInvite(string email);

        /// <summary>
        /// Retrieve a list of all active registration invites.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active registration invites in the database.</returns>
        Task<List<RegistrationInvite>> GetActiveInvites();

        /// <summary>
        /// Retrieve a list of all expired registration invites.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all expired registration invites in the database.</returns>
        Task<List<RegistrationInvite>> GetExpiredInvites();

        /// <summary>
        /// Retrieve a list of all pending registration invites.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all pending registration invites in the database.</returns>
        Task<List<RegistrationInvite>> GetPendingInvites();

        /// <summary>
        /// Delete an existing tag from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="registrationInvite">The registration invite to be deleted from the database.</param>
        /// <returns></returns>
        Task RemoveRegistrationInvite(RegistrationInvite registrationInvite);

        /// <summary>
        /// Update an existing registration invite in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="registrationInvite">The identifier of the registration invite in question.</param>
        /// <returns></returns>
        Task UpdateRegistrationInvite(RegistrationInvite registrationInvite);

        /// <summary>
        /// Validate an email/invite code combination to determine validity.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="email">The email address to be validated against.</param>
        /// <param name="inviteCode">The invite code to check against the email address.</param>
        /// <returns>A boolean value denoting if the registration email combination is valid.</returns>
        Task<bool> ValidateRegistrationInvite(string email, Guid inviteCode);
    }
}
