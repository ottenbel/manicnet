﻿using System;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for service used to send emails from site.
    /// </summary>
    public interface IManicNetEmailService
    {
        /// <summary>
        /// Send an account registration email.
        /// </summary>
        /// <param name="email">The email address that the account registration is sent to.</param>
        /// <param name="userName">The username of the account that the registration has been conducted for.</param>
        /// <param name="accountConfirmationURL">The url of the account confirmation link.</param>
        /// <returns></returns>
        Task SendAccountRegistration(string email, string userName, string accountConfirmationURL);

        /// <summary>
        /// Send an email confirmation email for an account
        /// </summary>
        /// <param name="email">The email address that the confirmation email is sent to.</param>
        /// <param name="userName">The username of the account that the registration has been conducted for.</param>
        /// <param name="accountConfirmationURL"></param>
        /// <returns></returns>
        Task SendEmailConfirmation(string email, string userName, string accountConfirmationURL);

        /// <summary>
        /// Sends an email containing a registration invite.
        /// </summary>
        /// <param name="email">The email address that the registration email is sent to.</param>
        /// <param name="registrationInviteId">The identifier of the registration.</param>
        /// <param name="expires">The date time that the registration invite will expire.</param>
        /// <returns></returns>
        Task SendRegistrationInvite(string email, Guid registrationInviteId, DateTime? expires);

        /// <summary>
        /// Send a password reset email.
        /// </summary>
        /// <param name="email">The email address that the password reset is sent to.</param>
        /// <param name="userName">The username of the account that the password reset has been requested for.</param>
        /// <param name="passwordResetURL">The url of the password reset link.</param>
        /// <returns></returns>
        Task SendResetPasssword(string email, string userName, string passwordResetURL);
    }
}
