﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying tags.
    /// </summary>
    public interface ITagUpdateService
    {
        /// <summary>
        /// Create a new tag in the database.  
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tag">The tag object to be created.</param>
        /// <param name="includedTags">The list of tags to be implied by the newly created tag.</param>
        /// <returns></returns>
        Task CreateTag(Tag tag, IEnumerable<Tag> includedTags);

        /// <summary>
        /// Migrate a tag to associated with collection to a new tag.
        /// </summary>
        /// <param name="source">The tag to be mgirated.</param>
        /// <param name="destination">The tag to be migrated to.</param>
        /// <param name="ConvertMigratedTagToDestinationAlias">If the name of the tag should be created as an alias on the destination.</param>
        /// <param name="RedirectMigratedAliasesToDestination">If aliases on the source tag should be migrated to the destination tags.</param>
        /// <returns></returns>
        Task MigrateTag(Tag source, Tag destination, bool ConvertMigratedTagToDestinationAlias, bool RedirectMigratedAliasesToDestination);

        /// <summary>
        /// Delete an existing tag from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tag">The tag to be deleted from the database.</param>
        /// <param name="calledAsTopLevel">Determines if the call to this function should trigger the save to the database 
        /// (or leaving it to the calling parent). The deletion check is only engaged if the function is called as the top level.</param>
        /// <exception cref="TagNotDeletableException">Thrown when the tag in question is not in a soft deleted state.</exception>
        /// <returns></returns>
        Task RemoveTag(Tag tag, bool calledAsTopLevel = true);

        /// <summary>
        /// Update an existing tag in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tag">The tag object to be updated.</param>
        /// <param name="calledAsTopLevel">Determines if the call to this function should trigger the save to the database 
        /// (or leaving it to the calling parent). The deletion check is only engaged if the function is called as the top level.</param>
        /// <returns></returns>
        Task UpdateTag(Tag tag, bool calledAsTopLevel = true);

        /// <summary>
        /// Update an existing tag in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tag">The tag object to be updated.</param>
        /// <param name="includedTags">The list of tags to be implied by the newly updated tag.</param>
        /// <param name="tagCacheCleanup"></param>
        /// <returns></returns>
        Task UpdateTag(Tag tag, IEnumerable<Tag> includedTags, TagCacheCleanup tagCacheCleanup);

        /// <summary>
        /// Compare a tag and list of tags to determine which tags can be implied by the tag in question
        /// and which tags cannot be implied by the tag in question without causing implication loops.
        /// 
        /// </summary>
        /// <param name="tag">The tag to validate adding implied tags to.</param>
        /// <param name="impliedTags">The tags to validate that can be implied by the tag in question.</param>
        /// <param name="ancestry">A list of tags defining the ancestery of the tag in question.</param>
        /// <returns></returns>
        TagSearchLists ValidateTagImplicationList(Tag tag, IEnumerable<Tag> impliedTags, IEnumerable<Tag> ancestry);
    }
}
