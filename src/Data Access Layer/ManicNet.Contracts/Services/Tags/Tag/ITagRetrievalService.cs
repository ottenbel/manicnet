﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding tags.
    /// </summary>
    public interface ITagRetrievalService
    {
        /// <summary>
        /// Returns a true/false value indicating if there are tags in a "deleted" state. 
        /// 
        /// Specifically if the tag is deleted or you can view deleted tag types and there are deleted tag types with associated tags
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="user">The current user. Determines if they have permission to view deleted tags</param>
        /// <returns>A boolean value denoting whether or not there are any soft deleted tags in the database.</returns>
        Task<bool> DoDeletedTagTypesExist(ClaimsPrincipal user);

        /// <summary>
        /// Tags a list of tags and transforms it into an included/excluded list based on the provided work type.
        /// 
        /// This function assumes that the provided tag lists include the tag type as a navigation property.
        /// </summary>
        /// <param name="tags">The list of tags to be processed.</param>
        /// <param name="WorkTypeID">The work type identifier to check against.</param>
        /// <param name="includeVirtual">A boolean to determine whether or not virtual tags can be included on the list.</param>
        /// <returns>An tag list object containing two lists of tags one for tags that can be included on the work type and one containing the 
        /// list of tags that cannot be included on the work type.</returns>
        TagSearchLists FilterTagsByWorkType(IEnumerable<Tag> tags, Guid WorkTypeID, bool includeVirtual);

        /// <summary>
        /// Retrieve a specified tag from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the tag in question.</param>
        /// <param name="user">The user attempting to retrieve the tag.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a tag to be modified).</param>
        /// <exception cref="TagNotFoundException">Thrown when the identifier in question does not correspond to a tag in the database.</exception>
        /// <exception cref="TagNotViewableException">Thrown when the user making the call does not have the correct permissions to view the tag in question.</exception>
        /// <exception cref="TagUnableToViewParentTagTypeException">Thrown when the parent tag type is in a soft deleted state and the user making the call doesn't have the correct permissions to view the parent tag type in question.</exception>
        /// <returns>The tag corresponding to the provided id.</returns>
        Task<Tag> FindTag(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified tag from the database by the tag name.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the tag in question.</param>
        /// <param name="user">The user attempting to retrieve the tag.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a tag to be modified).</param>
        /// <exception cref="TagNotFoundException">Thrown when the identifier in question does not correspond to a tag in the database.</exception>
        /// <exception cref="TagNotViewableException">Thrown when the user making the call does not have the correct permissions to view the tag in question.</exception>
        /// <exception cref="TagUnableToViewParentTagTypeException">Thrown when the parent tag type is in a soft deleted state and the user making the call doesn't have the correct permissions to view the parent tag type in question.</exception>
        /// <returns>The tag corresponding to the provided name string.</returns>
        Task<Tag> FindTagByName(string name, ClaimsPrincipal user);

        /// <summary>
        /// Retrieve a list of all non-deleted tags.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active tags in the database.</returns>
        Task<List<Tag>> GetActiveTags();

        /// <summary>
        /// Convert a search string into a two lists of public availiable tags to reflect included and excluded tags.
        /// </summary>
        /// <param name="searchString">The search string being converted to tag lists.</param>
        /// <returns>A tag search list corresponding to active tags in the database matching the search string.</returns>
        Task<TagSearchLists> GetActiveTagSearchListsFromSearchString(string searchString);

        /// <summary>
        /// Retrieves a list of all deleted tags.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all soft deleted tags in the database.</returns>
        Task<List<Tag>> GetDeletedTags(ClaimsPrincipal user);

        /// <summary>
        /// Retrieve a list of all tags that imply the provided tag. 
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tag">The tag to find all ancestors for.</param>
        /// <returns>A list of all tags that imply the provided tag.</returns>
        Task<List<Tag>> GetListOfTagsImplyingTag(Tag tag);

        /// <summary>
        /// Retrieve a list of all tags that imply the provided tags. 
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns>A list of related tag mappings mapping out the list of all tags that imply each tag in the input list.</returns>
        Task<List<RelatedTagList>> GetListOfTagsImplyingTags(IEnumerable<Tag> tags);

        /// <summary>
        /// Retrieve a list of all tags for a given tag type (used to clean out the cache during the deletion of a tag type).
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tagTypeID">The identifier of the tag type to retrieve tags for</param>
        /// <returns>A list of all tags under the given tag type.</returns>
        Task<List<Tag>> GetTagsByTagType(Guid tagTypeID);
    }
}
