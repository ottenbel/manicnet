﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling retrieving/finding tag types.
    /// </summary>
    public interface ITagTypeRetrievalService
    {
        /// <summary>
        /// Returns a boolean value reflecting whether or not deleted tag types exist in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A boolean value denoting whether or not there are any soft deleted tag types in the database.</returns>
        Task<bool> DoDeletedTagTypesExistAsync();

        /// <summary>
        /// Retrieve a specified tag type from the database.
        /// </summary>
        /// <param name="id">The identifier of the tag type in question.</param>
        /// <param name="user">The user attempting to retrieve the tag type.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a tag type to be modified).</param>
        /// <exception cref="TagTypeNotFoundException">Thrown when the identifier in question does not correspond to a tag type in the database.</exception>
        /// <exception cref="TagTypeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the tag type in question.</exception>
        /// <returns>The tag type corresponding to the provided id.</returns>
        TagType FindTagType(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified tag type from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="id">The identifier of the tag type in question.</param>
        /// <param name="user">The user attempting to retrieve the tag type.</param>
        /// <param name="bypassCache">Whether or not to ignore the cache (only used when retrieving a tag type to be modified).</param>
        /// <exception cref="TagTypeNotFoundException">Thrown when the identifier in question does not correspond to a tag type in the database.</exception>
        /// <exception cref="TagTypeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the tag type in question.</exception>
        /// <returns>The tag type corresponding to the provided id.</returns>
        Task<TagType> FindTagTypeAsync(Guid id, ClaimsPrincipal user, bool bypassCache = false);

        /// <summary>
        /// Retrieve a specified tag type (with associated tag -> work type information ) from the database.
        /// </summary>
        /// <param name="id">The identifier of the tag type in question.</param>
        /// <param name="user">The user attempting to retrieve the tag type.</param>
        /// <exception cref="TagTypeNotFoundException">Thrown when the identifier in question does not correspond to a tag type in the database.</exception>
        /// <exception cref="TagTypeNotViewableException">Thrown when the user making the call does not have the correct permissions to view the tag type in question.</exception>
        /// <returns>The tag type corresponding to the provided id.</returns>
        TagType FindTagTypeWithWorkAssociation(Guid id, ClaimsPrincipal user);

        /// <summary>
        /// Retrieve a list of all non-deleted tag types.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active tag types in the database.</returns>
        Task<List<TagType>> GetActiveTagTypes();

        /// <summary>
        /// Retrieve a list of all non-deleted tag types that can be attached to a given work type.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="workType">The identifier signifying the work type (work/volume/chapter)</param>
        /// <returns> list of all active tag types in the database for a given work type.</returns>
        Task<List<TagType>> GetActiveTagTypesForWorkType(Guid workType);

        /// <summary>
        /// Retrieves a list of all deleted tag types.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all soft deleted tag types in the database.</returns>
        Task<List<TagType>> GetDeletedTagTypes();

        /// <summary>
        /// Returns a boolean value corresponding to whether or not a given name is unique for tag types within the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="name">The tag type name to be checked for uniqueness.</param>
        /// <param name="tagTypeID">The identifier of the tag type being modified.</param>
        /// <returns>A boolean value denoting whether or not the provided name is unique in the database.</returns>
        bool HasUniqueName(string name, Guid? tagTypeID = null);

        /// <summary>
        /// Returns a boolean value corresponding to whether or not a given searchable prefix is unique for tag types within the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="prefix">The tag type prefix to be checked for uniqueness.</param>
        /// <param name="id">The identifier of the tag type being modified.</param>
        /// <returns>A boolean value denoting whether or not the provided prefix is unique in the database.</returns>
        bool HasUniqueSearchablePrefix(string prefix, Guid? id = null);

        /// <summary>
        /// Returns the active tag types as a select list.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A select list of tag types.</returns>
        Task<List<SelectListItem>> GetTagTypesSelectList();
    }
}
