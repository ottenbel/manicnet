﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling modifying tag types.
    /// </summary>
    public interface ITagTypeUpdateService
    {
        /// <summary>
        /// Create a new tag type in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tagType">The tag type object to be created.</param>
        Task CreateTagType(TagType tagType);

        /// <summary>
        /// Remove a specified tag type from the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tagType">The tag type object to be removed.</param>
        /// <param name="associatedTags">The tags associated with the tag type to be cleaned up during the removal.</param>
        /// <exception cref="TagTypeNotDeletableException">Thrown when the tag type is not already in a soft deleted state.</exception>
        Task RemoveTagType(TagType tagType, IEnumerable<Tag> associatedTags);

        /// <summary>
        /// Update a new tag type in the database.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="tagType">The tag type object to be updated.</param>
        Task UpdateTagType(TagType tagType, IEnumerable<Tag> associatedTags);
    }
}
