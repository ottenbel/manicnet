﻿using System;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling common functionality between tags and tag aliases.
    /// </summary>
    public interface ICommonTagService
    {
        /// <summary>
        /// Returns a boolean value corresponding to whether or not a given alias name is unique for tags/aliases within the database.
        /// </summary>
        /// <param name="name">The alias name to be checked for uniqueness.</param>
        /// <param name="tagID">The identifier of the tag that the alias is being checked for.</param>
        /// <returns>A boolean value denoting if a given alias is a unique between tag names and aliases.</returns>
        bool HasUniqueAlias(string name, Guid? tagID = null);

        /// <summary>
        /// Returns a boolean value corresponding to whether or not a tag given name is unique for tags/aliases within the database.
        /// </summary>
        /// <param name="name">The tag name to be checked for uniqueness.</param>
        /// <param name="tagID">The (optional) identifier of the tag being modified.</param>
        /// <returns>A boolean value denoting if a given tag name is a unique between tag names and aliases.</returns>
        bool HasUniqueName(string name, Guid? tagID = null);
    }
}
