﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Contracts.Services
{
    /// <summary>
    /// Interface for the service handling tag aliases.
    /// </summary>
    public interface ITagAliasService
    {
        /// <summary>
        /// Get the list of all active (attached to a non-deleted tag) aliases.
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <returns>A list of all active tag aliases in the database.</returns>
        Task<List<TagAlias>> GetActiveTagAliases();

        /// <summary>
        /// Takes a list of strings and returns a list of aliases corresponding to that string. 
        /// If an alias in the list doesn't exist a new aliases will be detected. Aliases 
        /// associated with deleted tags will be returned by this function as well
        /// 
        /// This function must be awaited or it will cause problems with DBContext threading issues.
        /// </summary>
        /// <param name="aliases">The list of strings to be transformed into a list of aliases.</param>
        /// <returns>A list of all aliases in the database corresponding to a list of strings.</returns>
        Task<List<TagAlias>> GetAliasList(IEnumerable<string> aliases);
    }
}
