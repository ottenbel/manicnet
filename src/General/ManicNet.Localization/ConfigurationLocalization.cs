﻿namespace ManicNet.Localization
{
    public class ConfigurationLocalization
    {
        public class CacheConfiguration
        {
            public const string ALL = "All";
            public const string DAY = "Day";
            public const string FIND = "Find";
            public const string HOUR = "Hour";
            public const string MINUTE = "Minute";
            public const string SECOND = "Second";
            public const string CACHE_CONFIGURATION_SUCCESSFULLY_EDITED = "Successfully edited cache configurations.";
            public const string CACHE_CONFIGURATIONS = "Cache Configurations";
            public const string CONFIGURATION_ADMINISTRATION = "Configuration Administration";
            public const string ERROR_EDITING_CACHE_CONFIGURATIONS = "Error editing cache configurations.";
            public const string NO_CACHE_CONFIGURATIONS_FOUND = "No cache configurations found in the database. Check the configuration file to ensure that cache configurations are being seeded.";
            public const string UNABLE_TO_RETRIEVE_LIST_OF_CACHE_CONFIGURATIONS = "Unable to retrieve list of cache configurations.";

            public class Validation
            {
                public const string INVALID_SECTION = "Section {0} does not correspond to a known section.";
                public const string INVALID_LENGTH_TYPE = "Length type {0} does not correspond to a known type.";
                public const string INVALID_LOOKUP_TYPE = "Lookup type {0} does not correspond to a known type.";
                public const string INVALID_LENGTH = "Length {0} must be greater than zero.";
            }
        }
    }
}
