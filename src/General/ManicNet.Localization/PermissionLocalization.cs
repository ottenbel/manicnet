﻿namespace ManicNet.Localization
{
    public class PermissionLocalization
    {
        public const string DELETE_COLLECTION = "Delete Collection";
        public const string DELETE_REGISTRATION_INVITE = "Delete Registration Invite";
        public const string DELETE_ROLE = "Delete Role";
        public const string DELETE_TAG = "Delete Tag";
        public const string DELETE_TAG_TYPE = "Delete Tag Type";
        public const string DELETE_WORK = "Delete Work";
        public const string DOWNLOAD_CHAPTER = "Download Chapter";
        public const string DOWNLOAD_VOLUME = "Download Volume";
        public const string DOWNLOAD_WORK = "Download Work";
        public const string EDIT_ADMINISTRATOR_PERMISSIONS = "Edit Administrator Permissions";
        public const string EDIT_ADMINISTRATOR_ROLES = "Edit Administrator Roles";
        public const string EDIT_COLLECTION = "Edit Collection";
        public const string EDIT_OWNER_PERMISSIONS = "Edit Owner Permissions";
        public const string EDIT_OWNER_ROLES = "Edit Owner Roles";
        public const string EDIT_REGISTRATION_INVITE = "Edit Registration Invite";
        public const string EDIT_ROLE = "Edit Role";
        public const string EDIT_TAG = "Edit Tag";
        public const string EDIT_TAG_TYPE = "Edit Tag Type";
        public const string EDIT_USER_PERMISSIONS = "Edit User Permissions";
        public const string EDIT_USER_ROLES = "Edit User Roles";
        public const string EDIT_WORK = "Edit Work";
        public const string MANAGE_CACHE_CONFIGURATION = "Manage Cache Configurations";
        public const string MIGRATE_TAG = "Migrate Tag";
        public const string REASSIGN_DEFAULT_USER_ROLE = "Reassign Default User Role";
        public const string VIEW_DELETED_COLLECTION = "View Deleted Collection";
        public const string VIEW_DELETED_TAG = "View Deleted Tag";
        public const string VIEW_DELETED_TAG_TYPE = "View Deleted Tag Type";
        public const string VIEW_DELETED_WORK = "View Deleted Work";
    }
}
