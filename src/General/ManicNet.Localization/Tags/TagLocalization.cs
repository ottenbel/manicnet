﻿namespace ManicNet.Localization
{
    public class TagLocalization
    {
        public const string ACTIVE_TAG_TYPES_MUST_EXIST = "Active tag types must exist before tags can be created.";
        public const string ALIASES = "Aliases";
        public const string BULK_CREATE_TAGS = "Bulk Create Tags";
        public const string CREATE_NEW_TAGS = "Create New Tags";
        public const string CREATE_TAG = "Create Tag";
        public const string DELETED_TAGS = "Deleted Tags";
        public const string EDIT_TAG = "Edit Tag - {0}";
        public const string ERROR_CREATING_TAG = "Error creating tag {0}.";
        public const string ERROR_CREATING_TAGS = "Error creating tags {0}.";
        public const string ERROR_EDITING_TAG = "Error editing tag {0}.";
        public const string IMPLIED_BY = "Implied By";
        public const string IMPLIES = "Implies";
        public const string IMPLIES_TAGS = "Implies Tags";
        public const string MIGRATE_TAG = "Migrate Tag - {0}";
        public const string PRIMARY_TAG_OF_TAG_TYPE = "Primary {0}";
        public const string RELATED_TAGS = "Related Tags";
        public const string NO_TAGS_FOUND = "No tags found in the database that match the provided parameters.";
        public const string SEARCH_FOR_TAG = "Search For Tag";
        public const string SECONDARY_TAG_OF_TAG_TYPE = "Secondary {0}";
        public const string SUCCESSFULLY_RESTORED_TAG_UNABLE_TO_IMPLY_ALL = "Successfully restored tag object, but was unable to successfully include all implied tags.";
        public const string SUCCESSFULLY_UPDATED_TAG_UNABLE_TO_IMPLY_ALL = "Successfully updated tag object, but was unable to successfully include all implied tags.";
        public const string TAG_ADMINISTRATION = "Tag Administration";
        public const string TAG_CAUSES_IMPLICATION_LOOPS = "<strong>{0}</strong> causes tag implication loops with {1} and cannot be implied.";
        public const string TAG_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A tag cannot be edited while in a deleted state. Restore the tag before attempting to edit it.";
        public const string TAG_IS_NOT_IN_DELETABLE_STATE = "Tag {0} is not in a deletable state.";
        public const string TAG_IS_NOT_IN_DELETABLE_STATE_IMPLIES_OTHER_TAGS = "Tag {0} is not in a deletable state as it implies other tags. Remove all implied tags before triggering deletion.";
        public const string TAG_TITLE = "Tag - {0}";
        public const string TAG_TYPE = "Tag Type";
        public const string TAG_SUCCESSFULLY_CREATED = "Tag {0} was successfully created.";
        public const string TAG_SUCCESSFULLY_DELETED = "Tag {0} was successfully deleted.";
        public const string TAG_SUCCESSFULLY_EDITED = "Tag {0} was successfully edited.";
        public const string TAG_SUCCESSFULLY_RESTORED = "Tag {0} was successfully restored.";
        public const string TAG_SUCCESSFULLY_SOFT_DELETED = "Tag {0} was successfully soft deleted.";
        public const string TAGS = "Tags";
        public const string TAGS_WERE_SUCCESSFULLY_CREATED = "Tags were successfully created. {0}";
        public const string UNABLE_TO_DELETE_TAG = "Unable to delete tag {0}.";
        public const string UNABLE_TO_LOAD_INFORMATION_FOR_TAG = "Unable to load information for tag.";
        public const string UNABLE_TO_MIGRATE_TAG_SOURCE_AND_DESTINATION_CANNOT_BE_THE_SAME = "Unable to migrate tag {0}. Source and destination tags cannot be the same.";
        public const string UNABLE_TO_MIGRATE_TAG_DESTINATION_DOES_NOT_EXIST = "Unable to migrate tag. Destination tag {0} does not exist.";
        public const string UNABLE_TO_MIGRATE_TAG_DESTINATION_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO = "Unable to migrate tag. Destination tag {0} is not in a state to be migrated to.";
        public const string UNABLE_TO_MIRGRATE_TAG = "Unable to migrate tag {0}.";
        public const string UNABLE_TO_RESTORE_TAG_WITH_DELETED_TAG_TYPE = "{0} cannot be restored while the associated tag type {1} is in a deleted state.";
        public const string UNABLE_TO_RESTORE_TAG = "Unable to restore tag {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_TAGS = "Unable to retrieve list of deleted tags.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_TAGS = "Unable to retrieve list of tags.";
        public const string UNABLE_TO_RETRIEVE_TAG_INFORMATION = "Unable to retrieve tag information.";
        public const string UNABLE_TO_RETRIEVE_TAG_TYPE_INFORMATION = "Unable to retrieve tag type information.";
        public const string UNABLE_TO_SOFT_DELETE_TAG = "Unable to soft delete tag {0}.";
        public const string VIRTUAL_TAG_CANNOT_BE_MIGRATED = "A virtual tag cannot be migrated.";

        public class Help
        {
            public const string A_COMMA_DELIMITED_LIST_OF_PRIMARY_TAGS_RELATED_TO_THE_GIVEN_OBJECT = "A comma delimited list of primary {0} tags related to the given object.";
            public const string A_COMMA_DELIMITED_LIST_OF_SECONDARY_TAGS_RELATED_TO_THE_GIVEN_OBJECT = "A comma delimited list of secondary {0} tags related to the given object.";
            public const string A_COMMA_DELIMITED_LIST_OF_TAGS_RELATED_TO_THE_GIVEN_OBJECT = "A comma delimited list of {0} tags related to the given object";
            public const string ALIASES = "A comma delimited list of alternate names that the tag is also known by.";
            public const string BULK_ARE_VIRTUAL = "Are the tags searchable only.";
            public const string BULK_IMPLIED_TAGS = "A comma delimited list of tags implied by the given tags.";
            public const string BULK_NAMES = "The names of the tags.";
            public const string BULK_TAG_TYPES = "The tag type that the tags belongs to.";
            public const string CONVERT_MIGRATED_TAG_TO_DESTINATION_ALIAS = "An alias is created for the destination tag with the name of the migrated tag.";
            public const string FONT_FAMILY = "The font awesome family that the icon belongs to.";
            public const string IMPLIED_TAGS = "A comma delimited list of tags implied by the given tag.";
            public const string IS_VIRTUAL = "Is the tag searchable only.";
            public const string ICON = "The (unicode value of the) font awesome icon attached to the tags overwriting the tag type icon.";
            public const string LONG_DESCRIPTION = "A detailed description of the tag.";
            public const string MIGRATED_DESTINATION_TAG_NAME = "The destination tag that the migrated tag will be moved to.";
            public const string NAME = "The name of the tag.";
            public const string REDIRECT_MIGRATED_ALIASES_TO_DESTINATION = "Any aliases on the migrated tag are redirected to point to the destination tag.";
            public const string SHORT_DESCRIPTION = "A short description of the tag.";
            public const string TAG_TYPE = "The tag type that this tag belongs to.";
            public const string URL = "A URL providing additional information about the tag.";
        }

        public class Validation
        {
            public const string ALIAS_IS_NOT_IN_CORRECT_FORMAT = "'Alias' {0} is not in the correct format.";
            public const string ALIAS_IS_NOT_UNIQUE = "'Alias' {0} is not unique.";
            public const string FONT_FAMILY_IS_REQUIRED = "The font family for the icon must be specified.";
            public const string FONT_FAMILY_IS_UNKNOWN = "The font family for the icon does not match a known font family.";
            public const string NAME_IS_NOT_IN_CORRECT_FORMAT = "'Name' {0} is not in the correct format.";
            public const string NAME_IS_NOT_UNIQUE = "'Name' {0} is not unique.";
            public const string TAG_TYPE_MUST_EXIST_AND_BE_ACTIVE = "'Tag Type' must exist and be active.";
        }
    }
}
