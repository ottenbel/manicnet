﻿namespace ManicNet.Localization
{
    public class TagTypeLocalization
    {
        public const string CREATE_TAG_TYPE = "Create Tag Type";
        public const string DELETED_TAG_TYPES = "Deleted Tag Types";
        public const string EDIT_TAG_TYPE = "Edit Tag Type - {0}";
        public const string ERROR_CREATING_TAG_TYPE = "Error creating tag type {0}.";
        public const string TAG_TYPE_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A tag type cannot be edited while in a deleted state. Restore the tag type before attempting to edit it.";
        public const string TAG_TYPE_IS_NOT_IN_A_DELETABLE_STATE = "Tag type {0} is not in a deletable state.";
        public const string TAG_TYPE_TITLE = "Tag Type - {0}";
        public const string TAG_TYPE_SUCCESSFULLY_CREATED = "Tag type {0} was successfully created.";
        public const string TAG_TYPE_SUCCESSFULLY_DELETED = "Tag type {0} was successfully deleted.";
        public const string TAG_TYPE_SUCCESSFULLY_RESTORED = "Tag type {0} was successfully restored.";
        public const string TAG_TYPE_SUCCESSFULLY_SOFT_DELETED = "Tag type {0} was successfully soft deleted.";
        public const string TAG_TYPE_SUCCESSFULLY_UPDATED = "Tag type {0} was successfully updated.";
        public const string TAG_TYPE_MUST_BE_USABLE = "Tag type must be usable on at least one work type.";
        public const string TAG_TYPES = "Tag Types";
        public const string UNABLE_TO_DELETE_TAG_TYPE = "Unable to delete tag type {0}.";
        public const string UNABLE_TO_LOAD_TAG_TYPE_INFORMATION = "Unable to load information for tag type.";
        public const string UNABLE_TO_RESTORE_TAG_TYPE = "Unable to restore tag type {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_TAG_TYPES = "Unable to retrieve list of deleted tag types.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_TAG_TYPES = "Unable to retrieve list of tag types.";
        public const string UNABLE_TO_SOFT_DELETE_TAG_TYPE = "Unable to soft delete tag type {0}.";
        public const string UNABLE_TO_UPDATE_TAG_TYPE_INFORMATION = "Unable to update information for tag type.";

        public class Help
        {
            public const string AVAILABLE_ON_WORK = "Tags of this type be added to a work.";
            public const string AVAILABLE_ON_VOLUME = "Tags of this type can be added to a volume.";
            public const string AVAILABLE_ON_CHAPTER = "Tags of this type can be added to a chapter.";
            public const string BODY_COLOUR = "The (hex notation of the) default body colour of all tags under the tag type.";
            public const string BORDER_COLOUR = "The (hex notation of the) default border colour of all tags under the tag type.";
            public const string DISPLAYED_IN_WORK_SUMMARY = "Tags of this type are displayed as part of work summaries.";
            public const string DISPLAYED_IN_VOLUME_SUMMARY = "Tags of this type are displayed as part of volume summaries.";
            public const string DISPLAYED_IN_CHAPTER_SUMMARY = "Tags of this type are displayed as part of chapter summaries.";
            public const string FONT_FAMILY = "The font awesome family that the icon belongs to.";
            public const string ICON = "The (unicode value of the) default font awesome icon attached to all tags under the tag type.";
            public const string LONG_DESCRIPTION = "A detailed description of the tag type.";
            public const string NAME = "The name of the tag type.";
            public const string NUMBER_DISPLAYED_IN_WORK_SUMMARY = "The number of tags of this type that are displayed as part of work summaries.";
            public const string NUMBER_DISPLAYED_IN_VOLUME_SUMMARY = "The number of tags of this type that are displayed as part of volume summaries.";
            public const string NUMBER_DISPLAYED_IN_CHAPTER_SUMMARY = "The number of tags of this type that are displayed as part of chapter summaries.";
            public const string PRIORITY = "The order in which the tag types are displayed.";
            public const string SEARCHABLE_PREFIX = "The prefix used when searching for tags of this type.";
            public const string SHORT_DESCRIPTION = "A short description of the tag type.";
            public const string TEXT_COLOUR = "The (hex notation of the) default text colour of all tags under the tag type.";
            public const string URL = "A URL providing additional information about the tag type.";
        }

        public class Validation
        {
            public const string FONT_FAMILY_IS_REQUIRED = "The font family for the icon must be specified.";
            public const string FONT_FAMILY_IS_UNKNOWN = "The font family for the icon does not match a known font family.";
            public const string NAME_MUST_BE_UNIQUE = "'Name' must be unique.";
            public const string NUMBER_OF_TAGS_ON_CHAPTER_SUMMARY_MUST_BE_GREATER_THEN_ZERO = "Number of tags to be displayed on chapter summary must be greater than 0.";
            public const string NUMBER_OF_TAGS_ON_VOLUME_SUMMARY_MUST_BE_GREATER_THEN_ZERO = "Number of tags to be displayed on volume summary must be greater than 0.";
            public const string NUMBER_OF_TAGS_ON_WORK_SUMMARY_MUST_BE_GREATER_THEN_ZERO = "Number of tags to be displayed on work summary must be greater than 0.";
            public const string SEARCHABLE_PREFIX_MUST_BE_UNIQUE = "'Searchable prefix' must be unique.";
            public const string TAGS_OF_THIS_TYPE_EXIST_ON_WORKS = "Tags of this tag type already exist on works. Unable to remove availability of tag type on work.";
            public const string TAGS_OF_THIS_TYPE_EXIST_ON_CHAPTERS = "Tags of this tag type already exist on chapters. Unable to remove availability of tag type on chapter.";
            public const string TAGS_OF_THIS_TYPE_EXIST_ON_VOLUMES = "Tags of this tag type already exist on volumes. Unable to remove availability of tag type on volume.";

        }
    }
}
