﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManicNet.Localization
{
    public class SocialLocalization
    {
        public const string CREATE_SOCIAL_LINK = "Create Social Link";
        public const string DELETED_SOCIAL_LINKS = "Deleted Social Links";
        public const string EDIT_SOCIAL_LINK = "Edit Social Link - {0}";
        public const string ERROR_CREATING_SOCIAL_LINK = "Error creating social link {0}.";
        public const string ERROR_EDITING_SOCIAL_LINK = "Error editing social link {0}.";
        public const string NO_SOCIAL_LINKS_FOUND = "No social links found in the database that match the provided parameters.";
        public const string SOCIAL_LINKS = "Social Links";
        public const string SOCIAL_LINK_ADMINISTRATION = "Social Link Administration";
        public const string SOCIAL_LINK_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A social link cannot be edited while in a deleted state. Restore the social link before attempting to edit it.";
        public const string SOCIAL_LINK_IS_NOT_DELETABLE = "Social link {0} is not in a deletable state.";
        public const string SOCIAL_LINK_TITLE = "Social Link - {0}";
        public const string SOCIAL_LINK_SUCCESSFULLY_CREATED = "Social link {0} was successfully created.";
        public const string SOCIAL_LINK_SUCCESSFULLY_DELETED = "Social Link {0} was successfully deleted.";
        public const string SOCIAL_LINK_SUCCESSFULLY_SOFT_DELETED = "Social link {0} was successfully soft deleted.";
        public const string SOCIAL_LINK_SUCCESSFULLY_UPDATED = "Social link {0} was successfully edited.";
        public const string SOCIAL_LINK_SUCCESSFULLY_RESTORED = "Social link {0} was successfully restored.";
        public const string UNABLE_TO_DELETE_SOCIAL_LINK = "Unable to delete social link {0}.";
        public const string UNABLE_TO_LOAD_SOCIAL_LINK_INFORMATION = "Unable to retrieve social link information";
        public const string UNABLE_TO_SOFT_DELETE_SOCIAL_LINK = "Unable to soft delete social link {0}.";
        public const string UNABLE_TO_RESTORE_SOCIAL_LINK = "Unable to restore social link {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_SOCIAL_LINKS = "Unable to retrieve list of deleted social links.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_SOCIAL_LINKS = "Unable to retrieve list of social links.";
        
        public class Validation
        {
            public const string FONT_FAMILY_IS_REQUIRED = "The font family for the icon must be specified.";
            public const string FONT_FAMILY_IS_UNKNOWN = "The font family for the icon does not match a known font family.";
            public const string INVALID_TYPE = "'Type' is not a valid social link type.";
        }

        public class LinkType
        {
            public const string EMAIL = "Email";
            public const string LINK = "Link";
            public const string UNKNOWN = "Unknown";
        }

        public class Help
        {
            public const string FONT_FAMILY = "The font awesome family that the icon belongs to.";
            public const string ICON = "The (unicode value of the) font awesome icon displayed with this social link.";
            public const string NAME = "The name of the social link.";
            public const string PRIORITY = "The order in which the social links are displayed.";
            public const string TEXT = "The link text displayed for the social link.";
            public const string TYPE = "The type of the given social link (email or link).";
            public const string URL = "The URL that the social link will point to.";
        }
    }
}
