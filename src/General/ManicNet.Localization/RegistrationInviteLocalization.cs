﻿namespace ManicNet.Localization
{
    public class RegistrationInviteLocalization
    {
        public const string A_REGISTRATION_INVITE_ALREADY_EXISTS = "A registration invite already exists with the email address {0}, please approve or re-approve the existing registration invite.";
        public const string A_REGISTRATION_INVITE_WAS_SUCCESSFULLY_CREATED = "A registration invite was successfully created for {0}.";
        public const string A_USER_ALREADY_EXISTS_WITH_THE_EMAIL_ADDRESS = "A user already exists with the email address {0}, a registration invite for that email address cannot be created.";
        public const string APPROVE = "Approve";
        public const string APPROVED_REGISTRATION_INVITES = "Approved Registration Invites";
        public const string CREATE_AN_INVITE_TO_A_NEW_ACCOUNT = "Create an invite to a new account.";
        public const string CREATE_INVITE = "Create Invite";
        public const string EXPIRED_REGISTRATION_INVITES = "Expired Registration Invites";
        public const string INVITE_ADMINISTRATION = "Invite Administration";
        public const string NO_REGISTRATION_INVITES_FOUND = "No registration invites are found in the database that match the provided parameters.";
        public const string PENDING_REGISTRATION_INVITES = "Pending Registration Invites";
        public const string RE_APPROVE = "Re-Approve";
        public const string REGISTRATION_INVITE_REQUEST_WAS_SUCCESSFULLY_APPROVED = "Registration invite request was successfully approved.";
        public const string REGISTRATION_INVITE_REQUEST_WAS_SUCCESSFULLY_DELETED = "Registration invite request was successfully deleted.";
        public const string REQUEST_INVITE = "Request Invite";
        public const string UNABLE_TO_APPROVE_REGISTRATION_INVITE_REQUEST = "Unable to approve registration invite request {0}.";
        public const string UNABLE_TO_CREATE_REGISTRATION_INVITE = "Unable to create registration invite.";
        public const string UNABLE_TO_DELETE_REGISTRATION_INVITE_REQUEST = "Unable to delete registration invite request {0}.";
        public const string UNABLE_TO_FIND_INVITE_REQUEST = "Unable to find invite request {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_APPROVED_REGISTRATION_INVITES = "Unable to retrieve list of approved registration invites.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_EXPIRED_REGISTRATION_INVITES = "Unable to retrieve list of expired registration invites.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_PENDING_REGISTRATION_INVITES = "Unable to retrieve list of pending registration invites.";
    }
}
