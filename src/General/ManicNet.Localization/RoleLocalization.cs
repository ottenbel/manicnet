﻿namespace ManicNet.Localization
{
    public class RoleLocalization
    {
        public const string CREATE_ROLE = "Create Role";
        public const string CREATED_ROLE = "Created role {0}";
        public const string DEFAULT_USER_ROLE = "Default User Role";
        public const string EDIT_ROLE = "Edit Role - {0}";
        public const string MAKE_DEFAULT = "Make Default";
        public const string NO_PERMISSIONS_ARE_ASSOCIATED_WITH_THIS_ROLE = "No permissions are associated with this role.";
        public const string NO_ROLES_FOUND = "No roles found in the database that match the provided parameters.";
        public const string PERMISSIONS = "Permissions";
        public const string ROLE_ADAMINISTRATION = "Role Administration";
        public const string ROLE_IS_ASSIGNED_TO_YOUR_ACCOUNT_AND_CANNOT_BE_DELETED = "{0} is a role assigned to your account and cannot be deleted";
        public const string ROLE_IS_DEFAULT_USER_ROLE_AND_CANNOT_BE_DELETED = "{0} is the default user role and cannot be deleted";
        public const string ROLE_INDEX = "Role Index";
        public const string ROLES = "Roles";
        public const string SUCCESSFULLY_ASSIGNED_ROLE_AS_DEFAULT_ROLE = "Successfully assigned role {0} as default role.";
        public const string SUCCESSFULLY_DELETED_ROLE = "Successfully deleted role {0}";
        public const string SHOW_ROLE = "Role - {0}";
        public const string UNABLE_TO_ADD_PERMISSION_TO_ROLE = "Unable to add permission {0} to role {1}.";
        public const string UNABLE_TO_CREATE_ROLE = "Unable to create role {0}";
        public const string UNABLE_TO_DELETE_ROLE = "Unable to delete role.";
        public const string UNABLE_TO_POPULATE_CREATE_ROLE_SCREEN = "Unable to populate create role screen.";
        public const string UNABLE_TO_POPULATE_EDIT_ROLE_SCREEN = "Unable to populate edit role screen.";
        public const string UNABLE_TO_REASSIGN_ROLE = "Unable to reassign role.";
        public const string UNABLE_TO_REMOVE_PERMISSION_FROM_ROLE = "Unable to remove permission {0} from role {1}.";
        public const string UNABLE_TO_RETRIEVE_ROLES = "Unable to retrieve list of roles.";
        public const string UNABLE_TO_RETRIEVE_ROLE_INFORMATION = "Unable to retrieve role information.";
        public const string UPDATED_ROLE = "Updated Role {0}";
        public const string UNABLE_TO_UPDATE_PERMISSIONS_FOR_ROLE = "Unable to update permissions for role {0}";
        public const string VIEW_ROLE = "View Role";

        public class Help
        {
            public static readonly string NAME = "The name of the role.";
            public static readonly string DESCRIPTION = "A general description of the role.";
        }
    }
}
