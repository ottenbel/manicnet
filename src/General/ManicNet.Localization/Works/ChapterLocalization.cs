﻿namespace ManicNet.Localization
{
    public class ChapterLocalization
    {
        public const string CHAPTER_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A chapter cannot be edited while in a deleted state. Restore the chapter before attempting to edit it.";
        public const string CHAPTER_IS_NOT_IN_DELETABLE_STATE = "Chapter {0} is not in a deletable state.";
        public const string CHAPTER_OVERVIEW = "Additional information about the chapter.";
        public const string CHAPTER_OVERVIEW_TITLE = "Chapter {0} - Overview";
        public const string CHAPTER_OVERVIEW_TITLE_WITH_TITLE = "Chapter {0} - {1} - Overview";
        public const string CHAPTER_VIEWER_TITLE = "Chapter {0} - Viewer";
        public const string CHAPTER_VIEWER_TITLE_WITH_TITLE = "Chapter {0} - {1} - Viewer";
        public const string CHAPTER_SUCCESSFULLY_CREATED = "Chapter {0} was successfully created.";
        public const string CHAPTER_SUCCESSFULLY_CREATED_WITH_TITLE = "Chapter {0} - {1} was successfully created.";
        public const string CHAPTER_SUCCESSFULLY_DELETED = "Chapter {0} was successfully deleted.";
        public const string CHAPTER_SUCCESSFULLY_MIGRATED = "Chapter {0} was successfully migrated to volume.";
        public const string CHAPTER_SUCCESSFULLY_RESTORED = "Chapter {0} was successfully restored.";
        public const string CHAPTER_SUCCESSFULLY_UPDATED = "Chapter {0} was successfully updated.";
        public const string CHAPTER_SUCCESSFULLY_UPDATED_WITH_TITLE = "Chapter {0} - {1} was successfully updated.";
        public const string CHAPTER_SUCCESSFULLY_SOFT_DELETED = "Chapter number {0} was successfully soft deleted.";
        public const string CREATE_CHAPTER = "Create Chapter";
        public const string CREATED_CHAPTER_WITH_PROBLEMS = "Created chapter, but problems occurred.";
        public const string DELETED_CHAPTERS = "Deleted Chapters";
        public const string EDIT_CHAPTER = "Edit Chapter {0}";
        public const string EDIT_CHAPTER_WITH_TITLE = "Edit Chapter {0} - {1}";
        public const string ERROR_ADDING_CHAPTER_FOR_NON_EXISTENT_VOLUME = "Error adding chapter for non-existent volume {0}.";
        public const string ERROR_CREATING_CHAPTER = "Error creating chapter for volume {0}.";
        public const string ERROR_EDITING_CHAPTER = "Error editing chapter {0}.";
        public const string ERROR_EDITING_NON_EXISTENT_CHAPTER = "Error editing non-existent chapter {0}.";
        public const string ERROR_EDITING_CHAPTER_WITH_TITLE = "Error editing chapter {0} - {1}.";
        public const string LAST_PAGE_OF_PREVIOUS_CHAPTER = "Last Page of Previous Chapter";
        public const string ERROR_RESTORING_NON_EXISTENT_CHAPTER = "Error restoring non-existent chapter {0}.";
        public const string MIGRATE_CHAPTER = "Migrate Chapter - {0}";
        public const string MIGRATE_CHAPTER_WITH_TITLE = "Migrate Chapter - {0} - {1}";
        public const string NEXT_CHAPTER = "Next Chapter";
        public const string NEXT_PAGE = "Next Page";
        public const string NO_CHAPTERS_FOUND = "No chapters found in the database that match the provided parameters.";
        public const string OVERVIEW = "Overview";
        public const string PAGE_NUMBER = "Page {0}";
        public const string PARENT_WORK = "Parent Work";
        public const string PREVIOUS_CHAPTER = "Previous Chapter";
        public const string PREVIOUS_PAGE = "Previous Page";
        public const string TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_CHAPTER = "<strong>{0}</strong> is of a type that cannot be be added to a chapter.";
        public const string TOGGLE_CHAPTER_SUMMARY = "Toggle Chapter Summary";
        public const string UNABLE_TO_DELETE_CHAPTER = "Unable to delete chapter {0}.";
        public const string UNABLE_TO_DOWNLOAD_CHAPTER = "Unable to download chapter.";
        public const string UNABLE_TO_LOAD_INFORMATION_FOR_CHAPTER = "Unable to load information for chapter.";
        public const string UNABLE_TO_MIGRATE_CHAPTER = "Unable to migrate chapter {0}.";
        public const string UNABLE_TO_MIGRATE_CHAPTER_DESTINATION_VOLUME_DOES_NOT_EXIST = "Unable to migrate chapter. Destination volume {0} does not exist.";
        public const string UNABLE_TO_MIGRATE_CHAPTER_DESTINATION_VOLUME_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO = "Unable to migrate chapter. Destination volume {0} is not in a state to be migrated to.";
        public const string UNABLE_TO_RESTORE_CHAPTER = "Unable to restore chapter {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_CHAPTERS = "Unable to retrieve list of deleted chapters.";
        public const string UNABLE_TO_SOFT_DELETE_CHAPTER = "Unable to soft delete chapter {0}.";
        public const string UPDATED_CHAPTER_WITH_PROBLEMS = "Updated chapter, but problems occurred.";
        public const string VIEWER = "Viewer";
        public const string VOLUME_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_CHAPTERS = "Volume {0} must be active before adding associated chapters. Please restore the volume before continuing.";
        public const string VOLUME_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_CHAPTERS = "Volume {0} must be active before editing associated chapters. Please restore the volume before continuing.";
        public const string VOLUME_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_CHAPTERS = "Volume {0} must be active before restoring associated chapters. Please restore the volume before continuing.";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_CHAPTERS = "{0} must be active before adding associated chapters. Please restore the work before continuing.";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_CHAPTERS = "{0} must be active before editing associated chapters. Please restore the work before continuing.";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_CHAPTERS = "{0} must be active before restoring associated chapters. Please restore the work before continuing.";

        public class Help
        {
            public const string NUMBER = "The number of the chapter within the parent work.";
            public const string TITLE = "The title of the chapter.";
            public const string SHORT_DESCRIPTION = "A short description of the chapter.";
            public const string LONG_DESCRIPTION = "A detailed description of the chapter.";
            public const string TAGS = "A comma delimited list of tags which describe the chapter.";
            public const string FILES = "Files to be uploaded as pages of the chapter.";
            public const string URL = "A URL providing additional information about the chapter.";

            //Migration
            public const string MIGRATE_CHAPTER_INSIDE_OF_WORK = "The destination volume that the chapter will be migrated to.";
            public const string MIGRATE_CHAPTER_OUTSIDE_OF_WORK = "The identifier of the volume that the chapter is migrated to.";
            public const string MIGRATE_CHAPTER_INSIDE_OR_OUTSIDE_OF_WORK = "Is the chapter migrated to a volume outside of the current work or a volume inside of the current work.";
            public const string MIGRATE_CHAPTER_NUMBER = "The number of the chapter within the parent work.";
        }

        public class Validation
        {
            public const string PAGES_ARE_REQUIRED = "'Pages' are required.";
        }
    }
}
