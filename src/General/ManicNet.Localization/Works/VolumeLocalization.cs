﻿namespace ManicNet.Localization
{
    public class VolumeLocalization
    {
        public const string ADD_CHAPTER = "Add Chapter";
        public const string CREATE_VOLUME = "Create Volume";
        public const string CREATED_VOLUME_WITH_PROBLEMS = "Created volume, but problems occurred.";
        public const string EDIT_VOLUME = "Edit Volume {0}";
        public const string EDIT_VOLUME_WITH_TITLE = "Edit Volume {0} - {1}";
        public const string EDITED_VOLUME_WITH_PROBLEMS = "Edited volume, but problems occurred.";
        public const string ERROR_ADDING_VOLUME_FOR_NON_EXISTENT_WORK = "Error adding volume for non-existent work {0}.";
        public const string ERROR_CREATING_VOLUME = "Error creating volume {0}.";
        public const string ERROR_CREATING_VOLUME_WITH_TITLE = "Error creating volume {0} - {1}.";
        public const string ERROR_CREATING_VOLUME_FOR_WORK = "Error creating volume for work {0}.";
        public const string ERROR_EDITING_VOLUME = "Error editing volume number {0}.";
        public const string ERROR_EDITING_VOLUME_FOR_NON_EXISTENT_WORK = "Error editing volume for non-existent work {0}.";
        public const string ERROR_RESTORING_VOLUME_FOR_NON_EXISTENT_WORK = "Error restoring volume for non-existent work {0}.";
        public const string DELETED_VOLUMES = "Deleted Volumes";
        public const string MIGRATE_VOLUME = "Migrate Volume - {0}";
        public const string NO_VOLUMES_FOUND = "No volumes found in the database that match the provided parameters.";
        public const string PARENT_WORK = "Parent Work";
        public const string TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_VOLUME = "<strong>{0}</strong> is of a type that cannot be be added to a volume.";
        public const string TOGGLE_VOLUME_SUMMARY = "Toggle Volume Summary";
        public const string UNABLE_TO_DELETE_VOLUME = "Unable to delete volume {0}.";
        public const string UNABLE_TO_DOWNLOAD_VOLUME = "Unable to download volume.";
        public const string UNABLE_TO_LOAD_VOLUME_INFORMATION = "Unable to load information for volume.";
        public const string UNABLE_TO_MIGRATE_VOLUME = "Unable to migrate volume {0}.";
        public const string UNABLE_TO_MIGRATE_VOLUME_DESTINATION_WORK_DOES_NOT_EXIST = "Unable to migrate volume {0}. Destination work {1} is not in a state to be migrated to.";
        public const string UNABLE_TO_MIGRATE_VOLUME_DESTINATION_WORK_IS_NOT_IN_A_STATE_TO_BE_MIGRATED_TO = "Unable to migrate volume {0}. Destination work {1} is not in a state to be migrated to.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_VOLUMES = "Unable to retrieve list of deleted volumes.";
        public const string UNABLE_TO_RESTORE_VOLUME = "Unable to restore volume {0}.";
        public const string UNABLE_TO_SOFT_DELETE_VOLUME = "Unable to soft delete volume {0}.";
        public const string VOLUME_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A volume cannot be edited while in a deleted state. Restore the volume before attempting to edit it.";
        public const string VOLUME_IS_NOT_IN_DELETABLE_STATE = "Volume {0} is not in a deletable state.";
        public const string VOLUME_SUCCESSFULLY_CREATED = "Volume {0} was successfully created.";
        public const string VOLUME_SUCCESSFULLY_CREATED_WITH_TITLE = "Volume {0} - {1} was successfully created.";
        public const string VOLUME_SUCCESSFULLY_DELETED = "Volume number {0} was successfully deleted.";
        public const string VOLUME_SUCCESSFULLY_EDITED = "Volume number {0} was successfully edited.";
        public const string VOLUME_SUCCESSFULLY_MIGRATED = "Volume {0} was successfully migrated to work {1}.";
        public const string VOLUME_SUCCESSFULLY_RESTORED = "Volume number {0} was successfully restored.";
        public const string VOLUME_SUCCESSFULLY_SOFT_DELETED = "Volume number {0} was successfully soft deleted.";
        public const string VOLUME_TITLE = "Volume {0}";
        public const string VOLUME_TITLE_WITH_TITLE = "Edit Volume {0} - {1}";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_VOLUMES = "{0} must be active before adding associated volumes. Please restore the work before continuing.";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_VOLUMES = "{0} must be active before editing associated volumes. Please restore the work before continuing.";
        public const string WORK_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_VOLUMES = "{0} must be active before restoring associated volumes. Please restore the work before continuing.";

        public class Help
        {
            public static readonly string COVER = "A cover image for the volume.";
            public static readonly string NUMBER = "The number of the volume within the parent work.";
            public static readonly string TITLE = "The title of the volume.";
            public static readonly string SHORT_DESCRIPTION = "A short description of the volume.";
            public static readonly string LONG_DESCRIPTION = "A detailed description of the volume.";
            public static readonly string URL = "A URL providing additional information about the volume.";
            public static readonly string TAGS = "A comma delimited list of tags which describe the volume.";
            public static readonly string MIGRATE_VOLUME = "The identifier of the work that the volume is migrated to.";
        }
    }
}
