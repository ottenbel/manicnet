﻿namespace ManicNet.Localization
{
    public class WorkLocalization
    {
        public const string ADD_VOLUME = "Add Volume";
        public const string CREATE_WORK = "Create Work";
        public const string CREATED_WORK_WITH_PROBLEMS = "Created work {0}, but problems occurred.";
        public const string DELETED_WORKS = "Deleted Works";
        public const string EDIT_WORK = "Edit Work - {0}";
        public const string EDITED_WORK_WITH_PROBLEMS = "Edited work {0}, but problems occurred.";
        public const string ERROR_CREATING_WORK = "Error creating work {0}.";
        public const string ERROR_EDITING_WORK = "Error editing work {0}.";
        public const string NO_WORKS_FOUND = "No works found in the database that match the provided parameters.";
        public const string RECENTLY_ADDED_WORK_TYPES = "Recently Added Work Types";
        public const string RECENTLY_ADDED_WORK_TYPES_DESCRIPTION = "{0} - Recently Added Work Types Feed";
        public const string RECENTLY_ADDED_WORK_TYPES_DESCRIPTION_WITH_SEARCH = "{0} - Recently Added Work Types For {1} Feed";
        public const string RECENTLY_UPDATED_WORK_TYPES = "Recently Updated Work Types";
        public const string RECENTLY_UPDATED_WORK_TYPES_DESCRIPTION = "{0} - Recently Updated Work Types Feed";
        public const string RECENTLY_UPDATED_WORK_TYPES_DESCRIPTION_WITH_SEARCH = "{0} - Recently Updated Work Types For {1} Feed";
        public const string TAG_IS_OF_TYPE_THAT_CANNOT_BE_ADDED_TO_WORK = "<strong>{0}</strong> is of a type that cannot be be added to a work.";
        public const string TOGGLE_COLLECTION_SUMMARY = "Toggle Collection Summary";
        public const string UNABLE_TO_DELETE_WORK = "Unable to delete work {0}.";
        public const string UNABLE_TO_LOAD_INFORMATION_FOR_RANDOM_WORK = "Unable to load information for random work.";
        public const string UNABLE_TO_DOWNLOAD_WORK = "Unable to download work.";
        public const string UNABLE_TO_LOAD_INFORMATION_FOR_WORK = "Unable to load information for work.";
        public const string UNABLE_TO_RESTORE_WORK = "Unable to restore work {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_WORKS = "Unable to retrieve list of deleted works.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_WORKS = "Unable to retrieve list of works.";
        public const string UNABLE_TO_SOFT_DELETE_WORK = "Unable to soft delete work {0}.";
        public const string VOLUMES = "Volumes";
        public const string WORK_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE = "A work cannot be edited while in a deleted state. Restore the work before attempting to edit it.";
        public const string WORK_CHAPTER_UPDATE_FEED = "{0} - {1} Chapter Update Feed";
        public const string WORK_COPYRIGHT = "{0} - {1}";
        public const string WORK_FEED_CHAPTER = "{0} - Chapter {1}";
        public const string WORK_FEED_CHAPTER_WITH_TITLE = "{0} - Chapter {1} - {2}";
        public const string WORK_TITLE = "Work - {0}";
        public const string WORK_IS_NOT_IN_DELETABLE_STATE = "Work {0} is not in a deletable state.";
        public const string WORK_SUCCESSFULLY_CREATED = "Work {0} was successfully created.";
        public const string WORK_WAS_SUCCESSFULLY_DELETED = "Work {0} was successfully deleted.";
        public const string WORK_SUCCESSFULLY_EDITED = "Work {0} was successfully edited.";
        public const string WORK_WAS_SUCCESSFULLY_RESTORED = "Work {0} was successfully restored.";
        public const string WORK_SUCCESSFULLY_SOFT_DELETED = "Work {0} was successfully soft deleted.";
        public const string WORK_ADMINISTRATION = "Work Administration";
        public const string WORKS = "Works";

        public class Help
        {
            public const string COVER = "A cover image for the work.";
            public const string TITLE = "The title of the work.";
            public const string SHORT_DESCRIPTION = "A short description of the work.";
            public const string LONG_DESCRIPTION = "A detailed description of the work.";
            public const string URL = "A URL providing additional information about the work.";
            public const string TAGS = "A comma delimited list of tags which describe the work.";
        }
    }
}
