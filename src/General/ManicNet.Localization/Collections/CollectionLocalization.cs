﻿namespace ManicNet.Localization
{
    public class CollectionLocalization
    {
        public const string ADD_WORK = "Add Work";
        public const string ADD_WORK_TO_COLLECTION = "Add Work to Collection";
        public const string BACK_TO_COLLECTION = "Back to Collection";
        public const string COLLECTION_ADMINISTRATION = "Collection Administration";
        public const string COLLECTION_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_WORKS = "{0} must be active before adding associated works. Please restore the collection before continuing.";
        public const string COLLECTION_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_WORKS = "{0} must be active before editing associated works. Please restore the collection before continuing.";
        public const string COLLECTIONS = "Collections";
        public const string COLLECTION_CANNOT_BE_EDITED_WHIL_IN_A_DELETED_STATE = "A collection cannot be edited while in a deleted state. Restore the collection before attempting to edit it.";
        public const string COLLECTION_IS_NOT_DELETABLE = "Collection {0} is not in a deletable state.";
        public const string COLLECTION_SUCCESSFULLY_CREATED = "Collection {0} was successfully created.";
        public const string COLLECTION_SUCCESSFULLY_DELETED = "Collection {0} was successfully deleted.";
        public const string COLLECTION_SUCCESSFULLY_EDITED = "Collection {0} was successfully edited.";
        public const string COLLECTION_SUCCESSFULLY_RESTORED = "Collection {0} was successfully restored.";
        public const string COLLECTION_SUCCESSFULLY_SOFT_DELETED = "Collection {0} was successfully soft deleted.";
        public const string COLLECTION_TITLE = "Collection - {0}";
        public const string CREATE_COLLECTION = "Create Collection";
        public const string CREATE_NEW_COLLECTION = "Create New Collection";
        public const string DELETED_COLLECTIONS = "Deleted Collections";
        public const string EDIT_COLLECTION = "Edit Collection - {0}";
        public const string ERROR_ADDING_WORK_TO_COLLECTION = "Error adding work {0} to collection {1}.";
        public const string ERROR_ADDING_WORK_TO_NON_EXISTENT_COLLECTION = "Error adding work for non-existent collection {0}.";
        public const string ERROR_CREATING_COLLECTION = "Error creating collection {0}.";
        public const string ERROR_EDITING_COLLECTION = "Error editing collection {0}.";
        public const string ERROR_EDITING_WORK_TO_NON_EXISTENT_COLLECTION = "Error editing work for non-existent collection {0}.";
        public const string NO_COLLECTIONS_FOUND = "No collections found in the database that match the provided parameters.";
        public const string TOGGLE_WORK_SUMMARY = "Toggle Work Summary";
        public const string UNABLE_TO_DELETE_COLLECTION = "Unable to delete collection {0}.";
        public const string UNABLE_TO_LOAD_COLLECTION_INFORMATION = "Unable to load information for collection.";
        public const string UNABLE_TO_RESTORE_COLLECTION = "Unable to restore collection {0}.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_COLLECTIONS = "Unable to retrieve list of collections.";
        public const string UNABLE_TO_RETRIEVE_LIST_OF_DELETED_COLLECTIONS = "Unable to retrieve list of deleted collections.";
        public const string UNABLE_TO_SOFT_DELETE_COLLECTION = "Unable to soft delete collection {0}.";
        public const string WORK_WAS_SUCCESSFULLY_ADDED_TO_COLLECTION = "Work was successfully added to collection.";

        public class Help
        {
            //Collection
            public const string TITLE = "The title of the collection.";
            public const string SHORT_DESCRIPTION = "A short description of the collection.";
            public const string LONG_DESCRIPTION = "A detailed description of the collection.";
            public const string URL = "A URL providing additional information about the collection.";

            //Collection work
            public const string WORK_ID = "The identifier of the work to associate with the collection.";
            public const string NUMBER = "The number of the work in the collection.";
        }

        public class Validation
        {
            public const string WORK_ALREADY_EXISTS_ON_COLLECTION = "'Work' already exists on collection.";
            public const string WORK_MUST_EXIST = "'Work' must exist and be active.";
        }
    }
}
