﻿namespace ManicNet.Localization
{
    public class UserLocalization
    {
        public const string ADMIN_USER_INDEX = "Admin User Index";
        public const string EDIT_USER = "Edit User - {0}";
        public const string MEMBER_ADMINISTRATION = "Member Administration";
        public const string NO_USERS_FOUND = "No users found in the database that match the provided parameters.";
        public const string PERMISSIONS = "Permissions";
        public const string UNABLE_TO_ADD_PERMISSIONS_TO_USER = "Unable to add permissions to user {0}.";
        public const string UNABLE_TO_ADD_ROLES_TO_USER = "Unable to add roles to user {0}.";
        public const string UNABLE_TO_FIND_USER_WITH_USER_ID = "Unable to find user with user ID: {0}.";
        public const string UNABLE_TO_POPULATE_ADMINISTRATION_USER_EDIT_PAGE = "Unable to populate administration user edit page for user.";
        public const string UNABLE_TO_POPULATE_ADMINISTRATION_USER_INDEX = "Unable to populate administration user index.";
        public const string UNABLE_TO_REMOVE_PERMISSIONS_FROM_USER = "Unable to remove permissions from user {0}.";
        public const string UNABLE_TO_REMOVE_ROLES_FROM_USER = "Unable to remove roles from user {0}.";
        public const string UNABLE_TO_RETRIEVE_INFORMATION_ABOUT_THE_EDITING_USER = "Unable to retrieve information about the editing user.";
        public const string UNABLE_TO_RETRIEVE_INFORMATION_ABOUT_THE_USER_TO_BE_EDITED = "Unable to retrieve information about the user to be edited";
        public const string UNABLE_TO_UPDATE_USER_PERMISSIONS = "Unable to update user permissions for {0}.";
        public const string USER_ADMINISTRATION = "User Administration";
        public const string USER_PERMISSIONS_SUCCESSFULLY_UPDATED = "User permissions successfully updated for {0}.";
    }
}
