﻿namespace ManicNet.Email.Models
{
    public sealed class AccountRegistrationEmailModel
    {
        public string Username { get; set; }
        public string AccountConfirmationUrl { get; set; }
        public string SiteName { get; set; }
    }
}
