﻿using System;

namespace ManicNet.Email.Models
{
    public sealed class RegistrationInviteEmailModel
    {
        public Guid RegistrationInviteId { get; set; }
        public DateTime? Expires { get; set; }
        public string SiteName { get; set; }
    }
}
