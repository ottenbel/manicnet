﻿namespace ManicNet.Email.Models
{
    public sealed class EmailConfirmationEmailModel
    {
        public string Username { get; set; }
        public string AccountConfirmationUrl { get; set; }
        public string SiteName { get; set; }
    }
}
