﻿namespace ManicNet.Email.Models
{
    public sealed class ResetPasswordEmailModel
    {
        public string Username { get; set; }
        public string PasswordResetUrl { get; set; }
        public string SiteName { get; set; }
    }
}