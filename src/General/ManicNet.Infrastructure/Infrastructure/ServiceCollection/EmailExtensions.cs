﻿using ManicNet.Domain.Models;
using ManicNet.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Reflection;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class EmailExtensions
    {
        public static IServiceCollection AddCustomEmailHandling(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            if (string.Equals(settings.EmailSettings.EmailType, Constants.Email.Types.NONE, StringComparison.OrdinalIgnoreCase))
            {
                logger.LogInformation("Startup: Email handler in settings set to none (outgoing emails will be swallowed).");
            }
            else if (string.Equals(settings.EmailSettings.EmailType, Constants.Email.Types.SMTP, StringComparison.OrdinalIgnoreCase))
            {
                services.AddSMTPEmailHandling(settings, logger);
            }
            else if (string.Equals(settings.EmailSettings.EmailType, Constants.Email.Types.SENDGRID, StringComparison.OrdinalIgnoreCase))
            {
                services.AddSendGridEmailHandling(settings, logger);
            }
            else if (string.Equals(settings.EmailSettings.EmailType, Constants.Email.Types.MAILGUN, StringComparison.OrdinalIgnoreCase))
            {
                services.AddMailgunEmailHandling(settings, logger);
            }
            else
            {
                logger.LogInformation("Startup: Email handler in settings not recognized, set to none.");
            }

            return services;
        }

        public static IServiceCollection AddSMTPEmailHandling(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Registering SMTP server as email handler.");

            var projectRoot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            services.AddFluentEmail(settings.EmailSettings.SentAs)
                .AddRazorRenderer(projectRoot)
                .AddSmtpSender(settings.EmailSettings.SMTP.Host, settings.EmailSettings.SMTP.Port, settings.EmailSettings.SMTP.Username, settings.EmailSettings.SMTP.Password);

            return services;
        }

        public static IServiceCollection AddSendGridEmailHandling(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Registering SendGrid as email handler.");

            var projectRoot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            services.AddFluentEmail(settings.EmailSettings.SentAs)
                .AddRazorRenderer(projectRoot)
                .AddSendGridSender(settings.EmailSettings.SendGrid.ApiKey, settings.EmailSettings.SendGrid.SandboxMode);

            return services;
        }

        public static IServiceCollection AddMailgunEmailHandling(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Registering Mailgun as email handler.");

            var projectRoot = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            services.AddFluentEmail(settings.EmailSettings.SentAs)
                .AddRazorRenderer(projectRoot)
                .AddMailGunSender(settings.EmailSettings.Mailgun.DomainName, settings.EmailSettings.Mailgun.ApiKey, MailgunRegionHelper.Convert(settings.EmailSettings.Mailgun.Region));

            return services;
        }
    }
}
