﻿using ManicNet.Context;
using ManicNet.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class DatabaseExtensions
    {
        /// <summary>
        /// Configure the database.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="settings"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static IServiceCollection ConfigureDatabse(this IServiceCollection services, IConfiguration configuration, ManicNetSettings settings, ILogger logger)
        {
            if (string.Equals(settings.DatabaseSettings.DatabaseType, Constants.Database.MSSQL, StringComparison.OrdinalIgnoreCase))
            {
                services.ConfigureMSSQLDatabseProvider(configuration, logger);
            }
            else if (string.Equals(settings.DatabaseSettings.DatabaseType, Constants.Database.MYSQL, StringComparison.OrdinalIgnoreCase))
            {
                services.ConfigureMySQLDatabseProvider(configuration, settings, logger);
            }
            else if (string.Equals(settings.DatabaseSettings.DatabaseType, Constants.Database.MARIADB, StringComparison.OrdinalIgnoreCase))
            {
                services.ConfigureMariaDBDatabseProvider(configuration, settings, logger);
            }
            else if (string.Equals(settings.DatabaseSettings.DatabaseType, Constants.Database.POSTGRESQL, StringComparison.OrdinalIgnoreCase))
            {
                services.ConfigurePostgreSQLDatabseProvider(configuration, logger);
            }
            else
            {
                logger.LogError("Startup: Unknown database provider.");
                throw new Exception("Unknown database provider");
            }
            return services;
        }

        /// <summary>
        /// Configure default microsoft sql provider.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        private static IServiceCollection ConfigureMSSQLDatabseProvider(this IServiceCollection services, IConfiguration configuration, ILogger logger)
        {
            logger.LogInformation("Startup: Registering MSSQL as database server.");

            services.AddDbContext<ManicNetContext>(options =>
                options.UseSqlServer(
                configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly("ManicNet.MSSQLMigrations")));

            return services;
        }

        private static IServiceCollection ConfigureMySQLDatabseProvider(this IServiceCollection services, IConfiguration configuration, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Registering MySQL as database server.");

            MySqlServerVersion version = new MySqlServerVersion(new Version(settings.DatabaseSettings.MYSQL.ServerVersion.Major, settings.DatabaseSettings.MYSQL.ServerVersion.Minor, settings.DatabaseSettings.MYSQL.ServerVersion.Build));

            services.AddDbContext<ManicNetContext>(
                options => options.UseMySql(configuration.GetConnectionString("DefaultConnection"), version, x => x.MigrationsAssembly("ManicNet.MYSQLMigrations")));

            return services;
        }

        private static IServiceCollection ConfigureMariaDBDatabseProvider(this IServiceCollection services, IConfiguration configuration, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Registering MariaDB as database server.");

            MariaDbServerVersion version = new MariaDbServerVersion(new Version(settings.DatabaseSettings.MYSQL.ServerVersion.Major, settings.DatabaseSettings.MYSQL.ServerVersion.Minor, settings.DatabaseSettings.MYSQL.ServerVersion.Build));

            services.AddDbContext<ManicNetContext>(
                options => options.UseMySql(configuration.GetConnectionString("DefaultConnection"), version, x => x.MigrationsAssembly("ManicNet.MariaDBMigrations")));

            return services;
        }

        private static IServiceCollection ConfigurePostgreSQLDatabseProvider(this IServiceCollection services, IConfiguration configuration, ILogger logger)
        {
            logger.LogInformation("Startup: Registering PostgreSQL as database server.");

            services.AddDbContext<ManicNetContext>(
                options => options.UseNpgsql(configuration.GetConnectionString("DefaultConnection"), x => x.MigrationsAssembly("ManicNet.PostgreSQLMigrations")));

            return services;
        }
    }
}
