﻿using ManicNet.Constants;
using ManicNet.Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class CachingExtensions
    {
        public static IServiceCollection AddCustomCaching(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            if (string.Equals(settings.CacheSettings.CacheType, Caching.Types.MEMORY_CACHE, StringComparison.OrdinalIgnoreCase))
            {
                logger.LogInformation("Startup: Caching provider set to memory cache from settings.");
                services.AddDistributedMemoryCache();
            }
            else if (string.Equals(settings.CacheSettings.CacheType, Caching.Types.SQL_SERVER_CACHE, StringComparison.OrdinalIgnoreCase))
            {
                services.AddConfiguredDistributedSqlServerCache(settings, logger);
            }
            else if (string.Equals(settings.CacheSettings.CacheType, Caching.Types.REDIS_CACHE, StringComparison.OrdinalIgnoreCase))
            {
                services.AddConfiguredRedisCache(settings, logger);
            }
            else
            {
                logger.LogInformation("Startup: Caching provider value in settings not recognized, set to memory cache.");
                services.AddDistributedMemoryCache();
            }

            return services;
        }

        /// <summary>
        /// Set up the distributed SQL server cache for site caching.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="settings"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static IServiceCollection AddConfiguredDistributedSqlServerCache(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Caching provider set to SQL server cache from settings.");
            services.AddDistributedSqlServerCache(options =>
            {
                options.ConnectionString = settings.CacheSettings.SQLServer.ConnectionString;
                options.SchemaName = settings.CacheSettings.SQLServer.SchemaName;
                options.TableName = settings.CacheSettings.SQLServer.TableName;
            });

            return services;
        }

        /// <summary>
        /// Set up Redis cache for site caching.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="settings"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public static IServiceCollection AddConfiguredRedisCache(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Caching provider set to Redis cache from settings.");
            services.AddStackExchangeRedisCache(options =>
            {
                options.Configuration = settings.CacheSettings.RedisCache.Configuration;
                options.InstanceName = settings.CacheSettings.RedisCache.InstanceName;
            });

            return services;
        }
    }
}