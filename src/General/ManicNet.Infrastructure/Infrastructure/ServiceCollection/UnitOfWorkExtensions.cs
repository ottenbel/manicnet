﻿using ManicNet.Contracts.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class UnitOfWorkExtensions
    {
        /// <summary>
        /// Register the unit of work with the service collection.
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddCustomUnitOfWork(this IServiceCollection services)
        {
            //Unit of work mappings
            services.AddScoped<IManicNetUnitOfWork, ManicNetUnitOfWork.ManicNetUnitOfWork>();

            return services;
        }
    }
}
