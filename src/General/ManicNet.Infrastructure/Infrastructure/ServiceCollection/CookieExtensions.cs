﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class CookieExtensions
    {
        public static IServiceCollection ConfigureCookies(this IServiceCollection services)
        {
            services.ConfigureCookiePolicy();
            services.ConfigureApplicationCookie();

            return services;
        }

        /// <summary>
        /// Configure cookie policies.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection ConfigureCookiePolicy(this IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            return services;
        }

        /// <summary>
        /// Configure site application cookies.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection ConfigureApplicationCookie(this IServiceCollection services)
        {
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });

            return services;
        }
    }
}
