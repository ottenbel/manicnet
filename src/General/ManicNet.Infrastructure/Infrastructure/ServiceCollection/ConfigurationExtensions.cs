﻿using ManicNet.Domain.Models;
using ManicNet.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class ConfigurationExtensions
    {
        public static IServiceCollection ConfigureCustomServices(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            services.AddCustomCaching(settings, logger);
            services.AddCustomPolicies();
            services.AddCustomPermissionHandlers();
            services.AddCustomUnitOfWork();
            services.AddCustomRepositories();
            services.AddCustomServices();
            services.AddCustomEmailHandling(settings, logger);
            services.AddCustomFileHandling(settings, logger);

            //Custom user manager https://stackoverflow.com/questions/30453567/how-to-register-custom-userstore-usermanager-in-di
            services.AddScoped<ManicNetUserManager>();

            return services;
        }
    }
}
