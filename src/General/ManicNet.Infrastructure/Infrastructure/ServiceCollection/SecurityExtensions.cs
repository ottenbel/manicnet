﻿using ManicNet.Constants;
using ManicNet.Services.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.DependencyInjection;
using static ManicNet.Constants.ManicNetPermissions;

namespace ManicNet.Infrastructure.ServiceCollection
{
    /// <summary>
    /// A class containing extension methods used to clean up the Startup.cs file by grouping common 
    /// security functionality into simple methods that can be called during setup.
    /// </summary>
    public static class SecurityExtensions
    {
        /// <summary>
        ///  Register all defined policies with the service collection.
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddCustomPolicies(this IServiceCollection services)
        {
            services.AddAuthorization(options =>
            {
                options.AddCustomUserGenericElevatedPolicies();
                options.AddCustomUserAdministrationPolicies();
                options.AddCustomRoleAdministrationPolicies();
                options.AddGeneralTagPolicies();
                options.AddCustomRegistrationInvitationPolicies();
                options.AddCustomWorkPolicies();
                options.AddCustomCollectionPolicies();
                options.AddCustomConfigurationPolicies();
                options.AddCustomSocialLinkPolicies();
            });

            return services;
        }

        /// <summary>
        /// Register all generic policies marking a user with elevated permissions.
        /// </summary>
        /// <param name="options"></param>
        private static void AddCustomUserGenericElevatedPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.ELEVATED_SITE_PERMISSION,
                    policy => policy.Requirements.Add(new AdministrationRequirement()));
        }

        /// <summary>
        /// Register all policies related to managing users.
        /// </summary>
        /// <param name="options"></param>
        private static void AddCustomUserAdministrationPolicies(this AuthorizationOptions options)
        {
            //Generic 
            options.AddPolicy(Policies.Generic.MANAGE_USER_PERMISSION,
                policy => policy.Requirements.Add(new UserAdministrationRequirement()));

            //Specific user edit permissions
            options.AddPolicy(Policies.Owner.EDIT_OWNER_PERMISSIONS,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_OWNER_PERMISSIONS)));
            options.AddPolicy(Policies.Owner.EDIT_ADMINISTRATOR_PERMISSIONS,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_ADMINISTRATOR_PERMISSIONS)));
            options.AddPolicy(Policies.Owner.EDIT_USER_PERMISSIONS,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_USER_PERMISSIONS)));

            //Specific user role edit permissions
            options.AddPolicy(Policies.Owner.EDIT_OWNER_ROLES,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_OWNER_ROLES)));
            options.AddPolicy(Policies.Owner.EDIT_ADMINISTRATOR_ROLES,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_ADMINISTRATOR_ROLES)));
            options.AddPolicy(Policies.Owner.EDIT_USER_ROLES,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(User.Owner.EDIT_USER_ROLES)));
        }

        private static void AddCustomRoleAdministrationPolicies(this AuthorizationOptions options)
        {
            //Generic 
            options.AddPolicy(Policies.Generic.MANAGE_ROLE_PERMISSION,
                    policy => policy.Requirements.Add(new RoleAdministrationRequirement()));

            //Specific role edit permissions
            options.AddPolicy(Role.Owner.EDIT_ROLES,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Role.Owner.EDIT_ROLES)));
            options.AddPolicy(Role.Owner.DELETE_ROLES,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Role.Owner.DELETE_ROLES)));
            options.AddPolicy(Role.Owner.REASSIGN_DEFAULT_USER_ROLE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Role.Owner.REASSIGN_DEFAULT_USER_ROLE)));
        }

        /// <summary>
        /// Register all policies related to the managment of tags and related objects.
        /// </summary>
        /// <param name="options"></param>
        private static void AddGeneralTagPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_TAG_GENERIC,
                policy => policy.Requirements.Add(new GenericTagAdministrationRequirement()));

            options.AddCustomTagTypePolicies();
            options.AddCustomTagPolicies();
        }

        /// <summary>
        /// Register all policies related to managing tag types.
        /// </summary>
        /// <param name="options"></param>
        private static void AddCustomTagTypePolicies(this AuthorizationOptions options)
        {
            //Generic
            options.AddPolicy(Policies.Generic.MANAGE_TAG_TYPE,
                policy => policy.Requirements.Add(new TagTypeAdministrationRequirement()));

            //Specific tag type policies
            options.AddPolicy(Policies.Owner.DELETE_TAG_TYPE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(TagType.Owner.DELETE_TAG_TYPE)));
            options.AddPolicy(Policies.Administrator.EDIT_TAG_TYPE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(TagType.Administrator.EDIT_TAG_TYPE)));
            options.AddPolicy(Policies.Administrator.VIEW_DELETED_TAG_TYPE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(TagType.Administrator.VIEW_DELETED_TAG_TYPE)));
        }

        /// <summary>
        /// Register all policies related to managing tags.
        /// </summary>
        /// <param name="options"></param>
        private static void AddCustomTagPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_TAG,
                policy => policy.Requirements.Add(new TagAdministrationRequirement()));

            //Specific tag type policies
            options.AddPolicy(Policies.Owner.DELETE_TAG,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Tag.Owner.DELETE_TAG)));
            options.AddPolicy(Policies.Owner.MIGRATE_TAG,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Tag.Owner.MIGRATE_TAG)));
            options.AddPolicy(Policies.Administrator.EDIT_TAG,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Tag.Administrator.EDIT_TAG)));
            options.AddPolicy(Policies.Administrator.VIEW_DELETED_TAG,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Tag.Administrator.VIEW_DELETED_TAG)));
        }

        /// <summary>
        /// Register all policies related to managing registration invites.
        /// </summary>
        /// <param name="options"></param>
        private static void AddCustomRegistrationInvitationPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_REGISTRATION_INVITE,
                policy => policy.Requirements.Add(new RegistrationInviteAdministrationRequirement()));

            //Specific tag type policies
            options.AddPolicy(Policies.Owner.DELETE_REGISTRATION_INVITE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(RegistrationInvite.Owner.DELETE_REGISTRATION_INVITE)));
            options.AddPolicy(Policies.Administrator.EDIT_REGISTRATION_INVITE,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(RegistrationInvite.Administrator.EDIT_REGISTRATION_INVITE)));
        }

        private static void AddCustomWorkPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_WORK,
                policy => policy.Requirements.Add(new WorkAdministrationRequirement()));

            //Specific work policies
            options.AddPolicy(Policies.Owner.DELETE_WORK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Owner.DELETE_WORK)));
            options.AddPolicy(Policies.Administrator.EDIT_WORK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Administrator.EDIT_WORK)));
            options.AddPolicy(Policies.Administrator.VIEW_DELETED_WORK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Administrator.VIEW_DELETED_WORK)));

            options.AddPolicy(Policies.User.DOWNLOAD_WORK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Normal.DOWNLOAD_WORK)));
            options.AddPolicy(Policies.User.DOWNLOAD_VOLUME,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Normal.DOWNLOAD_VOLUME)));
            options.AddPolicy(Policies.User.DOWNLOAD_CHAPTER,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Work.Normal.DOWNLOAD_CHAPTER)));
        }

        private static void AddCustomCollectionPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_COLLECTION,
                policy => policy.Requirements.Add(new CollectionAdministrationRequirement()));

            //Specific work policies
            options.AddPolicy(Policies.Owner.DELETE_COLLECTION,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Collection.Owner.DELETE_COLLECTION)));
            options.AddPolicy(Policies.Administrator.EDIT_COLLECTION,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Collection.Administrator.EDIT_COLLECTION)));
            options.AddPolicy(Policies.Administrator.VIEW_DELETED_COLLECTION,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(Collection.Administrator.VIEW_DELETED_COLLECTION)));
        }

        private static void AddCustomConfigurationPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_CONFIGURATION_GENERIC,
                policy => policy.Requirements.Add(new GenericConfigurationAdministrationRequirement()));

            options.AddCustomCachingConfigurationPolicies();
        }

        private static void AddCustomCachingConfigurationPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_CACHING_CONFIGURATION_GENERIC,
                policy => policy.Requirements.Add(new GenericCachingConfigurationAdministrationRequirement()));

            options.AddPolicy(Policies.Owner.MANAGE_CACHE_CONFIGURATIONS,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(SiteConfiguration.Cache.Owner.MANAGE_CACHE_CONFIGURATIONS)));
        }

        private static void AddCustomSocialLinkPolicies(this AuthorizationOptions options)
        {
            options.AddPolicy(Policies.Generic.MANAGE_SOCIAL_LINK,
                policy => policy.Requirements.Add(new SocialLinkAdministrationRequirement()));

            options.AddPolicy(Policies.Owner.DELETE_SOCIAL_LINK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(SocialLink.Owner.DELETE_SOCIAL_LINK)));
            options.AddPolicy(Policies.Administrator.EDIT_SOCIAL_LINK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(SocialLink.Administrator.EDIT_SOCIAL_LINK)));
            options.AddPolicy(Policies.Administrator.VIEW_DELETED_SOCIAL_LINK,
                policy => policy.Requirements.Add(new GenericPermissionRequirement(SocialLink.Administrator.VIEW_DELETED_SOCIAL_LINK)));
        }

        /// <summary>
        /// Register all authorization handlers with the service collection
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddCustomPermissionHandlers(this IServiceCollection services)
        {
            services.AddSingleton<IAuthorizationHandler, GenericPermissionHandler>();
            services.AddSingleton<IAuthorizationHandler, AdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, UserAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, RoleAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, GenericTagAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, TagTypeAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, TagAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, RegistrationInviteAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, WorkAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, CollectionAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, GenericConfigurationAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, GenericCachingConfigurationAdministrationHandler>();
            services.AddSingleton<IAuthorizationHandler, SocialLinkAdministrationHandler>();

            return services;
        }
    }
}
