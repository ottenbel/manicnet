﻿using ManicNet.Contracts.Services;
using ManicNet.Services;
using Microsoft.Extensions.DependencyInjection;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class ServiceExtensions
    {
        /// <summary>
        /// Register services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddCustomServices(this IServiceCollection services)
        {
            services.AddLoggingServices()
                .AddAuthorizationServices()
                .AddImageServices()
                .AddTagServices()
                .AddRegistrationServices()
                .AddWorkServices()
                .AddCollectionServices()
                .AddEmailServices()
                .AddMetadataServices()
                .AddCacheServices()
                .AddConfigurationServices()
                .AddSocialServices();

            return services;
        }

        /// <summary>
        /// Register tag services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        private static IServiceCollection AddTagServices(this IServiceCollection services)
        {
            services.AddScoped<ITagTypeRetrievalService, TagTypeRetrievalService>();
            services.AddScoped<ITagTypeUpdateService, TagTypeUpdateService>();
            services.AddScoped<ITagRetrievalService, TagRetrievalService>();
            services.AddScoped<ITagUpdateService, TagUpdateService>();
            services.AddScoped<ITagAliasService, TagAliasService>();
            services.AddScoped<ICommonTagService, CommonTagService>();
            services.AddScoped<IUserBlockedTagsRetrievalService, UserBlockedTagsRetrievalService>();
            services.AddScoped<IUserBlockedTagsUpdateService, UserBlockedTagsUpdateService>();

            return services;
        }

        /// <summary>
        /// Register image services wih the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="settings"></param>
        /// <returns></returns>
        private static IServiceCollection AddImageServices(this IServiceCollection services)
        {
            services.AddScoped<IImageProcessingService, ImageProcessingService>();

            services.AddScoped<IContentService, ContentService>();
            services.AddScoped<IThumbnailService, ThumbnailService>();
            services.AddScoped<IAvatarService, AvatarService>();

            services.AddScoped<IContentFileHandlerService, ContentFileHandlerService>();
            services.AddScoped<IAvatarFileHandlerService, AvatarFileHandlerService>();

            services.AddScoped<IUploadService, UploadService>();

            return services;
        }

        /// <summary>
        /// Register registration services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddRegistrationServices(this IServiceCollection services)
        {
            services.AddScoped<IRegistrationInviteService, RegistrationInviteService>();

            return services;
        }

        /// <summary>
        /// Register logging services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddLoggingServices(this IServiceCollection services)
        {
            services.AddScoped<IManicNetLoggerService, ManicNetLoggerService>();

            return services;
        }

        /// <summary>
        /// Register authorization/permission services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddAuthorizationServices(this IServiceCollection services)
        {
            services.AddSingleton<IManicNetAuthorizationService, ManicNetAuthorizationService>();
            services.AddScoped<IManicNetPermissionService, ManicNetPermissionService>();

            return services;
        }

        /// <summary>
        /// Register work services with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddWorkServices(this IServiceCollection services)
        {
            services.AddScoped<IWorkUpdateService, WorkUpdateService>();
            services.AddScoped<IWorkRetrievalService, WorkRetrievalService>();
            services.AddScoped<IVolumeUpdateService, VolumeUpdateService>();
            services.AddScoped<IVolumeRetrievalService, VolumeRetrievalService>();
            services.AddScoped<IChapterUpdateService, ChapterUpdateService>();
            services.AddScoped<IChapterRetrievalService, ChapterRetrievalService>();

            return services;
        }

        public static IServiceCollection AddCollectionServices(this IServiceCollection services)
        {
            services.AddScoped<ICollectionRetrievalService, CollectionRetrievalService>();
            services.AddScoped<ICollectionUpdateService, CollectionUpdateService>();

            return services;
        }

        public static IServiceCollection AddEmailServices(this IServiceCollection services)
        {
            services.AddScoped<IManicNetEmailService, ManicNetEmailService>();

            return services;
        }

        public static IServiceCollection AddMetadataServices(this IServiceCollection services)
        {
            services.AddScoped<IMetadataService, MetadataService>();

            return services;
        }

        public static IServiceCollection AddCacheServices(this IServiceCollection services)
        {
            services.AddScoped<ICacheCleanerService, CacheCleanerService>();

            return services;
        }

        public static IServiceCollection AddConfigurationServices(this IServiceCollection services)
        {
            services.AddScoped<ICacheConfigurationRetrievalService, CacheConfigurationRetrievalService>();
            services.AddScoped<ICacheConfigurationUpdateService, CacheConfigurationUpdateService>();

            return services;
        }

        public static IServiceCollection AddSocialServices(this IServiceCollection services)
        {
            services.AddScoped<ISocialLinkService, SocialLinkService>();

            return services;
        }
    }
}
