﻿using ManicNet.Contracts.Repositories;
using ManicNet.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class RepositoryExtensions
    {
        /// <summary>
        /// Register repositories with the service collection.
        /// </summary>
        /// <param name="services"></param>
        public static IServiceCollection AddCustomRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>()
                .AddTagRepositories()
                .AddImageRepositories()
                .AddRegistrationRepositories()
                .AddWorkRepositories()
                .AddCollectionRepositories()
                .AddConfigurationRepositories()
                .AddSocialRepositories();

            return services;
        }

        /// <summary>
        /// Register tag repositories with the service collection.
        /// </summary>
        /// <param name="services"></param>
        private static IServiceCollection AddTagRepositories(this IServiceCollection services)
        {
            services.AddScoped<ITagTypeRepository, TagTypeRepository>();
            services.AddScoped<ITagRepository, TagRepository>();
            services.AddScoped<ITagAliasRepository, TagAliasRepository>();
            services.AddScoped<IUserBlockedTagsRepository, UserBlockedTagsRepository>();

            return services;
        }

        /// <summary>
        /// Register image repositories with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddImageRepositories(this IServiceCollection services)
        {
            services.AddScoped<IContentRepository, ContentRepository>();
            services.AddScoped<IThumbnailRepository, ThumbnailRepository>();
            services.AddScoped<IAvatarRepository, AvatarRepository>();

            return services;
        }

        /// <summary>
        /// Register registration invite repositories with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddRegistrationRepositories(this IServiceCollection services)
        {
            services.AddScoped<IRegistrationInviteRepository, RegistrationInviteRepository>();

            return services;
        }

        /// <summary>
        /// Register work repositories with the service collection.
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        private static IServiceCollection AddWorkRepositories(this IServiceCollection services)
        {
            services.AddScoped<IWorkRepository, WorkRepository>();
            services.AddScoped<IVolumeRepository, VolumeRepository>();
            services.AddScoped<IChapterRepository, ChapterRepository>();

            return services;
        }

        private static IServiceCollection AddCollectionRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICollectionRepository, CollectionRepository>();

            return services;
        }

        public static IServiceCollection AddConfigurationRepositories(this IServiceCollection services)
        {
            services.AddScoped<ICacheConfigurationRepository, CacheConfigurationRepository>();

            return services;
        }

        public static IServiceCollection AddSocialRepositories(this IServiceCollection services)
        {
            services.AddScoped<ISocialLinkRepository, SocialLinkRepository>();

            return services;
        }
    }
}
