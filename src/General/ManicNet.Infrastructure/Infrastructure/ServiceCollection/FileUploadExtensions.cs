﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;

namespace ManicNet.Infrastructure.ServiceCollection
{
    public static class FileUploadExtensions
    {
        public static IServiceCollection AddCustomFileHandling(this IServiceCollection services, ManicNetSettings settings, ILogger logger)
        {
            if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.LOCAL, StringComparison.OrdinalIgnoreCase))
            {
                services.AddLocalFileHandling(logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.AZURE_BLOB_STORAGE, StringComparison.OrdinalIgnoreCase))
            {
                services.AddAzureBlobStorageFileHandling(logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.AMAZON_S3, StringComparison.OrdinalIgnoreCase))
            {
                services.AddAamazonS3FileHandling(logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.DIGITAL_OCEAN_SPACES, StringComparison.OrdinalIgnoreCase))
            {
                services.AddDigitalOceanSpacesFileHandling(logger);
            }
            else
            {
                logger.LogError("Startup: Unknown file upload provider.");
                throw new Exception("Unknown value provided while registering file upload service.");
            }

            return services;
        }

        public static IServiceCollection AddLocalFileHandling(this IServiceCollection services, ILogger logger)
        {
            logger.LogInformation("Startup: Registering local file storage as file handler.");

            services.AddScoped<IFileIOService, LocalFileIOService>();

            return services;
        }

        public static IServiceCollection AddAzureBlobStorageFileHandling(this IServiceCollection services, ILogger logger)
        {
            logger.LogInformation("Startup: Registering azure blob storage as file handler.");

            services.AddScoped<IFileIOService, AzureBlobStorageFileIOService>();

            return services;
        }

        public static IServiceCollection AddAamazonS3FileHandling(this IServiceCollection services, ILogger logger)
        {
            logger.LogInformation("Startup: Registering amazon s3 as file handler.");

            services.AddScoped<IFileIOService, AmazonS3FileIOService>();

            return services;
        }

        public static IServiceCollection AddDigitalOceanSpacesFileHandling(this IServiceCollection services, ILogger logger)
        {
            logger.LogInformation("Startup: Registering digital ocean spaces as file handler.");

            services.AddScoped<IFileIOService, DigitalOceanSpacesFileIOService>();

            return services;
        }
    }
}
