﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using ManicNet.Constants;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;

namespace ManicNet.Infrastructure.ApplicationBuilder
{
    public static class FileUploadExtensions
    {
        public static IApplicationBuilder ConfigureFileUpload(this IApplicationBuilder app, ManicNetSettings settings, ILogger logger)
        {
            app.UseStaticFiles();

            if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.LOCAL, StringComparison.OrdinalIgnoreCase))
            {
                app.ConfigureLocalUpload(settings, logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.AZURE_BLOB_STORAGE, StringComparison.OrdinalIgnoreCase))
            {
                app.ConfigureAzureBlobStorageUpload(settings, logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.AMAZON_S3, StringComparison.OrdinalIgnoreCase))
            {
                app.ConfigureAmazonS3Upload(settings, logger);
            }
            else if (string.Equals(settings.FileUploadSettings.UploadType, Uploads.Type.DIGITAL_OCEAN_SPACES, StringComparison.OrdinalIgnoreCase))
            {
                app.ConfigureDigitalOceanSpacesUpload(settings, logger);
            }
            else
            {
                logger.LogCritical("Startup: File upload provider value in settings not recognized.");
                throw new Exception("File upload provider value in settings not recognized");
            }

            return app;
        }

        /// <summary>
        /// Configue file uploads to local storage.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="settings"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        private static IApplicationBuilder ConfigureLocalUpload(this IApplicationBuilder app, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Configuring local file storage as file provider");

            if (!Directory.Exists(settings.FileUploadSettings.LocalUpload.Avatar.Directory))
            {
                logger.LogInformation(string.Format("Startup: Folder {0} to store avatars does not exist. Creating missing folder.", settings.FileUploadSettings.LocalUpload.Avatar.Directory));
                Directory.CreateDirectory(settings.FileUploadSettings.LocalUpload.Avatar.Directory);
                logger.LogInformation(string.Format("Startup: Folder {0} created successfully.", settings.FileUploadSettings.LocalUpload.Avatar.Directory));
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(settings.FileUploadSettings.LocalUpload.Avatar.Directory),
                RequestPath = settings.FileUploadSettings.LocalUpload.Avatar.PublicPath
            });

            if (!Directory.Exists(settings.FileUploadSettings.LocalUpload.Thumbnail.Directory))
            {
                logger.LogInformation(string.Format("Startup: Folder {0} to store thumbnails does not exist. Creating missing folder.", settings.FileUploadSettings.LocalUpload.Thumbnail.Directory));
                Directory.CreateDirectory(settings.FileUploadSettings.LocalUpload.Thumbnail.Directory);
                logger.LogInformation(string.Format("Startup: Folder {0} created successfully.", settings.FileUploadSettings.LocalUpload.Thumbnail.Directory));
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(settings.FileUploadSettings.LocalUpload.Thumbnail.Directory),
                RequestPath = settings.FileUploadSettings.LocalUpload.Thumbnail.PublicPath
            });

            if (!Directory.Exists(settings.FileUploadSettings.LocalUpload.Content.Directory))
            {
                logger.LogInformation(string.Format("Startup: Folder {0} to store content does not exist. Creating missing folder.", settings.FileUploadSettings.LocalUpload.Content.Directory));
                Directory.CreateDirectory(settings.FileUploadSettings.LocalUpload.Content.Directory);
                logger.LogInformation(string.Format("Startup: Folder {0} created successfully.", settings.FileUploadSettings.LocalUpload.Content.Directory));
            }

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(settings.FileUploadSettings.LocalUpload.Content.Directory),
                RequestPath = settings.FileUploadSettings.LocalUpload.Content.PublicPath
            });

            logger.LogInformation("Startup: File upload successfully set to local storage.");

            return app;
        }

        private static IApplicationBuilder ConfigureAzureBlobStorageUpload(this IApplicationBuilder app, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Configuring azure blob storage as file provider");

            BlobServiceClient blobServiceClient = new BlobServiceClient(settings.FileUploadSettings.AzureBlobStorageUpload.ConnectionString);
            BlobContainerClient avatarContainerClient = blobServiceClient.GetBlobContainerClient(settings.FileUploadSettings.AzureBlobStorageUpload.Avatar.Container);
            BlobContainerClient thumbnailContainerClient = blobServiceClient.GetBlobContainerClient(settings.FileUploadSettings.AzureBlobStorageUpload.Thumbnail.Container);
            BlobContainerClient contentContainerClient = blobServiceClient.GetBlobContainerClient(settings.FileUploadSettings.AzureBlobStorageUpload.Content.Container);

            CreateMissingContainers(avatarContainerClient, settings.FileUploadSettings.AzureBlobStorageUpload.Avatar.AccessLevel, logger);
            CreateMissingContainers(thumbnailContainerClient, settings.FileUploadSettings.AzureBlobStorageUpload.Thumbnail.AccessLevel, logger);
            CreateMissingContainers(contentContainerClient, settings.FileUploadSettings.AzureBlobStorageUpload.Content.AccessLevel, logger);

            logger.LogInformation("Startup: File upload successfully set to azure blob storage.");

            return app;
        }

        private static IApplicationBuilder ConfigureAmazonS3Upload(this IApplicationBuilder app, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Configuring amazon s3 as file provider");

            BasicAWSCredentials credentials = new BasicAWSCredentials(settings.FileUploadSettings.AmazonS3Upload.AccessKey, settings.FileUploadSettings.AmazonS3Upload.Secret);

            var avatarConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.FileUploadSettings.AmazonS3Upload.Avatar.Region) };
            var thumbnailConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.FileUploadSettings.AmazonS3Upload.Thumbnail.Region) };
            var contentConfig = new AmazonS3Config { RegionEndpoint = AmazonS3RegionHelper.Convert(settings.FileUploadSettings.AmazonS3Upload.Content.Region) };

            AmazonS3Client avatarClient = new AmazonS3Client(credentials, avatarConfig);
            AmazonS3Client thumbnailClient = new AmazonS3Client(credentials, thumbnailConfig);
            AmazonS3Client contentClient = new AmazonS3Client(credentials, contentConfig);

            CreateMissingBuckets(avatarClient, "avatars", settings.FileUploadSettings.AmazonS3Upload.Avatar.Bucket, settings.FileUploadSettings.AmazonS3Upload.Avatar.AccessLevel, logger);
            CreateMissingBuckets(thumbnailClient, "thumbnails", settings.FileUploadSettings.AmazonS3Upload.Thumbnail.Bucket, settings.FileUploadSettings.AmazonS3Upload.Thumbnail.AccessLevel, logger);
            CreateMissingBuckets(contentClient, "contents", settings.FileUploadSettings.AmazonS3Upload.Content.Bucket, settings.FileUploadSettings.AmazonS3Upload.Content.AccessLevel, logger);

            logger.LogInformation("Startup: File upload successfully set to amazon s3.");

            return app;
        }

        private static IApplicationBuilder ConfigureDigitalOceanSpacesUpload(this IApplicationBuilder app, ManicNetSettings settings, ILogger logger)
        {
            logger.LogInformation("Startup: Configuring digital ocean spaces as file provider");

            BasicAWSCredentials credentials = new BasicAWSCredentials(settings.FileUploadSettings.DigitalOceanSpacesUpload.AccessKey, settings.FileUploadSettings.DigitalOceanSpacesUpload.Secret);

            AmazonS3Config avatarConfig = new AmazonS3Config { ServiceURL = DigitalOceanSpacesServiceURLHelper.GetServiceURL(settings.FileUploadSettings.DigitalOceanSpacesUpload.Avatar.Region) };
            AmazonS3Config thumbnailConfig = new AmazonS3Config { ServiceURL = DigitalOceanSpacesServiceURLHelper.GetServiceURL(settings.FileUploadSettings.DigitalOceanSpacesUpload.Thumbnail.Region) };
            AmazonS3Config contentConfig = new AmazonS3Config { ServiceURL = DigitalOceanSpacesServiceURLHelper.GetServiceURL(settings.FileUploadSettings.DigitalOceanSpacesUpload.Content.Region) };

            AmazonS3Client avatarClient = new AmazonS3Client(credentials, avatarConfig);
            AmazonS3Client thumbnailClient = new AmazonS3Client(credentials, thumbnailConfig);
            AmazonS3Client contentClient = new AmazonS3Client(credentials, contentConfig);

            CreateMissingBuckets(avatarClient, "avatars", settings.FileUploadSettings.DigitalOceanSpacesUpload.Avatar.Bucket, settings.FileUploadSettings.DigitalOceanSpacesUpload.Avatar.AccessLevel, logger);
            CreateMissingBuckets(thumbnailClient, "thumbnails", settings.FileUploadSettings.DigitalOceanSpacesUpload.Thumbnail.Bucket, settings.FileUploadSettings.DigitalOceanSpacesUpload.Thumbnail.AccessLevel, logger);
            CreateMissingBuckets(contentClient, "contents", settings.FileUploadSettings.DigitalOceanSpacesUpload.Content.Bucket, settings.FileUploadSettings.DigitalOceanSpacesUpload.Content.AccessLevel, logger);

            logger.LogInformation("Startup: File upload successfully set to digital ocean spaces.");

            return app;
        }

        private static void CreateMissingContainers(BlobContainerClient containerClient, string accessLevel, ILogger logger)
        {
            if (!containerClient.Exists())
            {
                logger.LogInformation(string.Format("Startup: Container {0} to store avatars does not exist. Creating missing container.", containerClient.Name));

                if (string.Equals(accessLevel, Uploads.AzureContainer.CONTAINER, StringComparison.OrdinalIgnoreCase))
                {
                    containerClient.CreateIfNotExists(PublicAccessType.BlobContainer);
                    logger.LogInformation(string.Format("Startup: Container {0} created successfully with Container level access.", containerClient.Name));
                }
                else if (string.Equals(accessLevel, Uploads.AzureContainer.BLOB, StringComparison.OrdinalIgnoreCase))
                {
                    containerClient.CreateIfNotExists(PublicAccessType.Blob);
                    logger.LogInformation(string.Format("Startup: Container {0} created successfully with Blob level access.", containerClient.Name));
                }
                else if (string.Equals(accessLevel, Uploads.AzureContainer.PRIVATE, StringComparison.OrdinalIgnoreCase))
                {
                    containerClient.CreateIfNotExists(PublicAccessType.None);
                    logger.LogInformation(string.Format("Startup: Container {0} created successfully with Private level access.", containerClient.Name));
                }
                else
                {
                    containerClient.CreateIfNotExists(PublicAccessType.None);
                    logger.LogInformation(string.Format("Startup: Container {0} created successfully with Unknown level access (defaulting to Private).", containerClient.Name));
                }
            }
        }

        private static void CreateMissingBuckets(AmazonS3Client client, string bucketType, string bucketName, string accessLevel, ILogger logger)
        {
            var buckets = client.ListBucketsAsync().GetAwaiter().GetResult();

            if (!buckets.Buckets.Any(x => x.BucketName == bucketName))
            {
                logger.LogInformation(string.Format("Startup: Bucket {0} to store {1} does not exist. Creating missing container.", bucketName, bucketType));

                PutBucketRequest request = new PutBucketRequest()
                {

                    BucketName = bucketName,
                    CannedACL = string.Equals(accessLevel, Uploads.AmazonS3.Bucket.PUBLIC, StringComparison.OrdinalIgnoreCase) ? S3CannedACL.PublicRead : S3CannedACL.Private
                };

                var response = client.PutBucketAsync(request).GetAwaiter().GetResult();

                logger.LogInformation(string.Format("Startup: Bucket {0} created successfully with {1} level access.", bucketName, accessLevel));
            }
        }
    }
}
