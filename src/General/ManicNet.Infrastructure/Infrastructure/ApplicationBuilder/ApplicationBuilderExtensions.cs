﻿using ManicNet.Infrastructure.Middleware;
using Microsoft.AspNetCore.Builder;

namespace ManicNet.Infrastructure.ApplicationBuilder
{
    /// <summary>
    /// A class containing extension methods used to clean up the Startup.cs file by grouping common 
    /// functionality into simple methods that can be called during application configuration.
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// An extension method used to register custom middleware during startup.
        /// </summary>
        /// <param name="app"></param>
        public static IApplicationBuilder AddCustomMiddleware(this IApplicationBuilder app)
        {
            app.UseUserIdentityMiddleware().UseLogErrorsMiddleware();
            return app;
        }
    }
}
