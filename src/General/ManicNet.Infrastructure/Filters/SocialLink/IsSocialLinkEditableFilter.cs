﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public class IsSocialLinkEditableFilter : TypeFilterAttribute
    {
        public IsSocialLinkEditableFilter() : base(typeof(IsSocialLinkEditableImplementation)) { }

        private class IsSocialLinkEditableImplementation : IsSocialLinkEditableBase
        {
            public IsSocialLinkEditableImplementation(IStringLocalizer<SocialLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsSocialLinkEditableFilter") { }
        }
    }

    public class IsSocialLinkEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<SocialLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsSocialLinkEditableBase(IStringLocalizer<SocialLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            SocialLink socialLink = (SocialLink)context.HttpContext.Items[FilterIdentifiers.SOCIAL_LINK_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (socialLink.IsDeleted)
                {
                    warnings.Add(localizer[SocialLocalization.SOCIAL_LINK_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.ADMINISTRATION },
                        { "controller", Routing.Controllers.SocialLinks.CONTROLLER },
                        { "action", Routing.Controllers.Work.Routes.SHOW },
                        { "id", socialLink.SocialLinkID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
