﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public class CheckSocialLinkExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckSocialLinkExistenceBypassCacheFilter() : base(typeof(CheckSocialLinkExistenceBypassCacheImplementation)) { }

        private class CheckSocialLinkExistenceBypassCacheImplementation : CheckSocialLinkExistenceBase
        {
            public CheckSocialLinkExistenceBypassCacheImplementation(ISocialLinkService socialLinkService, IManicNetLoggerService logger) : base(socialLinkService, logger, true, "CheckSocialLinkExistenceBypassCacheFilter") { }
        }
    }

    public class CheckSocialLinkExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckSocialLinkExistenceIncludeCacheFilter() : base(typeof(CheckSocialLinkExistenceIncludeCacheImplementation)) { }

        private class CheckSocialLinkExistenceIncludeCacheImplementation : CheckSocialLinkExistenceBase
        {
            public CheckSocialLinkExistenceIncludeCacheImplementation(ISocialLinkService socialLinkService, IManicNetLoggerService logger) : base(socialLinkService, logger, false, "CheckSocialLinkExistenceIncludeCacheFilter") { }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckSocialLinkExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly ISocialLinkService socialLinkService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckSocialLinkExistenceBase(ISocialLinkService socialLinkService, IManicNetLoggerService logger, bool BypassCache, string functionIdentifier)
        {
            this.socialLinkService = socialLinkService;
            this.logger = logger;
            this.BypassCache = BypassCache;

            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? socialLinkID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                socialLinkID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                socialLinkID = ((SocialLinkModifyModel)model).SocialLinkID;
            }

            if (socialLinkID.HasValue)
            {
                try
                {
                    SocialLink socialLink = await socialLinkService.FindSocialLinkAsync(socialLinkID.Value, context.HttpContext.User, BypassCache);
                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.SOCIAL_LINK_FROM_FILTER, socialLink);

                    await next();
                }
                catch (SocialLinkNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find social link with ID {0}.", socialLinkID));
                    context.Result = new NotFoundResult();
                }
                catch (SocialLinkNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view social link with ID {1}.", context.HttpContext.User.Identity.Name, socialLinkID));
                    context.Result = new NotFoundResult();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
