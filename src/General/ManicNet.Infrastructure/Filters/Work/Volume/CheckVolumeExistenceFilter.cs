﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckVolumeExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckVolumeExistenceBypassCacheFilter() : base(typeof(CheckVolumeExistenceBypassCacheImplementation)) { }

        private sealed class CheckVolumeExistenceBypassCacheImplementation : CheckVolumeExistenceBase
        {
            public CheckVolumeExistenceBypassCacheImplementation(IVolumeRetrievalService volumeRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger) : base(volumeRetrievalService, userBlockedTagsRetrievalService, workRetrievalService, logger, true, "CheckVolumeExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckVolumeExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckVolumeExistenceIncludeCacheFilter() : base(typeof(CheckVolumeExistenceIncludeCacheImplementation)) { }

        private sealed class CheckVolumeExistenceIncludeCacheImplementation : CheckVolumeExistenceBase
        {
            public CheckVolumeExistenceIncludeCacheImplementation(IVolumeRetrievalService volumeRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger) : base(volumeRetrievalService, userBlockedTagsRetrievalService, workRetrievalService, logger, false, "CheckVolumeExistenceIncludeCacheFilter") { }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckVolumeExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckVolumeExistenceBase(IVolumeRetrievalService volumeRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger, bool BypassCache, string functionIdentifier)
        {
            this.volumeRetrievalService = volumeRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.logger = logger;
            this.BypassCache = BypassCache;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? volumeID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                volumeID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                volumeID = ((VolumeModifyModel)model).VolumeID;
            }
            else if (context.ActionArguments.TryGetValue("migrationModel", out object migrationModel))
            {
                volumeID = ((VolumeMigrationModel)migrationModel).VolumeID;
            }

            if (volumeID.HasValue)
            {
                try
                {
                    Volume volume = await volumeRetrievalService.FindVolume(volumeID.Value, context.HttpContext.User, BypassCache);

                    Guid? userID = context.HttpContext.User.GetUserID();

                    if (userID.HasValue)
                    {
                        List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                        List<Guid> tagsAssociatedWithWork = await workRetrievalService.GetAllTagsAssociatedWithVolume(volume.WorkID, volume.VolumeID);

                        if (tagsAssociatedWithWork.Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any())
                        {
                            context.Result = new NotFoundResult();
                            return;
                        }
                    }

                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.VOLUME_FROM_FILTER, volume);
                    context.HttpContext.Items.Add(FilterIdentifiers.VOLUME_ID_FROM_FILTER, volume.VolumeID);
                    context.HttpContext.Items.Add(FilterIdentifiers.WORK_ID_FROM_FILTER, volume.WorkID);

                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_IDENTIFIER, volume.WorkID);
                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_NAME, volume.Work.Title);

                    await next();
                }
                catch (VolumeNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find volume with ID {0}.", volumeID));
                    context.Result = new NotFoundResult();
                }
                catch (VolumeNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view volume with ID {1}.", context.HttpContext.User.Identity.Name, volumeID));
                    context.Result = new NotFoundResult();
                }
                catch (VolumeUnableToViewParentWorkException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Parent work is in a soft deleted state. User {0} doesn't have access to view soft deleted works.", context.HttpContext.User.Identity.Name));
                    context.Result = new NotFoundResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex);
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
