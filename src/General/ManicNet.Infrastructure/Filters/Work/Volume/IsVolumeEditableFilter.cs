﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsVolumeEditableFilter : TypeFilterAttribute
    {
        public IsVolumeEditableFilter() : base(typeof(IsVolumeEditableImplementation)) { }

        private sealed class IsVolumeEditableImplementation : IsVolumeEditableBase
        {
            public IsVolumeEditableImplementation(IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsVolumeEditableFilter") { }
        }
    }

    public class IsVolumeEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<VolumeLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsVolumeEditableBase(IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Volume volume = (Volume)context.HttpContext.Items[FilterIdentifiers.VOLUME_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (volume.IsDeleted)
                {
                    warnings.Add(localizer[VolumeLocalization.VOLUME_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Volume.CONTROLLER },
                        { "action", Routing.Controllers.Volume.Routes.SHOW },
                        { "id", volume.VolumeID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
