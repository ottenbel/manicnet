﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckVolumeCreatableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckVolumeCreatableBasedOnParentFilter() : base(typeof(CheckVolumeCreatableBasedOnParentFilterImplementation)) { }

        private sealed class CheckVolumeCreatableBasedOnParentFilterImplementation : CheckVolumeCanActionBasedOnParentBase
        {
            public CheckVolumeCreatableBasedOnParentFilterImplementation(IWorkRetrievalService workRetrievalService, IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger) : base(workRetrievalService, localizer, logger, FilterIdentifiers.ADDING, "CheckVolumeCreatableBasedOnParentFilter") { }
        }
    }

    public sealed class CheckVolumeEditableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckVolumeEditableBasedOnParentFilter() : base(typeof(CheckVolumeEditableBasedOnParentFilterImplementation)) { }

        private sealed class CheckVolumeEditableBasedOnParentFilterImplementation : CheckVolumeCanActionBasedOnParentBase
        {
            public CheckVolumeEditableBasedOnParentFilterImplementation(IWorkRetrievalService workRetrievalService, IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger) : base(workRetrievalService, localizer, logger, FilterIdentifiers.EDITING, "CheckVolumeEditableBasedOnParentFilter") { }
        }
    }

    public sealed class CheckVolumeRestorableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckVolumeRestorableBasedOnParentFilter() : base(typeof(CheckVolumeRestorableBasedOnParentFilterImplementation)) { }

        private sealed class CheckVolumeRestorableBasedOnParentFilterImplementation : CheckVolumeCanActionBasedOnParentBase
        {
            public CheckVolumeRestorableBasedOnParentFilterImplementation(IWorkRetrievalService workRetrievalService, IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger) : base(workRetrievalService, localizer, logger, FilterIdentifiers.RESTORING, "CheckVolumeRestorableBasedOnParentFilter") { }
        }
    }

    public class CheckVolumeCanActionBasedOnParentBase : IAsyncActionFilter
    {
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IStringLocalizer<VolumeLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly Guid actionIdentifier;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckVolumeCanActionBasedOnParentBase(IWorkRetrievalService workRetrievalService, IStringLocalizer<VolumeLocalization> localizer, IManicNetLoggerService logger, Guid actionIdentifier, string functionIdentifier)
        {
            this.workRetrievalService = workRetrievalService;
            this.localizer = localizer;
            this.logger = logger;
            this.actionIdentifier = actionIdentifier;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Guid? workID = null;
            Guid? volumeID = null;

            try
            {
                volumeID = (Guid?)context.HttpContext.Items[FilterIdentifiers.VOLUME_ID_FROM_FILTER];
                workID = (Guid?)context.HttpContext.Items[FilterIdentifiers.WORK_ID_FROM_FILTER];

                if (!workID.HasValue)
                {
                    if (context.ActionArguments.TryGetValue("id", out object id))
                    {
                        workID = (Guid)id;
                    }
                    else if (context.ActionArguments.TryGetValue("model", out object model))
                    {
                        workID = ((VolumeModifyModel)model).WorkID;
                    }
                }

                if (workID.HasValue)
                {
                    bool redirect = false;
                    List<string> warnings = new List<string>();

                    Work work = await workRetrievalService.FindWorkAsync(workID.Value, context.HttpContext.User);

                    if (work.IsDeleted)
                    {
                        if (actionIdentifier == FilterIdentifiers.ADDING)
                        {
                            warnings.Add(localizer[VolumeLocalization.WORK_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_VOLUMES, work.Title]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.EDITING)
                        {
                            warnings.Add(localizer[VolumeLocalization.WORK_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_VOLUMES, work.Title]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.RESTORING)
                        {
                            warnings.Add(localizer[VolumeLocalization.WORK_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_VOLUMES, work.Title]);
                        }

                        context.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            { "area", Routing.Areas.GENERAL },
                            { "controller", Routing.Controllers.Work.CONTROLLER },
                            { "action", Routing.Controllers.Work.Routes.SHOW },
                            { "id", work.WorkID }
                        });

                        redirect = true;
                    }

                    if (redirect)
                    {
                        controller.TempData[Alerts.TempData.WARNING] = warnings;
                    }
                    else
                    {
                        context.HttpContext.Items.Add(FilterIdentifiers.WORK_FROM_FILTER, work);

                        await next();
                    }
                }
                else
                {
                    await next();
                }
            }
            catch (WorkNotFoundException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding volume {0}. Unable to find work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_ADDING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing volume {0}. Unable to find work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_EDITING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring on volume {0}. Unable to find work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_RESTORING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (WorkNotViewableException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding volume {0}. User doesn't have access to view work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_ADDING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing volume {0}. User doesn't have access to view work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_EDITING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring volume {0}. User doesn't have access to view work with ID {1}.", volumeID, workID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[VolumeLocalization.ERROR_RESTORING_VOLUME_FOR_NON_EXISTENT_WORK, workID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
