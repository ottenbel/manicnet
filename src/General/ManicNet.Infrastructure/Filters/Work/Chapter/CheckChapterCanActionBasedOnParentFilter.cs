﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckChapterCreatableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckChapterCreatableBasedOnParentFilter() : base(typeof(CheckChapterCreatableBasedOnParentFilterImplementation)) { }

        private sealed class CheckChapterCreatableBasedOnParentFilterImplementation : CheckChapterCanActionBasedOnParentBase
        {
            public CheckChapterCreatableBasedOnParentFilterImplementation(IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService, IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger) : base(volumeRetrievalService, workRetrievalService, localizer, logger, FilterIdentifiers.ADDING, "CheckChapterCreatableBasedOnParentFilter") { }
        }
    }

    public sealed class CheckChapterEditableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckChapterEditableBasedOnParentFilter() : base(typeof(CheckChapterEditableBasedOnParentFilterImplementation)) { }

        private sealed class CheckChapterEditableBasedOnParentFilterImplementation : CheckChapterCanActionBasedOnParentBase
        {
            public CheckChapterEditableBasedOnParentFilterImplementation(IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService, IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger) : base(volumeRetrievalService, workRetrievalService, localizer, logger, FilterIdentifiers.EDITING, "CheckChapterEditableBasedOnParentFilter") { }
        }
    }

    public sealed class CheckChapterRestorableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckChapterRestorableBasedOnParentFilter() : base(typeof(CheckChapterRestorableBasedOnParentFilterImplementation)) { }

        private sealed class CheckChapterRestorableBasedOnParentFilterImplementation : CheckChapterCanActionBasedOnParentBase
        {
            public CheckChapterRestorableBasedOnParentFilterImplementation(IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService, IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger) : base(volumeRetrievalService, workRetrievalService, localizer, logger, FilterIdentifiers.RESTORING, "CheckChapterRestorableBasedOnParentFilter") { }
        }
    }

    public class CheckChapterCanActionBasedOnParentBase : IAsyncActionFilter
    {
        private readonly IVolumeRetrievalService volumeRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IStringLocalizer<ChapterLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly Guid actionIdentifier;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;


        public CheckChapterCanActionBasedOnParentBase(IVolumeRetrievalService volumeRetrievalService, IWorkRetrievalService workRetrievalService, IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger, Guid actionIdentifier, string functionIdentifier)
        {
            this.volumeRetrievalService = volumeRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.localizer = localizer;
            this.logger = logger;
            this.actionIdentifier = actionIdentifier;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Work work = new Work();
            Volume volume = new Volume();

            Guid? volumeID = null;
            Guid? chapterID = null;

            try
            {
                chapterID = (Guid?)context.HttpContext.Items[FilterIdentifiers.CHAPTER_ID_FROM_FILTER];
                volumeID = (Guid?)context.HttpContext.Items[FilterIdentifiers.VOLUME_ID_FROM_FILTER];

                if (!volumeID.HasValue)
                {
                    if (context.ActionArguments.TryGetValue("id", out object id))
                    {
                        volumeID = (Guid)id;
                    }
                    else if (context.ActionArguments.TryGetValue("model", out object model))
                    {
                        volumeID = ((ChapterModifyModel)model).VolumeID;
                    }
                }

                if (volumeID.HasValue)
                {
                    bool redirect = false;
                    List<string> warnings = new List<string>();

                    volume = await volumeRetrievalService.FindVolume(volumeID.Value, context.HttpContext.User);
                    work = await workRetrievalService.FindWorkAsync(volume.WorkID, context.HttpContext.User);

                    if (work.IsDeleted)
                    {
                        if (actionIdentifier == FilterIdentifiers.ADDING)
                        {
                            warnings.Add(localizer[ChapterLocalization.WORK_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_CHAPTERS, work.Title]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.EDITING)
                        {
                            warnings.Add(localizer[ChapterLocalization.WORK_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_CHAPTERS, work.Title]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.RESTORING)
                        {
                            warnings.Add(localizer[ChapterLocalization.WORK_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_CHAPTERS, work.Title]);
                        }
                        context.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            { "area", Routing.Areas.GENERAL },
                            { "controller", Routing.Controllers.Work.CONTROLLER },
                            { "action", Routing.Controllers.Work.Routes.SHOW },
                            { "id", work.WorkID }
                        });

                        redirect = true;
                    }
                    else if (volume.IsDeleted)
                    {
                        if (actionIdentifier == FilterIdentifiers.ADDING)
                        {
                            warnings.Add(localizer[ChapterLocalization.VOLUME_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_CHAPTERS, volume.Number]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.EDITING)
                        {
                            warnings.Add(localizer[ChapterLocalization.VOLUME_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_CHAPTERS, volume.Number]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.RESTORING)
                        {
                            warnings.Add(localizer[ChapterLocalization.VOLUME_MUST_BE_ACTIVE_BEFORE_RESTORING_ASSOCIATED_CHAPTERS, volume.Number]);
                        }

                        context.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            { "area", Routing.Areas.GENERAL },
                            { "controller", Routing.Controllers.Volume.CONTROLLER },
                            { "action", Routing.Controllers.Volume.Routes.SHOW },
                            { "id", volume.VolumeID }
                        });

                        redirect = true;
                    }

                    if (redirect)
                    {
                        controller.TempData[Alerts.TempData.WARNING] = warnings;
                    }
                    else
                    {
                        context.HttpContext.Items.Add(FilterIdentifiers.VOLUME_FROM_FILTER, volume);
                        context.HttpContext.Items.Add(FilterIdentifiers.WORK_FROM_FILTER, work);

                        await next();
                    }
                }
                else
                {
                    await next();
                }
            }
            catch (WorkNotFoundException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {

                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding chapter {0}. Unable to find work with ID {1}.", chapterID, volume.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_ADDING_CHAPTER_FOR_NON_EXISTENT_VOLUME, volumeID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing chapter {0}. Unable to find work with ID {1}.", chapterID, volume.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_EDITING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring chapter {0}. Unable to find work with ID {1}.", chapterID, volume.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_RESTORING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (WorkNotViewableException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding chapter {0}. User doesn't have access to view work with ID {1}.", chapterID, work.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_ADDING_CHAPTER_FOR_NON_EXISTENT_VOLUME, volumeID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing chapter {0}. User doesn't have access to view work with ID {1}.", chapterID, work.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_EDITING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring chapter {0}. User doesn't have access to view work with ID {1}.", chapterID, work.WorkID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_RESTORING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (VolumeNotFoundException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding chapter {0}. Unable to find volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_ADDING_CHAPTER_FOR_NON_EXISTENT_VOLUME, volumeID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing chapter {0}. Unable to find volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_EDITING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring chapter {0}. Unable to find volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_RESTORING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (VolumeNotViewableException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding chapter {0}. User doesn't have access to view volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_ADDING_CHAPTER_FOR_NON_EXISTENT_VOLUME, volumeID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing chapter {0}. User doesn't have access to view volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_EDITING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.RESTORING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error restoring chapter {0}. User doesn't have access to view volume with ID {1}.", chapterID, volumeID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[ChapterLocalization.ERROR_RESTORING_NON_EXISTENT_CHAPTER, chapterID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Work.CONTROLLER },
                    { "action", Routing.Controllers.Work.Routes.INDEX },
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
