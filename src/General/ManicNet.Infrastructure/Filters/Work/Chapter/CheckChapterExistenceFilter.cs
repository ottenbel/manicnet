﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckChapterExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckChapterExistenceBypassCacheFilter() : base(typeof(CheckChapterExistenceBypassCacheImplementation)) { }

        private sealed class CheckChapterExistenceBypassCacheImplementation : CheckChapterExistenceBase
        {
            public CheckChapterExistenceBypassCacheImplementation(IChapterRetrievalService chapterRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger) : base(chapterRetrievalService, userBlockedTagsRetrievalService, workRetrievalService, logger, true, "CheckChapterExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckChapterExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckChapterExistenceIncludeCacheFilter() : base(typeof(CheckChapterExistenceIncludeCacheImplementation)) { }

        private sealed class CheckChapterExistenceIncludeCacheImplementation : CheckChapterExistenceBase
        {
            public CheckChapterExistenceIncludeCacheImplementation(IChapterRetrievalService chapterRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger) : base(chapterRetrievalService, userBlockedTagsRetrievalService, workRetrievalService, logger, false, "CheckChapterExistenceIncludeCacheFilter") { }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckChapterExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly IChapterRetrievalService chapterRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckChapterExistenceBase(IChapterRetrievalService chapterRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IWorkRetrievalService workRetrievalService, IManicNetLoggerService logger, bool BypassCache, string functionIdentifier)
        {
            this.chapterRetrievalService = chapterRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.workRetrievalService = workRetrievalService;
            this.logger = logger;
            this.BypassCache = BypassCache;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? chapterID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                chapterID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                chapterID = ((ChapterModifyModel)model).ChapterID;
            }
            else if (context.ActionArguments.TryGetValue("migrationModel", out object migrationModel))
            {
                chapterID = ((ChapterMigrationModel)migrationModel).ChapterID;
            }

            if (chapterID.HasValue)
            {
                try
                {
                    Chapter chapter = await chapterRetrievalService.FindChapter(chapterID.Value, context.HttpContext.User, BypassCache);

                    Guid? userID = context.HttpContext.User.GetUserID();

                    if (userID.HasValue)
                    {
                        List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                        List<Guid> tagsAssociatedWithWork = await workRetrievalService.GetAllTagsAssociatedWithChapter(chapter.Volume.WorkID, chapter.VolumeID, chapter.ChapterID);

                        if (tagsAssociatedWithWork.Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any())
                        {
                            context.Result = new NotFoundResult();
                            return;
                        }
                    }

                    context.HttpContext.Items.Add(FilterIdentifiers.CHAPTER_FROM_FILTER, chapter);
                    context.HttpContext.Items.Add(FilterIdentifiers.VOLUME_ID_FROM_FILTER, chapter.VolumeID);
                    context.HttpContext.Items.Add(FilterIdentifiers.CHAPTER_ID_FROM_FILTER, chapter.ChapterID);

                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_IDENTIFIER, chapter.Volume.WorkID);
                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_NAME, chapter.Volume.Work.Title);

                    await next();
                }
                catch (ChapterNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find chapter with ID {0}.", chapterID));
                    context.Result = new NotFoundResult();
                }
                catch (ChapterNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view chapter with ID {1}.", context.HttpContext.User.Identity.Name, chapterID));
                    context.Result = new NotFoundResult();
                }
                catch (ChapterUnableToViewParentWorkException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Parent work is in a soft deleted state. User {0} doesn't have access to view soft deleted works.", context.HttpContext.User.Identity.Name));
                    context.Result = new NotFoundResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex);
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
