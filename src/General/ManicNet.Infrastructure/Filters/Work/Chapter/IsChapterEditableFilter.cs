﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsChapterEditableFilter : TypeFilterAttribute
    {
        public IsChapterEditableFilter() : base(typeof(IsChapterEditableImplementation)) { }

        private sealed class IsChapterEditableImplementation : IsChapterEditableBase
        {
            public IsChapterEditableImplementation(IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsChapterEditableFilter") { }
        }
    }

    public class IsChapterEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<ChapterLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsChapterEditableBase(IStringLocalizer<ChapterLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Chapter chapter = (Chapter)context.HttpContext.Items[FilterIdentifiers.CHAPTER_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (chapter.IsDeleted)
                {
                    warnings.Add(localizer[ChapterLocalization.CHAPTER_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Chapter.CONTROLLER },
                        { "action", Routing.Controllers.Chapter.Routes.OVERVIEW },
                        { "id", chapter.ChapterID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
