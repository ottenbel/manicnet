﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckWorkExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckWorkExistenceBypassCacheFilter() : base(typeof(CheckWorkExistenceBypassCacheImplementation)) { }

        private sealed class CheckWorkExistenceBypassCacheImplementation : CheckWorkExistenceBase
        {
            public CheckWorkExistenceBypassCacheImplementation(IWorkRetrievalService workRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IManicNetLoggerService logger, ManicNetUserManager userManager) : base(workRetrievalService, userBlockedTagsRetrievalService, logger, userManager, true, "CheckWorkExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckWorkExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckWorkExistenceIncludeCacheFilter() : base(typeof(CheckWorkExistenceIncludeCacheImplementation)) { }

        private sealed class CheckWorkExistenceIncludeCacheImplementation : CheckWorkExistenceBase
        {
            public CheckWorkExistenceIncludeCacheImplementation(IWorkRetrievalService workRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IManicNetLoggerService logger, ManicNetUserManager userManager) : base(workRetrievalService, userBlockedTagsRetrievalService, logger, userManager, false, "CheckWorkExistenceIncludeCacheFilter") { }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckWorkExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService;
        private readonly IManicNetLoggerService logger;
        private readonly ManicNetUserManager userManager;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckWorkExistenceBase(IWorkRetrievalService workRetrievalService, IUserBlockedTagsRetrievalService userBlockedTagsRetrievalService, IManicNetLoggerService logger, ManicNetUserManager userManager, bool BypassCache, string functionIdentifier)
        {
            this.workRetrievalService = workRetrievalService;
            this.userBlockedTagsRetrievalService = userBlockedTagsRetrievalService;
            this.logger = logger;
            this.userManager = userManager;
            this.BypassCache = BypassCache;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? workID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                workID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                workID = ((WorkModifyModel)model).WorkID;
            }

            if (workID.HasValue)
            {
                try
                {
                    Work work = await workRetrievalService.FindWorkAsync(workID.Value, context.HttpContext.User, BypassCache);

                    Guid? userID = null;

                    if (context.ActionArguments.TryGetValue("rssKey", out object key))
                    {
                        Guid rssKey = (Guid)key;
                        ManicNetUser user = userManager.GetUserFromRSSKey(rssKey);
                        userID = user.Id;
                    }
                    else
                    {
                        userID = context.HttpContext.User.GetUserID();
                    }

                    if (userID.HasValue)
                    {
                        List<User_Blocked_Tag> userBlockedTags = await userBlockedTagsRetrievalService.GetUserBlockedTags(userID.Value);
                        List<Guid> tagsAssociatedWithWork = await workRetrievalService.GetAllTagsAssociatedWithWork(work.WorkID);

                        if (tagsAssociatedWithWork.Intersect(userBlockedTags.Select(ubt => ubt.TagID)).Any())
                        {
                            context.Result = new NotFoundResult();
                            return;
                        }
                    }

                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.WORK_FROM_FILTER, work);

                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_IDENTIFIER, work.WorkID);
                    context.HttpContext.Items.Add(RSSIdentifiers.COLLECTION_RSS_NAME, work.Title);

                    await next();
                }
                catch (WorkNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find work with ID {0}.", workID));
                    context.Result = new NotFoundResult();
                }
                catch (WorkNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view work with ID {1}.", context.HttpContext.User.Identity.Name, workID));
                    context.Result = new NotFoundResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex);
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
