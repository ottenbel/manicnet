﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsWorkEditableFilter : TypeFilterAttribute
    {
        public IsWorkEditableFilter() : base(typeof(IsWorkEditableImplementation)) { }

        private sealed class IsWorkEditableImplementation : IsWorkEditableBase
        {
            public IsWorkEditableImplementation(IStringLocalizer<WorkLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsWorkEditableFilter") { }
        }
    }

    public class IsWorkEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<WorkLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsWorkEditableBase(IStringLocalizer<WorkLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Work work = (Work)context.HttpContext.Items[FilterIdentifiers.WORK_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (work.IsDeleted)
                {
                    warnings.Add(localizer[WorkLocalization.WORK_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Work.CONTROLLER },
                        { "action", Routing.Controllers.Work.Routes.SHOW },
                        { "id", work.WorkID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
