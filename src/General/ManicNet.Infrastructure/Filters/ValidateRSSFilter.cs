﻿using ManicNet.Domain.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class ValidateRSSFilter : TypeFilterAttribute
    {
        public ValidateRSSFilter() : base(typeof(ValidateRSSImplementation)) { }

        private sealed class ValidateRSSImplementation : ValidateRSSBase
        {
            public ValidateRSSImplementation(IOptions<ManicNetSettings> settings, ManicNetUserManager userManager) : base(settings, userManager) { }
        }

        public class ValidateRSSBase : IAsyncActionFilter
        {
            private readonly IOptions<ManicNetSettings> settings;
            private readonly ManicNetUserManager userManager;

            public ValidateRSSBase(IOptions<ManicNetSettings> settings, ManicNetUserManager userManager)
            {
                this.settings = settings;
                this.userManager = userManager;
            }

            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                Guid? rssKey = null;

                if (context.ActionArguments.TryGetValue("rssKey", out object id))
                {
                    rssKey = (Guid?)id;
                }

                if (settings.Value.RequireAccount && (!(rssKey != null && rssKey != Guid.Empty && userManager.ValidateRSSKey(rssKey))))
                {
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    await next();
                }

            }
        }
    }
}
