﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckTagTypeExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckTagTypeExistenceBypassCacheFilter() : base(typeof(CheckTagTypeExistenceBypassCacheImplementation)) { }

        private sealed class CheckTagTypeExistenceBypassCacheImplementation : CheckTagTypeExistenceBase
        {
            public CheckTagTypeExistenceBypassCacheImplementation(ITagTypeRetrievalService tagTypeRetrievalService, IManicNetLoggerService logger) : base(tagTypeRetrievalService, logger, true, "CheckTagTypeExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckTagTypeExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckTagTypeExistenceIncludeCacheFilter() : base(typeof(CheckTagTypeExistenceIncludeCacheImplementation)) { }

        private sealed class CheckTagTypeExistenceIncludeCacheImplementation : CheckTagTypeExistenceBase
        {
            public CheckTagTypeExistenceIncludeCacheImplementation(ITagTypeRetrievalService tagTypeRetrievalService, IManicNetLoggerService logger) : base(tagTypeRetrievalService, logger, false, "CheckTagTypeExistenceIncludeCacheFilter") { }
        }
    }

    public class CheckTagTypeExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckTagTypeExistenceBase(ITagTypeRetrievalService tagTypeRetrievalService, IManicNetLoggerService logger, bool bypassCache, string functionIdentifier)
        {
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.logger = logger;
            this.BypassCache = bypassCache;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? tagTypeID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                tagTypeID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                tagTypeID = ((TagTypeModifyModel)model).TagTypeID;
            }

            if (tagTypeID.HasValue)
            {
                try
                {
                    TagType work = await tagTypeRetrievalService.FindTagTypeAsync(tagTypeID.Value, context.HttpContext.User, BypassCache);
                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.TAG_TYPE_FROM_FILTER, work);
                    await next();
                }
                catch (TagTypeNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find tag type with ID {0}.", id));
                    context.Result = new NotFoundResult();
                }
                catch (TagTypeNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view tag type with ID {1}.", context.HttpContext.User.Identity.Name, id));
                    context.Result = new NotFoundResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex);
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
