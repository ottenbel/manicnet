﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsTagTypeEditableFilter : TypeFilterAttribute
    {
        public IsTagTypeEditableFilter() : base(typeof(IsTagTypeEditableImplementation)) { }

        private sealed class IsTagTypeEditableImplementation : IsTagTypeEditableBase
        {
            public IsTagTypeEditableImplementation(IStringLocalizer<TagTypeLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsTagTypeEditableFilter") { }
        }
    }

    public class IsTagTypeEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<TagTypeLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsTagTypeEditableBase(IStringLocalizer<TagTypeLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            TagType tagType = (TagType)context.HttpContext.Items[FilterIdentifiers.TAG_TYPE_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (tagType.IsDeleted)
                {
                    warnings.Add(localizer[TagTypeLocalization.TAG_TYPE_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.TagType.CONTROLLER },
                        { "action", Routing.Controllers.TagType.Routes.SHOW },
                        { "id", tagType.TagTypeID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
