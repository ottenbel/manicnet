﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsTagNotVirtualFilter : TypeFilterAttribute
    {
        public IsTagNotVirtualFilter() : base(typeof(IsTagNotVirtualImplementation)) { }

        private sealed class IsTagNotVirtualImplementation : IsTagNotVirtualBase
        {
            public IsTagNotVirtualImplementation(IStringLocalizer<TagLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsTagNotVirtualFilter") { }
        }
    }

    public class IsTagNotVirtualBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<TagLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsTagNotVirtualBase(IStringLocalizer<TagLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Tag tag = (Tag)context.HttpContext.Items[FilterIdentifiers.TAG_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (tag.IsVirtual)
                {
                    warnings.Add(localizer[TagLocalization.VIRTUAL_TAG_CANNOT_BE_MIGRATED]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Tag.CONTROLLER },
                        { "action", Routing.Controllers.Tag.Routes.SHOW },
                        { "id", tag.TagID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
