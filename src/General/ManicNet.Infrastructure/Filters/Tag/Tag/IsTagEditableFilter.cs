﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsTagEditableFilter : TypeFilterAttribute
    {
        public IsTagEditableFilter() : base(typeof(IsTagEditableImplementation)) { }

        private sealed class IsTagEditableImplementation : IsTagEditableBase
        {
            public IsTagEditableImplementation(IStringLocalizer<TagLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsTagEditableFilter") { }
        }
    }

    public class IsTagEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<TagLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsTagEditableBase(IStringLocalizer<TagLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Tag tag = (Tag)context.HttpContext.Items[FilterIdentifiers.TAG_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (tag.IsDeleted)
                {
                    warnings.Add(localizer[TagLocalization.TAG_CANNOT_BE_EDITED_WHILE_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Tag.CONTROLLER },
                        { "action", Routing.Controllers.Tag.Routes.SHOW },
                        { "id", tag.TagID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
