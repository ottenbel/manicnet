﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckTagExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckTagExistenceBypassCacheFilter() : base(typeof(CheckTagExistenceBypassCacheImplementation)) { }

        private sealed class CheckTagExistenceBypassCacheImplementation : CheckTagExistenceBase
        {
            public CheckTagExistenceBypassCacheImplementation(ITagRetrievalService tagRetrievalService, IManicNetLoggerService logger) : base(tagRetrievalService, logger, true, "CheckTagExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckTagExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckTagExistenceIncludeCacheFilter() : base(typeof(CheckTagExistenceIncludeCacheImplementation)) { }

        private sealed class CheckTagExistenceIncludeCacheImplementation : CheckTagExistenceBase
        {
            public CheckTagExistenceIncludeCacheImplementation(ITagRetrievalService tagRetrievalService, IManicNetLoggerService logger) : base(tagRetrievalService, logger, false, "CheckTagExistenceIncludeCacheFilter") { }
        }
    }

    public class CheckTagExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly ITagRetrievalService tagRetrievalService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckTagExistenceBase(ITagRetrievalService tagService, IManicNetLoggerService logger, bool bypassCache, string functionIdentifier)
        {
            this.tagRetrievalService = tagService;
            this.logger = logger;
            this.BypassCache = bypassCache;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? tagID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                tagID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                tagID = ((TagModifyModel)model).TagID;
            }
            else if (context.ActionArguments.TryGetValue("migrationModel", out object migrationModel))
            {
                tagID = ((TagMigrationModel)migrationModel).TagID;
            }

            if (tagID.HasValue)
            {
                try
                {
                    Tag tag = await tagRetrievalService.FindTag(tagID.Value, context.HttpContext.User, BypassCache);
                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.TAG_FROM_FILTER, tag);
                    context.HttpContext.Items.Add(RSSIdentifiers.TAG_RSS_IDENTIFIER, tag.Name);
                    await next();
                }
                catch (TagNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find tag with ID {0}.", id));
                    context.Result = new NotFoundResult();
                }
                catch (TagNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view tag with ID {1}.", context.HttpContext.User.Identity.Name, id));
                    context.Result = new NotFoundResult();
                }
                catch (TagUnableToViewParentTagTypeException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Parent tag type is in a soft deleted state. User {0} doesn't have access to view soft deleted tag types.", context.HttpContext.User.Identity.Name));
                    context.Result = new NotFoundResult();
                }
                catch (Exception ex)
                {
                    logger.LogError(path, functionIdentifier, ex);
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
