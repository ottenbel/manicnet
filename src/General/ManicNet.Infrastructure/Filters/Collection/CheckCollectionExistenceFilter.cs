﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CheckCollectionExistenceBypassCacheFilter : TypeFilterAttribute
    {
        public CheckCollectionExistenceBypassCacheFilter() : base(typeof(CheckCollectionExistenceBypassCacheImplementation)) { }

        private class CheckCollectionExistenceBypassCacheImplementation : CheckCollectionExistenceBase
        {
            public CheckCollectionExistenceBypassCacheImplementation(ICollectionRetrievalService collectionRetrievalService, IManicNetLoggerService logger) : base(collectionRetrievalService, logger, true, "CheckCollectionExistenceBypassCacheFilter") { }
        }
    }

    public sealed class CheckCollectionExistenceIncludeCacheFilter : TypeFilterAttribute
    {
        public CheckCollectionExistenceIncludeCacheFilter() : base(typeof(CheckCollectionExistenceIncludeCacheImplementation)) { }

        private sealed class CheckCollectionExistenceIncludeCacheImplementation : CheckCollectionExistenceBase
        {
            public CheckCollectionExistenceIncludeCacheImplementation(ICollectionRetrievalService collectionRetrievalService, IManicNetLoggerService logger) : base(collectionRetrievalService, logger, false, "CheckCollectionExistenceIncludeCacheFilter") { }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CheckCollectionExistenceBase : IAsyncActionFilter
    {
        public bool BypassCache { get; set; }

        private readonly ICollectionRetrievalService collectionRetrievalService;
        private readonly IManicNetLoggerService logger;

        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckCollectionExistenceBase(ICollectionRetrievalService collectionRetrievalService, IManicNetLoggerService logger, bool BypassCache, string functionIdentifier)
        {
            this.collectionRetrievalService = collectionRetrievalService;
            this.logger = logger;
            this.BypassCache = BypassCache;

            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid? collectionID = null;

            if (context.ActionArguments.TryGetValue("id", out object id))
            {
                collectionID = (Guid)id;
            }
            else if (context.ActionArguments.TryGetValue("model", out object model))
            {
                collectionID = ((CollectionModifyModel)model).CollectionID;
            }

            if (collectionID.HasValue)
            {
                try
                {
                    Collection collection = await collectionRetrievalService.FindCollection(collectionID.Value, context.HttpContext.User, BypassCache);
                    //Store the work object to pass on to the appropriate function call
                    context.HttpContext.Items.Add(FilterIdentifiers.COLLECTION_FROM_FILTER, collection);

                    await next();
                }
                catch (CollectionNotFoundException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Unable to find collection with ID {0}.", collectionID));
                    context.Result = new NotFoundResult();
                }
                catch (CollectionNotViewableException ex)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("User {0} doesn't have access to view collection with ID {1}.", context.HttpContext.User.Identity.Name, collectionID));
                    context.Result = new NotFoundResult();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                await next();
            }
        }
    }
}
