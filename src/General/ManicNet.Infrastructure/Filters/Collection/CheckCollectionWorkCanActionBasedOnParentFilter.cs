﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{


    public sealed class CheckCollectionWorkAddableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckCollectionWorkAddableBasedOnParentFilter() : base(typeof(CheckCollectionWorkCreatableBasedOnParentFilterImplementation)) { }

        private sealed class CheckCollectionWorkCreatableBasedOnParentFilterImplementation : CheckCollectionWorkCanActionBasedOnParentBase
        {
            public CheckCollectionWorkCreatableBasedOnParentFilterImplementation(ICollectionRetrievalService collectionRetrievalService, IStringLocalizer<CollectionLocalization> localizer, IManicNetLoggerService logger) : base(collectionRetrievalService, localizer, logger, FilterIdentifiers.ADDING, "CheckCollectionWorkAddableBasedOnParentFilter") { }
        }
    }

    public sealed class CheckCollectionWorkEditableBasedOnParentFilter : TypeFilterAttribute
    {
        public CheckCollectionWorkEditableBasedOnParentFilter() : base(typeof(CheckCollectionWorkEditableBasedOnParentFilterImplementation)) { }

        private sealed class CheckCollectionWorkEditableBasedOnParentFilterImplementation : CheckCollectionWorkCanActionBasedOnParentBase
        {
            public CheckCollectionWorkEditableBasedOnParentFilterImplementation(ICollectionRetrievalService collectionRetrievalService, IManicNetLoggerService logger, IStringLocalizer<CollectionLocalization> localizer) : base(collectionRetrievalService, localizer, logger, FilterIdentifiers.EDITING, "CheckCollectionWorkEditableBasedOnParentFilter") { }
        }
    }

    public class CheckCollectionWorkCanActionBasedOnParentBase : IAsyncActionFilter
    {
        private readonly ICollectionRetrievalService collectionRetrievalService;
        private readonly IStringLocalizer<CollectionLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly Guid actionIdentifier;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public CheckCollectionWorkCanActionBasedOnParentBase(ICollectionRetrievalService collectionRetrievalService, IStringLocalizer<CollectionLocalization> localizer, IManicNetLoggerService logger, Guid actionIdentifier, string functionIdentifier)
        {
            this.collectionRetrievalService = collectionRetrievalService;
            this.localizer = localizer;
            this.logger = logger;
            this.actionIdentifier = actionIdentifier;
            this.functionIdentifier = functionIdentifier;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Guid? collectionID = null;

            try
            {
                bool bypassCache = false;
                collectionID = (Guid?)context.HttpContext.Items[FilterIdentifiers.COLLECTION_ID_FROM_FILTER];

                if (!collectionID.HasValue)
                {
                    if (context.ActionArguments.TryGetValue("id", out object id))
                    {
                        collectionID = (Guid)id;
                    }
                    else if (context.ActionArguments.TryGetValue("model", out object model))
                    {
                        collectionID = ((CollectionWorkAddModel)model).CollectionID;
                        bypassCache = true;
                    }
                }

                if (collectionID.HasValue)
                {
                    bool redirect = false;
                    List<string> warnings = new List<string>();

                    Collection collection = await collectionRetrievalService.FindCollection(collectionID.Value, context.HttpContext.User, bypassCache);

                    if (collection.IsDeleted)
                    {
                        if (actionIdentifier == FilterIdentifiers.ADDING)
                        {
                            logger.LogWarning(path, functionIdentifier, string.Format("Error adding work. Collection {0} must be active before adding works.", collectionID));
                            warnings.Add(localizer[CollectionLocalization.COLLECTION_MUST_BE_ACTIVE_BEFORE_ADDING_ASSOCIATED_WORKS, collection.Title]);
                        }
                        else if (actionIdentifier == FilterIdentifiers.EDITING)
                        {
                            logger.LogWarning(path, functionIdentifier, string.Format("Error editing work. Collection {0} must be active before editing works.", collectionID));
                            warnings.Add(localizer[CollectionLocalization.COLLECTION_MUST_BE_ACTIVE_BEFORE_EDITING_ASSOCIATED_WORKS, collection.Title]);
                        }

                        context.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            { "area", Routing.Areas.GENERAL },
                            { "controller", Routing.Controllers.Collection.CONTROLLER },
                            { "action", Routing.Controllers.Collection.Routes.SHOW },
                            { "id", collection.CollectionID}
                        });

                        redirect = true;
                    }

                    if (redirect)
                    {
                        controller.TempData[Alerts.TempData.WARNING] = warnings;
                    }
                    else
                    {
                        context.HttpContext.Items.Add(FilterIdentifiers.COLLECTION_FROM_FILTER, collection);

                        await next();
                    }
                }
                else
                {
                    await next();
                }
            }
            catch (CollectionNotFoundException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding work. Unable to find collection with ID {0}.", collectionID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_ADDING_WORK_TO_NON_EXISTENT_COLLECTION, collectionID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing work. Unable to find collection with ID {0}.", collectionID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_EDITING_WORK_TO_NON_EXISTENT_COLLECTION, collectionID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Collection.CONTROLLER },
                    { "action", Routing.Controllers.Collection.Routes.INDEX },
                });
            }
            catch (CollectionNotViewableException ex)
            {
                if (actionIdentifier == FilterIdentifiers.ADDING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error adding work. User doesn't have access to view collection with ID {0}.", collectionID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_ADDING_WORK_TO_NON_EXISTENT_COLLECTION, collectionID].ToString();
                }
                else if (actionIdentifier == FilterIdentifiers.EDITING)
                {
                    logger.LogWarning(path, functionIdentifier, ex, string.Format("Error editing work. User doesn't have access to view collection with ID {0}.", collectionID));
                    controller.TempData[Alerts.TempData.ERRROR] = localizer[CollectionLocalization.ERROR_EDITING_WORK_TO_NON_EXISTENT_COLLECTION, collectionID].ToString();
                }

                context.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    { "area", Routing.Areas.GENERAL },
                    { "controller", Routing.Controllers.Collection.CONTROLLER },
                    { "action", Routing.Controllers.Collection.Routes.INDEX },
                });
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
