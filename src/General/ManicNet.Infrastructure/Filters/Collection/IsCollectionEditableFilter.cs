﻿using ManicNet.Constants;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class IsCollectionEditableFilter : TypeFilterAttribute
    {
        public IsCollectionEditableFilter() : base(typeof(IsCollectionEditableImplementation)) { }

        private sealed class IsCollectionEditableImplementation : IsCollectionEditableBase
        {
            public IsCollectionEditableImplementation(IStringLocalizer<CollectionLocalization> localizer, IManicNetLoggerService logger) : base(localizer, logger, "IsCollectionEditableFilter") { }
        }
    }

    public class IsCollectionEditableBase : IAsyncActionFilter
    {
        private readonly IStringLocalizer<CollectionLocalization> localizer;
        private readonly IManicNetLoggerService logger;
        private readonly string path = "ManicNet.Infrastructure.Filters";
        private readonly string functionIdentifier;

        public IsCollectionEditableBase(IStringLocalizer<CollectionLocalization> localizer, IManicNetLoggerService logger, string functionIdentifier)
        {
            this.localizer = localizer;
            this.logger = logger;
            this.functionIdentifier = functionIdentifier;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Controller controller = context.Controller as Controller;

            Collection collection = (Collection)context.HttpContext.Items[FilterIdentifiers.COLLECTION_FROM_FILTER];
            List<string> warnings = new List<string>();

            try
            {
                if (collection.IsDeleted)
                {
                    warnings.Add(localizer[CollectionLocalization.COLLECTION_CANNOT_BE_EDITED_WHIL_IN_A_DELETED_STATE]);
                    controller.TempData[Alerts.TempData.WARNING] = warnings;
                    context.Result = new RedirectToRouteResult(new RouteValueDictionary
                    {
                        { "area", Routing.Areas.GENERAL },
                        { "controller", Routing.Controllers.Collection.CONTROLLER },
                        { "action", Routing.Controllers.Collection.Routes.SHOW },
                        { "id", collection.CollectionID }
                    });
                }
                else
                {
                    await next();
                }
            }
            catch (Exception ex)
            {
                logger.LogError(path, functionIdentifier, ex);
                throw;
            }
        }
    }
}
