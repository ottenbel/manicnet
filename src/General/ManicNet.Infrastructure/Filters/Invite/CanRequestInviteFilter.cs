﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class CanRequestInviteFilter : TypeFilterAttribute
    {
        public CanRequestInviteFilter() : base(typeof(CanRequestInviteImplementation))
        {

        }

        private sealed class CanRequestInviteImplementation : IResultFilter
        {
            private readonly IOptions<ManicNetSettings> settings;
            public CanRequestInviteImplementation(IOptions<ManicNetSettings> settings)
            {
                this.settings = settings;
            }

            public void OnResultExecuted(ResultExecutedContext context)
            {
                //Pass through without issue
            }

            public void OnResultExecuting(ResultExecutingContext context)
            {
                if (!(settings.Value.Registration.RequiresInvite && settings.Value.Registration.CanRequestInvite))
                {
                    context.Result = new NotFoundResult();
                }
            }
        }
    }
}
