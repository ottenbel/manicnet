﻿using ManicNet.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace ManicNet.Infrastructure.Filters
{
    public sealed class RequiresInviteFilter : TypeFilterAttribute
    {
        public RequiresInviteFilter() : base(typeof(RequiresInviteImplementation))
        {

        }

        private sealed class RequiresInviteImplementation : IResultFilter
        {
            private readonly IOptions<ManicNetSettings> settings;
            public RequiresInviteImplementation(IOptions<ManicNetSettings> settings)
            {
                this.settings = settings;
            }

            public void OnResultExecuted(ResultExecutedContext context)
            {
                //Pass through without issue
            }

            public void OnResultExecuting(ResultExecutingContext context)
            {
                if (!settings.Value.Registration.RequiresInvite)
                {
                    context.Result = new NotFoundResult();
                }
            }
        }
    }
}
