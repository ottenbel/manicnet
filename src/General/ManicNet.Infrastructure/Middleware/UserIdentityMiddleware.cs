﻿using ManicNet.Contracts.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Middleware
{
    public sealed class UserIdentityMiddleware
    {
        private readonly RequestDelegate _next;

        public UserIdentityMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IUserRepository userRepository)
        {
            userRepository.Configure(httpContext?.User);
            await _next(httpContext);
            
        }
    }

    public static class UserIdentityMiddlewareExtensions
    {
        public static IApplicationBuilder UseUserIdentityMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserIdentityMiddleware>();
        }
    }
}
