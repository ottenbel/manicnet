﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ManicNet.Infrastructure.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public sealed class LogErrorsMiddleware
    {
        private readonly RequestDelegate _next;

        public LogErrorsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext, IUserRepository userRepository, IManicNetLoggerService logger)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                string errorMessage = string.Format("{0}Exception caught by middleware.", userRepository.GetHumanReadableUserIdentifier());

                logger.LogCritical(ex, errorMessage);

                throw;
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LogErrorsMiddlewareExtensions
    {
        public static IApplicationBuilder UseLogErrorsMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LogErrorsMiddleware>();
        }
    }
}
