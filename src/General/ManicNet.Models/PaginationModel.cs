﻿namespace ManicNet.Models
{
    public sealed class PaginationModel
    {
        public int PageNumber { get; set; }

        public int PageLength { get; set; }

        public PaginationModel(int? pageNumber, int pageLength)
        {
            this.PageNumber = pageNumber ?? 1;
            this.PageLength = pageLength;
        }
    }
}
