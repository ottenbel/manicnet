﻿namespace ManicNet.Models
{
    public sealed class ContentModel
    {
        public string Content { get; set; }

        public string Thumbnail { get; set; }
    }
}
