﻿namespace ManicNet.Models
{
    public sealed class ImageModel
    {
        public string Content { get; set; }

        public string Thumbnail { get; set; }
    }
}
