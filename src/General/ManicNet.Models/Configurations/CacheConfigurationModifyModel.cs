﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class CacheConfigurationModifyModel
    {
        public Guid CacheConfigurationID { get; set; }
        public Guid SectionID { get; set; }
        public string SectionText { get; set; }
        public Guid LookupTypeID { get; set; }
        public string LookupTypeText { get; set; }
        public Guid LengthTypeID { get; set; }
        public List<SelectListItem> LengthTypes { get; set; }
        public int Length { get; set; }

        public CacheConfigurationModifyModel()
        {
            LengthTypes = new List<SelectListItem>();
        }

        public CacheConfigurationModifyModel(Guid cacheConfigurationID, Guid sectionID, Guid lookupTypeID, Guid lengthTypeID, int length)
        {
            this.CacheConfigurationID = cacheConfigurationID;
            this.SectionID = sectionID;
            this.LookupTypeID = lookupTypeID;
            this.LengthTypeID = lengthTypeID;
            this.Length = length;

            LengthTypes = new List<SelectListItem>();
        }
    }
}
