﻿using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class CacheConfigurationManageModel
    {
        public List<CacheConfigurationModifyModel> cacheConfigurations { get; set; }

        public CacheConfigurationManageModel()
        {
            this.cacheConfigurations = new List<CacheConfigurationModifyModel>();
        }
    }
}
