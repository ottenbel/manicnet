﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class WorkIndexViewModel : MetadataViewModel
    {
        public IPagedList<WorkSummaryViewModel> Works { get; set; }

        public string Path { get; set; }

        public string Search { get; set; }

        public bool HasDeleted { get; set; }

        public WorkIndexViewModel()
        {
            this.Image = new ContentModel();
        }

        public WorkIndexViewModel(string Path, string Search)
        {
            this.Path = Path;
            this.Search = Search;

            this.Image = new ContentModel();
        }
    }
}
