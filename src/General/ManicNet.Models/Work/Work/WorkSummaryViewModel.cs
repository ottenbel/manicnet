﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class WorkSummaryViewModel
    {
        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.COVER)]
        public ContentModel Cover { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }

        public DateTime SortabledDate { get; set; }

        public WorkSummaryViewModel()
        {
            this.Cover = new ContentModel();
            this.Tags = new List<TagTypeTagListViewModel>();
        }

        public WorkSummaryViewModel(Guid workID, string title, string shortDescription, DateTime sortableDate)
        {
            this.WorkID = workID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.SortabledDate = sortableDate;

            this.Cover = new ContentModel();
            this.Tags = new List<TagTypeTagListViewModel>();
        }
    }
}
