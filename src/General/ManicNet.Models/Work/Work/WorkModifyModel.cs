﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class WorkModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.COVER)]
        public IFormFile Cover { get; set; }

        public TagTypeTagPriorityInputModel Tags { get; set; }

        public string CoverImage { get; set; }

        [Display(Name = ModelLocalization.DELETE_COVER)]
        public bool DeleteCover { get; set; }

        public WorkModifyModel()
        {
            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }

        public WorkModifyModel(Guid workID, string title, string shortDescription, string longDescription, string URL)
        {
            this.WorkID = workID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;

            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }
    }
}
