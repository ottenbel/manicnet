﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class WorkViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.COVER)]
        public ContentModel Cover { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        public DateTime SortabledDate { get; set; }

        public bool HasDeletedVolumes { get; set; }

        public List<VolumeSummaryViewModel> Volumes { get; set; }

        public Guid? FirstChapterID { get; set; }

        public List<WorkCollectionViewModel> Collections { get; set; }

        //TODO: Related works

        public WorkViewModel()
        {
            Cover = new ContentModel();
            Tags = new List<TagTypeTagListViewModel>();
            Volumes = new List<VolumeSummaryViewModel>();
            Collections = new List<WorkCollectionViewModel>();
            Image = new ContentModel();
        }

        public WorkViewModel(Guid workID, string title, string shortDescription, string longDescription, string URL, DateTime sortableDate, bool isDeleted)
        {
            this.WorkID = workID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.SortabledDate = sortableDate;
            this.IsDeleted = isDeleted;

            Cover = new ContentModel();
            this.Tags = new List<TagTypeTagListViewModel>();
            Volumes = new List<VolumeSummaryViewModel>();
            Collections = new List<WorkCollectionViewModel>();
            Image = new ContentModel();
        }

        public override MetadataViewModel GetMetadata()
        {
            string keywords = string.Empty;
            foreach (TagTypeTagListViewModel tag in this.Tags)
            {
                keywords += string.IsNullOrWhiteSpace(keywords) ? tag.ToKeywordsString() : string.Format(", {0}", tag.ToKeywordsString());
            }

            return new MetadataViewModel()
            {
                Description = this.ShortDescription,
                Image = this.Cover,
                Keywords = keywords
            };
        }
    }
}
