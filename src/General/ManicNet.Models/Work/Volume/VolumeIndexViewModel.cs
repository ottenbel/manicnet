﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using X.PagedList;

namespace ManicNet.Models
{
    public sealed class VolumeIndexViewModel
    {
        public IPagedList<VolumeSummaryViewModel> Volumes { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid? WorkID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string WorkTitle { get; set; }

        public string Path { get; set; }

        public string Search { get; set; }

        public VolumeIndexViewModel()
        {
            //Default constructor
        }
    }
}
