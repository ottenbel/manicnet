﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class VolumeViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.NUMBER)]
        public int Number { get; set; }

        public ContentModel Cover { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.WORK_TITLE)]
        public string WorkTitle { get; set; }

        [Display(Name = ModelLocalization.IS_PARENT_DELETED)]
        public bool IsParentDeleted { get; set; }

        public List<ChapterSummaryViewModel> Chapters { get; set; }

        [Display(Name = ModelLocalization.HAS_DELETED_CHAPTERS)]
        public bool HasDeletedChapters { get; set; }

        public Guid? FirstChapterID { get; set; }

        public VolumeViewModel()
        {
            Tags = new List<TagTypeTagListViewModel>();
            Chapters = new List<ChapterSummaryViewModel>();
            Cover = new ContentModel();
            Image = new ContentModel();
        }

        public VolumeViewModel(Guid volumeID, Guid workID, int number, string title, string workTitle, string shortDescription, string longDescription, string URL,
            bool isDeleted, bool isParentDeleted)
        {
            this.VolumeID = volumeID;
            this.WorkID = workID;
            this.Number = number;
            this.Title = title;
            this.WorkTitle = workTitle;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.IsDeleted = isDeleted;
            this.IsParentDeleted = isParentDeleted;

            Tags = new List<TagTypeTagListViewModel>();
            Chapters = new List<ChapterSummaryViewModel>();
            Cover = new ContentModel();
            Image = new ContentModel();
        }

        public override MetadataViewModel GetMetadata()
        {
            string keywords = string.Empty;
            foreach (TagTypeTagListViewModel tag in this.Tags)
            {
                keywords += string.IsNullOrWhiteSpace(keywords) ? tag.ToKeywordsString() : string.Format(", {0}", tag.ToKeywordsString());
            }

            return new MetadataViewModel()
            {
                Description = this.ShortDescription,
                Image = this.Cover,
                Keywords = keywords
            };
        }
    }
}
