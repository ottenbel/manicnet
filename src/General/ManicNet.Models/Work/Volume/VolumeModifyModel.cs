﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Http;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class VolumeModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int Number { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.COVER)]
        public IFormFile Cover { get; set; }

        public TagTypeTagPriorityInputModel Tags { get; set; }

        public string CoverImage { get; set; }

        [Display(Name = ModelLocalization.DELETE_COVER)]
        public bool DeleteCover { get; set; }

        public VolumeModifyModel()
        {
            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }

        public VolumeModifyModel(Guid workID)
        {
            this.WorkID = workID;

            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }

        public VolumeModifyModel(Guid volumeID, Guid workID, int number, string title, string shortDescription, string longDescription, string URL)
        {
            this.VolumeID = volumeID;
            this.WorkID = workID;
            this.Number = number;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;

            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }
    }
}
