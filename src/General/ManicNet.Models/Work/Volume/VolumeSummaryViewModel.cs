﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public sealed class VolumeSummaryViewModel
    {
        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        public ContentModel Cover { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.WORK_TITLE)]
        public string WorkTitle { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int Number { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }

        public List<ChapterSummaryViewModel> Chapters { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsExpanded { get; set; }

        public VolumeSummaryViewModel()
        {
            Tags = new List<TagTypeTagListViewModel>();
            Chapters = new List<ChapterSummaryViewModel>();
        }

        public VolumeSummaryViewModel(Guid VolumeID, Guid WorkID, string Title, string WorkTitle, int Number,
            string ShortDescription, bool IsDeleted, DateTime UpdatedDate)
        {
            this.VolumeID = VolumeID;
            this.WorkID = WorkID;
            this.Title = Title;
            this.WorkTitle = WorkTitle;
            this.Number = Number;
            this.ShortDescription = ShortDescription;
            this.IsDeleted = IsDeleted;
            this.UpdatedDate = UpdatedDate;
            this.IsExpanded = false;

            Tags = new List<TagTypeTagListViewModel>();
            Chapters = new List<ChapterSummaryViewModel>();
        }

        public bool HasSummaryToDisplay()
        {
            return !string.IsNullOrWhiteSpace(Cover.Thumbnail) || !string.IsNullOrWhiteSpace(ShortDescription) || Tags.Any();
        }
    }
}
