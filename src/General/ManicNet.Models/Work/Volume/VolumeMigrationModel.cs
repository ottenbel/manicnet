﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class VolumeMigrationModel
    {
        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int Number { get; set; }

        [Display(Name = ModelLocalization.DESTINATION_WORK)]
        public Guid? WorkID { get; set; }

        public VolumeMigrationModel()
        {
            //Default constructor
        }

        public VolumeMigrationModel(Guid volumeID, int number)
        {
            this.VolumeID = volumeID;
            this.Number = number;
        }
    }
}
