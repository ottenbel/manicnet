﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class ChapterModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.CHAPTER_IDENTIFIER)]
        public Guid ChapterID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.CHAPTER_NUMBER)]
        public int Number { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.FILES)]
        public IFormFile[] Files { get; set; }

        public TagTypeTagPriorityInputModel Tags { get; set; }

        public List<PageModifyViewModel> Pages { get; set; }

        public ChapterModifyModel()
        {
            Pages = new List<PageModifyViewModel>();
            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }

        public ChapterModifyModel(Guid volumeID)
        {
            this.VolumeID = volumeID;

            Pages = new List<PageModifyViewModel>();
            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }

        public ChapterModifyModel(Guid chapterID, Guid volumeID, int number, string title, string shortDescription, string longDescription, string URL)
        {
            this.ChapterID = chapterID;
            this.VolumeID = volumeID;
            this.Number = number;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;

            Pages = new List<PageModifyViewModel>();
            Tags = new TagTypeTagPriorityInputModel();
            Image = new ContentModel();
        }
    }
}
