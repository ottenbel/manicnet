﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class ChapterMigrationModel
    {
        [Display(Name = ModelLocalization.CHAPTER_IDENTIFIER)]
        public Guid ChapterID { get; set; }

        [Display(Name = ModelLocalization.CHAPTER_NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.CHAPTER_NUMBER)]
        public int Number { get; set; }

        [Display(Name = ModelLocalization.DESTINATION_VOLUME)]
        public Guid? VolumeID { get; set; }

        public List<SelectListItem> CollectionVolumes { get; set; }

        [Display(Name = ModelLocalization.MIGRATE_CHAPTER_TO_VOLUME_OUTSIDE_OF_PARENT_WORK)]
        public bool MigrateToVolumeOutsideOfCollection { get; set; }

        public ChapterMigrationModel()
        {
            this.CollectionVolumes = new List<SelectListItem>();
        }

        public ChapterMigrationModel(Guid chapterID, int number)
        {
            this.ChapterID = chapterID;
            this.Number = number;

            this.CollectionVolumes = new List<SelectListItem>();
        }
    }
}
