﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public class ChapterSummaryViewModel
    {
        [Display(Name = ModelLocalization.CHAPTER_IDENTIFIER)]
        public Guid ChapterID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.CHAPTER_NUMBER)]
        public int Number { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime UpdatedDate { get; set; }

        public bool IsExpanded { get; set; }

        public ChapterSummaryViewModel()
        {
            Tags = new List<TagTypeTagListViewModel>();
        }

        public ChapterSummaryViewModel(Guid chapterID, string title, int number, string shortDescription, bool isDeleted, DateTime UpdatedDate)
        {
            this.ChapterID = chapterID;
            this.Title = title;
            this.Number = number;
            this.ShortDescription = shortDescription;
            this.IsDeleted = isDeleted;
            this.UpdatedDate = UpdatedDate;
            this.IsExpanded = false;

            Tags = new List<TagTypeTagListViewModel>();
        }

        public bool HasSummaryToDisplay()
        {
            return !string.IsNullOrWhiteSpace(ShortDescription) || Tags.Any();
        }
    }
}
