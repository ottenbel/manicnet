﻿using ManicNet.Localization;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class PageViewModel
    {
        public ContentModel Content { get; set; }

        [Display(Name = ModelLocalization.PAGE_NUMBER)]
        public int PageNumber { get; set; }

        public PageViewModel()
        {
            this.Content = new ContentModel();
        }
    }
}
