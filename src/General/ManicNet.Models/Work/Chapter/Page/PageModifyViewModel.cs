﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class PageModifyViewModel
    {
        [Display(Name = ModelLocalization.PAGE_IDENTIFIER)]
        public Guid PageID { get; set; }

        [Display(Name = ModelLocalization.PAGE_NUMBER)]
        public int PageNumber { get; set; }

        [Display(Name = ModelLocalization.DELETE_PAGE)]
        public bool Delete { get; set; }

        public ContentModel Content { get; set; }

        public PageModifyViewModel()
        {
            this.Delete = false;
            this.Content = new ContentModel();
        }
    }
}
