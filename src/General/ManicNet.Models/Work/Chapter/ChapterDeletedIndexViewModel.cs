﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;
using X.PagedList;

namespace ManicNet.Models
{
    public sealed class ChapterDeletedIndexViewModel
    {
        public IPagedList<ChapterDeletedSummaryViewModel> Chapters { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid? WorkID { get; set; }

        [Display(Name = ModelLocalization.WORK_TITLE)]
        public string WorkTitle { get; set; }

        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid? VolumeID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int? VolumeNumber { get; set; }

        [Display(Name = ModelLocalization.VOLUME_TITLE)]
        public string VolumeTitle { get; set; }

        public string Path { get; set; }

        public string Search { get; set; }
    }
}
