﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class ChapterDeletedSummaryViewModel : ChapterSummaryViewModel
    {
        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int VolumeNumber { get; set; }

        [Display(Name = ModelLocalization.VOLUME_TITLE)]
        public string VolumeTitle { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.WORK_TITLE)]
        public string WorkTitle { get; set; }

        public ChapterDeletedSummaryViewModel()
        {
            Tags = new List<TagTypeTagListViewModel>();
        }

        public ChapterDeletedSummaryViewModel(Guid volumeID, int volumeNumber, string volumeTitle, Guid workID, string workTitle, Guid chapterID, string title, int number, string shortDescription, bool isDeleted, DateTime UpdatedDate)
        {
            this.VolumeID = volumeID;
            this.VolumeNumber = volumeNumber;
            this.VolumeTitle = volumeTitle;
            this.WorkID = workID;
            this.WorkTitle = workTitle;
            this.ChapterID = chapterID;
            this.Title = title;
            this.Number = number;
            this.ShortDescription = shortDescription;
            this.IsDeleted = isDeleted;
            this.UpdatedDate = UpdatedDate;

            Tags = new List<TagTypeTagListViewModel>();
        }

        public string GetParentVolumeTitle()
        {
            string output = string.Format("Volume {0}", VolumeNumber);

            if (!string.IsNullOrWhiteSpace(VolumeTitle))
            {
                output += string.Format(" - {0}", VolumeTitle);
            }

            return output;
        }

        public string GetTitle()
        {
            string output = string.Format("Chapter {0}", Number);

            if (!string.IsNullOrWhiteSpace(Title))
            {
                output += string.Format(" - {0}", Title);
            }

            return output;
        }
    }
}
