﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class ChapterViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.CHAPTER_IDENTIFIER)]
        public Guid ChapterID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.NUMBER)]
        public int Number { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }

        [Display(Name = ModelLocalization.WORK_TITLE)]
        public string WorkTitle { get; set; }

        [Display(Name = ModelLocalization.VOLUME_IDENTIFIER)]
        public Guid VolumeID { get; set; }

        [Display(Name = ModelLocalization.VOLUME_NUMBER)]
        public int VolumeNumber { get; set; }

        [Display(Name = ModelLocalization.VOLUME_TITLE)]
        public string VolumeTitle { get; set; }

        [Display(Name = ModelLocalization.ARE_PARENTS_DELETED)]
        public bool AreParentsDeleted { get; set; }

        [Display(Name = ModelLocalization.NEXT_CHAPTER_IDENTIFIER)]
        public Guid? NextChapterID { get; set; }

        [Display(Name = ModelLocalization.PREVIOUS_CHAPTER_IDENTIFIER)]
        public Guid? PreviousChapterID { get; set; }

        [Display(Name = ModelLocalization.PREVIOUS_CHAPTER_LAST_PAGE)]
        public int? PreviousChapterLastPage { get; set; }

        [Display(Name = ModelLocalization.CURRENT_PAGE_NUMBER)]
        public int? CurrentPageNumber { get; set; }

        public ContentModel CurrentPage { get; set; }

        public List<PageViewModel> Pages { get; set; }

        public ChapterViewModel()
        {
            this.Tags = new List<TagTypeTagListViewModel>();
            this.Pages = new List<PageViewModel>();
            this.CurrentPage = new ContentModel();
            this.Image = new ContentModel();
        }

        public ChapterViewModel(Guid chapterID, string title, int number, string shortDescription, string longDescription, string URL, bool isDeleted,
            Guid workID, string workTitle, Guid volumeID, int volumeNumber, string volumeTitle, bool areParentsDeleted, Guid? previousChapterID,
            Guid? nextChapterID, int? previousChapterLastPage, int? currentPageNumber)
        {
            this.ChapterID = chapterID;
            this.Title = title;
            this.Number = number;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.IsDeleted = isDeleted;
            this.WorkID = workID;
            this.WorkTitle = workTitle;
            this.VolumeID = volumeID;
            this.VolumeNumber = volumeNumber;
            this.VolumeTitle = volumeTitle;
            this.AreParentsDeleted = areParentsDeleted;
            this.PreviousChapterID = previousChapterID;
            this.NextChapterID = nextChapterID;
            this.PreviousChapterLastPage = previousChapterLastPage;

            this.CurrentPageNumber = currentPageNumber ?? 0;

            this.Tags = new List<TagTypeTagListViewModel>();
            this.Pages = new List<PageViewModel>();
            this.CurrentPage = new ContentModel();
            this.Image = new ContentModel();
        }

        public string GetChapterTitle()
        {
            string output = string.Format("{0} - Chapter {1}", WorkTitle, Number);

            if (!string.IsNullOrWhiteSpace(Title))
            {
                output += string.Format(" - {0}", Title);
            }

            return output;
        }

        public override MetadataViewModel GetMetadata()
        {
            string keywords = string.Empty;
            foreach (TagTypeTagListViewModel tag in this.Tags)
            {
                keywords += string.IsNullOrWhiteSpace(keywords) ? tag.ToKeywordsString() : string.Format(", {0}", tag.ToKeywordsString());
            }

            return new MetadataViewModel()
            {
                Description = this.ShortDescription,
                Keywords = keywords
            };
        }
    }
}
