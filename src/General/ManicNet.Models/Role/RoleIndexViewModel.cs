﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class RoleIndexViewModel
    {
        public IPagedList<IndexRole> Roles { get; set; }

        public RoleIndexViewModel()
        {
            //Default constructor
        }
    }

    public sealed class IndexRole
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public bool IsDefaultUser { get; set; }
    }
}
