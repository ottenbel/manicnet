﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class RoleEditModel
    {
        [Display(Name = ModelLocalization.IDENTIFIER)]
        public Guid ID { get; set; }

        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.DESCRIPTION)]
        public string Description { get; set; }

        [Display(Name = ModelLocalization.IS_DEFAULT_USER_ROLE)]
        public bool IsDefaultUser { get; set; }

        public List<PermissionSection> Permissions { get; set; }

        public RoleEditModel()
        {
            Permissions = new List<PermissionSection>();
        }
    }
}
