﻿using ManicNet.Domain.Models;
using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class TagViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.TAG_IDENTIFIER)]
        public Guid TagID { get; set; }

        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        public TagTypeViewModel TagType { get; set; }

        [Display(Name = ModelLocalization.COUNT)]
        public int Count { get; set; }

        [Display(Name = ModelLocalization.IS_VIRTUAL)]
        public bool IsVirtual { get; set; }

        [Display(Name = ModelLocalization.IS_PARENT_DELETED)]
        public bool IsParentDeleted { get; set; }

        [Display(Name = ModelLocalization.IS_PRIMARY)]
        public bool IsPrimary { get; set; }

        [Display(Name = ModelLocalization.IMPLIED_BY)]
        public List<TagTypeTagListViewModel> ImpliedBy { get; set; }

        [Display(Name = ModelLocalization.IMPLIES)]
        public List<TagTypeTagListViewModel> Implies { get; set; }

        [Display(Name = ModelLocalization.ALIASES)]
        public List<string> Aliases { get; set; }

        public TagViewModel()
        {
            this.TagType = new TagTypeViewModel();
            this.ImpliedBy = new List<TagTypeTagListViewModel>();
            this.Implies = new List<TagTypeTagListViewModel>();
            this.Aliases = new List<string>();
            this.Image = new ContentModel();
            this.IsPrimary = false;
        }

        public TagViewModel(Tag tag)
        {
            this.TagID = tag.TagID;
            this.Name = tag.Name;
            this.ShortDescription = tag.ShortDescription;
            this.LongDescription = tag.LongDescription;
            this.URL = tag.URL;
            this.Count = tag.Count;
            this.IsVirtual = tag.IsVirtual;
            this.IsPrimary = tag.IsPrimary;
            this.IsDeleted = tag.IsDeleted;
            this.IsParentDeleted = tag.TagType != null && tag.TagType.IsDeleted;

            this.TagType = new TagTypeViewModel();
            this.ImpliedBy = new List<TagTypeTagListViewModel>();
            this.Implies = new List<TagTypeTagListViewModel>();
            this.Aliases = new List<string>();
            this.Image = new ContentModel();
        }

        public string GetSearchName()
        {
            return string.Format("{0}:{1}", TagType.Name, this.Name);
        }
    }
}
