﻿using System;
using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class TagTypeTagInputModel
    {
        public List<DynamicTagInput> Tags { get; set; }

        public bool AllowVirtual { get; set; }

        public string Header { get; set; }

        public TagTypeTagInputModel()
        {
            this.Tags = new List<DynamicTagInput>();
        }
    }

    public sealed class TagTypeTagPriorityInputModel
    {
        public List<DynamicTagInput> PrimaryTags { get; set; }

        public List<DynamicTagInput> SecondaryTags { get; set; }

        public bool AllowVirtual { get; set; }

        public TagTypeTagPriorityInputModel()
        {
            this.PrimaryTags = new List<DynamicTagInput>();
            this.SecondaryTags = new List<DynamicTagInput>();
        }
    }

    public sealed class DynamicTagInput
    {
        public SimplifiedTagType TagType { get; set; }
        public string Tags { get; set; }

        public DynamicTagInput()
        {
            this.TagType = new SimplifiedTagType();
        }
    }

    public sealed class SimplifiedTagType
    {
        public Guid TagTypeID { get; set; }
        public string Name { get; set; }
        public int Priority { get; set; }

        public SimplifiedTagType()
        {
            //Default constructor
        }

        public SimplifiedTagType(Guid tagTypeID, string name, int priority)
        {
            this.TagTypeID = tagTypeID;
            this.Name = name;
            this.Priority = priority;
        }
    }
}
