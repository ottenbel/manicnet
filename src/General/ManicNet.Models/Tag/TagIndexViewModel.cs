﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class TagIndexViewModel
    {
        public IPagedList<TagViewModel> Tags { get; set; }

        public string Path { get; set; }

        public string Search { get; set; }

        public bool HasDeleted { get; set; }

        public TagIndexViewModel()
        {
            //Default constructor
        }

        public TagIndexViewModel(string Path, string Search)
        {
            this.Path = Path;
            this.Search = Search;
        }
    }
}
