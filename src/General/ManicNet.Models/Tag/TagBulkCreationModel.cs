﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public sealed class TagBulkCreationModel
    {
        [Display(Name = ModelLocalization.TAG_TYPE_IDENTIFIER)]
        public Guid TagTypeID { get; set; }

        public List<SelectListItem> TagTypes { get; set; }

        [Display(Name = ModelLocalization.NAMES)]
        public string Names { get; set; }

        [Display(Name = ModelLocalization.ICON)]
        public string Icon { get; set; }
        [Display(Name = ModelLocalization.FONT_FAMILY)]
        public Guid? FontFamily { get; set; }

        [Display(Name = ModelLocalization.IS_VIRTUAL)]
        public bool IsVirtual { get; set; }

        public TagTypeTagInputModel ImpliedTags { get; set; }
        public List<SelectListItem> FontFamilies { get; set; }

        public TagBulkCreationModel()
        {
            TagTypes = new List<SelectListItem>();
            ImpliedTags = new TagTypeTagInputModel();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }
    }
}
