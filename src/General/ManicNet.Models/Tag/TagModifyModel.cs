﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public sealed class TagModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.TAG_IDENTIFIER)]
        public Guid TagID { get; set; }

        [Display(Name = ModelLocalization.TAG_TYPE)]
        public Guid TagTypeID { get; set; }

        public List<SelectListItem> TagTypes { get; set; }

        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.ALIASES)]
        public string Aliases { get; set; }

        [Display(Name = ModelLocalization.ICON)]
        public string Icon { get; set; }
        [Display(Name = ModelLocalization.FONT_FAMILY)]
        public Guid? FontFamily { get; set; }

        [Display(Name = ModelLocalization.IS_VIRTUAL)]
        public bool IsVirtual { get; set; }

        public TagTypeTagInputModel ImpliedTags { get; set; }

        public List<SelectListItem> FontFamilies { get; set; }

        public TagModifyModel()
        {
            TagTypes = new List<SelectListItem>();
            ImpliedTags = new TagTypeTagInputModel();
            this.Image = new ContentModel();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }

        public TagModifyModel(Guid TagID, Guid TagTypeID, string Name, string ShortDescription, string LongDescription, string URL, bool IsVirtual, string icon, Guid? fontFamily)
        {
            this.TagID = TagID;
            this.TagTypeID = TagTypeID;
            this.Name = Name;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
            this.IsVirtual = IsVirtual;
            this.Icon = icon;
            this.FontFamily = fontFamily;

            TagTypes = new List<SelectListItem>();
            ImpliedTags = new TagTypeTagInputModel();
            this.Image = new ContentModel();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }
    }
}
