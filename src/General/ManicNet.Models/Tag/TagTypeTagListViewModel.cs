﻿using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class TagTypeTagListViewModel
    {
        public List<TagViewModel> Tags { get; set; }

        public TagTypeViewModel TagType { get; set; }

        public TagTypeTagListViewModel()
        {
            this.Tags = new List<TagViewModel>();
            this.TagType = new TagTypeViewModel();
        }

        public string ToKeywordsString()
        {
            string keywords = string.Empty;

            foreach (TagViewModel tag in Tags)
            {
                string fragment = string.IsNullOrWhiteSpace(keywords) ? string.Empty : ", ";

                fragment = string.Format("{0}{1}:{2}", fragment, TagType.SearchablePrefix, tag.Name);
                keywords += fragment;
            }

            return keywords;
        }
    }
}
