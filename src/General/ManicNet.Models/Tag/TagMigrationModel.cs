﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class TagMigrationModel
    {
        [Display(Name = ModelLocalization.TAG_IDENTIFIER)]
        public Guid TagID { get; set; }

        [Display(Name = ModelLocalization.TAG_NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.DESTINATION_TAG_NAME)]
        public string DestinationTagName { get; set; }

        [Display(Name = ModelLocalization.CONVERT_MIGRATED_TAG_TO_DESTINATION_ALIAS)]
        public bool ConvertMigratedTagToDestinationAlias { get; set; }

        [Display(Name = ModelLocalization.REDIRECT_ALIASES_ON_MIGRATED_TAG_TO_DESTINATION_TAG)]
        public bool RedirectMigratedAliasesToDestination { get; set; }

        public TagMigrationModel()
        {
            //Default constructor
        }

        public TagMigrationModel(Guid tagID, string name)
        {
            this.TagID = tagID;
            this.Name = name;
        }
    }
}
