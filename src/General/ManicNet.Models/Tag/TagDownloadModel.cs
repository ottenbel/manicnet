﻿using System;

namespace ManicNet.Models
{
    public sealed class TagDownloadModel
    {
        public Guid TagID { get; set; }
        public string Name { get; set; }
        public Guid TagTypeID { get; set; }
        public string TagTypeName { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string URL { get; set; }

        public TagDownloadModel()
        {
            //Default constructor
        }

        public TagDownloadModel(Guid TagID, string Name, Guid TagTypeID, string TagTypeName, string ShortDescription, string LongDescription, string URL)
        {
            this.TagID = TagID;
            this.Name = Name;
            this.TagTypeID = TagTypeID;
            this.TagTypeName = TagTypeName;
            this.ShortDescription = ShortDescription;
            this.LongDescription = LongDescription;
            this.URL = URL;
        }
    }
}
