﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class CollectionModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }
        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        public List<CollectionWorkModifyModel> Works { get; set; }

        public CollectionModifyModel()
        {
            this.Works = new List<CollectionWorkModifyModel>();
            this.Image = new ContentModel();
        }

        public CollectionModifyModel(Guid collectionID, string title, string shortDescription, string longDescription, string URL)
        {
            this.CollectionID = collectionID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;

            this.Works = new List<CollectionWorkModifyModel>();
            this.Image = new ContentModel();
        }
    }
}
