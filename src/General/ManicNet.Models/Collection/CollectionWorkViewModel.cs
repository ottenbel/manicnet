﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public sealed class CollectionWorkViewModel
    {
        [Display(Name = ModelLocalization.NUMBER)]
        public int Number { get; set; }
        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }
        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        public ContentModel Cover { get; set; }

        public List<TagTypeTagListViewModel> Tags { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }
        [Display(Name = ModelLocalization.IS_EXPANDED)]
        public bool IsExpanded { get; set; }

        public CollectionWorkViewModel()
        {
            this.IsExpanded = false;

            this.Cover = new ContentModel();
            this.Tags = new List<TagTypeTagListViewModel>();
        }

        public CollectionWorkViewModel(int number, Guid workID, string title, string shortDescription)
        {
            this.Number = number;
            this.WorkID = workID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.IsExpanded = false;

            this.Tags = new List<TagTypeTagListViewModel>();
            this.Cover = new ContentModel();
        }

        public bool HasSummaryToDisplay()
        {
            return !string.IsNullOrWhiteSpace(Cover.Thumbnail) || !string.IsNullOrWhiteSpace(ShortDescription) || Tags.Any();
        }
    }
}
