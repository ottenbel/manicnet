﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class WorkCollectionViewModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }
        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }
        [Display(Name = ModelLocalization.IS_EXPANDED)]
        public bool IsExpanded { get; set; }

        public WorkCollectionViewModel()
        {
            //Default constructor
        }

        public WorkCollectionViewModel(Guid collectionID, string title, string shortDescription)
        {
            this.CollectionID = collectionID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.IsExpanded = false;
        }

        public bool HasSummaryToDisplay()
        {
            return !string.IsNullOrWhiteSpace(this.ShortDescription);
        }
    }
}
