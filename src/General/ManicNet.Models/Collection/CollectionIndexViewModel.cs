﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class CollectionIndexViewModel : MetadataViewModel
    {
        public IPagedList<CollectionSummaryViewModel> Collections { get; set; }

        public string Path { get; set; }

        public bool HasDeleted { get; set; }

        public CollectionIndexViewModel()
        {
            this.Image = new ContentModel();
        }

        public CollectionIndexViewModel(string Path)
        {
            this.Path = Path;
            this.Image = new ContentModel();
        }
    }
}
