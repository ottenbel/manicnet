﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class CollectionWorkModifyModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }
        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid WorkID { get; set; }
        [Display(Name = ModelLocalization.NUMBER)]
        public int Number { get; set; }
        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }
        [Display(Name = ModelLocalization.DELETE)]
        public bool Delete { get; set; }

        public CollectionWorkModifyModel()
        {
            //Default constructor
        }

        public CollectionWorkModifyModel(Guid collectionID, Guid workID, int number, string title)
        {
            this.CollectionID = collectionID;
            this.WorkID = workID;
            this.Number = number;
            this.Title = title;
            this.Delete = false;
        }
    }
}
