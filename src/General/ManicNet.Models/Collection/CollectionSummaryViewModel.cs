﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class CollectionSummaryViewModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }

        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }

        [Display(Name = ModelLocalization.WORKS_IN_COLLECTION)]
        public int WorkCount { get; set; }

        public CollectionSummaryViewModel()
        {
            //Default constructor
        }

        public CollectionSummaryViewModel(Guid collectionID, string title, string shortDescription, int workCount)
        {
            this.CollectionID = collectionID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.WorkCount = workCount;
        }
    }
}
