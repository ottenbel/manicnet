﻿using ManicNet.Localization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class CollectionViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }
        [Display(Name = ModelLocalization.TITLE)]
        public string Title { get; set; }

        public List<CollectionWorkViewModel> Works { get; set; }

        public CollectionViewModel()
        {
            this.Works = new List<CollectionWorkViewModel>();
            this.Image = new ContentModel();
        }

        public CollectionViewModel(Guid collectionID, string title, string shortDescription, string longDescription, string URL, bool isDeleted)
        {
            this.CollectionID = collectionID;
            this.Title = title;
            this.ShortDescription = shortDescription;
            this.LongDescription = longDescription;
            this.URL = URL;
            this.IsDeleted = isDeleted;

            this.Works = new List<CollectionWorkViewModel>();
            this.Image = new ContentModel();
        }
    }
}
