﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class CollectionWorkAddModel
    {
        [Display(Name = ModelLocalization.COLLECTION_IDENTIFIER)]
        public Guid CollectionID { get; set; }

        [Display(Name = ModelLocalization.WORK_IDENTIFIER)]
        public Guid? WorkID { get; set; }

        [Display(Name = ModelLocalization.NUMBER)]
        public int Number { get; set; }

        public CollectionWorkAddModel()
        {
            //Default constructor
        }

        public CollectionWorkAddModel(Guid collectionID)
        {
            this.CollectionID = collectionID;
        }
    }
}
