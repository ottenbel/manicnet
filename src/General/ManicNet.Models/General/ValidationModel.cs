﻿using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class ValidationModel
    {
        public Dictionary<string, string> ModelState { get; set; }

        public bool IsValid { get; set; }

        public ValidationModel()
        {
            ModelState = new Dictionary<string, string>();
            this.IsValid = true;
        }

        public ValidationModel(bool isValid)
        {
            ModelState = new Dictionary<string, string>();
            this.IsValid = isValid;
        }

        public ValidationModel(Dictionary<string, string> modelState)
        {
            this.ModelState = modelState;
            IsValid = true;
        }
    }
}
