﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class TagTypeIndexViewModel
    {
        public IPagedList<TagTypeViewModel> TagTypes { get; set; }

        public string Path { get; set; }

        public bool HasDeleted { get; set; }
    }
}
