﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ManicNet.Models
{
    public sealed class TagTypeModifyModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.TAG_TYPE_IDENTIFIER)]
        public Guid TagTypeID { get; set; }

        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.SEARCHABLE_PREFIX)]
        public string SearchablePrefix { get; set; }

        [Display(Name = ModelLocalization.PRIORITY)]
        public int Priority { get; set; }

        [Display(Name = ModelLocalization.PRIMARY_BODY_COLOUR)]
        public string PrimaryBodyColour { get; set; }

        [Display(Name = ModelLocalization.PRIMARY_BORDER_COLOUR)]
        public string PrimaryBorderColour { get; set; }

        [Display(Name = ModelLocalization.PRIMARY_TEXT_COLOUR)]
        public string PrimaryTextColour { get; set; }

        [Display(Name = ModelLocalization.SECONDARY_BODY_COLOUR)]
        public string SecondaryBodyColour { get; set; }

        [Display(Name = ModelLocalization.SECONDARY_BORDER_COLOUR)]
        public string SecondaryBorderColour { get; set; }

        [Display(Name = ModelLocalization.SECONDARY_TEXT_COLOUR)]
        public string SecondaryTextColour { get; set; }

        [Display(Name = ModelLocalization.ICON)]
        public string Icon { get; set; }

        [Display(Name = ModelLocalization.FONT_FAMILY)]
        public Guid? FontFamily { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_WORK)]
        public bool AvailableOnWork { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_VOLUME)]
        public bool AvailableOnVolume { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_CHAPTER)]
        public bool AvailableOnChapter { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_WORK_SUMMARY)]
        public bool DisplayedInWorkSummary { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_VOLUME_SUMMARY)]
        public bool DisplayedInVolumeSummary { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_CHAPTER_SUMMARY)]
        public bool DisplayedInChapterSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInWorkSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInVolumeSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInChapterSummary { get; set; }
        public List<SelectListItem> FontFamilies { get; set; }

        public TagTypeModifyModel()
        {
            this.Image = new ContentModel();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }

        public TagTypeModifyModel(string primaryBodyColour, string primaryBorderColour, string primaryTextColour, string secondaryBodyColour, string secondaryBorderColour, string secondaryTextColour, string icon, Guid fontFamily, int priority, int numberDisplayedInSummary)
        {
            this.PrimaryBodyColour = primaryBodyColour;
            this.PrimaryBorderColour = primaryBorderColour;
            this.PrimaryTextColour = primaryTextColour;
            this.SecondaryBodyColour = secondaryBodyColour;
            this.SecondaryBorderColour = secondaryBorderColour;
            this.SecondaryTextColour = secondaryTextColour;
            this.Icon = icon;
            this.FontFamily = fontFamily;
            this.Priority = priority;
            this.NumberDisplayedInWorkSummary = numberDisplayedInSummary;
            this.NumberDisplayedInVolumeSummary = numberDisplayedInSummary;
            this.NumberDisplayedInChapterSummary = numberDisplayedInSummary;

            this.Image = new ContentModel();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }
    }
}
