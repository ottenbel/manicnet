﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class TagTypeViewModel : DescribableAndDeletableViewModel
    {
        [Display(Name = ModelLocalization.TAG_TYPE_IDENTIFIER)]
        public Guid TagTypeID { get; set; }
        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.SEARCHABLE_PREFIX)]
        public string SearchablePrefix { get; set; }

        [Display(Name = ModelLocalization.PRIORITY)]
        public int Priority { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_WORK)]
        public bool AvailableOnWork { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_VOLUME)]
        public bool AvailableOnVolume { get; set; }

        [Display(Name = ModelLocalization.AVAILIABLE_ON_CHAPTER)]
        public bool AvailableOnChapter { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_WORK_SUMMARY)]
        public bool DisplayedInWorkSummary { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_VOLUME_SUMMARY)]
        public bool DisplayedInVolumeSummary { get; set; }

        [Display(Name = ModelLocalization.DISPLAYED_ON_CHAPTER_SUMMARY)]
        public bool DisplayedInChapterSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInWorkSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInVolumeSummary { get; set; }

        [Display(Name = ModelLocalization.NUMBER_OF_TAGS_DISPLAYED)]
        public int? NumberDisplayedInChapterSummary { get; set; }

        public TagTypeViewModel()
        {
            this.Image = new ContentModel();
        }
    }
}
