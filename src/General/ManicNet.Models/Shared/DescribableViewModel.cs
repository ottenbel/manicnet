﻿using ManicNet.Localization;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public class DescribableViewModel : MetadataViewModel
    {
        [Display(Name = ModelLocalization.SUMMARY)]
        public string ShortDescription { get; set; }

        [Display(Name = ModelLocalization.DETAILS)]
        public string LongDescription { get; set; }
        [Display(Name = ModelLocalization.URL)]
        public string URL { get; set; }

        public override MetadataViewModel GetMetadata()
        {
            return new MetadataViewModel()
            {
                Description = this.ShortDescription
            };
        }
    }
}
