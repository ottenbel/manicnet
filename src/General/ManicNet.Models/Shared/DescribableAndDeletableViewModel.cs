﻿using ManicNet.Localization;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public class DescribableAndDeletableViewModel : DescribableViewModel
    {
        [Display(Name = ModelLocalization.IS_DELETED)]
        public bool IsDeleted { get; set; }
    }
}
