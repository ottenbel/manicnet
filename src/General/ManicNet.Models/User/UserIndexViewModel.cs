﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class UserIndexViewModel
    {
        public IPagedList<IndexUser> Users { get; set; }

        public string Search { get; set; }

        public UserIndexViewModel()
        {
            //Default constructor
        }

        public UserIndexViewModel(string search)
        {
            Search = search;
        }
    }

    public sealed class IndexUser
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }
}
