﻿using System;
using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class UserEditViewModel
    {
        public Guid ID { get; set; }

        public string Username { get; set; }

        public List<PermissionSection> Permissions { get; set; }

        public RoleSection Roles { get; set; }

        public UserEditViewModel()
        {
            Permissions = new List<PermissionSection>();
        }
    }
}
