﻿using ManicNet.Constants.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Claims;

namespace ManicNet.Models
{
    public sealed class PermissionSection
    {
        public string SectionName { get; set; }

        public List<ManicNetPermission> OwnerPermissions { get; set; }

        public List<ManicNetPermission> AdministratorPermissions { get; set; }

        public List<ManicNetPermission> NormalPermissions { get; set; }

        public bool HasOwnerPermission { get; set; }

        public bool HasAdministratorPermission { get; set; }

        public bool HasNormalPermission { get; set; }

        public PermissionSection()
        {
            OwnerPermissions = new List<ManicNetPermission>();
            AdministratorPermissions = new List<ManicNetPermission>();
            NormalPermissions = new List<ManicNetPermission>();
        }

        public PermissionSection(PermissionsSection section, ICollection<Claim> claims, bool hasOwnerPermission, bool hasAdministratorPermission, bool hasNormalPermission)
        {
            OwnerPermissions = new List<ManicNetPermission>();
            AdministratorPermissions = new List<ManicNetPermission>();
            NormalPermissions = new List<ManicNetPermission>();

            claims ??= new List<Claim>();

            SectionName = section.SectionName;
            foreach (string permisison in section.OwnerPermissions)
            {
                OwnerPermissions.Add(new ManicNetPermission(permisison, claims.Any(x => x.Value == permisison && x.Type == Constants.ManicNetPermissions.PERMISSION_CLAIM_TYPE)));
            }
            HasOwnerPermission = hasOwnerPermission;

            foreach (string permisison in section.AdministratorPermissions)
            {
                AdministratorPermissions.Add(new ManicNetPermission(permisison, claims.Any(x => x.Value == permisison && x.Type == Constants.ManicNetPermissions.PERMISSION_CLAIM_TYPE)));
            }
            HasAdministratorPermission = hasAdministratorPermission;

            foreach (string permisison in section.NormalPermissions)
            {
                NormalPermissions.Add(new ManicNetPermission(permisison, claims.Any(x => x.Value == permisison && x.Type == Constants.ManicNetPermissions.PERMISSION_CLAIM_TYPE)));
            }
            HasNormalPermission = hasNormalPermission;
        }
    }

    public sealed class ManicNetPermission : IEquatable<ManicNetPermission>, IComparable<ManicNetPermission>
    {
        public string PermissionName { get; set; }
        public bool HasPermission { get; set; }

        public ManicNetPermission()
        {

        }

        public ManicNetPermission(string permissionName, bool hasPermission)
        {
            PermissionName = permissionName;
            HasPermission = hasPermission;
        }

        public bool Equals(ManicNetPermission p)
        {
            // If parameter is null, return false.
            if (p is null)
            {
                return false;
            }

            // Optimization for a common success case.
            if (Object.ReferenceEquals(this, p))
            {
                return true;
            }

            // If run-time types are not exactly the same, return false.
            if (this.GetType() != p.GetType())
            {
                return false;
            }

            return (this.PermissionName == p.PermissionName) && (this.HasPermission == p.HasPermission);
        }

        public override bool Equals(object obj)
        {
            return obj is ManicNetPermission permissionObject && Equals(permissionObject);
        }

        public override int GetHashCode()
        {
            return 0;
        }

        public int CompareTo([AllowNull] ManicNetPermission other)
        {
            return string.Compare(this.PermissionName, other.PermissionName);
        }
    }
}
