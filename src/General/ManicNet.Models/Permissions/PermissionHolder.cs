﻿namespace ManicNet.Models
{
    public sealed class PermissionHolder
    {
        public bool HasOwnerPermission { get; set; }

        public bool HasAdministratorPermission { get; set; }

        public bool HasUserPermission { get; set; }

        public PermissionHolder()
        {
            //Default constructor
        }

        public PermissionHolder(bool hasOwnerPermission, bool hasAdministratorPermission, bool hasUserPermission)
        {
            this.HasOwnerPermission = hasOwnerPermission;
            this.HasAdministratorPermission = hasAdministratorPermission;
            this.HasUserPermission = hasUserPermission;
        }
    }
}
