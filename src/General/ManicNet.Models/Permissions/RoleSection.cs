﻿using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class RoleSection
    {
        public List<SectionRole> OwnerRoles { get; set; }

        public List<SectionRole> AdministratorRoles { get; set; }

        public List<SectionRole> NormalRoles { get; set; }

        public bool HasOwnerPermission { get; set; }

        public bool HasAdministratorPermission { get; set; }

        public bool HasNormalPermission { get; set; }

        public RoleSection()
        {
            OwnerRoles = new List<SectionRole>();
            AdministratorRoles = new List<SectionRole>();
            NormalRoles = new List<SectionRole>();
        }
    }

    public sealed class SectionRole
    {
        public string RoleName { get; set; }
        public bool HasRole { get; set; }

        public SectionRole()
        {
            //Default constructor
        }

        public SectionRole(string roleName, bool hasRole)
        {
            RoleName = roleName;
            HasRole = hasRole;
        }
    }
}
