﻿using System.Collections.Generic;
using System.Security.Claims;

namespace ManicNet.Models
{
    public sealed class UpdateClaimsHolder
    {
        public List<Claim> UserClaims { get; set; }

        public List<Claim> ClaimsToAdd { get; set; }

        public List<Claim> ClaimsToRemove { get; set; }

        public UpdateClaimsHolder()
        {
            this.UserClaims = new List<Claim>();
            this.ClaimsToAdd = new List<Claim>();
            this.ClaimsToRemove = new List<Claim>();
        }

        public UpdateClaimsHolder(List<Claim> userClaims)
        {
            this.UserClaims = userClaims ?? new List<Claim>();
            this.ClaimsToAdd = new List<Claim>();
            this.ClaimsToRemove = new List<Claim>();
        }
    }
}
