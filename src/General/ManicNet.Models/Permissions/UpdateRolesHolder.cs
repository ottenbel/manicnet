﻿using ManicNet.Domain.Models;
using System.Collections.Generic;

namespace ManicNet.Models
{
    public sealed class UpdateRolesHolder
    {
        public List<ManicNetRole> UserRoles { get; set; }

        public List<SectionRole> RolesToAdd { get; set; }

        public List<SectionRole> RolesToRemove { get; set; }

        public UpdateRolesHolder()
        {
            this.UserRoles = new List<ManicNetRole>();
            this.RolesToAdd = new List<SectionRole>();
            this.RolesToRemove = new List<SectionRole>();
        }

        public UpdateRolesHolder(List<ManicNetRole> userRoles)
        {
            this.UserRoles = userRoles ?? new List<ManicNetRole>();
            this.RolesToAdd = new List<SectionRole>();
            this.RolesToRemove = new List<SectionRole>();
        }
    }
}
