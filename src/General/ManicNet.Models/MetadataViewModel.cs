﻿namespace ManicNet.Models
{
    public class MetadataViewModel
    {
        public string Author { get; set; }

        public string Description { get; set; }

        public ContentModel Image { get; set; }

        public string Keywords { get; set; }

        public string CanonicalURL { get; set; }

        public virtual MetadataViewModel GetMetadata()
        {
            return this;
        }
    }
}
