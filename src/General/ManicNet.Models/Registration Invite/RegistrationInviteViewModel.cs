﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class RegistrationInviteViewModel
    {
        [Display(Name = ModelLocalization.REGISTRATION_INVITE_IDENTIFIER)]
        public string RegistrationInviteID { get; set; }

        [Display(Name = ModelLocalization.EMAIL)]
        public string Email { get; set; }

        [Display(Name = ModelLocalization.EXPIRES)]
        public DateTime Expires { get; set; }

        [Display(Name = ModelLocalization.APPROVED)]
        public bool Approved { get; set; }

        [Display(Name = ModelLocalization.NOTE)]
        public string Note { get; set; }
    }
}
