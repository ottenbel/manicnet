﻿using ManicNet.Localization;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public sealed class RequestInviteCreationModel
    {
        [Display(Name = ModelLocalization.EMAIL)]
        public string Email { get; set; }

        [Display(Name = ModelLocalization.NOTE)]
        public string Note { get; set; }
    }
}
