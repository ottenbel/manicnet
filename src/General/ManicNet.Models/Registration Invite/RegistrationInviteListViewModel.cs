﻿using X.PagedList;

namespace ManicNet.Models
{
    public sealed class RegistrationInviteListViewModel
    {
        public IPagedList<RegistrationInviteViewModel> invites { get; set; }

    }
}
