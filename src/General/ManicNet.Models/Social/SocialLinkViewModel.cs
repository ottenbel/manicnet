﻿using ManicNet.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace ManicNet.Models
{
    public class SocialLinkViewModel
    {
        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.SOCIAL_LINK_IDENTIFIER)]
        public Guid SocialLinkID { get; set; }
        [Display(Name = ModelLocalization.PRIORITY)]
        public int Priority { get; set; }
        [Display(Name = ModelLocalization.ICON)]
        public string Icon { get; set; }
        [Display(Name = ModelLocalization.FONT_FAMILY)]
        public Guid? FontFamily { get; set; }
        [Display(Name = ModelLocalization.URL)]
        public string URL { get; set; }
        [Display(Name = ModelLocalization.LINK_TEXT)]
        public string Text { get; set; }
        [Display(Name = ModelLocalization.LINK_TYPE_IDENTIFIER)]
        public Guid TypeID { get; set; }
        [Display(Name = ModelLocalization.LINK_TYPE)]
        public string TypeName { get; set; }
        [Display(Name = ModelLocalization.IS_DELETED)]
        public bool IsDeleted { get; set; }
    }
}
