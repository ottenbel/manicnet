﻿using ManicNet.Localization;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Models
{
    public class SocialLinkModifyModel
    {
        [Display(Name = ModelLocalization.SOCIAL_LINK_IDENTIFIER)]
        public Guid SocialLinkID { get; set; }

        [Display(Name = ModelLocalization.NAME)]
        public string Name { get; set; }

        [Display(Name = ModelLocalization.PRIORITY)]
        public int Priority { get; set; }
        [Display(Name = ModelLocalization.ICON)]
        public string Icon { get; set; }
        [Display(Name = ModelLocalization.FONT_FAMILY)]
        public Guid? FontFamily {get;set;}
        [Display(Name = ModelLocalization.URL)]
        public string URL { get; set; }
        [Display(Name = ModelLocalization.LINK_TEXT)]
        public string Text { get; set; }
        [Display(Name = ModelLocalization.LINK_TYPE_IDENTIFIER)]
        public Guid TypeID { get; set; }

        public List<SelectListItem> LinkTypes { get; set; }
        public List<SelectListItem> FontFamilies { get; set; }

        public SocialLinkModifyModel()
        {
            LinkTypes = new List<SelectListItem>();
            FontFamilies = Constants.FrontEnd.FONT_FAMILY_MAP.Select(x => new SelectListItem(x.Value, x.Key.ToString())).ToList();
        }
    }
}
