﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Models
{
    public class SocialLinkIndexViewModel
    {
        public List<SocialLinkViewModel> Links { get; set; }
        public bool HasDeleted { get; set; }

        public SocialLinkIndexViewModel()
        {
            Links = new List<SocialLinkViewModel>();
        }
    }
}
