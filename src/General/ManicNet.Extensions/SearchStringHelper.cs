﻿using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Extensions
{
    public static class SearchStringHelper
    {
        /// <summary>
        /// Transforms a comma seperated string into a list of distinct, trimmed, non-empty parts.
        /// </summary>
        /// <param name="input">A comma seperated string to be transformed into a list of strings.</param>
        /// <returns></returns>
        public static List<string> FromCommaSeparatedStringToDistinctPopulatedList(this string input)
        {
            List<string> result = new List<string>();
            if (input == null)
            {
                return result;
            }

            result = input.Split(",").ToList();

            for (int t = 0; t < result.Count; t++)
            {
                result[t] = result[t].Trim();
            }

            result = result.Distinct().Where(x => x != string.Empty).ToList();

            return result;
        }

    }
}
