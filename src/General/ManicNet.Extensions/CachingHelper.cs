﻿using ManicNet.Constants;
using ManicNet.Domain.Models;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ManicNet.Extensions
{
    public static class CachingHelper
    {
        /// <summary>
        /// Convert the provided object to a byte array and save to the cache using the provided key value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public static async Task SetSerializedAsync<T>(this IDistributedCache cache, string key, T source, CacheConfiguration cacheConfiguration)
        {
            byte[] content = SerializeToBytes<T>(source);
            DistributedCacheEntryOptions options = BuildOptions(cacheConfiguration);
            if (options == null)
            {
                await cache.SetAsync(key, content);
            }
            else
            {
                await cache.SetAsync(key, content, options);
            }
        }

        public static void SetSerialized<T>(this IDistributedCache cache, string key, T source, CacheConfiguration cacheConfiguration)
        {
            byte[] content = SerializeToBytes<T>(source);
            DistributedCacheEntryOptions options = BuildOptions(cacheConfiguration);
            if (options == null)
            {
                cache.Set(key, content);
            }
            else
            {
                cache.Set(key, content, options);
            }
        }

        /// <summary>
        /// Checks the cache for an instance of a key and returns the stored binary array into an instance of the provided class.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="cache"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<T> GetDeserializedAsync<T>(this IDistributedCache cache, string key)
        {
            byte[] result = await cache.GetAsync(key);
            if (result == null)
            {
                return default;
            }
            else
            {
                return await result.DeserializeFromBytesAsync<T>();
            }
        }

        public static T GetDeserialized<T>(this IDistributedCache cache, string key)
        {
            byte[] result = cache.Get(key);
            if (result == null)
            {
                return default;
            }
            else
            {
                return result.DeserializeFromBytes<T>();
            }
        }


        private static byte[] SerializeToBytes<T>(this T source)
        {
            var options = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };

            return JsonSerializer.SerializeToUtf8Bytes(source, options);
        }

        private static async Task<T> DeserializeFromBytesAsync<T>(this byte[] source)
        {
            var options = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };

            using var stream = new MemoryStream(source);
            return (await JsonSerializer.DeserializeAsync<T>(stream, options));
        }

        private static T DeserializeFromBytes<T>(this byte[] source)
        {
            var options = new JsonSerializerOptions
            {
                ReferenceHandler = ReferenceHandler.Preserve,
            };

            return JsonSerializer.Deserialize<T>(source, options);
        }

        private static DistributedCacheEntryOptions BuildOptions(CacheConfiguration cacheConfiguration)
        {
            DistributedCacheEntryOptions options = null;

            if (cacheConfiguration.Length > 0)
            {
                if (cacheConfiguration.LengthTypeID == Caching.Configurations.LengthTypes.SECOND)
                {
                    options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(cacheConfiguration.Length));
                }
                else if (cacheConfiguration.LengthTypeID == Caching.Configurations.LengthTypes.MINUTE)
                {
                    options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromMinutes(cacheConfiguration.Length));
                }
                else if (cacheConfiguration.LengthTypeID == Caching.Configurations.LengthTypes.HOUR)
                {
                    options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromHours(cacheConfiguration.Length));
                }
                else if (cacheConfiguration.LengthTypeID == Caching.Configurations.LengthTypes.DAY)
                {
                    options = new DistributedCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(cacheConfiguration.Length));
                }
            }

            return options;
        }
    }
}
