﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManicNet.Validators.SocialLink
{
    public class SocialLinkModifyModelValidator : AbstractValidator<SocialLinkModifyModel>
    {
        private readonly IStringLocalizer<SocialLocalization> localizer;

        public SocialLinkModifyModelValidator(IStringLocalizer<SocialLocalization> localizer)
        {
            this.localizer = localizer;

            RuleFor(x => x.Name).NotEmpty().MaximumLength(10).Matches(Constants.Regex.SOCIAL_LINK_NAME);
            RuleFor(x => x.Priority).NotEmpty().InclusiveBetween(0, int.MaxValue);
            RuleFor(x => x.Icon).MaximumLength(6);
            RuleFor(x => x.URL).NotEmpty()
                .ValidateURL().When(x => x.TypeID == Constants.Social.LinkTypes.LINK, ApplyConditionTo.CurrentValidator)
                .ValidateEmail().When(x => x.TypeID == Constants.Social.LinkTypes.EMAIL, ApplyConditionTo.CurrentValidator);
            RuleFor(x => x.Text).NotEmpty();

            ValidateType();
            ValidateIconType();
        }

        private void ValidateType()
        {
            List<Guid> validLinks = new List<Guid>() 
            { 
                Constants.Social.LinkTypes.LINK,
                Constants.Social.LinkTypes.EMAIL,
            };

            RuleFor(x => x).Custom((socialLinkModel, context) =>
            {
                if (!validLinks.Contains(socialLinkModel.TypeID))
                {
                    context.AddFailure(new ValidationFailure("TypeID", localizer[SocialLocalization.Validation.INVALID_TYPE]));
                }
            });
        }

        private void ValidateIconType()
        {
            RuleFor(x => x).Custom((socialLinkModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(socialLinkModel.Icon))
                {
                    if (!socialLinkModel.FontFamily.HasValue || socialLinkModel.FontFamily == Guid.Empty)
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[SocialLocalization.Validation.FONT_FAMILY_IS_REQUIRED]));
                    }
                    else if (!Constants.FrontEnd.FONT_FAMILIES.Contains(socialLinkModel.FontFamily.Value))
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[SocialLocalization.Validation.FONT_FAMILY_IS_UNKNOWN]));
                    }
                }
            });
        }
    }
}
