﻿using FluentValidation;
using FluentValidation.Validators;
using ManicNet.Contracts.Services;
using Microsoft.AspNetCore.Http;
using System.Web;
using static ManicNet.Domain.Enums.FileValidation;

namespace ManicNet.Validators
{
    public sealed class ImageValidator<T, TProperty> : PropertyValidator<T, TProperty>
    {
        private readonly IImageProcessingService imageProcessingService;

        public ImageValidator(IImageProcessingService imageProcessingService)
        {
            this.imageProcessingService = imageProcessingService;
        }

        public override string Name => "ImageValidator";

        public override bool IsValid(ValidationContext<T> context, TProperty value)
        {
            IFormFile file = value as IFormFile;

            if (file != null)
            {
                FileValidationResult validationResult = imageProcessingService.ValidateImage(file);

                if (validationResult != FileValidationResult.IsValid)
                {
                    context.MessageFormatter.AppendArgument("filename", HttpUtility.HtmlEncode(file.FileName));

                    string problem = validationResult switch
                    {
                        FileValidationResult.FileTooLarge => "was too large",
                        FileValidationResult.FileTooSmall => "was too small",
                        FileValidationResult.UnsupportedImageType => "is not of an accepted image type",
                        FileValidationResult.InvalidFileType => "is not an image",
                        FileValidationResult.UnknownValidationErrror => " failed validation with an unspecified error",
                        _ => "returned an unexpected validation result",
                    };

                    context.MessageFormatter.AppendArgument("problem", problem);
                    return false;
                }
            }

            return true;
        }

        protected override string GetDefaultMessageTemplate(string errorCode) => "{PropertyName} image uploaded ({filename}) {problem}.";
    }
}