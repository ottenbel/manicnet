﻿using FluentValidation;
using FluentValidation.Validators;
using System;

namespace ManicNet.Validators
{
    public sealed class URLValidator<T, TProperty> : PropertyValidator<T, TProperty>
    {
        public override string Name => "URLValidator";

        public override bool IsValid(ValidationContext<T> context, TProperty value)
        {
            string url = value as string;

            if (!string.IsNullOrEmpty(url))
            {
                if (!Uri.TryCreate(url, UriKind.Absolute, out Uri generated))
                {
                    return false;
                }
            }

            return true;
        }

        protected override string GetDefaultMessageTemplate(string errorCode) => "{PropertyName} is not a valid url.";
    }
}
