﻿using FluentValidation;
using ManicNet.Contracts.Services;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class WorkModifyModelValidator : AbstractValidator<WorkModifyModel>
    {
        public WorkModifyModelValidator(IImageProcessingService imageProcessingService)
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Cover).ValidateImage(imageProcessingService);
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(x => x.URL).ValidateURL();
        }
    }
}
