﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Contracts.Services;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;
using System.Linq;

namespace ManicNet.Validators
{
    public sealed class ChapterModifyModelValidator : AbstractValidator<ChapterModifyModel>
    {
        private readonly IStringLocalizer<ChapterLocalization> localizer;

        public ChapterModifyModelValidator(IImageProcessingService imageProcessingService, IStringLocalizer<ChapterLocalization> localizer)
        {
            RuleFor(c => c.Number).NotEmpty();
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(c => c.URL).ValidateURL();
            RuleForEach(c => c.Files).ValidateImage(imageProcessingService);

            this.localizer = localizer;

            PagesAreRequired();
        }

        private void PagesAreRequired()
        {
            RuleFor(x => x).Custom((chapterModifyModel, context) =>
            {
                if ((chapterModifyModel.Files != null && !chapterModifyModel.Files.Any()) || (chapterModifyModel.Files == null && !chapterModifyModel.Pages.Any(p => !p.Delete)))
                {
                    context.AddFailure(new ValidationFailure("Pages", localizer[ChapterLocalization.Validation.PAGES_ARE_REQUIRED]));
                }
            });
        }
    }
}
