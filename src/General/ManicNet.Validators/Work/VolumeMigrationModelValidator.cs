﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class VolumeMigrationModelValidator : AbstractValidator<VolumeMigrationModel>
    {
        public VolumeMigrationModelValidator()
        {
            RuleFor(c => c.VolumeID).NotEmpty();
            RuleFor(c => c.WorkID).NotEmpty();
            RuleFor(c => c.Number).NotEmpty();
        }
    }
}
