﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class ChapterMigrationModelValidator : AbstractValidator<ChapterMigrationModel>
    {
        public ChapterMigrationModelValidator()
        {
            RuleFor(c => c.ChapterID).NotEmpty();
            RuleFor(c => c.Number).NotEmpty();
            RuleFor(c => c.VolumeID).NotEmpty();
        }
    }
}
