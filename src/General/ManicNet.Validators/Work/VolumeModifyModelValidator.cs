﻿using FluentValidation;
using ManicNet.Contracts.Services;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class VolumeModifyModelValidator : AbstractValidator<VolumeModifyModel>
    {
        public VolumeModifyModelValidator(IImageProcessingService imageProcessingService)
        {
            RuleFor(x => x.Number).NotEmpty();
            RuleFor(x => x.Cover).ValidateImage(imageProcessingService);
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(x => x.URL).ValidateURL();
        }
    }
}
