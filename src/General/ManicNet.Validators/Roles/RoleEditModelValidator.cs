﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Domain.Models;
using ManicNet.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;

namespace ManicNet.Validators
{
    public sealed class RoleEditModelValidator : AbstractValidator<RoleEditModel>
    {
        private readonly RoleManager<ManicNetRole> roleManager;
        public RoleEditModelValidator(RoleManager<ManicNetRole> roleManager)
        {
            this.roleManager = roleManager;

            RuleFor(x => x.Name).NotEmpty();
            ValidateName();
        }

        private void ValidateName()
        {
            RuleFor(x => x).Custom((roleEditModel, context) =>
            {
                if (roleManager.Roles.Where(r => r.Name == roleEditModel.Name && r.Id != roleEditModel.ID).Any())
                {
                    //TODO: Localize this
                    context.AddFailure(new ValidationFailure("Name", string.Format("'Name' {0} is not unique.", roleEditModel.Name)));
                }
            });
        }
    }
}
