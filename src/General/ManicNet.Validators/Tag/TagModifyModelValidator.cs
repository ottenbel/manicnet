﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ManicNet.Validators
{
    public sealed class TagModifyModelValidator : AbstractValidator<TagModifyModel>
    {
        private readonly ICommonTagService commonTagService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IStringLocalizer<TagLocalization> localizer;

        public TagModifyModelValidator(ICommonTagService commonTagService, ITagTypeRetrievalService tagTypeRetrievalService, IHttpContextAccessor httpContextAccessor, IStringLocalizer<TagLocalization> localizer)
        {
            this.commonTagService = commonTagService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.httpContextAccessor = httpContextAccessor;
            this.localizer = localizer;

            RuleFor(x => x.TagTypeID).NotEmpty();
            RuleFor(x => x.Name).NotEmpty().Matches(Constants.Regex.TAG_NAME);
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(x => x.URL).ValidateURL();
            RuleFor(x => x.Icon).MaximumLength(6);

            ValidateName();
            TagTypeExistsAndIsActive();
            ValidateAliases();
            ValidateIconType();
        }

        private void ValidateName()
        {
            RuleFor(x => x).Custom((tagModifyModel, context) =>
            {
                if (!commonTagService.HasUniqueName(tagModifyModel.Name, tagModifyModel.TagID))
                {
                    context.AddFailure(new ValidationFailure("Name", localizer[TagLocalization.Validation.NAME_IS_NOT_UNIQUE, tagModifyModel.Name]));
                }
            });
        }

        private void TagTypeExistsAndIsActive()
        {
            RuleFor(x => x).Custom((tagModifyModel, context) =>
            {
                try
                {
                    TagType tagType = tagTypeRetrievalService.FindTagType(tagModifyModel.TagTypeID, httpContextAccessor.HttpContext.User);

                    if (tagType.IsDeleted)
                    {
                        context.AddFailure(new ValidationFailure("TagTypeID", localizer[TagLocalization.Validation.TAG_TYPE_MUST_EXIST_AND_BE_ACTIVE]));
                    }
                }
                catch (Exception)
                {
                    context.AddFailure(new ValidationFailure("TagTypeID", localizer[TagLocalization.Validation.TAG_TYPE_MUST_EXIST_AND_BE_ACTIVE]));
                }
            });
        }

        private void ValidateAliases()
        {
            RuleFor(x => x).Custom((tagModifyModel, context) =>
            {
                List<string> aliases = SearchStringHelper.FromCommaSeparatedStringToDistinctPopulatedList(tagModifyModel.Aliases);

                foreach (var alias in aliases)
                {
                    if (!Regex.Match(alias, Constants.Regex.TAG_NAME).Success)
                    {
                        context.AddFailure(new ValidationFailure("Aliases", localizer[TagLocalization.Validation.ALIAS_IS_NOT_IN_CORRECT_FORMAT, alias]));
                    }
                    else if (alias.Trim() == tagModifyModel.Name || !commonTagService.HasUniqueAlias(alias, tagModifyModel.TagID))
                    {
                        context.AddFailure(new ValidationFailure("Aliases", localizer[TagLocalization.Validation.ALIAS_IS_NOT_UNIQUE, alias]));
                    }
                }
            });
        }

        private void ValidateIconType()
        {
            RuleFor(x => x).Custom((tagModifyModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(tagModifyModel.Icon))
                {
                    if (!tagModifyModel.FontFamily.HasValue || tagModifyModel.FontFamily == Guid.Empty)
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagLocalization.Validation.FONT_FAMILY_IS_REQUIRED]));
                    }
                    else if (!Constants.FrontEnd.FONT_FAMILIES.Contains(tagModifyModel.FontFamily.Value))
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagLocalization.Validation.FONT_FAMILY_IS_UNKNOWN]));
                    }
                }
            });
        }
    }
}
