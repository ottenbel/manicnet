﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class TagMigrationModelValidator : AbstractValidator<TagMigrationModel>
    {
        public TagMigrationModelValidator()
        {
            RuleFor(x => x.TagID).NotEmpty();
            RuleFor(x => x.DestinationTagName).NotEmpty();
        }
    }
}
