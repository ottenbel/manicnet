﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Extensions;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ManicNet.Validators
{
    public sealed class TagBulkCreationModelValidator : AbstractValidator<TagBulkCreationModel>
    {
        private readonly ICommonTagService commonTagService;
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IStringLocalizer<TagLocalization> localizer;

        public TagBulkCreationModelValidator(ICommonTagService commonTagService, ITagTypeRetrievalService tagTypeRetrievalService, IHttpContextAccessor httpContextAccessor, IStringLocalizer<TagLocalization> localizer)
        {
            this.commonTagService = commonTagService;
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.httpContextAccessor = httpContextAccessor;
            this.localizer = localizer;

            RuleFor(x => x.TagTypeID).NotEmpty();
            RuleFor(x => x.Names).NotEmpty();
            RuleFor(x => x.Icon).MaximumLength(6);

            ValidateNames();
            TagTypeExistsAndIsActive();
            ValidateIconType();
        }

        private void ValidateNames()
        {
            RuleFor(x => x).Custom((bulkCreationModel, context) =>
            {
                List<string> names = bulkCreationModel.Names.FromCommaSeparatedStringToDistinctPopulatedList();

                foreach (string name in names)
                {
                    if (!Regex.Match(name, Constants.Regex.TAG_NAME).Success)
                    {
                        context.AddFailure(new ValidationFailure("Name", localizer[TagLocalization.Validation.NAME_IS_NOT_IN_CORRECT_FORMAT, name]));
                    }
                    else if (!commonTagService.HasUniqueName(name, null))
                    {
                        context.AddFailure(new ValidationFailure("Name", localizer[TagLocalization.Validation.NAME_IS_NOT_UNIQUE, name]));
                    }
                }
            });
        }

        private void TagTypeExistsAndIsActive()
        {
            RuleFor(x => x).Custom((bulkCreationModel, context) =>
            {
                TagType tagType;

                try
                {
                    tagType = tagTypeRetrievalService.FindTagType(bulkCreationModel.TagTypeID, httpContextAccessor.HttpContext.User);

                    if (tagType.IsDeleted)
                    {
                        context.AddFailure(new ValidationFailure("TagTypeID", localizer[TagLocalization.Validation.TAG_TYPE_MUST_EXIST_AND_BE_ACTIVE]));
                    }
                }
                catch (Exception)
                {
                    context.AddFailure(new ValidationFailure("TagTypeID", localizer[TagLocalization.Validation.TAG_TYPE_MUST_EXIST_AND_BE_ACTIVE]));
                }
            });
        }

        private void ValidateIconType()
        {
            RuleFor(x => x).Custom((bulkCreationModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(bulkCreationModel.Icon))
                {
                    if (!bulkCreationModel.FontFamily.HasValue || bulkCreationModel.FontFamily == Guid.Empty)
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagLocalization.Validation.FONT_FAMILY_IS_REQUIRED]));
                    }
                    else if (!Constants.FrontEnd.FONT_FAMILIES.Contains(bulkCreationModel.FontFamily.Value))
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagLocalization.Validation.FONT_FAMILY_IS_UNKNOWN]));
                    }
                }
            });
        }
    }
}
