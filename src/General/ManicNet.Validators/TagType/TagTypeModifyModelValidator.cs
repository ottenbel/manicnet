﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using System;
using System.Linq;

namespace ManicNet.Validators
{
    public sealed class TagTypeModifyModelValidator : AbstractValidator<TagTypeModifyModel>
    {
        private readonly ITagTypeRetrievalService tagTypeRetrievalService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IStringLocalizer<TagTypeLocalization> localizer;

        public TagTypeModifyModelValidator(ITagTypeRetrievalService tagTypeRetrievalService, IHttpContextAccessor httpContextAccessor, IStringLocalizer<TagTypeLocalization> localizer)
        {
            this.tagTypeRetrievalService = tagTypeRetrievalService;
            this.httpContextAccessor = httpContextAccessor;
            this.localizer = localizer;

            RuleFor(x => x.Name).NotEmpty().Matches(Constants.Regex.TAG_TYPE_NAME);
            RuleFor(x => x.SearchablePrefix).NotEmpty().MaximumLength(20).Matches(Constants.Regex.TAG_TYPE_PREFIX);
            RuleFor(x => x.Priority).NotEmpty().InclusiveBetween(0, int.MaxValue);
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(x => x.URL).ValidateURL();
            RuleFor(x => x.PrimaryBodyColour).Matches(Constants.Regex.COLOUR).When(x => x.PrimaryBodyColour.Any()).MaximumLength(7);
            RuleFor(x => x.PrimaryBorderColour).Matches(Constants.Regex.COLOUR).When(x => x.PrimaryBorderColour.Any()).MaximumLength(7);
            RuleFor(x => x.PrimaryTextColour).Matches(Constants.Regex.COLOUR).When(x => x.PrimaryTextColour.Any()).MaximumLength(7);
            RuleFor(x => x.SecondaryBodyColour).Matches(Constants.Regex.COLOUR).When(x => x.SecondaryBodyColour.Any()).MaximumLength(7);
            RuleFor(x => x.SecondaryBorderColour).Matches(Constants.Regex.COLOUR).When(x => x.SecondaryBorderColour.Any()).MaximumLength(7);
            RuleFor(x => x.SecondaryTextColour).Matches(Constants.Regex.COLOUR).When(x => x.SecondaryTextColour.Any()).MaximumLength(7);
            RuleFor(x => x.Icon).MaximumLength(6);

            HasUniqueName();
            HasUniqueSearchablePrefix();
            SummaryMappingToWorkTypeIsValid();
            ValidateSummaryNumbers();
            ValidateIconType();
        }

        private void HasUniqueName()
        {
            RuleFor(x => x).Custom((tagTypeModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(tagTypeModel.Name) && !tagTypeRetrievalService.HasUniqueName(tagTypeModel.Name, tagTypeModel.TagTypeID))
                {
                    context.AddFailure(new ValidationFailure("Name", localizer[TagTypeLocalization.Validation.NAME_MUST_BE_UNIQUE]));
                }
            });
        }

        private void HasUniqueSearchablePrefix()
        {
            RuleFor(x => x).Custom((tagTypeModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(tagTypeModel.SearchablePrefix) && !tagTypeRetrievalService.HasUniqueSearchablePrefix(tagTypeModel.SearchablePrefix, tagTypeModel.TagTypeID))
                {
                    context.AddFailure(new ValidationFailure("SearchablePrefix", localizer[TagTypeLocalization.Validation.SEARCHABLE_PREFIX_MUST_BE_UNIQUE]));
                }
            });
        }

        private void SummaryMappingToWorkTypeIsValid()
        {
            RuleFor(x => x).Custom((tagTypeModel, context) =>
            {
                if (!(tagTypeModel.AvailableOnWork || tagTypeModel.AvailableOnVolume || tagTypeModel.AvailableOnChapter))
                {
                    context.AddFailure(new ValidationFailure("TagTypeWorkAssociation", localizer[TagTypeLocalization.TAG_TYPE_MUST_BE_USABLE]));
                }

                if (tagTypeModel.TagTypeID != Guid.Empty)
                {
                    //Grab the tag type shallowly for initial case
                    TagType tagType = tagTypeRetrievalService.FindTagType(tagTypeModel.TagTypeID, httpContextAccessor.HttpContext.User);

                    bool workConflict = (tagType.AvailableOnWork && !tagTypeModel.AvailableOnWork);
                    bool volumeConflict = (tagType.AvailableOnVolume && !tagTypeModel.AvailableOnVolume);
                    bool chapterConflict = (tagType.AvailableOnChapter && !tagTypeModel.AvailableOnChapter);

                    if (workConflict || volumeConflict || chapterConflict)
                    {
                        tagType = tagTypeRetrievalService.FindTagTypeWithWorkAssociation(tagTypeModel.TagTypeID, httpContextAccessor.HttpContext.User);

                        if (workConflict && tagType.Tags.SelectMany(t => t.Works).Any())
                        {
                            context.AddFailure(new ValidationFailure("TagTypeWorkAssociation", localizer[TagTypeLocalization.Validation.TAGS_OF_THIS_TYPE_EXIST_ON_WORKS]));
                        }

                        if (volumeConflict && tagType.Tags.SelectMany(t => t.Volumes).Any())
                        {
                            context.AddFailure(new ValidationFailure("TagTypeVolumeAssociation", localizer[TagTypeLocalization.Validation.TAGS_OF_THIS_TYPE_EXIST_ON_VOLUMES]));
                        }

                        if (chapterConflict && tagType.Tags.SelectMany(t => t.Chapters).Any())
                        {
                            context.AddFailure(new ValidationFailure("TagTypeChapterAssociation", localizer[TagTypeLocalization.Validation.TAGS_OF_THIS_TYPE_EXIST_ON_CHAPTERS]));
                        }
                    }
                }
            });
        }

        private void ValidateSummaryNumbers()
        {
            RuleFor(x => x).Custom((tagTypeModel, context) =>
            {
                if (tagTypeModel.DisplayedInWorkSummary && (tagTypeModel.NumberDisplayedInWorkSummary == null || tagTypeModel.NumberDisplayedInWorkSummary.Value < 1))
                {
                    context.AddFailure(new ValidationFailure("NumberDisplayedInWorkSummary", localizer[TagTypeLocalization.Validation.NUMBER_OF_TAGS_ON_WORK_SUMMARY_MUST_BE_GREATER_THEN_ZERO]));
                }

                if (tagTypeModel.DisplayedInVolumeSummary && (tagTypeModel.NumberDisplayedInVolumeSummary == null || tagTypeModel.NumberDisplayedInVolumeSummary.Value < 1))
                {
                    context.AddFailure(new ValidationFailure("NumberDisplayedInVolumeSummary", localizer[TagTypeLocalization.Validation.NUMBER_OF_TAGS_ON_VOLUME_SUMMARY_MUST_BE_GREATER_THEN_ZERO]));
                }

                if (tagTypeModel.DisplayedInChapterSummary && (tagTypeModel.NumberDisplayedInChapterSummary == null || tagTypeModel.NumberDisplayedInChapterSummary.Value < 1))
                {
                    context.AddFailure(new ValidationFailure("NumberDisplayedInChapterSummary", localizer[TagTypeLocalization.Validation.NUMBER_OF_TAGS_ON_CHAPTER_SUMMARY_MUST_BE_GREATER_THEN_ZERO]));
                }
            });
        }

        private void ValidateIconType()
        {
            RuleFor(x => x).Custom((tagTypeModel, context) =>
            {
                if (!string.IsNullOrWhiteSpace(tagTypeModel.Icon)) 
                {
                    if (!tagTypeModel.FontFamily.HasValue || tagTypeModel.FontFamily == Guid.Empty)
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagTypeLocalization.Validation.FONT_FAMILY_IS_REQUIRED]));
                    }
                    else if (!Constants.FrontEnd.FONT_FAMILIES.Contains(tagTypeModel.FontFamily.Value))
                    {
                        context.AddFailure(new ValidationFailure("FontFamily", localizer[TagTypeLocalization.Validation.FONT_FAMILY_IS_UNKNOWN]));
                    }
                }
            });
        }
    }
}
