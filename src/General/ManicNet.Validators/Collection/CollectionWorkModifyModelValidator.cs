﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class CollectionWorkModifyModelValidator : AbstractValidator<CollectionWorkModifyModel>
    {
        public CollectionWorkModifyModelValidator()
        {
            RuleFor(x => x.Number).NotEmpty();
        }
    }
}
