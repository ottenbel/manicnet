﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class CollectionModifyModelValidator : AbstractValidator<CollectionModifyModel>
    {
        public CollectionModifyModelValidator()
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.ShortDescription).MaximumLength(160);
            RuleFor(x => x.URL).ValidateURL();
        }
    }
}
