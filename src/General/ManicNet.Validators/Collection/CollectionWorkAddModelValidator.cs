﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using System;

namespace ManicNet.Validators
{
    public sealed class CollectionWorkAddModelValidator : AbstractValidator<CollectionWorkAddModel>
    {
        private readonly IWorkRetrievalService workRetrievalService;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly IStringLocalizer<CollectionLocalization> localizer;

        public CollectionWorkAddModelValidator(IWorkRetrievalService workRetrievalService, IHttpContextAccessor httpContextAccessor, IStringLocalizer<CollectionLocalization> localizer)
        {
            this.workRetrievalService = workRetrievalService;
            this.httpContextAccessor = httpContextAccessor;
            this.localizer = localizer;

            RuleFor(x => x.CollectionID).NotEmpty();
            RuleFor(x => x.WorkID).NotEmpty();
            RuleFor(x => x.Number).NotEmpty();

            ValidateWork();
        }

        private void ValidateWork()
        {
            RuleFor(x => x).Custom((collectionWorkAddModel, context) =>
            {
                if (collectionWorkAddModel.WorkID.HasValue)
                {
                    try
                    {
                        Work work = workRetrievalService.FindWork(collectionWorkAddModel.WorkID.Value, httpContextAccessor.HttpContext.User);

                        if (work.IsDeleted)
                        {
                            context.AddFailure(new ValidationFailure("WorkID", localizer[CollectionLocalization.Validation.WORK_MUST_EXIST]));
                        }
                    }
                    catch (Exception)
                    {
                        context.AddFailure(new ValidationFailure("WorkID", localizer[CollectionLocalization.Validation.WORK_MUST_EXIST]));
                    }
                }
            });
        }
    }
}
