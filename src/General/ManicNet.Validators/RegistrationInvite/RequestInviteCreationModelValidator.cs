﻿using FluentValidation;
using ManicNet.Models;

namespace ManicNet.Validators
{
    public sealed class RequestInviteCreationModelValidator : AbstractValidator<RequestInviteCreationModel>
    {
        public RequestInviteCreationModelValidator()
        {
            RuleFor(x => x.Email).NotEmpty().EmailAddress();
            RuleFor(x => x.Note).MaximumLength(250);
        }
    }
}
