﻿using FluentValidation;
using ManicNet.Contracts.Services;

namespace ManicNet.Validators
{
    public static class ImageValidatorExtension
    {
        public static IRuleBuilderOptions<T, IFormFile> ValidateImage<T, IFormFile>(this IRuleBuilder<T, IFormFile> ruleBuilder, IImageProcessingService imageProcessingService)
        {
            return ruleBuilder.SetValidator(new ImageValidator<T, IFormFile>(imageProcessingService));
        }
    }
}
