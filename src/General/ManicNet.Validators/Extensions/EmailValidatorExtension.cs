﻿using FluentValidation;

namespace ManicNet.Validators
{
    public static class EmailValidatorExtension
    {
        public static IRuleBuilderOptions<T, string> ValidateEmail<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new EmailValidator<T, string>());
        }
    }
}
