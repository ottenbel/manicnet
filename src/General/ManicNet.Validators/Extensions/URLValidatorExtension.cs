﻿using FluentValidation;

namespace ManicNet.Validators
{
    public static class URLValidatorExtension
    {
        public static IRuleBuilderOptions<T, string> ValidateURL<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.SetValidator(new URLValidator<T, string>());
        }
    }
}
