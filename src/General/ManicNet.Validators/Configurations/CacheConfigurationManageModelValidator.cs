﻿using FluentValidation;
using FluentValidation.Results;
using ManicNet.Constants;
using ManicNet.Localization;
using ManicNet.Models;
using Microsoft.Extensions.Localization;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Validators.Configurations
{
    public sealed class CacheConfigurationManageModelValidator : AbstractValidator<CacheConfigurationManageModel>
    {
        private readonly IStringLocalizer<ConfigurationLocalization> localizer;

        public CacheConfigurationManageModelValidator(IStringLocalizer<ConfigurationLocalization> localizer)
        {
            this.localizer = localizer;

            ValidateCacheConfigurations();
        }

        private void ValidateCacheConfigurations()
        {
            RuleFor(x => x).Custom((cacheConfigurationManageModel, context) =>
            {
                for (int i = 0; i < cacheConfigurationManageModel.cacheConfigurations.Count(); i++)
                {
                    if (!IsValidSectionID(cacheConfigurationManageModel.cacheConfigurations[i].SectionID))
                    {
                        context.AddFailure(new ValidationFailure(string.Format("cacheConfigurations[{0}].SectionID", i), localizer[ConfigurationLocalization.CacheConfiguration.Validation.INVALID_SECTION, i]));
                    }

                    if (!IsValidLookupTypeID(cacheConfigurationManageModel.cacheConfigurations[i].LookupTypeID))
                    {
                        context.AddFailure(new ValidationFailure(string.Format("cacheConfigurations[{0}].LookupTypeID", i), localizer[ConfigurationLocalization.CacheConfiguration.Validation.INVALID_LOOKUP_TYPE, i]));
                    }

                    if (!IsValidLengthTypeID(cacheConfigurationManageModel.cacheConfigurations[i].LengthTypeID))
                    {
                        context.AddFailure(new ValidationFailure(string.Format("cacheConfigurations[{0}].LengthTypeID", i), localizer[ConfigurationLocalization.CacheConfiguration.Validation.INVALID_LOOKUP_TYPE, i]));
                    }

                    if (cacheConfigurationManageModel.cacheConfigurations[i].Length <= 0)
                    {
                        context.AddFailure(new ValidationFailure(string.Format("cacheConfigurations[{0}].Length", i), localizer[ConfigurationLocalization.CacheConfiguration.Validation.INVALID_LENGTH, i]));
                    }
                }
            });
        }

        private bool IsValidSectionID(Guid sectionID)
        {
            bool result = false;

            List<Guid> knownSections = new List<Guid>() {
                Caching.Configurations.Sections.TAG_TYPE,
                Caching.Configurations.Sections.TAG,
                Caching.Configurations.Sections.TAG_ALIAS,
                Caching.Configurations.Sections.WORK,
                Caching.Configurations.Sections.VOLUME,
                Caching.Configurations.Sections.CHAPTER,
                Caching.Configurations.Sections.COLLECTION,
                Caching.Configurations.Sections.REGISTRATION_INVITE,
                Caching.Configurations.Sections.USER_BLOCKED_TAG,
                Caching.Configurations.Sections.CACHE_CONFIGURATION,
                Caching.Configurations.Sections.SOCIAL_LINK
            };

            if (knownSections.Contains(sectionID))
            {
                result = true;
            }

            return result;
        }

        private bool IsValidLookupTypeID(Guid lookupTypeID)
        {
            bool result = false;

            List<Guid> knownLookupTypes = new List<Guid>()
            {
                Caching.Configurations.LookupTypes.ALL,
                Caching.Configurations.LookupTypes.FIND,
            };

            if (knownLookupTypes.Contains(lookupTypeID))
            {
                result = true;
            }

            return result;
        }

        private bool IsValidLengthTypeID(Guid lengthTypeID)
        {
            bool result = false;

            List<Guid> knownLengthTypes = new List<Guid>()
            {
                Caching.Configurations.LengthTypes.DAY,
                Caching.Configurations.LengthTypes.HOUR,
                Caching.Configurations.LengthTypes.MINUTE,
                Caching.Configurations.LengthTypes.SECOND,
            };

            if (knownLengthTypes.Contains(lengthTypeID))
            {
                result = true;
            }

            return result;
        }
    }
}
