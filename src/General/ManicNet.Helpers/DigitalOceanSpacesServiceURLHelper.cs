﻿namespace ManicNet.Helpers
{
    public static class DigitalOceanSpacesServiceURLHelper
    {
        public static string GetServiceURL(string region)
        {
            return string.Format("https://{0}.digitaloceanspaces.com", region);
        }
    }
}
