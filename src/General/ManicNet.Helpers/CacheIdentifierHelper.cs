﻿using ManicNet.Constants;
using System;

namespace ManicNet.Helpers
{
    public static class CacheIdentifierHelper
    {
        public static string GetIndividualRegistrationInviteCachingKey(Guid id)
        {
            return Caching.Keys.RegistrationInvite.INDIVIDUAL + "-" + id;
        }

        public static string GetAllRegistrationInviteCachingKey()
        {
            return Caching.Keys.RegistrationInvite.ALL;
        }

        public static string GetIndividualTagTypeCachingKey(Guid id)
        {
            return Caching.Keys.TagType.INDIVIDUAL + "-" + id;
        }

        public static string GetAllTagTypeCachingKey()
        {
            return Caching.Keys.TagType.ALL;
        }

        public static string GetIndividualTagCachingKey(Guid id)
        {
            return Caching.Keys.Tag.INDIVIDUAL + "-" + id;
        }

        public static string GetAllTagCachingKey()
        {
            return Caching.Keys.Tag.ALL;
        }

        public static string GetIndividualTagAliasCachingKey(Guid id)
        {
            return Caching.Keys.TagAlias.INDIVIDUAL + "-" + id;
        }

        public static string GetAllTagAliasCachingKey()
        {
            return Caching.Keys.TagAlias.ALL;
        }

        public static string GetIndividualWorkCachingKey(Guid id)
        {
            return Caching.Keys.Work.INDIVIDUAL + "-" + id;
        }

        public static string GetAllWorkCachingKey()
        {
            return Caching.Keys.Work.ALL;
        }

        public static string GetIndividualVolumeCachingKey(Guid id)
        {
            return Caching.Keys.Volume.INDIVIDUAL + "-" + id;
        }

        public static string GetAllVolumeCachingKey()
        {
            return Caching.Keys.Volume.ALL;
        }

        public static string GetIndividualChapterCachingKey(Guid id)
        {
            return Caching.Keys.Chapter.INDIVIDUAL + "-" + id;
        }

        public static string GetAllChapterCachingKey()
        {
            return Caching.Keys.Chapter.ALL;
        }

        public static string GetIndividualCollectionCachingKey(Guid id)
        {
            return Caching.Keys.Collection.INDIVIDUAL + "-" + id;
        }

        public static string GetAllCollectionCachingKey()
        {
            return Caching.Keys.Collection.ALL;
        }

        public static string GetIndividualSocialLinkCachingKey(Guid id)
        {
            return Caching.Keys.SocialLink.INDIVIDUAL + "-" + id;
        }

        public static string GetAllSocialLinkCachingKey()
        {
            return Caching.Keys.SocialLink.ALL;
        }

        public static string GetAllUserBlockedTagsCachingKey()
        {
            return Caching.Keys.UserBlockedTags.ALL;
        }

        public static string GetAllCacheConfigurationCachingKey()
        {
            return Caching.Keys.CacheConfiguration.ALL;
        }
    }
}
