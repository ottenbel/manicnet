﻿using Newtonsoft.Json;

namespace ManicNet.Helpers
{
    public static class CloneHelper
    {
        public static T Clone<T>(T original)
        {
            T clone = JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(original));
            return clone;
        }
    }
}
