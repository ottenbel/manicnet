﻿using ManicNet.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace ManicNet.Helpers
{
    public static class TagOutputHelper
    {
        /// <summary>
        /// Create a sorted tag list for better displaying tags to users.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="tagTypes"></param>
        /// <returns></returns>
        public static List<TagTypeTagList> ConvertTagListIntoSortedTagList(IEnumerable<Tag> tags, IEnumerable<TagType> tagTypes)
        {
            List<TagTypeTagList> sortedTagList = new List<TagTypeTagList>();

            foreach (TagType tagType in tagTypes.OrderBy(tt => tt.Priority))
            {
                var tagTypeTags = tags.Where(t => t.TagTypeID == tagType.TagTypeID).OrderBy(t => t.Name).ToList();

                if (tagTypeTags.Any())
                {
                    sortedTagList.Add(new TagTypeTagList(tagType, tagTypeTags));
                }
            }

            return sortedTagList;
        }

        /// <summary>
        /// Create a sorted tag list for better displaying tags to users.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="tagTypes"></param>
        /// <returns></returns>
        public static List<TagTypeTagList> ConvertTagListIntoSortedTagList(IEnumerable<Tag> tags, IEnumerable<TagType> tagTypes, List<Chapter_Tag> chapterTags)
        {
            List<TagTypeTagList> sortedTagList = new List<TagTypeTagList>();

            foreach (TagType tagType in tagTypes.OrderBy(tt => tt.Priority))
            {
                var tagTypeTags = tags.Where(t => t.TagTypeID == tagType.TagTypeID).OrderBy(t => t.Name).ToList();

                if (tagTypeTags.Any())
                {
                    sortedTagList.Add(new TagTypeTagList(tagType, tagTypeTags, chapterTags));
                }
            }

            return sortedTagList;
        }

        /// <summary>
        /// Create a sorted tag list for better displaying tags to users.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="tagTypes"></param>
        /// <returns></returns>
        public static List<TagTypeTagList> ConvertTagListIntoSortedTagList(IEnumerable<Tag> tags, IEnumerable<TagType> tagTypes, List<Volume_Tag> volumeTags)
        {
            List<TagTypeTagList> sortedTagList = new List<TagTypeTagList>();

            foreach (TagType tagType in tagTypes.OrderBy(tt => tt.Priority))
            {
                var tagTypeTags = tags.Where(t => t.TagTypeID == tagType.TagTypeID).OrderBy(t => t.Name).ToList();

                if (tagTypeTags.Any())
                {
                    sortedTagList.Add(new TagTypeTagList(tagType, tagTypeTags, volumeTags));
                }
            }

            return sortedTagList;
        }

        /// <summary>
        /// Create a sorted tag list for better displaying tags to users.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="tagTypes"></param>
        /// <returns></returns>
        public static List<TagTypeTagList> ConvertTagListIntoSortedTagList(IEnumerable<Tag> tags, IEnumerable<TagType> tagTypes, List<Work_Tag> workTags)
        {
            List<TagTypeTagList> sortedTagList = new List<TagTypeTagList>();

            foreach (TagType tagType in tagTypes.OrderBy(tt => tt.Priority))
            {
                var tagTypeTags = tags.Where(t => t.TagTypeID == tagType.TagTypeID).OrderBy(t => t.Name).ToList();

                if (tagTypeTags.Any())
                {
                    sortedTagList.Add(new TagTypeTagList(tagType, tagTypeTags, workTags));
                }
            }

            return sortedTagList;
        }
    }

    public sealed class TagTypeTagList
    {
        public TagType tagType { get; set; }
        public List<Tag> tags { get; set; }

        public TagTypeTagList()
        {
            tagType = new TagType();
            tags = new List<Tag>();
        }

        public TagTypeTagList(TagType tagType, List<Tag> tags)
        {
            this.tagType = tagType;
            this.tags = new List<Tag>();

            foreach (Tag tag in tags)
            {
                tag.IsPrimary = true;

                this.tags.Add(tag);
            }
        }

        public TagTypeTagList(TagType tagType, List<Tag> tags, List<Chapter_Tag> chapterTags)
        {
            this.tagType = tagType;
            this.tags = new List<Tag>();

            foreach (Tag tag in tags)
            {
                Chapter_Tag chapterTag = chapterTags.Where(ct => ct.TagID == tag.TagID).First();
                tag.IsPrimary = chapterTag == null || chapterTag.IsPrimary;

                this.tags.Add(tag);
            }
        }

        public TagTypeTagList(TagType tagType, List<Tag> tags, List<Volume_Tag> volumeTags)
        {
            this.tagType = tagType;
            this.tags = new List<Tag>();

            foreach (Tag tag in tags)
            {
                Volume_Tag volumeTag = volumeTags.Where(ct => ct.TagID == tag.TagID).First();
                tag.IsPrimary = volumeTag == null || volumeTag.IsPrimary;

                this.tags.Add(tag);
            }
        }

        public TagTypeTagList(TagType tagType, List<Tag> tags, List<Work_Tag> workTags)
        {
            this.tagType = tagType;
            this.tags = new List<Tag>();

            foreach (Tag tag in tags)
            {
                Work_Tag workTag = workTags.Where(ct => ct.TagID == tag.TagID).First();
                tag.IsPrimary = workTag == null || workTag.IsPrimary;

                this.tags.Add(tag);
            }
        }
    }
}
