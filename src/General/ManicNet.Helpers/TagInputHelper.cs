﻿namespace ManicNet.Helpers
{
    public static class TagInputHelper
    {
        /// <summary>
        /// Takes a "tag" input string and convertes it into a more accurate identifier based on how input is parsed.
        /// </summary>
        /// <param name="fragment">The input fragment to be converted.</param>
        /// <returns></returns>
        public static ExplodedTagFragment ParseInputFragment(string fragment)
        {
            ExplodedTagFragment result = new ExplodedTagFragment();

            fragment = fragment.Trim();

            if (fragment.StartsWith("-"))
            {
                result.isExcluded = true;
                fragment = fragment.Remove(0, 1);
                fragment = fragment.Trim();
            }

            string[] pieces = fragment.Split(new char[] { ':' }, 2);

            if (pieces.Length == 1)
            {
                result.tagName = pieces[0].Trim();
            }
            else
            {
                result.tagType = pieces[0].Trim();
                result.tagName = pieces[1].Trim();
            }

            return result;
        }
    }

    public class ExplodedTagFragment
    {
        /// <summary>
        /// Signals whether or not the fragement is attempting to exclude the tag.
        /// </summary>
        public bool isExcluded { get; set; }
        /// <summary>
        /// The fragement specifies a tag type for the tag in question.
        /// </summary>
        public string tagType { get; set; }
        /// <summary>
        /// The name of the tag provided by the fragement.
        /// </summary>
        public string tagName { get; set; }

        public ExplodedTagFragment()
        {
            isExcluded = false;
            tagType = string.Empty;
            tagName = string.Empty;
        }

        public ExplodedTagFragment(bool isExcluded, string tagType, string tagName)
        {
            this.isExcluded = isExcluded;
            this.tagType = tagType;
            this.tagName = tagName;
        }
    }
}
