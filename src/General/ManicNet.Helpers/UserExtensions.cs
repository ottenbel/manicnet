﻿using System;
using System.Security.Claims;

namespace ManicNet.Helpers
{
    public static class UserExtensions
    {
        public static Guid? GetUserID(this ClaimsPrincipal principal)
        {
            string id = principal.FindFirstValue(ClaimTypes.NameIdentifier);

            if (id == null)
            {
                return null;
            }
            else
            {
                return new Guid(id);
            }
        }
    }
}
