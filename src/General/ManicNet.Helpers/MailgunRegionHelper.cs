﻿using FluentEmail.Mailgun;
using ManicNet.Constants;
using System;

namespace ManicNet.Helpers
{
    public static class MailgunRegionHelper
    {
        public static MailGunRegion Convert(string region)
        {
            MailGunRegion regionEndpoint = MailGunRegion.USA;

            if (string.Equals(region, Email.Mailgun.Region.USA, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = MailGunRegion.USA;
            }
            else if (string.Equals(region, Email.Mailgun.Region.EU, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = MailGunRegion.EU;
            }

            return regionEndpoint;
        }
    }
}
