﻿using Amazon;
using System;
using static ManicNet.Constants.Uploads;

namespace ManicNet.Helpers
{
    public static class AmazonS3RegionHelper
    {
        public static RegionEndpoint Convert(string region)
        {
            RegionEndpoint regionEndpoint = RegionEndpoint.CACentral1;

            if (string.Equals(region, AmazonS3.Region.AF_SOUTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.AFSouth1;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APEast1;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_NORTH_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APNortheast1;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_NORTH_EAST_2, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APNortheast2;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_NORTH_EAST_3, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APNortheast3;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_SOUTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APSouth1;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_SOUTH_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APSoutheast1;
            }
            else if (string.Equals(region, AmazonS3.Region.AP_SOUTH_EAST_2, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.APSoutheast2;
            }
            else if (string.Equals(region, AmazonS3.Region.CA_CENTRAL_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.CACentral1;
            }
            else if (string.Equals(region, AmazonS3.Region.CN_NORTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.CNNorth1;
            }
            else if (string.Equals(region, AmazonS3.Region.CN_NORTH_WEST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.CNNorthWest1;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_CENTRAL_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUCentral1;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_NORTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUNorth1;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_SOUTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUSouth1;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_WEST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUWest1;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_WEST_2, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUWest2;
            }
            else if (string.Equals(region, AmazonS3.Region.EU_WEST_3, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.EUWest3;
            }
            else if (string.Equals(region, AmazonS3.Region.ME_SOUTH_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.MESouth1;
            }
            else if (string.Equals(region, AmazonS3.Region.SA_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.SAEast1;
            }
            else if (string.Equals(region, AmazonS3.Region.US_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USEast1;
            }
            else if (string.Equals(region, AmazonS3.Region.US_EAST_2, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USEast2;
            }
            else if (string.Equals(region, AmazonS3.Region.US_GOV_CLOUD_EAST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USGovCloudEast1;
            }
            else if (string.Equals(region, AmazonS3.Region.US_GOV_CLOUD_WEST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USGovCloudWest1;
            }
            else if (string.Equals(region, AmazonS3.Region.US_WEST_1, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USWest1;
            }
            else if (string.Equals(region, AmazonS3.Region.US_WEST_2, StringComparison.OrdinalIgnoreCase))
            {
                regionEndpoint = RegionEndpoint.USWest2;
            }

            return regionEndpoint;
        }
    }
}
