﻿using ManicNet.Helpers;
using Xunit;

namespace UnitTest.ManicNet.Helpers
{
    public sealed class TagInputHelperTest
    {
        [Theory]
        [MemberData(nameof(ParseInputFragmentTheoryData))]
        public void ParseInputFragment_ReturnsExpectedResult(string fragment, ExplodedTagFragment expected)
        {
            //Arrange 

            //Act
            ExplodedTagFragment result = TagInputHelper.ParseInputFragment(fragment);

            //Assert
            Assert.Equal(expected.isExcluded, result.isExcluded);
            Assert.Equal(expected.tagType, result.tagType);
            Assert.Equal(expected.tagName, result.tagName);
        }

        public static TheoryData<string, ExplodedTagFragment> ParseInputFragmentTheoryData
        {
            get
            {
                var data = new TheoryData<string, ExplodedTagFragment>
                {
                    { "Some Tag", new ExplodedTagFragment(false, string.Empty, "Some Tag") },
                    { "-Some Tag", new ExplodedTagFragment(true, string.Empty, "Some Tag") },
                    { "tag:Some Tag", new ExplodedTagFragment(false, "tag", "Some Tag") },
                    { "-tag:Some Tag", new ExplodedTagFragment(true, "tag", "Some Tag") },
                    { " Some Tag ", new ExplodedTagFragment(false, string.Empty, "Some Tag") },
                    { " -Some Tag ", new ExplodedTagFragment(true, string.Empty, "Some Tag") },
                    { " tag:Some Tag ", new ExplodedTagFragment(false, "tag", "Some Tag") },
                    { " -tag:Some Tag ", new ExplodedTagFragment(true, "tag", "Some Tag") }
                };

                return data;
            }
        }


    }
}
