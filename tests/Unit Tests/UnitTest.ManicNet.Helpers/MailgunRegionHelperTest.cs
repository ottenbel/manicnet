﻿using FluentEmail.Mailgun;
using ManicNet.Constants;
using ManicNet.Helpers;
using Xunit;

namespace UnitTest.ManicNet.Helpers
{
    public sealed class MailgunRegionHelperTest
    {
        [Theory]
        [MemberData(nameof(ConvertRegionTheoryData))]
        public void ConvertRegion_ReturnsExpectedResult(string region, MailGunRegion expected)
        {
            //Arrange

            //Act
            MailGunRegion result = MailgunRegionHelper.Convert(region);

            //Assert
            Assert.Equal(expected, result);
        }

        public static TheoryData<string, MailGunRegion> ConvertRegionTheoryData
        {
            get
            {
                var data = new TheoryData<string, MailGunRegion>
                {
                    { null, MailGunRegion.USA },
                    { string.Empty, MailGunRegion.USA },
                    { "Not a Real Region", MailGunRegion.USA },
                    { Email.Mailgun.Region.USA, MailGunRegion.USA },
                    { Email.Mailgun.Region.EU, MailGunRegion.EU }
                };

                return data;
            }
        }
    }
}
