﻿using Amazon;
using ManicNet.Helpers;
using Xunit;
using static ManicNet.Constants.Uploads;

namespace UnitTest.ManicNet.Helpers
{
    public sealed class AmazonS3RegionHelperTest
    {
        [Theory]
        [MemberData(nameof(ConvertRegionTheoryData))]
        public void ConvertRegion_ReturnsExpectedResult(string region, RegionEndpoint expected)
        {
            //Arrange

            //Act
            RegionEndpoint result = AmazonS3RegionHelper.Convert(region);

            //Assert
            Assert.Equal(expected, result);
        }

        public static TheoryData<string, RegionEndpoint> ConvertRegionTheoryData
        {
            get
            {
                var data = new TheoryData<string, RegionEndpoint>
                {
                    { null, RegionEndpoint.CACentral1 },
                    { string.Empty, RegionEndpoint.CACentral1 },
                    { "Not a Real Region", RegionEndpoint.CACentral1 },
                    { AmazonS3.Region.AF_SOUTH_1, RegionEndpoint.AFSouth1 },
                    { AmazonS3.Region.AF_SOUTH_1.ToUpper(), RegionEndpoint.AFSouth1 },
                    { AmazonS3.Region.AF_SOUTH_1.ToLower(), RegionEndpoint.AFSouth1 },
                    { AmazonS3.Region.AP_EAST_1, RegionEndpoint.APEast1 },
                    { AmazonS3.Region.AP_EAST_1.ToUpper(), RegionEndpoint.APEast1 },
                    { AmazonS3.Region.AP_EAST_1.ToLower(), RegionEndpoint.APEast1 },
                    { AmazonS3.Region.AP_NORTH_EAST_1, RegionEndpoint.APNortheast1 },
                    { AmazonS3.Region.AP_NORTH_EAST_1.ToUpper(), RegionEndpoint.APNortheast1 },
                    { AmazonS3.Region.AP_NORTH_EAST_1.ToLower(), RegionEndpoint.APNortheast1 },
                    { AmazonS3.Region.AP_NORTH_EAST_2, RegionEndpoint.APNortheast2 },
                    { AmazonS3.Region.AP_NORTH_EAST_2.ToUpper(), RegionEndpoint.APNortheast2 },
                    { AmazonS3.Region.AP_NORTH_EAST_2.ToLower(), RegionEndpoint.APNortheast2 },
                    { AmazonS3.Region.AP_NORTH_EAST_3, RegionEndpoint.APNortheast3 },
                    { AmazonS3.Region.AP_NORTH_EAST_3.ToUpper(), RegionEndpoint.APNortheast3 },
                    { AmazonS3.Region.AP_NORTH_EAST_3.ToLower(), RegionEndpoint.APNortheast3 },
                    { AmazonS3.Region.AP_SOUTH_1, RegionEndpoint.APSouth1 },
                    { AmazonS3.Region.AP_SOUTH_1.ToUpper(), RegionEndpoint.APSouth1 },
                    { AmazonS3.Region.AP_SOUTH_1.ToLower(), RegionEndpoint.APSouth1 },
                    { AmazonS3.Region.AP_SOUTH_EAST_1, RegionEndpoint.APSoutheast1 },
                    { AmazonS3.Region.AP_SOUTH_EAST_1.ToUpper(), RegionEndpoint.APSoutheast1 },
                    { AmazonS3.Region.AP_SOUTH_EAST_1.ToLower(), RegionEndpoint.APSoutheast1 },
                    { AmazonS3.Region.AP_SOUTH_EAST_2, RegionEndpoint.APSoutheast2 },
                    { AmazonS3.Region.AP_SOUTH_EAST_2.ToUpper(), RegionEndpoint.APSoutheast2 },
                    { AmazonS3.Region.AP_SOUTH_EAST_2.ToLower(), RegionEndpoint.APSoutheast2 },
                    { AmazonS3.Region.CA_CENTRAL_1, RegionEndpoint.CACentral1 },
                    { AmazonS3.Region.CA_CENTRAL_1.ToUpper(), RegionEndpoint.CACentral1 },
                    { AmazonS3.Region.CA_CENTRAL_1.ToLower(), RegionEndpoint.CACentral1 },
                    { AmazonS3.Region.CN_NORTH_1, RegionEndpoint.CNNorth1 },
                    { AmazonS3.Region.CN_NORTH_1.ToUpper(), RegionEndpoint.CNNorth1 },
                    { AmazonS3.Region.CN_NORTH_1.ToLower(), RegionEndpoint.CNNorth1 },
                    { AmazonS3.Region.CN_NORTH_WEST_1, RegionEndpoint.CNNorthWest1 },
                    { AmazonS3.Region.CN_NORTH_WEST_1.ToUpper(), RegionEndpoint.CNNorthWest1 },
                    { AmazonS3.Region.CN_NORTH_WEST_1.ToLower(), RegionEndpoint.CNNorthWest1 },
                    { AmazonS3.Region.EU_CENTRAL_1, RegionEndpoint.EUCentral1 },
                    { AmazonS3.Region.EU_CENTRAL_1.ToUpper(), RegionEndpoint.EUCentral1 },
                    { AmazonS3.Region.EU_CENTRAL_1.ToLower(), RegionEndpoint.EUCentral1 },
                    { AmazonS3.Region.EU_NORTH_1, RegionEndpoint.EUNorth1 },
                    { AmazonS3.Region.EU_NORTH_1.ToUpper(), RegionEndpoint.EUNorth1 },
                    { AmazonS3.Region.EU_NORTH_1.ToLower(), RegionEndpoint.EUNorth1 },
                    { AmazonS3.Region.EU_SOUTH_1, RegionEndpoint.EUSouth1 },
                    { AmazonS3.Region.EU_SOUTH_1.ToUpper(), RegionEndpoint.EUSouth1 },
                    { AmazonS3.Region.EU_SOUTH_1.ToLower(), RegionEndpoint.EUSouth1 },
                    { AmazonS3.Region.EU_WEST_1, RegionEndpoint.EUWest1 },
                    { AmazonS3.Region.EU_WEST_1.ToUpper(), RegionEndpoint.EUWest1 },
                    { AmazonS3.Region.EU_WEST_1.ToLower(), RegionEndpoint.EUWest1 },
                    { AmazonS3.Region.EU_WEST_2, RegionEndpoint.EUWest2 },
                    { AmazonS3.Region.EU_WEST_2.ToUpper(), RegionEndpoint.EUWest2 },
                    { AmazonS3.Region.EU_WEST_2.ToLower(), RegionEndpoint.EUWest2 },
                    { AmazonS3.Region.EU_WEST_3, RegionEndpoint.EUWest3 },
                    { AmazonS3.Region.EU_WEST_3.ToUpper(), RegionEndpoint.EUWest3 },
                    { AmazonS3.Region.EU_WEST_3.ToLower(), RegionEndpoint.EUWest3 },
                    { AmazonS3.Region.ME_SOUTH_1, RegionEndpoint.MESouth1 },
                    { AmazonS3.Region.ME_SOUTH_1.ToUpper(), RegionEndpoint.MESouth1 },
                    { AmazonS3.Region.ME_SOUTH_1.ToLower(), RegionEndpoint.MESouth1 },
                    { AmazonS3.Region.SA_EAST_1, RegionEndpoint.SAEast1 },
                    { AmazonS3.Region.SA_EAST_1.ToUpper(), RegionEndpoint.SAEast1 },
                    { AmazonS3.Region.SA_EAST_1.ToLower(), RegionEndpoint.SAEast1 },
                    { AmazonS3.Region.US_EAST_1, RegionEndpoint.USEast1 },
                    { AmazonS3.Region.US_EAST_1.ToUpper(), RegionEndpoint.USEast1 },
                    { AmazonS3.Region.US_EAST_1.ToLower(), RegionEndpoint.USEast1 },
                    { AmazonS3.Region.US_EAST_2, RegionEndpoint.USEast2 },
                    { AmazonS3.Region.US_EAST_2.ToUpper(), RegionEndpoint.USEast2 },
                    { AmazonS3.Region.US_EAST_2.ToLower(), RegionEndpoint.USEast2 },
                    { AmazonS3.Region.US_GOV_CLOUD_EAST_1, RegionEndpoint.USGovCloudEast1 },
                    { AmazonS3.Region.US_GOV_CLOUD_EAST_1.ToUpper(), RegionEndpoint.USGovCloudEast1 },
                    { AmazonS3.Region.US_GOV_CLOUD_EAST_1.ToLower(), RegionEndpoint.USGovCloudEast1 },
                    { AmazonS3.Region.US_GOV_CLOUD_WEST_1, RegionEndpoint.USGovCloudWest1 },
                    { AmazonS3.Region.US_GOV_CLOUD_WEST_1.ToUpper(), RegionEndpoint.USGovCloudWest1 },
                    { AmazonS3.Region.US_GOV_CLOUD_WEST_1.ToLower(), RegionEndpoint.USGovCloudWest1 },
                    { AmazonS3.Region.US_WEST_1, RegionEndpoint.USWest1 },
                    { AmazonS3.Region.US_WEST_1.ToUpper(), RegionEndpoint.USWest1 },
                    { AmazonS3.Region.US_WEST_1.ToLower(), RegionEndpoint.USWest1 },
                    { AmazonS3.Region.US_WEST_2, RegionEndpoint.USWest2 },
                    { AmazonS3.Region.US_WEST_2.ToUpper(), RegionEndpoint.USWest2 },
                    { AmazonS3.Region.US_WEST_2.ToLower(), RegionEndpoint.USWest2 },
                };

                return data;
            }
        }
    }
}
