﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Collections
{
    public sealed class CollectionViewModelTest
    {
        [Fact]
        public void CollectionViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionViewModel model = new CollectionViewModel();

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Image);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CollectionViewModelConstructor_InitializesAsExpected(bool isDeleted)
        {
            //Arrange
            CollectionViewModel model = new CollectionViewModel(Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, isDeleted);

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Image);
        }
    }
}
