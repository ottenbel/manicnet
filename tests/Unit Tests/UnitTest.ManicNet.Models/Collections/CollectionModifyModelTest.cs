﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Collections
{
    public sealed class CollectionModifyModelTest
    {
        [Fact]
        public void CollectionModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionModifyModel model = new CollectionModifyModel();

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void CollectionModifyModelConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionModifyModel model = new CollectionModifyModel(Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Image);
        }
    }
}
