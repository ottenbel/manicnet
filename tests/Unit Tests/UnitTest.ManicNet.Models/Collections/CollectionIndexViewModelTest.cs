﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Collections
{
    public sealed class CollectionIndexViewModelTest
    {
        [Fact]
        public void CollectionIndexViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionIndexViewModel model = new CollectionIndexViewModel();

            //Act/Assert
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void CollectionIndexViewModelConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionIndexViewModel model = new CollectionIndexViewModel(string.Empty);

            //Act/Assert
            Assert.NotNull(model.Image);
        }
    }
}
