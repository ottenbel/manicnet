﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Collections
{
    public sealed class CollectionWorkViewModelTest
    {
        [Fact]
        public void CollectionWorkViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionWorkViewModel model = new CollectionWorkViewModel();

            //Act/Assert
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Tags);
        }

        [Fact]
        public void CollectionViewModelConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionWorkViewModel model = new CollectionWorkViewModel(0, Guid.Empty, string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Tags);
        }
    }
}
