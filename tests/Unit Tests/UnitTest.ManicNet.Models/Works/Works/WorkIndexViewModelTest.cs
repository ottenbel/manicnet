﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Works
{
    public sealed class WorkIndexViewModelTest
    {
        [Fact]
        public void WorkIndexViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkIndexViewModel model = new WorkIndexViewModel();

            //Act/Assert
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void WorkIndexViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkIndexViewModel model = new WorkIndexViewModel(string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Image);
        }
    }
}
