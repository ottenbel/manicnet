﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Works
{
    public sealed class WorkSummaryViewModelTest
    {
        [Fact]
        public void WorkSummaryViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkSummaryViewModel model = new WorkSummaryViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Cover);
        }

        [Fact]
        public void WorkSummaryViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkSummaryViewModel model = new WorkSummaryViewModel(Guid.Empty, string.Empty, string.Empty, DateTime.UtcNow);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Cover);
        }
    }
}
