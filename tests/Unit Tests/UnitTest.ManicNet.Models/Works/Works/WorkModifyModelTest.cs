﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Works
{
    public sealed class WorkModifyModelTest
    {
        [Fact]
        public void WorkModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkModifyModel model = new WorkModifyModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void WorkModifyModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkModifyModel model = new WorkModifyModel(Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }
    }
}
