﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Works
{
    public sealed class WorkViewModelTest
    {
        [Fact]
        public void WorkViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkViewModel model = new WorkViewModel();

            //Act/Assert
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Collections);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void WorkViewModelModelConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkViewModel model = new WorkViewModel(Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, DateTime.UtcNow, false);

            //Act/Assert
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Collections);
            Assert.NotNull(model.Image);
        }
    }
}
