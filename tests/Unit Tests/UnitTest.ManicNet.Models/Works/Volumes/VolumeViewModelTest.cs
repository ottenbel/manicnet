﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Volumes
{
    public sealed class VolumeViewModelTest
    {
        [Fact]
        public void VolumeViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeViewModel model = new VolumeViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void VolumeViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeViewModel model = new VolumeViewModel(Guid.Empty, Guid.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, false);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.Cover);
            Assert.NotNull(model.Image);
        }
    }
}
