﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Volumes
{
    public sealed class VolumeSummaryViewModelTest
    {
        [Fact]
        public void VolumeSummaryViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeSummaryViewModel model = new VolumeSummaryViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
        }

        [Fact]
        public void VolumeSummaryViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeSummaryViewModel model = new VolumeSummaryViewModel(Guid.Empty, Guid.Empty, string.Empty, string.Empty, 0, string.Empty, false, DateTime.UtcNow);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
        }
    }
}
