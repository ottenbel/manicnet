﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Volumes
{
    public sealed class VolumeModifyModelTest
    {
        [Fact]
        public void VolumeModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeModifyModel model = new VolumeModifyModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void VolumeModifyModelIDConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeModifyModel model = new VolumeModifyModel(Guid.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void VolumeModifyModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            VolumeModifyModel model = new VolumeModifyModel(Guid.Empty, Guid.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }
    }
}
