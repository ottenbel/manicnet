﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters
{
    public sealed class ChapterViewModelTest
    {
        [Fact]
        public void ChapterViewModelTestDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterViewModel model = new ChapterViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Pages);
            Assert.NotNull(model.CurrentPage);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void ChapterViewModelTestManualConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterViewModel model = new ChapterViewModel(Guid.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, false, Guid.Empty, string.Empty, Guid.Empty, 0, string.Empty, false, Guid.Empty, Guid.Empty, 0, 0);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Pages);
            Assert.NotNull(model.CurrentPage);
            Assert.NotNull(model.Image);
        }
    }
}
