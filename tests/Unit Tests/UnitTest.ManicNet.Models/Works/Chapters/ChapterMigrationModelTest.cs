﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters
{
    public sealed class ChapterMigrationModelTest
    {
        [Fact]
        public void ChapterMigrationModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterMigrationModel model = new ChapterMigrationModel();

            //Act/Assert
            Assert.NotNull(model.CollectionVolumes);
        }

        [Fact]
        public void ChapterMigrationModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterMigrationModel model = new ChapterMigrationModel(Guid.Empty, 0);

            //Act/Assert
            Assert.NotNull(model.CollectionVolumes);
        }
    }
}
