﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters.Pages
{
    public sealed class PageModifyViewModelTest
    {
        [Fact]
        public void PageModifyViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            PageModifyViewModel model = new PageModifyViewModel();

            //Act/Assert
            Assert.NotNull(model.Content);
        }
    }
}
