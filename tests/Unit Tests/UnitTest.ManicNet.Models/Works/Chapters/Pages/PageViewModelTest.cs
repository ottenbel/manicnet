﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters.Pages
{
    public sealed class PageViewModelTest
    {
        [Fact]
        public void PageViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            PageViewModel model = new PageViewModel();

            //Act/Assert
            Assert.NotNull(model.Content);
        }
    }
}
