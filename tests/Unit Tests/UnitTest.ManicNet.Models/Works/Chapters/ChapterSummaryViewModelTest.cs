﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters
{
    public sealed class ChapterSummaryViewModelTest
    {
        [Fact]
        public void ChapterSummaryViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterSummaryViewModel model = new ChapterSummaryViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
        }

        [Fact]
        public void ChapterSummaryViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterSummaryViewModel model = new ChapterSummaryViewModel(Guid.Empty, string.Empty, 0, string.Empty, false, DateTime.UtcNow);

            //Act/Assert
            Assert.NotNull(model.Tags);
        }
    }
}
