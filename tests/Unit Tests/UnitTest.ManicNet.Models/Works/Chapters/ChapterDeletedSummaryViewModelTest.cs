﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters
{
    public sealed class ChapterDeletedSummaryViewModelTest
    {
        [Fact]
        public void ChapterDeletedSummaryViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterDeletedSummaryViewModel model = new ChapterDeletedSummaryViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
        }

        [Fact]
        public void ChapterDeletedSummaryViewModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterDeletedSummaryViewModel model = new ChapterDeletedSummaryViewModel(Guid.Empty, 0, string.Empty, Guid.Empty, string.Empty, Guid.Empty, string.Empty, 0, string.Empty, false, DateTime.UtcNow);

            //Act/Assert
            Assert.NotNull(model.Tags);
        }
    }
}
