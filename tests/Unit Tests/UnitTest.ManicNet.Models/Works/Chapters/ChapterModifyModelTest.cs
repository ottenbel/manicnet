﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Works.Chapters
{
    public sealed class ChapterModifyModelTest
    {
        [Fact]
        public void ChapterModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterModifyModel model = new ChapterModifyModel();

            //Act/Assert
            Assert.NotNull(model.Pages);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void ChapterModifyModelInitializeIDConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterModifyModel model = new ChapterModifyModel(Guid.Empty);

            //Act/Assert
            Assert.NotNull(model.Pages);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void ChapterModifyModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            ChapterModifyModel model = new ChapterModifyModel(Guid.Empty, Guid.Empty, 0, string.Empty, string.Empty, string.Empty, string.Empty);

            //Act/Assert
            Assert.NotNull(model.Pages);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Image);
        }
    }
}
