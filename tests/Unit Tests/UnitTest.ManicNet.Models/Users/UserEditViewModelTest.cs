﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Users
{
    public sealed class UserEditViewModelTest
    {
        [Fact]
        public void EditViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            UserEditViewModel model = new UserEditViewModel();

            //Act/Assert
            Assert.NotNull(model.Permissions);
        }
    }
}
