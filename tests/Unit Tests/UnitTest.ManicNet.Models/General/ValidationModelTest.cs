﻿using ManicNet.Models;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Models.General
{
    public sealed class ValidationModelTest
    {
        [Fact]
        public void ValidationModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            ValidationModel result = new ValidationModel();

            //Act/Assert
            Assert.NotNull(result.ModelState);
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void ValidationModelIsValidConstructor_InitializesAsExpected(bool isValid)
        {
            //Arrange
            ValidationModel model = new ValidationModel(isValid);

            //Act/Assert
            Assert.NotNull(model.ModelState);
        }

        [Fact]
        public void ValidationModelModelStateConstructor_InitializesAsExpected()
        {
            //Arrange
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            ValidationModel model = new ValidationModel(dictionary);

            //Act/Assert
            Assert.NotNull(model.ModelState);
        }
    }
}
