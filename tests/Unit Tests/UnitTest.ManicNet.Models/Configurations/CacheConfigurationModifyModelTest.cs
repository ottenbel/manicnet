﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Configurations
{
    public sealed class CacheConfigurationModifyModelTest
    {
        [Fact]
        public void CacheConfigurationModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CacheConfigurationModifyModel model = new CacheConfigurationModifyModel();

            //Act/Assert
            Assert.NotNull(model.LengthTypes);
        }

        [Fact]
        public void CacheConfigurationModifyModelConstructor_InitializesAsExpected()
        {
            //Arrange
            CacheConfigurationModifyModel model = new CacheConfigurationModifyModel(Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), Guid.NewGuid(), 0);

            //Act/Assert
            Assert.NotNull(model.LengthTypes);
        }
    }
}
