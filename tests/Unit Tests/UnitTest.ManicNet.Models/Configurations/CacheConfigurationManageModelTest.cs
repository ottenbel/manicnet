﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Configurations
{
    public sealed class CacheConfigurationManageModelTest
    {
        [Fact]
        public void CacheConfigurationManageModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CacheConfigurationManageModel model = new CacheConfigurationManageModel();

            //Act/Assert
            Assert.NotNull(model.cacheConfigurations);
        }
    }
}
