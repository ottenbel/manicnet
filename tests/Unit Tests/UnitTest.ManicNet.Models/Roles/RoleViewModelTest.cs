﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Roles
{
    public sealed class RoleViewModelTest
    {
        [Fact]
        public void RoleViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            RoleViewModel model = new RoleViewModel();

            //Act/Assert
            Assert.NotNull(model.Permissions);
        }

        [Fact]
        public void RoleViewModelConstructor_InitializesAsExpected()
        {
            //Arrange
            RoleViewModel model = new RoleViewModel(Guid.NewGuid(), string.Empty, string.Empty, true);

            //Act/Assert
            Assert.NotNull(model.Permissions);
        }
    }
}
