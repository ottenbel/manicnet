﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Roles
{
    public sealed class RoleEditModelTest
    {
        [Fact]
        public void RoleEditModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            RoleEditModel model = new RoleEditModel();

            //Act/Assert
            Assert.NotNull(model.Permissions);
        }
    }
}
