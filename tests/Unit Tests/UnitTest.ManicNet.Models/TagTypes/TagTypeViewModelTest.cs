﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.TagTypes
{
    public sealed class TagTypeViewModelTest
    {
        [Fact]
        public void TagTypeViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeViewModel model = new TagTypeViewModel();

            //Act/Assert
            Assert.NotNull(model.Image);
        }
    }
}
