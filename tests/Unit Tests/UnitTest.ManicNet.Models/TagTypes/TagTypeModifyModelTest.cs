﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.TagTypes
{
    public sealed class TagTypeModifyModelTest
    {
        [Fact]
        public void TagTypeModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeModifyModel model = new TagTypeModifyModel();

            //Act/Assert
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void TagTypeModifyModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeModifyModel model = new TagTypeModifyModel(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, Guid.Empty, 0, 0);

            //Act/Assert
            Assert.NotNull(model.Image);
        }
    }
}
