﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Permissions
{
    public sealed class RoleSectionTest
    {
        [Fact]
        public void RoleSectionDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            RoleSection model = new RoleSection();

            //Act/Assert
            Assert.NotNull(model.OwnerRoles);
            Assert.NotNull(model.AdministratorRoles);
            Assert.NotNull(model.NormalRoles);
        }

    }
}
