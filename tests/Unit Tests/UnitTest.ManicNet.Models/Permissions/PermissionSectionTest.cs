﻿using ManicNet.Constants.Models;
using ManicNet.Models;
using System.Collections.Generic;
using System.Security.Claims;
using Xunit;

namespace UnitTest.ManicNet.Models.Permissions
{
    public sealed class PermissionSectionTest
    {
        [Fact]
        public void UserPermissionSectionDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            PermissionSection model = new PermissionSection();

            //Act/Assert
            Assert.NotNull(model.OwnerPermissions);
            Assert.NotNull(model.AdministratorPermissions);
            Assert.NotNull(model.NormalPermissions);
        }

        [Theory]
        [MemberData(nameof(UserPermissionSectionManualConstructor_InitializesAsExpectedTheoryData))]
        public void UserPermissionSectionManualConstructor_InitializesAsExpected(PermissionsSection permissionSection, List<Claim> claims, List<ManicNetPermission> expectedOwnerPermissions, List<ManicNetPermission> expectedAdminPermissions, List<ManicNetPermission> expectedUserPermissions)
        {
            //Arrange/Act
            PermissionSection model = new PermissionSection(permissionSection, claims, true, true, true);

            //Assert
            Assert.Equal(model.OwnerPermissions, expectedOwnerPermissions);
            Assert.Equal(model.AdministratorPermissions, expectedAdminPermissions);
            Assert.Equal(model.NormalPermissions, expectedUserPermissions);
        }

        public static TheoryData<PermissionsSection, List<Claim>, List<ManicNetPermission>, List<ManicNetPermission>, List<ManicNetPermission>> UserPermissionSectionManualConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                string sectionName = string.Empty;
                var data = new TheoryData<PermissionsSection, List<Claim>, List<ManicNetPermission>, List<ManicNetPermission>, List<ManicNetPermission>>()
                {
                    { new PermissionsSection(), null, new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>(), new List<ManicNetPermission>(),
                        new List<ManicNetPermission>(), new List<ManicNetPermission>()},
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), null,
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>(),
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), null,
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>(),
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>() },
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), null,
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>(),
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), null,
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>(),
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Admin") } ,
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>()), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>()},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), null,
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>(),
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>(), new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>(), new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), null,
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>(),
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>(), new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>(), new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), null,
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>(),
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", false) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", false) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", false) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                    { new PermissionsSection(sectionName, new List<string>() { "Owner" }, new List<string>() { "Admin" }, new List<string>() { "User" }), new List<Claim>() { new Claim("Permission", "Owner"), new Claim("Permission", "Admin"), new Claim("Permission", "User") },
                        new List<ManicNetPermission>() { new ManicNetPermission("Owner", true) }, new List<ManicNetPermission>() { new ManicNetPermission("Admin", true) }, new List<ManicNetPermission>(){ new ManicNetPermission("User", true) }},
                };

                return data;
            }
        }
    }
}
