﻿using ManicNet.Domain.Models;
using ManicNet.Models;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Models.Permissions
{
    public sealed class UpdateRolesHolderTest
    {
        [Fact]
        public void UpdateRolesHolderDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            UpdateRolesHolder model = new UpdateRolesHolder();

            //Act/Assert
            Assert.NotNull(model.UserRoles);
            Assert.NotNull(model.RolesToAdd);
            Assert.NotNull(model.RolesToRemove);
        }

        [Theory]
        [MemberData(nameof(UpdateRolesHolderProvideUserRolesConstructor_InitializesAsExpectedTheoryData))]
        public void UpdateRolesHolderProvideUserRolesConstructor_InitializesAsExpected(List<ManicNetRole> userRoles)
        {
            //Arrange
            UpdateRolesHolder model = new UpdateRolesHolder(userRoles);

            //Act/Assert
            Assert.NotNull(model.UserRoles);
            Assert.NotNull(model.RolesToAdd);
            Assert.NotNull(model.RolesToRemove);
        }

        public static TheoryData<List<ManicNetRole>> UpdateRolesHolderProvideUserRolesConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<ManicNetRole>>()
                {
                    { null },
                    { new List<ManicNetRole>() }
                };

                return data;
            }
        }
    }
}
