﻿using ManicNet.Models;
using System.Collections.Generic;
using System.Security.Claims;
using Xunit;

namespace UnitTest.ManicNet.Models.Permissions
{
    public sealed class UpdateClaimsHolderTest
    {
        [Fact]
        public void UpdateClaimsHolderDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            UpdateClaimsHolder model = new UpdateClaimsHolder();

            //Act/Assert
            Assert.NotNull(model.UserClaims);
            Assert.NotNull(model.ClaimsToAdd);
            Assert.NotNull(model.ClaimsToRemove);
        }

        [Theory]
        [MemberData(nameof(UpdateClaimsHolderProvideUserClaimsConstructor_InitializesAsExpectedTheoryData))]
        public void UpdateClaimsHolderProvideUserClaimsConstructor_InitializesAsExpected(List<Claim> userClaims)
        {
            //Arrange
            UpdateClaimsHolder model = new UpdateClaimsHolder(userClaims);

            //Act/Assert
            Assert.NotNull(model.UserClaims);
            Assert.NotNull(model.ClaimsToAdd);
            Assert.NotNull(model.ClaimsToRemove);
        }

        public static TheoryData<List<Claim>> UpdateClaimsHolderProvideUserClaimsConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Claim>>()
                {
                    { null },
                    { new List<Claim>() }
                };

                return data;
            }
        }
    }
}
