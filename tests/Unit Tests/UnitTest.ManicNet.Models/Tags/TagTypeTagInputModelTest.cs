﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagTypeTagInputModelTest
    {
        [Fact]
        public void TagTypeTagInputModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeTagInputModel model = new TagTypeTagInputModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
        }

        [Fact]
        public void DynamicTagInputDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            DynamicTagInput model = new DynamicTagInput();

            //Act/Assert
            Assert.NotNull(model.TagType);
        }
    }
}
