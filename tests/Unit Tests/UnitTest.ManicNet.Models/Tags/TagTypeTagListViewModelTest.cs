﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagTypeTagListViewModelTest
    {
        [Fact]
        public void TagTypeTagListViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeTagListViewModel model = new TagTypeTagListViewModel();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.TagType);
        }
    }
}
