﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagTypeTagPriorityInputModelTest
    {
        [Fact]
        public void TagTypeTagPriorityInputModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeTagPriorityInputModel model = new TagTypeTagPriorityInputModel();

            //Act/Assert
            Assert.NotNull(model.PrimaryTags);
            Assert.NotNull(model.SecondaryTags);
        }
    }
}
