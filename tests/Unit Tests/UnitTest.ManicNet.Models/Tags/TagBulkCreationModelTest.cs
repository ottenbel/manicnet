﻿using ManicNet.Models;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagBulkCreationModelTest
    {
        [Fact]
        public void TagBulkCreationModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagBulkCreationModel model = new TagBulkCreationModel();

            //Act/Assert
            Assert.NotNull(model.TagTypes);
        }
    }
}
