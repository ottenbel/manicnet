﻿using ManicNet.Domain.Models;
using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagViewModelTest
    {
        [Fact]
        public void TagViewModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagViewModel model = new TagViewModel();

            //Act/Assert
            Assert.NotNull(model.TagType);
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void TagViewModelFromTagManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Tag tag = new Tag()
            {
                TagID = Guid.Empty,
                Name = string.Empty,
                ShortDescription = string.Empty,
                LongDescription = string.Empty,
                URL = string.Empty,
                Count = 0,
                IsVirtual = false,
                IsPrimary = true,
                IsDeleted = false
            };
            TagViewModel model = new TagViewModel(tag);

            //Act/Assert
            Assert.NotNull(model.TagType);
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Image);
        }
    }
}
