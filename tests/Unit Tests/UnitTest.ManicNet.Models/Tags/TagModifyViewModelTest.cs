﻿using ManicNet.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Models.Tags
{
    public sealed class TagModifyViewModelTest
    {
        [Fact]
        public void TagModifyModelDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagModifyModel model = new TagModifyModel();

            //Act/Assert
            Assert.NotNull(model.TagTypes);
            Assert.NotNull(model.ImpliedTags);
            Assert.NotNull(model.Image);
        }

        [Fact]
        public void TagModifyModelManualConstructor_InitializesAsExpected()
        {
            //Arrange
            TagModifyModel model = new TagModifyModel(Guid.Empty, Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, string.Empty, null);

            //Act/Assert
            Assert.NotNull(model.TagTypes);
            Assert.NotNull(model.ImpliedTags);
            Assert.NotNull(model.Image);
        }
    }
}
