﻿using ManicNet.Domain.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Domain.Images
{
    public sealed class ContentTest
    {
        [Fact]
        public void ContentDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Content model = new Content();

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Pages);
        }

        [Fact]
        public void ContentManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Content model = new Content(string.Empty, Array.Empty<byte>());

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Pages);
        }
    }
}
