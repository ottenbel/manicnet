﻿using ManicNet.Domain.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Domain.Images
{
    public sealed class AvatarTest
    {
        [Fact]
        public void AvatarDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Avatar model = new Avatar();

            //Act/Assert
            Assert.NotNull(model.UserAvatars);
        }

        [Fact]
        public void AvatarInsertConstructor_InitializesAsExpected()
        {
            //Arrange
            Avatar model = new Avatar(string.Empty, Array.Empty<byte>());

            //Act/Assert
            Assert.NotNull(model.UserAvatars);
        }

        [Fact]
        public void AvatarManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Avatar model = new Avatar(Guid.Empty, string.Empty, Array.Empty<byte>());

            //Act/Assert
            Assert.NotNull(model.UserAvatars);
        }
    }
}
