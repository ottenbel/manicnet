﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTest.ManicNet.Domain.Caches
{
    public sealed class TagCacheCleanupTest
    {
        public static readonly Guid TAG_ID_1 = new Guid("3E4B4FC3-AAD3-499B-A46D-A8110FB2B951");
        public static readonly Guid TAG_ID_2 = new Guid("303713B5-B53B-4C4E-801E-258759F87599");
        public static readonly Guid TAG_ID_3 = new Guid("9B09014B-815A-4801-9798-E3A0157BF4A4");
        public static readonly Guid TAG_ID_4 = new Guid("0ADC30BD-E87A-480D-B779-AD8EE07F6368");
        public static readonly Guid TAG_ID_5 = new Guid("966001F3-F6A1-4233-84CF-17F4B8730352");
        public static readonly Guid WORK_ID_1 = new Guid("0444D016-8002-4599-BE99-79289654EDF5");
        public static readonly Guid VOLUME_ID_1 = new Guid("F3960CBA-7F82-40EA-8EB6-31BCA05CC039");
        public static readonly Guid CHAPTER_ID_1 = new Guid("D43E723E-F618-492A-9B1F-FA3B5DA82FD2");

        [Fact]
        public void TagCacheCleanupDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagCacheCleanup model = new TagCacheCleanup();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.RelatedTags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }

        [Theory]
        [MemberData(nameof(TagCacheCleanupTagConstructor_InitializesAsExpectedTheoryData))]
        public void TagCacheCleanupTagConstructor_InitializesAsExpected(Tag input, TagCacheCleanup expected)
        {
            //Arrange
            TagCacheCleanup model = new TagCacheCleanup(input);

            //Act/Assert
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.RelatedTags.OrderBy(t => t), model.RelatedTags.OrderBy(t => t));
            Assert.Equal(expected.Works.OrderBy(t => t), model.Works.OrderBy(t => t));
            Assert.Equal(expected.Volumes.OrderBy(t => t), model.Volumes.OrderBy(t => t));
            Assert.Equal(expected.Chapters.OrderBy(t => t), model.Chapters.OrderBy(t => t));
        }

        public static TheoryData<Tag, TagCacheCleanup> TagCacheCleanupTagConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Tag, TagCacheCleanup>
                {
                    { null, new TagCacheCleanup() },
                    { new Tag(), new TagCacheCleanup() },
                    { new Tag() { TagID = Guid.Empty }, new TagCacheCleanup() },
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        }
                        , new TagCacheCleanup()
                        {
                            Tags = new List<Guid>() { TAG_ID_1 },
                            RelatedTags = new List<Guid>() { TAG_ID_2, TAG_ID_3 },
                            Works = new List<Guid>() { WORK_ID_1 },
                            Volumes = new List<Guid>() { VOLUME_ID_1 },
                            Chapters = new List<Guid>() { CHAPTER_ID_1 },
                        }
                    }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(TagCacheCleanupTagWithRelatedTagsConstructor_InitializesAsExpectedTheoryData))]
        public void TagCacheCleanupTagWithRelatedTagsConstructor_InitializesAsExpected(Tag input, IEnumerable<Guid> relatedTags, TagCacheCleanup expected)
        {
            //Arrange
            TagCacheCleanup model = new TagCacheCleanup(input, relatedTags);

            //Act/Assert
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.RelatedTags.OrderBy(t => t), model.RelatedTags.OrderBy(t => t));
            Assert.Equal(expected.Works.OrderBy(t => t), model.Works.OrderBy(t => t));
            Assert.Equal(expected.Volumes.OrderBy(t => t), model.Volumes.OrderBy(t => t));
            Assert.Equal(expected.Chapters.OrderBy(t => t), model.Chapters.OrderBy(t => t));
        }

        public static TheoryData<Tag, IEnumerable<Guid>, TagCacheCleanup> TagCacheCleanupTagWithRelatedTagsConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Tag, IEnumerable<Guid>, TagCacheCleanup>
                {
                    { null, null, new TagCacheCleanup() },
                    { new Tag(), null, new TagCacheCleanup() },
                    { null, new List<Guid>(), new TagCacheCleanup() },
                    { new Tag(), new List<Guid>(), new TagCacheCleanup() },
                    { new Tag() { TagID = Guid.Empty }, new List<Guid>(), new TagCacheCleanup() },
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new List<Guid>(),
                        new TagCacheCleanup()
                        {
                            Tags = new List<Guid>() { TAG_ID_1 },
                            RelatedTags = new List<Guid>() { TAG_ID_2, TAG_ID_3 },
                            Works = new List<Guid>() { WORK_ID_1 },
                            Volumes = new List<Guid>() { VOLUME_ID_1 },
                            Chapters = new List<Guid>() { CHAPTER_ID_1 },
                        }
                    },
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new List<Guid>(),
                        new TagCacheCleanup()
                        {
                            Tags = new List<Guid>() { TAG_ID_1 },
                            RelatedTags = new List<Guid>() { TAG_ID_2, TAG_ID_3 },
                            Works = new List<Guid>() { WORK_ID_1 },
                            Volumes = new List<Guid>() { VOLUME_ID_1 },
                            Chapters = new List<Guid>() { CHAPTER_ID_1 },
                        }
                    },
                    { new Tag(), new List<Guid>() { TAG_ID_4, TAG_ID_5 }, new TagCacheCleanup() { RelatedTags = new List<Guid>() { TAG_ID_4, TAG_ID_5 } } },
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new List<Guid>() { TAG_ID_4, TAG_ID_5 },
                        new TagCacheCleanup()
                        {
                            Tags = new List<Guid>() { TAG_ID_1 },
                            RelatedTags = new List<Guid>() { TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5 },
                            Works = new List<Guid>() { WORK_ID_1 },
                            Volumes = new List<Guid>() { VOLUME_ID_1 },
                            Chapters = new List<Guid>() { CHAPTER_ID_1 },
                        }
                    },
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new List<Guid>() { TAG_ID_4, TAG_ID_5, TAG_ID_2 },
                        new TagCacheCleanup()
                        {
                            Tags = new List<Guid>() { TAG_ID_1 },
                            RelatedTags = new List<Guid>() { TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5 },
                            Works = new List<Guid>() { WORK_ID_1 },
                            Volumes = new List<Guid>() { VOLUME_ID_1 },
                            Chapters = new List<Guid>() { CHAPTER_ID_1 },
                        }
                    }
                };

                return data;
            }
        }
    }
}
