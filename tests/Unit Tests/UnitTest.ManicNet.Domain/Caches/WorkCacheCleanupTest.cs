﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTest.ManicNet.Domain.Caches
{
    public sealed class WorkCacheCleanupTest
    {
        public static readonly Guid WORK_ID_1 = new Guid("98441DDB-BDD8-4F82-8ABD-DD2EB0489A6F");
        public static readonly Guid VOLUME_ID_1 = new Guid("4277EAC6-F23B-4FE8-8F43-993795E134C2");
        public static readonly Guid VOLUME_ID_2 = new Guid("60F0ACD8-94AC-4ADE-B525-0EDE63E78897");
        public static readonly Guid VOLUME_ID_3 = new Guid("76435688-4C97-49E8-BBD2-B25333D52CAF");
        public static readonly Guid CHAPTER_ID_1 = new Guid("C81EBAD0-564A-4772-947C-FECBF360B549");
        public static readonly Guid CHAPTER_ID_2 = new Guid("FA89FB8F-E782-40B1-A854-CFFA2FC75B44");
        public static readonly Guid CHAPTER_ID_3 = new Guid("6ACDE3AD-69D7-4A11-A177-2823BDD5CAE9");
        public static readonly Guid TAG_ID_1 = new Guid("412639B9-F4DA-4A05-A15C-E6C1A87F4E63");
        public static readonly Guid TAG_ID_2 = new Guid("4CD1E469-67EE-43E5-B1EC-6622C8DD2554");
        public static readonly Guid TAG_ID_3 = new Guid("66CC8A17-05EB-465D-8881-D086934768EB");
        public static readonly Guid TAG_ID_4 = new Guid("C7E36C49-354C-4001-9458-129144F6015B");
        public static readonly Guid TAG_ID_5 = new Guid("45F47D40-D84F-47C9-9BAA-F02E8DD3788B");
        public static readonly Guid TAG_ID_6 = new Guid("25FC5645-F38E-4EA0-B942-BCF744F7BC30");
        public static readonly Guid TAG_ID_7 = new Guid("F48F4CBD-09B0-4674-AF5D-6726A1589A08");
        public static readonly Guid TAG_ID_8 = new Guid("0B6EE652-B95F-4935-A555-AC4E4A467528");
        public static readonly Guid TAG_ID_9 = new Guid("43D7ED6C-45BF-4E08-A9F3-F480CF5A1D08");
        public static readonly Guid TAG_ID_10 = new Guid("CD9C0622-F41B-4845-B312-5CD15A8BC863");
        public static readonly Guid TAG_ID_11 = new Guid("3FC63FBA-EFFE-4DBA-8B96-29910F47F6CF");
        public static readonly Guid TAG_ID_12 = new Guid("70F8F428-6B89-4954-9367-EE69EFAFB4F7");
        public static readonly Guid TAG_ID_13 = new Guid("EAC8DFE2-00FE-4F0D-9134-4E3D3D6A930B");
        public static readonly Guid TAG_ID_14 = new Guid("ADD32A7A-B511-4252-95C3-D1313F95C760");
        public static readonly Guid TAG_ID_15 = new Guid("623522A3-2AD2-4147-9937-024A3F500AC2");
        public static readonly Guid TAG_ID_16 = new Guid("4F73DB2E-1EB4-4FC6-8D34-FC1279624291");
        public static readonly Guid TAG_ID_17 = new Guid("54A0D6E7-8351-4FFA-8842-7F1417D58B25");
        public static readonly Guid TAG_ID_18 = new Guid("F653F13C-B34D-41A2-8EF4-3145877E052A");
        public static readonly Guid TAG_ID_19 = new Guid("22CABB5F-FD3D-4A43-BA8A-C78FBA2910B6");
        public static readonly Guid TAG_ID_20 = new Guid("35DC4D37-2377-4410-AD00-D71BAA6AA1BB");
        public static readonly Guid TAG_ID_21 = new Guid("A761C690-BC70-43E9-9CCC-8301A6EC7751");
        public static readonly Guid TAG_ID_22 = new Guid("D0800411-01FA-43C2-9075-999615143DB4");
        public static readonly Guid TAG_ID_23 = new Guid("10BFDFD9-0439-438C-A1CC-E52F0722ED8A");
        public static readonly Guid TAG_ID_24 = new Guid("E50394A8-79F8-4647-8A27-CAFD1C2A0E94");
        public static readonly Guid TAG_ID_25 = new Guid("C3899017-EE44-43C2-B221-499F3D8A12B0");
        public static readonly Guid COLLECTION_ID_1 = new Guid("2061B84C-7157-4EE8-AE66-7F1AEE649A4C");
        public static readonly Guid COLLECTION_ID_2 = new Guid("3B21DD87-8B59-4C3A-A49F-E9BAC42DE10D");
        public static readonly Guid COLLECTION_ID_3 = new Guid("42ED5238-B540-4F89-A5A3-BCEAF2811E3D");
        public static readonly Guid COLLECTION_ID_4 = new Guid("2CA9AB43-867E-49CB-9E40-C08B8F191D8F");

        [Fact]
        public void WorkCacheCleanupDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            WorkCacheCleanup model = new WorkCacheCleanup();

            //Act/Assert
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Collections);
        }

        [Theory]
        [MemberData(nameof(WorkCacheCleanupWorkConstructor_InitializesAsExpectedTheoryData))]
        public void WorkCacheCleanupWorkConstructor_InitializesAsExpected(Work input, WorkCacheCleanup expected)
        {
            //Arrange
            WorkCacheCleanup model = new WorkCacheCleanup(input);

            //Act/Assert    
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
            Assert.Equal(expected.Volumes.OrderBy(v => v), model.Volumes.OrderBy(v => v));
            Assert.Equal(expected.Chapters.OrderBy(c => c), model.Chapters.OrderBy(c => c));
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.Collections.OrderBy(c => c), model.Collections.OrderBy(c => c));
        }

        public static TheoryData<Work, WorkCacheCleanup> WorkCacheCleanupWorkConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Work, WorkCacheCleanup>
                {
                    { null, new WorkCacheCleanup() },
                    { new Work(), new WorkCacheCleanup() },
                    { new Work() { WorkID = Guid.Empty }, new WorkCacheCleanup() },
                    { new Work()
                    {
                        WorkID = WORK_ID_1,
                        Tags = new List<Work_Tag>()
                        {
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_1 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_2 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_3 }
                        },
                        Volumes = new List<Volume>()
                        {
                            new Volume()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_1,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_4 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_5 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_6 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_1,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_7 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_8 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_9 }
                                        }
                                    },
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_2,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_10 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_11 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_12 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_2,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_13 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_14 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_15 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_3,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_16 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_17 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_18 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_3,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_19 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_20 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_21 }
                                }
                            }
                        },
                        Collections = new List<Collection_Work>()
                        {
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_1 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_2 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_3 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_4 },
                        }
                    }, new WorkCacheCleanup()
                    {
                        Works = new List<Guid>() { WORK_ID_1 },
                        Volumes = new List<Guid>() { VOLUME_ID_1, VOLUME_ID_2, VOLUME_ID_3 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1, CHAPTER_ID_2, CHAPTER_ID_3 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5, TAG_ID_6, TAG_ID_7, TAG_ID_8, TAG_ID_9, TAG_ID_10, TAG_ID_11, TAG_ID_12, TAG_ID_13, TAG_ID_14, TAG_ID_15, TAG_ID_16, TAG_ID_17, TAG_ID_18, TAG_ID_19, TAG_ID_20, TAG_ID_21},
                        Collections = new List<Guid>() { COLLECTION_ID_1, COLLECTION_ID_2, COLLECTION_ID_3, COLLECTION_ID_4 }
                    } },
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(WorkCacheCleanupWorkWithTagsConstructor_InitializesAsExpectedTheoryData))]
        public void WorkCacheCleanupWorkWithTagsConstructor_InitializesAsExpected(Work input, List<Guid> tags, WorkCacheCleanup expected)
        {
            //Arrange
            WorkCacheCleanup model = new WorkCacheCleanup(input, tags);

            //Act/Assert    
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
            Assert.Equal(expected.Volumes.OrderBy(v => v), model.Volumes.OrderBy(v => v));
            Assert.Equal(expected.Chapters.OrderBy(c => c), model.Chapters.OrderBy(c => c));
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.Collections.OrderBy(c => c), model.Collections.OrderBy(c => c));
        }

        public static TheoryData<Work, List<Guid>, WorkCacheCleanup> WorkCacheCleanupWorkWithTagsConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Work, List<Guid>, WorkCacheCleanup>
                {
                    { null, null, new WorkCacheCleanup() },
                    { null, null, new WorkCacheCleanup() },
                    { new Work(), null, new WorkCacheCleanup() },
                    { null, new List<Guid>(), new WorkCacheCleanup() },
                    { new Work(), new List<Guid>(), new WorkCacheCleanup() },
                    { new Work() { WorkID = Guid.Empty }, new List<Guid>(), new WorkCacheCleanup() },
                    { new Work()
                    {
                        WorkID = WORK_ID_1,
                        Tags = new List<Work_Tag>()
                        {
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_1 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_2 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_3 }
                        },
                        Volumes = new List<Volume>()
                        {
                            new Volume()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_1,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_4 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_5 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_6 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_1,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_7 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_8 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_9 }
                                        }
                                    },
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_2,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_10 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_11 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_12 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_2,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_13 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_14 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_15 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_3,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_16 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_17 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_18 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_3,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_19 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_20 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_21 }
                                }
                            }
                        },
                        Collections = new List<Collection_Work>()
                        {
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_1 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_2 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_3 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_4 },
                        }
                    }, new List<Guid>(), new WorkCacheCleanup()
                    {
                        Works = new List<Guid>() { WORK_ID_1 },
                        Volumes = new List<Guid>() { VOLUME_ID_1, VOLUME_ID_2, VOLUME_ID_3 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1, CHAPTER_ID_2, CHAPTER_ID_3 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5, TAG_ID_6, TAG_ID_7, TAG_ID_8, TAG_ID_9, TAG_ID_10, TAG_ID_11, TAG_ID_12, TAG_ID_13, TAG_ID_14, TAG_ID_15, TAG_ID_16, TAG_ID_17, TAG_ID_18, TAG_ID_19, TAG_ID_20, TAG_ID_21},
                        Collections = new List<Guid>() { COLLECTION_ID_1, COLLECTION_ID_2, COLLECTION_ID_3, COLLECTION_ID_4 }
                    } },
                    { new Work(), new List<Guid>() { TAG_ID_20, TAG_ID_21, TAG_ID_22, TAG_ID_23, TAG_ID_24, TAG_ID_25 },  new WorkCacheCleanup() { Tags = new List<Guid>(){ TAG_ID_20, TAG_ID_21, TAG_ID_22, TAG_ID_23, TAG_ID_24, TAG_ID_25 } } },
                    { new Work()
                    {
                        WorkID = WORK_ID_1,
                        Tags = new List<Work_Tag>()
                        {
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_1 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_2 },
                            new Work_Tag() { WorkID = WORK_ID_1, TagID = TAG_ID_3 }
                        },
                        Volumes = new List<Volume>()
                        {
                            new Volume()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_1,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_4 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_5 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_1, TagID = TAG_ID_6 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_1,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_7 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_8 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_1, TagID = TAG_ID_9 }
                                        }
                                    },
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_2,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_10 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_11 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_2, TagID = TAG_ID_12 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_2,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_13 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_14 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_2, TagID = TAG_ID_15 }
                                },
                                Chapters = new List<Chapter>()
                                {
                                    new Chapter()
                                    {
                                        ChapterID = CHAPTER_ID_3,
                                        Tags = new List<Chapter_Tag>()
                                        {
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_16 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_17 },
                                            new Chapter_Tag() { ChapterID = CHAPTER_ID_3, TagID = TAG_ID_18 }
                                        }
                                    }
                                }
                            },
                            new Volume ()
                            {
                                WorkID = WORK_ID_1,
                                VolumeID = VOLUME_ID_3,
                                Tags = new List<Volume_Tag>()
                                {
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_19 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_20 },
                                    new Volume_Tag() { VolumeID = VOLUME_ID_3, TagID = TAG_ID_21 }
                                }
                            }
                        },
                        Collections = new List<Collection_Work>()
                        {
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_1 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_2 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_3 },
                            new Collection_Work() { WorkID = WORK_ID_1, CollectionID = COLLECTION_ID_4 },
                        }
                    },
                        new List<Guid>() { TAG_ID_20, TAG_ID_21, TAG_ID_22, TAG_ID_23, TAG_ID_24, TAG_ID_25 },
                        new WorkCacheCleanup()
                    {
                        Works = new List<Guid>() { WORK_ID_1 },
                        Volumes = new List<Guid>() { VOLUME_ID_1, VOLUME_ID_2, VOLUME_ID_3 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1, CHAPTER_ID_2, CHAPTER_ID_3 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5, TAG_ID_6, TAG_ID_7, TAG_ID_8, TAG_ID_9, TAG_ID_10, TAG_ID_11, TAG_ID_12, TAG_ID_13, TAG_ID_14, TAG_ID_15, TAG_ID_16, TAG_ID_17, TAG_ID_18, TAG_ID_19, TAG_ID_20, TAG_ID_21, TAG_ID_22, TAG_ID_23, TAG_ID_24, TAG_ID_25 },
                        Collections = new List<Guid>() { COLLECTION_ID_1, COLLECTION_ID_2, COLLECTION_ID_3, COLLECTION_ID_4 }
                    } },
                };

                return data;
            }
        }
    }
}
