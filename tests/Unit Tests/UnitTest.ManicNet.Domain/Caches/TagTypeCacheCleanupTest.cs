﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTest.ManicNet.Domain.Caches
{
    public sealed class TagTypeCacheCleanupTest
    {
        public static readonly Guid TAG_TYPE_ID_1 = new Guid("CBD195F4-C232-492F-A0EA-6413048EF529");
        public static readonly Guid TAG_ID_1 = new Guid("3E4B4FC3-AAD3-499B-A46D-A8110FB2B951");
        public static readonly Guid TAG_ID_2 = new Guid("303713B5-B53B-4C4E-801E-258759F87599");
        public static readonly Guid TAG_ID_3 = new Guid("9B09014B-815A-4801-9798-E3A0157BF4A4");
        public static readonly Guid TAG_ID_4 = new Guid("A37AD9F5-D484-4D6B-B5B3-6FF721A36628");
        public static readonly Guid TAG_ID_5 = new Guid("F2CDCD1A-346E-40CB-B1F5-FC6B2C1A967F");
        public static readonly Guid TAG_ID_6 = new Guid("9D19A71A-53D4-41D0-B767-0EBDCF6F9A9E");
        public static readonly Guid TAG_ID_7 = new Guid("FBB1A953-3FAE-4B73-8AF3-C4A5D97635CD");
        public static readonly Guid WORK_ID_1 = new Guid("0444D016-8002-4599-BE99-79289654EDF5");
        public static readonly Guid WORK_ID_2 = new Guid("660E9B7C-36D7-482B-9E23-38DF725AFFA0");
        public static readonly Guid VOLUME_ID_1 = new Guid("F3960CBA-7F82-40EA-8EB6-31BCA05CC039");
        public static readonly Guid VOLUME_ID_2 = new Guid("52501BE7-E9D9-44A9-845F-1A6DD4C28B3A");
        public static readonly Guid CHAPTER_ID_1 = new Guid("D43E723E-F618-492A-9B1F-FA3B5DA82FD2");
        public static readonly Guid CHAPTER_ID_2 = new Guid("B39A34A7-C2DD-4CE6-A7A2-C89C40B8916C");

        [Fact]
        public void TagTypeCacheCleanupDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagTypeCacheCleanup model = new TagTypeCacheCleanup();

            //Act/Assert
            Assert.NotNull(model.TagTypes);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }

        [Theory]
        [MemberData(nameof(TagTypeCacheCleanupTagTypeConstructor_InitializesAsExpectedTheoryData))]
        public void TagTypeCacheCleanupTagTypeConstructor_InitializesAsExpected(TagType input, TagTypeCacheCleanup expected)
        {
            //Arrange
            TagTypeCacheCleanup model = new TagTypeCacheCleanup(input);

            //Act/Assert
            Assert.Equal(expected.TagTypes.OrderBy(t => t), model.TagTypes.OrderBy(t => t));
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
            Assert.Equal(model.Volumes.OrderBy(v => v), model.Volumes.OrderBy(v => v));
            Assert.Equal(model.Chapters.OrderBy(c => c), model.Chapters.OrderBy(c => c));
        }

        public static TheoryData<TagType, TagTypeCacheCleanup> TagTypeCacheCleanupTagTypeConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<TagType, TagTypeCacheCleanup>
                {
                    { null, new TagTypeCacheCleanup() },
                    { new TagType(), new TagTypeCacheCleanup() },
                    { new TagType() { TagTypeID = Guid.Empty }, new TagTypeCacheCleanup() },
                    { new TagType() { TagTypeID = TAG_TYPE_ID_1 }, new TagTypeCacheCleanup() { TagTypes = new List<Guid>() { TAG_TYPE_ID_1 } } },
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(TagTypeCacheCleanupTagTypeWithTagsConstructor_InitializesAsExpectedTheoryData))]
        public void TagTypeCacheCleanupTagTypeWithTagsConstructor_InitializesAsExpected(TagType input, List<Tag> tags, TagTypeCacheCleanup expected)
        {
            //Arrange
            TagTypeCacheCleanup model = new TagTypeCacheCleanup(input, tags);

            //Act/Assert
            Assert.Equal(expected.TagTypes.OrderBy(t => t), model.TagTypes.OrderBy(t => t));
            Assert.Equal(expected.Tags.OrderBy(t => t), model.Tags.OrderBy(t => t));
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
            Assert.Equal(model.Volumes.OrderBy(v => v), model.Volumes.OrderBy(v => v));
            Assert.Equal(model.Chapters.OrderBy(c => c), model.Chapters.OrderBy(c => c));
        }

        public static TheoryData<TagType, List<Tag>, TagTypeCacheCleanup> TagTypeCacheCleanupTagTypeWithTagsConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<TagType, List<Tag>, TagTypeCacheCleanup>
                {
                    { null, null, new TagTypeCacheCleanup() },
                    { new TagType(), null, new TagTypeCacheCleanup() },
                    { null, new List<Tag>(), new TagTypeCacheCleanup() },
                    { new TagType(), new List<Tag>(), new TagTypeCacheCleanup() },
                    { new TagType() { TagTypeID = Guid.Empty }, new List<Tag>(), new TagTypeCacheCleanup() },
                    { new TagType() { TagTypeID = TAG_TYPE_ID_1 }, new List<Tag>(), new TagTypeCacheCleanup() { TagTypes = new List<Guid>() { TAG_TYPE_ID_1 } } },
                    { new TagType() { TagTypeID = TAG_TYPE_ID_1 }, new List<Tag>()
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                    }, new TagTypeCacheCleanup()
                    {
                        TagTypes = new List<Guid>() { TAG_TYPE_ID_1 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3 },
                        Works = new List<Guid>() { WORK_ID_1 },
                        Volumes = new List<Guid>() { VOLUME_ID_1 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1 },
                    } },
                    { new TagType() { TagTypeID = TAG_TYPE_ID_1 }, new List<Tag>()
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new Tag ()
                        {
                            TagID = TAG_ID_4,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_5, ImpliesTagID = TAG_ID_4 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_4, ImpliesTagID = TAG_ID_6 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_4, WorkID = WORK_ID_2 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_4, VolumeID = VOLUME_ID_2 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_4, ChapterID = CHAPTER_ID_2 } }
                        },
                    }, new TagTypeCacheCleanup()
                    {
                        TagTypes = new List<Guid>() { TAG_TYPE_ID_1 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5, TAG_ID_6 },
                        Works = new List<Guid>() { WORK_ID_1, WORK_ID_2 },
                        Volumes = new List<Guid>() { VOLUME_ID_1, VOLUME_ID_2 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1, CHAPTER_ID_2 },
                    } },
                    { new TagType() { TagTypeID = TAG_TYPE_ID_1 }, new List<Tag>()
                    {
                        new Tag ()
                        {
                            TagID = TAG_ID_1,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_1 } },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_1, ImpliesTagID = TAG_ID_3 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_1, WorkID = WORK_ID_1 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_1, VolumeID = VOLUME_ID_1 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_1, ChapterID = CHAPTER_ID_1 } }
                        },
                        new Tag ()
                        {
                            TagID = TAG_ID_4,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_5, ImpliesTagID = TAG_ID_4 },  },
                            Implies = new List<Tag_Tag>() { new Tag_Tag() { ImpliedByTagID = TAG_ID_4, ImpliesTagID = TAG_ID_6 } },
                            Works = new List<Work_Tag>() { new Work_Tag() { TagID = TAG_ID_4, WorkID = WORK_ID_2 } },
                            Volumes = new List<Volume_Tag>() { new Volume_Tag() { TagID = TAG_ID_4, VolumeID = VOLUME_ID_2 } },
                            Chapters = new List<Chapter_Tag>() { new Chapter_Tag() { TagID = TAG_ID_4, ChapterID = CHAPTER_ID_2 } }
                        },
                        //Tag with all dupes
                        new Tag ()
                        {
                            TagID = TAG_ID_7,
                            TagTypeID = TAG_TYPE_ID_1,
                            ImpliedBy = new List<Tag_Tag>()
                            {
                                new Tag_Tag() { ImpliedByTagID = TAG_ID_5, ImpliesTagID = TAG_ID_7 },
                                new Tag_Tag() { ImpliedByTagID = TAG_ID_2, ImpliesTagID = TAG_ID_7 }
                            },
                            Implies = new List<Tag_Tag>()
                            {
                                new Tag_Tag() { ImpliedByTagID = TAG_ID_7, ImpliesTagID = TAG_ID_6 },
                                new Tag_Tag() { ImpliedByTagID = TAG_ID_7, ImpliesTagID = TAG_ID_3 }
                            },
                            Works = new List<Work_Tag>()
                            {
                                new Work_Tag() { TagID = TAG_ID_7, WorkID = WORK_ID_2 },
                                new Work_Tag() { TagID = TAG_ID_7, WorkID = WORK_ID_1 }
                            },
                            Volumes = new List<Volume_Tag>()
                            {
                                new Volume_Tag() { TagID = TAG_ID_7, VolumeID = VOLUME_ID_2 },
                                new Volume_Tag() { TagID = TAG_ID_7, VolumeID = VOLUME_ID_1 }
                            },
                            Chapters = new List<Chapter_Tag>()
                            {
                                new Chapter_Tag() { TagID = TAG_ID_7, ChapterID = CHAPTER_ID_2 },
                                new Chapter_Tag() { TagID = TAG_ID_7, ChapterID = CHAPTER_ID_2 }
                            }
                        },
                    }, new TagTypeCacheCleanup()
                    {
                        TagTypes = new List<Guid>() { TAG_TYPE_ID_1 },
                        Tags = new List<Guid>() { TAG_ID_1, TAG_ID_2, TAG_ID_3, TAG_ID_4, TAG_ID_5, TAG_ID_6, TAG_ID_7 },
                        Works = new List<Guid>() { WORK_ID_1, WORK_ID_2 },
                        Volumes = new List<Guid>() { VOLUME_ID_1, VOLUME_ID_2 },
                        Chapters = new List<Guid>() { CHAPTER_ID_1, CHAPTER_ID_2 },
                    } },
                };

                return data;
            }
        }
    }
}
