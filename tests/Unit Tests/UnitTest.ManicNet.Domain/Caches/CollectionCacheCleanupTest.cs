﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace UnitTest.ManicNet.Domain.Caches
{
    public sealed class CollectionCacheCleanupTest
    {
        public static readonly Guid COLLECTION_ID_1 = new Guid("A48F55AE-C9BB-4465-BB40-D4FE24580410");
        public static readonly Guid WORK_ID_1 = new Guid("EEA57903-C0CE-491C-8C4A-3A2D32F645EA");
        public static readonly Guid WORK_ID_2 = new Guid("81464AF6-9B0D-4F7D-A337-2F873F5D9C30");
        public static readonly Guid WORK_ID_3 = new Guid("8CA55ED8-C4A3-4D13-A193-C48FA7FF331F");
        public static readonly Guid WORK_ID_4 = new Guid("4053592F-3928-4FE3-A54C-6B7FAB288B7B");
        public static readonly Guid WORK_ID_5 = new Guid("F1804DAC-554B-4A0E-B9CC-B763F72A8065");

        [Fact]
        public void CollectionCacheCleanupDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CollectionCacheCleanup model = new CollectionCacheCleanup();

            //Act/Assert
            Assert.NotNull(model.Collections);
            Assert.NotNull(model.Works);
        }

        [Theory]
        [MemberData(nameof(CollectionCacheCleanupCollectionConstructor_InitializesAsExpectedTheoryData))]
        public void CollectionCacheCleanupCollectionConstructor_InitializesAsExpected(Collection collection, CollectionCacheCleanup expected)
        {
            //Arrange
            CollectionCacheCleanup model = new CollectionCacheCleanup(collection);

            //Act/Assert
            Assert.Equal(expected.Collections.OrderBy(c => c), model.Collections.OrderBy(c => c));
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
        }

        public static TheoryData<Collection, CollectionCacheCleanup> CollectionCacheCleanupCollectionConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Collection, CollectionCacheCleanup>
                {
                    { null, new CollectionCacheCleanup() },
                    { new Collection(), new CollectionCacheCleanup() },
                    { new Collection() { CollectionID = Guid.Empty }, new CollectionCacheCleanup() },
                    { new Collection() { CollectionID = COLLECTION_ID_1 }, new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_2 } } }, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2 } } },
                    { new Collection() { CollectionID = COLLECTION_ID_1, Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 }, Works = new List<Guid>(){ WORK_ID_1 } } },
                    { new Collection() { CollectionID = COLLECTION_ID_1, Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_2 } } }, new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 }, Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2 } } }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(CollectionCacheCleanupCollectionAndWorksConstructor_InitializesAsExpectedTheoryData))]
        public void CollectionCacheCleanupCollectionAndWorksConstructor_InitializesAsExpected(Collection collection, IEnumerable<Guid> works, CollectionCacheCleanup expected)
        {
            //Arrange
            CollectionCacheCleanup model = new CollectionCacheCleanup(collection, works);

            //Act/Assert
            Assert.Equal(expected.Collections.OrderBy(c => c), model.Collections.OrderBy(c => c));
            Assert.Equal(expected.Works.OrderBy(w => w), model.Works.OrderBy(w => w));
        }

        public static TheoryData<Collection, IEnumerable<Guid>, CollectionCacheCleanup> CollectionCacheCleanupCollectionAndWorksConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<Collection, IEnumerable<Guid>, CollectionCacheCleanup>
                {
                    { null, null, new CollectionCacheCleanup() },
                    { new Collection(), null, new CollectionCacheCleanup() },
                    { null, new List<Guid>(), new CollectionCacheCleanup() },
                    { new Collection(), new List<Guid>(), new CollectionCacheCleanup() },
                    { new Collection() { CollectionID = Guid.Empty }, new List<Guid>(),  new CollectionCacheCleanup() },
                    { new Collection() { CollectionID = COLLECTION_ID_1 }, new List<Guid>(), new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, null, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_2 } } }, null, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, new List<Guid>(), new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1 } } },
                    { new Collection() { Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_2 } } }, new List<Guid>(), new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2 } } },
                    { new Collection(), new List<Guid>() { WORK_ID_1 }, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1 } } },
                    { new Collection(), new List<Guid>() { WORK_ID_1, WORK_ID_2 }, new CollectionCacheCleanup() { Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2 } } },
                    { new Collection() { CollectionID = COLLECTION_ID_1, Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, new List<Guid>() { WORK_ID_3 }, new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 }, Works = new List<Guid>(){ WORK_ID_1, WORK_ID_3 } } },
                    { new Collection() { CollectionID = COLLECTION_ID_1, Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_2 } } }, new List<Guid>() { WORK_ID_4, WORK_ID_5 }, new CollectionCacheCleanup() { Collections = new List<Guid>(){ COLLECTION_ID_1 }, Works = new List<Guid>(){ WORK_ID_1, WORK_ID_2, WORK_ID_4, WORK_ID_5 } } },
                    { new Collection(){ Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 }, new Collection_Work() { WorkID = WORK_ID_1 } } }, new List<Guid>(), new CollectionCacheCleanup() { Works = new List<Guid>() { WORK_ID_1 } } },
                    { new Collection(), new List<Guid>() { WORK_ID_1, WORK_ID_1 }, new CollectionCacheCleanup() { Works = new List<Guid>() { WORK_ID_1 } } },
                    { new Collection(){ Works = new List<Collection_Work>() { new Collection_Work() { WorkID = WORK_ID_1 } } }, new List<Guid>() { WORK_ID_1 }, new CollectionCacheCleanup(){ Works = new List<Guid>() { WORK_ID_1 } } }
                };

                return data;
            }
        }
    }
}
