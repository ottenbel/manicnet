﻿using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using Xunit;

namespace UnitTest.ManicNet.Domain.Caches
{
    public sealed class RegistrationInviteCacheCleanupTest
    {
        [Fact]
        public void RegistrationInviteCacheCleanupDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            RegistrationInviteCacheCleanup model = new RegistrationInviteCacheCleanup();

            //Act/Assert
            Assert.NotNull(model.RegistrationInvites);
        }

        [Theory]
        [MemberData(nameof(RegistrationInviteCacheCleanupRegistrationInviteConstructor_InitializesAsExpectedTheoryData))]
        public void RegistrationInviteCacheCleanupRegistrationInviteConstructor_InitializesAsExpected(RegistrationInvite input)
        {
            //Arrange
            RegistrationInviteCacheCleanup model = new RegistrationInviteCacheCleanup(input);

            //Act/Assert
            Assert.NotNull(model.RegistrationInvites);
        }

        public static TheoryData<RegistrationInvite> RegistrationInviteCacheCleanupRegistrationInviteConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<RegistrationInvite>
                {
                    { null },
                    { new RegistrationInvite() }
                };

                return data;
            }
        }
    }
}
