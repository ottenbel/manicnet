﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Domain.Works
{
    public sealed class ChapterTest
    {
        [Fact]
        public void ChapterDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Chapter model = new Chapter();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Pages);
        }

        [Theory]
        [MemberData(nameof(ChapterManualConstructor_InitializesAsExpectedTheoryData))]
        public void ChapterManualConstructor_InitializesAsExpected(List<Page> pages)
        {
            //Arrange
            Chapter model = new Chapter(Guid.Empty, new Volume(), 0, string.Empty, pages, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, Guid.Empty, DateTime.UtcNow, Guid.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Pages);
        }

        public static TheoryData<List<Page>> ChapterManualConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Page>>
                {
                    { null },
                    { new List<Page>() }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(ChapterManualConstructorWithTags_InitializesAsExpectedTheoryData))]
        public void ChapterManualConstructorWithTags_InitializesAsExpected(List<Page> pages, List<Chapter_Tag> tags)
        {
            //Arrange
            Chapter model = new Chapter(Guid.Empty, new Volume(), 0, string.Empty, pages, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, Guid.Empty, DateTime.UtcNow, Guid.Empty, tags);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Pages);
        }

        public static TheoryData<List<Page>, List<Chapter_Tag>> ChapterManualConstructorWithTags_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Page>, List<Chapter_Tag>>
                {
                    { null, null },
                    { new List<Page>(), null },
                    { null, new List<Chapter_Tag>() },
                    { new List<Page>(), new List<Chapter_Tag>() }
                };

                return data;
            }
        }
    }
}
