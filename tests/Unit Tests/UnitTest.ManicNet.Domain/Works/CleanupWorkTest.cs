﻿using ManicNet.Domain.Models;
using Xunit;

namespace UnitTest.ManicNet.Domain.Works
{
    public sealed class CleanupWorkTest
    {
        [Fact]
        public void CleanupWorkDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            CleanupWork model = new CleanupWork();

            //Act/Assert
            Assert.NotNull(model.Contents);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }

        [Fact]
        public void CleanupWorkManualConstructorWithWork_InitializesAsExpected()
        {
            //Arrange
            CleanupWork model = new CleanupWork(new Work());

            //Act/Assert
            Assert.NotNull(model.Contents);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }

        [Fact]
        public void CleanupWorkManualConstructorWithVolume_InitializesAsExpected()
        {
            //Arrange
            CleanupWork model = new CleanupWork(new Volume());

            //Act/Assert
            Assert.NotNull(model.Contents);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }

        [Fact]
        public void CleanupWorkManualConstructorWithChapter_InitializesAsExpected()
        {
            //Arrange
            CleanupWork model = new CleanupWork(new Chapter());

            //Act/Assert
            Assert.NotNull(model.Contents);
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
        }
    }
}
