﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Domain.Works
{
    public sealed class VolumeTest
    {
        [Fact]
        public void VolumeDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Volume model = new Volume();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
        }

        [Fact]
        public void VolumeManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Volume model = new Volume(Guid.Empty, new Work(), 0, string.Empty, Guid.Empty, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, Guid.Empty, DateTime.UtcNow, Guid.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
        }

        [Theory]
        [MemberData(nameof(VolumeManualConstructorWithTags_InitializesAsExpectedTheoryData))]
        public void VolumeManualConstructorWithTags_InitializesAsExpected(List<Volume_Tag> tags)
        {
            //Arrange
            Volume model = new Volume(Guid.Empty, new Work(), 0, string.Empty, Guid.Empty, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, Guid.Empty, DateTime.UtcNow, Guid.Empty, tags);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Chapters);
        }

        public static TheoryData<List<Volume_Tag>> VolumeManualConstructorWithTags_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Volume_Tag>>
                {
                    { null },
                    { new List<Volume_Tag>() }
                };

                return data;
            }
        }
    }
}
