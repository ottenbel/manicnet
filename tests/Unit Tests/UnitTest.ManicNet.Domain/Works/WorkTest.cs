﻿using ManicNet.Domain.Models;
using System;
using Xunit;

namespace UnitTest.ManicNet.Domain.Works
{
    public sealed class WorkTest
    {
        [Fact]
        public void WorkDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Work model = new Work();

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Collections);
        }

        [Fact]
        public void WorkManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Work model = new Work(Guid.Empty, string.Empty, Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, Guid.Empty, DateTime.UtcNow, Guid.Empty);

            //Act/Assert
            Assert.NotNull(model.Tags);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Collections);
        }
    }
}
