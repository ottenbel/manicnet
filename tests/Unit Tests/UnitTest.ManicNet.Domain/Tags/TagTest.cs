﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Domain.Tags
{
    public sealed class TagTest
    {
        [Fact]
        public void TagDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Tag model = new Tag();

            //Act/Assert
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.UserBlockedTags);
        }

        [Fact]
        public void TagManualConstructor_InitializesAsExpected()
        {
            //Arrange
            Tag model = new Tag(Guid.Empty, Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, false);

            //Act/Assert
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.UserBlockedTags);
        }

        [Theory]
        [MemberData(nameof(TagManualConstructorWithLists_InitializesAsExpectedTheoryData))]
        public void TagManualConstructorWithLists_InitializesAsExpected(List<Guid> impliedByList, List<Guid> impliesList, List<TagAlias> aliases)
        {
            //Arrange
            Tag model = new Tag(Guid.Empty, Guid.Empty, string.Empty, string.Empty, string.Empty, string.Empty, false, false, impliedByList, impliesList, aliases);

            //Act/Assert
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.UserBlockedTags);
        }

        public static TheoryData<List<Guid>, List<Guid>, List<TagAlias>> TagManualConstructorWithLists_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Guid>, List<Guid>, List<TagAlias>>
                {
                    { null, null, null },
                    { new List<Guid>(), new List<Guid>(), new List<TagAlias>() }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(TagManualConstructorWithLists_InitializesAsExpectedTheoryData))]
        public void TagManualConstructorWithTagTypeAndLists_InitializesAsExpected(List<Guid> impliedByList, List<Guid> impliesList, List<TagAlias> aliases)
        {
            //Arrange
            Tag model = new Tag(Guid.Empty, new TagType(), string.Empty, false, false, impliedByList, impliesList, aliases);

            //Act/Assert
            Assert.NotNull(model.ImpliedBy);
            Assert.NotNull(model.Implies);
            Assert.NotNull(model.Aliases);
            Assert.NotNull(model.Works);
            Assert.NotNull(model.Volumes);
            Assert.NotNull(model.Chapters);
            Assert.NotNull(model.UserBlockedTags);
        }

        [Fact]
        public void TagSearchListDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagSearchLists model = new TagSearchLists();

            //Act/Assert
            Assert.NotNull(model.Excluded);
            Assert.NotNull(model.Included);
        }

        [Theory]
        [MemberData(nameof(TagSearchListManualConstructor_InitializesAsExpectedTheoryData))]
        public void TagSearchListManualConstructor_InitializesAsExpected(List<Tag> excluded, List<Tag> included)
        {
            //Arrange
            TagSearchLists model = new TagSearchLists(included, excluded);

            //Act/Assert
            Assert.NotNull(model.Excluded);
            Assert.NotNull(model.Included);
        }

        public static TheoryData<List<Tag>, List<Tag>> TagSearchListManualConstructor_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Tag>, List<Tag>>
                {
                    { null, null },
                    { new List<Tag>(), null },
                    { null, new List<Tag>() },
                    { new List<Tag>(), new List<Tag>() }
                };

                return data;
            }
        }


        [Fact]
        public void RelatedTagListDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            RelatedTagList model = new RelatedTagList();

            //Act/Assert
            Assert.NotNull(model.Related);
        }
    }
}
