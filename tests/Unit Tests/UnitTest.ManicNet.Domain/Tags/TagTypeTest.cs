﻿using ManicNet.Domain.Models;
using System;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Domain.Tags
{
    public sealed class TagTypeTest
    {
        [Fact]
        public void TagTypeDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            TagType model = new TagType();

            //Act/Assert
            Assert.NotNull(model.Tags);
        }

        [Fact]
        public void TagTypeManualConstructor_InitializesAsExpected()
        {
            //Arrange
            TagType model = new TagType(Guid.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, true, true, true, true, true, true, 0, 0, 0, false);

            //Act/Assert
            Assert.NotNull(model.Tags);

        }

        [Theory]
        [MemberData(nameof(TagTypeManualConstructorWithTags_InitializesAsExpectedTheoryData))]
        public void TagTypeManualConstructorWithTags_InitializesAsExpected(List<Tag> tags)
        {
            //Arrange
            TagType model = new TagType(Guid.Empty, string.Empty, string.Empty, 0, string.Empty, string.Empty, string.Empty, true, true, true, true, true, true, 0, 0, 0, false, tags);

            //Act/Assert
            Assert.NotNull(model.Tags);
        }

        public static TheoryData<List<Tag>> TagTypeManualConstructorWithTags_InitializesAsExpectedTheoryData
        {
            get
            {
                var data = new TheoryData<List<Tag>>
                {
                    { null },
                    { new List<Tag>() }
                };

                return data;
            }
        }
    }
}
