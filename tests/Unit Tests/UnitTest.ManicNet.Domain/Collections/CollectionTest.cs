﻿using ManicNet.Domain.Models;
using Xunit;

namespace UnitTest.ManicNet.Domain.Collections
{
    public sealed class CollectionTest
    {
        [Fact]
        public void CollectionDefaultConstructor_InitializesAsExpected()
        {
            //Arrange
            Collection model = new Collection();

            //Act/Assert
            Assert.NotNull(model.Works);
        }
    }
}
