﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Users
{
    public sealed class RegistrationInviteServiceTest
    {
        private readonly List<RegistrationInvite> registrationInvites;
        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<IRegistrationInviteRepository> mockRegistrationInviteRepository;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;
        private readonly Mock<IManicNetLoggerService> mockLogger;

        private const string PENDING_REGISTRATION_INVITE_ID_1 = "EBAF39F8-3400-416D-B902-3B48BE411527";
        private const string PENDING_REGISTRATION_INVITE_ID_2 = "777D54BA-0E8E-40A1-8236-76FE08E183D9";
        private const string APPROVED_REGISTRATION_INVITE_ID_1 = "8701D1B9-3604-47D7-B21C-B3F86030ACDA";
        private const string APPROVED_REGISTRATION_INVITE_ID_2 = "5443AA4E-2ADC-474C-8EB6-D56A09C0BBDA";
        private const string APPROVED_REGISTRATION_INVITE_ID_3 = "3C3EC8DE-3AFF-47C9-8874-5FA5BC53B172";
        private const string APPROVED_REGISTRATION_INVITE_ID_4 = "2C00D9C7-5E6B-466F-8D77-BB5206BE0E88";
        private const string EXPIRED_REGISTRATION_INVITE_ID_1 = "438FCF63-B0AC-424A-8DDE-40CA07F67269";
        private const string EXPIRED_REGISTRATION_INVITE_ID_2 = "7D8A5956-2728-40EB-9278-C7C498DF295F";
        private const string EXPIRED_REGISTRATION_INVITE_ID_3 = "32CD40E7-8E05-4982-95D9-27386DCEAB7E";
        private const string NON_EXISTING_REGISTRATION_INVITE_ID_1 = "785D3563-D23F-45B3-AB20-DBD6BCEF2903";

        private static readonly RegistrationInvite pending1 = new RegistrationInvite(new Guid(PENDING_REGISTRATION_INVITE_ID_1), "pendingregistrationinvite1@test.com", string.Empty);
        private static readonly RegistrationInvite pending2 = new RegistrationInvite(new Guid(PENDING_REGISTRATION_INVITE_ID_2), "pendingregistrationinvite2@test.com", string.Empty);
        private static readonly RegistrationInvite approved1 = new RegistrationInvite(new Guid(APPROVED_REGISTRATION_INVITE_ID_1), "approvedregistrationinvite1@test.com", string.Empty, true, DateTime.UtcNow.AddDays(7));
        private static readonly RegistrationInvite approved2 = new RegistrationInvite(new Guid(APPROVED_REGISTRATION_INVITE_ID_2), "approvedregistrationinvite2@test.com", string.Empty, true, DateTime.UtcNow.AddDays(6));
        private static readonly RegistrationInvite approved3 = new RegistrationInvite(new Guid(APPROVED_REGISTRATION_INVITE_ID_3), "approvedregistrationinvite3@test.com", string.Empty, true, DateTime.UtcNow.AddDays(5));
        private static readonly RegistrationInvite approved4 = new RegistrationInvite(new Guid(APPROVED_REGISTRATION_INVITE_ID_4), "approvedregistrationinvite4@test.com", string.Empty, true, DateTime.UtcNow.AddDays(4));
        private static readonly RegistrationInvite expired1 = new RegistrationInvite(new Guid(EXPIRED_REGISTRATION_INVITE_ID_1), "expiredregistrationinvite1@test.com", string.Empty, true, DateTime.UtcNow.AddDays(-7));
        private static readonly RegistrationInvite expired2 = new RegistrationInvite(new Guid(EXPIRED_REGISTRATION_INVITE_ID_2), "expiredregistrationinvite2@test.com", string.Empty, true, DateTime.UtcNow.AddDays(-6));
        private static readonly RegistrationInvite expired3 = new RegistrationInvite(new Guid(EXPIRED_REGISTRATION_INVITE_ID_3), "expiredregistrationinvite3@test.com", string.Empty, true, DateTime.UtcNow.AddDays(-5));

        public RegistrationInviteServiceTest()
        {
            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            registrationInvites = CloneHelper.Clone(new List<RegistrationInvite>()
            {
                pending1, pending2, approved1, approved2, approved3, approved4, expired1,
                expired2, expired3
            });

            mockRegistrationInviteRepository = new Mock<IRegistrationInviteRepository>();
            mockRegistrationInviteRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(registrationInvites));
            mockCacheCleanerService = new Mock<ICacheCleanerService>();
        }

        [Theory]
        [MemberData(nameof(FindRegistrationInviteEmailTheoryData))]
        public async Task FindRegistrationInviteByEmail_ReturnsExpectedResult(string email, RegistrationInvite expected)
        {
            //Arrange
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);
            mockRegistrationInviteRepository.Setup(r => r.FindAsync(email)).Returns(Task.FromResult(registrationInvites.Where(i => i.Email == email).FirstOrDefault()));

            //Act
            RegistrationInvite result = await registrationInviteService.FindRegistrationInvite(email);

            //Assert
            Assert.Equal(expected, result);
            mockRegistrationInviteRepository.Verify(mock => mock.FindAsync(email), Times.Once());
        }

        public static TheoryData<string, RegistrationInvite> FindRegistrationInviteEmailTheoryData
        {
            get
            {
                var data = new TheoryData<string, RegistrationInvite>
                {
                    { "pendingregistrationinvite1@test.com", CloneHelper.Clone(pending1) },
                    { "approvedregistrationinvite2@test.com", CloneHelper.Clone(approved2) },
                    { "expiredregistrationinvite3@test.com", CloneHelper.Clone(expired3) },
                    { "doesntexist@test.com", null }
                };
                return data;
            }
        }

        [Theory]
        [MemberData(nameof(FindExistingRegistrationInviteByIDTheoryData))]
        public async Task FindExistingRegistrationInviteByID_ReturnsExpectedResult(Guid registrationInviteID, RegistrationInvite expected)
        {
            //Arrange
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);
            mockRegistrationInviteRepository.Setup(r => r.FindAsync(registrationInviteID, It.IsAny<bool>())).Returns(Task.FromResult(registrationInvites.Where(i => i.RegistrationInviteID == registrationInviteID).FirstOrDefault()));

            //Act
            RegistrationInvite result = await registrationInviteService.FindRegistrationInvite(registrationInviteID);

            //Asssert
            Assert.Equal(expected, result);
            mockRegistrationInviteRepository.Verify(mock => mock.FindAsync(registrationInviteID, false), Times.Once());
        }

        public static TheoryData<Guid, RegistrationInvite> FindExistingRegistrationInviteByIDTheoryData
        {
            get
            {
                var data = new TheoryData<Guid, RegistrationInvite>
                {
                    { new Guid(PENDING_REGISTRATION_INVITE_ID_1), CloneHelper.Clone(pending1) },
                    { new Guid(APPROVED_REGISTRATION_INVITE_ID_2), CloneHelper.Clone(approved2) },
                    { new Guid(EXPIRED_REGISTRATION_INVITE_ID_3), CloneHelper.Clone(expired3) }
                };
                return data;
            }
        }

        [Fact]
        public async Task FindRegistrationNonExistingInviteByID_ThrowsRegistrationInviteNotFoundException()
        {
            //Arrange
            Guid registrationInviteID = new Guid(NON_EXISTING_REGISTRATION_INVITE_ID_1);
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);
            mockRegistrationInviteRepository.Setup(r => r.FindAsync(registrationInviteID, It.IsAny<bool>())).Returns(Task.FromResult(registrationInvites.Where(i => i.RegistrationInviteID == registrationInviteID).FirstOrDefault()));

            //Act/Asssert
            RegistrationInviteNotFoundException exception = await Assert.ThrowsAsync<RegistrationInviteNotFoundException>(() => registrationInviteService.FindRegistrationInvite(registrationInviteID));
            mockRegistrationInviteRepository.Verify(mock => mock.FindAsync(registrationInviteID, false), Times.Once());
        }

        [Fact]
        public async Task GetPendingInvites_ReturnsExpectedResult()
        {
            //Arrange
            List<RegistrationInvite> expected = CloneHelper.Clone(new List<RegistrationInvite>() { pending1, pending2 });
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            List<RegistrationInvite> result = await registrationInviteService.GetPendingInvites();

            //Assert
            Assert.Equal(expected, result);
            Assert.Empty(result.Where(i => (!(i.Approved == false))));
        }

        [Fact]
        public async Task GetActiveInvites_ReturnsExpectedResult()
        {
            //Arrange
            List<RegistrationInvite> expected = CloneHelper.Clone(new List<RegistrationInvite>() { approved1, approved2, approved3, approved4 });
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            List<RegistrationInvite> result = await registrationInviteService.GetActiveInvites();

            //Assert
            Assert.Equal(expected, result);
            Assert.Empty(result.Where(i => (!(i.Approved && i.Expires >= DateTime.UtcNow))));
        }

        [Fact]
        public async Task GetExpiredInvites_ReturnsExpectedResult()
        {
            //Arrange
            List<RegistrationInvite> expected = CloneHelper.Clone(new List<RegistrationInvite>() { expired1, expired2, expired3 });
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            List<RegistrationInvite> result = await registrationInviteService.GetExpiredInvites();

            //Assert
            Assert.Equal(expected, result);
            Assert.Empty(result.Where(i => (!(i.Approved && i.Expires < DateTime.UtcNow))));
        }

        [Theory]
        [InlineData("pendingregistrationinvite1@test.com", PENDING_REGISTRATION_INVITE_ID_1, false)] //Attempt to validate a pending invite with a correct email
        [InlineData("pendingregistrationinvite1@test.com", PENDING_REGISTRATION_INVITE_ID_2, false)] //Attempt to validate a pending invite with an incorrect email
        [InlineData("approvedregistrationinvite1@test.com", APPROVED_REGISTRATION_INVITE_ID_1, true)] //Attempt to validate an active invite with a correct email
        [InlineData("approvedregistrationinvite4@test.com", APPROVED_REGISTRATION_INVITE_ID_2, false)] //Attempt to validate an active invite with an incorrect email
        [InlineData("expiredregistrationinvite1@test.com", EXPIRED_REGISTRATION_INVITE_ID_1, false)] //Attempt to validate an expired invite with a correct email
        [InlineData("expiredregistrationinvite2@test.com", EXPIRED_REGISTRATION_INVITE_ID_3, false)] //Attempt to validate an expired invite with an incorrect email
        [InlineData("doesntexist@test.com", NON_EXISTING_REGISTRATION_INVITE_ID_1, false)]
        public async Task ValidateRegistrationInvite_ReturnsExpectedResults(string email, string id, bool expected)
        {
            //Arrange
            Guid inviteCode = new Guid(id);
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);
            mockRegistrationInviteRepository.Setup(x => x.FindAsync(inviteCode, It.IsAny<bool>())).Returns(Task.FromResult(registrationInvites.Where(r => r.RegistrationInviteID == inviteCode).FirstOrDefault()));

            //Act
            bool result = await registrationInviteService.ValidateRegistrationInvite(email, inviteCode);

            //Assert
            Assert.Equal(expected, result);
        }

        [Fact]
        public async Task CreateRegistrationInvite_CallsExpectedMethods()
        {
            //Arrange
            RegistrationInvite registration = CloneHelper.Clone(pending1);
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await registrationInviteService.CreateRegistrationInvite(registration);

            //Assert
            mockRegistrationInviteRepository.Verify(mock => mock.Insert(registration), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task CreateRegistrationInviteWithUser_CallsExpectedMethods()
        {
            //Arrange
            RegistrationInvite registration = CloneHelper.Clone(pending1);
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await registrationInviteService.CreateRegistrationInvite(registration);

            //Assert
            mockRegistrationInviteRepository.Verify(mock => mock.Insert(registration), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateRegistrationInviteWithUser_CallsExpectedMethods()
        {
            //Arrange
            RegistrationInvite registration = CloneHelper.Clone(pending1);
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await registrationInviteService.UpdateRegistrationInvite(registration);

            //Assert
            mockRegistrationInviteRepository.Verify(mock => mock.Update(registration), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveRegistrationInvite_CallsExpectedMethods()
        {
            //Arrange
            RegistrationInvite registration = pending1;
            RegistrationInviteService registrationInviteService = new RegistrationInviteService(mockUnitOfWork.Object, mockRegistrationInviteRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await registrationInviteService.RemoveRegistrationInvite(registration);

            //Assert
            mockRegistrationInviteRepository.Verify(mock => mock.Delete(registration), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }
    }
}
