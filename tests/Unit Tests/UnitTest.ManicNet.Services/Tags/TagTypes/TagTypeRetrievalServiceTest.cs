﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags.TagTypes
{
    public sealed class TagTypeRetrievalServiceTest
    {
        private bool bypassCache { get; set; }
        private readonly List<TagType> tagTypes;
        private readonly Mock<ITagTypeRepository> mockTagTypeRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;

        private ClaimsPrincipal User { get; set; }

        private static Guid ARTIST_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SERIES_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CHARACTER_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid TAG_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid SCANALATOR_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");
        private static Guid DELETED_ID_3 = new Guid("7B342989-1FCB-4335-B428-F71E448F4FA1");

        private static readonly TagType artist = new TagType(ARTIST_ID, "Artist", "Artist", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType series = new TagType(SERIES_ID, "Series", "Series", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType character = new TagType(CHARACTER_ID, "Character", "Character", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType tag = new TagType(TAG_ID, "Tag", "Tag", 1, string.Empty, string.Empty, string.Empty, true, true, true, true, false, false, 0, 0, 0, false);
        private static readonly TagType scanalator = new TagType(SCANALATOR_ID, "Scanalator", "Scanalator", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, false);
        private static readonly TagType deleted1 = new TagType(DELETED_ID_1, "Deleted 1", "Deleted1", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);
        private static readonly TagType deleted2 = new TagType(DELETED_ID_2, "Deleted 2", "Deleted2", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);
        private static readonly TagType deleted3 = new TagType(DELETED_ID_3, "Deleted 3", "Deleted3", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);

        public TagTypeRetrievalServiceTest()
        {
            bypassCache = true;

            User = new ClaimsPrincipal();

            tagTypes = CloneHelper.Clone(new List<TagType> {
                artist, series, character, tag, scanalator, deleted1, deleted2, deleted3
            });

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockTagTypeRepository = new Mock<ITagTypeRepository>();
            mockTagTypeRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tagTypes));
            mockTagTypeRepository.Setup(x => x.All()).Returns(tagTypes);

            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();
        }

        [Theory]
        [MemberData(nameof(HasUniqueNameTheoryData))]
        public void HasUniqueName_ReturnsExpectedResult(string name, bool expected, Guid? id)
        {
            //Arrange
            Guid? tagTypeID = null;
            bypassCache = true;

            if (id.HasValue)
            {
                tagTypeID = id.Value;
            }

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            bool result = tagtypeRetrievalService.HasUniqueName(name, tagTypeID);

            //Assert
            Assert.Equal(expected, result);
            mockTagTypeRepository.Verify(mock => mock.All(), Times.Once());
        }

        public static TheoryData<string, bool, Guid?> HasUniqueNameTheoryData
        {
            get
            {
                var data = new TheoryData<string, bool, Guid?> {
                    { "Relationship", true, null }, //Attempt to create a tag type with a new name 
                    { "Artist", false, null }, //Attempt to create a tag type with an existing name
                    { "Artist", true, ARTIST_ID }, //Attempt to rename an existing tag type to its current name
                    { "Relationship", true, ARTIST_ID }, //Attempt to rename an existing tag type to a new name
                    { "Series", false, ARTIST_ID } //Attempt to rename an exsiting tag type to an existing name
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(HasUniqueSearchablePrefixTheoryData))]
        public void HasUniqueSearchablePrefix_ReturnsExpectedResult(string prefix, bool expected, Guid? id)
        {
            //Arrange
            Guid? tagTypeID = null;

            if (id.HasValue)
            {
                tagTypeID = id.Value;
            }

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            bool result = tagtypeRetrievalService.HasUniqueSearchablePrefix(prefix, tagTypeID);

            //Assert
            Assert.Equal(expected, result);
            mockTagTypeRepository.Verify(mock => mock.All(), Times.Once());
        }

        public static TheoryData<string, bool, Guid?> HasUniqueSearchablePrefixTheoryData
        {
            get
            {
                var data = new TheoryData<string, bool, Guid?> {
                    { "Relationship", true, null }, //Attempt to create a tag type with a new name 
                    { "Artist", false, null }, //Attempt to create a tag type with an existing name
                    { "Artist", true, ARTIST_ID }, //Attempt to rename an existing tag type to its current searchable prefix
                    { "Relationship", true, ARTIST_ID }, //Attempt to rename an existing tag type to a new searchable prefix
                    { "Series", false, ARTIST_ID }, //Attempt to rename an exsiting tag type to an existing searchable prefix
                };

                return data;
            }
        }

        [Fact]
        public async Task GetActiveTagTypes_ReturnsExpectedResult()
        {
            //Arrange
            List<TagType> expected = CloneHelper.Clone(new List<TagType>() { artist, series, character, tag, scanalator });

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<TagType> result = await tagtypeRetrievalService.GetActiveTagTypes();

            //Assert
            Assert.Equal(expected, result);
            Assert.Empty(result.Where(x => x.IsDeleted == true));
            mockTagTypeRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        [Fact]
        public async Task GetDeletedTagTypes_ReturnsExpectedResult()
        {
            //Arrange
            List<TagType> expected = CloneHelper.Clone(new List<TagType>() { deleted1, deleted2, deleted3 });

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<TagType> result = await tagtypeRetrievalService.GetDeletedTagTypes();

            //Assert
            Assert.Equal(expected, result);
            Assert.Empty(result.Where(x => x.IsDeleted == false));
            mockTagTypeRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleTagTypeTheoryData))]
        public async Task FindExistingAccessibleTagType_ReturnsTagType(Guid id, TagType expected)
        {
            //Arrange
            mockTagTypeRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(tagTypes.Where(x => x.TagTypeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            TagType result = await tagtypeRetrievalService.FindTagTypeAsync(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockTagTypeRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid, TagType> FindExistingAccessibleTagTypeTheoryData
        {
            get
            {
                TheoryData<Guid, TagType> data = new TheoryData<Guid, TagType>
                {
                    { ARTIST_ID, CloneHelper.Clone(artist) },
                    { DELETED_ID_1, CloneHelper.Clone(deleted1) }

                };
                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingTagType_ThrowsTagTypeNotFoundException()
        {
            //Arrange
            Guid id = Guid.NewGuid();

            mockTagTypeRepository.Setup(x => x.FindAsync(It.IsAny<Guid>(), It.IsAny<bool>())).Returns(Task.FromResult<TagType>(null));

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagTypeNotFoundException exception = await Assert.ThrowsAsync<TagTypeNotFoundException>(() => tagtypeRetrievalService.FindTagTypeAsync(id, User, bypassCache));
            mockTagTypeRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindDeletedTagTypeWithoutPermissionTheoryData))]
        public async Task FindDeletedTagTypeWithoutPermission_ThrowsTagTypeNotViewableException(Guid id)
        {
            //Arrange
            Guid identifier = id;

            mockTagTypeRepository.Setup(x => x.FindAsync(identifier, It.IsAny<bool>())).Returns(Task.FromResult(tagTypes.Where(x => x.TagTypeID == identifier).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            TagTypeRetrievalService tagtypeRetrievalService = new TagTypeRetrievalService(mockTagTypeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagTypeNotViewableException exception = await Assert.ThrowsAsync<TagTypeNotViewableException>(() => tagtypeRetrievalService.FindTagTypeAsync(identifier, User, bypassCache));
            mockTagTypeRepository.Verify(mock => mock.FindAsync(identifier, bypassCache), Times.Once());
        }

        public static TheoryData<Guid> FindDeletedTagTypeWithoutPermissionTheoryData
        {
            get
            {
                var data = new TheoryData<Guid> {
                    DELETED_ID_1
                };

                return data;
            }
        }
    }
}
