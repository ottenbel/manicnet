﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags.TagTypes
{
    public sealed class TagTypeUpateServiceTest
    {
        private readonly List<TagType> tagTypes;
        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<ITagTypeRepository> mockTagTypeRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;
        private readonly Mock<ITagUpdateService> mockTagUpdateService;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;

        private ClaimsPrincipal User { get; set; }

        private static Guid ARTIST_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SERIES_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CHARACTER_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid TAG_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid SCANALATOR_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");
        private static Guid DELETED_ID_3 = new Guid("7B342989-1FCB-4335-B428-F71E448F4FA1");
        private static Guid DELETED_ID_WITH_CHILDREN = new Guid("066C7E1F-6FCD-4066-9CF5-F36E0B55F019");

        private static Guid CHILD_ID_1 = new Guid("B62779D8-8ACC-4E40-B3ED-8EF5F8136233");
        private static Guid CHILD_ID_2 = new Guid("61EC9D0F-3054-4C64-B81C-641B1EA068E1");

        private static readonly Tag child1 = new Tag(CHILD_ID_1, DELETED_ID_WITH_CHILDREN, "Child1", string.Empty, string.Empty, string.Empty, false, true);
        private static readonly Tag child2 = new Tag(CHILD_ID_2, DELETED_ID_WITH_CHILDREN, "Child2", string.Empty, string.Empty, string.Empty, false, false);

        private static readonly TagType artist = new TagType(ARTIST_ID, "Artist", "Artist", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType series = new TagType(SERIES_ID, "Series", "Series", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType character = new TagType(CHARACTER_ID, "Character", "Character", 1, string.Empty, string.Empty, string.Empty, true, false, false, true, false, false, 0, 0, 0, false);
        private static readonly TagType tag = new TagType(TAG_ID, "Tag", "Tag", 1, string.Empty, string.Empty, string.Empty, true, true, true, true, false, false, 0, 0, 0, false);
        private static readonly TagType scanalator = new TagType(SCANALATOR_ID, "Scanalator", "Scanalator", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, false);
        private static readonly TagType deleted1 = new TagType(DELETED_ID_1, "Deleted 1", "Deleted1", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);
        private static readonly TagType deleted2 = new TagType(DELETED_ID_2, "Deleted 2", "Deleted2", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);
        private static readonly TagType deleted3 = new TagType(DELETED_ID_3, "Deleted 3", "Deleted3", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true);
        private static readonly TagType deletedWithChildren = new TagType(DELETED_ID_WITH_CHILDREN, "Deleted With Children", "Deleted With Children", 1, string.Empty, string.Empty, string.Empty, false, false, true, false, false, true, 0, 0, 0, true, new List<Tag>() { child1, child2 });

        public TagTypeUpateServiceTest()
        {
            User = new ClaimsPrincipal();

            tagTypes = CloneHelper.Clone(new List<TagType> {
                artist, series, character, tag, scanalator, deleted1, deleted2, deleted3
            });

            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockTagTypeRepository = new Mock<ITagTypeRepository>();
            mockTagTypeRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tagTypes));
            mockTagTypeRepository.Setup(x => x.All()).Returns(tagTypes);

            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();

            mockTagUpdateService = new Mock<ITagUpdateService>();

            mockCacheCleanerService = new Mock<ICacheCleanerService>();
        }

        [Fact]
        public async Task RemoveTagTypeInNonDeletableState_ThrowsTagTypeNotDeletableException()
        {
            //Arrange
            Guid id = ARTIST_ID;
            TagType tagType = CloneHelper.Clone(tagTypes.Where(x => x.TagTypeID == id).FirstOrDefault());

            TagTypeUpdateService tagTypeUpdateService = new TagTypeUpdateService(mockUnitOfWork.Object, mockTagTypeRepository.Object, mockCacheCleanerService.Object, mockLogger.Object, mockTagUpdateService.Object);

            //Act/Assert
            TagTypeNotDeletableException exception = await Assert.ThrowsAsync<TagTypeNotDeletableException>(() => tagTypeUpdateService.RemoveTagType(tagType, tagType.Tags));
            mockTagTypeRepository.Verify(mock => mock.Delete(tagType), Times.Never());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveTagType_CallsExpectedMethods()
        {
            //Arrange
            mockTagUpdateService.Setup(x => x.RemoveTag(It.IsAny<Tag>(), false)).Returns(Task.FromResult(new List<Guid>()));
            TagTypeUpdateService tagTypeUpdateService = new TagTypeUpdateService(mockUnitOfWork.Object, mockTagTypeRepository.Object, mockCacheCleanerService.Object, mockLogger.Object, mockTagUpdateService.Object);
            TagType tagType = CloneHelper.Clone(deleted1);

            //Act
            await tagTypeUpdateService.RemoveTagType(tagType, tagType.Tags);

            //Assert
            mockTagTypeRepository.Verify(mock => mock.Delete(tagType), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveTagTypeWithChildren_CallsExpectedMethods()
        {
            //Arrange
            mockTagUpdateService.Setup(x => x.RemoveTag(It.IsAny<Tag>(), false)).Returns(Task.FromResult(new List<Guid>()));
            TagTypeUpdateService tagTypeUpdateService = new TagTypeUpdateService(mockUnitOfWork.Object, mockTagTypeRepository.Object, mockCacheCleanerService.Object, mockLogger.Object, mockTagUpdateService.Object);
            TagType tagType = CloneHelper.Clone(deletedWithChildren);

            //Act
            await tagTypeUpdateService.RemoveTagType(tagType, tagType.Tags);

            //Assert
            foreach (Tag tag in tagType.Tags)
            {
                mockTagUpdateService.Verify(mock => mock.RemoveTag(tag, false), Times.Once());
            }

            mockTagTypeRepository.Verify(mock => mock.Delete(tagType), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task CreateTagType_CallsExpectedMethods()
        {
            //Arrange
            TagTypeUpdateService tagTypeUpdateService = new TagTypeUpdateService(mockUnitOfWork.Object, mockTagTypeRepository.Object, mockCacheCleanerService.Object, mockLogger.Object, mockTagUpdateService.Object);
            TagType tagType = CloneHelper.Clone(artist);

            //Act
            await tagTypeUpdateService.CreateTagType(tag);

            //Assert
            mockTagTypeRepository.Verify(mock => mock.Insert(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateTagType_CallsExpectedMethods()
        {
            //Arrange
            TagTypeUpdateService tagTypeUpdateService = new TagTypeUpdateService(mockUnitOfWork.Object, mockTagTypeRepository.Object, mockCacheCleanerService.Object, mockLogger.Object, mockTagUpdateService.Object);
            TagType tagType = CloneHelper.Clone(artist);

            //Act
            await tagTypeUpdateService.UpdateTagType(tagType, tagType.Tags);

            //Assert
            mockTagTypeRepository.Verify(mock => mock.Update(tagType), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }
    }
}
