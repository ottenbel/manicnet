﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags
{
    public sealed class TagAliasServiceTest
    {
        private readonly List<TagAlias> tagAliases;

        private readonly Mock<ITagAliasRepository> mockTagAliasRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;

        //Alias IDs
        private static Guid ACTION_ADVENTURE_ID = new Guid("333061E2-C86A-434E-B300-A781788A5520");
        private static Guid HARD_DRIVE_DYSTOPIA_ID = new Guid("9ADA7818-A8FA-4DBB-A3A9-E806407C2D64");
        private static Guid HALLMARK_ID = new Guid("D8A08F27-8263-4030-8EF6-37F8988E8B59");

        private static Guid REMOVED_1_ID = new Guid("2E45279E-D7AD-4579-A3B5-780D356A930E");
        private static Guid REMOVED_A_ID = new Guid("BF7E80C8-8A23-4681-A5B1-924CCEB71827");
        private static Guid REMOVED_2_ID = new Guid("C6F7456C-2EE5-4E7A-87AF-84E0B88D80A3");

        //Tag IDs
        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");

        private static Guid TAG_TYPE_ID = new Guid("B529B4FF-97F3-4CBA-A5C9-6F91D15ACCC3");

        private static readonly Tag action = new Tag(ACTION_ID, TAG_TYPE_ID, "Action", "Short Action Description", "Long Action Description", string.Empty, false, false);
        private static readonly Tag cyberpunk = new Tag(CYBERPUNK_ID, TAG_TYPE_ID, "Cyberpunk", "Short Cyberpunk Description", "Long Cyberpunk Description", string.Empty, false, false);
        private static readonly Tag romance = new Tag(ROMANCE_ID, TAG_TYPE_ID, "Romance", "Short Romance Description", "Long Romance Description", string.Empty, false, false);
        private static readonly Tag deleted1 = new Tag(DELETED_ID_1, TAG_TYPE_ID, "Deleted 1", "Short Deleted 1 Description", "Long Deleted 1 Description", string.Empty, false, true);
        private static readonly Tag deleted2 = new Tag(DELETED_ID_2, TAG_TYPE_ID, "Deleted 2", "Short Deleted 2 Description", "Long Deleted 2 Description", string.Empty, false, true);

        private static readonly TagAlias actionAdventure = new TagAlias(ACTION_ADVENTURE_ID, ACTION_ID,
            "Action Adventure", action);
        private static readonly TagAlias thriller = new TagAlias(new Guid(), new Guid(),
            "Thriller", action);
        private static readonly TagAlias hardDriveDystopia = new TagAlias(HARD_DRIVE_DYSTOPIA_ID, CYBERPUNK_ID,
            "Hard Drive Dystopia", cyberpunk);
        private static readonly TagAlias hallmark = new TagAlias(HALLMARK_ID, ROMANCE_ID,
            "Hallmark", romance);
        private static readonly TagAlias removed1 = new TagAlias(REMOVED_1_ID, DELETED_ID_1,
            "Removed 1", deleted1);
        private static readonly TagAlias removedA = new TagAlias(REMOVED_A_ID, DELETED_ID_1,
            "Removed A", deleted1);
        private static readonly TagAlias removed2 = new TagAlias(REMOVED_2_ID, DELETED_ID_2,
            "Removed 2", deleted2);

        public TagAliasServiceTest()
        {
            tagAliases = CloneHelper.Clone(new List<TagAlias>() { actionAdventure, thriller, hardDriveDystopia,
                hallmark, removed1, removedA, removed2 });

            mockTagAliasRepository = new Mock<ITagAliasRepository>();
            mockTagAliasRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tagAliases));

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
        }

        [Theory]
        [MemberData(nameof(GetAliasListTheoryData))]
        public async Task GetAliasList_ReturnsExpected_Result(List<string> input, List<TagAlias> expectedResult)
        {
            //Arrange
            mockTagAliasRepository.Setup(x => x.FindAsync(It.IsAny<List<string>>())).Returns(Task.FromResult<List<TagAlias>>(tagAliases.Where(t => input.Contains(t.Name)).ToList()));

            TagAliasService tagAliasService = new TagAliasService(mockTagAliasRepository.Object, mockLogger.Object);

            //Act
            List<TagAlias> result = await tagAliasService.GetAliasList(input);

            //Assert
            Assert.Equal(expectedResult, result);
        }

        public static TheoryData<List<string>, List<TagAlias>> GetAliasListTheoryData
        {
            get
            {
                var data = new TheoryData<List<string>, List<TagAlias>>
                {
                    { new List<string>(), CloneHelper.Clone(new List<TagAlias>()) },

                    {
                        new List<string>() { "Action Adventure", "Thriller", "Hard Drive Dystopia", "Hallmark" },
                        CloneHelper.Clone(new List<TagAlias>() { actionAdventure, thriller, hardDriveDystopia, hallmark })
                    },
                    {
                        new List<string>() { "Removed 1", "Removed A", "Removed 2" },
                        CloneHelper.Clone(new List<TagAlias>() { removed1, removedA, removed2 })
                    },
                    {
                        new List<string>() { "Techno Noir", "Treasure Hunt" },
                        CloneHelper.Clone(new List<TagAlias>() { new TagAlias("Techno Noir"), new TagAlias("Treasure Hunt") })
                    },
                    {
                        new List<string>() { "Action Adventure", "Thriller", "Hard Drive Dystopia", "Hallmark", "Removed 1", "Removed A", "Removed 2", "Techno Noir", "Treasure Hunt" },
                        CloneHelper.Clone(new List<TagAlias>() { actionAdventure, thriller, hardDriveDystopia, hallmark, removed1, removedA, removed2, new TagAlias("Techno Noir"), new TagAlias("Treasure Hunt") })
                    }
                };

                return data;
            }
        }

        [Fact]
        public async Task GetActiveAvailiableTags_ReturnsExpectedResult()
        {
            //Arrange
            List<TagAlias> expectedResultsReturned = CloneHelper.Clone(new List<TagAlias>() { actionAdventure, thriller, hardDriveDystopia, hallmark });
            TagAliasService tagAliasService = new TagAliasService(mockTagAliasRepository.Object, mockLogger.Object);

            //Act
            List<TagAlias> result = await tagAliasService.GetActiveTagAliases();

            //Assert
            Assert.Equal(expectedResultsReturned, result);
            Assert.Empty(result.Where(x => x.Tag.IsDeleted == true));
        }
    }
}
