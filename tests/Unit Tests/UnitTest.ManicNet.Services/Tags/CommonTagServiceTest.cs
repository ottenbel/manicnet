﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags
{
    public sealed class CommonTagServiceTest
    {
        private readonly List<Tag> tags;
        private readonly List<TagAlias> aliases;

        private readonly Mock<ITagRepository> mockTagRepository;
        private readonly Mock<ITagAliasRepository> mockTagAliasRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;

        private static Guid TAG_TYPE_ID = new Guid("B529B4FF-97F3-4CBA-A5C9-6F91D15ACCC3");

        //Tag identifiers
        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SCIENCE_FICTION_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid SWORDS_AND_SANDALS_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");
        private static Guid DELETED_ID_3 = new Guid("7B342989-1FCB-4335-B428-F71E448F4FA1");
        private static Guid IMPLIES_NOTHING = new Guid("1BDDD575-D114-476D-9C45-80CCF779C95C");
        private static Guid TAG_A_ID = new Guid("720514C5-35BE-410C-BEDC-145F426C0501");
        private static Guid TAG_A1_ID = new Guid("134BFF47-B45E-4786-ACF6-A9A5EF33A258");
        private static Guid TAG_A2_ID = new Guid("E6F8272E-F25E-4A16-B215-63768C81D0D4");
        private static Guid TAG_A12_ID = new Guid("8FBC173E-DF41-4DC0-B06C-EE96839D5288");
        private static Guid TAG_A21_ID = new Guid("0AD50DC9-AAC0-4263-ADF4-DAD8DE650281");
        private static Guid TAG_A22_ID = new Guid("9D90E94C-4737-4048-B00F-314F2295209A");
        private static Guid TAG_A223_ID = new Guid("02D58B9F-B0CF-484D-8707-121F7CBD68FC");

        //Alias identifiers
        private static Guid ACTION_ADVENTURE_ID = new Guid("333061E2-C86A-434E-B300-A781788A5520");
        private static Guid THRILLER_ID = new Guid("9128E028-558D-43E1-952A-CD145D42A201");
        private static Guid HARD_DRIVE_DYSTOPIA_ID = new Guid("9ADA7818-A8FA-4DBB-A3A9-E806407C2D64");

        private static readonly Tag action = new Tag(ACTION_ID, TAG_TYPE_ID, "Action", "Short Action Description", "Long Action Description", string.Empty, false, false);
        private static readonly Tag scienceFiction = new Tag(SCIENCE_FICTION_ID, TAG_TYPE_ID, "Science Fiction", "Short Science Fiction Description", "Long Science Fiction Description", string.Empty, false, false);
        private static readonly Tag cyberpunk = new Tag(CYBERPUNK_ID, TAG_TYPE_ID, "Cyberpunk", "Short Cyberpunk Description", "Long Cyberpunk Description", string.Empty, false, false);
        private static readonly Tag swordsAndSandals = new Tag(SWORDS_AND_SANDALS_ID, TAG_TYPE_ID, "Swords and Sandals", "Short Swords and Sandals Description", "Long Swords and Sandals Description", string.Empty, false, false);
        private static readonly Tag romance = new Tag(ROMANCE_ID, TAG_TYPE_ID, "Romance", "Short Romance Description", "Long Romance Description", string.Empty, false, false);
        private static readonly Tag deleted1 = new Tag(DELETED_ID_1, TAG_TYPE_ID, "Deleted 1", "Short Deleted 1 Description", "Long Deleted 1 Description", string.Empty, false, true);
        private static readonly Tag deleted2 = new Tag(DELETED_ID_2, TAG_TYPE_ID, "Deleted 2", "Short Deleted 2 Description", "Long Deleted 2 Description", string.Empty, false, true);
        private static readonly Tag deleted3 = new Tag(DELETED_ID_3, TAG_TYPE_ID, "Deleted 3", "Short Deleted 3 Description", "Long Deleted 3 Description", string.Empty, false, true);
        private static readonly Tag impliesNothing = new Tag(IMPLIES_NOTHING, TAG_TYPE_ID, "Implies Nothing", "Short Description", "Long Description", string.Empty, false, false);
        private static readonly Tag tagA = new Tag(TAG_A_ID, TAG_TYPE_ID, "Tag A", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>(), new List<Guid>() { TAG_A1_ID, TAG_A2_ID }, new List<TagAlias>());
        private static readonly Tag tagA1 = new Tag(TAG_A1_ID, TAG_TYPE_ID, "Tag A1", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A_ID }, new List<Guid>() { TAG_A12_ID, TAG_A21_ID }, new List<TagAlias>());
        private static readonly Tag tagA2 = new Tag(TAG_A2_ID, TAG_TYPE_ID, "Tag A2", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A_ID }, new List<Guid>() { TAG_A12_ID, TAG_A21_ID, TAG_A22_ID }, new List<TagAlias>());
        private static readonly Tag tagA12 = new Tag(TAG_A12_ID, TAG_TYPE_ID, "Tag A12", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A1_ID, TAG_A2_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA21 = new Tag(TAG_A21_ID, TAG_TYPE_ID, "Tag A21", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A1_ID, TAG_A2_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA22 = new Tag(TAG_A22_ID, TAG_TYPE_ID, "Tag A22", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A2_ID }, new List<Guid>() { TAG_A223_ID }, new List<TagAlias>());
        private static readonly Tag tagA223 = new Tag(TAG_A223_ID, TAG_TYPE_ID, "Tag A223", "Short Description", "Long Description", string.Empty, false, false,
            new List<Guid>() { TAG_A22_ID }, new List<Guid>(), new List<TagAlias>());

        private static readonly TagAlias actionAdventure = new TagAlias(ACTION_ADVENTURE_ID, ACTION_ID, "Action-Adventure");
        private static readonly TagAlias thriller = new TagAlias(THRILLER_ID, ACTION_ID, "Thriller");
        private static readonly TagAlias hardDriveDystopia = new TagAlias(HARD_DRIVE_DYSTOPIA_ID, CYBERPUNK_ID, "Hard Drive Dystopia");
        private static readonly TagAlias removed1 = new TagAlias(HARD_DRIVE_DYSTOPIA_ID, DELETED_ID_1, "Removed 1");

        public CommonTagServiceTest()
        {
            tags = CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, swordsAndSandals, romance, deleted1, deleted2, deleted3, impliesNothing, tagA, tagA1, tagA2, tagA12, tagA21, tagA22, tagA223 });
            aliases = CloneHelper.Clone(new List<TagAlias>() { actionAdventure, thriller, hardDriveDystopia, removed1 });

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tags));
            mockTagRepository.Setup(x => x.All()).Returns(tags);


            mockTagAliasRepository = new Mock<ITagAliasRepository>();
            mockTagAliasRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(aliases));
            mockTagAliasRepository.Setup(x => x.All()).Returns(aliases);
        }


        [Theory]
        [MemberData(nameof(HasUniqueNameTheoryData))]
        public void HasUniqueName_ReturnsExpectedResult(string name, bool expected, Guid? id)
        {
            //Arrange
            Guid? tagID = null;

            if (id.HasValue)
            {
                tagID = id.Value;
            }

            CommonTagService commonTagService = new CommonTagService(mockTagRepository.Object, mockTagAliasRepository.Object, mockLogger.Object);

            //Act
            bool result = commonTagService.HasUniqueName(name, tagID);

            //Assert
            Assert.Equal(expected, result);
        }

        public static TheoryData<string, bool, Guid?> HasUniqueNameTheoryData
        {
            get
            {
                var data = new TheoryData<string, bool, Guid?>
                {
                    { "Urban Fantasy", true, null }, //Attempt to create a tag with a new name 
                    { "Romance", false, null }, //Attempt to create a tag with an existing name
                    { "Deleted 1", false, null }, //Attempt to create a tag with an existing (deleted) name
                    { "Thriller", false, null }, //Attempt to create a tag with the name of an existing alias
                    { "Removed 1", false, null }, //Attempt to create a tag with the name of an existing (deleted) alias
                    { "Science Fiction", true, SCIENCE_FICTION_ID }, //Attempt to rename an existing tag to its current name 
                    { "Steampunk", true, CYBERPUNK_ID }, //Attempt to rename an existing tag to a new name
                    { "Swords and Sandals", false, ROMANCE_ID }, //Attempt to rename an exsiting tag to an existing name
                    { "Deleted 1", false, ACTION_ID } //Attempt to rename a tag with an existing (deleted) name//Attempt to rename a tag with an existing (deleted) name
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(HasUniqueAliasTheoryData))]
        public void HasUniqueAlias_ReturnsExpectedResult(string name, bool expected, Guid id)
        {
            //Arrange
            Guid tagID = id;
            CommonTagService commonTagService = new CommonTagService(mockTagRepository.Object, mockTagAliasRepository.Object, mockLogger.Object);

            //Act
            bool result = commonTagService.HasUniqueAlias(name, tagID);

            //Assert
            Assert.Equal(expected, result);
        }

        public static TheoryData<string, bool, Guid?> HasUniqueAliasTheoryData
        {
            get
            {
                var data = new TheoryData<string, bool, Guid?>()
                {
                    { "Love Story", true, ROMANCE_ID }, //Attempt to create a new alias
                    { "Action-Adventure", false, CYBERPUNK_ID }, //Attempt to create an already existing alias (that exists on another tag)
                    { "Thriller", true, ACTION_ID }, //Attempt to create an already existing alias (that exists on the same tag)
                };

                return data;
            }
        }
    }
}
