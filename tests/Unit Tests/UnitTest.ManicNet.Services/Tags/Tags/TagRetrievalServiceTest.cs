﻿using ManicNet.Constants;
using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags.Tags
{
    public sealed class TagRetrievalServiceTest
    {
        private bool bypassCache { get; set; }
        private readonly List<Tag> tags;

        private readonly Mock<ITagRepository> mockTagRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;

        private ClaimsPrincipal User { get; set; }

        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SCIENCE_FICTION_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid SWORDS_AND_SANDALS_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");
        private static Guid DELETED_ID_3 = new Guid("7B342989-1FCB-4335-B428-F71E448F4FA1");

        private static Guid IMPLIES_NOTHING = new Guid("1BDDD575-D114-476D-9C45-80CCF779C95C");
        private static Guid VIRTUAL_FORMAT = new Guid("210E29FE-986E-45F6-BDC6-4E5537B8AAF4");
        private static Guid TAG_A = new Guid("720514C5-35BE-410C-BEDC-145F426C0501");
        private static Guid TAG_A1 = new Guid("134BFF47-B45E-4786-ACF6-A9A5EF33A258");
        private static Guid TAG_A2 = new Guid("E6F8272E-F25E-4A16-B215-63768C81D0D4");
        private static Guid TAG_A12 = new Guid("8FBC173E-DF41-4DC0-B06C-EE96839D5288");
        private static Guid TAG_A21 = new Guid("0AD50DC9-AAC0-4263-ADF4-DAD8DE650281");
        private static Guid TAG_A22 = new Guid("9D90E94C-4737-4048-B00F-314F2295209A");
        private static Guid TAG_A223 = new Guid("02D58B9F-B0CF-484D-8707-121F7CBD68FC");
        private static Guid DELETED_WITH_IMPLIED_BY_RELATIONSHIPS = new Guid("25440259-578E-4FF5-A8F8-15B8773D0BAA");
        private static Guid DELETED_WITH_IMPLIES_RELATIONSHIPS = new Guid("3AC3D9FD-E514-439B-A1FF-F715BF91F121");
        private static Guid ACTIVE_WITH_IMPLIED_BY_RELATIONSHIPS = new Guid("1690CA31-1FC8-4257-9957-59D3C0C02754");
        private static Guid ACTIVE_WITH_IMPLIES_RELATIONSHIPS = new Guid("551CD5A1-1346-4E82-AFEA-92895580916A");

        private static Guid TAG_UNDER_DELETED_1 = new Guid("88E06373-B829-455A-9AE0-D161279FF43B");
        private static Guid TAG_UNDER_DELETED_2 = new Guid("7586E388-2999-4A42-AB00-C694642F459F");


        //Alias IDs
        private static Guid ACTION_ADVENTURE_ID = new Guid("333061E2-C86A-434E-B300-A781788A5520");
        private static Guid HARD_DRIVE_DYSTOPIA_ID = new Guid("9ADA7818-A8FA-4DBB-A3A9-E806407C2D64");
        private static Guid HALLMARK_ID = new Guid("D8A08F27-8263-4030-8EF6-37F8988E8B59");

        private static Guid REMOVED_1_ID = new Guid("2E45279E-D7AD-4579-A3B5-780D356A930E");
        private static Guid REMOVED_A_ID = new Guid("BF7E80C8-8A23-4681-A5B1-924CCEB71827");
        private static Guid REMOVED_2_ID = new Guid("C6F7456C-2EE5-4E7A-87AF-84E0B88D80A3");

        private static Guid TAG_TYPE_ID = new Guid("B529B4FF-97F3-4CBA-A5C9-6F91D15ACCC3");
        private static Guid ARTIST_TYPE_ID = new Guid("4E72F519-5527-42CD-A805-6E64782AC4D6");
        private static Guid TRANSLATOR_TYPE_ID = new Guid("22528AEE-AC97-403B-87CA-D7225A697CA7");
        private static Guid FORMAT_TYPE_ID = new Guid("A184AF36-B721-420E-A8E3-3B299967C267");
        private static Guid SERIES_TYPE_ID = new Guid("0D33FC5E-F050-474E-9EE5-4FDB711D1251");
        private static Guid DELETED_TYPE_ID = new Guid("E783FDDF-DA7A-4A33-A8B9-50A677EBD138");

        private static readonly TagAlias actionAdventure = new TagAlias(ACTION_ADVENTURE_ID, ACTION_ID,
            "Action Adventure");
        private static readonly TagAlias thriller = new TagAlias(new Guid(), new Guid(),
            "Thriller");
        private static readonly TagAlias hardDriveDystopia = new TagAlias(HARD_DRIVE_DYSTOPIA_ID, CYBERPUNK_ID,
            "Hard Drive Dystopia");
        private static readonly TagAlias hallmark = new TagAlias(HALLMARK_ID, ROMANCE_ID,
            "Hallmark");
        private static readonly TagAlias removed1 = new TagAlias(REMOVED_1_ID, DELETED_ID_1,
            "Removed 1");
        private static readonly TagAlias removedA = new TagAlias(REMOVED_A_ID, DELETED_ID_1,
            "Removed A");
        private static readonly TagAlias removed2 = new TagAlias(REMOVED_2_ID, DELETED_ID_2,
            "Removed 2");

        private static readonly TagType tagTagType = new TagType(TAG_TYPE_ID, "Tag", "tag", 0, string.Empty, string.Empty, string.Empty, true, true, true, false, false, false, 0, 0, 0, false);
        private static readonly TagType artistTagType = new TagType(ARTIST_TYPE_ID, "Artist", "artist", 1, string.Empty, string.Empty, string.Empty, true, false, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType seriesTagType = new TagType(SERIES_TYPE_ID, "Series", "series", 2, string.Empty, string.Empty, string.Empty, true, false, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType formatTagType = new TagType(FORMAT_TYPE_ID, "Format", "Format", 3, string.Empty, string.Empty, string.Empty, false, true, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType translatorTagType = new TagType(TRANSLATOR_TYPE_ID, "Translator", "translator", 4, string.Empty, string.Empty, string.Empty, false, false, true, false, false, false, 0, 0, 0, false);
        private static readonly TagType deleted = new TagType(DELETED_TYPE_ID, "Deleted", "deleted", 5, string.Empty, string.Empty, string.Empty, true, true, true, true, true, true, 0, 0, 0, true);

        private static readonly Tag action = new Tag(ACTION_ID, tagTagType, "Action", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { actionAdventure, thriller });
        private static readonly Tag scienceFiction = new Tag(SCIENCE_FICTION_ID, tagTagType, "Science Fiction", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag cyberpunk = new Tag(CYBERPUNK_ID, tagTagType, "Cyberpunk", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { hardDriveDystopia });
        private static readonly Tag swordsAndSandals = new Tag(SWORDS_AND_SANDALS_ID, tagTagType, "Swords and Sandals", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag romance = new Tag(ROMANCE_ID, tagTagType, "Romance", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { hallmark });
        private static readonly Tag deleted1 = new Tag(DELETED_ID_1, tagTagType, "Deleted 1", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { removed1, removedA });
        private static readonly Tag deleted2 = new Tag(DELETED_ID_2, tagTagType, "Deleted 2", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { removed2 });
        private static readonly Tag deleted3 = new Tag(DELETED_ID_3, tagTagType, "Deleted 3", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag impliesNothing = new Tag(IMPLIES_NOTHING, formatTagType, "Implies Nothing", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag virtualFormat = new Tag(VIRTUAL_FORMAT, formatTagType, "Virtual Format", true, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA = new Tag(TAG_A, seriesTagType, "Tag A", false, false,
            new List<Guid>(), new List<Guid>() { TAG_A1, TAG_A2 }, new List<TagAlias>());
        private static readonly Tag tagA1 = new Tag(TAG_A1, artistTagType, "Tag A1", false, false,
            new List<Guid>() { TAG_A }, new List<Guid>() { TAG_A12, TAG_A21 }, new List<TagAlias>());
        private static readonly Tag tagA2 = new Tag(TAG_A2, translatorTagType, "Tag A2", false, false,
            new List<Guid>() { TAG_A }, new List<Guid>() { TAG_A12, TAG_A21, TAG_A22 }, new List<TagAlias>());
        private static readonly Tag tagA12 = new Tag(TAG_A12, artistTagType, "Tag A12", false, false,
            new List<Guid>() { TAG_A1, TAG_A2 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA21 = new Tag(TAG_A21, artistTagType, "Tag A21", true, false,
            new List<Guid>() { TAG_A1, TAG_A2 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA22 = new Tag(TAG_A22, translatorTagType, "Tag A22", false, false,
            new List<Guid>() { TAG_A2 }, new List<Guid>() { TAG_A223 }, new List<TagAlias>());
        private static readonly Tag tagA223 = new Tag(TAG_A223, translatorTagType, "Tag A223", true, false,
            new List<Guid>() { TAG_A22 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag deletedWithImpliedByRelationships = new Tag(DELETED_WITH_IMPLIED_BY_RELATIONSHIPS, seriesTagType, "Deleted With Implied By Relationships", false, true,
            new List<Guid>() { CYBERPUNK_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag deletedWithImpliesRelationships = new Tag(DELETED_WITH_IMPLIES_RELATIONSHIPS, seriesTagType, "Deleted With Implies Relationships", false, true,
            new List<Guid>(), new List<Guid>() { ACTION_ID, SCIENCE_FICTION_ID }, new List<TagAlias>());
        private static readonly Tag activeWithImpliedByRelationships = new Tag(ACTIVE_WITH_IMPLIED_BY_RELATIONSHIPS, seriesTagType, "Active With Implied By Relationships", false, false,
            new List<Guid>() { CYBERPUNK_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag activeWithImpliesRelationships = new Tag(ACTIVE_WITH_IMPLIES_RELATIONSHIPS, seriesTagType, "Active With Implies Relationships", false, false,
            new List<Guid>(), new List<Guid>() { ACTION_ID, SCIENCE_FICTION_ID }, new List<TagAlias>());

        private static readonly Tag tagUnderDeleted1 = new Tag(TAG_UNDER_DELETED_1, deleted, "Tag Under Deleted", false, false, new List<Guid>(),
            new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagUnderDeleted2 = new Tag(TAG_UNDER_DELETED_1, deleted, "Tag Under Deleted", false, false, new List<Guid>(),
            new List<Guid>(), new List<TagAlias>());

        public TagRetrievalServiceTest()
        {
            bypassCache = true;

            User = new ClaimsPrincipal();

            tags = CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, swordsAndSandals, romance, deleted1, deleted2, deleted3, impliesNothing, virtualFormat, tagA, tagA1, tagA2, tagA12, tagA21, tagA22, tagA223, deletedWithImpliedByRelationships, deletedWithImpliesRelationships, activeWithImpliedByRelationships, activeWithImpliesRelationships, tagUnderDeleted1, tagUnderDeleted2 });

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tags));

            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();
        }

        [Theory]
        [MemberData(nameof(GetActiveTagSearchListsFromSearchStringAsyncTheoryData))]
        public async Task GetTagListsFromSearchString_ReturnsExpectedResult(string searchString, List<Guid> included, List<Guid> excluded)
        {
            //Arrange
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            TagSearchLists result = await tagRetrievalService.GetActiveTagSearchListsFromSearchString(searchString);

            //Assert
            Assert.Equal(included.Count, result.Included.Count);
            Assert.Equal(excluded.Count, result.Excluded.Count);
            Assert.Equal(included.OrderBy(i => i).ToList(), result.Included.Select(t => t.TagID).OrderBy(t => t).ToList());
            Assert.Equal(excluded.OrderBy(i => i).ToList(), result.Excluded.Select(t => t.TagID).OrderBy(t => t).ToList());
        }

        public static TheoryData<string, List<Guid>, List<Guid>> GetActiveTagSearchListsFromSearchStringAsyncTheoryData
        {
            get
            {
                var data = new TheoryData<string, List<Guid>, List<Guid>>
                {
                    { string.Empty,  new List<Guid>(), new List<Guid>() }, //Empty string returns empty lists
                    { null, new List<Guid>(), new List<Guid>() }, //Null object returns empty lists
                    { ", ,  ,   ,    ,    ,-, -,  -,  -  ,", new List<Guid>(), new List<Guid>() }, //Whitespace and dash string returns empty lists
                    { "Finance", new List<Guid>(), new List<Guid>() }, //Tag that doesn't exist returns empty lists
                    { "Tag:Finance", new List<Guid>(), new List<Guid>() }, //Tag that doesn't exist returns empty lists (with searchable prefixes)
                    { "-Finance", new List<Guid>(), new List<Guid>() }, //Excluding a tag that doesn't exist returns empty lists
                    { "-Tag:Finance", new List<Guid>(), new List<Guid>() }, //Excluding a tag that doesn't exist returns empty lists (with searchable prefixes)
                    { "Finance, -Bitcoin, Investment Banking, -Real Estate", new List<Guid>(), new List<Guid>() }, //Including and excluding tags that don't exist returns empty lists
                    { "Tag:Finance, -Tag:Bitcoin, Tag:Investment Banking, -Tag:Real Estate", new List<Guid>(), new List<Guid>() }, //Including and excluding tags that don't exist returns empty lists (with searchable prefixes)
                    { "Science Fiction", new List<Guid>() { SCIENCE_FICTION_ID }, new List<Guid>() }, //Include one tag that exists
                    { "Thriller", new List<Guid>() { ACTION_ID }, new List<Guid>() }, //Include one alias that exists
                    { "Tag:Science Fiction", new List<Guid>() { SCIENCE_FICTION_ID }, new List<Guid>() }, //Include one tag that exists (with searchable prefixes)
                    { "Tag:Hallmark", new List<Guid>() { ROMANCE_ID }, new List<Guid>() }, //Include one alias that exists (with searchable prefixes)
                    { "-Swords and Sandals", new List<Guid>(), new List<Guid>() { SWORDS_AND_SANDALS_ID } }, //Exclude one tag that exists
                    { "-Hard Drive Dystopia", new List<Guid>(), new List<Guid>() { CYBERPUNK_ID } }, //Exclude one alias that exists
                    { "-Tag:Swords and Sandals", new List<Guid>(), new List<Guid>() { SWORDS_AND_SANDALS_ID } }, //Exclude one tag that exists (with searchable prefixes)
                    { "-Tag:Hard Drive Dystopia", new List<Guid>(), new List<Guid>() { CYBERPUNK_ID } }, //Exclude one tag that exists (with searchable prefixes)
                    {
                        "Science Fiction, -Swords and Sandals",
                        new List<Guid>() { SCIENCE_FICTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID }
                    }, //Include and exclude one existing tag each
                    {
                        "Hallmark, -Hard Drive Dystopia",
                        new List<Guid>() { ROMANCE_ID },
                        new List<Guid>() { CYBERPUNK_ID }
                    }, //Include and exclude one existing alias each
                    {
                        "Tag:Science Fiction, -Tag:Swords and Sandals",
                        new List<Guid>() { SCIENCE_FICTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID }
                    }, //Include and exclude one existing tag each (with searchable prefixes)
                    {
                        "Tag:Hallmark, -Tag:Hard Drive Dystopia",
                        new List<Guid>() { ROMANCE_ID },
                        new List<Guid>() { CYBERPUNK_ID }
                    }, //Include and exclude one existing alias each (with searchable prefixes)
                    {
                        "Science Fiction, -Swords and Sandals, Romance, -Cyberpunk, -Action",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID, ACTION_ID }
                    }, //Include and exclude multiple existing tags
                    {
                        "Thriller, Hallmark, -Hard Drive Dystopia",
                        new List<Guid>() { ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { CYBERPUNK_ID, }
                    }, //Include and exclude multiple existing aliases
                    {
                        "Tag:Science Fiction, -Tag:Swords and Sandals, Tag:Romance, -Tag:Cyberpunk, -Tag:Action",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID, ACTION_ID }
                    }, //Include and exclude multiple existing tags (with searchable prefixes)
                    {
                        "Tag:Thriller, Tag:Hallmark, -Tag:Hard Drive Dystopia",
                        new List<Guid>() { ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { CYBERPUNK_ID, }
                    }, //Include and exclude multiple existing aliases
                    {
                        "Science Fiction, -Swords and Sandals, Romance, -Cyberpunk, Action, Bitcoin, -Finance, Steampunk, Adventure",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID }
                    }, //Include and exclude a combination of tags that exist and tags that don't
                    {
                        "Science Fiction, -Swords and Sandals, Hallmark, -Cyberpunk, Thriller, Bitcoin, -Finance, Steampunk, Adventure",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID }
                    }, //Include and exclude a combination of tags that exist and tags that don't
                    {
                        "Tag:Science Fiction, -Tag:Swords and Sandals, Tag:Hallmark, -Tag:Cyberpunk, Tag:Thriller, Tag:Bitcoin, -Tag:Finance, Tag:Steampunk, Tag:Adventure",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID }
                    }, //Include and exclude a combination of tags that exist and tags that don't (with searchable prefixes)
                    { "Deleted 1", new List<Guid>(), new List<Guid>() }, //Include a deleted tag
                    { "Removed A", new List<Guid>(), new List<Guid>() }, //Include a deleted alias
                    { "Tag:Deleted 1", new List<Guid>(), new List<Guid>() }, //Include a deleted tag (with searchable prefix)
                    { "Tag:Removed 1", new List<Guid>(), new List<Guid>() }, //Include a deleted tag (with searchable prefix)
                    { "-Deleted 1", new List<Guid>(), new List<Guid>() }, //Exclude a deleted tag
                    { "-Removed 1", new List<Guid>(), new List<Guid>() }, //Exclude a deleted alias
                    { "-Tag:Deleted 1", new List<Guid>(), new List<Guid>() }, //Exclude a deleted tag (with searchable prefix)
                    { "-Tag:Removed 1", new List<Guid>(), new List<Guid>() }, //Exclude a deleted alias (with searchable prefix)
                    { "Deleted 1, -Deleted 2, -Deleted 3", new List<Guid>(), new List<Guid>() }, //Include and exclude a combination of private and deleted tags
                    { "Removed A, -Removed 2, -Deleted 3", new List<Guid>(), new List<Guid>() }, //Include and exclude a combination of private and deleted tags
                    { "Tag:Deleted 1, -Tag:Private 2, -Tag:Deleted 2, -Tag:Deleted 3", new List<Guid>(), new List<Guid>() }, // Include and exclude a combination of private and deleted tags (with searchable prefixs)
                    {
                        "Science Fiction, -Swords and Sandals, Romance, -Cyberpunk, Action, Bitcoin, -Finance, Steampunk, Adventure, Deleted 1, -Deleted 2, -Deleted 3",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID }
                    }, //Include a combination of active tags with a combination of private and deleted tags
                    {
                        "Tag:Science Fiction, -Tag:Swords and Sandals, Tag:Romance, -Tag:Cyberpunk, Tag:Action, Tag:Bitcoin, -Tag:Finance, Tag:Steampunk, Tag;Adventure, Tag:Private 1, Tag:Deleted 1, -Tag:Private 2, -Tag:Deleted 2, -Tag:Deleted 3",
                        new List<Guid>() { SCIENCE_FICTION_ID, ROMANCE_ID, ACTION_ID },
                        new List<Guid>() { SWORDS_AND_SANDALS_ID, CYBERPUNK_ID }
                    }, //Include a combination of active tags with a combination of private and deleted tags (with searchable prefixes)
                };

                return data;
            }
        }

        [Fact]
        public async Task GetActiveTags_ReturnsExpectedResult()
        {
            //Arrange
            List<Tag> expectedResultsReturned = CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, swordsAndSandals, romance, impliesNothing, virtualFormat, tagA, tagA1, tagA2, tagA12, tagA21, tagA22, tagA223, activeWithImpliedByRelationships, activeWithImpliesRelationships });

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetActiveTags();

            //Assert
            Assert.Equal(expectedResultsReturned, result);
            Assert.Empty(result.Where(x => x.IsDeleted == true));
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleTagTheoryData))]
        public async Task FindExistingAccessibleTag_ReturnsTag(Guid id, Tag expected)
        {
            //Arrange
            mockTagRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(tags.Where(x => x.TagID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            Tag result = await tagRetrievalService.FindTag(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid, Tag> FindExistingAccessibleTagTheoryData
        {
            get
            {
                TheoryData<Guid, Tag> data = new TheoryData<Guid, Tag>
                {
                    { ACTION_ID, CloneHelper.Clone(action) },
                    { DELETED_ID_1, CloneHelper.Clone(deleted1) }
                };

                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingTag_ThrowsTagNotFoundException()
        {
            //Arrange
            Guid id = Guid.NewGuid();

            mockTagRepository.Setup(x => x.FindAsync(It.IsAny<Guid>(), It.IsAny<bool>())).Returns(Task.FromResult<Tag>(null));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagNotFoundException exception = await Assert.ThrowsAsync<TagNotFoundException>(() => tagRetrievalService.FindTag(id, User, bypassCache));
            mockTagRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task ExistingTagWithoutPermission_ThrowsTagNotViewableException()
        {
            //Arrange
            Guid id = DELETED_ID_1;

            mockTagRepository.Setup(x => x.FindAsync(It.IsAny<Guid>(), It.IsAny<bool>())).Returns(Task.FromResult<Tag>(deleted1));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagNotViewableException exception = await Assert.ThrowsAsync<TagNotViewableException>(() => tagRetrievalService.FindTag(id, User, bypassCache));
            mockTagRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindActiveTagWithoutParentPermission_ThrowsTagUnableToViewParentTagTypeException()
        {
            //Arrange
            Guid id = TAG_UNDER_DELETED_1;

            mockTagRepository.Setup(x => x.FindAsync(It.IsAny<Guid>(), It.IsAny<bool>())).Returns(Task.FromResult<Tag>(tagUnderDeleted1));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            //Act
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagUnableToViewParentTagTypeException exception = await Assert.ThrowsAsync<TagUnableToViewParentTagTypeException>(() => tagRetrievalService.FindTag(id, User, bypassCache));
            mockTagRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindDeletedTagWithoutParentPermission_ThrowsTagUnableToViewParentTagTypeException()
        {
            //Arrange
            Guid id = TAG_UNDER_DELETED_2;

            mockTagRepository.Setup(x => x.FindAsync(It.IsAny<Guid>(), It.IsAny<bool>())).Returns(Task.FromResult<Tag>(tagUnderDeleted2));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            //Act
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            TagUnableToViewParentTagTypeException exception = await Assert.ThrowsAsync<TagUnableToViewParentTagTypeException>(() => tagRetrievalService.FindTag(id, User, bypassCache));
            mockTagRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task GetRemovedTagsWithParentPermission_ReturnsExpectedResult()
        {
            //Arrange
            List<Tag> expected = CloneHelper.Clone(new List<Tag> { deleted1, deleted2, deleted3, deletedWithImpliedByRelationships, deletedWithImpliesRelationships, tagUnderDeleted1, tagUnderDeleted2 });
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetDeletedTags(User);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        [Fact]
        public async Task GetRemovedTagsWithoutTagPermissionWithParentPermission_ReturnsExpectedResult()
        {
            //Arrange
            List<Tag> expected = CloneHelper.Clone(new List<Tag>());
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetDeletedTags(User);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Never());
        }

        [Fact]
        public async Task GetRemovedTagsWithoutParentPermission_ReturnsExpectedResult()
        {
            //Arrange
            List<Tag> expected = CloneHelper.Clone(new List<Tag> { deleted1, deleted2, deleted3, deletedWithImpliedByRelationships, deletedWithImpliesRelationships });
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetDeletedTags(User);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        [Fact]
        public async Task GetRemovedTagsWithoutTagPermissionWithoutParentPermission_ReturnsExpectedResult()
        {
            //Arrange
            List<Tag> expected = CloneHelper.Clone(new List<Tag>());
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), Policies.Administrator.VIEW_DELETED_TAG_TYPE)).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetDeletedTags(User);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Never());
        }

        [Theory]
        [MemberData(nameof(GetListOfImpliedByTagsTheoryData))]
        public async Task GetListOfImpliedByTags_ReturnsExpectedResult(Tag tag, List<Tag> impliedByTags)
        {
            //Arrange
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetListOfTagsImplyingTag(tag);

            //Assert
            Assert.Equal(impliedByTags.OrderBy(t => t).ToList(), result.OrderBy(t => t).ToList());
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        public static TheoryData<Tag, List<Tag>> GetListOfImpliedByTagsTheoryData
        {
            get
            {
                TheoryData<Tag, List<Tag>> data = new TheoryData<Tag, List<Tag>>
                {
                    { CloneHelper.Clone(impliesNothing), CloneHelper.Clone(new List<Tag>()) },
                    { CloneHelper.Clone(tagA1), CloneHelper.Clone(new List<Tag>() { tagA }) },
                    { CloneHelper.Clone(tagA223), CloneHelper.Clone(new List<Tag>() { tagA22, tagA2, tagA }) },
                    { CloneHelper.Clone(tagA12), CloneHelper.Clone(new List<Tag>() { tagA1, tagA2, tagA }) }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(GetTagsByTagTypeAsyncTheoryData))]
        public async Task GetTagsByTagTypeAsyncAsync_ReturnsExpectedResult(Guid tagTypeID, List<Tag> expected)
        {
            //Arrange
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> result = await tagRetrievalService.GetTagsByTagType(tagTypeID);

            //Assert
            Assert.Equal(expected, result);
            mockTagRepository.Verify(mock => mock.AllAsync(), Times.Once());
        }

        public static TheoryData<Guid, List<Tag>> GetTagsByTagTypeAsyncTheoryData
        {
            get
            {
                TheoryData<Guid, List<Tag>> data = new TheoryData<Guid, List<Tag>>
                {
                    { Guid.Empty, CloneHelper.Clone(new List<Tag>()) },
                    { TAG_TYPE_ID, CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, swordsAndSandals, romance, deleted1, deleted2, deleted3 }) },
                    { FORMAT_TYPE_ID, CloneHelper.Clone(new List<Tag>() { impliesNothing, virtualFormat }) },
                    { SERIES_TYPE_ID, CloneHelper.Clone(new List<Tag>() { tagA, deletedWithImpliedByRelationships, deletedWithImpliesRelationships, activeWithImpliedByRelationships, activeWithImpliesRelationships }) },
                    { DELETED_TYPE_ID, CloneHelper.Clone(new List<Tag>() { tagUnderDeleted1, tagUnderDeleted2 }) }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(FilterTagsByWorkTypeTheoryData))]
        public void FilterTagsByWorkType_ReturnsExpectedResult(List<Tag> tags, Guid workTypeID, bool includeVirtual, TagSearchLists expected)
        {
            //Arrange
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            TagSearchLists result = tagRetrievalService.FilterTagsByWorkType(tags, workTypeID, includeVirtual);

            //Assert
            Assert.Equal(expected.Included, result.Included);
            Assert.Equal(expected.Excluded, result.Excluded);
        }

        public static TheoryData<List<Tag>, Guid, bool, TagSearchLists> FilterTagsByWorkTypeTheoryData
        {
            get
            {
                var data = new TheoryData<List<Tag>, Guid, bool, TagSearchLists>()
                {
                    //WORK
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA }),
                         WorkTypes.WORK, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA },
                        new List<Tag>() { }))
                    }, //Tags that exist for a given work type (no virtual) [include virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA }),
                        WorkTypes.WORK, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA },
                        new List<Tag>() { }))
                    }, //Tags that exist for a given work type (no virtual) [exclude virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { impliesNothing, tagA2, tagA22 }),
                        WorkTypes.WORK, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { },
                        new List<Tag>() { impliesNothing, tagA2, tagA22 }))
                    }, //Tags that don't exist for a given work type (no virtual) [include virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { impliesNothing, tagA2, tagA22 }),
                        WorkTypes.WORK, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { },
                        new List<Tag>() { impliesNothing, tagA2, tagA22 }))
                    }, //Tags that don't exist for a given work type (no virtual) [exclude virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA, impliesNothing, tagA2, tagA22, virtualFormat, tagA21 }),
                        WorkTypes.WORK, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA, tagA21 },
                        new List<Tag>() { impliesNothing, tagA2, tagA22, virtualFormat, }))
                    }, //Pull mix of tags can and cannot appear on work (with virtual tags) [include virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA, impliesNothing, tagA2, tagA22, virtualFormat, tagA21 }),
                        WorkTypes.WORK, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA1, tagA },
                        new List<Tag>() { impliesNothing, tagA2, tagA22, virtualFormat, tagA21, }))
                    }, //Pull mix of tags can and cannot appear on work (with virtual tags) [exclude virtual]
                    //VOLUME
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing }),
                        WorkTypes.VOLUME, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing },
                        new List<Tag>() { }))
                    }, //Tags that exist for a given volume type (no virtual)
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing }),
                        WorkTypes.VOLUME, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing },
                        new List<Tag>() { }))
                    }, //Tags that exist for a given volume type (no virtual)
                    {
                        CloneHelper.Clone(new List<Tag>() { tagA1, tagA12, tagA, tagA2, tagA22 }),
                        WorkTypes.VOLUME, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { },
                        new List<Tag>() { tagA1, tagA12, tagA, tagA2, tagA22 }))
                    }, //Tags that don't exist for a given work type (no virtual) [include virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { tagA1, tagA12, tagA, tagA2, tagA22 }),
                        WorkTypes.VOLUME, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { },
                        new List<Tag>() { tagA1, tagA12, tagA, tagA2, tagA22 }))
                    }, //Tags that don't exist for a given work type (no virtual) [exclude virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing, virtualFormat, tagA1, tagA12, tagA, tagA2, tagA22, tagA21, tagA223 }),
                        WorkTypes.VOLUME, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing, virtualFormat, },
                        new List<Tag>() { tagA1, tagA12, tagA, tagA2, tagA22, tagA21, tagA223  }))
                    }, //Pull mix of tags can and cannot appear on work (with virtual tags included]) include virtual
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing, virtualFormat, tagA1, tagA12, tagA, tagA2, tagA22, tagA21, tagA223 }),
                        WorkTypes.VOLUME, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, impliesNothing,   },
                        new List<Tag>() { virtualFormat, tagA1, tagA12, tagA, tagA2, tagA22, tagA21, tagA223 }))
                    }, //Pull mix of tags can and cannot appear on work (with virtual tags included]) exclude virtual

                    //CHAPTER
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22 }),
                        WorkTypes.CHAPTER, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22 },
                        new List<Tag>() { }))
                    }, //Tags that exist for a given chapter type (no virtual) include virtual
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22 }),
                        WorkTypes.CHAPTER, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22 },
                        new List<Tag>() { }))
                    },//Tags that exist for a given chapter type (no virtual) exclude virtual
                    {
                        CloneHelper.Clone(new List<Tag>() { tagA1, tagA12, tagA, impliesNothing }),
                        WorkTypes.CHAPTER, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() {  },
                        new List<Tag>() { tagA1, tagA12, tagA, impliesNothing }))
                    }, //Tags that don't exist for a given chapter type (no virtual) [include virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { tagA1, tagA12, tagA, impliesNothing }),
                        WorkTypes.CHAPTER, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() {  },
                        new List<Tag>() { tagA1, tagA12, tagA, impliesNothing }))
                    },//Tags that don't exist for a given chapter type (no virtual) [exclude virtual]
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22, tagA223, tagA1, tagA12, tagA, impliesNothing, tagA21, virtualFormat }),
                        WorkTypes.CHAPTER, true,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22, tagA223,  },
                        new List<Tag>() { tagA1, tagA12, tagA, impliesNothing, tagA21, virtualFormat }))
                    },//Pull mix of tags can and cannot appear on work (with virtual tags included]) include virtual
                    {
                        CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22, tagA223, tagA1, tagA12, tagA, impliesNothing, tagA21, virtualFormat }),
                        WorkTypes.CHAPTER, false,
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { action, scienceFiction, cyberpunk, tagA2, tagA22, },
                        new List<Tag>() { tagA223, tagA1, tagA12, tagA, impliesNothing, tagA21, virtualFormat }))
                    },//Pull mix of tags can and cannot appear on work (with virtual tags included]) exclude virtual
                };

                return data;
            }
        }
    }
}
