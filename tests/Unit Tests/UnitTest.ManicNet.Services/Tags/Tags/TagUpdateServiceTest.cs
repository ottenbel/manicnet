﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Tags.Tags
{
    public sealed class TagUpdateServiceTest
    {
        private readonly List<Tag> tags;

        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<ITagRepository> mockTagRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;

        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SCIENCE_FICTION_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid SWORDS_AND_SANDALS_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");
        private static Guid DELETED_ID_1 = new Guid("ECBDAD4E-05B1-4BAE-A070-A45E86B31DE4");
        private static Guid DELETED_ID_2 = new Guid("59BE9AA1-EC30-457E-AF48-1189E137EB07");
        private static Guid DELETED_ID_3 = new Guid("7B342989-1FCB-4335-B428-F71E448F4FA1");

        private static Guid IMPLIES_NOTHING = new Guid("1BDDD575-D114-476D-9C45-80CCF779C95C");
        private static Guid VIRTUAL_FORMAT = new Guid("210E29FE-986E-45F6-BDC6-4E5537B8AAF4");
        private static Guid TAG_A = new Guid("720514C5-35BE-410C-BEDC-145F426C0501");
        private static Guid TAG_A1 = new Guid("134BFF47-B45E-4786-ACF6-A9A5EF33A258");
        private static Guid TAG_A2 = new Guid("E6F8272E-F25E-4A16-B215-63768C81D0D4");
        private static Guid TAG_A12 = new Guid("8FBC173E-DF41-4DC0-B06C-EE96839D5288");
        private static Guid TAG_A21 = new Guid("0AD50DC9-AAC0-4263-ADF4-DAD8DE650281");
        private static Guid TAG_A22 = new Guid("9D90E94C-4737-4048-B00F-314F2295209A");
        private static Guid TAG_A223 = new Guid("02D58B9F-B0CF-484D-8707-121F7CBD68FC");
        private static Guid DELETED_WITH_IMPLIED_BY_RELATIONSHIPS = new Guid("25440259-578E-4FF5-A8F8-15B8773D0BAA");
        private static Guid DELETED_WITH_IMPLIES_RELATIONSHIPS = new Guid("3AC3D9FD-E514-439B-A1FF-F715BF91F121");
        private static Guid ACTIVE_WITH_IMPLIED_BY_RELATIONSHIPS = new Guid("1690CA31-1FC8-4257-9957-59D3C0C02754");
        private static Guid ACTIVE_WITH_IMPLIES_RELATIONSHIPS = new Guid("551CD5A1-1346-4E82-AFEA-92895580916A");

        private static Guid TAG_UNDER_DELETED_1 = new Guid("88E06373-B829-455A-9AE0-D161279FF43B");

        //Alias IDs
        private static Guid ACTION_ADVENTURE_ID = new Guid("333061E2-C86A-434E-B300-A781788A5520");
        private static Guid HARD_DRIVE_DYSTOPIA_ID = new Guid("9ADA7818-A8FA-4DBB-A3A9-E806407C2D64");
        private static Guid HALLMARK_ID = new Guid("D8A08F27-8263-4030-8EF6-37F8988E8B59");

        private static Guid REMOVED_1_ID = new Guid("2E45279E-D7AD-4579-A3B5-780D356A930E");
        private static Guid REMOVED_A_ID = new Guid("BF7E80C8-8A23-4681-A5B1-924CCEB71827");
        private static Guid REMOVED_2_ID = new Guid("C6F7456C-2EE5-4E7A-87AF-84E0B88D80A3");

        private static Guid TAG_TYPE_ID = new Guid("B529B4FF-97F3-4CBA-A5C9-6F91D15ACCC3");
        private static Guid ARTIST_TYPE_ID = new Guid("4E72F519-5527-42CD-A805-6E64782AC4D6");
        private static Guid TRANSLATOR_TYPE_ID = new Guid("22528AEE-AC97-403B-87CA-D7225A697CA7");
        private static Guid FORMAT_TYPE_ID = new Guid("A184AF36-B721-420E-A8E3-3B299967C267");
        private static Guid SERIES_TYPE_ID = new Guid("0D33FC5E-F050-474E-9EE5-4FDB711D1251");
        private static Guid DELETED_TYPE_ID = new Guid("E783FDDF-DA7A-4A33-A8B9-50A677EBD138");

        private static readonly TagAlias actionAdventure = new TagAlias(ACTION_ADVENTURE_ID, ACTION_ID,
            "Action Adventure");
        private static readonly TagAlias thriller = new TagAlias(new Guid(), new Guid(),
            "Thriller");
        private static readonly TagAlias hardDriveDystopia = new TagAlias(HARD_DRIVE_DYSTOPIA_ID, CYBERPUNK_ID,
            "Hard Drive Dystopia");
        private static readonly TagAlias hallmark = new TagAlias(HALLMARK_ID, ROMANCE_ID,
            "Hallmark");
        private static readonly TagAlias removed1 = new TagAlias(REMOVED_1_ID, DELETED_ID_1,
            "Removed 1");
        private static readonly TagAlias removedA = new TagAlias(REMOVED_A_ID, DELETED_ID_1,
            "Removed A");
        private static readonly TagAlias removed2 = new TagAlias(REMOVED_2_ID, DELETED_ID_2,
            "Removed 2");

        private static readonly TagType tagTagType = new TagType(TAG_TYPE_ID, "Tag", "tag", 0, string.Empty, string.Empty, string.Empty, true, true, true, false, false, false, 0, 0, 0, false);
        private static readonly TagType artistTagType = new TagType(ARTIST_TYPE_ID, "Artist", "artist", 1, string.Empty, string.Empty, string.Empty, true, false, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType seriesTagType = new TagType(SERIES_TYPE_ID, "Series", "series", 2, string.Empty, string.Empty, string.Empty, true, false, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType formatTagType = new TagType(FORMAT_TYPE_ID, "Format", "Format", 3, string.Empty, string.Empty, string.Empty, false, true, false, false, false, false, 0, 0, 0, false);
        private static readonly TagType translatorTagType = new TagType(TRANSLATOR_TYPE_ID, "Translator", "translator", 4, string.Empty, string.Empty, string.Empty, false, false, true, false, false, false, 0, 0, 0, false);
        private static readonly TagType deleted = new TagType(DELETED_TYPE_ID, "Deleted", "deleted", 5, string.Empty, string.Empty, string.Empty, true, true, true, true, true, true, 0, 0, 0, true);

        private static readonly Tag action = new Tag(ACTION_ID, tagTagType, "Action", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { actionAdventure, thriller });
        private static readonly Tag scienceFiction = new Tag(SCIENCE_FICTION_ID, tagTagType, "Science Fiction", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag cyberpunk = new Tag(CYBERPUNK_ID, tagTagType, "Cyberpunk", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { hardDriveDystopia });
        private static readonly Tag swordsAndSandals = new Tag(SWORDS_AND_SANDALS_ID, tagTagType, "Swords and Sandals", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag romance = new Tag(ROMANCE_ID, tagTagType, "Romance", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { hallmark });
        private static readonly Tag deleted1 = new Tag(DELETED_ID_1, tagTagType, "Deleted 1", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { removed1, removedA });
        private static readonly Tag deleted2 = new Tag(DELETED_ID_2, tagTagType, "Deleted 2", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>() { removed2 });
        private static readonly Tag deleted3 = new Tag(DELETED_ID_3, tagTagType, "Deleted 3", false, true, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag impliesNothing = new Tag(IMPLIES_NOTHING, formatTagType, "Implies Nothing", false, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag virtualFormat = new Tag(VIRTUAL_FORMAT, formatTagType, "Virtual Format", true, false, new List<Guid>(), new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA = new Tag(TAG_A, seriesTagType, "Tag A", false, false,
            new List<Guid>(), new List<Guid>() { TAG_A1, TAG_A2 }, new List<TagAlias>());
        private static readonly Tag tagA1 = new Tag(TAG_A1, artistTagType, "Tag A1", false, false,
            new List<Guid>() { TAG_A }, new List<Guid>() { TAG_A12, TAG_A21 }, new List<TagAlias>());
        private static readonly Tag tagA2 = new Tag(TAG_A2, translatorTagType, "Tag A2", false, false,
            new List<Guid>() { TAG_A }, new List<Guid>() { TAG_A12, TAG_A21, TAG_A22 }, new List<TagAlias>());
        private static readonly Tag tagA12 = new Tag(TAG_A12, artistTagType, "Tag A12", false, false,
            new List<Guid>() { TAG_A1, TAG_A2 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA21 = new Tag(TAG_A21, artistTagType, "Tag A21", true, false,
            new List<Guid>() { TAG_A1, TAG_A2 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagA22 = new Tag(TAG_A22, translatorTagType, "Tag A22", false, false,
            new List<Guid>() { TAG_A2 }, new List<Guid>() { TAG_A223 }, new List<TagAlias>());
        private static readonly Tag tagA223 = new Tag(TAG_A223, translatorTagType, "Tag A223", true, false,
            new List<Guid>() { TAG_A22 }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag deletedWithImpliedByRelationships = new Tag(DELETED_WITH_IMPLIED_BY_RELATIONSHIPS, seriesTagType, "Deleted With Implied By Relationships", false, true,
            new List<Guid>() { CYBERPUNK_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag deletedWithImpliesRelationships = new Tag(DELETED_WITH_IMPLIES_RELATIONSHIPS, seriesTagType, "Deleted With Implies Relationships", false, true,
            new List<Guid>(), new List<Guid>() { ACTION_ID, SCIENCE_FICTION_ID }, new List<TagAlias>());
        private static readonly Tag activeWithImpliedByRelationships = new Tag(ACTIVE_WITH_IMPLIED_BY_RELATIONSHIPS, seriesTagType, "Active With Implied By Relationships", false, false,
            new List<Guid>() { CYBERPUNK_ID }, new List<Guid>(), new List<TagAlias>());
        private static readonly Tag activeWithImpliesRelationships = new Tag(ACTIVE_WITH_IMPLIES_RELATIONSHIPS, seriesTagType, "Active With Implies Relationships", false, false,
            new List<Guid>(), new List<Guid>() { ACTION_ID, SCIENCE_FICTION_ID }, new List<TagAlias>());

        private static readonly Tag tagUnderDeleted1 = new Tag(TAG_UNDER_DELETED_1, deleted, "Tag Under Deleted", false, false, new List<Guid>(),
            new List<Guid>(), new List<TagAlias>());
        private static readonly Tag tagUnderDeleted2 = new Tag(TAG_UNDER_DELETED_1, deleted, "Tag Under Deleted", false, false, new List<Guid>(),
            new List<Guid>(), new List<TagAlias>());

        public TagUpdateServiceTest()
        {
            tags = CloneHelper.Clone(new List<Tag>() { action, scienceFiction, cyberpunk, swordsAndSandals, romance, deleted1, deleted2, deleted3, impliesNothing, virtualFormat, tagA, tagA1, tagA2, tagA12, tagA21, tagA22, tagA223, deletedWithImpliedByRelationships, deletedWithImpliesRelationships, activeWithImpliedByRelationships, activeWithImpliesRelationships, tagUnderDeleted1, tagUnderDeleted2 });

            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockTagRepository = new Mock<ITagRepository>();
            mockTagRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(tags));

            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();

            mockCacheCleanerService = new Mock<ICacheCleanerService>();
        }

        [Theory]
        [MemberData(nameof(ValidateTagImplicationListTheoryData))]
        public async Task ValidateTagImplicationList_ReturnsExpectedResult(Tag tag, List<Tag> impliedTags, TagSearchLists output)
        {
            //Arrange
            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);
            TagRetrievalService tagRetrievalService = new TagRetrievalService(mockTagRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Tag> ancestry = await tagRetrievalService.GetListOfTagsImplyingTag(tag);
            TagSearchLists result = tagUpdateService.ValidateTagImplicationList(tag, impliedTags, ancestry);

            //Assert
            Assert.Equal(output.Included.OrderBy(t => t).ToList(), result.Included.OrderBy(t => t).ToList());
            Assert.Equal(output.Excluded.OrderBy(t => t).ToList(), result.Excluded.OrderBy(t => t).ToList());
        }

        public static TheoryData<Tag, List<Tag>, TagSearchLists> ValidateTagImplicationListTheoryData
        {
            get
            {
                var data = new TheoryData<Tag, List<Tag>, TagSearchLists>
                {
                    { CloneHelper.Clone(action), CloneHelper.Clone(new List<Tag>()), CloneHelper.Clone(new TagSearchLists()) }, // No tags implied
                    {
                        CloneHelper.Clone(scienceFiction),
                        CloneHelper.Clone(new List<Tag>() { cyberpunk, swordsAndSandals, romance, impliesNothing, tagA12, tagA22 }),
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { cyberpunk, swordsAndSandals, romance, impliesNothing, tagA12, tagA22 },
                    new List<Tag>()))
                    }, // Tags implied no loop
                    {
                        CloneHelper.Clone(impliesNothing),
                        CloneHelper.Clone(new List<Tag>() { impliesNothing }),
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>(),
                    new List<Tag>() { impliesNothing }))
                    }, // Implied tag is referencing parent tag directly
                    {
                        CloneHelper.Clone(tagA21),
                        CloneHelper.Clone(new List<Tag>() { tagA, tagA1, tagA2 }),
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>(),
                    new List<Tag>() { tagA, tagA1, tagA2 }))
                    },// Only looping tags
                    {
                        CloneHelper.Clone(tagA223),
                        CloneHelper.Clone(new List<Tag>() { impliesNothing, tagA12, tagA21, tagA, tagA2, tagA22 }),
                        CloneHelper.Clone(new TagSearchLists(new List<Tag>() { impliesNothing, tagA12, tagA21 },
                    new List<Tag>() { tagA, tagA2, tagA22 }))
                    } // Looping tags and regular tags
                };
                return data;
            }
        }

        [Fact]
        public async Task CreateTag_CallsExpectedMethods()
        {
            //Arrange
            Tag tag = CloneHelper.Clone(tagA1);
            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);


            List<Tag> includedTags = new List<Tag>();
            List<Guid> includedTagsIDList = includedTags.Select(s => s.TagID).ToList();

            //Act
            await tagUpdateService.CreateTag(tag, includedTags);

            //Asert
            mockTagRepository.Verify(mock => mock.Insert(tag, includedTags), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateTag_CallsExpectedMethods()
        {
            //Arrange
            Tag tag = CloneHelper.Clone(tagA1);
            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            List<Guid> includedTags = new List<Guid>();

            //Act
            await tagUpdateService.UpdateTag(tag);

            //Asert
            mockTagRepository.Verify(mock => mock.Update(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateTagWithIncludedTags_CallsExpectedMethods()
        {
            //Arrange
            Tag tag = CloneHelper.Clone(tagA1);
            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            List<Tag> includedTags = new List<Tag>();

            TagCacheCleanup tagCacheCleanup = new TagCacheCleanup(tag);

            //Act
            await tagUpdateService.UpdateTag(tag, includedTags, tagCacheCleanup);

            //Asert
            mockTagRepository.Verify(mock => mock.Update(tag, includedTags), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveTagAsTopLevelInNonDeletableState_ThrowsTagNotDeletableException()
        {
            //Arrange
            Tag tag = CloneHelper.Clone(action);

            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act/Assert
            TagNotDeletableException exception = await Assert.ThrowsAsync<TagNotDeletableException>(() => tagUpdateService.RemoveTag(tag));
            mockTagRepository.Verify(mock => mock.Delete(tag), Times.Never());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        [Theory]
        [MemberData(nameof(RemoveTagAsTopLevelRelatedToOtherTags_ReturnsExpectedResultsTheoryData))]
        public async Task RemoveTagAsTopLevelRelatedToOtherTags_ReturnsExpectedResults(Tag tag)
        {
            //Arrange
            List<Guid> relatedTags = tag.Implies.Select(i => i.ImpliesTagID).Concat(tag.ImpliedBy.Select(i => i.ImpliedByTagID)).ToList();

            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await tagUpdateService.RemoveTag(tag);

            //Assert
            mockTagRepository.Verify(mock => mock.Update(tag), Times.Once());
            mockTagRepository.Verify(mock => mock.Delete(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        public static TheoryData<Tag> RemoveTagAsTopLevelRelatedToOtherTags_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Tag> data = new TheoryData<Tag>()
                {
                    { CloneHelper.Clone(deletedWithImpliedByRelationships) },
                    { CloneHelper.Clone(deletedWithImpliesRelationships) }
                };

                return data;
            }
        }

        [Fact]
        public async Task RemoveTagAsTopLevel_CallsExpectedMethods()
        {
            //Arrange
            Tag tag = CloneHelper.Clone(deleted1);
            List<Guid> expected = new List<Guid>();
            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await tagUpdateService.RemoveTag(tag);

            //Assert
            mockTagRepository.Verify(mock => mock.Update(tag), Times.Never());
            mockTagRepository.Verify(mock => mock.Delete(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Theory]
        [MemberData(nameof(RemoveTagNotTopLevel_ReturnsExpectedResultsTheoryData))]
        public async Task RemoveTagNotTopLevel_ReturnsExpectedResults(Tag tag)
        {
            //Arrange
            List<Guid> relatedTags = tag.Implies.Select(i => i.ImpliesTagID).Concat(tag.ImpliedBy.Select(i => i.ImpliedByTagID)).ToList();

            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await tagUpdateService.RemoveTag(tag, false);

            //Assert
            mockTagRepository.Verify(mock => mock.Update(tag), Times.Never());
            mockTagRepository.Verify(mock => mock.Delete(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        public static TheoryData<Tag> RemoveTagNotTopLevel_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Tag> data = new TheoryData<Tag>()
                {
                    { CloneHelper.Clone(deleted1) },
                    { CloneHelper.Clone(impliesNothing) }
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(RemoveTagNotTopLevelRelatedToOtherTags_ReturnsExpectedResultsTheoryData))]
        public async Task RemoveTagNotTopLevelRelatedToOtherTags_ReturnsExpectedResults(Tag tag)
        {
            //Arrange
            List<Guid> relatedTags = tag.Implies.Select(i => i.ImpliesTagID).Concat(tag.ImpliedBy.Select(i => i.ImpliedByTagID)).ToList();

            TagUpdateService tagUpdateService = new TagUpdateService(mockUnitOfWork.Object, mockTagRepository.Object, mockCacheCleanerService.Object, mockLogger.Object);

            //Act
            await tagUpdateService.RemoveTag(tag, false);

            //Assert
            mockTagRepository.Verify(mock => mock.Update(tag), Times.Once());
            mockTagRepository.Verify(mock => mock.Delete(tag), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        public static TheoryData<Tag> RemoveTagNotTopLevelRelatedToOtherTags_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Tag> data = new TheoryData<Tag>()
                {
                    { CloneHelper.Clone(deletedWithImpliedByRelationships) },
                    { CloneHelper.Clone(deletedWithImpliesRelationships) },
                    { CloneHelper.Clone(activeWithImpliedByRelationships) },
                    { CloneHelper.Clone(activeWithImpliesRelationships) }
                };

                return data;
            }
        }
    }
}
