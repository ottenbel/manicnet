﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Works
{
    public sealed class WorkUpdateServiceTest
    {
        private readonly List<Work> works;

        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<IWorkRepository> mockWorkRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IContentFileHandlerService> mockContentFileHandlerService;
        private readonly Mock<IVolumeUpdateService> mockVolumeUpdateService;
        private readonly Mock<IUploadService> mockUploadService;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;

        private static Guid ACTIVE_WORK_1 = new Guid("FF778EE5-2546-46F4-AC4D-EFAE272CE05E");
        private static Guid ACTIVE_WORK_2 = new Guid("53322CB2-C329-45C9-A8F7-A31761C2873F");
        private static Guid ACTIVE_WORK_3 = new Guid("60E31C7E-6F2D-40FC-9783-1A5D58C2EEFF");
        private static Guid ACTIVE_WORK_4 = new Guid("F2755311-0F74-483F-B4B9-82F9A0EC5ABA");
        private static Guid ACTIVE_WORK_5 = new Guid("40E91D39-73EE-42AD-B396-714368145988");
        private static Guid DELETED_WORK_1 = new Guid("5901855C-AFF2-47C3-B91E-70A2E73D5442");
        private static Guid DELETED_WORK_2 = new Guid("A803F4EA-A522-4344-AC44-F7EEA5578F82");
        private static Guid DELETED_WORK_3 = new Guid("276AD87E-ADC6-4DBA-B543-18496492191D");

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork2 = new Work(ACTIVE_WORK_2, "Active Work 2", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork3 = new Work(ACTIVE_WORK_3, "Active Work 3", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork4 = new Work(ACTIVE_WORK_4, "Active Work 4", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork5 = new Work(ACTIVE_WORK_5, "Active Work 5", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork1 = new Work(DELETED_WORK_1, "Deleted Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork2 = new Work(DELETED_WORK_2, "Deleted Work 2", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork3 = new Work(DELETED_WORK_3, "Deleted Work 3", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        public WorkUpdateServiceTest()
        {
            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();

            mockWorkRepository = new Mock<IWorkRepository>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockContentFileHandlerService = new Mock<IContentFileHandlerService>();
            mockUploadService = new Mock<IUploadService>();

            mockVolumeUpdateService = new Mock<IVolumeUpdateService>();

            mockCacheCleanerService = new Mock<ICacheCleanerService>();

            works = CloneHelper.Clone(new List<Work>() { activeWork1, activeWork2, activeWork3, activeWork4, activeWork5, deletedWork1, deletedWork2, deletedWork3 });

            mockWorkRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(works));
        }

        [Fact]
        public async Task CreateWork_CallsExpectedMethods()
        {
            //Arrange
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work();
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();

            //Act
            await workUpdateService.CreateWork(work, primaryTags, secondaryTags, null);

            //Assert
            mockWorkRepository.Verify(mock => mock.Insert(work, primaryTags, secondaryTags), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateWork_CallsExpectedMethods()
        {
            //Arrange
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work();

            //Act
            await workUpdateService.UpdateWork(work);

            //Assert
            mockWorkRepository.Verify(mock => mock.Update(work), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateWorkWithIncludedTags_CallsExpectedMethods()
        {
            //Arrange
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work();
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();

            //Act
            await workUpdateService.UpdateWork(work, primaryTags, secondaryTags, null, false);

            //Assert
            mockWorkRepository.Verify(mock => mock.Update(work, primaryTags, secondaryTags), Times.Once);
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once);
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveWorkInNonDeletableState_ThrowsWorkNotDeletableException()
        {
            //Arrange
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work();

            //Act/Assert
            WorkNotDeletableException exception = await Assert.ThrowsAsync<WorkNotDeletableException>(() => workUpdateService.RemoveWork(work));

            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(work.CoverID), Times.Never());
            mockWorkRepository.Verify(mock => mock.Delete(work), Times.Never());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveWork_CallsExpectedMethods()
        {
            //Arrange
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work
            {
                IsDeleted = true
            };

            List<Guid> works = new List<Guid>() { work.WorkID };
            List<Guid> contents = new List<Guid>();

            //Act
            await workUpdateService.RemoveWork(work);

            //Assert
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(contents), Times.Once());
            mockVolumeUpdateService.Verify(mock => mock.RemoveVolume(It.IsAny<Volume>(), It.IsAny<WorkCacheCleanup>(), It.IsAny<bool>()), Times.Never());
            mockWorkRepository.Verify(mock => mock.Delete(work), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveWorkWithVolumes_CallsExpectedMethods()
        {
            //Arrange
            Volume volume1 = new Volume() { VolumeID = new Guid("A5E91B25-D4BD-4258-A283-89A8DF60D9A5") };
            Volume volume2 = new Volume() { VolumeID = new Guid("4B3C8773-9E3C-4140-AA3E-6D583D06FDC9") };
            Volume volume3 = new Volume() { VolumeID = new Guid("D04F877A-E954-4C25-AA7F-DEAC9AC50565") };

            mockVolumeUpdateService.Setup(x => x.RemoveVolume(volume1, It.IsAny<WorkCacheCleanup>(), false)).Returns(Task.FromResult(new CleanupWork(volume1)));
            mockVolumeUpdateService.Setup(x => x.RemoveVolume(volume2, It.IsAny<WorkCacheCleanup>(), false)).Returns(Task.FromResult(new CleanupWork(volume2)));
            mockVolumeUpdateService.Setup(x => x.RemoveVolume(volume3, It.IsAny<WorkCacheCleanup>(), false)).Returns(Task.FromResult(new CleanupWork(volume3)));
            WorkUpdateService workUpdateService = new WorkUpdateService(mockUnitOfWork.Object, mockWorkRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockVolumeUpdateService.Object, mockUploadService.Object, mockCacheCleanerService.Object);

            Work work = new Work
            {
                IsDeleted = true,
                Volumes = new List<Volume>() { volume1, volume2, volume3 }
            };

            List<Guid> works = new List<Guid>() { work.WorkID };
            List<Guid> contents = new List<Guid>();

            //Act
            await workUpdateService.RemoveWork(work);

            //Assert
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(contents), Times.Once());

            foreach (Volume volume in work.Volumes)
            {
                mockVolumeUpdateService.Verify(mock => mock.RemoveVolume(volume, It.IsAny<WorkCacheCleanup>(), false), Times.Once());
            }

            mockWorkRepository.Verify(mock => mock.Delete(work), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }
    }
}
