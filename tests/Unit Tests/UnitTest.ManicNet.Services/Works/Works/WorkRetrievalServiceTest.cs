﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Works
{
    public sealed class WorkRetrievalServiceTest
    {
        private bool bypassCache { get; set; }
        private readonly List<Work> works;

        private readonly Mock<IWorkRepository> mockWorkRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;
        private readonly Mock<ITagRetrievalService> mockTagRetrievalService;
        private readonly Mock<IUserBlockedTagsRetrievalService> mockUserBlockedTagRetrievalService;

        private ClaimsPrincipal User { get; set; }

        private static Guid ACTIVE_WORK_1 = new Guid("FF778EE5-2546-46F4-AC4D-EFAE272CE05E");
        private static Guid ACTIVE_WORK_2 = new Guid("53322CB2-C329-45C9-A8F7-A31761C2873F");
        private static Guid ACTIVE_WORK_3 = new Guid("60E31C7E-6F2D-40FC-9783-1A5D58C2EEFF");
        private static Guid ACTIVE_WORK_4 = new Guid("F2755311-0F74-483F-B4B9-82F9A0EC5ABA");
        private static Guid ACTIVE_WORK_5 = new Guid("40E91D39-73EE-42AD-B396-714368145988");
        private static Guid DELETED_WORK_1 = new Guid("5901855C-AFF2-47C3-B91E-70A2E73D5442");
        private static Guid DELETED_WORK_2 = new Guid("A803F4EA-A522-4344-AC44-F7EEA5578F82");
        private static Guid DELETED_WORK_3 = new Guid("276AD87E-ADC6-4DBA-B543-18496492191D");

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork2 = new Work(ACTIVE_WORK_2, "Active Work 2", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork3 = new Work(ACTIVE_WORK_3, "Active Work 3", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork4 = new Work(ACTIVE_WORK_4, "Active Work 4", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work activeWork5 = new Work(ACTIVE_WORK_5, "Active Work 5", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork1 = new Work(DELETED_WORK_1, "Deleted Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork2 = new Work(DELETED_WORK_2, "Deleted Work 2", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork3 = new Work(DELETED_WORK_3, "Deleted Work 3", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        public WorkRetrievalServiceTest()
        {
            mockWorkRepository = new Mock<IWorkRepository>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();

            mockTagRetrievalService = new Mock<ITagRetrievalService>();

            works = CloneHelper.Clone(new List<Work>() { activeWork1, activeWork2, activeWork3, activeWork4, activeWork5, deletedWork1, deletedWork2, deletedWork3 });

            mockWorkRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(works));

            mockUserBlockedTagRetrievalService = new Mock<IUserBlockedTagsRetrievalService>();
            mockUserBlockedTagRetrievalService.Setup(x => x.GetUserBlockedTags(It.IsAny<Guid>())).Returns(Task.FromResult(new List<User_Blocked_Tag>()));
        }

        [Fact]
        public async Task GetRemovedWorks_ReturnsExpectedResult()
        {
            //Arrange
            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            List<Work> expectedResultsReturned = new List<Work> { deletedWork1, deletedWork2, deletedWork3 };

            //Act
            List<Work> result = await workRetrievalService.GetDeletedWorks(null);

            //Assert
            Assert.Equal(expectedResultsReturned, result);
            Assert.Empty(result.Where(x => x.IsDeleted == false));
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleWorkTheoryData))]
        public async Task FindExistingAccessibleWork_ReturnsWork(Guid id, Work expected)
        {
            //Arrange
            mockWorkRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act
            Work result = await workRetrievalService.FindWorkAsync(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockWorkRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid, Work> FindExistingAccessibleWorkTheoryData
        {
            get
            {
                TheoryData<Guid, Work> data = new TheoryData<Guid, Work>
                {
                    { ACTIVE_WORK_1, CloneHelper.Clone(activeWork1) },
                    { DELETED_WORK_1, CloneHelper.Clone(deletedWork1) }
                };

                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingWork_ThrowsWorkNotFoundException()
        {
            //Arrange
            Guid id = new Guid();

            mockWorkRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act/Assert
            WorkNotFoundException exception = await Assert.ThrowsAsync<WorkNotFoundException>(() => workRetrievalService.FindWorkAsync(id, User, bypassCache));
            mockWorkRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindExistingWorkWithoutPermission_ThrowsWorkNotViewableException()
        {
            Guid id = DELETED_WORK_1;

            mockWorkRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act/Assert
            WorkNotViewableException exception = await Assert.ThrowsAsync<WorkNotViewableException>(() => workRetrievalService.FindWorkAsync(id, User, bypassCache));
            mockWorkRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleWorkTheoryData))]
        public async Task FindExistingAccessibleWorkForDeletion_ReturnsWork(Guid id, Work expected)
        {
            //Arrange
            mockWorkRepository.Setup(x => x.FindForDeletionAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act
            Work result = await workRetrievalService.FindWorkForDeletion(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockWorkRepository.Verify(mock => mock.FindForDeletionAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindNonExistingWorkForDeletion_ThrowsWorkNotFoundException()
        {
            //Arrange
            Guid id = new Guid();

            mockWorkRepository.Setup(x => x.FindForDeletionAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act/Assert
            WorkNotFoundException exception = await Assert.ThrowsAsync<WorkNotFoundException>(() => workRetrievalService.FindWorkForDeletion(id, User, bypassCache));
            mockWorkRepository.Verify(mock => mock.FindForDeletionAsync(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindExistingWorkForDeletionWithoutPermission_ThrowsWorkNotViewableException()
        {
            Guid id = DELETED_WORK_1;

            mockWorkRepository.Setup(x => x.FindForDeletionAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(works.Where(x => x.WorkID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            WorkRetrievalService workRetrievalService = new WorkRetrievalService(mockWorkRepository.Object, mockLogger.Object, mockAuthorizationService.Object, mockTagRetrievalService.Object, mockUserBlockedTagRetrievalService.Object);

            //Act/Assert
            WorkNotViewableException exception = await Assert.ThrowsAsync<WorkNotViewableException>(() => workRetrievalService.FindWorkForDeletion(id, User, bypassCache));
            mockWorkRepository.Verify(mock => mock.FindForDeletionAsync(id, bypassCache), Times.Once());
        }
    }
}
