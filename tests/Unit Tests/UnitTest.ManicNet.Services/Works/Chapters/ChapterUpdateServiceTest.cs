﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Models;
using ManicNet.Services;
using Microsoft.AspNetCore.Http;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Chapters
{
    public sealed class ChapterUpdateServiceTest
    {
        private readonly Mock<IUploadService> mockUploadService;
        private readonly Mock<IChapterRepository> mockChapterRepository;
        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IContentFileHandlerService> mockContentFileHandlerService;
        private readonly Mock<IWorkRepository> mockWorkRepository;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;

        //WorkIDs
        private static Guid ACTIVE_WORK_1 = new Guid("2D3CBCBE-6F95-41A8-A886-4F77C290C45E");

        //Volume ID's
        private static Guid ACTIVE_VOLUME_1 = new Guid("FAC0A91A-9FE8-4D57-B890-CFE2B0D8D4A7"); //Active under active

        //Chapter ID's
        private static Guid ACTIVE_CHAPTER_1 = new Guid("FDACB8D9-6C29-468E-8FFB-79334F8DB1DC"); //Under ACTIVE_VOLUME_1
        private static Guid DELETED_CHAPTER_1 = new Guid("825EB9E0-1F69-4FB5-BCBB-ECE2B8B90D3D"); //Under ACTIVE_VOLUME_1

        private static Guid ACTIVE_CHAPTER_WITH_TAGS = new Guid("36EEE1D0-72D7-471B-AFE7-E01F09BA9803");
        private static Guid DELETED_CHAPTER_WITH_TAGS = new Guid("738F9B0A-62A3-43B9-A3D8-87C5EA9A41CD");

        //Tag Guids
        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SCIENCE_FICTION_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid SWORDS_AND_SANDALS_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Volume activeVolume1 = new Volume(ACTIVE_VOLUME_1, activeWork1, 1, "Active Volume 1", null, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Chapter activeChapter1 = new Chapter(ACTIVE_CHAPTER_1, activeVolume1, 1, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter deletedChapter1 = new Chapter(DELETED_CHAPTER_1, activeVolume1, 5, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Chapter activeChapterWithTags = new Chapter(ACTIVE_CHAPTER_WITH_TAGS, activeVolume1, 9, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null, new List<Chapter_Tag>()
        {
            new Chapter_Tag() { ChapterID = ACTIVE_CHAPTER_WITH_TAGS, TagID = ACTION_ID },
            new Chapter_Tag() { ChapterID = ACTIVE_CHAPTER_WITH_TAGS, TagID = SCIENCE_FICTION_ID },
            new Chapter_Tag() { ChapterID = ACTIVE_CHAPTER_WITH_TAGS, TagID = CYBERPUNK_ID },
        });
        private static readonly Chapter deletedChapterWithTags = new Chapter(DELETED_CHAPTER_WITH_TAGS, activeVolume1, 10, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null, new List<Chapter_Tag>()
        {
            new Chapter_Tag() { ChapterID = DELETED_CHAPTER_WITH_TAGS, TagID = SWORDS_AND_SANDALS_ID },
            new Chapter_Tag() { ChapterID = DELETED_CHAPTER_WITH_TAGS, TagID = ROMANCE_ID },
        });

        public ChapterUpdateServiceTest()
        {
            mockUploadService = new Mock<IUploadService>();
            mockChapterRepository = new Mock<IChapterRepository>();
            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();
            mockLogger = new Mock<IManicNetLoggerService>();
            mockContentFileHandlerService = new Mock<IContentFileHandlerService>();
            mockWorkRepository = new Mock<IWorkRepository>();
            mockCacheCleanerService = new Mock<ICacheCleanerService>();
        }

        [Fact]
        public async Task CreateChapter_CallsExpectedMethods()
        {
            //Arrange
            Chapter chapter = new Chapter();
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();
            Guid workID = new Guid("93CA4EBC-A7A8-4D1D-BF75-025E73EDA659");
            Work work = new Work();
            IFormFile[] pageFiles = Array.Empty<IFormFile>();
            List<Content> contents = new List<Content>();

            mockWorkRepository.Setup(x => x.FindAsync(workID, It.IsAny<bool>())).Returns(Task.FromResult(work));

            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);

            //Act
            await chapterUpdateService.CreateChapter(chapter, primaryTags, secondaryTags, pageFiles, workID, new WorkCacheCleanup());

            //Assert
            mockChapterRepository.Verify(mock => mock.Insert(chapter, primaryTags, secondaryTags, contents), Times.Once());
            mockWorkRepository.Verify(mock => mock.Update(It.IsAny<Work>()), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateChapterWithIncludedTags_CallsExpectedMethods()
        {
            //Arrange
            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);

            Chapter chapter = new Chapter();
            Guid workID = new Guid("4F5A7482-3759-4CAD-9B9D-18D664D0396B");
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();
            ChapterModifyModel chapterModifyModel = new ChapterModifyModel();

            //Act
            await chapterUpdateService.UpdateChapter(chapter, primaryTags, secondaryTags, workID, chapterModifyModel, new WorkCacheCleanup());

            //Assert
            mockChapterRepository.Verify(mock => mock.Update(chapter, primaryTags, secondaryTags, new List<Content>()), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(new List<Guid>()), Times.Once());
        }

        [Fact]
        public async Task UpdateChapter_CallsExpectedMethods()
        {
            //Arrange
            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);

            Chapter chapter = new Chapter();
            Guid workID = new Guid("4F5A7482-3759-4CAD-9B9D-18D664D0396B");

            //Act
            await chapterUpdateService.UpdateChapter(chapter, new WorkCacheCleanup());

            //Assert
            mockChapterRepository.Verify(mock => mock.Update(chapter), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveChapterAsTopLevelInNonDeletableState_ThrowsChapterNotDeletableException()
        {
            //Arrange
            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);

            Chapter chapter = CloneHelper.Clone(activeChapter1);

            //Act/Assert
            ChapterNotDeletableException exception = await Assert.ThrowsAsync<ChapterNotDeletableException>(() => chapterUpdateService.RemoveChapter(chapter, new WorkCacheCleanup()));

            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(It.IsAny<Guid?>()), Times.Never());
            mockChapterRepository.Verify(mock => mock.Delete(chapter), Times.Never());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveChapterAsTopLevel_ReturnsExpectedResults()
        {
            //Arrange
            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);

            Chapter chapter = deletedChapter1;
            CleanupWork expected = new CleanupWork();

            //Act
            CleanupWork result = await chapterUpdateService.RemoveChapter(chapter, new WorkCacheCleanup());

            //Assert
            Assert.Equal(expected, result);
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(It.Is<IEnumerable<Guid>>(p => p.SequenceEqual(chapter.Pages.Select(p => p.ContentID)))), Times.Once());
            mockChapterRepository.Verify(mock => mock.Delete(chapter), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }


        [Theory]
        [MemberData(nameof(RemoveChapterNotAsTopLevel_ReturnsExpectedResultsTheoryData))]
        public async Task RemoveChapterNotTopLevel_ReturnsExpectedResults(Chapter chapter)
        {
            //Arrange
            ChapterUpdateService chapterUpdateService = new ChapterUpdateService(mockUploadService.Object, mockChapterRepository.Object, mockUnitOfWork.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockWorkRepository.Object, mockCacheCleanerService.Object);
            CleanupWork expected = new CleanupWork(chapter);

            //Act
            CleanupWork result = await chapterUpdateService.RemoveChapter(chapter, new WorkCacheCleanup(), false);

            //Assert
            Assert.Equal(expected, result);
            mockChapterRepository.Verify(mock => mock.Delete(chapter), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(It.IsAny<List<Guid>>()), Times.Never());
        }

        public static TheoryData<Chapter> RemoveChapterNotAsTopLevel_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Chapter> data = new TheoryData<Chapter>
                {
                    CloneHelper.Clone(activeChapter1),
                    CloneHelper.Clone(deletedChapter1),
                    CloneHelper.Clone(activeChapterWithTags),
                    CloneHelper.Clone(deletedChapterWithTags),
                };

                return data;
            }
        }
    }
}
