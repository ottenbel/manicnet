﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Chapters
{
    public sealed class ChapterRetrievalServiceTest
    {
        private bool bypassCache { get; set; }
        private readonly List<Chapter> chapters;

        private readonly Mock<IChapterRepository> mockChapterRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;

        private ClaimsPrincipal User { get; set; }

        //WorkIDs
        private static Guid ACTIVE_WORK_1 = new Guid("2D3CBCBE-6F95-41A8-A886-4F77C290C45E");
        private static Guid DELETED_WORK_1 = new Guid("3224CFF4-0FDE-4E3E-9B69-512BF4B67F43");

        //Volume ID's
        private static Guid ACTIVE_VOLUME_1 = new Guid("FAC0A91A-9FE8-4D57-B890-CFE2B0D8D4A7"); //Active under active
        private static Guid ACTIVE_VOLUME_2 = new Guid("82BC4E7D-8B05-4BCD-9A62-0127951ABCF0"); //Active under deleted
        private static Guid DELETED_VOLUME_1 = new Guid("356F4A15-8F16-4E96-8704-50B50EF843A1"); //Deleted under active
        private static Guid DELETED_VOLUME_2 = new Guid("5A3F373D-5DFA-4833-A3E2-FB67A8CE25BD"); //Deleted under deleted

        //Chapter ID's
        private static Guid ACTIVE_CHAPTER_1 = new Guid("FDACB8D9-6C29-468E-8FFB-79334F8DB1DC"); //Under ACTIVE_VOLUME_1
        private static Guid ACTIVE_CHAPTER_2 = new Guid("70AF8669-7396-40DF-B082-B885256792C1"); //Under ACTIVE_VOLUME_2
        private static Guid ACTIVE_CHAPTER_3 = new Guid("B1512271-E90E-4BFA-BCBE-A096F013F6F7"); //Under DELETED_VOLUME_1
        private static Guid ACTIVE_CHAPTER_4 = new Guid("60EC1B78-9CD6-4592-8C73-D44081E7CA34"); //Under DELETED_VOLUME_2
        private static Guid DELETED_CHAPTER_1 = new Guid("825EB9E0-1F69-4FB5-BCBB-ECE2B8B90D3D"); //Under ACTIVE_VOLUME_1
        private static Guid DELETED_CHAPTER_2 = new Guid("9A81785B-743A-49DC-959C-E2E67129DB3F"); //Under ACTIVE_VOLUME_2
        private static Guid DELETED_CHAPTER_3 = new Guid("8FC35CF5-4DDD-4066-9D35-7E1C0EF0951E"); //Under DELETED_VOLUME_1
        private static Guid DELETED_CHAPTER_4 = new Guid("0B861705-CDFB-4D54-ABF9-8C34D3600C89"); //Under DELETED_VOLUME_2

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork1 = new Work(DELETED_WORK_1, "Deleted Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Volume activeVolume1 = new Volume(ACTIVE_VOLUME_1, activeWork1, 1, "Active Volume 1", null, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolume2 = new Volume(ACTIVE_VOLUME_2, deletedWork1, 2, "Active Volume 2", null, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume1 = new Volume(DELETED_VOLUME_1, deletedWork1, 3, "Deleted Volume 1", null, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume2 = new Volume(DELETED_VOLUME_2, activeWork1, 4, "Deleted Volume 2", null, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Chapter activeChapter1 = new Chapter(ACTIVE_CHAPTER_1, activeVolume1, 1, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter activeChapter2 = new Chapter(ACTIVE_CHAPTER_2, activeVolume2, 2, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter activeChapter3 = new Chapter(ACTIVE_CHAPTER_3, deletedVolume1, 3, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter activeChapter4 = new Chapter(ACTIVE_CHAPTER_4, deletedVolume2, 4, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter deletedChapter1 = new Chapter(DELETED_CHAPTER_1, activeVolume1, 5, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter deletedChapter2 = new Chapter(DELETED_CHAPTER_2, activeVolume2, 6, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter deletedChapter3 = new Chapter(DELETED_CHAPTER_3, deletedVolume1, 7, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Chapter deletedChapter4 = new Chapter(DELETED_CHAPTER_4, deletedVolume2, 8, string.Empty, new List<Page>(), string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        public ChapterRetrievalServiceTest()
        {
            mockChapterRepository = new Mock<IChapterRepository>();
            mockLogger = new Mock<IManicNetLoggerService>();
            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();

            bypassCache = false;

            chapters = CloneHelper.Clone(new List<Chapter>() { activeChapter1, activeChapter2, activeChapter3, activeChapter4, deletedChapter1, deletedChapter2, deletedChapter3, deletedChapter4 });
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleChapterTheoryData))]
        public async Task FindExistingChapterVolume_ReturnsExpectedVolume(Guid id, Chapter expected)
        {
            //Arrange
            mockChapterRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(chapters.Where(x => x.ChapterID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            ChapterRetrievalService chapterRetrievalService = new ChapterRetrievalService(mockChapterRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            Chapter result = await chapterRetrievalService.FindChapter(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockChapterRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid, Chapter> FindExistingAccessibleChapterTheoryData
        {
            get
            {
                TheoryData<Guid, Chapter> data = new TheoryData<Guid, Chapter>
                {
                    { ACTIVE_CHAPTER_1, activeChapter1 },
                    { ACTIVE_CHAPTER_2, activeChapter2 },
                    { ACTIVE_CHAPTER_3, activeChapter3 },
                    { ACTIVE_CHAPTER_4, activeChapter4 },
                    { DELETED_CHAPTER_1, deletedChapter1 },
                    { DELETED_CHAPTER_2, deletedChapter2 },
                    { DELETED_CHAPTER_3, deletedChapter3 },
                    { DELETED_CHAPTER_4, deletedChapter4 },
                };

                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingChapter_ThrowsChapterNotFoundException()
        {
            //Arrange
            Guid id = new Guid();

            mockChapterRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(chapters.Where(x => x.ChapterID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            ChapterRetrievalService chapterRetrievalService = new ChapterRetrievalService(mockChapterRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            ChapterNotFoundException exception = await Assert.ThrowsAsync<ChapterNotFoundException>(() => chapterRetrievalService.FindChapter(id, User, bypassCache));
            mockChapterRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingChapterWithoutPermission_ThrowsChapterNotViewableExceptionTheoryData))]
        public async Task FindExistingChapterWithoutPermission_ThrowsVolumeNotViewableException(Guid id)
        {
            //Arrange
            mockChapterRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(chapters.Where(x => x.ChapterID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            ChapterRetrievalService chapterRetrievalService = new ChapterRetrievalService(mockChapterRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            ChapterNotViewableException exception = await Assert.ThrowsAsync<ChapterNotViewableException>(() => chapterRetrievalService.FindChapter(id, User, bypassCache));
            mockChapterRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid> FindExistingChapterWithoutPermission_ThrowsChapterNotViewableExceptionTheoryData
        {
            get
            {
                TheoryData<Guid> data = new TheoryData<Guid>
                {
                    { DELETED_CHAPTER_1 },
                    { DELETED_CHAPTER_2 },
                    { DELETED_CHAPTER_3 },
                    { DELETED_CHAPTER_4 },
                };

                return data;
            }
        }

        [Theory]
        [MemberData(nameof(FindExistingChapterWithoutPermission_ThrowsChapterUnableToViewParentWorkExceptionTheoryData))]
        public async Task FindExistingChapterWithoutViewParentPermission_ChapterUnableToViewParentWorkException(Guid id)
        {
            //Arrange
            mockChapterRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(chapters.Where(x => x.ChapterID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            ChapterRetrievalService chapterRetrievalService = new ChapterRetrievalService(mockChapterRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            ChapterUnableToViewParentWorkException exception = await Assert.ThrowsAsync<ChapterUnableToViewParentWorkException>(() => chapterRetrievalService.FindChapter(id, User, bypassCache));
            mockChapterRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid> FindExistingChapterWithoutPermission_ThrowsChapterUnableToViewParentWorkExceptionTheoryData
        {
            get
            {
                TheoryData<Guid> data = new TheoryData<Guid>
                {
                    { ACTIVE_CHAPTER_2 },
                    { ACTIVE_CHAPTER_3 },
                    { ACTIVE_CHAPTER_4 }
                };

                return data;
            }
        }
    }
}
