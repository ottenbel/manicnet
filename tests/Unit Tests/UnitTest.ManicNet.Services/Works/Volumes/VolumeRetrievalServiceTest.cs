﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Microsoft.AspNetCore.Authorization;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Volumes
{
    public sealed class VolumeRetrievalServiceTest
    {
        private bool bypassCache { get; set; }
        private readonly List<Volume> volumes;

        private readonly Mock<IVolumeRepository> mockVolumeRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IManicNetAuthorizationService> mockAuthorizationService;

        private ClaimsPrincipal User { get; set; }

        //Work Guids
        private static Guid ACTIVE_WORK_1 = new Guid("32F91D2D-1CF7-4667-98AA-81D45C78E3B9");
        private static Guid ACTIVE_WORK_2 = new Guid("C80E0534-774A-4381-8004-F4670B5D1E04");
        private static Guid DELETED_WORK_1 = new Guid("AC897654-1CEA-4D59-A695-2A0A4867EF78");

        //Volume Guids
        private static Guid ACTIVE_VOLUME_1 = new Guid("8258454B-BDEC-4412-8C05-8D73B231C9C2");
        private static Guid ACTIVE_VOLUME_2 = new Guid("B8B7247E-213D-4378-830E-8B7E7E149066");
        private static Guid ACTIVE_VOLUME_3 = new Guid("34806F1C-6412-4616-B5D1-5B391115BD36");
        private static Guid ACTIVE_VOLUME_4 = new Guid("0579C950-98C6-4954-95BA-B7EF92A1A467");
        private static Guid ACTIVE_VOLUME_5 = new Guid("0A2A113B-9719-42AA-BFE6-C496B52760A5");
        private static Guid DELETED_VOLUME_1 = new Guid("63D44803-7D9A-4A52-955B-B93CA9813F31");
        private static Guid DELETED_VOLUME_2 = new Guid("A451A604-E626-40F3-9FCD-365B19F0C925");
        private static Guid DELETED_VOLUME_3 = new Guid("6B9EDBE2-3D41-4E62-93C9-5917E7B36555");

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork1 = new Work(DELETED_WORK_1, "Deleted Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Volume activeVolume1 = new Volume(ACTIVE_VOLUME_1, activeWork1, 1, "Active Volume 1", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolume2 = new Volume(ACTIVE_VOLUME_2, activeWork1, 2, "Active Volume 2", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolume3 = new Volume(ACTIVE_VOLUME_3, deletedWork1, 3, "Active Volume 3", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolume4 = new Volume(ACTIVE_VOLUME_4, activeWork1, 4, "Active Volume 4", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolume5 = new Volume(ACTIVE_VOLUME_5, activeWork1, 5, "Active Volume 5", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume1 = new Volume(DELETED_VOLUME_1, deletedWork1, 1, "Deleted Volume 1", null, "Short description", "Long description", string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume2 = new Volume(DELETED_VOLUME_2, deletedWork1, 2, "Deleted Volume 2", null, "Short description", "Long description", string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume3 = new Volume(DELETED_VOLUME_3, activeWork1, 3, "Deleted Volume 3", null, "Short description", "Long description", string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        public VolumeRetrievalServiceTest()
        {
            mockVolumeRepository = new Mock<IVolumeRepository>();
            mockLogger = new Mock<IManicNetLoggerService>();
            mockAuthorizationService = new Mock<IManicNetAuthorizationService>();

            bypassCache = false;

            volumes = CloneHelper.Clone(new List<Volume>() { activeVolume1, activeVolume2, activeVolume3, activeVolume4, activeVolume5, deletedVolume1, deletedVolume2, deletedVolume3 });
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleVolumeTheoryData))]
        public async Task FindExistingAccessibleVolume_ReturnsExpectedVolume(Guid id, Volume expected)
        {
            //Arrange
            mockVolumeRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            Volume result = await volumeRetrievalService.FindVolume(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockVolumeRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid, Volume> FindExistingAccessibleVolumeTheoryData
        {
            get
            {
                TheoryData<Guid, Volume> data = new TheoryData<Guid, Volume>
                {
                    { ACTIVE_VOLUME_1, CloneHelper.Clone(activeVolume1) },   //Active volume under active work
                    { DELETED_VOLUME_3, CloneHelper.Clone(deletedVolume3) }, //Deleted volume under active work
                    { ACTIVE_VOLUME_3, CloneHelper.Clone(activeVolume3) },   //Active volume under deleted work
                    { DELETED_VOLUME_1, CloneHelper.Clone(deletedVolume1) }, //Active volume under deleted work
                };

                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingVolume_ThrowsVolumeNotFoundException()
        {
            //Arrange
            Guid id = new Guid();

            mockVolumeRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeNotFoundException exception = await Assert.ThrowsAsync<VolumeNotFoundException>(() => volumeRetrievalService.FindVolume(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingVolumeWithoutPermission_ThrowsVolumeNotViewableExceptionTheoryData))]
        public async Task FindExistingVolumeWithoutPermission_ThrowsVolumeNotViewableException(Guid id)
        {
            //Arrange
            mockVolumeRepository.Setup(x => x.FindAsync(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeNotViewableException exception = await Assert.ThrowsAsync<VolumeNotViewableException>(() => volumeRetrievalService.FindVolume(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindAsync(id, bypassCache), Times.Once());
        }

        public static TheoryData<Guid> FindExistingVolumeWithoutPermission_ThrowsVolumeNotViewableExceptionTheoryData
        {
            get
            {
                TheoryData<Guid> data = new TheoryData<Guid>
                {
                    { DELETED_VOLUME_3 }, //Deleted volume under active work
                    { DELETED_VOLUME_1 }, //Active volume under deleted work
                };

                return data;
            }
        }

        [Fact]
        public async Task FindExistingVolumeWithoutViewParentPermission_VolumeUnableToViewParentWorkException()
        {
            //Arrange
            Guid id = ACTIVE_VOLUME_3;
            mockVolumeRepository.Setup(x => x.FindForDeletion(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeUnableToViewParentWorkException exception = await Assert.ThrowsAsync<VolumeUnableToViewParentWorkException>(() => volumeRetrievalService.FindVolumeForDeletion(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindForDeletion(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingAccessibleVolumeTheoryData))]
        public async Task FindExistingAccessibleVolumeForDeletion_ReturnsExpectedVolume(Guid id, Volume expected)
        {
            //Arrange
            mockVolumeRepository.Setup(x => x.FindForDeletion(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            Volume result = await volumeRetrievalService.FindVolumeForDeletion(id, User, bypassCache);

            //Assert
            Assert.Equal(expected, result);
            mockVolumeRepository.Verify(mock => mock.FindForDeletion(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindNonExistingVolumeForDeletion_ThrowsVolumeNotFoundException()
        {
            //Arrange
            Guid id = new Guid();

            mockVolumeRepository.Setup(x => x.FindForDeletion(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Success()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeNotFoundException exception = await Assert.ThrowsAsync<VolumeNotFoundException>(() => volumeRetrievalService.FindVolumeForDeletion(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindForDeletion(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindExistingVolumeWithoutPermission_ThrowsVolumeNotViewableExceptionTheoryData))]
        public async Task FindExistingVolumeWithoutPermissionForDeletion_ThrowsVolumeNotViewableException(Guid id)
        {
            //Arrange
            mockVolumeRepository.Setup(x => x.FindForDeletion(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeNotViewableException exception = await Assert.ThrowsAsync<VolumeNotViewableException>(() => volumeRetrievalService.FindVolumeForDeletion(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindForDeletion(id, bypassCache), Times.Once());
        }

        [Fact]
        public async Task FindExistingVolumeForDeletionWithoutViewParentPermission_VolumeUnableToViewParentWorkException()
        {
            //Arrange
            Guid id = ACTIVE_VOLUME_3;
            mockVolumeRepository.Setup(x => x.FindForDeletion(id, It.IsAny<bool>())).Returns(Task.FromResult(volumes.Where(x => x.VolumeID == id).FirstOrDefault()));
            mockAuthorizationService.Setup(x => x.Authorize(It.IsAny<ClaimsPrincipal>(), It.IsAny<string>())).Returns(Task.FromResult<AuthorizationResult>(AuthorizationResult.Failed()));

            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act/Assert
            VolumeUnableToViewParentWorkException exception = await Assert.ThrowsAsync<VolumeUnableToViewParentWorkException>(() => volumeRetrievalService.FindVolumeForDeletion(id, User, bypassCache));
            mockVolumeRepository.Verify(mock => mock.FindForDeletion(id, bypassCache), Times.Once());
        }

        [Theory]
        [MemberData(nameof(GetVolumesForWork_ReturnsExpectedResultsTheoryData))]
        public async Task GetVolumesForWork_ReturnsExpectedResults(Guid id, List<Volume> expected)
        {
            //Arrange
            mockVolumeRepository.Setup(x => x.AllAsync()).Returns(Task.FromResult(volumes));
            VolumeRetrievalService volumeRetrievalService = new VolumeRetrievalService(mockVolumeRepository.Object, mockLogger.Object, mockAuthorizationService.Object);

            //Act
            List<Volume> result = await volumeRetrievalService.GetVolumesForWork(id);

            //Assert
            Assert.Equal(expected.OrderBy(x => x.VolumeID), result.OrderBy(x => x.VolumeID));
        }

        public static TheoryData<Guid?, List<Volume>> GetVolumesForWork_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Guid?, List<Volume>> data = new TheoryData<Guid?, List<Volume>>
                {
                    { ACTIVE_WORK_2, CloneHelper.Clone(new List<Volume>()) },
                    { ACTIVE_WORK_1, CloneHelper.Clone(new List<Volume>() { activeVolume1, activeVolume2, activeVolume4, activeVolume5, deletedVolume3 }) },
                    { DELETED_WORK_1, CloneHelper.Clone(new List<Volume>() { activeVolume3, deletedVolume1, deletedVolume2, }) }
                };

                return data;
            }
        }
    }
}
