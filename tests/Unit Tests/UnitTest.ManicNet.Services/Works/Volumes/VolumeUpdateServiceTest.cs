﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Domain.Models.Cache;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Works.Volumes
{
    public sealed class VolumeUpdateServiceTest
    {
        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<IVolumeRepository> mockVolumeRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;
        private readonly Mock<IContentFileHandlerService> mockContentFileHandlerService;
        private readonly Mock<IUploadService> mockUploadService;
        private readonly Mock<IChapterUpdateService> mockChapterUpdateService;
        private readonly Mock<ICacheCleanerService> mockCacheCleanerService;

        //Work Guids
        private static Guid ACTIVE_WORK_1 = new Guid("32F91D2D-1CF7-4667-98AA-81D45C78E3B9");
        private static Guid DELETED_WORK_1 = new Guid("AC897654-1CEA-4D59-A695-2A0A4867EF78");

        //Volume Guids
        private static Guid ACTIVE_VOLUME_1 = new Guid("8258454B-BDEC-4412-8C05-8D73B231C9C2");
        private static Guid DELETED_VOLUME_1 = new Guid("63D44803-7D9A-4A52-955B-B93CA9813F31");
        private static Guid ACTIVE_VOLUME_WITH_TAGS = new Guid("2E131380-9B7C-4DAC-ABC5-8E75DD1EB699");
        private static Guid DELETED_VOLUME_WITH_TAGS = new Guid("BE111E33-9DD1-40E1-ABB1-F0E152EB708A");

        //Tag Guids
        private static Guid ACTION_ID = new Guid("D8C9FEAB-0080-4F37-B302-38FBF93501EB");
        private static Guid SCIENCE_FICTION_ID = new Guid("67B2538F-C1D6-41B0-909E-5D8A5CA183F8");
        private static Guid CYBERPUNK_ID = new Guid("0A7EAF35-8F29-4EF5-BB40-CADC612D7CB8");
        private static Guid SWORDS_AND_SANDALS_ID = new Guid("56725834-8379-48FF-8C64-990684DA11B6");
        private static Guid ROMANCE_ID = new Guid("F5787A2A-BFE3-4796-AA2E-CD4316471413");

        private static readonly Work activeWork1 = new Work(ACTIVE_WORK_1, "Active Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Work deletedWork1 = new Work(DELETED_WORK_1, "Deleted Work 1", Guid.Empty, DateTime.UtcNow, string.Empty, string.Empty, string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);

        private static readonly Volume activeVolume1 = new Volume(ACTIVE_VOLUME_1, activeWork1, 1, "Active Volume 1", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume deletedVolume1 = new Volume(DELETED_VOLUME_1, deletedWork1, 1, "Deleted Volume 1", null, "Short description", "Long description", string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null);
        private static readonly Volume activeVolumeWithTags = new Volume(ACTIVE_VOLUME_WITH_TAGS, activeWork1, 1, "Active Volume 1", null, "Short description", "Long description", string.Empty, false, DateTime.UtcNow, null, DateTime.UtcNow, null,
            new List<Volume_Tag>()
            {
                new Volume_Tag() { VolumeID = ACTIVE_VOLUME_WITH_TAGS, TagID = ACTION_ID },
                new Volume_Tag() { VolumeID = ACTIVE_VOLUME_WITH_TAGS, TagID = SCIENCE_FICTION_ID },
                new Volume_Tag() { VolumeID = ACTIVE_VOLUME_WITH_TAGS, TagID = CYBERPUNK_ID },
            });
        private static readonly Volume deletedVolumeWithTags = new Volume(DELETED_VOLUME_WITH_TAGS, activeWork1, 3, "Deleted Volume 3", null, "Short description", "Long description", string.Empty, true, DateTime.UtcNow, null, DateTime.UtcNow, null,
            new List<Volume_Tag>()
            {
                new Volume_Tag() { VolumeID = ACTIVE_VOLUME_WITH_TAGS, TagID = SWORDS_AND_SANDALS_ID },
                new Volume_Tag() { VolumeID = ACTIVE_VOLUME_WITH_TAGS, TagID = ROMANCE_ID },
            });

        public VolumeUpdateServiceTest()
        {
            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();
            mockVolumeRepository = new Mock<IVolumeRepository>();
            mockLogger = new Mock<IManicNetLoggerService>();
            mockContentFileHandlerService = new Mock<IContentFileHandlerService>();
            mockUploadService = new Mock<IUploadService>();
            mockChapterUpdateService = new Mock<IChapterUpdateService>();
            mockCacheCleanerService = new Mock<ICacheCleanerService>();
        }

        [Fact]
        public async Task CreateVolume_CallsExpectedMethods()
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);

            Volume volume = new Volume();
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();

            //Act
            await volumeUpdateService.CreateVolume(volume, primaryTags, secondaryTags, null, new WorkCacheCleanup());

            //Assert
            mockVolumeRepository.Verify(mock => mock.Insert(volume, primaryTags, secondaryTags), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateVolume_CallsExpectedMethods()
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);

            Volume volume = new Volume();

            //Act
            await volumeUpdateService.UpdateVolume(volume, new WorkCacheCleanup());

            //Assert
            mockVolumeRepository.Verify(mock => mock.Update(volume), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Fact]
        public async Task UpdateVolumeWithIncludedTags_CallsExpectedMethods()
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);

            Volume volume = new Volume();
            List<Tag> primaryTags = new List<Tag>();
            List<Tag> secondaryTags = new List<Tag>();

            //Act
            await volumeUpdateService.UpdateVolume(volume, primaryTags, secondaryTags, null, false, new WorkCacheCleanup());

            //Assert
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(volume.CoverID), Times.Once());
            mockVolumeRepository.Verify(mock => mock.Update(volume, primaryTags, secondaryTags), Times.Once);
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once);
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveVolumeAsTopLevelInNonDeletableState_ThrowsVolumeNotDeletableException()
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);

            Volume volume = new Volume();

            //Act/Assert
            VolumeNotDeletableException exception = await Assert.ThrowsAsync<VolumeNotDeletableException>(() => volumeUpdateService.RemoveVolume(volume, new WorkCacheCleanup()));

            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(volume.CoverID), Times.Never());
            mockVolumeRepository.Verify(mock => mock.Delete(volume), Times.Never());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        [Fact]
        public async Task RemoveVolumeAsTopLevel_ReturnsExpectedResults()
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);

            Volume volume = new Volume
            {
                IsDeleted = true
            };
            CleanupWork expected = new CleanupWork();

            //Act
            CleanupWork result = await volumeUpdateService.RemoveVolume(volume, new WorkCacheCleanup());

            //Assert
            Assert.Equal(expected, result);
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(result.Contents), Times.Once());
            mockVolumeRepository.Verify(mock => mock.Delete(volume), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
            //TODO: Check update cache
        }

        [Theory]
        [MemberData(nameof(RemoveVolumeNotAsTopLevel_ReturnsExpectedResultsTheoryData))]
        public async Task RemoveVolumeNotTopLevel_ReturnsExpectedResults(Volume volume)
        {
            //Arrange
            VolumeUpdateService volumeUpdateService = new VolumeUpdateService(mockUnitOfWork.Object, mockVolumeRepository.Object, mockLogger.Object, mockContentFileHandlerService.Object, mockUploadService.Object, mockChapterUpdateService.Object, mockCacheCleanerService.Object);
            CleanupWork expected = new CleanupWork(volume);

            //Act
            CleanupWork result = await volumeUpdateService.RemoveVolume(volume, new WorkCacheCleanup(), false);

            //Assert
            Assert.Equal(expected, result);
            mockContentFileHandlerService.Verify(mock => mock.CleanupContent(volume.CoverID), Times.Never());
            mockVolumeRepository.Verify(mock => mock.Delete(volume), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Never());
            //TODO: Check update cache
        }

        public static TheoryData<Volume> RemoveVolumeNotAsTopLevel_ReturnsExpectedResultsTheoryData
        {
            get
            {
                TheoryData<Volume> data = new TheoryData<Volume>
                {
                    CloneHelper.Clone(activeVolume1),
                    CloneHelper.Clone(deletedVolume1),
                    CloneHelper.Clone(activeVolumeWithTags),
                    CloneHelper.Clone(deletedVolumeWithTags)
                };

                return data;
            }
        }
    }
}
