﻿using ManicNet.Contracts.Repositories;
using ManicNet.Contracts.Services;
using ManicNet.Contracts.UnitOfWork;
using ManicNet.Domain.Exceptions;
using ManicNet.Domain.Models;
using ManicNet.Helpers;
using ManicNet.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace UnitTest.ManicNet.Services.Images
{
    public sealed class AvatarServiceTest
    {
        private readonly List<Avatar> avatars;

        private readonly Mock<IManicNetUnitOfWork> mockUnitOfWork;
        private readonly Mock<IAvatarRepository> mockAvatarRepository;
        private readonly Mock<IManicNetLoggerService> mockLogger;

        public const string AVATAR_1_ID = "04490699-B2CF-4A7A-A7DA-EC791BACA146";
        public const string AVATAR_2_ID = "51B5A173-DD99-497C-9739-CDE13B4A234C";
        public const string AVATAR_3_ID = "A49FB494-D714-496F-BA6D-F73525D57551";
        public const string AVATAR_UNKNOWN_ID = "B1B65BFA-533E-476F-BD1C-A1641667A567";

        private static readonly byte[] AVATAR_1_HASH = new byte[] { 0x61, 0x07, 0xe7, 0x5c, 0x31, 0x5d, 0x6e, 0x7d, 0xbd, 0xd3 };
        private static readonly byte[] AVATAR_2_HASH = new byte[] { 0x36, 0x52, 0x7c, 0x24, 0x92, 0x29, 0x11, 0x9b, 0xa3, 0x31 };
        private static readonly byte[] AVATAR_3_HASH = new byte[] { 0x78, 0xe1, 0x24, 0x73, 0xf0, 0xb8, 0x3b, 0xac, 0xc3, 0x3f };
        private static readonly byte[] AVATAR_UNKNOWN_HASH = new byte[] { 0x71, 0xdc, 0xde, 0x44, 0x1f, 0x4d, 0xcc, 0x3e, 0x60, 0x41 };

        private static readonly Avatar avatar1 = new Avatar(new Guid(AVATAR_1_ID), "File 1", AVATAR_1_HASH);
        private static readonly Avatar avatar2 = new Avatar(new Guid(AVATAR_2_ID), "File 2", AVATAR_2_HASH);
        private static readonly Avatar avatar3 = new Avatar(new Guid(AVATAR_3_ID), "File 3", AVATAR_3_HASH);

        public AvatarServiceTest()
        {
            avatars = CloneHelper.Clone(new List<Avatar>() { avatar1, avatar2, avatar3 });

            mockUnitOfWork = new Mock<IManicNetUnitOfWork>();

            mockLogger = new Mock<IManicNetLoggerService>();
            mockLogger.Setup(x => x.LogError(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));
            mockLogger.Setup(x => x.LogWarning(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Exception>(), It.IsAny<string>()));

            mockAvatarRepository = new Mock<IAvatarRepository>();
        }

        [Theory]
        [MemberData(nameof(FindExistingAvatarByIDTheoryData))]
        public async Task FindExistingAvatarByID_ReturnsExpectedResult(Guid id, Avatar expected)
        {
            //Arrange
            mockAvatarRepository.Setup(x => x.FindAsync(id)).Returns(Task.FromResult<Avatar>(avatars.Where(a => a.AvatarID == id).FirstOrDefault()));

            AvatarService avatarService = new AvatarService(mockUnitOfWork.Object, mockAvatarRepository.Object, mockLogger.Object);

            //Act
            Avatar result = await avatarService.FindAvatar(id);

            //Assert
            Assert.Equal(expected, result);
            mockAvatarRepository.Verify(mock => mock.FindAsync(id), Times.Once());
        }

        public static TheoryData<Guid, Avatar> FindExistingAvatarByIDTheoryData
        {
            get
            {
                TheoryData<Guid, Avatar> data = new TheoryData<Guid, Avatar>
                {
                    { new Guid(AVATAR_1_ID), CloneHelper.Clone(avatar1) },
                    { new Guid(AVATAR_2_ID), CloneHelper.Clone(avatar2) },
                    { new Guid(AVATAR_3_ID), CloneHelper.Clone(avatar3) }
                };

                return data;
            }
        }

        [Fact]
        public async Task FindNonExistingAvatarByID_ThrowsAvatarNotFoundException()
        {
            //Arrange
            Guid id = new Guid(AVATAR_UNKNOWN_ID);

            mockAvatarRepository.Setup(x => x.FindAsync(id)).Returns(Task.FromResult<Avatar>(avatars.Where(a => a.AvatarID == id).FirstOrDefault()));

            AvatarService avatarService = new AvatarService(mockUnitOfWork.Object, mockAvatarRepository.Object, mockLogger.Object);

            //Act/Assert
            AvatarNotFoundException exception = await Assert.ThrowsAsync<AvatarNotFoundException>(() => avatarService.FindAvatar(id));
            mockAvatarRepository.Verify(mock => mock.FindAsync(id), Times.Once());
        }

        [Theory]
        [MemberData(nameof(FindAvatarByHashTheoryData))]
        public async Task FindAvatarByHash_ReturnsExpectedResult(byte[] hash, Avatar expected)
        {
            //Arrange
            mockAvatarRepository.Setup(x => x.FindAsync(hash)).Returns(Task.FromResult<Avatar>(avatars.Where(a => a.FileHash.SequenceEqual(hash)).FirstOrDefault()));

            AvatarService avatarService = new AvatarService(mockUnitOfWork.Object, mockAvatarRepository.Object, mockLogger.Object);

            //Act
            var result = await avatarService.FindAvatar(hash);

            //Assert
            Assert.Equal(expected, result);
            mockAvatarRepository.Verify(mock => mock.FindAsync(hash), Times.Once());
        }

        public static TheoryData<byte[], Avatar> FindAvatarByHashTheoryData
        {
            get
            {
                TheoryData<byte[], Avatar> data = new TheoryData<byte[], Avatar>
                {
                    { AVATAR_1_HASH, CloneHelper.Clone(avatar1) },
                    { AVATAR_2_HASH, CloneHelper.Clone(avatar2) },
                    { AVATAR_3_HASH, CloneHelper.Clone(avatar3) },
                    { AVATAR_UNKNOWN_HASH, null }
                };

                return data;
            }
        }

        [Fact]
        public async Task CreateAvatar_CallsExpectedMethods()
        {
            //Arrange
            Avatar avatar = new Avatar("new file", new byte[] { 0xcf, 0xf4, 0x58, 0x1a, 0x40, 0xa8, 0x9e, 0xa0, 0xbc, 0xa0 });
            AvatarService avatarService = new AvatarService(mockUnitOfWork.Object, mockAvatarRepository.Object, mockLogger.Object);

            //Act
            await avatarService.CreateAvatar(avatar);

            //Assert
            mockAvatarRepository.Verify(mock => mock.Insert(avatar), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
        }

        [Fact]
        public async Task RemoveAvatar_CallsExpectedMethods()
        {
            //Arrange
            Avatar avatar = new Avatar("new file", new byte[] { 0xcf, 0xf4, 0x58, 0x1a, 0x40, 0xa8, 0x9e, 0xa0, 0xbc, 0xa0 });
            AvatarService avatarService = new AvatarService(mockUnitOfWork.Object, mockAvatarRepository.Object, mockLogger.Object);

            //Act
            await avatarService.RemoveAvatar(avatar);

            //Assert
            mockAvatarRepository.Verify(mock => mock.Delete(avatar), Times.Once());
            mockUnitOfWork.Verify(mock => mock.SaveAsync(), Times.Once());
        }
    }
}
