using ManicNet.Extensions;
using System.Collections.Generic;
using Xunit;

namespace UnitTest.ManicNet.Extensions
{
    public sealed class SearchStringHelperTest
    {
        [Theory]
        [MemberData(nameof(FromCommaSeparatedStringToDistinctPopulatedListTheoryData))]
        public void FromCommaSeparatedStringToDistinctPopulatedList_ReturnsExpectedResult(string input, List<string> expected)
        {
            //Arrange

            //Act
            List<string> result = input.FromCommaSeparatedStringToDistinctPopulatedList();

            //Assert
            Assert.Equal(expected, result);
        }

        public static TheoryData<string, List<string>> FromCommaSeparatedStringToDistinctPopulatedListTheoryData
        {
            get
            {
                var data = new TheoryData<string, List<string>>
                {
                    { null, new List<string>()},
                    { string.Empty, new List<string>() },
                    { ", , ,    ,  ", new List<string>() },
                    { "Tag", new List<string>(){ "Tag" } },
                    { " Tag ", new List<string>(){ "Tag" } },
                    { "Tag 1, Tag 2,", new List<string>(){ "Tag 1", "Tag 2" } },
                    { "Tag 1, Tag 1, Tag 2", new List<string>(){ "Tag 1", "Tag 2" }},
                    { "Tag 1, Tag 1, Tag 2, , , , Tag 6", new List<string>(){ "Tag 1", "Tag 2", "Tag 6" }}
                };

                return data;
            }
        }
    }
}
